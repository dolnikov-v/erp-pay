<?php

namespace app\modules\report\extensions;

use app\helpers\report\Route;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\search\filters\NoDoubleFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormCallCenter;
use app\modules\report\components\ReportFormCallCenterCheckRequest;
use app\modules\report\components\ReportFormCheckUndelivery;
use app\modules\report\components\ReportFormDelivery;
use app\modules\report\components\ReportFormDeliverySummary;
use app\modules\report\components\ReportFormFinance;
use app\modules\report\components\ReportFormLeadByCountry;
use app\modules\report\components\ReportFormPurchaseStorage;
use app\modules\report\components\ReportFormSalary;
use app\modules\report\components\ReportFormStatus;
use app\modules\report\components\ReportFormTeam;
use app\modules\report\components\ReportFormWebmasterAnalytics;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class DataColumnCount
 * @package app\modules\report\extensions
 */
class DataColumnCount extends DataColumn
{
    /**
     * @var boolean
     */
    public $calculatePercent;

    /**
     * @var boolean
     */
    public $hidePercent = false;

    /**
     * @var string
     */
    public $columnLinkParams;

    /**
     * @var array
     */
    public $extraLinkParams = [];

    /**
     * @var boolean
     */
    public $makeLinkToOrders = true;

    /**
     * @var null|Country[]
     */
    protected static $countries = null;

    /**
     * @var null|Delivery[]
     */
    protected static $countryDeliveries = null;

    /**
     * @var array
     */
    public $statuses = [];

    /**
     * @var null|string|callable
     */
    public $beforeContent = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (is_null(self::$countries) || is_null(self::$countryDeliveries)) {
            self::$countries = Country::find()->with(['deliveries'])->indexBy('id')->all();
            self::$countryDeliveries = [];
            foreach (self::$countries as $country) {
                self::$countryDeliveries[$country->slug] = $country->deliveries;
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $this->calculatePercent = $this->grid->reportForm->withPercent;

        $dataCellContent = parent::renderDataCellContent($model, $key, $index);

        if ($this->makeLinkToOrders) {
            $dataCellContentView = $this->generateLinkToOrders($dataCellContent, $model[Query::GROUP_BY_NAME] ?? false, $model);
        } else {
            $dataCellContentView = $dataCellContent;
        }

        if (!$this->hidePercent && !$this->calculatePercent) {
            $totalCount = 0;

            if ($this->grid->reportForm instanceof ReportFormWebmasterAnalytics) {
                $dataCellContent = $this->getDataCellValue($model, $key, $index);
                if (!$dataCellContent) {
                    return '—';
                }
            }

            if ($this->grid->reportForm instanceof ReportFormFinance || $this->grid->reportForm instanceof ReportFormDelivery) {
                if ($this->attribute == Yii::t('common', 'Деньги получены') || $this->attribute == Yii::t('common', 'Деньги не получены')) {
                    $totalCount = isset($model[Yii::t('common', 'Выкуплено')]) ? $model[Yii::t('common',
                        'Выкуплено')] : 0;
                } else {
                    $totalCount = $this->getTotalCountFinance($model);
                }
            } else {
                if (isset($model[Yii::t('common', 'Всего')])) {
                    $totalCount = $model[Yii::t('common', 'Всего')];
                } elseif (isset($model['vsego'])) {
                    $totalCount = $model['vsego'];
                }
            }

            $percent = $this->calculatePercent($dataCellContent, $totalCount);
            $dataCellContentView .= ' (' . $percent . ')';
        }

        $beforeContent = '';
        if ($this->beforeContent) {
            if (is_callable($this->beforeContent)) {
                $beforeContent = (string)call_user_func($this->beforeContent, $model);
            }
            if (is_string($this->beforeContent)) {
                $beforeContent = $this->beforeContent;
            }
        }

        return $beforeContent . $dataCellContentView;
    }

    /**
     * @inheritdoc
     */
    protected function renderPageSummaryCellContent()
    {
        $pageSummaryCellContent = parent::renderPageSummaryCellContent();

        $calculatePercent = $this->calculatePercent ? false : true;
        $makeLinkToOrders = $this->makeLinkToOrders;
        if (!$this->pageSummary && ($this->grid->reportForm instanceof ReportFormDelivery || $this->grid->reportForm instanceof ReportFormDeliverySummary)) {
            $calculatePercent = false;
            $makeLinkToOrders = false;
        }


        if ($makeLinkToOrders) {
            $pageSummaryCellContentView = $this->generateLinkToOrders($pageSummaryCellContent);
        } else {
            $pageSummaryCellContentView = $pageSummaryCellContent;
        }

        $dataProvider = $this->grid->dataProvider;

        if ($this->grid->reportForm instanceof ReportFormFinance || $this->grid->reportForm instanceof ReportFormDelivery) {
            if ($this->attribute == Yii::t('common', 'Деньги получены') || $this->attribute == Yii::t('common', 'Деньги не получены')) {
                $totalCount = $dataProvider->getTotalCountFinanceMoneyReceived();
            } else {
                $totalCount = $dataProvider->getTotalCountFinance();
            }
        } elseif ($this->grid->reportForm instanceof ReportFormCallCenter) {
            if ($this->isGroupByProducts()) {
                $totalCount = $dataProvider->getTotalCountProductsForCallCenter();
            } else {
                $totalCount = $dataProvider->getTotalCount();
            }
        } elseif ($this->grid->reportForm instanceof ReportFormTeam ||
            $this->grid->reportForm instanceof ReportFormSalary ||
            $this->grid->reportForm instanceof ReportFormWebmasterAnalytics
        ) {
            $reportForm = $this->grid->reportForm;
            /* @var ReportFormTeam $reportForm */
            $totalCount = $reportForm->getTotalCount();
        } else {
            //if ($this->isGroupByProducts()) {
            //    $totalCount = $dataProvider->getTotalCountProducts();
            //} else {
            $totalCount = $dataProvider->getTotalCount();
            //}
        }

        if ($calculatePercent) {
            $percent = $this->calculatePercent($pageSummaryCellContent, $totalCount);
            $pageSummaryCellContentView .= ' (' . $percent . ')';
        }

        return $pageSummaryCellContentView;
    }

    /**
     * @param integer $count
     * @param integer $totalCount
     *
     * @return string
     */
    protected function calculatePercent($count, $totalCount)
    {
        if (is_numeric($totalCount) && (int)$totalCount > 0) {
            $percent = number_format((int)$count / (int)$totalCount * 100, 2,
                    Yii::$app->get('formatter')->decimalSeparator,
                    Yii::$app->get('formatter')->thousandSeparator) . '%';
        } else {
            $percent = number_format(0, 2) . '%';
        }

        return $percent;
    }

    /**
     * @param $text
     * @param string|boolean $rowLinkParams
     * @param array $model
     *
     * @return string
     * Формирование ссылки, для перехода на страницу заказов, с активными фильтрами
     */
    protected function generateLinkToOrders($text, $rowLinkParams = false, $model = [])
    {
        if (!$this->canIdentifyCountry($model)) {
            return $text;
        }

        $params = $this->getReportFormParams();

        $statusFilter = [];

        $checkingFilter = [];

        if ($this->grid->reportForm instanceof ReportFormCheckUndelivery ||
            $this->grid->reportForm instanceof ReportFormCallCenterCheckRequest
        ) {
            $buffer = explode(',', $this->columnLinkParams);
            $checkingFilter['type'] = [];
            $checkingFilter['informationType'] = [];
            foreach ($buffer as $status) {
                $checkingFilter['type'][] = 'check_undelivery';
                $checkingFilter['informationType'][] = $status;
            }

            if ($this->grid->reportForm instanceof ReportFormCallCenterCheckRequest) {
                $statusFilter['status'] = implode(',', $this->statuses);
            }
        } else {
            if (!is_array($this->columnLinkParams)) {
                $statusFilter['status'] = $this->columnLinkParams;
            }
        }

        $callCenterFilter = [
            'callCenter' => isset($params['callCenter']) ? $params['callCenter'] : "",
        ];

        $deliveryFilter = [
            'delivery' => $model['delivery_id'] ?? $params['delivery'] ?? $params['delivery_ids'] ?? "",
        ];

        $productFilter = [
            'product' => isset($params['product']) ? $params['product'] : "",
        ];

        $leadProductFilter = [
            'product' => $params['leadProduct'] ?? "",
        ];

        $sourceFilter = [
            'origin' => isset($params['source']) ? $params['source'] : "",
        ];

        $dateType = $params['type'] ?? DateFilter::TYPE_CREATED_AT;

        $textFilter = [];
        $numberFilter = [];

        $defaultDate = $this->grid->reportForm->getDateFilter();

        if ($rowLinkParams === false) {

            $dateFilter = [
                'from' => $defaultDate['from'],
                'to' => $defaultDate['to'],
                'type' => $dateType,
            ];

            if ($this->isGroupByProducts() ||
                $this->isGroupByDateProducts()
            ) {
                $productFilter['product'] = $this->grid->dataProvider->getProducts();
            }

            if ($this->isGroupByLeadProducts()) {
                $leadProductFilter['product'] = $this->grid->dataProvider->getProducts();
            }

            if ($this->isGroupByCountryLeadProducts() && isset($model['product_id'])) {
                $leadProductFilter['product'] = $model['product_id'];
            }

            if ($this->isGroupByDateProducts() && isset($model['product_id'])) {
                $leadProductFilter['product'] = $model['product_id'];
            }

        } else {

            $dateFilter = [
                'from' => isset($params['from']) ? $params['from'] : date('d.m.Y'),
                'to' => isset($params['to']) ? $params['to'] : date('d.m.Y'),
                'type' => $dateType,
            ];

            if ($this->isGroupByProducts()) {
                $productFilter['product'] = $rowLinkParams;
            }

            if ($this->isGroupByLeadProducts() ||
                $this->isGroupByCountryLeadProducts()
            ) {
                $leadProductFilter['product'] = $rowLinkParams;
            }

            if ($this->isGroupByDate()) {

                if (!$this->grid instanceof GridViewDeliverySummary) {
                    $dateFilter['from'] = $rowLinkParams;
                    $dateFilter['to'] = $rowLinkParams;
                }

            } elseif ($this->isGroupByCustomerCity()) {

                $textFilter = [
                    'entity' => 'city',
                    'text' => $rowLinkParams,
                ];

            } elseif ($this->isGroupByCustomerDistrict()) {

                $textFilter = [
                    'entity' => 'district',
                    'text' => $rowLinkParams,
                ];

            } elseif ($this->isGroupByCustomerProvince()) {

                $textFilter = [
                    'entity' => 'province',
                    'text' => $rowLinkParams,
                ];

            } elseif ($this->isGroupByCustomerZip()) {

                $textFilter = [
                    'entity' => 'zip',
                    'text' => $rowLinkParams,
                ];

            }

            if ($this->isGroupByDateProducts()) {
                if ($this->grid instanceof GridViewDeliverySummary) {
                    $productFilter['product'] = $rowLinkParams;
                }
                if ($this->grid instanceof GridViewDelivery) {
                    $dateFilter['from'] = $rowLinkParams;
                    $dateFilter['to'] = $rowLinkParams;

                    $productFilter['product'] = $model['product_id'];
                }
            }
        }

        if ($this->isGroupByWebmaster()) {
            $dateFilter = [
                'from' => $defaultDate['from'],
                'to' => $defaultDate['to'],
                'type' => $dateType,
            ];
            $textFilter = [
                'entity' => 'webmaster',
                'text' => $rowLinkParams,
            ];
        }
        if ($this->isGroupByLastForeignOperator()) {
            $dateFilter = method_exists($this->grid->reportForm, 'getDailyFilter') ? $this->grid->reportForm->getDailyFilter() : $this->grid->reportForm->getDateFilter();
            $dateFilter = [
                'from' => $dateFilter['from'],
                'to' => $dateFilter['to'],
                'type' => $dateType,
            ];
            $numberFilter = [
                'entity' => 'last_foreign_operator',
                'number' => $rowLinkParams,
            ];
        }
        if ($this->isGroupByYearMonth()) {
            $dateFilter = $this->grid->reportForm->getDateFilter();
            if (!empty($model) && !empty($model['groupbylabel'])) {
                $date = strtotime($model['groupbylabel'] . '-01');
                $dateFilter = [
                    'from' => date('d.m.Y', $date),
                    'to' => date('t.m.Y', $date),
                    'type' => $dateType,
                ];
            } else {
                $dateFilter = [
                    'from' => $dateFilter['from'],
                    'to' => $dateFilter['to'],
                    'type' => $dateType,
                ];
            }
        }

        if ($this->isGroupByCountryDate() || $this->isGroupByCountryDeliveryDate()) {
            $dateFilter = [
                'from' => $model ? $model['date'] ?? $dateFilter['from'] : $dateFilter['from'],
                'to' => $model ? $model['date'] ?? $dateFilter['to'] : $dateFilter['to'],
                'type' => $dateType,
            ];
        }

        $deliveryZipGroupFilter = [];
        if ($this->isGroupByDeliveryZipcodes()) {
            if (empty($model['delivery_zipcodes_id'])) {
                $allGroups = $this->grid->reportForm->getAllGroups();
                $deliveryZipGroupFilter = [
                    'not' => array_fill(0, sizeof($allGroups), 1),
                    'list' => $allGroups,
                ];
            } else {
                $deliveryZipGroupFilter = [
                    'list' => $model['delivery_zipcodes_id'],
                ];
            }
        }

        if ($this->isGroupByDateCountryDelivery()) {

            if (isset($model['delivery_id'])) {
                $deliveryFilter = [
                    'delivery' => $model['delivery_id'],
                ];
            }
        }

        if ($this->isGroupByProvinceCityDistrictZip() && $model) {
            $textFilter = [
                'entity' => [
                    'province',
                    'city',
                    'subdistrict',
                    'zip',
                ],
                'text' => [
                    $model['groupbylabel'],
                    $model['city'],
                    $model['subdistrict'],
                    $model['zip'],
                ],
            ];
        }

        if ($this->isGroupByDistrictZip() && $model) {
            $textFilter = [
                'entity' => [
                    'subdistrict',
                    'zip',
                ],
                'text' => [
                    $model['groupbylabel'],
                    $model['zip'],
                ],
            ];
        }

        if (empty($dateFilter['timezone']) && ($dateFilterTemp = $this->grid->reportForm->getDateFilter()) && !empty($dateFilterTemp['timezone'])) {
            $dateFilter = array_merge($dateFilter ?? [], ['timezone' => $dateFilterTemp['timezone']]);
        }

        if ($this->grid->reportForm instanceof ReportFormWebmasterAnalytics) {
            $reportForm = $this->grid->reportForm;

            if (!empty($model['webmaster_identifier'])) {
                $textFilter = [
                    'entity' => 'webmaster',
                    'text' => $model['webmaster_identifier'] ?? ''
                ];
            } else {
                /** @var ReportFormWebmasterAnalytics $reportForm */
                $webmasterFilter = $reportForm->getWebmasterFilter();
                if (!empty($webmasterFilter->name)) {
                    $textFilter = [
                        'entity' => 'webmaster',
                        'text' => $webmasterFilter->name
                    ];
                }
            }

            if (!empty($model['lead_product_id'])) {
                $leadProductFilter = [
                    'product' => $model['lead_product_id'] ?? ''
                ];
            }

            if (isset($this->columnLinkParams['calls_count'])) {
                $numberFilter = [
                    'entity' => 'calls_count',
                    'number' => $this->columnLinkParams['calls_count']
                ];
            }

            if (!empty($model['call_center_id'])) {
                $callCenterFilter = [
                    'callCenter' => $model['call_center_id'] ?? ''
                ];
            }

            if (isset($this->columnLinkParams['statuses'])) {
                $statusFilter['status'] = $this->columnLinkParams['statuses'];
            }

        }

        $url = [
            '/order/index/index'
        ];

        $url['DateFilter'] = Route::getOrderDateFilter($dateFilter);

        if ($statusFilter) {
            $url['StatusFilter'] = Route::getOrderStatusFilter($statusFilter);
        }

        if (!empty($callCenterFilter['callCenter'])) {
            $url['CallCenterFilter'] = $callCenterFilter;
        }
        if (!empty($deliveryFilter['delivery'])) {
            $url['DeliveryFilter'] = $deliveryFilter;
        }
        if (!empty($productFilter['product'])) {
            $url['ProductFilter'] = $productFilter;
        }
        if ($deliveryZipGroupFilter) {
            $url['DeliveryZipGroupFilter'] = $deliveryZipGroupFilter;
        }
        if ($checkingFilter) {
            $url['CheckingTypeFilter'] = $checkingFilter;
        }

        if ($slug = $this->getCountrySlug($model)) {
            $url['force_country_slug'] = $slug;
        }

        //Костыль чтобы исключить курьрки других стран иначе не проходит валидацию
        if ($deliveryFilter['delivery']) {
            $correctDeliveries = $this->filterDeliveriesByCountry($deliveryFilter['delivery'], $slug ?? Yii::$app->user->country->slug);
            $url['DeliveryFilter'] = ['delivery' => $correctDeliveries];
        }

        if ($textFilter) {
            $url['TextFilter'] = $textFilter;
        }

        if ($numberFilter) {
            $url['NumberFilter'] = $numberFilter;
        }

        if (!empty($sourceFilter['origin'])) {
            $url['OriginFilter'] = $sourceFilter;
        }

        if (isset($leadProductFilter)) {
            $url['LeadProductFilter'] = $leadProductFilter;
        }

        if ($this->grid->reportForm instanceof ReportFormCallCenter ||
            $this->grid->reportForm instanceof ReportFormFinance ||
            $this->grid->reportForm instanceof ReportFormStatus
        ) {
            $url['NoDoubleFilter']['list'][] = NoDoubleFilter::YES;
        }

        if ($this->extraLinkParams) {
            $url = array_merge($url, $this->extraLinkParams);
        }

        return Html::a(
            $text, $url, [
                'target' => '_blank',
            ]
        );
    }

    /**
     * @param $model
     *
     * @return integer
     */
    protected function getTotalCountFinance($model)
    {
        $result = 0;

        if ($this->attribute == Yii::t('common', 'В процессе')
            || $this->attribute == Yii::t('common', 'Не выкуплено')
            || $this->attribute == Yii::t('common', 'Выкуплено')
            || $this->attribute == Yii::t('common', 'Всего заказов')
            || $this->attribute == Yii::t('common', 'Всего')
            || $this->attribute == Yii::t('common', 'Претензия')
        ) {
            if (isset($model[Yii::t('common', 'Всего заказов')])) {
                $result = $model[Yii::t('common', 'Всего заказов')];
            } elseif (isset($model[Yii::t('common', 'Всего')])) {
                $result = $model[Yii::t('common', 'Всего')];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getReportFormParams()
    {
        $params = [];

        $formName = $this->grid->reportForm->formName();

        if (array_key_exists($formName, $this->grid->reportForm->searchQuery)) {
            $params = $this->grid->reportForm->searchQuery[$formName];
        }

        return $params;
    }


    /**
     * @return boolean
     */

    protected function isGroupByCustomerCity()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_CUSTOMER_CITY) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */

    protected function isGroupByCustomerProvince()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_CUSTOMER_PROVINCE) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */

    protected function isGroupByCustomerDistrict()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_CUSTOMER_DISTRIT) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */

    protected function isGroupByCustomerZip()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_CUSTOMER_ZIP) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDate()
    {
        $params = $this->getReportFormParams();

        $dates = [
            ReportForm::GROUP_BY_CREATED_AT,
            ReportForm::GROUP_BY_UPDATED_AT,
            ReportForm::GROUP_BY_CALL_CENTER_SENT_AT,
            ReportForm::GROUP_BY_CALL_CENTER_APPROVED_AT,
            ReportForm::GROUP_BY_DELIVERY_ACCEPTED_AT,
            ReportForm::GROUP_BY_DELIVERY_CREATED_AT,
            ReportForm::GROUP_BY_DELIVERY_PAID_AT,
            ReportForm::GROUP_BY_DELIVERY_RETURNED_AT,
            ReportForm::GROUP_BY_DELIVERY_ACCEPTED_AT,
            ReportForm::GROUP_BY_DELIVERY_APPROVED_AT,
            ReportForm::GROUP_BY_DELIVERY_SENT_AT,
            ReportForm::GROUP_BY_DELIVERY_FIRST_SENT_AT,
            ReportForm::GROUP_BY_DELIVERY_FIRST_SENT_AT,
            ReportForm::GROUP_BY_CHECK_SENT_AT,
            ReportForm::GROUP_BY_TS_SPAWN,
            ReportForm::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY,
        ];

        if (isset($params['groupBy']) && in_array($params['groupBy'], $dates)) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByProducts()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_PRODUCTS) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByLeadProducts()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_LEAD_PRODUCTS) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByCountryLeadProducts()
    {
        $params = $this->getReportFormParams();

        if (isset($params['groupBy']) && $params['groupBy'] == ReportForm::GROUP_BY_COUNTRY_LEAD_PRODUCTS) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByWebmaster()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_WEBMASTER_IDENTIFIER) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByLastForeignOperator()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_LAST_FOREIGN_OPERATOR) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByYearMonth()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_YEAR_MONTH) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByCountry()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByCountryDate()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_DATE) {
            return true;
        }

        return false;
    }

    protected function isGroupByCountryDeliveryDate()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByCountryProducts()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_PRODUCTS) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDeliveryZipcodes()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_DELIVERY_ZIPCODES) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDateCountryDelivery()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByProvinceCityDistrictZip()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDistrictZip()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_SUBDISTRICT_ZIP) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDelivery()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_DELIVERY) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isGroupByDateProducts()
    {
        if ($this->grid->reportForm->groupBy == ReportForm::GROUP_BY_DATE_PRODUCTS) {
            return true;
        }

        return false;
    }

    protected function canIdentifyCountry($model)
    {
        if ($this->grid->reportForm instanceof ReportFormWebmasterAnalytics && !empty($model['country_id'])) {
            return true;
        }

        if ($this->grid->reportForm->getCountryFilter()->all) {
            return false;
        }

        if ($this->grid->reportForm->getCountrySelectFilter()->hasApply || $this->grid->reportForm->getCountryDeliverySelectMultipleFilter()->hasApply || $this->grid->reportForm->getCountryDeliveryPartnerSelectMultipleFilter()->hasApply) {
            $params = $this->getReportFormParams();
            $count = count(ArrayHelper::getValue($params, 'country_ids', []));

            return $count == 1 || ($count != 1 && ($this->isGroupByCountryProducts() || $this->isGroupByCountry() || $this->isGroupByCountryDeliveryDate() || $this->isGroupByDateCountryDelivery() || $this->isGroupByDelivery()) && isset($model['country_id']) && !empty($model['country_id']));
        }

        return true;
    }

    protected function getCountrySlug($model)
    {
        $slug = null;

        if ($this->grid->reportForm->getCountrySelectFilter()->hasApply ||
            $this->grid->reportForm->getCountryDeliverySelectMultipleFilter()->hasApply ||
            $this->grid->reportForm->getCountryDeliveryPartnerSelectMultipleFilter()->hasApply ||
            $this->grid->reportForm instanceof ReportFormTeam ||
            $this->grid->reportForm instanceof ReportFormFinance ||
            $this->grid->reportForm instanceof ReportFormDelivery ||
            $this->grid->reportForm instanceof ReportFormCallCenterCheckRequest ||
            $this->grid->reportForm instanceof ReportFormWebmasterAnalytics
        ) {
            $countryId = ArrayHelper::getValue($model, 'country_id');
            if (!$countryId) {
                $params = $this->getReportFormParams();
                $count = count(ArrayHelper::getValue($params, 'country_ids', []));
                if ($count == 1) {
                    $countryId = $params['country_ids'][0];
                }
            }
            if ($countryId && $countryId != Yii::$app->user->country->id) {
                $country = self::$countries[$countryId] ?? null;
                if ($country) {
                    $slug = $country->slug;
                }
            }
        }

        return $slug;
    }

    /**
     * Фильтрует службы доставки по заданной стране
     *
     * @param $deliveries
     * @param $slug
     *
     * @return array
     */
    private function filterDeliveriesByCountry($deliveries, $slug)
    {
        $countryDeliveries = self::$countryDeliveries[$slug] ?? [];
        $countryDeliveries = ArrayHelper::getColumn($countryDeliveries, 'id');
        return array_values(array_intersect($countryDeliveries, is_array($deliveries) ? $deliveries : [$deliveries]));
    }
}
