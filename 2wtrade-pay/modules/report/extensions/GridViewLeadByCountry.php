<?php
namespace app\modules\report\extensions;

use Yii;

/**
 * Class GridViewProduct
 * @package app\modules\report\extensions
 */
class GridViewLeadByCountry extends GridView
{
    /**
     * @var ReportFormLeadByCountry
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                return date('d.m.Y', strtotime($model['yearcreatedat'] . '-' . $model['monthcreatedat'] . '-' . $model['daycreatedat'] . ' 00:00:00')) . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Лидов'),
            'attribute' => 'lidov',
        ];

        parent::initColumns();
    }
}
