<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormConsolidateSale;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewConsolidateSale
 * @package app\modules\report\extensions
 */
class GridViewConsolidateSale extends GridView
{
    /**
     * @var ReportFormConsolidateSale
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country',
            'group' => true,
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
            'width' => '110px'
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'date',
            'format' => ['date', 'php:d.m.Y'],
            'width' => '110px'
        ];

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'label' => Yii::t('common', 'Лиды'),
            'attribute' => "count_lead",
            'width' => '110px'
        ];

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        $lastColumn = null;
        foreach ($mapStatuses as $name => $statuses) {

            if ($name == Yii::t('common', 'Всего')) continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $column = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px'
            ];

            if ($name == Yii::t('common', 'Выкуплено')) {
                $lastColumn = [
                    'class' => DataColumn::className(),
                    'attribute' => $name,
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'width' => '110px',
                    'value' => function ($data) {
                        return $data[Yii::t('common', 'Выкуплено')] . " (" . round($data[Yii::t('common', 'vikup_alter_procent')], 2) . "%)";
                    }
                ];
            } else
                $this->columns[] = $column;
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Операторы'),
            'attribute' => Yii::t('common', 'Операторы'),
            'width' => '110px'
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Заказы / операторы'),
            'value' => function ($data) {
                $total = $data[Yii::t('common', 'Заказы')];
                $opers = $data[Yii::t('common', 'Операторы')];
                if (!$opers) return '-';
                return Yii::$app->formatter->asDecimal($total / $opers, 2);
            },
            'width' => '110px'
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек апрувленный'),
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Средний чек апрувленный')], 2);
            },
            'width' => '110px'
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек выкупной'),
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Средний чек выкупной')], 2);
            },
            'width' => '110px'
        ];

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'label' => Yii::t('common', 'Отгрузки'),
            'attribute' => Yii::t('common', 'Отгрузки'),
            'width' => '110px'
        ];

        if ($lastColumn)
            $this->columns[] = $lastColumn;

        parent::initColumns();
    }
}
