<?php

namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormCallCenter;
use app\modules\report\components\ReportFormShort;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class GridViewCallCenter
 * @package app\modules\report\extensions
 */
class GridViewCallCenter extends GridView
{
    /**
     * @var ReportFormShort
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (in_array($this->reportForm->groupBy, [ReportForm::GROUP_BY_COUNTRY_DATE, ReportForm::GROUP_BY_COUNTRY_LEAD_PRODUCTS])
            && $subTotals = $this->subTotalsCalculate()
        ) {
            $this->afterRow = function ($model, $key) use ($subTotals) {
                $row = null;
                if ($subTotal = ArrayHelper::getValue($subTotals, $key)) {
                    $options = $this->rowOptions; // save options
                    $this->rowOptions = ['class' => 'info kv-group-footer'];
                    $row = $this->renderTableRow($subTotal, null, null);
                    $this->rowOptions = $options; // load options
                }
                return $row;
            };
        }
    }

    /**
     * @param $value
     * @return DataColumn|null
     */
    private function findColumnByLabel($value)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->attribute != null) {
                if ($value == $column->attribute) {
                    return $this->columns[$key];
                }
            } else {
                if ($value == $column->label) {
                    return $this->columns[$key];
                }
            }
        }

        return null;
    }

    /**
     * @param $group
     * @param $name
     * @return mixed
     */
    private function subTotalSummary($group, $name)
    {
        $value = '';
        $column = $this->findColumnByLabel($name);
        if ($column && $column->pageSummary && !is_string($column->pageSummary)) {
            $operator = $column->pageSummaryFunc ?: GridView::F_SUM;
            $values = ArrayHelper::getColumn($group, $name);
            switch ($operator) {
                case GridView::F_SUM:
                    $value = array_sum($values);
                    break;
                case GridView::F_AVG:
                    $value = array_sum($values) / count($values);
                    break;
                case GridView::F_MAX:
                    $value = max($values);
                    break;
                case GridView::F_MIN:
                    $value = min($values);
                    break;
            }
        }

        return $value;
    }

    /**
     * @return array
     */
    private function subTotalsCalculate()
    {
        $subTotals = [];

        $models = $this->dataProvider->getModels();
        if ($models) {
            $groups = ArrayHelper::index($models, null, ['country_id']);
            if (count($groups) > 1) {
                $fields = array_keys(reset($models));
                foreach ($groups as $countryId => $group) {
                    foreach ($fields as $field) {
                        $subTotals[$countryId][$field] = $this->subTotalSummary($group, $field);
                    }
                }

                $map = [];
                foreach ($models as $rowKey => $model) {
                    $map[$model['country_id']] = $rowKey;
                }

                $tmp = [];
                foreach ($subTotals as $key => $subTotal) {
                    if ($rowKey = ArrayHelper::getValue($map, $key)) {
                        $tmp[$rowKey] = $subTotal;
                    }
                }
                $subTotals = $tmp;
            }
        }

        return $subTotals;
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        if ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_DATE) {

            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'date',
                'value' => function ($model) {
                    return $model['date'] ? Yii::$app->formatter->asDate($model['date']) : '';
                },
                'pageSummary' => false,
                'label' => Yii::t('common', 'Дата'),
            ];

        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_LEAD_PRODUCTS) {

            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'product_name',
                'pageSummary' => false,
                'label' => Yii::t('common', 'Товар'),
            ];

        } else {
            $this->columns[] = [
                'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
                'attribute' => Query::GROUP_BY_LABEL,
                'pageSummary' => Yii::t('common', 'Всего'),
                'width' => '110px',
            ];
        }

        $ids = [];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {
            $ids = array_merge($ids, $statuses);

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'label' => Yii::t('common', $name),
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px',
            ];
        }

        asort($ids);
        $innerStatuses = [];

        foreach ($ids as $status) {
            $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Всего'),
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего'),
            'columnLinkParams' => implode(',', $ids),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
            ],
            'width' => '110px',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Операторов'),
            'attribute' => Yii::t('common', 'Операторов'),
            'pageSummary' => '-',
            'format' => ['decimal', 0],
            'width' => '110px',
            'value' => function ($model) {
                return $model[Yii::t('common', 'Операторов')] ?? 0;
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек апрувленный'),
            'attribute' => Yii::t('common', 'Средний чек апрувленный'),
            'format' => ['decimal', 2],
            'width' => '110px',
            'value' => function ($model) {
                return round($model[Yii::t('common', 'Средний чек апрувленный')], 2);
            },
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }

                $models = $this->dataProvider->getModels();
                $sum = 0;
                $count = array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Подтверждено')));
                if (!$count) {
                    return 0;
                }
                foreach ($models as $model) {
                    $sum += ($model[Yii::t('common', 'Средний чек апрувленный')] * $model[Yii::t('common', 'Подтверждено')]);
                }
                return round($sum / $count, 2);
            },
        ];

        parent::initColumns();
    }
}
