<?php
namespace app\modules\report\extensions;

use yii\helpers\Inflector;

/**
 * Class Query
 * @package app\modules\report\extensions
 */
class Query extends \yii\db\Query
{
    /**
     * @var string Значение поля по которому будет проводится группировка
     */
    const GROUP_BY_NAME = 'groupbyname';

    /**
     * @var string Текст для отображения во вьюшке
     */
    const GROUP_BY_LABEL = 'groupbylabel';

    /**
     * @var array
     */
    private $labels;

    /**
     * @param string $attribute
     * @return string
     */
    public function getLabel($attribute)
    {
        if (isset($this->labels[$attribute])) {
            return $this->labels[$attribute];
        }

        return false;
    }

    /**
     * @param string $tableField
     * @param array $values
     * @param string $label
     * @return $this
     */
    public function addInConditionCount($tableField, $values, $label)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        $values = implode(',', $values);

        return $this->addCondition("COUNT({$tableField} IN({$values}) OR NULL)", $label);
    }

    /**
     * @param string $tableField
     * @param array $values
     * @param string $label
     * @return $this
     */
    public function addNotInConditionCount($tableField, $values, $label)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        $values = implode(',', $values);

        return $this->addCondition("COUNT({$tableField} NOT IN({$values}) OR NULL)", $label);
    }

    /**
     * @param string $tableField
     * @param string $label
     * @return $this
     */
    public function addSumCondition($tableField, $label)
    {
        return $this->addCondition("SUM($tableField)", $label);
    }

    /**
     * @param string $tableField
     * @param string $label
     * @return $this
     */
    public function addCountDistinctCondition($tableField, $label)
    {
        return $this->addCondition("COUNT(DISTINCT $tableField)", $label);
    }

    /**
     * @param string $tableField
     * @param string $label
     * @param boolean $round
     * @return $this
     */
    public function addAvgCondition($tableField, $label, $round = false)
    {
        if ($round) {
            return $this->addCondition("ROUND(AVG($tableField))", $label);
        } else {
            return $this->addCondition("AVG($tableField)", $label);
        }
    }

    /**
     * @param string $tableField
     * @param string $label
     * @param boolean $round
     * @return $this
     */
    public function addMinCondition($tableField, $label, $round = false)
    {
        if ($round) {
            return $this->addCondition("ROUND(MIN($tableField))", $label);
        } else {
            return $this->addCondition("MIN($tableField)", $label);
        }
    }

    /**
     * @param string $tableField
     * @param string $label
     * @param boolean $round
     * @return $this
     */
    public function addMaxCondition($tableField, $label, $round = false)
    {
        if ($round) {
            return $this->addCondition("ROUND(MAX($tableField))", $label);
        } else {
            return $this->addCondition("MAX($tableField)", $label);
        }
    }

    /**
     * @param string $sumField
     * @param string $conditionField
     * @param array|integer $values
     * @return string
     */
    public function buildCaseCondition($sumField, $conditionField, $values)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        $values = implode(',', $values);

        return "CASE WHEN {$conditionField} IN ({$values}) THEN {$sumField} ELSE NULL END";
    }

    /**
     * @param string $conditionField
     * @param array|integer $values
     * @param string $resultTrue
     * @param string $resultFalse
     * @return string
     */
    public function buildIfCondition($conditionField, $values, $resultTrue, $resultFalse)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        $values = implode(',', $values);

        return "IF ({$conditionField} IN ({$values}), {$resultTrue}, {$resultFalse})";
    }

    /**
     * @param string $select
     * @param string $label
     * @return $this
     */
    public function addCondition($select, $label)
    {
        $slug = Inflector::slug($label, '_');
        $this->labels[$slug] = $label;

        return $this->addSelect(["{$select} AS `{$slug}`"]);
    }
}
