<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormTeam;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewTeam
 * @package app\modules\report\extensions
 */
class GridViewTeam extends GridView
{
    /**
     * @var ReportFormTeam
     */

    /**
     * апрув от 40% ( уточнить по норме страны )
     */
    const GOOD_APPROVE = 40;

    /**
     * чек - от 65$ ( исключая стоимость доставки )
     */
    const GOOD_CHECK_USD = 65;

    /**
     * количество общих звонков за 8 часов - выше 100
     */
    const GOOD_CALLS_NUMBER = 100;

    /**
     * количество результативных - больше 30 (за те же 8 часов )
     */
    const GOOD_APPROVE_CALLS_NUMBER = 30;

    /**
     * выкуп - выше и включая 49% ( по циклу 15 дней)
     */
    const GOOD_BUYOUT = 49;

    /**
     * @var ReportFormTeam
     */
    public $reportForm;


    public $detailMode = false;

    public $showGeneral = true;

    public $showProducts = true;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        if (!$this->detailMode) {

            $this->rowOptions = function ($model) {
                return [
                    'class' => 'tr-vertical-align-middle ' . (!empty($model['active']) ? '' : 'text-muted'),
                ];
            };

            $this->columns[] = [
                'class' => 'kartik\grid\SerialColumn',
                'pageSummary' => Yii::t('common', 'Итого'),
            ];

            $this->columns[] = [
                'attribute' => 'parent_id',
                'label' => Yii::t('common', 'Руководитель'),
                'pageSummary' => false,
                'pageSummaryOptions' => ['hidden' => true],
                'hiddenFromExport' => true,
                'group' => true,  // enable grouping,
                'groupedRow' => true,                    // move grouped column to a single grouped row
                'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                'content' => function ($model) {
                    return Html::tag('b', $model['parent_name'] . ' (TL' . $model['team_lead_code'] . ')', [
                        'title' => $model['parent_name']
                    ]);
                },
                'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                    if (!$this->reportForm->person) {
                        $col1 = $col2 = $col3 = 2;
                        if ($this->showGeneral) {
                            return [
                                'mergeColumns' => [[2, $col1]], // columns to merge in summary
                                'content' => [             // content to show in each summary cell
                                    2 => Yii::t('common', 'Итого по региону') . ' TL' . $model['team_lead_code'],
                                    ++$col1 => GridView::F_SUM,
                                    ++$col1 => GridView::F_SUM,
                                    ++$col1 => GridView::F_SUM,
                                    ++$col1 => GridView::F_SUM,
                                    ++$col1 => $this->reportForm['teamLeads'][$model['team_lead_code']]['srednij_cek'] ? Yii::$app->formatter->asDecimal($this->reportForm['teamLeads'][$model['team_lead_code']]['srednij_cek'], 2) : 0,
                                    ++$col1 => $this->reportForm['teamLeads'][$model['team_lead_code']]['srednij_vykupnoj_cek'] ? Yii::$app->formatter->asDecimal($this->reportForm['teamLeads'][$model['team_lead_code']]['srednij_vykupnoj_cek'], 2) : 0,
                                    ++$col1 => $this->reportForm['teamLeads'][$model['team_lead_code']]['avg_calls'] ? Yii::$app->formatter->asDecimal($this->reportForm['teamLeads'][$model['team_lead_code']]['avg_calls'], 2) : 0,
                                    ++$col1 => $this->reportForm['teamLeads'][$model['team_lead_code']]['avg_approve_calls'] ? Yii::$app->formatter->asDecimal($this->reportForm['teamLeads'][$model['team_lead_code']]['avg_approve_calls'], 2) : 0,
                                    ++$col1 => $this->reportForm['teamLeads'][$model['team_lead_code']]['vrema_otveta'] ? Yii::$app->formatter->asDecimal($this->reportForm['teamLeads'][$model['team_lead_code']]['vrema_otveta'], 2) : 0,
                                ],
                                'contentFormats' => [      // content reformatting for each summary cell
                                    ++$col2 => ['format' => 'number'],
                                    ++$col2 => ['format' => 'number'],
                                    ++$col2 => ['format' => 'number'],
                                    ++$col2 => ['format' => 'number'],
                                    ++$col2 => '',
                                    ++$col2 => '',
                                    ++$col2 => '',
                                    ++$col2 => '',
                                    ++$col2 => '',
                                ],
                                'contentOptions' => [      // content html attributes for each summary cell
                                    2 => ['style' => 'font-variant:small-caps'],
                                    ++$col3 => ['style' => 'text-align:left'],
                                    ++$col3 => ['style' => 'text-align:left'],
                                    ++$col3 => ['style' => 'text-align:left'],
                                    ++$col3 => ['style' => 'text-align:left'],
                                    ++$col3 => ['style' => 'text-align:right'],
                                    ++$col3 => ['style' => 'text-align:right'],
                                    ++$col3 => ['style' => 'text-align:right'],
                                    ++$col3 => ['style' => 'text-align:right'],
                                    ++$col3 => ['style' => 'text-align:right'],
                                ],
                                // html attributes for group summary row
                                'options' => ['class' => 'info', 'style' => 'font-style:italic;']
                            ];
                        } else {
                            $c = 2;
                            $content = [
                                $c => Yii::t('common', 'Итого по региону') . ' TL' . $model['team_lead_code']
                            ];
                            $contentFormats = [];
                            $contentOptions = [
                                $c => ['style' => 'font-variant:small-caps']
                            ];
                            foreach ($this->reportForm->products as $productId => $productName) {
                                $c++;
                                $content[$c] = GridView::F_SUM;
                                $contentFormats[$c] = ['format' => 'number'];
                                $contentOptions[$c] = ['style' => 'text-align:left'];
                            }

                            return [
                                'mergeColumns' => [[2, $col1]], // columns to merge in summary
                                'content' => $content,
                                'contentFormats' => $contentFormats,
                                'contentOptions' => $contentOptions,
                                // html attributes for group summary row
                                'options' => ['class' => 'info', 'style' => 'font-style:italic;']
                            ];
                        }
                    }
                    return false;
                }
            ];
        }

        if (!$this->detailMode) {
            $this->columns[] = [
                'attribute' => 'user_logins',
                'label' => Yii::t('common', 'Логин пользователя в КЦ'),
                'pageSummary' => '',
                'content' => function ($model) {

                    $login = array_unique(explode(',', $model['call_center_user_logins'] ?? ''));
                    $login = implode(', ' , $login);

                    if ($this->reportForm->person) {
                        return Html::tag('span', str_replace(',', ', ', isset($login) ? $model['fio'] . ' (' . $login. ')' : '-'), [
                            'target' => '_blank',
                            'title' => $model['fio']
                        ]);
                    }
                    $route = [
                        '/report/team/index',
                        's' => [
                            'from' => $this->reportForm->getDateFilter()->from,
                            'to' => $this->reportForm->getDateFilter()->to,
                            'type' => $this->reportForm->getDateFilter()->type,
                            'office' => $this->reportForm->getOfficeFilter()->office,
                            'callCenter' => $this->reportForm->getCallCenterFilter()->callCenter,
                            'teamLead' => $this->reportForm->getTeamLeadFilter()->teamLead,
                        ],
                        'person' => $model['id'] ?? 0
                    ];
                    return Html::a(str_replace(',', ', ', isset($login) ? $model['fio'] . ' (' . $login . ')' : '-'), Url::toRoute($route), [
                        'target' => '_blank',
                        'title' => $model['fio']
                    ]);
                },
            ];
        }

        if ($this->detailMode) {
            $this->columns[] = [
                'attribute' => 'groupbylabel',
                'label' => Yii::t('common', 'Товар'),
                'pageSummary' => '',
                'content' => function ($model) {
                    return Yii::t('common', $model['groupbylabel'] ?? '-');
                },
            ];
        }

        if ($this->showGeneral) {
            $mapStatuses = $this->reportForm->getMapStatuses();
            $orderStatuses = OrderStatus::find()->collection();

            foreach ($mapStatuses as $name => $statuses) {

                $innerStatuses = [];

                foreach ($statuses as $status) {
                    $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
                }

                // без этого не подставлялось значение в ячейку. в MapStatuses пришлось поместить названия на английском, так они идут в $model
                $label = '';
                if ($name == 'approve') $label = 'Подтверждено';
                if ($name == 'buyout') $label = 'Выкуплено';
                if ($name == 'process') $label = 'В процессе';
                if ($name == 'notbuyout') $label = 'Не выкуплено';
                if ($name == 'total') continue;

                $this->columns[] = [
                    'class' => DataColumnCount::className(),
                    'makeLinkToOrders' => false,
                    'attribute' => $name,
                    'label' => Yii::t('common', $label),
                    'columnLinkParams' => implode(',', $statuses),
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'contentOptions' => function ($model) use ($name) {
                        $namePercent = $name . '_percent';
                        if ($name == 'approve') {
                            return (isset($model[$namePercent]) && $model[$namePercent] > 0 && $model[$namePercent] < self::GOOD_APPROVE) ? ['class' => 'text-danger'] : '';
                        }
                        if ($name == 'buyout') {
                            return (isset($model[$namePercent]) && $model[$namePercent] > 0 && $model[$namePercent] < self::GOOD_BUYOUT) ? ['class' => 'text-danger'] : '';
                        }
                        return '';
                    },
                    'width' => '100px',
                    'value' => function ($model) use ($name) {
                        return $model[$name] ?? 0;
                    },
                    'enableSorting' => true
                ];
            }

            $this->columns[] = [
                'attribute' => 'srednij_cek',
                'label' => Yii::t('common', 'Ср. чек апрувленный'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['srednij_cek'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['srednij_cek']) && $model['srednij_cek'] > 0 && $model['srednij_cek'] < self::GOOD_CHECK_USD) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['srednij_cek'] ?? 0;
                },
            ];

            $this->columns[] = [
                'attribute' => 'srednij_vykupnoj_cek',
                'label' => Yii::t('common', 'Ср. чек выкупной'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['srednij_vykupnoj_cek'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['srednij_vykupnoj_cek']) && $model['srednij_vykupnoj_cek'] > 0 && $model['srednij_vykupnoj_cek'] < self::GOOD_CHECK_USD) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['srednij_vykupnoj_cek'] ?? 0;
                },
            ];
        }

        if ($this->showGeneral && !$this->reportForm->person) {
            $this->columns[] = [
                'attribute' => 'avg_calls',
                'label' => Yii::t('common', 'Ср. кол-во звонков в день'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['avg_calls'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['avg_calls']) && $model['avg_calls'] > 0 && $model['avg_calls'] < self::GOOD_CALLS_NUMBER) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['avg_calls'] ?? 0;
                },
                'visible' => true
            ];

            $this->columns[] = [
                'attribute' => 'avg_approve_calls',
                'label' => Yii::t('common', 'Кол-во результат. звонков'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['avg_approve_calls'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['avg_approve_calls']) && $model['avg_approve_calls'] > 0 && $model['avg_approve_calls'] < self::GOOD_APPROVE_CALLS_NUMBER) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['avg_approve_calls'] ?? 0;
                },
                'visible' => true
            ];

            $this->columns[] = [
                'attribute' => 'vrema_otveta',
                'label' => Yii::t('common', 'Ср. время ответа клиенту, час'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['vrema_otveta'], 2);
                },
                'value' => function ($model) {
                    return $model['vrema_otveta'] ?? 0;
                },
                'visible' => true
            ];
        }

        if (!$this->detailMode && $this->showProducts) {
            foreach ($this->reportForm->products as $productId => $productName) {
                $this->columns[] = [
                    'attribute' => 'product_buyout' . $productId,
                    'label' => Yii::t('common', $productName),
                    'pageSummaryFunc' => GridView::F_SUM,
                    'width' => '100px',
                    'content' => function ($model) use ($productId) {
                        $product_buyout = $model['product_buyout' . $productId] ?? 0;
                        $product_buyout_percent = $model['product_buyout' . $productId . '_percent'] ?? 0;
                        $percent = number_format($product_buyout_percent, 2, Yii::$app->get('formatter')->decimalSeparator, Yii::$app->get('formatter')->thousandSeparator);
                        return $product_buyout . '&nbsp;(' . $percent . '%)';
                    },
                ];
            }
        }

        if ($this->detailMode) {
            $this->columns[] = [
                'label' => '',
                'pageSummary' => '',
                'width' => '220px',
                'content' => function () {
                    return '';
                },
                'visible' => false
            ];
        }

        parent::initColumns();
    }
}
