<?php

namespace app\modules\report\extensions;

use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormFinance;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class GridViewFinance
 * @package app\modules\report\extensions
 */
class GridViewFinance extends GridView
{
    /**
     * @var ReportFormFinance
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (in_array($this->reportForm->groupBy,
                [ReportForm::GROUP_BY_COUNTRY_DATE, ReportForm::GROUP_BY_COUNTRY_PRODUCTS])
            && $subTotals = $this->subTotalsCalculate()
        ) {
            $this->afterRow = function ($model, $key) use ($subTotals) {
                $row = null;
                if ($subTotal = ArrayHelper::getValue($subTotals, $key)) {
                    $options = $this->rowOptions; // save options
                    $this->rowOptions = ['class' => 'info kv-group-footer'];
                    $row = $this->renderTableRow($subTotal, null, null);
                    $this->rowOptions = $options; // load options
                }
                return $row;
            };
        }
    }

    /**
     * @param $value
     * @return DataColumn|null
     */
    private function findColumnByLabel($value)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->attribute != null) {
                if ($value == $column->attribute) {
                    return $this->columns[$key];
                }
            } else {
                if ($value == $column->label) {
                    return $this->columns[$key];
                }
            }
        }

        return null;
    }

    /**
     * @param $group
     * @param $name
     * @return mixed
     */
    private function subTotalSummary($group, $name)
    {
        $value = '';
        $column = $this->findColumnByLabel($name);
        if ($column && $column->pageSummary) {
            $operator = $column->pageSummaryFunc ?: GridView::F_SUM;
            $values = ArrayHelper::getColumn($group, $name);
            switch ($operator) {
                case GridView::F_SUM:
                    $value = array_sum($values);
                    break;
                case GridView::F_AVG:
                    $value = array_sum($values) / count($values);
                    break;
                case GridView::F_MAX:
                    $value = max($values);
                    break;
                case GridView::F_MIN:
                    $value = min($values);
                    break;
            }
        }

        return $value;
    }

    /**
     * @return array
     */
    private function subTotalsCalculate()
    {
        $subTotals = [];

        $models = $this->dataProvider->getModels();
        if ($models) {
            $groups = ArrayHelper::index($models, null, ['country_id']);
            if (count($groups) > 1) {
                $fields = array_keys(reset($models));
                foreach ($groups as $countryId => $group) {
                    foreach ($fields as $field) {
                        $subTotals[$countryId][$field] = $this->subTotalSummary($group, $field);
                    }
                }

                $map = [];
                foreach ($models as $rowKey => $model) {
                    $map[$model['country_id']] = $rowKey;
                }

                $tmp = [];
                foreach ($subTotals as $key => $subTotal) {
                    if ($rowKey = ArrayHelper::getValue($map, $key)) {
                        $tmp[$rowKey] = $subTotal;
                    }
                }
                $subTotals = $tmp;
            }
        }

        return $subTotals;
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        if ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_PRODUCTS) {

            $this->columns[] = [
                'label' => Yii::t('common', 'Страна'),
                'attribute' => 'country_name',
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'product_name',
                'pageSummary' => false,
                'label' => Yii::t('common', 'Товар'),
            ];

        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_DATE) {

            $this->columns[] = [
                'label' => Yii::t('common', 'Страна'),
                'attribute' => 'country_name',
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'date',
                'pageSummary' => false,
                'label' => Yii::t('common', 'Дата'),
            ];

        } else {

            $this->columns[] = [
                'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
                'attribute' => Query::GROUP_BY_LABEL,
                'pageSummary' => Yii::t('common', 'Всего'),
                'content' => function ($data) {
                    if ($this->reportForm->groupByCollection[$this->reportForm->groupBy] == Yii::t('common',
                            'Страна')
                    ) {
                        $getData = $this->reportForm->searchQuery;
                        $getData[$this->reportForm->formName()]['country_ids'] = $data['groupbyname'];
                        $getData[$this->reportForm->formName()]['groupBy'] = $getData[$this->reportForm->formName()]['type'];

                        return Html::a($data['groupbylabel'], [
                            '/report/finance/index',
                            $this->reportForm->formName() => $getData[$this->reportForm->formName()],
                        ],
                            [
                                'target' => '_blank',
                                'title' => Yii::t('common', 'Детальная информация'),
                            ]);
                    }
                    return $data['groupbylabel'];
                },
            ];
        }

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {

            if ($name == Yii::t('common', 'Всего заказов')) {
                $this->columns[] = [
                    'attribute' => Yii::t('common', 'Претензия'),
                    'class' => DataColumnCount::className(),
                    'value' => function ($data) {
                        return $data[Yii::t('common', 'Претензия')];
                    },
                    'extraLinkParams' => [
                        'PretensionFilter' => [
                            'pretension' => array_keys(OrderFinancePretension::getTypeCollecion())
                        ]
                    ],
                ];
            }

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек апрувленный'),
            'value' => function ($data) {
                if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
                    return 0;
                }
                if ($this->reportForm->dollar) {
                    return $data[Yii::t('common', 'Средний чек апрувленный')] / ReportFormFinance::$currencies[$data['country_id']];
                } else {
                    return $data[Yii::t('common', 'Средний чек апрувленный')];
                }
            },
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }
                $models = $this->dataProvider->getModels();
                $sum = 0;
                $count = array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Выкуплено'))) + array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Не выкуплено'))) + array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'В процессе')));
                if (!$count) {
                    return 0;
                }
                foreach ($models as $model) {
                    if (!isset(ReportFormFinance::$currencies[$model['country_id']]) || ReportFormFinance::$currencies[$model['country_id']] == 0) {
                        return 0;
                    }
                    $sum += ($model[Yii::t('common', 'Средний чек апрувленный')] * ($model[Yii::t('common', 'Выкуплено')] + $model[Yii::t('common', 'Не выкуплено')] + $model[Yii::t('common', 'В процессе')])) / ReportFormFinance::$currencies[$model['country_id']];
                }
                return $sum / $count;
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек выкупной'),
            'value' => function ($data) {
                if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
                    return 0;
                }
                if ($this->reportForm->dollar) {
                    return $data[Yii::t('common', 'Средний чек выкупной')] / ReportFormFinance::$currencies[$data['country_id']];
                } else {
                    return $data[Yii::t('common', 'Средний чек выкупной')];
                }
            },
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }
                $models = $this->dataProvider->getModels();
                $sum = 0;
                $count = array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Выкуплено')));
                if (!$count) {
                    return 0;
                }
                foreach ($models as $model) {
                    if (!isset(ReportFormFinance::$currencies[$model['country_id']]) || ReportFormFinance::$currencies[$model['country_id']] == 0) {
                        return 0;
                    }
                    $sum += ($model[Yii::t('common', 'Средний чек выкупной')] * $model[Yii::t('common', 'Выкуплено')]) / ReportFormFinance::$currencies[$model['country_id']];
                }
                return $sum / $count;
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
        ];

        /*$this->columns[] = [
            'label' => Yii::t('common', 'Ср. доход по выкупам'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Ср. доход по выкупам')], 2);
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
        ];*/

        $this->columns[] = [
            'label' => Yii::t('common', 'Ср. цена лида'),
            'value' => function ($data) {
                if (!isset($data[Yii::t('common', 'Всего заказов')]) || $data[Yii::t('common', 'Всего заказов')] == 0) {
                    return 0;
                }
                if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
                    return 0;
                }
                return ($data[Yii::t('common', 'Всего заказов')] ? ($data[Yii::t('common', 'Цена за лиды')] / $data[Yii::t('common', 'Всего заказов')]) : 0) * ($this->reportForm->dollar ? 1 : ReportFormFinance::$currencies[$data['country_id']]);
            },
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }
                $models = $this->dataProvider->getModels();
                $totalCount = array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Всего заказов')));

                return $totalCount ? (array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Цена за лиды'))) / $totalCount) : 0;
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
            'visible' => Yii::$app->user->can('view_finance_director'),
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Доход по выкупам'),
            'value' => function ($data) {
                if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
                    return 0;
                }
                if ($this->reportForm->dollar) {
                    return $data[Yii::t('common', 'Доход по выкупам')] / ReportFormFinance::$currencies[$data['country_id']];
                } else {
                    return $data[Yii::t('common', 'Доход по выкупам')];
                }
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
            'visible' => Yii::$app->user->can('view_finance_director'),
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Цена за лиды'),
            'value' => function ($data) {
                if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
                    return 0;
                }
                if (!$this->reportForm->dollar) {
                    return $data[Yii::t('common', 'Цена за лиды')] * ReportFormFinance::$currencies[$data['country_id']];
                } else {
                    return $data[Yii::t('common', 'Цена за лиды')];
                }
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
            'visible' => Yii::$app->user->can('view_finance_director'),
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы'),
            'value' => function () {
                return 0;
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
            'visible' => Yii::$app->user->can('view_finance_director'),
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Прибыль'),
            'value' => function ($data) {
                return $this->getValueIncome($data);
            },
            'content' => function ($data) {
                $value = $this->getValueIncome($data);
                $value = Yii::$app->formatter->asDecimal($value, 2);

                if ($value < 0) {
                    $value = Html::tag('span', $value, ['class' => 'text-danger']);
                }

                return $value;
            },
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
            'visible' => Yii::$app->user->can('view_finance_director'),
        ];

        parent::initColumns();
    }

    /**
     * @param $data
     * @return integer
     */
    protected function getValueIncome($data)
    {
        if (!isset(ReportFormFinance::$currencies[$data['country_id']]) || ReportFormFinance::$currencies[$data['country_id']] == 0) {
            $buyout = $leads = 0;
        } else {
            if ($this->reportForm->dollar) {
                $buyout = $data[Yii::t('common', 'Доход по выкупам')] / ReportFormFinance::$currencies[$data['country_id']];
                $leads = $data[Yii::t('common', 'Цена за лиды')];
            } else {
                $buyout = $data[Yii::t('common', 'Доход по выкупам')];
                $leads = $data[Yii::t('common', 'Цена за лиды')] * ReportFormFinance::$currencies[$data['country_id']];
            }
        }

        $consumptions = 0;

        return $buyout - $leads - $consumptions;
    }
}
