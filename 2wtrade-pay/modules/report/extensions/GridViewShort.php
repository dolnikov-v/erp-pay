<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormShort;
use Yii;

/**
 * Class GridViewShort
 * @package app\modules\report\extensions
 */
class GridViewShort extends GridView
{
    /**
     * @var ReportFormShort
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
        ];

        $ids = [];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {
            $ids = array_merge($ids, $statuses);

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px',
            ];
        }

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего'),
            'columnLinkParams' => implode(',', $ids),
            'width' => '110px',
        ];

        parent::initColumns();
    }
}
