<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewTeamTotal
 * @package app\modules\report\extensions
 */
class GridViewTeamTotal extends GridViewTeam
{
    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'class' => 'kartik\grid\SerialColumn',
            'pageSummary' => Yii::t('common', 'Итого'),
        ];

        $this->columns[] = [
            'attribute' => 'groupbylabel',
            'label' => Yii::t('common', 'Регион'),
            'pageSummary' => '',
            'content' => function ($model) {
                return Html::tag('span', 'TL' . $model['code'] . ' - ' . Yii::t('common', 'операторов: ') . $model['operators'], [
                    'title' => $model['name']
                ]);
            },
        ];

        if ($this->showGeneral) {
            $mapStatuses = $this->reportForm->getMapStatuses();
            $orderStatuses = OrderStatus::find()->collection();

            foreach ($mapStatuses as $name => $statuses) {

                $innerStatuses = [];

                foreach ($statuses as $status) {
                    $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
                }

                // без этого не подставлялось значение в ячейку. в MapStatuses пришлось поместить названия на английском, так они идут в $model
                $label = '';
                if ($name == 'approve') $label = 'Подтверждено';
                if ($name == 'buyout') $label = 'Выкуплено';
                if ($name == 'process') $label = 'В процессе';
                if ($name == 'notbuyout') $label = 'Не выкуплено';
                if ($name == 'total') continue;

                $this->columns[] = [
                    'class' => DataColumnCount::className(),
                    'makeLinkToOrders' => false,
                    'attribute' => $name,
                    'label' => Yii::t('common', $label),
                    'columnLinkParams' => implode(',', $statuses),
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'contentOptions' => function ($model) use ($name) {
                        $namePercent = $name . '_percent';
                        if ($name == 'approve') {
                            return (isset($model[$namePercent]) && $model[$namePercent] > 0 && $model[$namePercent] < self::GOOD_APPROVE) ? ['class' => 'text-danger'] : '';
                        }
                        if ($name == 'buyout') {
                            return (isset($model[$namePercent]) && $model[$namePercent] > 0 && $model[$namePercent] < self::GOOD_BUYOUT) ? ['class' => 'text-danger'] : '';
                        }
                        return '';
                    },
                    'width' => '100px',
                    'value' => function ($model) use ($name) {
                        return $model[$name] ?? 0;
                    },
                    'enableSorting' => true
                ];
            }

            $this->columns[] = [
                'attribute' => 'srednij_cek',
                'label' => Yii::t('common', 'Ср. чек апрувленный'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['srednij_cek'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['srednij_cek']) && $model['srednij_cek'] > 0 && $model['srednij_cek'] < self::GOOD_CHECK_USD) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['srednij_cek'] ?? 0;
                },
            ];

            $this->columns[] = [
                'attribute' => 'srednij_vykupnoj_cek',
                'label' => Yii::t('common', 'Ср. чек выкупной'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['srednij_vykupnoj_cek'], 2);
                },
                'contentOptions' => function ($model) {
                    return (isset($model['srednij_vykupnoj_cek']) && $model['srednij_vykupnoj_cek'] > 0 && $model['srednij_vykupnoj_cek'] < self::GOOD_CHECK_USD) ? ['class' => 'text-danger'] : '';
                },
                'value' => function ($model) {
                    return $model['srednij_vykupnoj_cek'] ?? 0;
                },
            ];

            if (!$this->reportForm->person) {
                $this->columns[] = [
                    'attribute' => 'avg_calls',
                    'label' => Yii::t('common', 'Ср. кол-во звонков в день'),
                    'format' => ['decimal', 2],
                    'hAlign' => 'right',
                    'width' => '100px',
                    'pageSummary' => function () {
                        return Yii::$app->formatter->asDecimal($this->reportForm->totals['avg_calls'], 2);
                    },
                    'contentOptions' => function ($model) {
                        return (isset($model['avg_calls']) && $model['avg_calls'] > 0 && $model['avg_calls'] < self::GOOD_CALLS_NUMBER) ? ['class' => 'text-danger'] : '';
                    },
                    'value' => function ($model) {
                        return $model['avg_calls'] ?? 0;
                    },
                ];

                $this->columns[] = [
                    'attribute' => 'avg_approve_calls',
                    'label' => Yii::t('common', 'Кол-во результат. звонков'),
                    'format' => ['decimal', 2],
                    'hAlign' => 'right',
                    'width' => '100px',
                    'pageSummary' => function () {
                        return Yii::$app->formatter->asDecimal($this->reportForm->totals['avg_approve_calls'], 2);
                    },
                    'contentOptions' => function ($model) {
                        return (isset($model['avg_approve_calls']) && $model['avg_approve_calls'] > 0 && $model['avg_approve_calls'] < self::GOOD_APPROVE_CALLS_NUMBER) ? ['class' => 'text-danger'] : '';
                    },
                    'value' => function ($model) {
                        return $model['avg_approve_calls'] ?? 0;
                    },
                ];
            }

            $this->columns[] = [
                'attribute' => 'vrema_otveta',
                'label' => Yii::t('common', 'Ср. время ответа клиенту, час'),
                'format' => ['decimal', 2],
                'hAlign' => 'right',
                'width' => '100px',
                'pageSummary' => function () {
                    return Yii::$app->formatter->asDecimal($this->reportForm->totals['vrema_otveta'], 2);
                },
                'value' => function ($model) {
                    return $model['vrema_otveta'] ?? 0;
                },
                'visible' => true
            ];
        }

        if ($this->showProducts) {
            foreach ($this->reportForm->products as $productId => $productName) {
                $this->columns[] = [
                    'attribute' => 'product_buyout' . $productId,
                    'label' => Yii::t('common', $productName),
                    'pageSummaryFunc' => GridView::F_SUM,
                    'width' => '100px',
                    'content' => function ($model) use ($productId) {
                        $product_buyout = $model['product_buyout' . $productId] ?? 0;
                        $product_buyout_percent = $model['product_buyout' . $productId . '_percent'] ?? 0;
                        $percent = number_format($product_buyout_percent, 2, Yii::$app->get('formatter')->decimalSeparator, Yii::$app->get('formatter')->thousandSeparator);
                        return $product_buyout . '&nbsp;(' . $percent . '%)';
                    },
                ];
            }
        }

        GridView::initColumns();
    }
}
