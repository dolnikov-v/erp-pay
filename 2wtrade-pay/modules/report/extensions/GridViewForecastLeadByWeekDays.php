<?php
namespace app\modules\report\extensions;

use Yii;

/**
 * Class GridViewForecastLeadByWeekDays
 * @package app\modules\report\extensions
 */
class GridViewForecastLeadByWeekDays extends GridView
{
    /**
     * @var
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $this->columns[] = [
            'label' => Yii::t('common', 'День недели'),
            'attribute' => 'weekday',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Прогноз количества лидов на 7 дней'),
            'attribute' => 'leadcount',
        ];

        parent::initColumns();
    }
}
