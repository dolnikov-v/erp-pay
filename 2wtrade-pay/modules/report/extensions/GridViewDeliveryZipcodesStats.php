<?php
namespace app\modules\report\extensions;

use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormDeliveryZipcodesStats;
use Yii;

/**
 * Class GridViewDeliveryZipcodesStats
 * @package app\modules\report\extensions
 */
class GridViewDeliveryZipcodesStats extends GridView
{
    /**
     * @var ReportFormDeliveryZipcodesStats
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => Yii::t('common', 'Группа индексов доставки'),
            'attribute' => DeliveryZipcodes::tableName() . '.id',
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
            'value' => function ($model) {
                if (!empty($model['delivery_zipcodes_name'])) {
                    return Yii::t('common', $model['delivery_zipcodes_name']);
                } else {
                    return Yii::t('common', 'Старое распределение');
                }
            }
        ];

        $ids = [];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {
            $ids = array_merge($ids, $statuses);

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px',
            ];
        }

        asort($ids);
        $innerStatuses = [];

        foreach ($ids as $status) {
            $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
        }

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего'),
            'columnLinkParams' => implode(',', $ids),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
            ],
            'width' => '110px',
        ];

        parent::initColumns();
    }
}
