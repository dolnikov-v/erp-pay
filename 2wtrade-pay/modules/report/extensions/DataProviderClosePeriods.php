<?php

namespace app\modules\report\extensions;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;

class DataProviderClosePeriods extends DataProvider
{

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected static function applyQueryClosureDate($attribute, $query)
    {
        $query
            ->addSelect(['date' => 'DATE_FORMAT(FROM_UNIXTIME(' . $attribute . '), "%Y-%m")'])
            ->andWhere(['>=', $attribute, 0])
            ->leftJoin(Delivery::tableName(), DeliveryRequest::tableName() . '.delivery_id = ' . Delivery::tableName() . '.id')
            ->groupBy(['date', 'delivery_id'])
            ->orderBy(['date' => SORT_DESC]);

    }
}