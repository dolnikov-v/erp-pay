<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormWebmaster;
use Yii;

/**
 * Class GridViewWebmaster
 * @package app\modules\report\extensions
 */
class GridViewWebmaster extends GridView
{
    /**
     * @var ReportFormWebmaster
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'attribute' => 'percent_group',
            'label' => Yii::t('common', 'Выкуп'),
            'pageSummary' => false,
            'group' => true
        ];
        $this->columns[] = [
            'attribute' => 'id',
            'label' => Yii::t('common', 'ID вебмастера'),
            'pageSummary' => Yii::t('common', 'Всего')
        ];
        $this->columns[] = [
            'attribute' => 'count_lead',
            'class' => DataColumnCount::className(),
            'label' => Yii::t('common', 'Количество лидов'),
            'width' => '150px',
            'hidePercent' => true
        ];
        $this->columns[] = [
            'attribute' => 'try_call_count',
            'label' => Yii::t('common', 'Среднее количество звонков на лид'),
            'width' => '150px',
            'format' => ['decimal', 2],
        ];
        $this->columns[] = [
            'attribute' => 'first_call',
            'label' => Yii::t('common', 'Среднее время до первого звонка, мин'),
            'width' => '150px',
            'format' => ['decimal', 2],
        ];

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {

            if ($name == 'total') continue;
            if ($name == 'vsego') continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $label = '';
            if ($name == 'failcall') $label = 'Недозвон';
            if ($name == 'approve') $label = 'Подтверждено';
            if ($name == 'buyout') $label = 'Выкуплено';
            if ($name == 'trash') $label = 'Трэш';
            if ($name == 'hold') $label = 'Холд';
            if ($name == 'reject') $label = 'Отказ';

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'makeLinkToOrders' => true,
                'attribute' => $name,
                'label' => Yii::t('common', $label),
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px',
            ];
        }

        parent::initColumns();
    }
}
