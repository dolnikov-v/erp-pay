<?php

namespace app\modules\report\extensions;

use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormDelivery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use Yii;

/**
 * Class GridViewDeliverySummary
 * @package app\modules\report\extensions
 */
class GridViewDeliverySummary extends GridView
{
    /**
     * @var ReportFormDelivery
     */
    public $reportForm;

    /**
     * @inheritdoc
     */

    /**
     * @return array
     */
    private function getDateColumn()
    {
        return [
            'label' => Yii::t('common', 'Дата'),
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
            'format' => 'raw',
            'group' => true,
            'value' => function () {
                if (!empty($this->reportForm->getDateFilter()->type) && !empty($this->reportForm->getDateFilter()->from) && !empty($this->reportForm->getDateFilter()->to)) {
                    return $this->reportForm->getDateFilter()->from . ' - ' . $this->reportForm->getDateFilter()->to;
                } elseif (!empty($this->reportForm->getDateFilter()->type) && !empty($this->reportForm->getDateFilter()->to)) {
                    return Yii::t('common', 'До {date}', ['date' => $this->reportForm->getDateFilter()->to]);
                } elseif (!empty($this->reportForm->getDateFilter()->type) && !empty($this->reportForm->getDateFilter()->from)) {
                    return Yii::t('common', 'С {date}', ['date' => $this->reportForm->getDateFilter()->from]);
                } else {
                    return Yii::t('common', 'Период не указан');
                }
            }
        ];
    }

    public function initColumns()
    {

        if ($this->reportForm->groupBy == ReportForm::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP) {
            $this->columns[] = [
                'label' => Yii::t('common', 'Провинция'),
                'pageSummary' => Yii::t('common', 'Всего'),
                'width' => '110px',
                'format' => 'raw',
                'group' => true,
                'value' => function ($model) {
                    return $model['groupbylabel'] == '' ? '—' : $model['groupbylabel'];
                }
            ];

            $this->columns[] = [
                'attribute' => 'city',
                'label' => Yii::t('common', 'Город'),
                'width' => '110px',
                'pageSummary' => false,
                'group' => true,
                'subGroupOf' => 0,
                'value' => function ($model) {
                    return $model['city'] == '' ? '—' : $model['city'];
                }
            ];
            $this->columns[] = [
                'attribute' => 'subdistrict',
                'label' => Yii::t('common', 'Квартал'),
                'width' => '110px',
                'pageSummary' => false,
                'group' => true,
                'subGroupOf' => 1,
                'value' => function ($model) {
                    return $model['subdistrict'] == '' ? '—' : $model['subdistrict'];
                }
            ];
            $this->columns[] = [
                'attribute' => 'zip',
                'label' => Yii::t('common', 'ZIP код'),
                'width' => '110px',
                'pageSummary' => false,
            ];

        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_SUBDISTRICT_ZIP) {
            $this->columns[] = [
                'label' => Yii::t('common', 'Квартал'),
                'pageSummary' => Yii::t('common', 'Всего'),
                'width' => '110px',
                'format' => 'raw',
                'group' => true,
                'value' => function ($model) {
                    return $model['groupbylabel'] == '' ? '—' : $model['groupbylabel'];
                }
            ];

            $this->columns[] = [
                'attribute' => 'zip',
                'label' => Yii::t('common', 'ZIP код'),
                'width' => '110px',
                'pageSummary' => false,
            ];

        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY) {
            $this->columns[] = $this->getDateColumn();
            $this->columns[] = [
                'attribute' => 'country',
                'label' => Yii::t('common', 'Страна'),
                'width' => '110px',
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'delivery',
                'label' => Yii::t('common', 'Служба доставки'),
                'width' => '110px',
                'pageSummary' => false,
            ];
        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_DATE_PRODUCTS) {
            $this->columns[] = $this->getDateColumn();
            $this->columns[] = [
                'attribute' => 'groupbylabel',
                'label' => Yii::t('common', 'Товар'),
                'width' => '110px',
                'pageSummary' => false,
            ];
        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_PRODUCT_CATEGORY) {
            $this->columns[] = [
                'label' => Yii::t('common', 'Категория товаров'),
                'pageSummary' => Yii::t('common', 'Всего'),
                'width' => '110px',
                'format' => 'raw',
                'group' => true,
                'value' => function ($model) {
                    return $model['groupbylabel'] == '' ? '—' : $model['groupbylabel'];
                }
            ];
        } else {
            $this->columns[] = [
                'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
                'pageSummary' => Yii::t('common', 'Всего'),
                'width' => '110px',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY ||
                        $this->reportForm->groupBy == ReportForm::GROUP_BY_CUSTOMER_PROVINCE ||
                        $this->reportForm->groupBy == ReportForm::GROUP_BY_CUSTOMER_CITY
                    ) {
                        $params['geoType'] = $this->reportForm->groupBy;
                        $params['geoName'] = $model['groupbylabel'];
                        return Html::a($model['groupbylabel'], '#', [
                            'title' => Yii::t('common', 'Открыть список ZIP-кодов'),
                            'data-url' => Url::toRoute(['zip'] + $params),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal_delivery',
                            'class' => 'lnk_delivery_get_zips',
                        ]);
                    }
                    return $model['groupbylabel'];
                }
            ];
        }

        $ids = [];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();
        asort($ids);
        $innerStatuses = [];
        foreach ($ids as $status) {
            $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
        }

        foreach ($mapStatuses as $name => $statuses) {
            $ids = array_merge($ids, $statuses);

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            // Скрываем колнку апрувлено
            if ($name == Yii::t('common', 'Апрувлено')) {
                continue;
            }
            // Сдвигаем колонку всего сразу после Не выкуплено
            if ($name == Yii::t('common', 'Деньги получены')) {

                $this->columns[] = [
                    'attribute' => Yii::t('common', 'Претензия'),
                    'class' => DataColumnCount::className(),
                    'value' => function ($data) {
                        return $data[Yii::t('common', 'Претензия')] ?? '';
                    },
                    'extraLinkParams' => [
                        'PretensionFilter' => [
                            'pretension' => array_keys(OrderFinancePretension::getTypeCollecion())
                        ]
                    ],
                    'width' => '110px',
                    'pageSummary' => $this->reportForm->groupBy == ReportForm::GROUP_BY_DATE_PRODUCTS ? false : true,
                ];

                $this->columns[] = [
                    'class' => DataColumnCount::className(),
                    'attribute' => Yii::t('common', 'Всего'),
                    'columnLinkParams' => implode(',', $ids),
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                    ],
                    'width' => '110px',
                    'pageSummary' => $this->reportForm->groupBy == ReportForm::GROUP_BY_DATE_PRODUCTS ? false : true,
                ];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'width' => '110px',
                'pageSummary' => $this->reportForm->groupBy == ReportForm::GROUP_BY_DATE_PRODUCTS ? false : true,
            ];
        }

        $this->columns[] = [
            'attribute' => Yii::t('common', 'Средний чек апрувленный'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', [
                    'statuses' => implode(', ', array_map(function ($item) use ($orderStatuses) {
                        return $item . '. ' . $orderStatuses[$item];
                    }, OrderStatus::getApproveList()))
                ]),
            ],
            'width' => '110px',
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }

                $models = $this->dataProvider->getModels();
                $sum = 0;
                $count = array_sum(ArrayHelper::getColumn($models, 'approved_cnt'));
                if (!$count) {
                    return 0;
                }
                foreach ($models as $model) {
                    $sum += ($model[Yii::t('common', 'Средний чек апрувленный')] * $model['approved_cnt']);
                }
                return round($sum / $count, 2);
            },
            'format' => ['decimal', 2]
        ];

        $this->columns[] = [
            'attribute' => Yii::t('common', 'Средний чек выкупной'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', [
                    'statuses' => implode(', ', array_map(function ($item) use ($orderStatuses) {
                        return $item . '. ' . $orderStatuses[$item];
                    }, $mapStatuses[yii::t('common', 'Выкуплено')]))
                ]),
            ],
            'width' => '110px',
            'pageSummaryFunc' => function () {
                if (!$this->reportForm->dollar) {
                    return 0;
                }
                $models = $this->dataProvider->getModels();
                $sum = 0;
                $count = array_sum(ArrayHelper::getColumn($models, Yii::t('common', 'Выкуплено')));
                if (!$count) {
                    return 0;
                }
                foreach ($models as $model) {
                    $sum += ($model[Yii::t('common', 'Средний чек выкупной')] * $model[Yii::t('common', 'Выкуплено')]);
                }
                return round($sum / $count, 2);
            },
            'format' => ['decimal', 2]
        ];

        $this->columns[] = [
            'attribute' => 'avg_delivery_time',
            'label' => Yii::t('common', 'Ср. срок доставки, дней'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
            ],
            'contentOptions' => [
                'title' => Yii::t('common', 'Указан в днях')
            ],
            'width' => '110px',
            'pageSummaryFunc' => GridView::F_AVG,
            'format' => ['integer']
        ];

        parent::initColumns();
    }
}
