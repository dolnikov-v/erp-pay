<?php

namespace app\modules\report\extensions;

use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/**
 * Class DataColumnPercent
 * @package app\modules\report\extensions
 */
class DataColumnPercent extends DataColumn
{
    /**
     * @var null | string
     */
    public $totalCountAttribute = null;

    /**
     * @var null | \Closure
     */
    public $link = null;

    /**
     * @var string
     */
    public $hAlign = 'center';

    /**
     * @var string
     */
    public $vAlign = null;

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $dataCellContent = parent::renderDataCellContent($model, $key, $index);

        $percent = $this->calculatePercent($dataCellContent, $model[$this->totalCountAttribute]);
        $dataCellContent .= ' (' . $percent . ')';

        if ($this->link) {
            $link = $this->link;
            $link = $link($model);
            return Html::a($dataCellContent, $link);
        }
        return $dataCellContent;
    }

    /**
     * @inheritdoc
     */
    protected function renderPageSummaryCellContent()
    {
        $pageSummaryCellContent = parent::renderPageSummaryCellContent();

        $dataProvider = $this->grid->dataProvider;

        $totalCount = array_sum(ArrayHelper::getColumn($dataProvider->models, $this->totalCountAttribute));

        $percent = self::calculatePercent($pageSummaryCellContent, $totalCount);
        $pageSummaryCellContent .= ' (' . $percent . ')';

        return $pageSummaryCellContent;
    }

    /**
     * @param integer $count
     * @param integer $totalCount
     * @return string
     */
    public static function calculatePercent($count, $totalCount)
    {
        if (is_numeric($totalCount) && (int)$totalCount > 0) {
            $percent = $count / $totalCount * 100;
            if ($percent < 0) {
                $percent = 0;
            }
            if ($percent > 100) {
                $percent = 100;
            }
            $percent = number_format($percent, 2,
                    Yii::$app->get('formatter')->decimalSeparator,
                    Yii::$app->get('formatter')->thousandSeparator) . '%';
        } else {
            $percent = number_format(0, 2) . '%';
        }

        return $percent;
    }
}