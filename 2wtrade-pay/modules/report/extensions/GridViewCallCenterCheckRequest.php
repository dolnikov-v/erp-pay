<?php

namespace app\modules\report\extensions;

use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\components\ReportFormShort;
use Yii;

/**
 * Class GridViewCallCenterCheckRequest
 * @package app\modules\report\extensions
 */
class GridViewCallCenterCheckRequest extends GridView
{
    /**
     * @var ReportFormShort
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Всего'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => Yii::t('common', 'Всего'),
            'class' => DataColumnCount::className(),
            'columnLinkParams' => 0,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Ожидает заказ'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'waiting',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_STILL_WAITING,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Заказ доставлен'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'received',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_ALREADY_RECEIVED,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Клиент отказался'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'notneeded',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_NO_LONGER_NEEDED,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'КС не связалась с клиентом'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'notcontacted',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Автоматическое отклонение'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'auto',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_AUTO,
            'statuses' => $this->dataProvider->statuses
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Не обработано'),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'notdone',
            'class' => DataColumnCount::className(),
            'columnLinkParams' => CallCenterCheckRequest::FOREIGN_SUB_STATUS_EMPTY,
            'statuses' => $this->dataProvider->statuses
        ];
        parent::initColumns();
    }
}
