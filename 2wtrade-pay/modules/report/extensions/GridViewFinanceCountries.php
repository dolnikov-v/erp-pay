<?php
namespace app\modules\report\extensions;

use app\models\Country;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormFinanceCountries;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewFinanceCountries
 * @package app\modules\report\extensions
 * @property array $countries
 */
class GridViewFinanceCountries extends GridView
{
    /**
     * @var ReportFormFinanceCountries
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        /**@TO_DO Стереть после тестирования**/
        //$countries = Country::find()->collection();
        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'Country',
            /*'value' => function ($data) {
                return $this->getCountries()[$data['Country']];
            },*/
            //'pageSummary' => Yii::t('common', 'Всего'),
        ];

        /*$this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
        ];*/

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {
            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Ср. чек КЦ'),
            'value' => function ($data) {
                if ($this->reportForm->dollar) {
                    return round($data[Yii::t('common', 'Ср. чек КЦ')] / ReportFormFinanceCountries::$currencies[$data['country_id']], 2);
                } else {
                    return round($data[Yii::t('common', 'Ср. чек КЦ')], 2);
                }
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Ср. чек КЦ'),
            'value' => function ($data) {
                if ($this->reportForm->dollar) {
                    return round($data[Yii::t('common', 'Ср. чек КЦ')] / ReportFormFinanceCountries::$currencies[$data['country_id']], 2);
                } else {
                    return round($data[Yii::t('common', 'Ср. чек КЦ')], 2);
                }
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
        ];

        /*$this->columns[] = [
            'label' => Yii::t('common', 'Ср. доход по выкупам'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Ср. доход по выкупам')], 2);
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
            'pageSummary' => !is_null($this->reportForm->dollar) ?: '—',
        ];*/

        $this->columns[] = [
            'label' => Yii::t('common', 'Доход по выкупам'),
            'value' => function ($data) {
                if ($this->reportForm->dollar) {
                    return round($data[Yii::t('common', 'Доход по выкупам')] / ReportFormFinanceCountries::$currencies[$data['country_id']], 2);
                } else {
                    return round($data[Yii::t('common', 'Доход по выкупам')], 2);
                }
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Цена за лиды'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Цена за лиды')], 2);
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы'),
            'value' => function () {
                return 0;
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Прибыль'),
            'value' => function ($data) {
                return $this->getValueIncome($data);
            },
            'content' => function ($data) {
                $value = $this->getValueIncome($data);
                $value = Yii::$app->formatter->asDecimal($value, 2);

                if ($value < 0) {
                    $value = Html::tag('span', $value, ['class' => 'text-danger']);
                }

                return $value;
            },
            'format' => ['decimal', 2],
        ];

        parent::initColumns();
    }

    /**
     * @param $data
     * @return integer
     */
    protected function getValueIncome($data)
    {
        $buyout = $data[Yii::t('common', 'Доход по выкупам')];
        $leads = $data[Yii::t('common', 'Цена за лиды')];
        $consumptions = 0;

        return $buyout - $leads - $consumptions;
    }

    /**
     *
     */
    public function getCountries()
    {
        return Country::find()->collection();
    }
}
