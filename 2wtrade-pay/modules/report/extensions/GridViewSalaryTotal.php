<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormSalary;
use Yii;
use app\modules\order\models\OrderStatus;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewSalaryTotal
 * @package app\modules\report\extensions
 */
class GridViewSalaryTotal extends GridView
{
    /**
     * @var ReportFormSalary
     */
    public $reportForm;

    public $export = false;

    public $summary = [];

    public function init()
    {
        $summaryFields = [
            'count_opers',
            'expense_amount',
            'staff_salary_hand_usd',
            'staff_salary_hand_local',
            'operators_salary_hand_usd',
            'operators_salary_hand_local',
            'staff_salary_tax_usd',
            'staff_salary_tax_local',
            'operators_salary_tax_usd',
            'operators_salary_tax_local',
        ];

        foreach ($summaryFields as $field) {
            $this->summary[$field] = 0;
        }

        foreach ($this->dataProvider->getModels() as $key => $model) {
            if (!$model['direction']) {
                foreach ($model as $field => $value) {
                    if (in_array($field, $summaryFields)) {
                        $this->summary[$field] += $value;
                    }
                }
            }
        }

        parent::init();

        $this->rowOptions = function ($model) {
            $options = [];
            if (array_key_exists('direction', $model) && is_null($model['direction'])) {
                $options = ['class' => 'warning'];
            }
            return $options;
        };
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'attribute' => 'office',
            'label' => Yii::t('common', 'Офис'),
            'pageSummary' => Yii::t('common', 'Итого'),
            'content' => function ($model) {
                return Yii::t('common', $model['office']);
            },
        ];
        $this->columns[] = [
            'attribute' => 'direction',
            'label' => Yii::t('common', 'Направление'),
            'pageSummary' => false,
            'content' => function ($model) {
                $content = '-';
                if (array_key_exists('direction', $model)) {
                    if (!is_null($model['direction'])) {
                        $content = Yii::t('common', $model['direction']);
                    } else {
                        $content = Yii::t('common', 'Все');
                    }
                }

                return $content;
            },
        ];
        $this->columns[] = [
            'attribute' => 'operators_salary_hand_local',
            'label' => Yii::t('common', 'ЗП операторов в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['operators_salary_hand_local'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['operators_salary_hand_local']) && $model['operators_salary_hand_local'] ? Yii::$app->formatter->asDecimal($model['operators_salary_hand_local'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['operators_salary_hand_local'], 2, ',', ' '),
            'visible' => false,
        ];
        $this->columns[] = [
            'attribute' => 'operators_salary_hand_usd',
            'label' => Yii::t('common', 'ЗП операторов в USD "на руки"'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'ЗП операторов в USD "на руки" с учетом апрува по направлениям'),
            ],
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['operators_salary_hand_usd'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['operators_salary_hand_usd']) && $model['operators_salary_hand_usd'] ? Yii::$app->formatter->asDecimal($model['operators_salary_hand_usd'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['operators_salary_hand_usd'], 2, ',', ' '),
        ];
        $this->columns[] = [
            'attribute' => 'operators_salary_tax_usd',
            'label' => Yii::t('common', 'ЗП операторов в USD с учетом налогов'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'ЗП операторов с учетом налогов и апрува по направлениям'),
            ],
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['operators_salary_tax_usd'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['operators_salary_tax_usd']) && $model['operators_salary_tax_usd'] ? Yii::$app->formatter->asDecimal($model['operators_salary_tax_usd'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['operators_salary_tax_usd'], 2, ',', ' '),
        ];
        $this->columns[] = [
            'attribute' => 'staff_salary_hand_local',
            'label' => Yii::t('common', 'ЗП персонала в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['staff_salary_hand_local'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['staff_salary_hand_local']) && $model['staff_salary_hand_local'] ? Yii::$app->formatter->asDecimal($model['staff_salary_hand_local'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['staff_salary_hand_local'], 2, ',', ' '),
            'visible' => false,
        ];
        $this->columns[] = [
            'attribute' => 'staff_salary_hand_usd',
            'label' => Yii::t('common', 'ЗП персонала в USD "на руки"'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'ЗП персонала в USD "на руки" с учетом коэффициента полезности операторов'),
            ],
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['staff_salary_hand_usd'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['staff_salary_hand_usd']) && $model['staff_salary_hand_usd'] ? Yii::$app->formatter->asDecimal($model['staff_salary_hand_usd'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['staff_salary_hand_usd'], 2, ',', ' '),
        ];
        $this->columns[] = [
            'attribute' => 'staff_salary_tax_usd',
            'label' => Yii::t('common', 'ЗП персонала в USD с учетом налогов'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'ЗП персонала с учетом налогов и коэффициента полезности операторов'),
            ],
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['staff_salary_tax_usd'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['staff_salary_tax_usd']) && $model['staff_salary_tax_usd'] ? Yii::$app->formatter->asDecimal($model['staff_salary_tax_usd'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['staff_salary_tax_usd'], 2, ',', ' '),
        ];
        $this->columns[] = [
            'attribute' => 'expense_amount',
            'label' => Yii::t('common', 'Постоянные расходы в USD'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Расходы офиса с учетом коэффициента полезности операторов'),
            ],
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['expense_amount'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['expense_amount']) && $model['expense_amount'] ? Yii::$app->formatter->asDecimal($model['expense_amount'], 2) : '-';
            },
            'pageSummary' => number_format($this->summary['expense_amount'], 2, ',', ' '),
        ];
        $this->columns[] = [
            'attribute' => 'operators_utility_normalize',
            'label' => Yii::t('common', 'Коэффициент полезности операторов, %'),
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Доля от "Подтверждено" всего офиса к направлению'),
            ],
            'format' => ['decimal', 4],
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function ($model) {
                return $model['operators_utility_normalize'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['operators_utility_normalize']) && $model['operators_utility_normalize'] ? Yii::$app->formatter->asDecimal($model['operators_utility_normalize'] * 100, 2) : '-';
            },
        ];
        $this->columns[] = [
            'attribute' => 'operators_approve_count',
            'label' => Yii::t('common', 'Подтверждено'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function ($model) {
                return $model['operators_approve_count'] ?? 0;
            },
            'content' => function ($model) {
                return $model['operators_approve_count'] ?? '-';
            },
        ];
        $this->columns[] = [
            'attribute' => 'rate',
            'label' => Yii::t('common', 'Курс'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function ($model) {
                return $model['rate'] ?? 0;
            },
            'content' => function ($model) {
                return isset($model['rate']) && $model['rate'] ? Yii::$app->formatter->asDecimal($model['rate'], 2) : '-';
            },
        ];
        $this->columns[] = [
            'attribute' => 'count_opers',
            'label' => Yii::t('common', 'Кол-во сотрудников'),
            'hAlign' => 'right',
            'pageSummary' => $this->summary['count_opers'],
        ];
        parent::initColumns();
    }
}