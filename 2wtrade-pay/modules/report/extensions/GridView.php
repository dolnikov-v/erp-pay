<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportForm;
use app\modules\report\widgets\ExportReportToMail;
use app\widgets\ButtonProgress;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridView
 * @package app\modules\report\extensions
 */
class GridView extends \kartik\grid\GridView
{
    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var DataProvider
     */
    public $dataProvider;

    /**
     * @var DataColumn[]
     */
    public $columns = [];

    /**
     * @var boolean
     */
    public $resizableColumns = true;

    /**
     * @var boolean
     */
    public $showPageSummary = true;

    /**
     * Показывать суммарную информацию сверху
     * @var boolean
     */
    public $showHeaderSummary = true;

    /**
     * @var string
     */
    public $dataColumnClass = 'app\modules\report\extensions\DataColumn';

    /**
     * @var array
     */
    public $tableOptions = [
        'class' => 'table table-report table-striped table-hover table-bordered tl-fixed',
    ];

    /**
     * @var string
     */
    public $exportFilename = '';

    /**
     * @var array|boolean
     */
    public $export = [
        'showConfirmAlert' => false,
        'fontAwesome' => true,
        'target' => self::TARGET_SELF,
        'header' => false,
    ];

    /**
     * @var array
     */
    public $exportConfig = [
        GridView::EXCEL => [],
        GridView::PDF => [],
        GridView::CSV => [],
        GridView::JSON => [],
    ];

    /**
     * показывать или нет кнопку отправки отчета на email
     */
    public $can_sent_to_mail = false;

    /**
     * @var array|boolean
     */
    public $pageSummaryRowOptions = [
        'class' => 'report-summary',
    ];

    /**
     * @var string
     */
    public $layout = '<div id="report_export_box">{toolbar}</div>{items}';

    /**
     * @var string
     */
    public $toolbar = '{export}';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->dataProvider->getCount() == 0) {
            $this->export = false;
            $this->can_sent_to_mail = false;
        }

        if ($this->export !== false) {
            $this->export['label'] = Yii::t('common', 'Экспорт');
            $this->export['options']['class'] = 'btn btn-default btn-sm';

            $exportConfig = [
                GridView::EXCEL => [
                    'label' => Yii::t('common', 'Excel'),
                ],
                GridView::PDF => [
                    'label' => Yii::t('common', 'PDF'),
                ],
                GridView::CSV => [
                    'label' => Yii::t('common', 'CSV'),
                ],
                GridView::JSON => [
                    'label' => Yii::t('common', 'JSON'),
                ],
            ];

            if ($this->exportFilename != '') {
                foreach ($exportConfig as &$config) {
                    $config['filename'] = $this->exportFilename;
                }
            }

            $this->exportConfig = $exportConfig;
        }

        if ($this->showPageSummary === false) {
            $this->pageSummaryRowOptions = false;
        }

        if ($this->can_sent_to_mail === true) {

            $this->layout = '<div id="report_export_to_mail"
                data-url="' .Url::toRoute('export-to-mail/sent') .'"
                data-controller="' .str_replace('-', '_', Yii::$app->controller->id) .'">'
                .ButtonProgress::widget([
                        "id" => "btn_send_report_to_mail",
                        "style" => ButtonProgress::STYLE_DEFAULT . " width-200",
                        "size" => ButtonProgress::SIZE_SMALL,
                        "label" => Yii::t("common", "Отправить отчет на Email"),
                    ])
                .'</div>' .$this->layout;


            echo ExportReportToMail::widget([
                'url' => Url::toRoute('export-to-mail/sent'),
            ]);
        }

        parent::init();
    }

    /**
     * @return string
     */
    public function renderTableHeader()
    {
        $cells = [];

        foreach ($this->columns as $index => $column) {
            if ($this->resizableColumns && $this->persistResize) {
                $column->headerOptions['data-resizable-column-id'] = "kv-col-{$index}";
            }

            $cells[] = $column->renderHeaderCell();
        }

        $content = Html::tag('tr', implode('', $cells), $this->headerRowOptions);

        if ($this->filterPosition == self::FILTER_POS_HEADER) {
            $content = $this->renderFilters() . $content;
        } elseif ($this->filterPosition == self::FILTER_POS_BODY) {
            $content .= $this->renderFilters();
        }

        if ($this->pageSummaryRowOptions !== false) {
            if ($this->showHeaderSummary) {
                $cells = [];

                foreach ($this->columns as $column) {
                    $cells[] = $column->renderPageSummaryCell();
                }

                $content .= Html::tag('tr', implode('', $cells), $this->pageSummaryRowOptions);
            }
        }

        return "<thead>" . PHP_EOL .
        $this->generateRows($this->beforeHeader) . PHP_EOL .
        $content . PHP_EOL .
        $this->generateRows($this->afterHeader) . PHP_EOL .
        "</thead>";
    }
}
