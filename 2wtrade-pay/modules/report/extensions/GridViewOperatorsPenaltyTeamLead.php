<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormOperatorsPenalty;
use Yii;

/**
 * Class GridViewOperatorsPenaltyTeamLead
 * @package app\modules\report\extensions
 */
class GridViewOperatorsPenaltyTeamLead extends GridView
{
    /**
     * @var ReportFormOperatorsPenalty
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => Yii::t('common', 'Колл-центр'),
            'attribute' => 'callCenterId',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Тимлид'),
            'attribute' => 'id',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Суммарный штраф операторов, USD'),
            'attribute' => 'penaltySum',
        ];

        parent::initColumns();
    }
}
