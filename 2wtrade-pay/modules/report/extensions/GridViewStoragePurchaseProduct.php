<?php

namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormPurchaseStorage;
use Exception;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewStoragePurchaseProduct
 * @package app\modules\report\extensions
 */
class GridViewStoragePurchaseProduct extends GridView
{
    /**
     * @var ReportFormPurchaseStorage
     */
    public $reportForm;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        if ($this->reportForm->groupBy == ReportFormPurchaseStorage::GROUP_BY_PRODUCTS) {
            $this->columns[] = [
                'attribute' => 'product_name',
                'label' => Yii::t('common', 'Товар'),
                'hAlign' => 'center',
                'group' => true,
            ];
            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'hAlign' => 'center',
            ];
        } else {
            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'hAlign' => 'center',
            ];

            $this->columns[] = [
                'attribute' => 'product_name',
                'label' => Yii::t('common', 'Товар'),
                'hAlign' => 'center',
            ];
        }

        $this->columns[] = [
            'value' => function ($model) {
                $text = '—';
                if (!empty(floatval($model['plan_lead_day']))) {
                    $text = round($model['balance'] / floatval($model['plan_lead_day']), 0, PHP_ROUND_HALF_DOWN);
                }
                return Html::a($text, Url::to(['/storage/purchase/index', 'force_country_slug' => $model['country_slug']], true));
            },
            'format' => 'raw',
            'label' => Yii::t('common', 'Планируемый остаток в днях'),
            'enableSorting' => false,
            'hAlign' => 'center',
        ];

        $this->columns[] = [
            'value' => function ($model) {
                $text = '—';
                if (!empty(floatval($model['orders']))) {
                    $text = round($model['balance'] / floatval($model['orders']), 0, PHP_ROUND_HALF_DOWN);
                }
                return Html::a($text, Url::to(['/storage/purchase/index', 'force_country_slug' => $model['country_slug']]));
            },
            'label' => Yii::t('common', 'Фактический остаток в днях'),
            'enableSorting' => false,
            'hAlign' => 'center',
            'format' => 'raw',
        ];

        $this->columns[] = [
            'attribute' => 'last_purchase_date',
            'label' => Yii::t('common', 'Дата планируемой закупки'),
            'enableSorting' => false,
            'format' => 'date',
            'hAlign' => 'center',
            'value' => function ($model) {
                if (empty($model['last_purchase_date'])) {
                    return null;
                }
                return $model['last_purchase_date'];
            }
        ];

        parent::initColumns();
    }
}