<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormConsolidateByCountry;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewConsolidateByCountry
 * @package app\modules\report\extensions
 */
class GridViewConsolidateByCountry extends GridView
{
    /**
     * @var ReportFormConsolidateByCountry
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'label' => Yii::t('common', 'Период'),
            'attribute' => Query::GROUP_BY_LABEL,
            'value' => function ($data) {
                $ym = explode("-", $data["year_month"]);
                return $ym[0] . ', ' . Yii::t('common', ReportFormConsolidateByCountry::monthName($ym[1]));
            },
        ];

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {

            if ($name == Yii::t('common', 'Всего')) continue;
            if ($name == Yii::t('common', 'Выкуп')) continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
            ];
        }


        $this->columns[] = [
            'label' => Yii::t('common', 'Расход на апрув'),
            'value' => function ($data) {
                return "-";
            },
        ];

        if (Yii::$app->user->can('view_finance_director')) {
            $this->columns[] = [
                'label' => Yii::t('common', 'Дебиторка'),
                'value' => function ($data) {
                    return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Дебиторка')], 2);
                },
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы на КЦ'),
            'value' => function ($data) {
                return "-";
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы на рекламу'),
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Расходы на рекламу')], 2);
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы на доставку'),
            'value' => function ($data) {
                return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Расходы на доставку')], 2);
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Прочие расходы'),
            'value' => function ($data) {
                return "-";
            },
        ];

        if (Yii::$app->user->can('view_finance_director')) {
            $this->columns[] = [
                'label' => Yii::t('common', 'Доход'),
                'value' => function ($data) {
                    return Yii::$app->formatter->asDecimal($data[Yii::t('common', 'Доход')], 2);
                },
            ];

            $this->columns[] = [
                'label' => Yii::t('common', 'Прибыль'),
                'value' => function ($data) {
                    return Yii::$app->formatter->asDecimal((
                        $data[Yii::t('common', 'Доход')] -
                        $data[Yii::t('common', 'Расходы на доставку')] -
                        $data[Yii::t('common', 'Расходы на рекламу')]), 2);
                },
            ];
        }

        parent::initColumns();
    }
}
