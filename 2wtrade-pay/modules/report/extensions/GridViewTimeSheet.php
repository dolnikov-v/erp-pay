<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormTimeSheet;
use Yii;
use app\modules\order\models\OrderStatus;
use yii\helpers\Html;

/**
 * Class GridViewTimeSheet
 * @package app\modules\report\extensions
 */
class GridViewTimeSheet extends GridView
{
    /**
     * @var ReportFormTimeSheet
     */
    public $reportForm;

    /**
     * @var array
     */
    public $dates;

    public function init()
    {
        $officeFilter = $this->reportForm->getOfficeFilter();
        $text = '';
        if ($officeFilter->office) {
            foreach ($officeFilter->office as $office) {
                $text .= $officeFilter->offices[$office] ?? '';
            }
            $text = Yii::t('common', $text, [], "en-US");
            $text = preg_replace('/[^\w]+/', '_', $text);
        }
        $this->exportFilename = 'time-sheet_' . $text . '_' . Yii::$app->formatter->asDate($this->reportForm->getDateWorkFilter()->from, 'php:Y-m-d') . '_' . Yii::$app->formatter->asDate($this->reportForm->getDateWorkFilter()->to, 'php:Y-m-d');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->rowOptions = function ($model) {
            $highLightPerson = Yii::$app->request->get('person');
            return [
                'class' => ($model['active'] ? '' : 'text-muted') . ($model['id'] == $highLightPerson ? ' high-light' : ''),
            ];
        };

        $this->columns[] = ['class' => 'kartik\grid\SerialColumn'];

        $this->columns[] = [
            'attribute' => 'parent_id',
            'label' => Yii::t('common', 'Руководитель'),
            'pageSummary' => false,
            'pageSummaryOptions' => ['hidden' => true],
            'hiddenFromExport' => true,
            'group' => true,  // enable grouping,
            'groupedRow' => true,                    // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
            'value' => function ($model) {
                return (($model['parent_id']) ? Yii::t('common', $model['parent_designation']) . ': ' . $model['parent_name'] : Yii::t('common', 'Руководитель не задан'));
            },
            'groupFooter' => function ($model, $key, $index, $widget) { // Closure method


                $content = $contentFormats = $contentOptions = [];
                $content[2] = Yii::t('common', 'Итого по региону') . (($model['parent_id']) ? ' ' . $model['parent_name'] : '');

                for ($j = 6; $j <= sizeof($model['dates']) + 6; $j++) {
                    $content[$j] = (($j == sizeof($model['dates']) + 6) ? GridView::F_SUM : '');
                    $contentFormats[$j] = ['format' => 'number', 'decimals' => 1, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator];
                    $contentOptions[$j] = ['style' => 'text-align:center'];
                }

                return [
                    'mergeColumns' => [[2, 5]], // columns to merge in summary
                    'content' => $content,
                    'contentFormats' => $contentFormats,
                    'contentOptions' => $contentOptions,
                    'options' => ['class' => 'info', 'style' => 'font-style:italic;']
                ];
            }
        ];
        $this->columns[] = [
            'attribute' => 'country_names',
            'label' => Yii::t('common', 'Направление'),
            'pageSummary' => Yii::t('common', 'Итого'),
            'content' => function ($model) {
                $r = [];
                foreach (explode(',', $model['country_names']) as $name) {
                    $r[$name] = Yii::t('common', $name);
                }
                return implode('<br>', $r);
            },
        ];
        $this->columns[] = [
            'attribute' => 'fio',
            'label' => Yii::t('common', 'ФИО'),
            'pageSummary' => '',
            'content' => function ($model) {
                return Html::a('', null, ['name' => 'person' . $model['id']]) . $model['fio'];
            }
        ];
        $this->columns[] = [
            'attribute' => 'designation',
            'label' => Yii::t('common', 'Должность'),
            'pageSummary' => '',
            'value' => function ($model) {
                return Yii::t('common', $model['designation']);
            },
        ];
        $this->columns[] = [
            'attribute' => 'call_center_user_logins',
            'label' => Yii::t('common', 'Логин'),
            'pageSummary' => '',
            'content' => function ($model) {
                return str_replace(',', '<br>', $model['call_center_user_logins']);
            },
        ];
        $weekDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        foreach ($this->dates as $date => $isHoliday) {
            $column = [
                'attribute' => 'dates',
                'header' => date('j', strtotime($date)) . '<br>' . Yii::t('common', $weekDays[date('w', strtotime($date))]),
                'format' => ['decimal', 1],
                'hAlign' => 'center',
                'value' => function ($model) use ($date) {
                    return $model['dates'][$date];
                },
                'content' => function ($model) use ($date) {
                    return Yii::$app->formatter->asDecimal($model['dates'][$date], 1) . '<br>' .
                           Yii::$app->formatter->asDecimal(($model['plan'][$date] ?? 0), 1);
                },
                'pageSummary' => '',
                'contentOptions' => function ($model) use ($date, $isHoliday) {
                    $r = ['class' => 'day'];
                    if ($isHoliday) {
                        $r = ['class' => 'holiday'];
                    } else {
                        if ($model['designation_calc_work_time']) {
                            if (!isset($model['plan'][$date]) || !$model['plan'][$date]) {
                                $r = ['class' => 'weekend'];
                            }
                        } else {
                            if (date('w', strtotime($date)) == 0) {
                                $r = ['class' => 'weekend'];
                            }
                        }
                    }
                    return $r;
                },
            ];


            $this->columns[] = $column;
        }

        $this->columns[] = [
            'attribute' => 'total',
            'label' => Yii::t('common', 'Итог'),
            'format' => ['decimal', 1],
            'hAlign' => 'center',
            'contentOptions' => ['class' => 'total'],
            'headerOptions' => ['class' => 'total'],
            'value' => function ($model) {
                return $model['total'] ?? 0;
            },
        ];

        parent::initColumns();
    }
}