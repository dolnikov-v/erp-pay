<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormProduct;
use Yii;

/**
 * Class GridViewRatingProduct
 * @package app\modules\report\extensions
 */
class GridViewRatingProduct extends GridView
{
    /**
     * @var ReportFormProduct
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => Yii::t('common', 'Название'),
            'attribute' => 'productName',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Доходность'),
            'attribute' => 'income',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'На складе'),
            'attribute' => 'quantityInStock',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Возможное количество заказов в день, до следующей поставки'),
            'attribute' => 'quantityPerDay',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Запас по месяцам'),
            'attribute' => 'quantityEnoughForMonth',
        ];

        parent::initColumns();
    }
}
