<?php
namespace app\modules\report\extensions;

/**
 * Class DataColumn
 * @package app\modules\report\extensions
 */
class DataColumn extends \kartik\grid\DataColumn
{
    /**
     * @var GridView
     */
    public $grid;

    /**
     * @var boolean
     */
    public $pageSummary = true;

    /**
     * @var string
     */
    public $vAlign = 'middle';

    /**
     * @param string $value
     * @return DataColumn|null
     */
    protected function getColumnByLabel($value)
    {
        foreach ($this->grid->columns as $key => $column) {
            if ($column->attribute != null) {
                if ($value == $column->attribute) {
                    return $this->grid->columns[$key];
                }
            } else {
                if ($value == $column->label) {
                    return $this->grid->columns[$key];
                }
            }
        }

        return null;
    }


    /**
     * Надстройка чтобы считать среднее при указании 'pageSummaryFunc' => GridView::F_AVG
     * будем отбрасывать все пустые значения
     * средний чек, средний срок доставки и тп.
     *
     * @return float
     */
    protected function calculateSummary()
    {
        $data = $this->_rows;
        $type = $this->pageSummaryFunc;
        if ($type == GridView::F_AVG) {
            $notEmptyData = [];
            foreach ($data as $val) {
                if (!empty($val)) {
                    $notEmptyData[] = $val;
                }
            }
            return count($notEmptyData) > 0 ? array_sum($notEmptyData) / count($notEmptyData) : null;
        }
        return parent::calculateSummary();
    }
}
