<?php
namespace app\modules\report\extensions;

use Yii;

/**
 * Class GridViewProduct
 * @package app\modules\report\extensions
 */
class GridViewDeliveryDelayByCountry extends GridView
{
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;


        $this->rowOptions = function ($model) {
            $color = 'white';
            $d = strtotime($model['yearcreatedat'] . '-' . $model['monthcreatedat'] . '-' . $model['daycreatedat'] . ' 00:00:00');
            if ($d < (time() - 60*60*24*3)) {
                $color = '#FFE7E7';
            }

        return ['style' => 'background-color: ' .$color];
        };

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
            'contentOptions' => ['style' => 'background-color: white'],
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                $date_text = '';
                $date = strtotime($model['yearcreatedat'] . '-' . $model['monthcreatedat'] . '-' . $model['daycreatedat'] . ' 00:00:00');
                if ($date == 0) {
                    $date_text = 'Без даты';
                }
                else {
                    $date_text = date('d.m.Y', strtotime($model['yearcreatedat'] . '-' . $model['monthcreatedat'] . '-' . $model['daycreatedat'] . ' 00:00:00'));
                }
                return $date_text . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Курьерская служба'),
            'attribute' => 'delivery_name',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Кол-во заказов'),
            'attribute' => 'order_count',
        ];

        parent::initColumns();
    }
}
