<?php

namespace app\modules\report\models;

use app\components\ModelTrait;
use app\modules\deliveryreport\models\PaymentOrder;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $invoice_id
 * @property integer $payment_id
 *
 * @property Invoice $invoice
 * @property PaymentOrder $payment
 */
class InvoicePayment extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoice_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'payment_id'], 'required'],
            [['invoice_id', 'payment_id'], 'integer'],
            [
                ['invoice_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Invoice::className(),
                'targetAttribute' => ['invoice_id' => 'id']
            ],
            [
                ['payment_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PaymentOrder::className(),
                'targetAttribute' => ['payment_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('common', 'Invoice ID'),
            'payment_id' => Yii::t('common', 'Payment ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_id']);
    }
}
