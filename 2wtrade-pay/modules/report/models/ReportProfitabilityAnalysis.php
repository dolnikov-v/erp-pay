<?php

namespace app\modules\report\models;

use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "report_profitability_analysis".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $product_id
 * @property integer $year
 * @property integer $month
 * @property string $data_json
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ReportValidateFinanceData[] $reportValidateFinanceData
 */
class ReportProfitabilityAnalysis extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_profitability_analysis}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'product_id', 'year', 'month', 'created_at', 'updated_at'], 'integer'],
            [['country_id', 'product_id', 'year', 'month'], 'required'],
            [['data_json'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'product_id' => Yii::t('common', 'Номер продукта'),
            'country_id' => Yii::t('common', 'Номер страны'),
            'month' => Yii::t('common', 'Месяц'),
            'year' => Yii::t('common', 'Год'),
            'data_json' => Yii::t('common', 'Данные'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data_json ? json_decode($this->data_json, true) : [];
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data_json = ($data && is_array($data)) ? json_encode($data) : '';
    }
}