<?php

namespace app\modules\report\models;

use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "report_validate_finance_data".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string $order_id
 * @property string $order_id_file
 * @property string $tracking
 * @property string $tracking_file
 * @property string $delivery_cost_percent
 * @property string $delivery_cost_percent_file
 * @property string $fulfillment_cost
 * @property string $fulfillment_cost_file
 * @property string $nds
 * @property string $nds_file
 * @property string $fulfillment_nds
 * @property string $fulfillment_nds_file
 * @property string $delivery_price
 * @property string $delivery_price_file
 * @property string $total_price
 * @property string $total_price_file
 * @property string $status
 * @property string $status_file
 * @property string $error
 * @property string $warning
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ReportValidateFinance $file
 */
class ReportValidateFinanceData extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_validate_finance_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_id', 'created_at', 'updated_at'], 'integer'],
            [['order_id', 'order_id_file', 'tracking', 'tracking_file', 'delivery_cost_percent', 'delivery_cost_percent_file', 'fulfillment_cost', 'fulfillment_cost_file', 'nds', 'nds_file', 'fulfillment_nds', 'fulfillment_nds_file', 'delivery_price', 'delivery_price_file', 'total_price', 'total_price_file', 'status', 'status_file'], 'string', 'max' => 50],
            [['error', 'warning'], 'string', 'max' => 500],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportValidateFinance::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'file_id' => Yii::t('common', 'Файл'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'order_id_file' => Yii::t('common', 'Номер заказа (файл)'),
            'tracking' => Yii::t('common', 'Трек номер'),
            'tracking_file' => Yii::t('common', 'Трек номер (файл)'),
            'delivery_cost_percent' => Yii::t('common', 'Процент от заказа для КС'),
            'delivery_cost_percent_file' => Yii::t('common', 'Процент от заказа для КС (файл)'),
            'fulfillment_cost' => Yii::t('common', 'Стоимость фулфиллмента'),
            'fulfillment_cost_file' => Yii::t('common', 'Стоимость фулфиллмента (файл)'),
            'nds' => Yii::t('common', 'НДС на доставку'),
            'nds_file' => Yii::t('common', 'НДС на доставку (файл)'),
            'fulfillment_nds' => Yii::t('common', 'НДС на фулфиллмент'),
            'fulfillment_nds_file' => Yii::t('common', 'НДС на фулфиллмент (файл)'),
            'delivery_price' => Yii::t('common', 'Стоимость доставки'),
            'delivery_price_file' => Yii::t('common', 'Стоимость доставки (файл)'),
            'total_price' => Yii::t('common', 'Общая сумма'),
            'total_price_file' => Yii::t('common', 'Общая сумма (файл)'),
            'status' => Yii::t('common', 'Статус'),
            'status_file' => Yii::t('common', 'Статус (файл)'),
            'error' => Yii::t('common', 'Текст ошибки'),
            'warning' => Yii::t('common', 'Текст предупреждения'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(ReportValidateFinance::className(), ['id' => 'file_id']);
    }
}
