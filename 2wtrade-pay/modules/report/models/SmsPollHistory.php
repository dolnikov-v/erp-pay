<?php
namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\order\models\Order;
use Yii;

/**
 * NPAY-476 История отправок сис опроса
 * Class SmsPollHistory
 *
 * @package app\modules\catalog\models
 * @property integer $id
 * @property integer $order_id
 * @property integer $question_id
 * @property string $question_text
 * @property string $status
 * @property integer $api_code
 * @property string $api_error
 * @property integer $created_at
 * @property integer $updated_at
 */
class SmsPollHistory extends ActiveRecordLogUpdateTime
{
    const HISTORY_STATUS_NEW = 'new';
    const HISTORY_STATUS_READY = 'ready_to_sent';
    const HISTORY_STATUS_SUCCESS = 'success';
    const HISTORY_STATUS_ERROR = 'error';

    const ANSWER_TYPE_ALL = 0;
    const ANSWER_TYPE_NOT_ANSWERED = 1;
    const ANSWER_TYPE_ANSWERED = 2;
    const ANSWER_TYPE_READY_TO_SEND = 3;
    const ANSWER_TYPE_ERROR = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_poll_history}}';
    }

    /**
     * @return []
     */
    public static function getStatusesCollection()
    {
        return [
            self::HISTORY_STATUS_SUCCESS,
            self::HISTORY_STATUS_ERROR
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'question_id', 'question_text'], 'required'],
            ['order_id', 'integer'],
            ['question_id', 'integer'],
            ['question_text', 'string'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            ['api_code', 'integer'],
            ['api_error', 'string'],
            ['created_at', 'integer'],
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Номер заказ'),
            'question_id' => Yii::t('common', 'Номер вопроса'),
            'question_text' => Yii::t('common', 'Вопрос'),
            'status' => Yii::t('common', 'статус'),
            'api_code' => Yii::t('common', 'Код ответа API'),
            'api_error' => Yii::t('common', 'Текст ошибки API'),
            'answer_text' => Yii::t('common', 'Ответ'),
            'answered_at' => Yii::t('common', 'Дата ответа'),
            'created_at' => Yii::t('common','Дата отправки'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    public function getOrder(){
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}