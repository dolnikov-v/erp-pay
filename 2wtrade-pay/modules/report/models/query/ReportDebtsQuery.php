<?php

namespace app\modules\report\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\report\models\ReportDebts]].
 *
 * @see \app\modules\report\models\ReportDebts
 */
class ReportDebtsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\report\models\ReportDebts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\report\models\ReportDebts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
