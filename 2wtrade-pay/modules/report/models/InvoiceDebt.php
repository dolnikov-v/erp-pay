<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use Yii;

/**
 * This is the model class for table "invoice_debt".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property string $debt_month
 * @property integer $period_from
 * @property integer $period_to
 * @property double $buyout_local
 * @property double $buyout_usd
 * @property double $service_local
 * @property double $service_usd
 * @property double $total_local
 * @property double $total_usd
 * @property double $additional_charges_local
 * @property double $additional_charges_usd
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Invoice $invoice
 * @property Order[] $orders
 */
class InvoiceDebt extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoice_debt}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id'], 'required'],
            [['invoice_id', 'period_from', 'period_to', 'created_at', 'updated_at'], 'integer'],
            [
                [
                    'buyout_local',
                    'buyout_usd',
                    'service_local',
                    'service_usd',
                    'total_local',
                    'total_usd',
                    'additional_charges_local',
                    'additional_charges_usd'
                ],
                'number'
            ],
            [['debt_month'], 'safe'],
            [
                ['invoice_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Invoice::className(),
                'targetAttribute' => ['invoice_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'invoice_id' => Yii::t('common', 'Инвойс'),
            'debt_month' => Yii::t('common', 'Месяц дебиторской задолженность'),
            'period_from' => Yii::t('common', 'Начало периода'),
            'buyout_local' => Yii::t('common', 'Выкуп в местной валюте'),
            'buyout_usd' => Yii::t('common', 'Выкуп'),
            'service_local' => Yii::t('common', 'Цена услуг СЕРВИСа  в местной валюте'),
            'service_usd' => Yii::t('common', 'Цена услуг СЕРВИСа'),
            'total_local' => Yii::t('common', 'Итоговая сумма в местной валюте'),
            'total_usd' => Yii::t('common', 'Итоговая сумма'),
            'additional_charges_local' => Yii::t('common', 'Дополнительные расходы в местной валюте'),
            'additional_charges_usd' => Yii::t('common', 'Дополнительные расходы'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable('{{%invoice_order}}', ['invoice_debt_id' => 'id']);
    }
}