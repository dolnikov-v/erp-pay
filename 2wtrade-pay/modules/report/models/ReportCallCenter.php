<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use Yii;

/**
 * Class ReportCallCenter
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $type
 * @property integer $from
 * @property integer $to
 * @property integer $cc_post_call_count
 * @property integer $cc_approved_count
 * @property integer $cc_fail_call_count
 * @property integer $cc_recall_count
 * @property integer $cc_rejected_count
 * @property integer $cc_double_count
 * @property integer $cc_trash_count
 * @property integer $our_post_call_count
 * @property integer $our_approved_count
 * @property integer $our_fail_call_count
 * @property integer $our_recall_count
 * @property integer $our_rejected_count
 * @property integer $our_double_count
 * @property integer $our_trash_count
 * @property string $difference
 * @property integer $difference_count
 * @property integer $created_at
 * @property integer $updated_at
 *
 *
 * @property integer $differentProductCount
 * @property integer $differentProductQuantityCount
 * @property integer $differentProductPriceCount
 * @property integer $ccTotalCount
 * @property integer $ourTotalCount
 *
 * @property Country $country
 */
class ReportCallCenter extends ActiveRecordLogUpdateTime
{

    const TYPE_DAILY = 'daily';
    const TYPE_WEEKLY = 'weekly';
    const TYPE_MONTHLY = 'monthly';
    const TYPE_HOURLY = 'hourly';

    const SHOW_ERROR_CC_POST_CALL = 'cc_post_call';
    const SHOW_ERROR_CC_FAIL_CALL = 'cc_fail_call';
    const SHOW_ERROR_CC_RECALL = 'cc_recall';
    const SHOW_ERROR_CC_REJECTED = 'cc_rejected';
    const SHOW_ERROR_CC_DOUBLE = 'cc_double';
    const SHOW_ERROR_CC_TRASH = 'cc_trash';
    const SHOW_ERROR_CC_APPROVED = 'cc_approved';

    const SHOW_ERROR_OUR_POST_CALL = 'our_post_call';
    const SHOW_ERROR_OUR_FAIL_CALL = 'our_fail_call';
    const SHOW_ERROR_OUR_RECALL = 'our_recall';
    const SHOW_ERROR_OUR_REJECTED = 'our_rejected';
    const SHOW_ERROR_OUR_DOUBLE = 'our_double';
    const SHOW_ERROR_OUR_TRASH = 'our_trash';
    const SHOW_ERROR_OUR_APPROVED = 'our_approved';

    const SHOW_ERROR_NOT_FOUND_PRODUCTS = 'not_found_products';
    const SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY = 'not_equal_prod_quantity';
    const SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE = 'not_equal_prod_price';

    /**
     * @var array
     */
    protected $differenceArray;

    /**
     * @var integer
     */
    protected $ourTotalCount;

    /**
     * @var integer
     */
    protected $ccTotalCount;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_call_center}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'from', 'to'], 'required'],
            [
                [
                    'country_id',
                    'from',
                    'to',
                    'cc_post_call_count',
                    'cc_approved_count',
                    'cc_fail_call_count',
                    'cc_recall_count',
                    'cc_rejected_count',
                    'cc_double_count',
                    'cc_trash_count',
                    'our_post_call_count',
                    'our_approved_count',
                    'our_fail_call_count',
                    'our_recall_count',
                    'our_rejected_count',
                    'our_double_count',
                    'our_trash_count',
                    'difference_count',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [['type', 'difference'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'type' => Yii::t('common', 'Тип'),
            'from' => Yii::t('common', 'С'),
            'to' => Yii::t('common', 'По'),
            'cc_post_call_count' => Yii::t('common', 'На обзвоне (КЦ)'),
            'cc_approved_count' => Yii::t('common', 'Одобрен (КЦ)'),
            'cc_fail_call_count' => Yii::t('common', 'Недозвон (КЦ)'),
            'cc_recall_count' => Yii::t('common', 'Перезвон (КЦ)'),
            'cc_rejected_count' => Yii::t('common', 'Отклонен (КЦ)'),
            'cc_double_count' => Yii::t('common', 'Дубль (КЦ)'),
            'cc_trash_count' => Yii::t('common', 'Мусор (КЦ)'),
            'our_post_call_count' => Yii::t('common', 'На обзвоне'),
            'our_approved_count' => Yii::t('common', 'Одобрен'),
            'our_fail_call_count' => Yii::t('common', 'Недозвон'),
            'our_recall_count' => Yii::t('common', 'Перезвон'),
            'our_rejected_count' => Yii::t('common', 'Отклонен'),
            'our_double_count' => Yii::t('common', 'Дубль'),
            'our_trash_count' => Yii::t('common', 'Мусор'),
            'difference' => Yii::t('common', 'Различия'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'ccTotalCount' => Yii::t('common', 'Всего в КЦ'),
            'ourTotalCount' => Yii::t('common', 'Всего у нас'),
            'differentProductCount' => Yii::t('common', 'Не совпадают продукты'),
            'differentProductQuantityCount' => Yii::t('common', 'Не совпадает кол-во продуктов'),
            'differentProductPriceCount' => Yii::t('common', 'Не совпадает цена'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return array
     */
    public function getDifferenceArray()
    {
        if (is_null($this->differenceArray)) {
            $this->differenceArray = json_decode($this->difference, true);
            if (!is_array($this->differenceArray)) {
                $this->differenceArray = [];
            }
        }
        return $this->differenceArray;
    }

    /**
     * @return integer
     */
    public function getCcTotalCount()
    {
        if (is_null($this->ccTotalCount)) {
            $this->ccTotalCount = $this->cc_approved_count + $this->cc_double_count + $this->cc_fail_call_count + $this->cc_post_call_count + $this->cc_recall_count + $this->cc_rejected_count + $this->cc_trash_count;
        }
        return $this->ccTotalCount;
    }

    /**
     * @return integer
     */
    public function getOurTotalCount()
    {
        if (is_null($this->ourTotalCount)) {
            $this->ourTotalCount = $this->our_approved_count + $this->our_double_count + $this->our_fail_call_count + $this->our_post_call_count + $this->our_recall_count + $this->our_rejected_count + $this->our_trash_count;
        }
        return $this->ourTotalCount;
    }

    /**
     * @return integer
     */
    public function getDifferentProductCount()
    {
        $this->getDifferenceArray();
        if (isset($this->differenceArray[self::SHOW_ERROR_NOT_FOUND_PRODUCTS])) {
            return $this->differenceArray[self::SHOW_ERROR_NOT_FOUND_PRODUCTS];
        }
        return 0;
    }

    /**
     * @return integer
     */
    public function getDifferentProductQuantityCount()
    {
        if (isset($this->differenceArray[self::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY])) {
            return $this->differenceArray[self::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY];
        }
        return 0;
    }

    /**
     * @return integer
     */
    public function getDifferentProductPriceCount()
    {
        $this->getDifferenceArray();
        if (isset($this->differenceArray[self::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE])) {
            return $this->differenceArray[self::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE];
        }
        return 0;
    }
}
