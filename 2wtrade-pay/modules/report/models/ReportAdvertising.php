<?php

namespace app\modules\report\models;

use Yii;

/**
 * This is the model class for table "report_advertising".
 *
 * @property integer $id
 * @property integer $foreign_id
 * @property string $foreign_id_file
 * @property string $country_code
 * @property string $country_code_file
 * @property integer $created_at
 * @property string $created_at_file
 * @property string $status_id
 * @property string $status_id_file
 * @property double $price
 * @property string $price_file
 * @property string $difference
 * @property integer $error
 * @property integer $day
 */
class ReportAdvertising extends \yii\db\ActiveRecord
{
    use ModelTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_advertising}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['error', 'day'], 'integer'],
            [['foreign_id', 'created_at', 'foreign_id_file', 'created_at_file'], 'string', 'max' => 11, 'message' => Yii::t('common', 'Длина поля Внешний id, Дата создания 11 символов')],
            [['country_code', 'country_code_file'], 'string', 'max' => 3, 'message' => Yii::t('common', 'Длина поля Код страны 3 символа')],
            [['status_id', 'status_id_file'], 'string', 'max' => 5, 'message' => Yii::t('common', 'Длина поля Статус 5 символов')],
            [['price_file', 'difference', 'price'], 'string', 'max' => 50, 'message' => Yii::t('common', 'Длина поля Цена, Разница 50 символов')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'foreign_id' => Yii::t('common', 'Внешний id'),
            'foreign_id_file' => Yii::t('common', 'Внешний id (файл)'),
            'country_code' => Yii::t('common', 'Код страны'),
            'country_code_file' => Yii::t('common', 'Код страны (файл)'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'created_at_file' => Yii::t('common', 'Дата создания (файл)'),
            'status_id' => Yii::t('common', 'Статус'),
            'status_id_file' => Yii::t('common', 'Статус (файл)'),
            'price' => Yii::t('common', 'Цена'),
            'price_file' => Yii::t('common', 'Цена (файл)'),
            'difference' => Yii::t('common', 'Разница'),
            'error' => Yii::t('common', 'Ошибка'),
            'day' => Yii::t('common', 'Дата загрузки'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\modules\report\models\query\ReportAdvertisingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\report\models\query\ReportAdvertisingQuery(get_called_class());
    }
}
