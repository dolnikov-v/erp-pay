<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\report\models\Invoice;
use app\modules\order\models\Order;
use Yii;

/**
 * This is the model class for table "{{%invoice_order}}".
 *
 * @property integer $invoice_id
 * @property integer $order_id
 * @property integer $invoice_debt_id
 *
 * @property Invoice $invoice
 * @property InvoiceDebt $invoiceDebt
 * @property Order $order
 */
class InvoiceOrder extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoice_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'order_id'], 'required'],
            [['invoice_id', 'order_id', 'invoice_debt_id'], 'integer'],
            [
                ['invoice_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Invoice::className(),
                'targetAttribute' => ['invoice_id' => 'id']
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('common', 'Номер инвойса'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'invoice_debt_id' => Yii::t('common', 'Мясяц дебиторской задолжности'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceDebt()
    {
        return $this->hasOne(InvoiceDebt::className(), ['id' => 'invoice_debt_id']);
    }
}
