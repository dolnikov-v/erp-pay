<?php
namespace app\modules\report\models;

use app\modules\report\models\query\ReportOrderQuery;
use yii\db\ActiveRecord;

/**
 * Class ReportOrder
 * @package app\modules\report\models
 * @property integer $id
 * @property integer $order_id
 * @property integer $report_id
 * @property string $type
 * @property integer $updated_at
 * @property integer $created_at
 */
class ReportOrder extends ActiveRecord
{
    const TYPE_DEBTS = 'debts';
    const TYPE_POTENTIAL_REVENUE = 'potential_revenue';
    const TYPE_REVENUE = 'revenue';
    const TYPE_REFUND = 'refund';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_order}}';
    }

    /**
     * @return ReportOrderQuery
     */
    public static function find()
    {
        return new ReportOrderQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_DEBTS => 'debts',
            self::TYPE_POTENTIAL_REVENUE => 'potential_revenue',
            self::TYPE_REVENUE => 'revenue',
            self::TYPE_REFUND => 'refund',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'report_id', 'type'], 'required'],
            [['order_id', 'report_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'report_id' => 'Report ID',
            'type' => 'Type',
        ];
    }
}
