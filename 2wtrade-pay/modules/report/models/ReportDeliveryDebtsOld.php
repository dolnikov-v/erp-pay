<?php

namespace app\modules\report\models;

use app\modules\delivery\models\Delivery;
use app\modules\report\models\query\ReportDeliveryDebtsOldQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * ReportDeliveryDebtsOld.
 *
 * @property integer $delivery_id
 * @property string $month
 * @property string $data
 * @property integer $created_at
 * @property Delivery $delivery
 * @property array $decodedData
 */
class ReportDeliveryDebtsOld extends ActiveRecord
{
    protected $decodedData = null;

    /**
     * @return ReportDeliveryDebtsOldQuery
     */
    public static function find()
    {
        return new ReportDeliveryDebtsOldQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_delivery_debts_old}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'month'], 'required'],
            [['month', 'data'], 'string'],
            [['delivery_id', 'created_at'], 'integer'],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', '#'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'month' => Yii::t('common', 'Месяц'),
            'created_at' => Yii::t('common', 'Дата добавления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return array
     */
    public function getDecodedData()
    {
        if (is_null($this->decodedData)) {
            $this->decodedData = json_decode($this->data, true);
        }
        return $this->decodedData;
    }

    /**
     * @param string $value
     */
    public function setDecodedData($value)
    {
        $this->decodedData = $value;
        $this->data = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
