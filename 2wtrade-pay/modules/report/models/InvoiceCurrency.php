<?php

namespace app\modules\report\models;

use app\models\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%invoice_currency}}".
 *
 * @property integer $invoice_id
 * @property integer $currency_id
 * @property integer $invoice_currency_id
 * @property float $rate
 *
 * @property Currency $currency
 * @property Invoice $invoice
 * @property Currency $invoiceCurrency
 */
class InvoiceCurrency extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoice_currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'currency_id', 'invoice_currency_id'], 'required'],
            [['invoice_id', 'currency_id', 'invoice_currency_id'], 'integer'],
            [['rate'], 'number'],
            [['currency_id', 'invoice_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('common', 'Invoice ID'),
            'currency_id' => Yii::t('common', 'Currency ID'),
            'rate' => Yii::t('common', 'Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'invoice_currency_id']);
    }
}
