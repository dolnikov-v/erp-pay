<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\Currency;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\invoice\generators\InvoiceFinanceBuilder;
use app\modules\report\components\invoice\generators\InvoiceTableBuilder;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\widgets\Label;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\swiftmailer\Message as SwiftMessage;
use yii\validators\EmailValidator;

/**
 * This is the model class for table "{{%invoice}}".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $contract_id
 * @property integer $currency_id
 * @property string $name
 * @property double $total
 * @property double $additional_charges
 * @property integer $period_from
 * @property integer $period_to
 * @property integer $payment_date
 * @property string $status
 * @property integer $round_precision
 * @property integer $user_id
 * @property string $invoice_filename
 * @property string $orders_filename
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $requisite_delivery_id
 *
 * @property Delivery $delivery
 * @property User $user
 * @property InvoiceOrder[] $invoiceOrders
 * @property Order[] $orders
 * @property DeliveryContract $contract
 * @property string $statusLabel
 * @property Currency[] $currencies
 * @property InvoiceCurrency[] $currencyRates
 * @property InvoicePayment[] $invoicePayments
 * @property PaymentOrder[] $payments
 * @property InvoiceDebt[] $invoiceDebts
 * @property Currency $currency
 * @property CurrencyRate $currencyRate
 */
class Invoice extends ActiveRecordLogUpdateTime
{
    public $sumTotalByOrders = 0;
    /**
     * @var null
     */
    public $sumTotalReceived = 0;

    const STATUS_QUEUE = 'queue';
    const STATUS_GENERATING = 'generating';
    const STATUS_GENERATED = 'generated';
    const STATUS_DONE = 'done';
    const STATUS_CLOSING = 'closing';
    const STATUS_APPROVED = 'approved';

    const PATH_UPLOAD = 'invoices';

    const FILE_INVOICE = 'file_invoice';
    const FILE_ORDERS = 'file_orders';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'period_from', 'period_to', 'requisite_delivery_id', 'currency_id'], 'required'],
            [
                [
                    'delivery_id',
                    'period_from',
                    'period_to',
                    'user_id',
                    'created_at',
                    'updated_at',
                    'contract_id',
                    'payment_date',
                    'round_precision',
                    'requisite_delivery_id',
                    'currency_id',
                ],
                'integer'
            ],
            [['total', 'additional_charges'], 'number'],
            ['status', 'default', 'value' => 'queue'],
            [['name', 'status', 'invoice_filename', 'orders_filename'], 'string', 'max' => 255],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'name' => Yii::t('common', 'Инвойс ID'),
            'period_from' => Yii::t('common', 'Начало периода'),
            'period_to' => Yii::t('common', 'Окончание периода'),
            'status' => Yii::t('common', 'Статус'),
            'statusLabel' => Yii::t('common', 'Статус'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'payment_date' => Yii::t('common', 'Дата платежа'),
            'round_precision' => Yii::t('common', 'Точность округления'),
            'requisite_delivery_id' => Yii::t('common', 'Реквизит КС'),
            'currency_id' => Yii::t('common', 'Валюта инвойса'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceOrders()
    {
        return $this->hasMany(InvoiceOrder::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable('{{%invoice_order}}', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFinancePredictions()
    {
        return $this->hasMany(OrderFinancePrediction::className(), ['order_id' => 'order_id'])
            ->viaTable('{{%invoice_order}}', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(DeliveryContract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceCurrencies()
    {
        return $this->hasMany(InvoiceCurrency::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['id' => 'currency_id'])->via('invoiceCurrencies');
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrencyRates()
    {
        return $this->hasMany(InvoiceCurrency::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInvoicePayments()
    {
        return $this->hasMany(InvoicePayment::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(PaymentOrder::className(), ['id' => 'payment_id'])->via('invoicePayments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceDebts()
    {
        return $this->hasMany(InvoiceDebt::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_QUEUE => Yii::t('common', 'Ожидает генерации'),
            self::STATUS_GENERATING => Yii::t('common', 'Идет генерация'),
            self::STATUS_GENERATED => Yii::t('common', 'Сгенерирован'),
            self::STATUS_DONE => Yii::t('common', 'Закрыт'),
            self::STATUS_CLOSING => Yii::t('common', 'Идет закрытие'),
            self::STATUS_APPROVED => Yii::t('common', 'Согласован'),
        ];
    }

    /**
     * @return array
     */
    public function getStatusLabelsStyles()
    {
        return [
            self::STATUS_QUEUE => Label::STYLE_PRIMARY,
            self::STATUS_DONE => Label::STYLE_DEFAULT,
            self::STATUS_APPROVED => Label::STYLE_SUCCESS,
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatusLabel()
    {
        $labels = self::getStatusLabels();

        return $labels[$this->status] ?? $this->status;
    }

    /**
     * @return mixed|string
     */
    public function getStatusLabelStyle()
    {
        $styles = $this->getStatusLabelsStyles();

        return $styles[$this->status] ?? Label::STYLE_PRIMARY;
    }

    /**
     * @param string $fileName
     * @param bool $prepareDir
     * @return string
     */
    public static function getUploadPath($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . self::PATH_UPLOAD . DIRECTORY_SEPARATOR;
        Utils::prepareDir($dir, 0777);

        $dir .= $subDir;
        if ($prepareDir) {
            Utils::prepareDir($dir, 0777);
        }
        return $dir;
    }

    /**
     * @return array
     */
    public static function getFileTypes()
    {
        return [
            self::FILE_INVOICE => 'invoice_filename',
            self::FILE_ORDERS => 'orders_filename',
        ];
    }

    /**
     * @param array $data
     * @param boolean $throughQueue
     * @throws \Exception
     * @throws \Throwable
     * @return boolean
     */
    public function close($data, $throughQueue = false)
    {
        if ($this->getOrders()->andWhere([
            'not in',
            Order::tableName() . '.status_id',
            array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList())
        ])->exists()
        ) {
            throw new \Exception(Yii::t('common', 'Невозможно закрыть инвойс, имеются заказы в несоответствующих статусах.'));
        }

        if ($throughQueue) {
            $client = $this->getSqsClient();
            $sendData = [
                'QueueUrl' => $client->getSqsQueueUrl('close_invoice'),
                'MessageGroupId' => 'closeInvoice',
                'MessageBody' => json_encode([
                    'invoice_id' => $this->id,
                    'data' => $data,
                ], JSON_UNESCAPED_UNICODE),
            ];
            $this->status = self::STATUS_CLOSING;
            $client->sendMessage($sendData);
            if (!$this->save()) {
                throw new \Exception($this->getFirstErrorAsString());
            }
            return true;
        }

        $statusMap = [
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED => OrderStatus::getBuyoutList(),
            OrderStatus::STATUS_DELIVERY_RETURNED => OrderStatus::getOnlyNotBuyoutInDeliveryList(),
        ];

        $transaction = Yii::$app->db->beginTransaction();
        try {

            foreach ($this->currencyRates as $currencyRate) {
                if (isset($data[$currencyRate->formName()][$currencyRate->currency_id])) {
                    $currencyRate->rate = $data[$currencyRate->formName()][$currencyRate->currency_id]['rate'];
                    $currencyRate->save();
                }
            }

            $this->recalculateTotals();

            $payment = new PaymentOrder();
            $payment->load($data);
            $payment->paid_at = $payment->paid_at ? Yii::$app->formatter->asTimestamp($payment->paid_at) : null;
            if (!$payment->save()) {
                throw new \Exception($payment->getFirstErrorAsString());
            }
            $invoicePayment = new InvoicePayment([
                'invoice_id' => $this->id,
                'payment_id' => $payment->id
            ]);
            if (!$invoicePayment->save()) {
                throw new \Exception($invoicePayment->getFirstErrorAsString());
            }

            foreach ($statusMap as $statusId => $statuses) {
                $query = $this->getOrders()->andWhere([Order::tableName() . '.status_id' => $statuses]);
                $query->with(['deliveryRequest', 'finance']);

                foreach ($query->batch(500) as $orders) {
                    /**
                     * @var Order[] $orders
                     */
                    foreach ($orders as $order) {
                        $order->status_id = $statusId;
                        $order->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                        /** @var OrderFinanceFact $finance */
                        $finance = $order->financeFact;
                        //претензионный факт не в счёт
                        //претензии в планах перенестив отдельную таблицу

                        //Убрали создание фактов в инвойсах
                        /*
                        if (!$finance || !is_null($finance->pretension)) {
                            $finance = new OrderFinanceFact([
                                'order_id' => $order->id
                            ]);
                        }*/

                        $finance->payment_id = $payment->id;

                        $financePrediction = $order->deliveryRequest->chargesCalculator->calculate($order);

                        if ($financePrediction) {
                            foreach ($financePrediction->attributes as $key => $val) {
                                if (!in_array($key, ['id', 'created_at', 'updated_at'])) {
                                    $finance->$key = $val;
                                }
                            }
                        }
                        else {
                            if (in_array($order->status_id, OrderStatus::getBuyoutList())) {
                                $finance->price_cod = $order->price_total + $order->delivery;
                            } else {
                                $finance->price_cod = 0;
                            }
                            foreach (OrderFinancePrediction::getCurrencyFields() as $key => $val) {
                                $finance->$val = $order->country->currency_id;
                            }
                        }

                        if (!$finance->save()) {
                            throw new \Exception($finance->getFirstErrorAsString());
                        }
                        if (!$order->save(false)) {
                            throw new \Exception($order->getFirstErrorAsString());
                        }
                        if (!$order->deliveryRequest->save()) {
                            throw new \Exception($order->deliveryRequest->getFirstErrorAsString());
                        }
                    }
                }
            }

            $this->status = self::STATUS_DONE;
            if (!$this->save()) {
                throw new \Exception($this->getFirstErrorAsString());
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @param string|string[] $email
     * @param bool $sendInvoiceFile
     * @param bool $sendOrdersFile
     * @throws \Exception
     */
    public function sendToEmail($email, $sendInvoiceFile = true, $sendOrdersFile = true)
    {
        if (!is_array($email)) {
            $email = [$email];
        }

        if (!$sendInvoiceFile && !$sendOrdersFile) {
            throw new \Exception(Yii::t('common', 'Не выбраны файлы для отправки.'));
        }
        $message = new SwiftMessage();
        $message->setFrom(Yii::$app->params['infoMailAddress']);
        $period = null;
        $dateFormat = 'd/m/Y';
        if ($this->period_from && $this->period_to) {
            $period = date($dateFormat, $this->period_from) . ' - ' . date($dateFormat, $this->period_to);
        } elseif ($this->period_from) {
            $period = "after " . date($dateFormat, $this->period_from);
        } elseif ($this->period_to) {
            $period = 'before ' . date($dateFormat, $this->period_to);
        }
        $message->setSubject("2WTrade invoice" . ($period ? " " . $period : ''));

        if ($sendOrdersFile) {
            $path = rtrim(trim(static::getUploadPath($this->orders_filename)), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->orders_filename;
            if (!file_exists($path)) {
                throw new \Exception(Yii::t('common', 'Таблица с информацией о заказах не была сгенерирована.'));
            }
            $info = pathinfo($this->orders_filename);
            $message->attach($path, [
                'fileName' => $this->name . '.' . (!empty($info['extension']) ? '.' . $info['extension'] : '')
            ]);
        }

        if ($sendInvoiceFile) {
            $path = rtrim(trim(static::getUploadPath($this->invoice_filename)), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->invoice_filename;
            if (!file_exists($path)) {
                throw new \Exception(Yii::t('common', 'Файл с информацией о инвойсе не был сгенерирован.'));
            }
            $info = pathinfo($this->invoice_filename);
            $message->attach($path, [
                'fileName' => $this->name . '.' . (!empty($info['extension']) ? '.' . $info['extension'] : '')
            ]);
        }

        foreach ($email as $emailAddress) {
            $validator = new EmailValidator();
            if (!$validator->validate($emailAddress)) {
                throw new \Exception(Yii::t('common', 'Указан некорректный e-mail адрес "{address}"', ['address' => $emailAddress]));
            }
            $message->setTo($emailAddress);

            if (!$message->send()) {
                throw new \Exception(Yii::t('common', 'Не удалось отправить письмо на адрес {address}', ['address' => $emailAddress]));
            }
        }
    }

    /**
     * Получаем сводную фин. информацию об инвойсе
     * В ответ:
     * [
     *  'rows' => [
     *      [
     *          'local_sum' => double,
     *          'usd_sum' => double,
     *          'label' => string,
     *          'quantity' => integer|double,
     *          'formula' => string|double|integer,
     *          'finance_type' => OrderFinancePrediction::FINANCE_TYPE_...,
     *          'calculating_type' => ChargesCalculatorAbstract::CALCULATING_TYPE_....
     *      ],
     *  ],
     *  'totalData' => [
     *      'total_sum' => double,
     *      'total_sum_usd' => double,
     *      'orders_count' => integer,
     *      'total_cod_sum' => double,
     *  ],
     *  'currencies' => [
     *      1,2,4,5
     *  ]
     * ]
     * @return array
     * @throws \Exception
     */
    public function getFinanceData()
    {
        $query = Invoice::find();
        $query->joinWith(['orders.financePrediction']);
        $query->where([Invoice::tableName() . '.id' => $this->id]);
        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $currencyRateRelations = OrderFinancePrediction::currencyRateRelationMap();
        if (!$this->contract) {
            throw new \Exception(\Yii::t('common', 'Не указан контракт'));
        }
        foreach (array_keys($currencyFields) as $key) {
            $query->leftJoin(InvoiceCurrency::tableName() . " as {$currencyRateRelations[$currencyFields[$key]]}_temp", "{$currencyRateRelations[$currencyFields[$key]]}_temp.invoice_id = {$this->id} and {$currencyRateRelations[$currencyFields[$key]]}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]}");
        }

        $months = ReportFormInvoice::getMonthsWithoutInvoices($this->delivery_id, $this->period_from, $this->period_to, $this->id);
        $currencyRates = ArrayHelper::map($this->currencyRates, 'currency_id', 'rate');
        return ReportFormInvoice::prepareRows($query, $this->contract, $this->currency_id, $currencyRates[$this->delivery->country->currency_id] ?? $this->delivery->country->currencyRate->rate / $this->currencyRate->rate, $currencyRates, true, count($months));
    }


    /**
     * Получаем сводную фин. информацию об инвойсе по месяцам
     * @return array
     * @throws \Exception
     */
    public function getFinanceDataDebt()
    {
        if (!$this->contract) {
            throw new \Exception(\Yii::t('common', 'Не указан контракт'));
        }
        $data = [];
        foreach ($this->invoiceDebts as $invoiceDebt) {
            $query = InvoiceDebt::find();
            $query->joinWith(['orders.financePrediction']);
            $query->where([InvoiceDebt::tableName() . '.id' => $invoiceDebt->id]);
            $currencyFields = OrderFinancePrediction::getCurrencyFields();
            $currencyRateRelations = OrderFinancePrediction::currencyRateRelationMap();
            foreach (array_keys($currencyFields) as $key) {
                $query->leftJoin(InvoiceCurrency::tableName() . " as {$currencyRateRelations[$currencyFields[$key]]}_temp", "{$currencyRateRelations[$currencyFields[$key]]}_temp.invoice_id = {$this->id} and {$currencyRateRelations[$currencyFields[$key]]}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]}");
            }

            $months = ReportFormInvoice::getMonthsWithoutInvoices($this->delivery_id, $invoiceDebt->period_from, $invoiceDebt->period_to, $this->id);
            $currencyRates = ArrayHelper::map($this->currencyRates, 'currency_id', 'rate');
            $monthData = ReportFormInvoice::prepareRows($query, $this->contract, $this->currency_id, $currencyRates[$this->delivery->country->currency_id] ?? $this->delivery->country->currencyRate->rate / $this->currencyRate->rate, $currencyRates, true, count($months));

            $usdCurrencyId = Currency::find()->where(['char_code' => 'USD'])->select('id')->scalar();
            $defaultCurrencyRate = (float)(!empty($currencyRates[$usdCurrencyId]) ? 1 / $currencyRates[$usdCurrencyId] : $this->currencyRate->rate);
            $invoiceDebt->total_usd = $monthData['totalData']['total_sum_usd'] / $defaultCurrencyRate;
            $invoiceDebt->total_local = 0;
            $invoiceDebt->additional_charges_usd = 0;
            $invoiceDebt->additional_charges_local = 0;
            $invoiceDebt->service_usd = 0;
            $invoiceDebt->service_local = 0;
            $invoiceDebt->buyout_local = 0;
            $invoiceDebt->buyout_usd = 0;
            foreach ($monthData['rows'] as $key => $values) {

                if ($key == 'price_cod') {
                    $invoiceDebt->buyout_usd += $values['usd_sum'] / $defaultCurrencyRate;
                    $invoiceDebt->buyout_local += $values['local_sum'];
                }
                else {
                    $invoiceDebt->service_usd += $values['usd_sum'] / $defaultCurrencyRate;
                    $invoiceDebt->service_local += $values['local_sum'];
                }

                if ($values['calculating_type'] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                    $invoiceDebt->additional_charges_usd += $values['usd_sum'] / $defaultCurrencyRate;
                    $invoiceDebt->additional_charges_local += $values['local_sum'];
                }
            }

            $invoiceDebt->total_local = $invoiceDebt->buyout_local - $invoiceDebt->service_local;

            if (!$invoiceDebt->save(true, [
                'total_usd',
                'total_local',
                'additional_charges_usd',
                'additional_charges_local',
                'service_usd',
                'service_local',
                'buyout_local',
                'buyout_usd',
            ])
            ) {
                throw new Exception($invoiceDebt->getFirstErrorAsString());
            }

            $monthData['totalData']['additional_charges'] = $invoiceDebt->additional_charges_usd;
            $data[] = $monthData;

        }
        return $data;
    }

    /***
     * Проверим, есть ли записи, если нет создать нужно
     * @param bool $check
     * @return array
     * @throws Exception
     */
    public function checkInvoiceDebts($check = false)
    {
        $monthOfDebts = [];
        if ($check) {
            $monthOfDebts = ArrayHelper::map(InvoiceDebt::find()
                ->select(['month' => 'DATE_FORMAT(debt_month, "%Y-%m")', 'id' => 'id'])
                ->where(['invoice_id' => $this->id])
                ->asArray()
                ->all(), 'month', 'id');
            if ($monthOfDebts) {
                return $monthOfDebts;
            }
        }

        $monthFrom = date('Y-m-01', $this->period_from);
        $monthTo = date('Y-m-01', $this->period_to);

        while (strtotime($monthFrom) <= strtotime($monthTo)) {

            $invoiceDebt = new InvoiceDebt();
            $invoiceDebt->invoice_id = $this->id;
            $invoiceDebt->debt_month = $monthFrom;
            $invoiceDebt->period_from = strtotime($monthFrom);
            $invoiceDebt->period_to = min((strtotime("+1 month", strtotime($monthFrom)) - 1), $this->period_to);

            if (!$invoiceDebt->save()) {
                throw new Exception($invoiceDebt->getFirstErrorAsString());
            }
            $monthOfDebts[date("Y-m", strtotime($monthFrom))] = $invoiceDebt->id;
            $monthFrom = date("Y-m-01", strtotime("+1 month", strtotime($monthFrom)));
        }
        return $monthOfDebts;
    }

    /**
     * Пересчет конечной суммы инвойса и доп. расходов по месяцам
     * @throws Exception
     * @throws \Exception
     */
    public function recalculateTotalsDebts()
    {
        $this->checkInvoiceDebts(true);
        $this->getFinanceDataDebt();
    }

    /**
     * Пересчет конечной суммы инвойса и доп. расходов
     * @throws \Exception
     */
    public function recalculateTotals()
    {
        $data = $this->getFinanceData();
        $this->total = $data['totalData']['total_sum_usd'];
        $this->additional_charges = 0;
        foreach ($data['rows'] as $values) {
            switch ($values['calculating_type']) {
                case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                    $this->additional_charges += $values['usd_sum'];
                    break;
            }
        }
    }

    /**
     * @param Invoice $invoice
     */
    public static function generateInvoiceByRequisite($invoice)
    {
        $invoice->status = Invoice::STATUS_GENERATING;
        $invoice->save(true, ['status']);
        try {
            $pdfGenerator = new InvoiceFinanceBuilder($invoice);
            $excelGenerator = new InvoiceTableBuilder($invoice);
            $invoice->invoice_filename = $pdfGenerator->generate();
            $invoice->orders_filename = $excelGenerator->generate();
            $invoice->status = Invoice::STATUS_GENERATED;
            if (!$invoice->save(true, ['invoice_filename', 'orders_filename', 'status'])) {
                throw new \Exception($invoice->getFirstErrorAsString());
            };
        } catch (\Throwable $e) {
            $invoice->status = Invoice::STATUS_QUEUE;
            $invoice->save(true, ['status']);

            echo $e->getMessage();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisiteDelivery()
    {
        return $this->hasOne(RequisiteDelivery::className(), ['id' => 'requisite_delivery_id']);
    }

    public function delete()
    {
        if ($this->status === self::STATUS_APPROVED) {
            $this->addError('status', Yii::t('common', 'Инвойс в текущем статусе не может быть удален.'));
            return false;
        } else {
            return parent::delete();
        }
    }
}
