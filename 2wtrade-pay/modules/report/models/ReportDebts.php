<?php
namespace app\modules\report\models;

use app\modules\report\models\query\ReportDebtsQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\delivery\models\Delivery;

/**
 * Class ReportDebts
 * @package app\modules\report\models
 * @property integer $id
 * @property string $month_year
 * @property integer $day
 * @property string $debts
 * @property string $potential_revenue
 * @property string $revenue
 * @property string $refund
 * @property integer $delivery_id
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ReportDebts extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_debts}}';
    }

    /**
     * @return ReportDebtsQuery
     */
    public static function find()
    {
        return new ReportDebtsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month_year', 'day'], 'required'],
            [['month_year'], 'safe'],
            [['day', 'delivery_id', 'country_id'], 'integer'],
            [['debts', 'potential_revenue', 'revenue', 'refund'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month_year' => 'Month Year',
            'day' => 'Day',
            'debts' => 'Debts',
            'potential_revenue' => 'Potential Revenue',
            'revenue' => 'Revenue',
            'refund' => 'Refund',
            'delivery_id' => 'Delivery ID',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
