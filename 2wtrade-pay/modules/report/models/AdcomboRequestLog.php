<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class AdcomboRequestLog
 *
 * @property integer $id
 * @property integer $adcombo_request_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AdcomboRequest $adcomboRequest
 */
class AdcomboRequestLog extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%adcombo_request_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adcombo_request_id'], 'required'],
            [['adcombo_request_id', 'created_at', 'updated_at'], 'integer'],
            [['field', 'old', 'new'], 'string', 'max' => 255],
            [
                ['adcombo_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => AdcomboRequest::className(),
                'targetAttribute' => ['adcombo_request_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'adcombo_request_id' => Yii::t('common', 'Adcombo Request ID'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdcomboRequest()
    {
        return $this->hasOne(AdcomboRequest::className(), ['id' => 'adcombo_request_id']);
    }
}
