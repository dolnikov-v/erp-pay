<?php

namespace app\modules\report\models;

use app\components\ModelTrait;
use app\modules\delivery\models\Delivery;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\models\query\ReportDeliveryDebtsQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * ReportDeliveryDebts.
 *
 * @property integer $delivery_id
 * @property string $month
 * @property string $data
 * @property integer $created_at
 * @property integer $is_old_data
 * @property Delivery $delivery
 * @property array $decodedData
 */
class ReportDeliveryDebts extends ActiveRecord
{
    use ModelTrait;

    protected $decodedData = null;

    /**
     * @return ReportDeliveryDebtsQuery
     */
    public static function find()
    {
        return new ReportDeliveryDebtsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_delivery_debts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'month'], 'required'],
            [['month', 'data'], 'string'],
            [['delivery_id', 'created_at', 'is_old_data'], 'integer'],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', '#'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'month' => Yii::t('common', 'Месяц'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'is_old_data' => Yii::t('common', 'Данные из старых источников'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return array
     */
    public function getDecodedData()
    {
        if (is_null($this->decodedData)) {
            $this->decodedData = json_decode($this->data, true);
        }
        return $this->decodedData;
    }

    /**
     * @param string $value
     */
    public function setDecodedData($value)
    {
        $this->decodedData = $value;
        $this->data = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Получить сумму в колонке Денег не получено от выкупленных заказов по КС и дней назад
     * @param $deliveryId
     * @param $days
     * @return double
     */
    public static function getEndSumByDelivery($deliveryId, $days)
    {
        if (!$deliveryId) {
            return 0;
        }
        $debts = ReportDeliveryDebts::find()
            ->where(['delivery_id' => $deliveryId])
            ->andWhere([
                'between',
                'month',
                ReportFormDeliveryDebts::DATE_FROM_DEBTS,
                date('Y-m-d', strtotime('first day of this month', strtotime('- ' . $days . ' days')))
            ])
            ->all();

        $endSumDollars = 0;
        if ($debts) {
            foreach ($debts as $monthDebt) {
                $data = json_decode($monthDebt->data, true);
                $endSumDollars += ($data['buyout_sum_total_dollars'] - $data['charges_not_accepted_dollars']);
            }
        }
        return $endSumDollars;
    }
}
