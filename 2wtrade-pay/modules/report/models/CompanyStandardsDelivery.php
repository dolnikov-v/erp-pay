<?php

namespace app\modules\report\models;

use app\models\Country;
use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\Delivery;
use Yii;

/**
 * Class CompanyStandardsDelivery
 *
 * @property string $month
 * @property integer $country_id
 * @property integer $delivery_id
 * @property float $accepted_time
 * @property float $payment_time
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 */
class CompanyStandardsDelivery extends ActiveRecordLogUpdateTime
{
    const PRECISION = 2;

    const NORM_ACCEPTED_TIME = 12; //час
    const NORM_PAYMENT_TIME = 7; //день

    /**
     * @var array
     */
    protected $_errorArray;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%report_company_standards_by_delivery}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'month', 'delivery_id'], 'required'],
            ['month', 'string'],
            [['accepted_time','payment_time'], 'double'],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            [['country_id', 'delivery_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'month' => Yii::t('common', 'Месяц'),
            'country_id' => Yii::t('common', 'Страна'),
            'delivery_id' => Yii::t('common', 'Создание лида'),
            'accepted_time' => Yii::t('common', 'Время начала доставки, норма ' . static::NORM_ACCEPTED_TIME . ' часов'),
            'payment_time' => Yii::t('common', 'Поступление ДС, норма ' . static::NORM_PAYMENT_TIME . ' дней'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @param string $attribute
     * @return string
     */
    public static function getLabel(string $attribute): string
    {
        return (new static())->attributeLabels()[$attribute] ?? $attribute;
    }
}
