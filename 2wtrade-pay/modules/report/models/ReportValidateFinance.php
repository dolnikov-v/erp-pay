<?php

namespace app\modules\report\models;

use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "report_validate_finance".
 *
 * @property integer $id
 * @property string $file_name
 * @property integer $row_begin
 * @property integer $cell_order_id
 * @property integer $cell_tracking
 * @property integer $cell_delivery_cost_percent
 * @property integer $cell_fulfillment_cost
 * @property integer $cell_nds
 * @property integer $cell_fulfillment_nds
 * @property integer $cell_delivery_price
 * @property integer $cell_total_price
 * @property integer $cell_status
 * @property string $status_map
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ReportValidateFinanceData[] $reportValidateFinanceData
 */
class ReportValidateFinance extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_validate_finance}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name'], 'required'],
            [['row_begin', 'cell_order_id', 'cell_tracking', 'cell_delivery_cost_percent', 'cell_fulfillment_cost', 'cell_nds', 'cell_fulfillment_nds', 'cell_delivery_price', 'cell_total_price', 'cell_status', 'created_at', 'updated_at'], 'integer'],
            [['status_map'], 'string'],
            [['file_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'file_name' => Yii::t('common', 'Имя файла'),
            'row_begin' => Yii::t('common', 'Строка начала данных в файле'),
            'cell_order_id' => Yii::t('common', 'Номер заказа: столбец в файле'),
            'cell_tracking' => Yii::t('common', 'Трекер: столбец в файле'),
            'cell_delivery_cost_percent' => Yii::t('common', '% от заказов службе доставки: столбец в файле'),
            'cell_fulfillment_cost' => Yii::t('common', 'Стоимость фулфиллмента: столбец в файле'),
            'cell_nds' => Yii::t('common', 'НДС на доставку: столбец в файле'),
            'cell_fulfillment_nds' => Yii::t('common', 'НДС на фулфиллмент: столбец в файле'),
            'cell_delivery_price' => Yii::t('common', 'Стоимость доставки: столбец в файле'),
            'cell_total_price' => Yii::t('common', 'Общая сумма: столбец в файле'),
            'cell_status' => Yii::t('common', 'Статус: столбец в файле'),
            'status_map' => Yii::t('common', 'Маппинг статусов'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportValidateFinanceData()
    {
        return $this->hasMany(ReportValidateFinanceData::className(), ['file_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\modules\report\models\query\ReportValidateFinanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\report\models\query\ReportValidateFinanceQuery(get_called_class());
    }
}