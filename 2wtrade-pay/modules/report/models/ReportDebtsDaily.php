<?php

namespace app\modules\report\models;

use app\components\db\ActiveRecord;
use app\modules\delivery\models\Delivery;
use Yii;

/**
 *
 * @property integer $delivery_id
 * @property string $date
 * @property float $amount
 * @property integer $created_at
 *
 * @property Delivery $delivery
 */
class ReportDebtsDaily extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_debts_daily}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'date'], 'required'],
            [['delivery_id', 'created_at'], 'integer'],
            [['date'], 'safe'],
            [['amount'], 'number'],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'date' => Yii::t('common', 'Дата'),
            'amount' => Yii::t('common', 'Долг'),
            'created_at' => Yii::t('common', 'Дата создания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}