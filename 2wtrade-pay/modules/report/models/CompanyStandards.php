<?php

namespace app\modules\report\models;

use app\models\Country;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class CompanyStandards
 *
 * @property string $month
 * @property integer $country_id
 * @property float $admission_lead
 * @property float $first_sms
 * @property float $send_to_delivery
 * @property float $first_call
 * @property integer $try_call_count
 * @property integer $try_call_days
 * @property float $approve_percent
 * @property float $avg_approve_check
 * @property integer $error_in_address
 * @property float $income
 * @property float $lead_cost
 * @property float $call_center_costs
 * @property float $product_costs
 * @property float $other_costs
 * @property float $costs
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CompanyStandardsDelivery[] $companyStandardsDelivery
 *
 */
class CompanyStandards extends ActiveRecordLogUpdateTime
{
    const PRECISION = 2;

    const NORM_ADMISSION_LEAD = 30; //сек
    const NORM_FIRST_SMS = 30; //сек
    const NORM_SEND_TO_DELIVERY = 8; //час
    const NORM_FIRST_CALL = 3; //мин
    const NORM_TRY_CALL_COUNT = 10; //шт
    const NORM_TRY_CALL_DAYS = 2; //шт
    const NORM_APPROVE_PERCENT = 30; //%
    const NORM_AVG_APPROVE_CHECK = 65; //USD
    const NORM_ERROR_IN_ADDRESS = 0; //шт
    const NORM_LEAD_COST = 30; //%
    const NORM_CALL_CENTER_COSTS = 8; //%
    const NORM_PRODUCT_COSTS = 2; //%
    const NORM_OTHER_COSTS = 30; //%
    const NORM_INCOME = 30; //%

    /**
     * @var array
     */
    protected $_errorArray;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%report_company_standards}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'month'], 'required'],
            ['month', 'string'],
            [
                [
                    'admission_lead',
                    'first_sms',
                    'send_to_delivery',
                    'first_call',
                    'try_call_count',
                    'try_call_days',
                    'approve_percent',
                    'avg_approve_check',
                    'error_in_address',
                    'costs',
                    'income',
                    'lead_cost',
                    'call_center_costs',
                    'product_costs',
                ],
                'double'
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [['country_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'month' => Yii::t('common', 'Месяц'),
            'country_id' => Yii::t('common', 'Страна'),
            'admission_lead' => Yii::t('common', 'Поступление Лида, норма ' . static::NORM_ADMISSION_LEAD . ' сек'),
            'first_sms' => Yii::t('common', 'Первая смс клиенту, норма ' . static::NORM_FIRST_SMS . ' сек'),
            'send_to_delivery' => Yii::t('common', 'Передача в КС, норма ' . static::NORM_SEND_TO_DELIVERY . ' часов'),
            'first_call' => Yii::t('common', 'Первый звонок, норма ' . static::NORM_FIRST_CALL . ' минуты'),
            'try_call_count' => Yii::t('common', 'Количество попыток дозвона, норма ' . static::NORM_TRY_CALL_COUNT . ' звонков'),
            'try_call_days' => Yii::t('common', 'Количество дней дозвона, норма ' . static::NORM_TRY_CALL_DAYS . ' дня'),
            'approve_percent' => Yii::t('common', 'Процент апрува, норма ' . static::NORM_APPROVE_PERCENT . '%'),
            'avg_approve_check' => Yii::t('common', 'Ср. апрув. чек, норма ' . static::NORM_AVG_APPROVE_CHECK . ' USD'),
            'error_in_address' => Yii::t('common', 'Ошибок в адресе'),
            'income' => Yii::t('common', 'Прибыль, норма более ' . static::NORM_ADMISSION_LEAD . '%'),
            'lead_cost' => Yii::t('common', 'Расходы на рекламу, норма менее ' . static::NORM_LEAD_COST . '%'),
            'call_center_costs' => Yii::t('common', 'Расходы на КЦ, норма менее ' . static::NORM_CALL_CENTER_COSTS . '%'),
            'product_costs' => Yii::t('common', 'Расходы на товары, норма менее ' . static::NORM_PRODUCT_COSTS. '%'),
            'other_costs' => Yii::t('common', 'Остальные расходы, норма менее ' . static::NORM_OTHER_COSTS. '%'),
            'costs' => Yii::t('common', 'Расходы'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return float|null
     */
    public function getOther_costs(): ?float
    {
        return !empty($this->costs) ? $this->costs - $this->lead_cost - $this->call_center_costs - $this->product_costs : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyStandardsDelivery()
    {
        return $this->hasMany(CompanyStandardsDelivery::className(), ['month'=>'month', 'country_id'=>'country_id']);
    }

    /**
     * @param string $attribute
     * @return string
     */
    public static function getLabel(string $attribute): string
    {
        return (new static())->attributeLabels()[$attribute] ?? $attribute;
    }
}
