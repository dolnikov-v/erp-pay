<?php

namespace app\modules\report\models;

use app\models\Country;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class ReportAdcombo
 *
 * @property integer $id
 * @property integer $type
 * @property integer $country_id
 * @property integer $from
 * @property integer $to
 * @property integer $hold_count
 * @property integer $our_hold_count
 * @property integer $confirmed_count
 * @property integer $our_confirmed_count
 * @property integer $trash_count
 * @property integer $our_trash_count
 * @property integer $cancelled_count
 * @property integer $our_cancelled_count
 * @property integer $adcombo_total_count
 * @property integer $our_total_count
 * @property integer $cc_count
 * @property integer $difference_count
 * @property string $difference
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property array $differenceArray
 * @property Country $country
 */
class ReportAdcombo extends ActiveRecordLogUpdateTime
{

    const TYPE_DAILY = 'daily';
    const TYPE_WEEKLY = 'weekly';
    const TYPE_MONTHLY = 'monthly';
    const TYPE_HOURLY = 'hourly';

    const ERROR_OUR_ORDER_NOT_FOUND = 'our_not_found';
    const ERROR_ADCOMBO_ORDER_NOT_FOUND = 'adcombo_not_found';
    const ERROR_STATUS_NOT_EQUAL = 'state_not_equal';

    const SHOW_ERROR_TYPE_TOTAL_ADCOMBO = 'total_adcombo';
    const SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO = 'confirmed_adcombo';
    const SHOW_ERROR_TYPE_HOLD_ADCOMBO = 'hold_adcombo';
    const SHOW_ERROR_TYPE_TRASH_ADCOMBO = 'trash_adcombo';
    const SHOW_ERROR_TYPE_CANCELLED_ADCOMBO = 'cancelled_adcombo';
    const SHOW_ERROR_TYPE_TOTAL_OUR = 'total_our';
    const SHOW_ERROR_TYPE_CONFIRMED_OUR = 'confirmed_our';
    const SHOW_ERROR_TYPE_HOLD_OUR = 'hold_our';
    const SHOW_ERROR_TYPE_TRASH_OUR = 'trash_our';
    const SHOW_ERROR_TYPE_CANCELLED_OUR = 'cancelled_our';
    const SHOW_ERROR_TYPE_CC = 'cc_count';

    /**
     * @var array
     */
    protected $_errorArray;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%report_adcombo}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'type',
                    'country_id',
                    'from',
                    'to',
                ],
                'required'
            ],
            [
                [
                    'country_id',
                    'from',
                    'to',
                    'hold_count',
                    'confirmed_count',
                    'trash_count',
                    'cancelled_count',
                    'our_hold_count',
                    'our_confirmed_count',
                    'our_trash_count',
                    'our_cancelled_count',
                    'difference_count',
                    'created_at',
                    'updated_at',
                    'adcombo_total_count',
                    'our_total_count',
                    'cc_count'
                ],
                'integer'
            ],
            [['difference', 'type'], 'string'],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', '#'),
            'type' => Yii::t('common', 'Тип'),
            'country_id' => Yii::t('common', 'Страна'),
            'from' => Yii::t('common', 'С'),
            'to' => Yii::t('common', 'До'),
            'hold_count' => Yii::t('common', 'Hold в AdCombo'),
            'confirmed_count' => Yii::t('common', 'Confirmed в AdCombo'),
            'trash_count' => Yii::t('common', 'Trash в AdCombo'),
            'cancelled_count' => Yii::t('common', 'Cancelled в AdCombo'),
            'difference' => Yii::t('common', 'Различия'),
            'our_hold_count' => Yii::t('common', 'Hold у нас'),
            'our_confirmed_count' => Yii::t('common', 'Confirmed у нас'),
            'our_cancelled_count' => Yii::t('common', 'Cancelled у нас'),
            'our_trash_count' => Yii::t('common', 'Trash у нас'),
            'difference_count' => Yii::t('common', 'Не совпадает статус'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'adcombo_total_count' => Yii::t('common', 'Всего в AdCombo'),
            'our_total_count' => Yii::t('common', 'Всего у нас'),
            'cc_count' => Yii::t('common', 'Записей в КЦ')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return array
     */
    public function getDifferenceArray()
    {
        if (is_null($this->_errorArray)) {
            $errors = json_decode($this->difference, true);
            if (!is_array($errors)) {
                $errors = [];
            }
            $this->_errorArray = $errors;
        }
        return $this->_errorArray;
    }
}
