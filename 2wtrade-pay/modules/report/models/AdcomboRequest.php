<?php

namespace app\modules\report\models;

use app\models\Country;
use app\modules\api\models\LeadStatus;
use app\modules\order\models\Order;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class AdcomboRequest
 *
 * @property integer $id
 * @property integer $foreign_id
 * @property string $country_code
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $state
 * @property integer $created_at
 *
 * @property Order $order
 * @property AdcomboRequestLog[] $adcomboRequestLogs
 */
class AdcomboRequest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%adcombo_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foreign_id', 'country_code'], 'required'],
            [['foreign_id', 'created_at'], 'integer'],
            [['country_code', 'phone'], 'string', 'max' => 50],
            [['name', 'email', 'state'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'foreign_id' => Yii::t('common', 'Внешний номер'),
            'country_code' => Yii::t('common', 'Код страны'),
            'name' => Yii::t('common', 'Имя покупателя (AdCombo)'),
            'phone' => Yii::t('common', 'Номер телефона (AdCombo)'),
            'email' => Yii::t('common', 'Email (AdCombo)'),
            'state' => Yii::t('common', 'Статус (AdCombo)'),
            'created_at' => Yii::t('common', 'Дата создания (AdCombo)')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['foreign_id' => 'foreign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdcomboRequestLogs()
    {
        return $this->hasMany(AdcomboRequestLog::className(), ['adcombo_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['char_code' => 'country_code'])->andWhere(['active' => 1]);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$insert) {
            foreach ($changedAttributes as $attribute => $value) {
                if ($this->getAttribute($attribute) != $value) {
                    $log = new AdcomboRequestLog();
                    $log->field = $attribute;
                    $log->old = $value;
                    $log->new = $this->$attribute;
                    $log->adcombo_request_id = $this->id;
                    $log->save();
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getStateName()
    {
        $labels = LeadStatus::stateLabels();
        if(isset($labels[$this->state])) {
            return $labels[$this->state];
        }
        return $this->state;
    }

    /**
     * @return null|string
     */
    public function getOrderStateName()
    {
        if(!empty($this->order)) {
            $labels = LeadStatus::stateLabels();
            $state = LeadStatus::$mapOrderStatusToLeadStatus[$this->order->status_id];
            if(isset($labels[$state])) {
                return $labels[$state];
            }
            return $state;
        }
        return null;
    }
}
