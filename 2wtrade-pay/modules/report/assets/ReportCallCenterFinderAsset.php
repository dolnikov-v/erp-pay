<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportCallCenterFinderAsset
 * @package app\modules\report\assets
 */
class ReportCallCenterFinderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report/report-call-center-finder';

    public $css = [
        'report-call-center-finder.css',
    ];

    public $js = [
        'report-call-center-finder.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
