<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAdversitingAsset
 * @package app\modules\report\assets
 */
class ReportAverageCheckByCountryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-average-check-by-country.css'
    ];

    public $js = [
        'report-average-check-by-country.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
