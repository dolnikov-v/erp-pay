<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportDeliveryAsset
 * @package app\modules\report\assets
 */
class ReportDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-delivery.css'
    ];

    public $js = [
        'report-delivery.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
