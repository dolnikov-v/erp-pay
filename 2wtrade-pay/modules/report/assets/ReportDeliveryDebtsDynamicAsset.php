<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportDeliveryDebtsDynamicAsset
 * @package app\modules\report\assets
 */
class ReportDeliveryDebtsDynamicAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $js = [
        'report-delivery-debts-dynamic.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
