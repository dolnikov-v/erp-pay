<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportConsolidateByCountryAsset
 * @package app\modules\report\assets
 */
class ReportConsolidateByCountryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-consolidate-by-country.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
