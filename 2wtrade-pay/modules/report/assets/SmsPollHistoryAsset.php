<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class SmsPollHistoryAsset
 * @package app\modules\report\assets
 */
class SmsPollHistoryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/report/sms-poll-history';

    public $js = [
        'sms-poll-history.js',
    ];

    public $css = [
        'sms-poll-history.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
