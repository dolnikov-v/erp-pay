<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportValidateFinanceAsset
 * @package app\modules\report\assets
 */
class ReportValidateFinanceAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $js = [
        'report-validate-finance.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];

}