<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class InvoiceAsset
 * @package app\modules\report\assets
 */
class InvoiceAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report/invoice';

    public $css = [
        'invoice.css'
    ];

    public $js = [
        'invoice.js',
        'invoice-generator.js',
        'invoice-close.js',
        'invoice-sender.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
        'app\assets\vendor\BootstrapLaddaAsset',
    ];
}
