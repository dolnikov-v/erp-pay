<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportCompanyStandardsAsset
 * @package app\modules\report\assets
 */
class ReportCompanyStandardsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-company-standards.css'
    ];

    public $js = [
        'report-company-standards.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
