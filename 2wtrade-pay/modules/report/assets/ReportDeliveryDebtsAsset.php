<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportDeliveryDebtsAsset
 * @package app\modules\report\assets
 */
class ReportDeliveryDebtsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-delivery-debts.css'
    ];

    public $js = [
        'report-delivery-debts.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
