<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportSalaryAsset
 * @package app\modules\report\assets
 */
class ReportSalaryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $js = [
        'report-salary.js',
    ];

    public $css = [
        'report-salary.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
