<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAsset
 * @package app\modules\report\assets
 */
class ReportAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report.css'
    ];

    public $js = [
        'report.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
