<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class FilterCountryDeliveryPartnersAsset
 * @package app\modules\report\assets
 */
class FilterCountryDeliveryPartnersAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/country-delivery-partners-multiple';

    public $css = [
        'country-delivery-partners-multiple.css'
    ];

    public $js = [
        'country-delivery-partners-multiple.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}