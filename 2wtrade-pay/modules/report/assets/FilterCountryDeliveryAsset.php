<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class FilterCountryDeliveryAsset
 * @package app\modules\report\assets
 */
class FilterCountryDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/country-delivery-multiple';

    public $css = [
        'country-delivery-multiple.css'
    ];

    public $js = [
        'country-delivery-multiple.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}