<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ActionNavAsset
 * @package app\modules\report\assets
 */
class ActionNavAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/report/action-nav';

    public $css = [
        'action-nav.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
