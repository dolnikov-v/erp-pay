<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportCallCenterOperatorsOnlineAsset
 * @package app\modules\report\assets
 */
class ReportCallCenterOperatorsOnlineAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $js = [
        'report-call-center-operators-online.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
