<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportDeliveryTermsAsset
 * @package app\modules\report\assets
 */
class ReportDeliveryTermsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-delivery-terms.css'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
