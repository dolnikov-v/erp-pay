<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportTimeSheetAsset
 * @package app\modules\report\assets
 */
class ReportTimeSheetAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-time-sheet.css'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
