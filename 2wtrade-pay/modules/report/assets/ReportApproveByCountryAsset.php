<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAdversitingAsset
 * @package app\modules\report\assets
 */
class ReportApproveByCountryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-approve-by-country.css'
    ];

    public $js = [
        'report-approve-by-country.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
