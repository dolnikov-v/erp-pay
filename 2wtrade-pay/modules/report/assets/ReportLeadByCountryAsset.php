<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAdversitingAsset
 * @package app\modules\report\assets
 */
class ReportLeadByCountryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-lead-by-country.css'
    ];

    public $js = [
        'report-lead-by-country.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
