<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportDeliveryClosedPeriodsAsset
 * @package app\modules\report\assets
 */
class ReportDeliveryClosedPeriodsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-delivery-closed-periods.css'
    ];

    public $js = [
        'report-delivery-closed-periods.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
