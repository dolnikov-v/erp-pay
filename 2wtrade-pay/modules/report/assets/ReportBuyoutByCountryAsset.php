<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAdversitingAsset
 * @package app\modules\report\assets
 */
class ReportBuyoutByCountryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-buyout-by-country.css'
    ];

    public $js = [
        'report-buyout-by-country.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
