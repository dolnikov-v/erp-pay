<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportTeamAsset
 * @package app\modules\report\assets
 */
class ReportTeamAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $js = [
        'report-team.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
