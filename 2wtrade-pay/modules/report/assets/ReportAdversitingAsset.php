<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportAdversitingAsset
 * @package app\modules\report\assets
 */
class ReportAdversitingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'reportAdversiting.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
