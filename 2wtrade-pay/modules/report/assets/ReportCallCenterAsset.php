<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportCallCenterAsset
 * @package app\modules\report\assets
 */
class ReportCallCenterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-call-center.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
