<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportConsolidateSaleAsset
 * @package app\modules\report\assets
 */
class ReportConsolidateSaleAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-consolidate-sale.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
