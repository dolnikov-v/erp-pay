<?php

namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class HighChartsNavAsset
 * @package app\modules\report\assets
 */
class HighChartsNavAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/report/high-charts-nav';

    public $js = [
        'high-charts-nav.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
