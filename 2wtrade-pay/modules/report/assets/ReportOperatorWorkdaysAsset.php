<?php
namespace app\modules\report\assets;

use yii\web\AssetBundle;

/**
 * Class ReportOperatorWorkdaysAsset
 * @package app\modules\report\assets
 */
class ReportOperatorWorkdaysAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-operator-workdays.css'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
