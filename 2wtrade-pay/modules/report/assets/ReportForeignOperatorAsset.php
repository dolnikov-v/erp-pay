<?php

namespace app\modules\report\assets;


use yii\web\AssetBundle;

/**
 * Class ReportForeignOperatorAsset
 * @package app\modules\report\assets
 */
class ReportForeignOperatorAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-foreign-operator.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
