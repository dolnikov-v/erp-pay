<?php

namespace app\modules\report\helpers;

use app\modules\report\models\AdcomboRequest;
use Yii;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;

/**
 * Class AdComboAPI
 * @package app\modules\report\helpers
 */
class AdcomboApi
{
    /**
     * @var string
     */
    public static $key = '481476737d2e4b5a75289b6fa88ee3f7';

    /**
     * @param array $ids
     * @param null|integer $fromTime
     * @param null|integer $toTime
     * @param string $state
     * @param string $country
     * @return mixed
     * @throws BadRequestHttpException
     */
    public static function query($ids = [], $fromTime = null, $toTime = null, $state = '', $country = '')
    {
        $url = "http://api.adcombo.com/order/2wtrade";

        $params = [
            'api_key' => self::$key
        ];
        if (!empty($ids) && is_array($ids)) {
            $params['id'] = implode(',', $ids);
        } elseif (is_string($ids)) {
            $params['id'] = $ids;
        }

        if (!empty($fromTime) && is_integer($fromTime)) {
            $params['ts'] = $fromTime;
        }

        if (!empty($toTime) && is_integer($toTime)) {
            $params['te'] = $toTime;
        }

        if (!empty($state)) {
            $params['state'] = $state;
        }

        if (!empty($country) && !isset($params['id'])) {
            $params['country_code'] = strtoupper($country);
        }

        $client = new Client();

        $response = $client->createRequest()->setMethod('get')->setUrl($url)->setData($params)->send();

        if (!$response->isOk) {
            throw new BadRequestHttpException(Yii::t('common', 'Система AdCombo в данный момент недоступна.'));
        }

        if (!isset($response->data['data'])) {
            throw new BadRequestHttpException(Yii::t('common', 'Данные из AdCombo в некорректном формате.'));
        }

        $data = $response->data['data'];

        $models = [];
        if (!is_array($data)) {
            return $data;
        }

        foreach ($data as $item) {
            $item['foreign_id'] = $item['id'];
            $item['created_at'] = $item['ts_spawn'];
            unset($item['ts_spawn']);
            unset($item['id']);
            $model = new AdcomboRequest();
            $model->load($item, '');
            $models[] = $model;
        }

        return $models;
    }
}
