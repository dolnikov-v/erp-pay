<?php
namespace app\modules\apidelivery;

use Yii;
use app\modules\apidelivery\components\filters\auth\HttpBearerAuth;

/**
 * Class Module
 * @package app\modules\apidelivery
 */
class Module extends \yii\base\Module
{
    /**
     * @var array Разрешенные контроллеры без токена
     */
    private $patternsIgnoredUrl = [
        '#^v1/auth/login#',
        '#^v1/ext-staff/index#',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        if ($this->isIgnoredUrl()) {
            return parent::behaviors();
        }

        Yii::$app->user->enableSession = false;

        $behaviors = [];

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        Yii::$app->language = 'en-US';

        return $behaviors;
    }

    /**
     * @return bool
     */
    private function isIgnoredUrl()
    {
        $url = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

        foreach ($this->patternsIgnoredUrl as $pattern) {
            if (preg_match($pattern, $url)) {
                return true;
            }
        }

        return false;
    }
}
