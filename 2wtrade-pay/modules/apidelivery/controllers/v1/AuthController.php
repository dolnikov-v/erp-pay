<?php
namespace app\modules\apidelivery\controllers\v1;

use app\components\rest\Controller;
use app\models\api\ApiUser;
use app\models\api\ApiUserToken;
use Yii;


/**
 * Class AuthController
 * @package app\modules\apidelivery\controllers
 */
class AuthController extends Controller
{
    /**
     * @return array
     */
    public function actionLogin()
    {
        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');

        if ($username && $password) {
            $user = ApiUser::find()
                ->byUsername($username)
                ->one();

            if (!$user || $user->status == ApiUser::STATUS_NO_ACTIVE) {
                $this->addMessage('User does not exist or is not active.');

                return $this->fail();
            }

            if (Yii::$app->security->validatePassword($password, $user->password)) {
                $apiToken = new ApiUserToken();
                $apiToken->user_id = $user->id;
                $apiToken->type = ApiUserToken::TYPE_DELIVERY;
                $apiToken->auth_token = $apiToken->generateToken();
                $apiToken->lifetime = $apiToken->getLifetimeToken();

                if ($apiToken->save()) {
                    $this->addDataValue('token', $apiToken->auth_token);
                    return $this->success();
                }
            } else {
                $this->addMessage('Invalid login or password.');
                return $this->fail();
            }
        } else {
            $this->addMessage('Username and password are required.');
            return $this->fail();
        }
    }
}
