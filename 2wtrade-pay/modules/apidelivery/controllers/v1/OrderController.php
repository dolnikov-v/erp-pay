<?php
namespace app\modules\apidelivery\controllers\v1;

use app\components\rest\Controller;
use app\modules\apidelivery\models\Order;
use Yii;

/**
 * Class OrderController
 * @package app\modules\apidelivery\controllers
 */
class OrderController extends Controller
{

    /**
     * Получение заказов по стране
     *
     * @return array
     */
    public function actionOrders()
    {
        $order = new Order();
        $order->setScenario(Order::SCENARIO_GET_ORDERS);
        $order->load(Yii::$app->request->get(), '');

        $result = $order->getOrders();

        if ($order->hasErrors()) {
            $this->addMessagesByModel($order);

            return $this->fail();
        } else {
            $this->addData($result);

            return $this->success();
        }
    }

    /**
     *
     * Обновление статуса заказа
     *
     * @return array
     */
    public function actionUpdateStatus()
    {
        $order = new Order();
        $order->setScenario(Order::SCENARIO_UPDATE_ORDER);
        $order->load(Yii::$app->request->get(), '');

        $result = $order->updateStatus();

        if ($order->hasErrors()) {
            $this->addMessagesByModel($order);

            return $this->fail();
        } else {
            $this->addData($result);

            return $this->success();
        }
    }
}
