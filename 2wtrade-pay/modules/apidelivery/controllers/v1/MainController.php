<?php
namespace app\modules\apidelivery\controllers\v1;

use app\components\rest\Controller;
use app\models\api\ApiUserDelivery;
use app\models\Product;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\db\Query;

class MainController extends Controller
{

    /**
     * Отдает продукты по имеющимся партиям на складах апи пользователя
     *
     * @return array
     */
    public function actionProducts(){
        $query = new Query();
        $products = $query->from(ApiUserDelivery::tableName())
            ->where([ApiUserDelivery::tableName() . '.user_id' => Yii::$app->apiUser->identity->id])
            ->leftJoin(Storage::tableName(), ApiUserDelivery::tableName() . '.delivery_id=' . Storage::tableName() . '.delivery_id')
            ->leftJoin(StorageProduct::tableName(), Storage::tableName() . '.id=' . StorageProduct::tableName() . '.storage_id')
            ->leftJoin(Product::tableName(), StorageProduct::tableName() . '.product_id=' . Product::tableName() . '.id')
            ->select([Product::tableName() . '.id', Product::tableName() . '.name'])->distinct()->all();
        if (!$products) {
            $this->addMessage('There is no products in your storage');
            return $this->fail();
        }
        foreach ($products as $product) {
            $this->addData($product);
        }
        return $this->success();
    }

}