<?php

namespace app\modules\apidelivery\controllers\v1;

use Yii;
//use yii\web\Controller;
use app\components\rest\Controller;
use app\modules\report\components\ReportFormForeignOperator;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\models\Country;
use yii\helpers\ArrayHelper;

/**
 * Class ExtStaffController
 * @package app\controllers
 */
class ExtStaffController extends Controller
{
    /**
     * @var string
     */
    private $token = '9e35e6d086634692415be59127947cc4';

    /**
     * @return array
     */
    public function actionIndex()
    {

        if (!$this->check()) {
            $this->responseData['data'] = 'Sorry, access denied';
            return $this->fail();
        }

        $country = Country::find()
            ->byCharCode(Yii::$app->request->post('country'))
            ->one();

        $params = [];
        $params['s']['from'] = strtotime(date('d.m.Y', time() - 60*60*24*45));
        $params['s']['to'] = strtotime(date('d.m.Y', (time() - 60*60*24*15))) + 86399;

        $callCenters = CallCenter::find()
            ->byCountryId($country->id)
            ->collection();

        $callCenters = array_keys($callCenters);

        $cc_users = CallCenterUser::find()
            ->where(['IN', CallCenterUser::tableName() .'.callcenter_id', $callCenters])
            ->asArray()
            ->all();

        $reportForm = new ReportFormForeignOperator();
        $reportForm->ext_country_id = $country->id;
        $dataProvider = $reportForm->apply($params);

        $result = [];
        foreach ($dataProvider->allModels as $model) {
            foreach ($cc_users as $user) {
                if ($user['user_id'] == $model['last_foreign_operator']) {
                    $result[] = Array('operator_id' => $model['last_foreign_operator'], 'operator_login' => $user['user_login'], 'operator_name' => $user['name'], 'bonus' => number_format(($model['sum'] / 100 * 4), 2));
                    break;
                }
            }
        }

        ArrayHelper::multisort($result, 'operator_id');
        $respond = json_encode($result);

        $this->responseData['data'] = $respond;
        $this->responseData['status'] = $this::RESPONSE_STATUS_SUCCESS;
        return $this->respond();
    }


    /**
     * @return bool
     */
    private function check() {

        if (!Yii::$app->request->post('token') || !Yii::$app->request->post('country')) {
            return false;
        }

        if (Yii::$app->request->post('token') != $this->token) {
            return false;
        }

        if (empty(trim(Yii::$app->request->post('country')))) {
            return false;
        }

        return true;
    }

}
