<?php
namespace app\modules\apidelivery\models;

use app\models\Country;
use app\modules\delivery\components\control\DeliveryControl;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order as OrderModel;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\Storage;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class Order
 * @package app\modules\apidelivery\models
 */
class Order extends Model
{
    const SCENARIO_GET_ORDERS = 'get-orders';
    const SCENARIO_UPDATE_ORDER = 'update-order';

    /**
     * @var Country $country
     */
    public $country;

    public $status = 0;

    public $page = 1;

    public $perPage = 10;

    public $orderId;

    public $date;

    public $track;

    /** @var  array */
    private $deliveryStatuses;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $deliveryStatuses = OrderStatus::find()
            ->where(['group' => OrderStatus::GROUP_DELIVERY])
            ->all();

        $this->deliveryStatuses = ArrayHelper::getColumn($deliveryStatuses, 'id');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['orderId', 'status'],
                'required',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ]
            ],
            [
                'orderId',
                'validateUpdateOrderId',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ],
            [
                'status',
                'validateUpdateStatus',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ],
            [
                'country',
                'required',
                'message' => 'Country is required.',
                'on' => [
                    self::SCENARIO_GET_ORDERS,
                ]
            ],
            [
                'country',
                'validateCountry',
                'on' => [
                    self::SCENARIO_GET_ORDERS,
                ],
            ],
            [
                'page',
                'integer',
                'min' => 1,
                'on' => [
                    self::SCENARIO_GET_ORDERS,
                ],
            ],
            [
                'perPage',
                'integer',
                'min' => 1,
                'max' => 100,
                'on' => [
                    self::SCENARIO_GET_ORDERS,
                ],
            ],
            [
                'status',
                'validateOrdersStatuses',
                'on' => [
                    self::SCENARIO_GET_ORDERS,
                ],
            ],
            [
                ['date', 'track'],
                'string',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ],
            [
                'date',
                'date',
                'format' => 'php:U',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country' => 'Country',
            'page' => 'Page',
            'perPage' => 'The number of orders',
            'status' => 'Status',
            'date' => 'Date',
            'track' => 'Track number',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return Yii::$app->apiUser->identity->id;
    }

    /**
     * @param string $attribute
     */
    public function validateCountry($attribute)
    {
        $country = Country::find()
            ->byCharCode($this->country)
            ->one();

        if ($country) {
            $this->country = $country;
        } else {
            $this->addError($attribute, Yii::t('common', 'Unknown country code "{code}".', [
                'code' => $this->country,
            ]));
        }
    }

    /**
     * @param $attribute
     */
    public function validateOrdersStatuses($attribute)
    {
        if ($this->status) {
            if (!in_array($this->status, $this->deliveryStatuses)) {
                $this->addError($attribute, Yii::t('common', 'Forbidden status "{status}".', [
                    'status' => $this->status,
                ]));
            }
        } else {
            $this->status = $this->deliveryStatuses;
        }
    }

    /**
     * @param $attribute
     */
    public function validateUpdateStatus($attribute)
    {
        if (!in_array($this->status, $this->deliveryStatuses)) {
            $this->addError($attribute, Yii::t('common', 'Forbidden status "{status}".', [
                'status' => $this->status,
            ]));
        }
    }

    /**
     * @param $attribute
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function validateUpdateOrderId($attribute)
    {
        $order = OrderModel::find()
            ->joinWith(['deliveryRequest'])
            ->where([OrderModel::tableName() . '.id' => $this->orderId])
            ->one();

        if ($order) {
            $idsDelivery = Yii::$app->apiUser->getDeliveriesId();

            //проверяем, входит ли статус заказа в группу статусов доставки
            if (!in_array($order->status_id, $this->deliveryStatuses)) {
                $this->addError($attribute, Yii::t('common', 'No access to edit order #{orderId}.', [
                    'orderId' => $this->orderId,
                ]));
            }

            //проверяем доступна ли такая служба доставки пользователю
            if (!$order->deliveryRequest || !in_array($order->deliveryRequest->delivery_id, $idsDelivery)) {
                $this->addError($attribute, Yii::t('common', 'No access to edit order #{orderId}.', [
                    'orderId' => $this->orderId,
                ]));
            }
        } else {
            $this->addError($attribute, Yii::t('common', 'Order #{orderId} not found.', [
                'orderId' => $this->orderId,
            ]));
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOrders()
    {
        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $delivery = Yii::$app->apiUser->getDelivery($this->country);

                $result = [];

                if (!$delivery->our_api) {
                    throw new InvalidParamException("API is disabled for your account in {$this->country->char_code}.");
                }

                $offset = ($this->page - 1) * $this->perPage;

                $orderWithRequests = OrderModel::find()
                    ->innerJoinWith('deliveryRequest')
                    ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                    ->andWhere([OrderModel::tableName() . ".status_id" => $this->status])
                    ->orderBy([OrderModel::tableName() . '.id' => SORT_ASC])
                    ->offset($offset)
                    ->limit($this->perPage);

                $ordersCount = $orderWithRequests->count(OrderModel::tableName() . '.id');
                $result['ordersCount'] = $ordersCount;
                $result['page'] = $this->page;
                $result['isLastPage'] = ($this->perPage * $this->page - $ordersCount) > 0;

                $orders = $orderWithRequests->all();

                if ($orders) {
                    /** @var OrderModel[] $orders */
                    foreach ($orders as $order) {
                        // формируем массив продуктов заказа
                        $products = [];

                        foreach ($order->orderProducts as $orderProduct) {
                            $products[] = [
                                'name' => $orderProduct->product->name,
                                'quantity' => $orderProduct->quantity,
                                'sku' => $orderProduct->product_id,
                                'price' => $orderProduct->price,
                            ];
                        }

                        $senderDetails = [];

                        if (!is_null($order->deliveryRequest)) {
                            if (!is_null($order->deliveryRequest->storage)) {
                                $storage = $order->deliveryRequest->storage;
                            } else {
                                $storage = Storage::findOne(['country_id' => $order->country_id, 'default' => 1]);
                            }
                            if ($storage) {
                                $senderDetails = [
                                    'sender_name' => $storage->sender_name,
                                    'sender_city' => $storage->sender_city,
                                    'sender_zip' => $storage->sender_zip,
                                    'sender_phone' => $storage->sender_phone,
                                    'sender_address' => $storage->address,
                                ];
                            }
                        }

                        $result[] = [
                            'id' => $order->id,
                            'status' => $order->status_id,
                            'full_name' => $order->customer_full_name,
                            'email' => $order->customer_email,
                            'phone' => $order->customer_phone,
                            'mobile' => $order->customer_mobile,
                            'province' => $order->customer_province,
                            'district' => $order->customer_district,
                            'city' => $order->customer_city,
                            'street' => $order->customer_street,
                            'house_number' => $order->customer_house_number,
                            'zip' => $order->customer_zip,
                            'address' => $order->customer_address,
                            'address_add' => $order->customer_address_add,
                            'comment' => $order->comment,
                            'products' => $products,
                            'collectable_amount' => ($order->price_total + $order->delivery),
                            'sender_details' => $senderDetails
                        ];

                    }
                }

                $transaction->commit();

                return $result;

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @return array|bool
     */
    public function updateStatus()
    {
        if ($this->validate()) {
            $order = OrderModel::findOne($this->orderId);

            $possibleStatuses = array_intersect($order->getNextStatuses($order->status_id), OrderStatus::getProcessDeliveryAndLogisticList());
            if (in_array($this->status, $possibleStatuses)) {
                $order->status_id = $this->status;

                if ($order->save()) {
                    $deliveryRequest = DeliveryRequest::findOne(['order_id' => $order->id]);
                    $deliveryRequest->status = DeliveryRequest::STATUS_IN_PROGRESS;
                    if ($this->track) {
                        $deliveryRequest->tracking = $this->track;
                    }
                    if ($this->date) {
                        switch ($this->status) {
                            case OrderStatus::STATUS_CC_APPROVED:
                                break;
                            case OrderStatus::STATUS_DELIVERY_ACCEPTED:
                                break;
                            case OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE:
                                break;
                            case OrderStatus::STATUS_DELIVERY_DENIAL:
                            case OrderStatus::STATUS_DELIVERY_BUYOUT:
                                $deliveryRequest->approved_at = $this->date;
                                break;
                            case OrderStatus::STATUS_DELIVERY_RETURNED:
                                $deliveryRequest->returned_at = $this->date;
                                break;
                            case OrderStatus::STATUS_DELIVERY_REDELIVERY:
                                break;
                            case OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE:
                                break;
                            case OrderStatus::STATUS_DELIVERY_LOST:
                                break;
                        }
                    }

                    if (empty($deliveryRequest->accepted_at)) {
                        $deliveryRequest->accepted_at = $this->date ?? time();
                    }

                    if (empty($deliveryRequest->sent_at)) {
                        $deliveryRequest->sent_at = time();
                    }

                    if (OrderStatus::isFinalDeliveryStatus($this->status)) {
                        $deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                        $deliveryRequest->done_at = time();
                    }
                    if ($this->status == OrderStatus::STATUS_DELIVERY_REJECTED) {
                        $deliveryRequest->status = DeliveryRequest::STATUS_ERROR;
                    }
                    $deliveryRequest->save();

                    if ($this->status == OrderStatus::STATUS_DELIVERY_REJECTED) {
                        $deliveryControl = new DeliveryControl();
                        if ($otherDeliveryId = $deliveryControl->shouldSendOrderToOtherDelivery($order)) {
                            $deliveryControl->sendOrderToOtherDelivery($order, $otherDeliveryId);
                        }
                    }

                    return [
                        'orderId' => $this->orderId,
                        'status' => $this->status,
                    ];
                }
            } else {
                $e = new InvalidParamException('Status of order cannot be changed to ' . $this->status . (empty($possibleStatuses) ? '.' : '. The next status of order may be ' . implode(', ',
                            $possibleStatuses)));
                $e->statusCode = 200;
                throw $e;
            }
        }

        return false;
    }
}
