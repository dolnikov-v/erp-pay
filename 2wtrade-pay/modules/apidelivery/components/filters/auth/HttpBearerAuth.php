<?php
namespace app\modules\apidelivery\components\filters\auth;

use app\models\api\ApiUserToken;
use Yii;
use yii\base\Exception;
use yii\filters\auth\AuthMethod;
use yii\web\UnauthorizedHttpException;

/**
 * Class HttpBearerAuth
 * @package app\modules\apidelivery\components\filters\auth
 */
class HttpBearerAuth extends AuthMethod
{
    /**
     * @var string
     */
    public $realm = 'apidelivery';

    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $authHeader = $request->getHeaders()->get('Authorization');

        if ($authHeader !== null && preg_match('/^Basic\s+(.*?)$/', $authHeader, $matches)) {
            $accessToken = base64_decode($matches[1]);

            $token = ApiUserToken::find()
                ->joinWith('user')
                ->byToken($accessToken)
                ->andWhere([
                    'OR',
                    ['is', ApiUserToken::tableName() . '.lifetime', null],
                    [ApiUserToken::tableName() . '.lifetime' => 0],
                    ['>', ApiUserToken::tableName() . '.lifetime', time()]
                ])
                ->one();

            if ($token) {
                Yii::$app->apiUser->token = $token;

                return true;
            }
        }

        return null;
    }

    /**
     * @param \yii\web\Response $response
     * @throws UnauthorizedHttpException
     */
    public function challenge($response)
    {
        $response->getHeaders()->set('WWW-Authenticate', "Basic realm=\"{$this->realm}\"");

        throw new UnauthorizedHttpException('Authentication token is invalid or has expired');
    }
}
