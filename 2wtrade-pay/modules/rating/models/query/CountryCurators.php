<?php
namespace app\modules\rating\models\query;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\models\Country;
use app\models\CurrencyRate;
use app\models\User;
use app\models\UserCountry;
use app\models\AuthAssignment;
use app\modules\media\models\Media;

use app\modules\report\extensions\Query;
use yii\db\Expression;
use yii;


/**
 * Class CountryCurators extends ActiveRecord
 * @package app\modules\rating
 */
class CountryCurators extends Query
{
    protected $query;
    public $params;

    public function getMapStatuses()
    {
        return [
            Yii::t('common', 'Выкуплено') => [
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            ],
            Yii::t('common', 'Подтверждено') => [
                OrderStatus::STATUS_DELIVERY_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY,
                OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY_REDELIVERY,
                OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY_DELAYED,
                OrderStatus::STATUS_LOG_ACCEPTED,
                OrderStatus::STATUS_LOG_GENERATED,
                OrderStatus::STATUS_LOG_SET,
                OrderStatus::STATUS_LOG_PASTED,
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_LOGISTICS_REJECTED,
                OrderStatus::STATUS_DELIVERY_REFUND,
                OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LOST,
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            ],
            Yii::t('common', 'Всего') => [
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM,
                OrderStatus::STATUS_CC_POST_CALL,
                OrderStatus::STATUS_CC_RECALL,
                OrderStatus::STATUS_CC_FAIL_CALL,
                OrderStatus::STATUS_CC_APPROVED,
                OrderStatus::STATUS_CC_REJECTED,
                OrderStatus::STATUS_LOG_ACCEPTED,
                OrderStatus::STATUS_LOG_GENERATED,
                OrderStatus::STATUS_LOG_SET,
                OrderStatus::STATUS_LOG_PASTED,
                OrderStatus::STATUS_DELIVERY_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY,
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY_REDELIVERY,
                OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                OrderStatus::STATUS_LOGISTICS_REJECTED,
                OrderStatus::STATUS_DELIVERY_DELAYED,
                OrderStatus::STATUS_CC_TRASH,
                OrderStatus::STATUS_CC_DOUBLE,
                OrderStatus::STATUS_DELIVERY_REFUND,
                OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LOST,
                OrderStatus::STATUS_CC_INPUT_QUEUE,
                OrderStatus::STATUS_DELIVERY_PENDING,
                OrderStatus::STATUS_LOG_DEFERRED,
            ]
        ];
    }

    /**
     * Получить Username терменов
     * @param $ids []
     * @return array
     */
    public static function getUsernames($ids)
    {
        $query = User::find()
            ->where(['in', User::tableName() . '.id', $ids]);

        return $query->asArray()->all();
    }

    /**
     * Получить данные по странам за определённый период
     * @return array
     */
    public function getDataByCountries()
    {
        $toDollar = '';
        $mapStatuses = $this->getMapStatuses();
        $dayStart =  strtotime("-15 day 00:00:00");
        $dayEnd =  $dayStart + 86400;

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'buyout' => new Expression('Count(' . Order::tableName() . '.status_id' . ' 
                        in (' . implode(',', $mapStatuses[Yii::t('common', 'Выкуплено')]) . ') or NULL)'),
            'approve' => new Expression('Count(' . Order::tableName() . '.status_id' . ' 
                        in (' . implode(',', $mapStatuses[Yii::t('common', 'Подтверждено')]) . ') or NULL)'),
            'all' => new Expression('Count(' . Order::tableName() . '.status_id' . ' 
                        in (' . implode(',', $mapStatuses[Yii::t('common', 'Всего')]) . ') or NULL)'),
        ])
            ->innerJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->groupBy([Country::tableName() . '.id']);

        $this->query->andFilterWhere(['>=', Order::tableName() . '.created_at', $dayStart]);
        $this->query->andFilterWhere(['<=', Order::tableName() . '.created_at', $dayEnd]);

        if ($this->params['dollar']) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(),
                Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $caseCheck = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            $mapStatuses[Yii::t('common', 'Подтверждено')]
        );

        $this->query->addAvgCondition($caseCheck, Yii::t('common', 'Ср. чек КЦ'));

        return $this->query->all();

    }

    /**
     * Получить список id тер.менеджеров по странам
     * @param integer $countryId
     * @return array
     */
    public static function getCuratorsByCountries($countryId = null)
    {
        $query = Country::find()
            ->select([
                'user_id' => User::tableName().'.id',
                'user_name' => User::tableName().'.username',
                'country_id' => Country::tableName().'.id',
                'country_name' => Country::tableName().'.name',
                'file' => Media::tableName().'.filename',
                'thumb' => Media::tableName().'.crop_file'
            ])
            ->leftJoin(User::tableName(), [User::tableName() . '.id' => new Expression(Country::tableName() . '.curator_user_id')])
            ->leftJoin(Media::tableName(),[Media::tableName().'.id' => new Expression(User::tableName().'.photo_id')])
            ->leftJoin(AuthAssignment::tableName(), [AuthAssignment::tableName() . '.user_id' => new Expression(User::tableName() . '.id')])
            ->where([AuthAssignment::tableName() . '.item_name' => 'country.curator'])
            ->andWhere([User::tableName() . '.status' => User::STATUS_DEFAULT]);

        if ($countryId) {
            $query->andWhere([UserCountry::tableName() . '.country_id' => $countryId]);
            $query->groupBy([UserCountry::tableName() . '.country_id']);
        }

        return $query->asArray()->all();
    }

}
