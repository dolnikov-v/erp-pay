<?php
namespace app\modules\rating\assets;

use yii\web\AssetBundle;

/**
 * Class ListAsset
 * @package app\modules\rating\assets
 */
class CountryCuratorsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/rating/';

    public $css = [
        'country-curators.css',
    ];

    public $js = [
        'country-curators.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
