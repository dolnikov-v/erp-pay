<?php
namespace app\modules\rating\components;

use app\models\Product;
use app\models\Country;
use yii\data\ArrayDataProvider;
use app\models\CalculatedParam;
use app\modules\report\extensions\Query;
use Yii;
use app\modules\report\components\filters\ForecastLeadCountrySelectFilter;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormRatingProduct
 * @package app\modules\report\components
 */
class ReportFormRatingProduct extends \app\modules\report\components\ReportForm
{
    public static $countries;
    public static $products;
    public static $days;
    public static $currencies;

    public function init()
    {
        parent::init();
        self::$countries = Country::find()->collection();
        self::$products = Product::find()->collection();
        self::$days = [7, 15, 30, 45];
        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);
            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        return $this->getResultArray($params, CalculatedParam::PERCENT_PRODUCT_BUYOUT, 100);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return ForecastLeadCountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new ForecastLeadCountrySelectFilter();
        }

        return $this->countrySelectFilter;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getCountrySelectFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param array $params
     * @param Query $subQuery
     * @return $this
     */
    protected function applyFiltersQuery($params, $subQuery)
    {
        $this->getCountrySelectFilter()->apply($subQuery, $params);
        return $this;
    }

    /**
     * @param array $params
     * @param string $calculatedName
     * @param integer $percent
     * @return ArrayDataProvider
     */
    protected function getResultArray($params, $calculatedName = CalculatedParam::PERCENT_PRODUCT_BUYOUT, $percent = 1)
    {
        if (empty($params)) {
            $params[$this->getCountrySelectFilter()->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
        }
        $result = [];
        foreach (self::$days as $day) {
            $this->query = new Query();
            $this->query->from(CalculatedParam::tableName())
                ->select([
                    'country_id' => CalculatedParam::tableName() . '.country_id',
                    'product_id' => CalculatedParam::tableName() . '.product_id',
                    'value' => 'ifnull(' . CalculatedParam::tableName() . '.value * ' . $percent . ', 0)',
                ])
                ->where(['name' => $calculatedName])
                ->andWhere("instr(" . CalculatedParam::tableName() . ".params, '" . 'daysAgo":' . $day . "') > 0");
            $this->applyFilters($params);
            $this->query->orderBy(['country_id' => SORT_ASC, 'value' => SORT_DESC]);

            $valueArray = $this->query->all();
            if (is_array($valueArray)) {
                $i = 1;
                $countryId = null;
                foreach ($valueArray as $valueItem) {
                    if ($countryId != $valueItem['country_id']) {
                        $i = 1;
                    }
                    $result[$valueItem['country_id'] . '_' . $i]['country_id'] = $valueItem['country_id'];
                    $result[$valueItem['country_id'] . '_' . $i]['value'.$day] = $valueItem['value'];
                    $result[$valueItem['country_id'] . '_' . $i]['product'.$day] = $valueItem['product_id'];
                    $countryId = $valueItem['country_id'];
                    $i++;
                }
            }
        }

        ksort($result);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    public function getSelectedCountryNames() {
        $selectedCountryIds = array_map("intval", $this->getCountrySelectFilter()->country_ids);
        return array_map(function($countryId) {
            return self::$countries[$countryId];
        }, $selectedCountryIds);
    }
}