<?php
namespace app\modules\rating\components;

use yii\data\ArrayDataProvider;
use app\models\CalculatedParam;

/**
 * Class ReportFormRatingBuyoutCheck
 * @package app\modules\report\components
 */
class ReportFormRatingBuyoutCheck extends ReportFormRatingProduct
{
    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        return $this->getResultArray($params, CalculatedParam::AVG_CHECK_BUYOUT);
    }
}