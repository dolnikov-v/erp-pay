<?php
namespace app\modules\rating\controllers;

use app\components\web\Controller;
use app\modules\rating\models\query\CountryCurators;
use yii;

/**
 * Class CountryCuratorsController
 * @package app\modules\rating\
 */
class CountryCuratorsController extends Controller
{
    const ORDERS_BUYOUT = 'buyout';
    const ORDERS_APPROVE = 'approve';
    const ORDERS_ALL = 'all';
    const ORDERS_CHECK = 'sr_cek_kc';

    /** @var  string */
    public $combineData;
    /** @var  array */
    public $params = [];

    /** @var  CountryCurators */
    public $model;

    public function init()
    {
        $module = yii::$app->getModule('rating');
        $this->params = $module->params;

        $this->model = new CountryCurators();
        $this->model->params = $module->params;

        $this->combineData = $this->generateData();

    }

    public function actionIndex()
    {
        $this->layout = 'country-curators';

        return $this->render('index', [
            'combineData' => $this->combineData,
            'params' => $this->params
        ]);
    }

    /**
     * @return array
     */
    public function generateData()
    {
        $data = $this->model->getDataByCountries();
        $agregateData = $this->agregateData($data);

        $users = $this->model->getCuratorsByCountries();

        $combineData = $this->combineData($users, $agregateData);

        return $combineData;
    }

    /**
     * Расчёт процентов по апруву и выкупу
     * @param $data []
     * @return array
     */
    public function agregateData($data)
    {
        $agregateData = [];
        if (!is_array($data)) {
            $agregateData = [];
        } else {
            foreach ($data as $key => $value) {
                $all = $value[self::ORDERS_ALL];
                $country_id = $value['country_id'];

                foreach ($value as $k => $v) {
                    switch ($k) {
                        case self::ORDERS_BUYOUT:
                            $agregateData[$country_id]['buyout_percent'] = ($all == 0) ? 0 : $v * 100 / $all;
                            break;
                        case self::ORDERS_APPROVE:
                            $agregateData[$country_id]['approve_percent'] = ($all == 0) ? 0 : $v * 100 / $all;
                            break;
                        case self::ORDERS_CHECK:
                            $agregateData[$country_id][$k] = is_null($v) ? 0 : $v;
                            break;
                        default :
                            $agregateData[$country_id][$k] = $v;
                            break;
                    }

                }
            }
        }

        return $agregateData;
    }

    /**
     * @param $users []
     * @param $data []
     * @return array
     */
    public function combineData($users, $data)
    {
        $usersData = [];

        foreach ($users as $k => $user) {
            if (isset($data[$user['country_id']])) {
                $usersData[] = array_merge($user, $data[$user['country_id']]);
            } else {
                $usersData[] = [
                    'user_id' => $user['user_id'],
                    'user_name' => $user['user_name'],
                    'country_id' => $user['country_id'],
                    'country_name' => $user['country_name'],
                    'file' => $user['file'],
                    'thumb' => $user['thumb'],
                    'buyout_percent' => 0,
                    'approve_percent' => 0,
                    'all' => 0,
                    'sr_cek_kc' => 0
                ];
            }
        }

        return $usersData;
    }
}