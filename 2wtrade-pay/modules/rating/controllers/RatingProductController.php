<?php
namespace app\modules\rating\controllers;

use app\components\web\Controller;
use app\modules\rating\components\ReportFormRatingProduct;
use Yii;

/**
 * Class RatingProductController
 * @package app\modules\report\controllers
 */
class RatingProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormRatingProduct();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

}