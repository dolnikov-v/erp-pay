<?php
use yii\helpers\Html;
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js css-menubar">
    <head>
        <title><?=Yii::t('common', 'Рейтинг 2WTRADE');?></title>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
        <?php $this->head() ?>
    </head>
<body>
<?php $this->beginBody(); ?>

<?php echo $content;?>

<?php $this->endBody(); ?>

<div class="footnote">
<?=Yii::t('common', '* Данные показываются по циклу 15ти дней');?>
</div>
</body>
</html>
<?php $this->endPage() ?>

