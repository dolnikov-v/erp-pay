<?php
use app\modules\media\components\Image;
use app\models\User;
use yii\helpers\Html;
use app\modules\rating\assets\CountryCuratorsAsset;
use app\assets\SiteAsset;

CountryCuratorsAsset::register($this);
SiteAsset::register($this);

$default = '/media/placeholder/avatar-profile.png';

/** @var  app\modules\rating\controllers\CountryCuratorsController $params */
/** @var  app\modules\rating\controllers\CountryCuratorsController $combineData */
?>

<div class="params" id="params_count"><?=$params['count'];?></div>
<div class="params" id="params_interval"><?=$params['interval'];?></div>
<div class="params" id="params_dollar"><?=$params['dollar'];?></div>

<div class="userdata">
    <?php foreach ($combineData as $k => $data): ?>

        <?php
            $model = User::find()->where([User::tableName().'.id' => $data['user_id']])->one();
            $avatar = file_exists($model->getAvatarUrl(true)) ? $model->getAvatarUrl(true) : $default;
        ?>

        <div class="row user " id="u<?=$k;?>">
            <div class="col-md-2 text-center">
                <img src="<?= $avatar; ?>" class="img-thumbnail"
                     alt="<?= $data['user_name']; ?>" width="304" height="236">
                <h4 class="profile-user"><?= $data['user_name']; ?></h4>
            </div>
            <div class="col-md-10">
                <div class="col-lg-12 text-center rounded"><h2><?= $data['country_name']; ?></h2></div>
                <div class="col-lg-4 text-center rounded">
                    <strong>Апрув</strong> <?= number_format($data['approve_percent'], 2); ?><?= ($params['dollar']) ? '%' : ''; ?>
                </div>
                <div class="col-lg-4 text-center rounded"><strong>Ср. чек</strong>
                    $<?= number_format($data['sr_cek_kc'], 2); ?></div>
                <div class="col-lg-4 text-center rounded">
                    <strong>Выкуп</strong> <?= number_format($data['buyout_percent'], 2); ?><?= ($params['dollar']) ? '%' : ''; ?>
                </div>
            </div>
        </div>
        <div class="clear" id="c<?=$k;?>"></div>
    <?php endforeach; ?>
</div>
