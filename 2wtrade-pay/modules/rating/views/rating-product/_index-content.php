<?php
use app\modules\report\extensions\GridViewRating;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var \app\modules\rating\components\ReportFormRatingProduct $reportForm */
?>

<?= GridViewRating::widget([
    'reportForm' => $reportForm,
    'dataProvider' => $dataProvider,
    'resizableColumns' => false,
    'showPageSummary' => false,
    'symbol' => '%',
    'toDollar' => false,
    "exportFilename" =>
        Yii::t('common', 'Рейтинг товаров по выкупу')
        . " (" . join(" - ", $reportForm->getSelectedCountryNames()) . ")"
        . date(" Y-m-d H:i:s")
]) ?>
