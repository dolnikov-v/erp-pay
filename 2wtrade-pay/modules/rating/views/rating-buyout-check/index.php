<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterRatingProduct;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\rating\components\ReportFormRatingBuyoutCheck $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Рейтинг товаров по вык. чеку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Рейтинги'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterRatingProduct::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Рейтинг товаров по вык. чеку'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
