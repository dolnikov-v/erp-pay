<?php
use app\modules\report\extensions\GridViewRating;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var \app\modules\rating\components\ReportFormRatingBuyoutCheck $reportForm */

?>

<?= GridViewRating::widget([
    'reportForm' => $reportForm,
    'dataProvider' => $dataProvider,
    'resizableColumns' => false,
    'showPageSummary' => false,
    'symbol' => '$',
    'toDollar' => true,
    "exportFilename" =>
        Yii::t('common', 'Рейтинг товаров по вык. чеку')
        . " (" . join(" - ", $reportForm->getSelectedCountryNames()) . ")"
        . date(" Y-m-d H:i:s")
]) ?>