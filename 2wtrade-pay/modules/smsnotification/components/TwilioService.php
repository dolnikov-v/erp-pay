<?php

namespace app\modules\smsnotification\components;

use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class TwilioService
 * @package app\components
 */
class TwilioService extends Component
{
    /**
     * @var string
     */
    public $sid;
    /**
     * @var string
     */
    public $token;
    /**
     * @var string
     */
    public $sid_test;
    /**
     * @var string
     */
    public $token_test;
    /**
     * @var bool
     */
    public $debug;
    /**
     * @var string
     */
    public $sender;

    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    /**
     * @param string $phoneTo
     * @param string $phoneFrom
     * @param string $message
     * @param string $defaultPhone
     * @param array $options
     * @return array
     */
    public function sendSmsNotification($phoneTo, $phoneFrom, $message, $defaultPhone, $options = [])
    {
        $module = Yii::$app->getModule('smsnotification');
        $customSmsSender = $module->params['smsSender'];
        $allowSmsSenderCountries = $module->params['countrySupportSmsSender'];
        $char_code = isset($options['country']) ? $options['country']->char_code : false;

        $customSmsSenderFlag = isset($allowSmsSenderCountries[$char_code])
            ? $allowSmsSenderCountries[$char_code]
            : false;

        if (empty($phoneFrom)) {
            $phoneFrom = $customSmsSender;
        }
        if (!$customSmsSenderFlag === false) {
            $phoneFrom = $defaultPhone;
        }

        unset($module);

        $data = [
            'from' => $phoneFrom,
            'body' => $message
        ];

        if (is_array($options)) {
            $data = ArrayHelper::merge($data, $options);
        }

        $client = new Client($this->debug ? $this->sid_test : $this->sid, $this->debug ? $this->token_test : $this->token);

        $result = [
            'status' => self::STATUS_SUCCESS,
        ];

        $phoneTo = "+" . trim($phoneTo, '+');

        try {
            $response = $client->messages->create($phoneTo, $data);
            $result['response'] = $response;
            if (!is_null($response->errorCode)) {
                $result['status'] = self::STATUS_FAIL;
                $result['message'] = $response->errorMessage;
                $response['code'] = $response->errorCode;
            }
        } catch (RestException $e) {
            $result['status'] = self::STATUS_FAIL;
            $result['message'] = $e->getMessage();
            $result['code'] = $e->getCode();
        }

        return $result;
    }

    /**
     * @param string $foreign_id
     * @return array
     */
    public function getNotificationInfo($foreign_id)
    {
        $result = [
            'status' => self::STATUS_FAIL,
        ];

        if (empty($foreign_id)) {
            $result['message'] = Yii::t('common', 'Указан пустой ид смс заявки.');
            return $result;
        }

        $client = new Client($this->debug ? $this->sid_test : $this->sid, $this->debug ? $this->token_test : $this->token);

        try {
            $response = $client->messages($foreign_id)->fetch();
            $result['response'] = $response;
            if (!is_null($response->errorCode)) {
                $result['status'] = self::STATUS_FAIL;
                $result['message'] = $response->errorMessage;
                $response['code'] = $response->errorCode;
            }
        } catch (RestException $e) {
            $result['status'] = self::STATUS_FAIL;
            $result['message'] = $e->getMessage();
            $result['code'] = $e->getCode();
        }

        return $result;
    }
}
