<?php

namespace app\modules\smsnotification\components;

use app\modules\smsnotification\abstracts\SmsNotificationInformationInterface;
use app\modules\smsnotification\abstracts\SmsServiceInterface;
use app\modules\smsnotification\abstracts\SmsServiceResponseInterface;
use app\modules\smsnotification\models\SmsNotificationInformation;
use app\modules\smsnotification\models\SmsServiceResponse;
use Yii;
use yii\base\Component;
use yii\httpclient\Client;

/**
 * Class ZorraService
 * @package app\components
 */
class ZorraService extends Component implements SmsServiceInterface
{
    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $actionAuth = 'auth/login';

    /**
     * @var string
     */
    public $actionSend = 'v1/mailing/single/send';

    /**
     * @var string
     */
    public $actionInfo = 'v1/mailing/single/state';

    /**
     * @var string
     */
    public $smsSender;

    /**
     * @var string
     */
    public $localSmsSender;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $tokenType = 'Bearer';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->auth();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function auth()
    {
        $client = new Client();
        $client->setTransport('yii\httpclient\CurlTransport');
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->url . '/' . $this->actionAuth)
            ->setOptions(['sslVerifyPeer' => false])
            ->setData([
                'email' => $this->login,
                'password' => $this->password,
            ])
            ->send();
        if ($response->isOk && $resultApi = json_decode($response->content)) {
            if (!$resultApi->access_token) {
                throw new \Exception(Yii::t('common', 'Авторизация. Некорректный ответ. Ответ: {response}', ['response' => json_encode($response, JSON_UNESCAPED_UNICODE)]));
            }
            if (!empty($resultApi->token_type)) {
                $this->tokenType = $resultApi->token_type;
            }
            $this->token = $resultApi->access_token;
            return true;
        } else {
            throw new \Exception(Yii::t('common', 'Авторизация. Некорректный ответ. Ответ: {response}', ['response' => json_encode($response, JSON_UNESCAPED_UNICODE)]));
        }
    }

    /**
     * @inheritdoc
     */
    public function sendSmsNotification(string $phoneTo, ?string $phoneFrom, string $message, string $defaultPhone, array $options = []): SmsServiceResponseInterface
    {
        $result = new SmsServiceResponse();
        $result->status = SmsServiceResponseInterface::STATUS_FAIL;
        try {
            if ($result->notificationInformation = $this->send($phoneTo, $phoneFrom, $message, $options)) {
                $result->status = SmsServiceResponseInterface::STATUS_SUCCESS;
            }
        } catch (\Exception $e) {
            $result->message = $e->getMessage();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getNotificationInfo(string $foreign_id): SmsServiceResponseInterface
    {
        $result = new SmsServiceResponse();
        $result->status = self::STATUS_FAIL;

        $client = new Client();
        $client->setTransport('yii\httpclient\CurlTransport');
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($this->url . '/' . $this->actionInfo . '/' . $foreign_id)
            ->setOptions(['sslVerifyPeer' => false])
            ->setHeaders([
                'Content-type' => 'application/json',
                'Authorization' => $this->tokenType . ' ' . $this->token,
            ])->send();

        $result->response = json_encode($response, JSON_UNESCAPED_UNICODE);

        if ($response->isOk && $resultApi = json_decode($response->content)) {
            if ($resultApi->success) {
                $message = $resultApi->message;
                $result->notificationInformation = new SmsNotificationInformation([
                    'sid' => $foreign_id,
                    'foreignStatus' => $resultApi->state,
                    'price' => $message->cost,
                ]);
//                Статусы сообщений 'pending','processing','failed','sent','delivered','not_delivered','expired','rejected'
                $result->status = self::STATUS_FAIL;
                switch ($resultApi->state) {
                    case 'sent':
                    case 'delivered':
                        $result->notificationInformation->status = SmsNotificationInformationInterface::STATUS_DONE;
                        $result->status = self::STATUS_SUCCESS;
                        break;
                    case 'pending':
                        $result->notificationInformation->status = SmsNotificationInformationInterface::STATUS_PENDING;
                        break;
                    case 'processing':
                        $result->notificationInformation->status = SmsNotificationInformationInterface::STATUS_IN_PROGRESS;
                        break;
                    default:
                        $result->notificationInformation->status = SmsNotificationInformationInterface::STATUS_ERROR;
                }
            }
        } else {
            $result->message = Yii::t('common', 'Отправка сообщения не удалась.');
        }

        return $result;
    }

    /**
     * @param string $phoneTo
     * @param null|string $phoneFrom
     * @param string $message
     * @param array $options
     * @return SmsNotificationInformation
     * @throws \Exception
     */
    public function send(string $phoneTo, ?string $phoneFrom, string $message, array $options = []): SmsNotificationInformation
    {
        if (empty($phoneFrom)) {
            $phoneFrom = $this->smsSender;
        }

        if (isset($options['local']) && $options['local'] === true) {
            $phoneFrom = $this->localSmsSender;
        }

        $client = new Client();
        $client->setTransport('yii\httpclient\CurlTransport');
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($this->url . '/' . $this->actionSend)
            ->setOptions(['sslVerifyPeer' => false])
            ->setHeaders([
                'Content-type' => 'application/json; charset=utf-8',
                'Authorization' => $this->tokenType . ' ' . $this->token,
            ])
            ->setData([
                "type" => "sms",
                "sender" => (string)$phoneFrom,
                "body" => (string)$message,
                "recipient" => (string)$phoneTo
            ])->send();

        if ($response->isOk && $resultApi = json_decode($response->content)) {
            if ($resultApi->success && $resultApi->id) {
                return new SmsNotificationInformation(['sid' => $resultApi->id, 'createdAt' => time(), 'status' => SmsNotificationInformationInterface::STATUS_IN_PROGRESS]);
            }
        }
        throw new \Exception(Yii::t('common', 'Отправка сообщения не удалась. Ответ: {response}', ['response' => json_encode($response, JSON_UNESCAPED_UNICODE)]));
    }
}
