<?php

namespace app\modules\smsnotification\models;


use app\components\base\Model;
use app\modules\smsnotification\abstracts\SmsNotificationInformationInterface;
use app\modules\smsnotification\abstracts\SmsServiceResponseInterface;

/**
 * Class SmsServiceResponse
 * @package app\modules\smsnotification\models
 */
class SmsServiceResponse extends Model implements SmsServiceResponseInterface
{
    /**
     * @var SmsNotificationInformationInterface
     */
    public $notificationInformation;

    /**
     * @var string
     */
    public $status, $message, $code, $response;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status', 'notificationInformation'], 'required'],
            [['status', 'message', 'code', 'response'], 'string'],
            ['notificationInformation', 'validateNotificationInformation'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateNotificationInformation(string $attribute): void
    {
        if (!(!empty($this->$attribute) && $this->$attribute instanceof SmsNotificationInformationInterface)) {
            $this->addError($attribute, \Yii::t('common', ''));
        }
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return null|string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }

    /**
     * @return SmsNotificationInformationInterface
     */
    public function getSmsNotificationInformation(): SmsNotificationInformationInterface
    {
        return $this->notificationInformation;
    }
}