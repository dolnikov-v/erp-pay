<?php

namespace app\modules\smsnotification\models;

use app\components\base\Model;
use app\modules\smsnotification\abstracts\SmsNotificationInformationInterface;

/**
 * Class MessageInstance
 * @package app\modules\smsnotification\components\models
 */
class SmsNotificationInformation extends Model implements SmsNotificationInformationInterface
{
    /**
     * @var string
     */
    public $sid, $status, $price, $foreignStatus, $createdAt, $updatedAt, $sentAt, $uri;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status', 'foreignStatus', 'uri'], 'string'],
            [['createdAt', 'updatedAt', 'sentAt'], 'integer'],
            ['price', 'number'],
        ];
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getForeignId(): ?string
    {
        return $this->sid;
    }

    /**
     * @return null|string
     */
    public function getForeignStatus(): ?string
    {
        return $this->foreignStatus;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    /**
     * @return int|null
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

    /**
     * @return int|null
     */
    public function getSentAt(): ?int
    {
        return $this->sentAt ?? $this->createdAt;
    }

    /**
     * @return null|string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }
}