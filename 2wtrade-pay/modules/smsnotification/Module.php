<?php

namespace app\modules\smsnotification;

/**
 * Class Module
 * @package app\modules\smsnotification
 */
class Module extends \app\components\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\smsnotification\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
