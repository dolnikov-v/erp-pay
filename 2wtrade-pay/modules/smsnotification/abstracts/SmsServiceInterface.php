<?php

namespace app\modules\smsnotification\abstracts;

/**
 * Interface SmsServiceInterface
 * @package app\modules\smsnotification\components
 */
interface SmsServiceInterface
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';
    const STATUS_SEND = 'send';

    /**
     * @param string $phoneTo
     * @param null|string $phoneFrom
     * @param string $message
     * @param string $defaultPhone
     * @param array $options
     * @return SmsServiceResponseInterface
     */
    public function sendSmsNotification(string $phoneTo, ?string $phoneFrom, string $message, string $defaultPhone, array $options = []): SmsServiceResponseInterface;

    /**
     * @param string $foreign_id
     * @return SmsServiceResponseInterface
     */
    public function getNotificationInfo(string $foreign_id): SmsServiceResponseInterface;
}