<?php

namespace app\modules\smsnotification\abstracts;

/**
 * Interface SmsNotificationInformationInterface
 * @package app\modules\smsnotification\abstracts
 */
interface SmsNotificationInformationInterface
{
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';
    const STATUS_ERROR = 'error';

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @return string
     */
    public function getForeignId(): ?string;

    /**
     * @return null|string
     */
    public function getForeignStatus(): ?string;

    /**
     * @return float|null
     */
    public function getPrice(): ?float;

    /**
     * @return int|null
     */
    public function getCreatedAt(): ?int;

    /**
     * @return int|null
     */
    public function getUpdatedAt(): ?int;

    /**
     * @return int|null
     */
    public function getSentAt(): ?int;

    /**
     * @return null|string
     */
    public function getUri(): ?string;
}