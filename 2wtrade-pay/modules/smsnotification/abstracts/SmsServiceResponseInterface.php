<?php

namespace app\modules\smsnotification\abstracts;

/**
 * Interface SmsServiceSendResponseInterface
 * @package app\modules\smsnotification\abstracts
 */
interface SmsServiceResponseInterface
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @return null|string
     */
    public function getMessage(): ?string;

    /**
     * @return null|string
     */
    public function getCode(): ?string;

    /**
     * @return null|string
     */
    public function getResponse(): ?string;

    /**
     * @return SmsNotificationInformationInterface
     */
    public function getSmsNotificationInformation(): SmsNotificationInformationInterface;
}