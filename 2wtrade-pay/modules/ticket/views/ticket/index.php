<?php
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\ticket\widgets\LinkNav;
use app\models\User;
use app\modules\ticket\helpers\TicketApi;
use app\models\Country;
use app\models\TicketGroup;
use app\modules\ticket\models\Ticket;
use app\components\grid\DateColumn;
use app\widgets\Label;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var bool $showInbox
 * @var User $currentUser
 * @var bool $showRoleFilter
 */

$this->title = Yii::t('common', 'Тикеты');

$tabs = [
    [
        'label' => Yii::t('common', 'Мои'),
    ],
];
?>

<?php if ($showInbox) {
    $tabs[] = ['label' => Yii::t('common', 'Новые')];
    $tabs[] = ['label' => Yii::t('common', 'Взятые в работу')];
    $tabs[] = ['label' => Yii::t('common', 'Закрытые')];
} ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'user' => $currentUser,
        'showRoleFilter' => $showRoleFilter
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'nav' => new LinkNav([
        'getParameter' => 'tab',
        'tabs' => $tabs,
    ]),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($data, $index, $widget, $grid) {
            if ($data->status == Ticket::STATUS_CLOSED) {
                return ['class' => 'text-muted'];
            } elseif ($data->has_new_message) {
                return ['class' => 'info'];
            } else {
                return [];
            }
        },
        'columns' => [
            [
                'label' => Yii::t('common', '#'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->id, Url::toRoute(['view', 'id' => $data->id]));
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'importantName',
                'value' => function ($data) {
                    return Label::widget([
                        'label' => $data->importantName,
                        'style' => $data->importantStyle
                    ]);
                }
            ],
            [
                'attribute' => 'statusName'
            ],
            [
                'attribute' => 'group.name'
            ],
            [
                'attribute' => 'country.name'
            ],
            [
                'attribute' => 'theme_id'
            ],
            [
                'attribute' => 'takenUsername'
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'changed'
            ],
            [
                'label' => Yii::t('common', 'Последний пользователь'),
                'attribute' => 'username'
            ],
        ],
    ]),
    'footer' => ButtonLink::widget([
            'url' => Url::toRoute('create'),
            'label' => Yii::t('common', 'Задать вопрос'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) . (!empty($dataProvider->pagination) ? LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ]) : ""),
])
?>
