<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Html;
use app\widgets\InputGroupFile;
use app\widgets\InputText;

/**
 * @var \app\modules\ticket\models\MessageForm $model
 */
?>

<?php $form = ActiveForm::begin([
    'id' => 'ticket-form',
    'options' => [
        'class' => 'new-tckt-form mb70',
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'control-label fs18 bold mb15'
        ],
    ]
]); ?>

<div class="has-error"><?= $form->errorSummary($model); ?></div>

<div class="form-group">
    <?= $form->field($model, 'message')->textarea(['class' => 'form-control bg-f0', 'rows' => 5]) ?>
</div>

<div class="row">
    <div class="col-sm-6 col-xs-12">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'id' => 'messageform-file',
                'type' => 'file',
                'name' => 'MessageForm[file]',
            ]),
        ]) ?>
    </div>

    <div class="col-md-6 text-right col-xs-12">
        <?= $form->submit(Yii::t('common', 'Написать сообщение'),
            ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </div>
</div>
<?php $form->end(); ?>
