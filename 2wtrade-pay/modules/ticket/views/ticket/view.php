<?php

use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\custom\DropdownActions;
use app\widgets\Label;

/**
 * @var \app\modules\ticket\models\MessageForm $model
 * @var \app\modules\ticket\models\Ticket $ticket
 * @var \app\modules\ticket\models\Message[] $messages
 * @var array $actions
 */

$this->title = Yii::t('common', 'Тикет #{id}',
        ['id' => $ticket->id]) . ($ticket->status == 'closed' ? ' (' . Yii::t('common', 'Закрыт') . ')' : '');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Тикеты'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'id' => 'ticket_view_info',
    'actions' => DropdownActions::widget([
        'links' => $actions
    ]),
    'title' => Yii::t('common', 'Информация'),
    'content' => $this->render('_ticket-info', ['ticket' => $ticket]),
    'collapse' => true
]) ?>

<?= Panel::widget([
    'id' => 'ticket_view_messages',
    'title' => Yii::t('common', 'Сообщения') .Label::widget([
            'label' => count($messages),
            'style' => Label::STYLE_SUCCESS
        ]),
    'content' => $this->render('_ticket-messages', ['messages' => $messages]),
    'collapse' => true
]) ?>

<?php
if ($ticket['status'] != 'closed') {
    echo Panel::widget([
        'title' => Yii::t('common', 'Написать сообщение'),
        'content' => $this->render('_message-form', ['model' => $model])
    ]);
}
?>
