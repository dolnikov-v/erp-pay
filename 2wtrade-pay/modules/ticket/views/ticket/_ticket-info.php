<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\WbIcon;
use yii\widgets\DetailView;
use app\models\User;
use app\modules\ticket\helpers\TicketApi;
use app\models\TicketGroup;
use app\components\grid\DateColumn;
use app\widgets\Label;

/**
 * @var \app\modules\ticket\models\Ticket $ticket
 */
?>

<div class="row">
    <div class="col-lg-12">
        <?= DetailView::widget([
            'model' => $ticket,
            'attributes' => [
                [
                    'attribute' => 'username'
                ],
                [
                    'attribute' => 'group.name',
                ],
                [
                    'attribute' => 'country.name'
                ],
                [
                    'attribute' => 'theme_id'
                ],
                [
                    'attribute' => 'statusName'
                ],
                [
                    'attribute' => 'takenUsername'
                ],
                [
                    'format' => 'raw',
                    'attribute' => 'importantName',
                    'value' => Label::widget([
                        'label' => $ticket->importantName,
                        'style' => $ticket->importantStyle
                    ])
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'created',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'changed',
                ]
            ]
        ]) ?>
    </div>
</div>
