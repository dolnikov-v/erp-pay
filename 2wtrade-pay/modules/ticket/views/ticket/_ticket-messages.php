<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\WbIcon;
use app\models\User;
use app\widgets\Label;
use app\modules\ticket\models\Message;

/**
 * @var \app\modules\ticket\models\Message[] $messages
 */
?>

<div class="messages">
    <?php foreach ($messages as $message): ?>
        <div class="media">
            <div class="media-body">
                <h5>
                    <i class="icon <?= \app\helpers\WbIcon::USER ?>"></i>
                    <?= $message->username ?>
                    <?php if ($message->status == Message::STATUS_NEW && $message->user_id != Yii::$app->user->id) echo Label::widget([
                        'label' => Yii::t('common', 'Новое'),
                        'style' => Label::STYLE_PRIMARY
                    ]) ?>
                </h5>
                <h6><?= Yii::$app->formatter->asDatetime($message->created) ?></h6>
                <p class="light"><?= Html::encode($message->message) ?></p>
                <?= (!empty($message->file)) ? Html::a(Yii::t('common', 'Скачать файл'),
                    Url::toRoute(['download-file']) . '/' . $message->file,
                    ['target' => '_blank', 'class' => 'icon ' . WbIcon::DOWNLOAD]) : '' ?>
            </div>
        </div>
        <hr>
    <?php endforeach; ?>
</div>
