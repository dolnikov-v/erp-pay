<?php
use yii\helpers\Url;
use app\widgets\Panel;

/**
 * @var \app\modules\ticket\models\TicketForm $model
 * @var array $categoryList
 * @var array $categoryListChild
 */

$this->title = Yii::t('common', 'Задать вопрос');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Тикеты'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
   'content' =>  $this->render('_form',
       ['model' => $model, 'categoryList' => $categoryList, 'categoryListChild' => $categoryListChild])
]); ?>
