<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Country;
use yii\helpers\ArrayHelper;
use app\widgets\Select2;
use yii\helpers\Html;

/**
 * @var \app\models\User $user
 * @var bool $showRoleFilter
 */

?>

<?php $form = ActiveForm::begin([
    'method' => 'get'
]); ?>
<div class="row">
    <div class="col-lg-<?=$showRoleFilter ? '6' : '12'?> form-group">
        <?= Html::label(Yii::t('common', 'Страна'), 'country') ?>
        <?= Select2::widget([
            'name' => 'country',
            'value' => Yii::$app->request->get('country'),
            'items' => ArrayHelper::map($user->isSuperadmin ? Country::find()->active()->all() : $user->countries,
                'slug', 'name'),
            'prompt' => Yii::t('common', 'Все страны')
        ]) ?>
    </div>
    <?php if($showRoleFilter): ?>
        <div class="col-lg-6 form-group">
            <?= Html::label(Yii::t('common', 'Группа'), 'ticket_prefix') ?>
            <?= Select2::widget([
                'name' => 'ticket_prefix',
                'value' => Yii::$app->request->get('ticket_prefix'),
                'items' => ArrayHelper::map($user->inboxTicketGroups,
                    'prefix', 'name'),
                'prompt' => Yii::t('common', 'Все группы')
            ]) ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
