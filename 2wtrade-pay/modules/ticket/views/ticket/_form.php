<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Html;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\ArrayHelper;
use app\models\Country;
use app\models\User;
use app\modules\ticket\models\Ticket;

/**
 * @var \app\modules\ticket\models\TicketForm $model
 * @var array $categoryList
 * @var array $categoryListChild
 */

$user = User::findIdentity(Yii::$app->user->id);
?>

<?php $form = ActiveForm::begin([
    'id' => 'ticket-form',
    'options' => [
        'class' => 'new-tckt-form mb70',
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'control-label fs18 bold mb15'
        ],
    ]
]); ?>

<div class="form-group b1">
    <span id="first-select">
        <?= $form->field($model,
            'country')->select2List(ArrayHelper::map($user->isSuperadmin ? Country::find()->active()->all() : User::findIdentity(Yii::$app->user->id)->countries,
            'slug',
            'name'),
            ['class' => 'form-control first-select', 'prompt' => Yii::t('common', 'Выберите страну')]) ?>
    </span>
    <span id="second-select col-lg-6">
        <?= $form->field($model, 'theme')->select2List($categoryList,
            ['class' => 'form-control first-select', 'prompt' => Yii::t('common', 'Тема вопроса')]) ?>
    </span>
    <span id="third-select col-lg-6">
        <?= $form->field($model, 'important')->select2List(Ticket::availableImportants(),
            ['class' => 'form-control first-select', 'prompt' => Yii::t('common', 'Выберите приоритет')]) ?>
    </span>
</div>
<hr>

<div class="form-group b2">
    <?= $form->field($model, 'message')->textarea(['class' => 'form-control', 'rows' => 5]) ?>
</div>

<div class="row">
    <div class="col-sm-6 col-xs-12">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'id' => 'ticketform-file',
                'type' => 'file',
                'name' => 'TicketForm[file]',
            ]),
        ]) ?>
    </div>

    <div class="col-md-6 text-right">
        <?= $form->submit(Yii::t('common', 'Задать вопрос'),
            ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </div>
</div>

<?php
$form->end();

$script = "";

foreach ($categoryListChild as $idParent => $child) {
    $appends = "";
    foreach ($child as $v => $k) {
        $appends .= '$(selectId + " select").append($(\'<option value="' . $v . '">' . $k . '</option>\'));';
    }

    $script .= <<< JS
if($(this).val() == $idParent) {
    $(selectId).html($("#first-select").html());
    $(selectId + " select").empty();
    $(selectId + " label").remove();
    $appends
    return false;
}
JS;
}

$script = <<< JS
$(document).ready(function () {
        $(".first-select").change(function () {
            var selectId = '#second-select';
            $script
            $(selectId).html('');
        });
    });
JS;

$this->registerJs($script);
?>

