<?php
namespace app\modules\ticket\controllers;

use app\components\web\Controller;
use app\models\Country;
use app\models\User;
use app\modules\ticket\helpers\TicketApi;
use app\modules\ticket\models\Message;
use app\modules\ticket\models\MessageForm;
use app\modules\ticket\models\Ticket;
use app\modules\ticket\models\TicketForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\widgets\LinkPager;
use app\models\TicketGroup;

/**
 * Class TicketController
 * @package app\modules\ticket\controllers
 */
class TicketController extends Controller
{
    /**
     * @return string
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionIndex()
    {
        $tab = Yii::$app->request->get('tab');
        $user = User::findOne(Yii::$app->user->id);
        $ticketRoles = $user->inboxTicketPrefix;
        $userRoles = [];

        $showRoleFilter = true;
        if (count($ticketRoles) < 2) {
            $showRoleFilter = false;
        }

        $country = Yii::$app->request->get('country');
        $choosedRole = Yii::$app->request->get('ticket_prefix');

        if (!empty($choosedRole)) {
            $ticketRoles = [$choosedRole];
        }

        if (!empty($country)) {
            $buffer = [];
            foreach ($ticketRoles as $role) {
                $buffer[] = "{$role}.{$country}";
            }
            $ticketRoles = $buffer;
            $userRoles[] = "{$user->ticketRole}.{$country}";
        } else {
            $countries = $user->isSuperadmin ? Country::find()->active()->all() : $user->countries;
            $buffer = [];
            foreach ($countries as $country) {
                foreach ($ticketRoles as $role) {
                    $buffer[] = "{$role}.{$country->slug}";
                }
            }
            $ticketRoles = $buffer;
        }

        switch ($tab) {
            case 1:
                $query = http_build_query([
                    'roles' => base64_encode(json_encode($ticketRoles)),
                    'unlink_user' => Yii::$app->user->can('ticket.ticket.showallticket'),
                ]);
                $ticketList = TicketApi::api('untakenTicketList', [], $query);
                break;
            case 2:
                $query = http_build_query([
                    'roles' => base64_encode(json_encode($ticketRoles)),
                    'unlink_user' => Yii::$app->user->can('ticket.ticket.showallticket'),
                ]);
                $ticketList = TicketApi::api('takenTicketList', [], $query);
                break;
            case 3:
                $query = http_build_query([
                    'roles' => base64_encode(json_encode($ticketRoles)),
                    'unlink_user' => Yii::$app->user->can('ticket.ticket.showallticket'),
                ]);
                $ticketList = TicketApi::api('closedTicketList', [], $query);
                break;
            default:
                $showRoleFilter = false;
                $query = http_build_query([
                    'roles' => base64_encode(json_encode($userRoles))
                ]);
                $ticketList = TicketApi::api('getTickets', [], $query);
        }

        $tickets = [];
        for ($i = 0; $i < count($ticketList); $i++) {
            $tickets[] = new Ticket();
        }
        Ticket::loadMultiple($tickets, $ticketList, '');

        $provider = new ArrayDataProvider([
            'allModels' => $tickets,
            'pagination' => []
        ]);

        LinkPager::setPageSize($provider->pagination);

        return $this->render('index', [
            'dataProvider' => $provider,
            'showInbox' => !empty($ticketRoles),
            'currentUser' => $user,
            'showRoleFilter' => $showRoleFilter
        ]);
    }

    /**
     * @return string
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new TicketForm;

        if ($model->load(Yii::$app->request->post())) {
            $filename = '';
            $file = UploadedFile::getInstance($model, 'file');

            if (!empty($file->name)) {
                $filename = md5(time() . $file->name) . '.' . $file->extension;
            }

            if ($model->validate()) {
                if (!empty($file->name)) {
                    $file->saveAs(TicketForm::getUploadPath() . $filename);
                }

                $user = User::findOne(Yii::$app->user->id);
                $ticketRole = $user->ticketRole;

                $ticketRole .= ".{$model->country}";

                $postToTicket = [];
                $postToTicket['Ticket']['user_id'] = Yii::$app->user->id;
                $postToTicket['Ticket']['theme_id'] = $model->theme;
                $postToTicket['Ticket']['important'] = $model->important;
                $postToTicket['Ticket']['role'] = $ticketRole;

                $postToTicket['Message']['message'] = $model->message;
                $postToTicket['Message']['username'] = Yii::$app->user->identity->username;
                $postToTicket['Message']['user_id'] = Yii::$app->user->id;
                $postToTicket['Message']['file'] = $filename;

                $ticket = TicketApi::api('saveNEWTicket', $postToTicket);

                if (!empty($ticket['id'])) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Сообщение успешно отправлено.'));
                    return $this->redirect(Url::to(['/ticket/ticket/view', 'id' => $ticket['id']]));
                } else {
                    Yii::$app->getSession()->setFlash('error',
                        Yii::t('common', 'Не удалось отправить сообщение, попытайтесь снова через несколько минут.'));
                    return $this->redirect(Url::to(['/ticket/ticket/index']));
                }
            }
        }

        if (empty($model->country)) {
            $model->country = Yii::$app->user->country->slug;
        }

        $categoryList = TicketApi::api('getCategoryList');

        $list = [];
        $listChild = [];

        foreach ($categoryList as $categories) {
            $i = 0;
            $parentCat = 0;

            foreach ($categories as $category) {
                if (count($categories) > 1) {
                    if ($i == 0) {
                        $list[$category['id']] = $category['name'];
                        $parentCat = $category['id'];
                    } else {
                        $listChild[$parentCat][$category['id']] = $category['name'];
                    }
                } else {
                    $list[$category['id']] = $category['name'];
                }

                $i++;
            }
        }

        return $this->render('create', [
            'model' => $model,
            'categoryList' => $list,
            'categoryListChild' => $listChild
        ]);
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionView($id)
    {
        $model = new MessageForm;

        if ($model->load(Yii::$app->request->post())) {
            $filename = '';
            $file = UploadedFile::getInstance($model, 'file');

            if (!empty($file->name)) {
                $filename = md5(time() . $file->name) . '.' . $file->extension;
            }

            if ($model->validate()) {
                if (!empty($file->name)) {
                    $file->saveAs(TicketForm::getUploadPath() . $filename);
                }

                $postToMessage = [];
                $postToMessage['Message']['message'] = $model->message;
                $postToMessage['Message']['username'] = Yii::$app->user->identity->username;
                $postToMessage['Message']['user_id'] = Yii::$app->user->id;
                $postToMessage['Message']['file'] = $filename;
                $postToMessage['Message']['ticket_id'] = $id;

                $message = TicketApi::api('saveNEWMessage', $postToMessage);

                if (!empty($message['id'])) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Сообщение успешно отправлено.'));
                    return $this->redirect(Url::to(['/ticket/ticket/view', 'id' => $id]));
                } else {
                    Yii::$app->getSession()->setFlash('error',
                        Yii::t('common', 'Не удалось отправить сообщение, попытайтесь снова через несколько минут.'));
                    return $this->redirect(Url::to(['/ticket/ticket/index']));
                }
            }
        }

        $infoList = TicketApi::api('getTicket', [], '&show_id=' . $id);

        $ticket = new Ticket();

        $ticket->load($infoList['ticket'], '');

        $messages = [];
        for ($i = 0; $i < count($infoList['messages']); $i++) {
            $messages[] = new Message();
        }
        Message::loadMultiple($messages, $infoList['messages'], '');

        $actions = [];

        if (($ticket->status == Ticket::STATUS_CREATED || $ticket->status == Ticket::STATUS_MOVED) && Yii::$app->user->can('ticket.ticket.takejob')) {
            if ($ticket->user_id != Yii::$app->user->id) {
                $actions[] = [
                    'label' => Yii::t('common', 'Взять в работу'),
                    'url' => Url::toRoute(['take-job', 'id' => $ticket->id])
                ];
            }
        }
        if (Yii::$app->user->can('ticket.ticket.closeticket') && $ticket->status != Ticket::STATUS_CLOSED && ($ticket->user_id == Yii::$app->user->id || ($ticket->take_user == Yii::$app->user->id))) {
            $actions[] = [
                'label' => Yii::t('common', 'Закрыть тикет'),
                'url' => Url::toRoute(['close-ticket', 'id' => $ticket->id])
            ];
        }

        return $this->render('view', [
            'ticket' => $ticket,
            'messages' => $messages,
            'model' => $model,
            'actions' => $actions
        ]);
    }

    /**
     * @param integer $id
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionTakeJob($id)
    {
        $query = http_build_query([
            'ticket_id' => $id
        ]);
        TicketApi::api('takeTicket', [], $query);

        $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param integer $id
     */
    public function actionCloseTicket($id)
    {
        $query = http_build_query([
            'ticket_id' => $id
        ]);
        TicketApi::api('closeTicket', [], $query);

        $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $name
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($name)
    {
        $path = TicketForm::getUploadPath() . $name;

        if (file_exists($path)) {
            Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Файл не найден.'));
        }
    }
}
