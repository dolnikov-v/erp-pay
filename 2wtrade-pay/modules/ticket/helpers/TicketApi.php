<?php
namespace app\modules\ticket\helpers;

use yii\web\BadRequestHttpException;
use yii\helpers\Json;
use app\models\User;
use Yii;

/**
 * Class TicketApi
 * @package app\modules\ticket\helpers
 */
class TicketApi
{
    /**
     * @param $type
     * @param array $post
     * @param string $params
     * @return mixed
     * @throws BadRequestHttpException
     */
    public static function api($type, $post = [], $params = '')
    {
        $user = User::findIdentity(Yii::$app->user->id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,
            Yii::$app->controller->module->params['siteTicket'] . $type . '?user_id=' . Yii::$app->user->id . '&username=' . $user->username . '&' . $params);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, Yii::$app->controller->module->params['userTicket']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        if (count($post)) {
            $post_string = http_build_query($post);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        }

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($info['http_code'] == 200) {
            return Json::decode($result, true);
        } else {
            throw new BadRequestHttpException(Yii::t('common', 'Тикетная система в данный момент недоступна.'));
        }
    }
}
