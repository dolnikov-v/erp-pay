<?php

namespace app\modules\ticket\models;


use app\models\User;
use yii\base\Model;
/**
 * Class Ticket
 * @package app\modules\ticket\models
 *
 * @property string $username
 */
class Message extends Model
{
    const STATUS_NEW = 'new';
    const STATUS_READ = 'read';

    /**
     * @var integer
     */
    public $id, $ticket_id, $user_id, $user_ticket, $created;

    /**
     * @var string
     */
    public $message, $status, $file;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var User
     */
    public $user;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'ticket_id', 'user_id', 'user_ticket', 'created'], 'integer'],
            [['message', 'status', 'username', 'file'],'string'],
        ];
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        if (is_null($this->user)) {
            $this->user = User::findOne($this->user_id);
        }

        return (empty($this->user) ? $this->username : $this->user->username);
    }
}
