<?php
namespace app\modules\ticket\models;

use Yii;
use yii\base\Model;

/**
 * Class MessageForm
 * @package app\modules\ticket\models
 */
class MessageForm extends Model
{

    public $message;
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['message'], 'string'],
            [['file'], 'file', 'extensions' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message' => Yii::t('common', 'Сообщение'),
            'file' => Yii::t('common', 'Файл'),
        ];
    }
}
