<?php

namespace app\modules\ticket\models;


use app\models\TicketGroup;
use app\models\User;
use app\widgets\Label;
use yii\base\Model;
use app\models\Country;
use Yii;

/**
 * Class Ticket
 * @package app\modules\ticket\models
 *
 * @property Country $country
 * @property TicketGroup $group
 * @property string $username
 * @property string $takenUsername
 * @property string $statusName
 * @property string $importantName
 *
 * @property string $importantStyle
 */
class Ticket extends Model
{
    const STATUS_CREATED = 'created';
    const STATUS_TAKEN = 'taken';
    const STATUS_CLOSED = 'closed';
    const STATUS_MOVED = 'moved';

    const IMPORTANT_NORMALLY = 'normaly';
    const IMPORTANT_SPEED = 'speed';
    const IMPORTANT_FULL_SPEED = 'fullspeed';
    const IMPORTANT_HIGH_SPEED = 'hightspeed';

    const IMPORTANT_BLOCKER = 'blocker';
    const IMPORTANT_CRITICAL = 'critical';
    const IMPORTANT_MAJOR = 'major';
    const IMPORTANT_MINOR = 'minor';
    const IMPORTANT_TRIVIAL = 'trivial';

    /**
     * @var integer
     */
    public $id, $user_id, $project_id, $theme_id, $group_id, $userTake, $take_user;

    /**
     * @var string
     */
    public $status, $important, $auth_url, $created, $changed, $role, $ticketTheme, $sendUsername, $ticketProject, $ticketUser, $ticketGroup, $faviconProjects;

    /**
     * @var boolean
     */
    public $has_new_message, $from_ticket;

    /**
     * @var Country
     */
    protected $_country;

    /**
     * @var TicketGroup
     */
    protected $_group;

    /**
     * @var User
     */
    public $user;

    /**
     * @var User
     */
    public $takenUser;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'project_id', 'theme_id', 'group_id', 'userTake', 'take_user'], 'integer'],
            [
                [
                    'status',
                    'important',
                    'auth_url',
                    'created',
                    'changed',
                    'role',
                    'ticketTheme',
                    'sendUsername',
                    'ticketProject',
                    'ticketUser',
                    'ticketGroup',
                    'faviconProjects'
                ],
                'string'
            ],
            [['has_new_message', 'from_ticket'], 'boolean'],
        ];
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        if (is_null($this->_country)) {
            if (preg_match('/.+\.([^\.]+)$/', $this->role, $match)) {
                if (isset($match[1])) {
                    $match = $match[1];
                    $country = Country::find()->where(['slug' => $match])->one();
                    if (!empty($country)) {
                        $this->_country = $country;
                    }
                }
            }
        }

        return $this->_country;
    }

    /**
     * @return TicketGroup|array|null|\yii\db\ActiveRecord
     */
    public function getGroup()
    {
        if (is_null($this->_group)) {
            if (preg_match('/^(.+)\.[^\.]+$/', $this->role, $match)) {
                if (isset($match[1])) {
                    $match = $match[1];
                    $group = TicketGroup::find()->where(['prefix' => $match])->one();
                    if (!empty($group)) {
                        $this->_group = $group;
                    }
                }
            }
        }

        return $this->_group;
    }

    /**
     * @return int|string
     */
    public function getUsername()
    {
        if (is_null($this->user)) {
            $this->user = User::findOne($this->user_id);
        }

        return (empty($this->user) ? $this->user_id : $this->user->username);
    }

    /**
     * @return int|string
     */
    public function getTakenUsername()
    {
        if (is_null($this->takenUser)) {
            $this->takenUser = User::findOne($this->take_user);
        }

        return (empty($this->takenUser) ? $this->take_user : $this->takenUser->username);
    }

    /**
     * @return mixed|string
     */
    public function getStatusName()
    {
        $labels = static::statusLabels();

        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }

        return $this->status;
    }

    /**
     * @return mixed|string
     */
    public function getImportantName()
    {
        $labels = static::importantLabels();

        if (isset($labels[$this->important])) {
            return $labels[$this->important];
        }

        return $this->important;
    }

    /**
     * @return mixed|string
     */
    public function getImportantStyle()
    {
        $styles = self::importantStyles();
        if (isset($styles[$this->important])) {
            return $styles[$this->important];
        }
        return '';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'status' => Yii::t('common', 'Статус'),
            'statusName' => Yii::t('common', 'Статус'),
            'important' => Yii::t('common', 'Приоритет'),
            'importantName' => Yii::t('common', 'Приоритет'),
            'username' => Yii::t('common', 'Добавил'),
            'takenUsername' => Yii::t('common', 'Взял в работу'),
            'group.name' => Yii::t('common', 'Группа'),
            'country.name' => Yii::t('common', 'Страна'),
            'theme_id' => Yii::t('common', 'Тема'),
            'created' => Yii::t('common', 'Дата создания'),
            'changed' => Yii::t('common', 'Дата обновления')
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_CREATED => Yii::t('common', 'Создан'),
            self::STATUS_TAKEN => Yii::t('common', 'Взят в работу'),
            self::STATUS_CLOSED => Yii::t('common', 'Закрыт'),
            self::STATUS_MOVED => Yii::t('common', 'Перенаправлен'),
        ];
    }

    /**
     * @return array
     */
    public static function importantLabels()
    {
        return [
            self::IMPORTANT_NORMALLY => Yii::t('common', 'Низкий'),
            self::IMPORTANT_SPEED => Yii::t('common', 'Обычная'),
            self::IMPORTANT_FULL_SPEED => Yii::t('common', 'Срочно'),
            self::IMPORTANT_HIGH_SPEED => Yii::t('common', 'Очень срочно'),
            self::IMPORTANT_BLOCKER => Yii::t('common', 'Теряем деньги'),
            self::IMPORTANT_CRITICAL => Yii::t('common', 'Партнеры теряют деньги'),
            self::IMPORTANT_MAJOR => Yii::t('common', 'Важный'),
            self::IMPORTANT_MINOR => Yii::t('common', 'Обычный'),
            self::IMPORTANT_TRIVIAL => Yii::t('common', 'Низкий'),
        ];
    }

    /**
     * @return array
     */
    public static function availableImportants()
    {
        $labels = self::importantLabels();
        return [
            self::IMPORTANT_BLOCKER => $labels[self::IMPORTANT_BLOCKER],
            self::IMPORTANT_CRITICAL => $labels[self::IMPORTANT_CRITICAL],
            self::IMPORTANT_MAJOR => $labels[self::IMPORTANT_MAJOR],
            self::IMPORTANT_MINOR => $labels[self::IMPORTANT_MINOR],
            self::IMPORTANT_TRIVIAL => $labels[self::IMPORTANT_TRIVIAL]
        ];
    }

    /**
     * @return array
     */
    public static function importantStyles()
    {
        return [
            self::IMPORTANT_BLOCKER => Label::STYLE_DANGER,
            self::IMPORTANT_CRITICAL => Label::STYLE_DANGER,
            self::IMPORTANT_MAJOR => Label::STYLE_WARNING,
            self::IMPORTANT_MINOR => Label::STYLE_PRIMARY,
            self::IMPORTANT_TRIVIAL => Label::STYLE_SUCCESS,
            self::IMPORTANT_HIGH_SPEED => Label::STYLE_DANGER,
            self::IMPORTANT_FULL_SPEED => Label::STYLE_WARNING,
            self::IMPORTANT_SPEED => Label::STYLE_PRIMARY,
            self::IMPORTANT_NORMALLY => Label::STYLE_SUCCESS
        ];
    }
}
