<?php
namespace app\modules\ticket\models;

use app\helpers\Utils;
use Yii;
use yii\base\Model;

/**
 * Class TicketForm
 * @package app\modules\ticket\models
 */
class TicketForm extends Model
{
    public $theme;
    public $message;
    public $file;
    public $country;
    public $important;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['theme', 'message', 'country', 'important'], 'required'],
            [['message', 'important'], 'string'],
            [['file'], 'file', 'extensions' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'theme' => Yii::t('common', 'Тема вопроса'),
            'message' => Yii::t('common', 'Сообщение'),
            'file' => Yii::t('common', 'Файл'),
            'country' => Yii::t('common', 'Страна'),
            'important' => Yii::t('common', 'Приоритет')
        ];
    }

    /**
     * @param $message
     * @return string
     */
    public static function getUrlUpload($message)
    {
        $url = self::getUploadPath() . $message['file'];

        return $url;
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function getUploadPath()
    {
        $dir = Yii::getAlias('@tickets') . DIRECTORY_SEPARATOR;
        Utils::prepareDir($dir);

        return $dir;
    }
}
