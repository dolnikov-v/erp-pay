<?php

namespace app\modules\ticket\widgets;

use app\widgets\Nav;

/**
 * Class LinkNav
 * @package app\modules\ticket\widgets
 */
class LinkNav extends Nav
{
    /**
     * @var int
     */
    public $currentTab = 0;

    /**
     * @var string
     */
    public $getParameter = 'tab';

    /**
     * @return string
     */
    public function renderTabs()
    {
        if (!$this->currentTab && \Yii::$app->request->get($this->getParameter)) {
            $this->currentTab = \Yii::$app->request->get($this->getParameter);
        }
        return $this->render('link-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
            'currentTab' => $this->currentTab,
            'getParam' => $this->getParameter
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return "";
    }
}
