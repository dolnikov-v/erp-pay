<?php

use app\modules\widget\widgets\GridStack;

/** @var \yii\web\View $this */
/** @var \app\modules\widget\models\WidgetUser[] $widgets */
/** @var \app\modules\widget\models\WidgetType[] $widgetTypes */

/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Домашняя страница');
?>

<?php if (!Yii::$app->user->country->id) { ?>

    <?= \yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-danger',
        ],
        'body' => Yii::t('common', 'Ни одна страна к вам не привязана.'),
    ]); ?>

<?php } else { ?>

    <?= GridStack::widget([
        'widgets' => $widgets,
        'widgetTypes' => $widgetTypes,
    ]); ?>

<?php } ?>


