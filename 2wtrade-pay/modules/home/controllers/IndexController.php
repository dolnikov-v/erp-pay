<?php
namespace app\modules\home\controllers;

use app\components\web\Controller;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\models\WidgetType;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class IndexController
 * @package app\modules\home\controllers
 */
class IndexController extends Controller
{
    /**
     * @return string
     */

    public function actionIndex()
    {
        $widgets = WidgetUser::find()
            ->joinWith(['type'])
            ->joinWith(['cache'])
            ->where([
                'user_id' => Yii::$app->user->id,
                WidgetType::tableName() . '.status' => WidgetType::STATUS_ACTIVE,
            ])
            ->all();

        $usedTypes = ArrayHelper::getColumn($widgets, 'type_id');

        $roles = Yii::$app->getAuthManager()->getRolesByUser(Yii::$app->user->id);
        $roles = ArrayHelper::getColumn($roles, 'name');

        if (Yii::$app->user->isSuperadmin) {
            $widgetTypes = WidgetType::find()
                ->orderBy(['name' => SORT_ASC])
                ->all();
        } else {
            $widgetTypes = WidgetType::find()
                ->joinWith(['roles'])
                ->where([
                    'status' => WidgetType::STATUS_ACTIVE,
                    'widget_role.role' => $roles,
                ])
                ->andWhere(['not in', WidgetType::tableName() . '.id', $usedTypes])
                ->orderBy(['name' => SORT_ASC])
                ->all();
        }

        return $this->render('index', [
            'widgets' => $widgets,
            'widgetTypes' => $widgetTypes,
        ]);
    }
}
