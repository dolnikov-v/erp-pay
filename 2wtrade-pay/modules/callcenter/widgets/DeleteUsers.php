<?php

namespace app\modules\callcenter\widgets;

/**
 * Class DeleteUsers
 * @package app\modules\callcenter\widgets
 */
class DeleteUsers extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-users/modal', [
            'url' => $this->url
        ]);
    }
}
