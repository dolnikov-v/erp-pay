<?php

namespace app\modules\callcenter\widgets;


use app\components\widgets\ActiveForm;
use app\widgets\Widget;
use app\modules\callcenter\models\search\filters\CheckingTypeFilter as Filter;

/**
 * Class CheckingTypeFilter
 * @package app\modules\callcenter\widgets
 */
class CheckingTypeFilter extends Widget
{
    /**
     * @var array
     */
    public $types = [];

    /**
     * @var array
     */
    public $informations = [];

    /**
     * @var Filter
     */
    public $filter;

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('checking-type-filter/filter', [
            'types' => $this->types,
            'informations' => $this->informations,
            'filter' => $this->filter,
            'form' => $this->form
        ]);
    }
}
