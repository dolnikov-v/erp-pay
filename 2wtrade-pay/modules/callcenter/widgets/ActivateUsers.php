<?php

namespace app\modules\callcenter\widgets;

use app\modules\callcenter\models\CallCenterUser;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * Class ActivateUsers
 * @package app\modules\callcenter\widgets
 */
class ActivateUsers extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('activate-users/modal', [
            'url' => $this->url
        ]);
    }
}
