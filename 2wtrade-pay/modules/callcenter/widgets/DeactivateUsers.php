<?php

namespace app\modules\callcenter\widgets;

use app\modules\callcenter\models\CallCenterUser;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * Class DeactivateUsers
 * @package app\modules\callcenter\widgets
 */
class DeactivateUsers extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('deactivate-users/modal', [
            'url' => $this->url
        ]);
    }
}
