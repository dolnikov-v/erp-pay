<?php
namespace app\modules\callcenter\widgets;

use yii\base\Widget;

/**
 * Class Source
 * @package app\modules\callcenter\widgets
 */
class Source extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $sourcesList;

    /**
     * @var
     */
    public $sourcesByCallCenter;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('source', [
            'model' => $this->model,
            'sourcesList' => $this->sourcesList,
            'sourcesByCallCenter' => $this->sourcesByCallCenter,
        ]);
    }
}