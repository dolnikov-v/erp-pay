<?php
use app\modules\callcenter\widgets\SourceSwitchery;

/** @var \app\modules\callcenter\models\CallCenter $model */
/** @var array $sourcesByCallCenter */
?>
<table class="table table-striped table-hover" data-callcenter-id="<?= $model->id ?>">
    <?php if (!empty($sourcesList)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Источник') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($sourcesList as $sourceId => $source) : ?>
            <tr>
                <td><?= Yii::t('common', $source) ?></td>
                <td class="text-center" data-source-id="<?= $sourceId ?>">
                    <?= SourceSwitchery::widget([
                        'checked' => in_array($sourceId, $sourcesByCallCenter),
                        'disabled' => false,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Источники отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
