<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_activate_users',
    'title' => Yii::t('common', 'Активировать пользователей'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


