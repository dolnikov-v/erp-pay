<?php

use app\modules\callcenter\models\search\filters\CheckingTypeFilter;
use app\widgets\Select2;
use yii\bootstrap\Html;

/** @var array $types */
/** @var array $informations */
/** @var \app\components\widgets\ActiveForm $form */
/** @var CheckingTypeFilter $filter */
?>

<div class="col-lg-3">
    <?= $form->field($filter, 'type')->select2List($types, ['prompt' => '—', 'id' => 'checking_type_select']) ?>
</div>
<div class="col-lg-3">
    <label for="<?= Html::getInputName($filter, 'informationType') ?>"><?= Yii::t('common',
            'Тип информации') ?></label>
    <?= Select2::widget([
        'name' => empty($filter->type) ? Html::getInputName($filter, 'informationType') : '',
        'items' => [],
        'value' => '',
        'prompt' => '—',
        'disabled' => true,
        'style' => (empty($filter->type) ? '' : 'hidden ') . 'checking-type-information-select checking-type-information-select-',
    ]) ?>
    <?php foreach ($types as $type => $label): ?>
        <?= Select2::widget([
            'name' => $type == $filter->type ? Html::getInputName($filter, 'informationType') : '',
            'items' => isset($informations[$type]) ? $informations[$type] : [],
            'value' => $type == $filter->type ? $filter->informationType : '',
            'prompt' => '—',
            'disabled' => !isset($informations[$type]),
            'style' => ($type == $filter->type ? '' : 'hidden ') . 'checking-type-information-select checking-type-information-select-' . $type
        ]) ?>
    <?php endforeach; ?>
</div>
