<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_delete_users',
    'title' => Yii::t('common', 'Удалить пользователей'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


