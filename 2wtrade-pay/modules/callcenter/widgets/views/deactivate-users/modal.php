<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_deactivate_users',
    'title' => Yii::t('common', 'Деактивировать пользователей'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


