<?php

namespace app\modules\callcenter\jobs;

use app\modules\callcenter\components\queue\CallCenterQueue;
use app\modules\order\models\Order;

/**
 * Class SendCallCenterRequestJob
 * @package app\modules\callcenter\jobs
 */
class SendCallCenterRequestJob extends BaseJob
{
    /**
     * @var integer
     */
    public $orderId;

    /**
     * @param CallCenterQueue $queue
     * @throws \yii\db\Exception
     */
    protected function run(CallCenterQueue $queue)
    {
        $order = Order::find()->byId($this->orderId)->one();
        if ($queue->mutex->acquire($this->getMutexName())) {
            try {
                if ($order) {
                    $queue->requestSender->sendCallCenterRequest($order);
                }
            } finally {
                $queue->mutex->release($this->getMutexName());
            }
        }
    }

    /**
     * @return string
     */
    protected function getMutexName()
    {
        return __CLASS__ . $this->orderId;
    }
}