<?php

namespace app\modules\callcenter\jobs;


use app\modules\callcenter\components\queue\CallCenterQueue;
use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Class BaseJob
 * @package app\modules\callcenter\jobs
 */
abstract class BaseJob extends BaseObject implements JobInterface
{
    /**
     * @param CallCenterQueue $queue
     */
    public function execute($queue)
    {
        if(!is_a($queue, CallCenterQueue::class)) {
            throw new \InvalidArgumentException("Job can execute only by " . CallCenterQueue::class);
        }

        $this->run($queue);
    }

    /**
     * @param CallCenterQueue $queue
     * @return mixed
     */
    abstract protected function run(CallCenterQueue $queue);
}