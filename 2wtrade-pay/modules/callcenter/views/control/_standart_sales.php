<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\callcenter\models\CallCenterStandartOperator $modelStandartOperator */
/** @var app\modules\callcenter\models\CallCenterStandartOperator $callCenterStandartOperatorsData */

$operator_ids = isset($callCenterStandartOperatorsData[0]) ? explode(', ', $callCenterStandartOperatorsData[0]['operator_id']) : [];
$clean_operators_ids = [];

if(isset($operator_ids[0]) && !empty($operator_ids[0])) {
    foreach ($operator_ids as $k => $v) {
        $clean_operators_ids[$v] = $v;
    }
}
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($modelStandartOperator, 'operator_id')->select2ListMultiple($clean_operators_ids, [
                    'tags' => true,
                    'value' => $clean_operators_ids
                ]); ?>
                <?= $form->field($modelStandartOperator, 'call_center_id')->hiddenInput(['value' => $model->id])->label(false); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>