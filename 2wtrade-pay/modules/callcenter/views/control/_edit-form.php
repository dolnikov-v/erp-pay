<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/** @var app\modules\callcenter\models\CallCenter $model */
/** @var array $offices */
/** @var mixed $officeId */

$officeIdValues = [];
if ($model->id) {
    $officeIdValues = ArrayHelper::getColumn($model->offices, 'id');
}
else if ($officeId) {
    $officeIdValues[] = $officeId;
}
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id, 'office_id' => $officeId])]); ?>
<div class="row">
    <div class="col-lg-8">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'office_ids')->select2ListMultiple($offices, [
                'value' => $officeIdValues,
            ])->label(Yii::t('common', 'Физический колл-центр')) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <div class="form-group">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'api_key')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($model, 'api_pid')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
            <?= $form->field($model, 'token')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'source_id')->select2List(\app\models\Source::find()->collection(), ['prompt' => '—']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textarea() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($model, 'is_amazon_query')->checkboxCustom([
                'value' => 1,
                'checked' => $model->is_amazon_query,
            ]) ?>
            <?= $form->field($model, 'get_operator_work_time_by_api')->checkboxCustom([
                'value' => 1,
                'checked' => $model->get_operator_work_time_by_api,
            ]) ?>
            <?= $form->field($model, 'get_operator_number_of_call_by_api')->checkboxCustom([
                'value' => 1,
                'checked' => $model->get_operator_number_of_call_by_api,
            ]) ?>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($model, 'jira_issue_collector')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить колл-центр') : Yii::t('common', 'Сохранить колл-центр')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
