<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;
use app\modules\callcenter\widgets\Source;

/** @var yii\web\View $this */
/** @var app\modules\callcenter\models\CallCenter $model */
/** @var app\modules\callcenter\models\CallCenterStandartOperator $modelStandartOperator */
/** @var app\modules\callcenter\models\CallCenterStandartOperator $callCenterStandartOperatorsData */
/** @var array $offices */
/** @var mixed $officeId */
/** @var app\models\Source[] $sources */
/** @var array $sourcesByCallCenter */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление колл-центра') : Yii::t('common', 'Редактирование колл-центра');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление колл-центрами'), 'url' => Url::toRoute('/call-center/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'alert' => $model->isNewRecord ? Yii::t('common', 'Колл-центр будет добавлен и привязан к активной стране - {country}.', [
        'country' => Yii::$app->user->country->name,
    ]) : '',
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Общая информация'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                    'offices' => $offices,
                    'officeId' => $officeId
                ]),
            ],
            [
                'label' => Yii::t('common', 'Эталонные продажи'),
                'content' => $this->render('_standart_sales', [
                    'modelStandartOperator' => $modelStandartOperator,
                    'model' => $model,
                    'callCenterStandartOperatorsData' => $callCenterStandartOperatorsData
                ]),
            ],
        ]
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список источников'),
    'collapse' => true,
    'withBody' => false,
    'content' => Source::widget([
        'model' => $model,
        'sourcesList' => $sources,
        'sourcesByCallCenter' => $sourcesByCallCenter,
    ]),
]) ?>

