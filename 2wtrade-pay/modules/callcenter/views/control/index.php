<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с колл-центрами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'weight',
                'content' => function ($model) {
                    return $model->weight;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url',
                'content' => function ($model) {
                    return $model->url ? Html::a($model->url, $model->url, [
                        'target' => '_blank',
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'office_id',
                'label' => Yii::t('common', 'Физический колл-центр'),
                'content' => function ($model) {
                    $return = [];
                    foreach ($model->offices as $office) {
                        $return[] = $office->name;
                    }
                    return implode(", ", $return);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'label' => 'Amazon',
                'attribute' => 'is_amazon_query',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_amazon_query ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_amazon_query ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'get_operator_work_time_by_api',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->get_operator_work_time_by_api ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->get_operator_work_time_by_api ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'get_operator_number_of_call_by_api',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->get_operator_number_of_call_by_api ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->get_operator_number_of_call_by_api ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/control/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.control.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/control/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('callcenter.control.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/control/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('callcenter.control.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/control/log', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.control.log');
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return (Yii::$app->user->can('callcenter.control.edit')
                                    || (Yii::$app->user->can('callcenter.control.activate') && !$model->active)
                                    || (Yii::$app->user->can('callcenter.control.deactivate') && $model->active))
                                || Yii::$app->user->can('callcenter.control.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/call-center/control/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.control.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('callcenter.control.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить колл-центр'),
                'url' => Url::toRoute('/call-center/control/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
