<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => Url::toRoute('/call-center/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Log::widget([
    'data' => $changeLog,
]); ?>