<?php
/** @var \yii\web\View $this */
use app\components\widgets\ActiveForm;
use app\modules\callcenter\models\CallCenterCheckRequest;
use yii\helpers\Url;

/** @var \app\modules\callcenter\models\search\CheckingRequestSearch $searchModel */

?>


<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-9">
        <?= $searchModel->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($searchModel, 'status')->select2List(CallCenterCheckRequest::getStatuses(), [
                'prompt' => '—',
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <?= $searchModel->getCheckingTypeFilter()->restore($form) ?>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $searchModel->getOrderIdFilter()->restore($form) ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $searchModel->getForeignIdFilter()->restore($form) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $searchModel->getCallCenterFilter()->restore($form) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
