<?php
use app\modules\callcenter\models\CallCenterCheckRequestLog;
use app\modules\callcenter\models\CallCenterCheckRequestActionLog;
use app\components\grid\DateColumn;
use app\widgets\Nav;
use app\widgets\Panel;
use app\components\widgets\PanelList;
use app\widgets\Label;
use app\helpers\logs\Route;
use yii\helpers\Url;

/**
 * @var CallCenterCheckRequestLog $changeLog
 * @var CallCenterCheckRequestActionLog $sendLog
 * @var CallCenterCheckRequestActionLog $updateLog
 */
$this->title = Yii::t('common', 'Просмотр заявки на проверку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => Url::toRoute('/call-center/checking-request/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заявки на проверку'), 'url' => Url::toRoute('/call-center/checking-request/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Информация о заказе'),
    'border' => false,
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'История изменения'),
                'content' => PanelList::widget([
                    'data' => $changeLog,
                    'groupBy' => 'group_id',
                    'id' => 'panel_tab_change',
                    'titleTemplates' => ['revertNum', 'created_at'],
                    'content' => [
                        'user.username',
                        [
                            'attribute' => 'route',
                            'content' => function ($model) {

                                $route = Route::getRouteGroup($model->route);

                                $style = Label::STYLE_PRIMARY;
                                if ($route == Route::GROUP_CALL_CENTER) {
                                    $style = Label::STYLE_INFO;
                                } elseif ($route == Route::GROUP_DELIVERY) {
                                    $style = Label::STYLE_SUCCESS;
                                }

                                return Label::widget([
                                    'label' => $route != "" ? Route::getGroupDescription($route) : Yii::t('common', 'Неопределено'),
                                    'style' => $route != "" ? $style : Label::STYLE_DEFAULT
                                ]);
                            }
                        ],
                        'field',
                        'old',
                        'new',
                        [
                            'class' => DateColumn::className(),
                            'attribute' => 'created_at',
                            'enableSorting' => false,
                        ]
                    ],
                ]),
            ],
            [
                'label' => Yii::t('common', 'Лог отправки'),
                'content' => PanelList::widget([
                    'data' => $sendLog,
                    'groupBy' => '_id',
                    'id' => 'panel_tab_send',
                    'titleTemplates' => ['num', 'created_at'],
                    'titleContent' => Label::widget(['label' => '{route}', 'style' => Label::STYLE_INFO]),
                    'content' => [
                        [
                            'label' => Yii::t('common', 'Запрос'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<pre><code>' . (string)$model->request . '</code></pre>';
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Ответ'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<pre><code>' . (string)$model->response . '</code></pre>';
                            }
                        ],
                    ],
                ]),
            ],
            [
                'label' => Yii::t('common', 'Лог получения'),
                'content' => PanelList::widget([
                    'data' => $updateLog,
                    'groupBy' => '_id',
                    'id' => 'panel_tab_update',
                    'titleTemplates' => ['num', 'created_at'],
                    'titleContent' => Label::widget(['label' => '{route}', 'style' => Label::STYLE_INFO]),
                    'content' => [
                        [
                            'label' => Yii::t('common', 'Данные'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<pre><code>' . (string)$model->response . '</code></pre>';
                            }
                        ],
                    ],
                ]),
            ],
        ]
    ]),
]) ?>