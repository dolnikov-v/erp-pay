<?php

/** @var \yii\web\View $this */
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\widgets\Label;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\callcenter\assets\CheckingTypeFilterAsset;

/** @var \app\modules\callcenter\models\search\CheckingRequestSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Заявки на проверку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

CheckingTypeFilterAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'searchModel' => $searchModel,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заявками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'callCenter.name',
                'enableSorting' => false,
                'label' => Yii::t('common', 'Колл-центр'),
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Html::a($model->order_id, Url::toRoute([
                        '/order/index/index',
                        'NumberFilter[not][]' => '',
                        'NumberFilter[entity][]' => 'id',
                        'NumberFilter[number][]' => $model->order_id,
                    ]), [
                        'target' => '_blank',
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'type',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => CallCenterCheckRequest::getTypes()[$model->type],
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'foreign_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $style = Label::STYLE_DANGER;

                    if ($model->status == CallCenterCheckRequest::STATUS_PENDING) {
                        $style = Label::STYLE_PRIMARY;
                    } elseif ($model->status == CallCenterCheckRequest::STATUS_IN_PROGRESS) {
                        $style = Label::STYLE_INFO;
                    } elseif ($model->status == CallCenterCheckRequest::STATUS_DONE) {
                        $style = Label::STYLE_DEFAULT;
                    }

                    return Label::widget([
                        'label' => CallCenterCheckRequest::getStatuses()[$model->status],
                        'style' => $style,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'request_comment',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'foreign_information',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $collection = CallCenterCheckRequest::getInformationCollection();

                    if (isset($collection[$model->type][$model->foreign_information])) {
                        return $collection[$model->type][$model->foreign_information];
                    }
                    return $model->foreign_information ?: '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'comment',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'api_error',
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->status == CallCenterCheckRequest::STATUS_ERROR) {
                        return $model->api_error ? $model->api_error : '—';
                    }

                    return '—';
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'sent_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'done_at',
            ],
            'actions' => [
                'class' => \app\components\grid\ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.index.view');
                        },
                    ],
                ]
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
