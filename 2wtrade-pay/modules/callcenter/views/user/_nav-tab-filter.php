<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var array $callCenters */
/** @var array $roles */
/** @var array $parentUsers */
/** @var app\modules\callcenter\models\search\CallCenterUserSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3 callcenter_holder">
            <?= $form->field($modelSearch, 'callcenter_id')->select2List($callCenters) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'user_role')->select2List($roles) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'user_login')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'user_id')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>