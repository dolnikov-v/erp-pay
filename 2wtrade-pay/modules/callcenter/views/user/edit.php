<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\callcenter\assets\UserAsset;

/** @var yii\web\View $this */
/** @var array $callCenters */
/** @var array $roles */
/** @var array $parentUsers */
/** @var array $persons */
/** @var app\modules\callcenter\models\CallCenterUser $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление пользователя колл-центра') : Yii::t('common', 'Редактирование пользователя колл-центра');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление пользователями'), 'url' => Url::toRoute('/call-center/user/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

UserAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Пользователь колл-центра'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'callCenters' => $callCenters,
        'roles' => $roles,
        'parentUsers' => $parentUsers,
        'persons' => $persons
    ])
]) ?>
