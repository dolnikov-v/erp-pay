<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var array $callCenters */
/** @var array $roles */
/** @var array $parentUsers */
/** @var array $persons */
/** @var app\modules\callcenter\models\CallCenterUser $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'person_id')->select2List($persons, ['prompt' => '-']) ?>
    </div>
    <div class="col-lg-4 callcenter_holder">
        <?= $form->field($model, 'callcenter_id')->select2List($callCenters) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'user_id')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'user_login')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'user_role')->select2List($roles, ['prompt' => '-']) ?>
    </div>
</div>
<div class="row m">
    <div class="col-lg-8"></div>
    <div class="col-lg-4 no-margin-custom-checkbox">
        <?= $form->field($model, 'active')->checkboxCustom(['value' => 1,
            'checked' => $model->active,]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить пользователя') : Yii::t('common', 'Сохранить пользователя')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
