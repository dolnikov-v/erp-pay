
<?php
use app\widgets\Nav;
use app\widgets\Panel;

/** @var array $callCenters */
/** @var array $roles */
/** @var array $parentUsers */
/* @var yii\web\View $this */
/* @var app\modules\callcenter\models\search\CallCenterUserSearch $modelSearch */
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch,
                    'callCenters' => $callCenters,
                    'roles' => $roles,
                    'parentUsers' => $parentUsers

                ]),
            ],
            [
                'label' => Yii::t('common', 'Групповые операции'),
                'content' => $this->render('_nav-tab-operations'),
            ],
        ]
    ]),
]) ?>
