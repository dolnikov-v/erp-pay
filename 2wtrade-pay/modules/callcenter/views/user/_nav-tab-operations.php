<?php
use app\assets\vendor\BootstrapLaddaAsset;
use app\components\widgets\ActiveForm;
use app\widgets\ButtonProgress;
use yii\helpers\Url;

BootstrapLaddaAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>

<?php if (Yii::$app->user->can('callcenter.user.activateusers')): ?>
    <div class="margin-bottom-5">
        <?= ButtonProgress::widget([
            'id' => 'btn_activate_users',
            'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
            'label' => Yii::t('common', 'Активировать пользователей'),
        ]) ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->user->can('callcenter.user.deactivateusers')): ?>
    <div class="margin-bottom-5">
        <?= ButtonProgress::widget([
            'id' => 'btn_deactivate_users',
            'style' => ButtonProgress::STYLE_DANGER . ' width-300',
            'label' => Yii::t('common', 'Деактивировать пользователей'),
        ]) ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->user->can('callcenter.user.deleteusers')): ?>
    <div class="margin-bottom-5">
        <?= ButtonProgress::widget([
            'id' => 'btn_delete_users',
            'style' => ButtonProgress::STYLE_DANGER . ' width-300',
            'label' => Yii::t('common', 'Удалить пользователей'),
        ]) ?>
    </div>
<?php endif; ?>