<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;
use yii\web\View;

use app\modules\callcenter\widgets\DeactivateUsers as DeactivateUsersWidget;
use app\modules\callcenter\widgets\ActivateUsers as ActivateUsersWidget;
use app\modules\callcenter\widgets\DeleteUsers as DeleteUsersWidget;

use app\modules\callcenter\assets\IndexAsset;
use app\modules\callcenter\assets\UserAsset;
use app\modules\callcenter\assets\DeactivateUsersAsset;
use app\modules\callcenter\assets\ActivateUsersAsset;
use app\modules\callcenter\assets\DeleteUsersAsset;

use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;

/** @var yii\web\View $this */
/** @var app\modules\callcenter\models\search\CallCenterUserSearch $modelSearch */
/** @var array $roles */
/** @var array $callCenters */
/** @var array $parentUsers */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Пользователи'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

IndexAsset::register($this);
UserAsset::register($this);
DeactivateUsersAsset::register($this);
ActivateUsersAsset::register($this);
DeleteUsersAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать пользователей.'] = '" . Yii::t('common', 'Необходимо выбрать пользователей.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Ничего не сделали.'] = '" . Yii::t('common', 'Ничего не сделали.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет прав для выполнения данной операции'] = '" . Yii::t('common', 'Нет прав для выполнения данной операции') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить руководителя'] = '" . Yii::t('common', 'Не удалось сменить руководителя') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось деактивировать пользователей'] = '" . Yii::t('common', 'Не удалось деактивировать пользователей') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось активировать пользователей'] = '" . Yii::t('common', 'Не удалось активировать пользователей') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить пользователей'] = '" . Yii::t('common', 'Не удалось удалить пользователей') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить руководителя, т.к. у пользователей разные КЦ'] = '" . Yii::t('common', 'Не удалось сменить руководителя, т.к. у пользователей разные КЦ') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось установить роль'] = '" . Yii::t('common', 'Не удалось установить роль') . "';", View::POS_HEAD);
?>


<?= Panel::widget([
    'title' => '',
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'callCenters' => $callCenters,
        'roles' => $roles,
        'parentUsers' => $parentUsers,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пользователями колл-центра'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'users_content',
        ],
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
            ],
            [
                'attribute' => 'user_login',
            ],
            [
                'attribute' => 'user_id',
            ],
            [
                'attribute' => 'person_id',
                'content' => function ($model) {
                    return ($model->person_id) ? $model->person->name : '-';
                }
            ],

            /*
            [
                'attribute' => 'user_role',
                'content' => function ($model) use ($roles) {
                    return (isset($roles[$model->user_role]) ? $roles[$model->user_role] : '-');
                }
            ],*/
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/user/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.user.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/user/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('callcenter.user.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/user/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('callcenter.user.deactivate') && $model->active;
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return (Yii::$app->user->can('callcenter.user.edit')
                                    || (Yii::$app->user->can('callcenter.user.activate') && !$model->active)
                                    || (Yii::$app->user->can('callcenter.user.deactivate') && $model->active))
                                || Yii::$app->user->can('callcenter.user.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/call-center/user/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.user.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('callcenter.user.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить пользователя'),
                'url' => Url::toRoute('/call-center/user/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>

<?php if (Yii::$app->user->can('callcenter.user.deactivateusers')): ?>
    <?= DeactivateUsersWidget::widget([
        'url' => Url::toRoute('deactivate-users'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('callcenter.user.activateusers')): ?>
    <?= ActivateUsersWidget::widget([
        'url' => Url::toRoute('activate-users'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('callcenter.user.deleteusers')): ?>
    <?= DeleteUsersWidget::widget([
        'url' => Url::toRoute('delete-users'),
    ]); ?>
<?php endif; ?>

<?= ModalConfirmDelete::widget() ?>