<?php
use app\components\widgets\ActiveForm;
use app\modules\callcenter\models\CallCenterRequest;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\callcenter\models\search\CallCenterRequestSearch $modelSearch */
/** @var array $callCentersCollection */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'call_center_id')->select2List($callCentersCollection, [
                'prompt' => '—',
            ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'status')->select2List(CallCenterRequest::getStatusesCollection(), [
                'prompt' => '—',
            ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'order_id')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
