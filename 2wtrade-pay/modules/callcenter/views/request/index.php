<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\callcenter\models\CallCenterRequest;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\callcenter\models\search\CallCenterRequestSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $callCentersCollection */

$this->title = Yii::t('common', 'Заявки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'callCentersCollection' => $callCentersCollection,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заявками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'callCenter.name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Html::a($model->order_id, Url::toRoute([
                        '/order/index/index',
                        'NumberFilter[not][]' => '',
                        'NumberFilter[entity][]' => 'id',
                        'NumberFilter[number][]' => $model->order_id,
                    ]), [
                        'target' => '_blank',
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'foreign_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $style = Label::STYLE_DANGER;

                    if ($model->status == CallCenterRequest::STATUS_PENDING) {
                        $style = Label::STYLE_PRIMARY;
                    } elseif ($model->status == CallCenterRequest::STATUS_IN_PROGRESS) {
                        $style = Label::STYLE_INFO;
                    } elseif ($model->status == CallCenterRequest::STATUS_DONE) {
                        $style = Label::STYLE_DEFAULT;
                    }

                    return Label::widget([
                        'label' => CallCenterRequest::getStatusesCollection()[$model->status],
                        'style' => $style,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'api_error',
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->status == CallCenterRequest::STATUS_ERROR) {
                        return $model->api_error ? $model->api_error : '—';
                    }

                    return '—';
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'История изменения'),
                        'url' => function ($model) {
                            return Url::toRoute(['/call-center/request/logs', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.request.logs');
                        }
                    ],
                ],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
