<?php

namespace app\modules\callcenter\components\api;

use app\components\curl\CurlFactory;
use app\models\{Country, Notification, Source};
use app\modules\{
    administration\models\CrontabTaskLog, administration\models\CrontabTaskLogAnswer, callcenter\models\CallCenter, callcenter\models\CallCenterCheckRequestActionLog, callcenter\models\CallCenterRequest, callcenter\models\CallCenterRequestCallData, callcenter\models\CallCenterRequestExtra, callcenter\models\CallCenterRequestHistory, callcenter\models\CallCenterRequestSendResponse, callcenter\models\CallCenterUser, callcenter\models\CallCenterWorkTime, catalog\controllers\AutotrashController, delivery\models\Delivery, delivery\models\DeliveryRequest, delivery\models\DeliveryRequestDeleted, order\models\Order, callcenter\models\CallCenterCheckRequest, order\models\OrderExpressDelivery, order\models\OrderProduct, order\models\OrderSendError, order\models\OrderStatus, salary\models\Person, storage\models\Storage, storage\models\StorageProduct
};
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\validators\StringValidator;
use app\modules\api\components\filters\auth\HttpBearerAuth;
use app\modules\callcenter\models\CallCenterProductCall;
use app\modules\callcenter\models\CallCenterUserOrder;

/**
 * Class Lead
 * @package app\modules\callcenter\components\api
 */
class Lead extends Component
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';
    const QUEUE_NOT_ORDER = 'not_order';

    const GET_STATUSES_TYPE_NOT_DEFERRED = 'not_deferred';
    const GET_STATUSES_TYPE_DEFERRED = 'deferred';

    /** @var array */
    private static $countriesWithCallCenters = [];

    /** @var Delivery[] */
    private static $deliveries = [];

    /** @var array */
    private static $callCenters = [];

    /** @var array */
    public static $storagesPart = [];

    /** @var array */
    private static $mapStatuses = [
        1 => OrderStatus::STATUS_CC_POST_CALL,
        4 => OrderStatus::STATUS_CC_APPROVED,
        5 => OrderStatus::STATUS_CC_FAIL_CALL,
        6 => OrderStatus::STATUS_CC_RECALL,
        7 => OrderStatus::STATUS_CC_REJECTED,
        8 => OrderStatus::STATUS_CC_REJECTED,
        9 => OrderStatus::STATUS_CC_APPROVED,
        10 => OrderStatus::STATUS_CC_REJECTED,
        11 => OrderStatus::STATUS_CC_REJECTED,
        12 => OrderStatus::STATUS_CC_APPROVED,
        13 => OrderStatus::STATUS_CC_REJECTED,
        15 => OrderStatus::STATUS_CC_DOUBLE,
        17 => OrderStatus::STATUS_CC_TRASH,
        19 => OrderStatus::STATUS_CC_TRASH,
        21 => OrderStatus::STATUS_CC_TRASH,
        33 => OrderStatus::STATUS_AUTOTRASH
    ];

    /**
     * @return array
     */
    public static function getMapStatuses()
    {
        return self::$mapStatuses;
    }

    /**
     * @param CrontabTaskLog $log
     */
    public static function createCallCenterRequests($log)
    {
        $offset = 0;
        $limit = 100;

        self::prepareCountriesWithCallCenters();
        self::prepareStoragesPart();

        self::$deliveries = self::getDeliveriesToGetStatus();

        $countries = Country::getCountriesWithActiveCallCenter();
        $ids = ArrayHelper::getColumn($countries, 'id');

        $query = Order::find()
            ->where([
                'in',
                Order::tableName() . '.status_id',
                [
                    OrderStatus::STATUS_SOURCE_LONG_FORM,
                    OrderStatus::STATUS_SOURCE_SHORT_FORM,
                ]
            ])
            ->byCountryIds($ids)
            ->limit($limit);
        $query->joinWith('callCenterRequest');
        $query->andWhere(['is', CallCenterRequest::tableName() . '.id', null]);
        $query->with(['deliveryRequest', 'country']);

        while ($orders = $query->offset($offset)->all()) {
            foreach ($orders as $order) {

                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->data = json_encode(['order_id' => $order->id], JSON_UNESCAPED_UNICODE);

                try {
                    if (self::checkIfProductsAvailable($order)) {
                        $inDelivery = false;
                        if ($order->status_id == OrderStatus::STATUS_SOURCE_LONG_FORM) {
                            if ($order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_PENDING) || $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)) {
                                $inDelivery = true;
                            }
                        }

                        if ($inDelivery) {
                            if (!array_key_exists($order->country_id, self::$deliveries)) {
                                // @todo: Наверное, нужно добавлять уведомление
                                continue;
                            }

                            $delivery = self::$deliveries[$order->country_id];
                            $result = Delivery::createRequest($delivery, $order, false);
                        } else {
                            if (!$order->canChangeStatusTo(OrderStatus::STATUS_CC_POST_CALL)) {
                                // @todo: Наверное, нужно добавлять уведомление
                                continue;
                            }


                            //не поняли почему так, но для заказов типа repeat из маркетинговых листов пришлось тоже добавить, иначе они приходят как новые заказы
                            //почему нельзя передавать в createCallCenterRequest просто $order->call_center_type ?
                            //приооединяюсь к предыдущему оратору(с) А то это какой-то пиздец
                            $callCenterType = Order::TYPE_CC_ORDER;
                            if ($order->call_center_type == Order::TYPE_CC_NEW_RETURN ||
                                $order->call_center_type == Order::TYPE_CC_ORDER_REPEAT ||
                                $order->call_center_type == Order::TYPE_CC_ORDER_2WSTORE ||
                                $order->call_center_type == Order::TYPE_CC_ORDER_TOPHOT ||
                                $order->call_center_type == Order::TYPE_CC_POST_SALE
                            ) {
                                $callCenterType = $order->call_center_type;
                            }

                            // Если включен автотреш NPAY-724
                            if (AutotrashController::getStateAutotrash($order->country_id)) {
                                //проверка полей заказа по фильтрам автотреша
                                if (AutotrashController::classifyOrder($order)) {
                                    $order->status_id = OrderStatus::STATUS_AUTOTRASH;
                                    if (!$order->save()) {
                                        throw new \Exception(Yii::t('common',
                                            'Не удалось изменить заказ {order} причина: {reason}',
                                            ['order' => $order->id, 'reason' => $order->getFirstErrorAsString()]));
                                    }
                                    continue;
                                }
                            }

                            // если галка Трешить дубли включена в стране и еще не сработал автотреш
                            if ($order->country->trash_doubles_enabled) {
                                // Ищем дубли по номеру телефона (апрувы за последние 30 дней)
                                $hasApprove = Order::find()
                                    ->byCountryId($order->country_id)
                                    ->andWhere(['<>', 'id', $order->id])
                                    ->andWhere(['>=', 'created_at', strtotime('-30 days')])
                                    ->andWhere(['customer_phone' => $order->customer_phone])
                                    ->andWhere(['status_id' => OrderStatus::getProcessList()])
                                    ->count();

                                if ($hasApprove) {
                                    $order->status_id = OrderStatus::STATUS_AUTOTRASH_DOUBLE;
                                    if (!$order->save()) {
                                        throw new \Exception(Yii::t('common',
                                            'Не удалось изменить заказ {order} причина: {reason}',
                                            ['order' => $order->id, 'reason' => $order->getFirstErrorAsString()]));
                                    }
                                    continue;
                                }
                            }
                            $result = Lead::createCallCenterRequest($order, $callCenterType);
                        }
                    } else {
                        throw new \Exception(Yii::t('common', 'Отсутствуют продукты на складе.'));
                    }
                } catch (\Throwable $e) {
                    $result = [
                        'status' => self::STATUS_FAIL,
                        'message' => $e->getMessage()
                    ];
                }

                $logAnswer->answer = json_encode($result, JSON_UNESCAPED_UNICODE);
                $logAnswer->status = $result['status'] == self::STATUS_SUCCESS ? CrontabTaskLogAnswer::STATUS_SUCCESS : CrontabTaskLogAnswer::STATUS_FAIL;

                $logAnswer->save();
            }

            $offset += $limit;
            sleep(rand(1, 3));
        }
    }

    /**
     * Определение КЦ по его весу
     * @param $country_id
     * @return CallCenter|bool
     */
    public static function getCallCenterByWeight($country_id)
    {
        $callCenters = CallCenter::find()
            ->where([CallCenter::tableName() . '.country_id' => $country_id])
            ->andWhere([CallCenter::tableName() . '.active' => 1])
            ->andWhere(['<>', CallCenter::tableName() . '.weight', 0])
            ->andWhere(['is not', CallCenter::tableName() . '.weight', null])
            ->orderBy([CallCenter::tableName() . '.weight' => SORT_ASC])
            ->all();

        $total = 0;
        foreach ($callCenters as $callCenter) {
            $total += $callCenter->weight;
        }

        if ($total <= 0) {
            return false;
        }

        $scale = 100 / $total;
        $r = rand(0, 100);

        foreach ($callCenters as $callCenter) {
            $weight = $callCenter->weight * $scale;
            if ($weight >= $r) {
                return $callCenter;
            }
            $r -= $weight;
        }

        return false;
    }

    /**
     * @param Order $model
     * @param string $callCenterType
     * @return array
     */
    public static function createCallCenterRequest(Order $model, $callCenterType = false)
    {
        $result = [
            'status' => self::STATUS_FAIL,
        ];

        $callCenter = self::getCallCenterByLead($model);

        if ($callCenter) {
            if (!$model->callCenterRequest) {
                $callCenterByWeight = self::getCallCenterByWeight($model->country_id);

                $request = new CallCenterRequest();
                $request->call_center_id = (!$callCenterByWeight) ? $callCenter->id : $callCenterByWeight->id;
                $request->order_id = $model->id;
                $request->status = CallCenterRequest::STATUS_PENDING;

                if ($callCenterType == Order::TYPE_CC_NEW_RETURN) {
                    if (isset($model->deliveryRequest)) {
                        if ($model->deliveryRequest->status == DeliveryRequest::STATUS_ERROR && $model->deliveryRequest->api_error) {
                            $request->delivery_error = $model->deliveryRequest->api_error;
                            // @todo: Необходима будет дополнительная проверка на $order->deliveryRequest->api_error_type == DeliveryRequest::API_ERROR_COURIER

                        }
                    }
                }

                $request->route = Yii::$app->controller->route;

                $transaction = Yii::$app->db->beginTransaction();

                if ($request->save()) {
                    if($callCenterType){
                        $model->call_center_type = $callCenterType;
                    }

                    if(!isset($model->call_center_type) && is_null($model->call_center_type)){
                        $model->call_center_type = Order::TYPE_CC_ORDER;
                    }

                    if ($model->save()) {
                        $result['status'] = self::STATUS_SUCCESS;
                    } else {
                        $result['message'] = $model->getErrorsAsArray()[0];
                    }
                } else {
                    $result['message'] = $request->getErrorsAsArray()[0];
                }

                if ($result['status'] == self::STATUS_SUCCESS) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            } else {
                $result['message'] = Yii::t('common',
                    'Заявка уже существует. Требуется повторная отправка заявки в колл-центр.');
            }
        } else {
            $result['message'] = Yii::t('common', 'Отсутствует колл-центр.');
        }

        if ($result['status'] == self::STATUS_FAIL) {
            Yii::$app->notification->send(
                Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR,
                [
                    'order_id' => $model->id,
                    'country' => $model->country->name,
                    'error' => is_array($result['message']) ? implode(' | ', $result['message']) : $result['message']
                ],
                $model->country_id
            );
        }

        return $result;
    }

    /**
     * @param CrontabTaskLog $log
     * @param integer $cc_id
     * @param string $queue
     */
    public static function sendCallCenterRequests($log, $cc_id = null, $queue = null)
    {
        $callCenters = self::getCallCentersToSendRequests();

        $fp = [];
        foreach ($callCenters as $callCenter) {
            if (is_numeric($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            // Блокируем отправку для одного КЦ
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }

            $file_lock = "/blocks/call_center_send_requests_{$callCenter->id}_{$callCenter->country->char_code}" . (is_null($queue) ? '' : "_{$queue}") . ".lock";

            $fp[$callCenter->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');

            if (!flock($fp[$callCenter->id], LOCK_EX | LOCK_NB)) {
                fclose($fp[$callCenter->id]);
                continue;
            }

            self::prepareStoragesPart($callCenter->country_id);

            $limit = 100;

            $query = CallCenterRequest::find()
                ->where(['status' => CallCenterRequest::STATUS_PENDING])
                ->joinWith([
                    'order.callCenterSendError',
                    'callCenter'
                ])
                ->with([
                    'order.country',
                    'order.country.currency',
                    'order.orderProducts',
                ])
                ->andWhere([CallCenter::tableName() . '.id' => $callCenter->id])
                ->andWhere([CallCenter::tableName() . '.active' => 1])
                ->andWhere(["=", CallCenter::tableName() . '.is_amazon_query', 0])
                ->andWhere(['is', OrderSendError::tableName() . '.type', null])
                ->limit($limit)
                ->orderBy([CallCenterRequest::tableName() . '.cron_launched_at' => SORT_ASC]);

            if ($queue && $queue == self::QUEUE_NOT_ORDER) {
                $query->andWhere(['!=', Order::tableName() . '.call_center_type', Order::TYPE_CC_ORDER]);
            } elseif ($queue) {
                $query->andWhere([Order::tableName() . '.call_center_type' => $queue]);
            }

            $requests = $query->all();
            foreach ($requests as $request) {

                $request->cron_launched_at = time();
                $request->save(false, ['cron_launched_at']);

                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                $logAnswerData = [];

                $logAnswerAnswer = [
                    'order_id' => $request->order_id,
                    'request_id' => $request->id,
                    'answer' => '',
                    'errors' => [],
                ];

                try {
                    $checkProduct = true;
                    if (in_array($request->order->call_center_type, [Order::TYPE_CC_RETURN_NO_PROD])) {
                        $checkProduct = false;
                    }
                    if ($data = self::prepareLeadToSendInCallCenter($request, $checkProduct)) {
                        $lastCcSendResponse = new CallCenterRequestSendResponse([
                            'call_center_request_id' => $request->id,
                            'request' => json_encode($data['fields'], JSON_UNESCAPED_UNICODE)
                        ]);
                        $request->setLastSendResponse($lastCcSendResponse);

                        $logAnswer->url = self::prepareUrlToSendInCallCenter($data['callCenter']);
                        $logAnswerData = $data['fields'];
                        $result = self::sendCallCenterRequest($data, $request);

                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            self::saveResponseAfterSent($request, $result);
                            $logAnswerAnswer['answer'] = $result['answer'];

                            if ($result['success']) {
                                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                            } else {
                                $logAnswerAnswer['errors'] = $result['errors'];
                            }

                            $transaction->commit();
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            $sendErrorModel = new OrderSendError([
                                'type' => OrderSendError::TYPE_CALL_CENTER_SEND,
                                'order_id' => $request->order_id,
                                'error' => $e->getMessage(),
                                'response' => json_encode($result, JSON_UNESCAPED_UNICODE),
                            ]);
                            $sendErrorModel->save();
                            throw $e;
                        }
                    } else {
                        $logAnswerAnswer['errors'][] = 'Не удалось подготовить лида на отправку в колл-центр.';

                    }

                    $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                    $logAnswer->save();
                } catch (\Throwable $e) {
                    $logAnswerAnswer['errors'][] = $e->getMessage();
                    $logAnswer->data = json_encode(['order_id' => $request->order_id], JSON_UNESCAPED_UNICODE);
                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                    $logAnswer->save();

                    Yii::$app->notification->send(Notification::TRIGGER_EXCEPTION_SENT_TO_CALL_CENTER, [
                        'order_id' => $request->order_id,
                        'error' => $e->getMessage(),
                    ],
                        $request->order->country_id
                    );
                }
            }

            fclose($fp[$callCenter->id]); // освобождаем лок сразу.
        }
        unset($fp);
    }

    /**
     * @param $data
     * @param CallCenterRequest $model | null
     * @return array
     */
    public static function sendCallCenterRequest($data, $model = null)
    {
        $result = [
            'success' => false,
            'errors' => [],
            'answer' => '',
            'foreign_id' => '',
            'foreign_status' => '',
            'foreign_substatus' => null,
            'request_error' => false,
        ];

        /** @var CallCenter $callCenter */
        $callCenter = $data['callCenter'];
        $curl = CurlFactory::build(CurlFactory::METHOD_POST, self::prepareUrlToSendInCallCenter($callCenter));

        $result['request'] = json_encode($data['fields'], JSON_UNESCAPED_UNICODE);
        $answer = $curl->query(['data' => $result['request']]);

        $result['answer'] = $answer;

        $answer = json_decode($answer, true);

        // Если колл-центр принял лида
        if (isset($answer['success']) && $answer['success'] == true) {
            $result['success'] = true;
            $result['foreign_id'] = $answer['response']['order_id'];
            $result['foreign_status'] = $answer['response']['status'];
            $result['foreign_substatus'] = $answer['response']['sub_status'];
        } else {
            if (isset($answer['error']['msg'])) {
                $result['errors'][] = $answer['error']['msg'];
            } else {
                $result['request_error'] = true;
                $result['errors'][] = 'Колл-центр ответил с ошибкой.';
            }

            if (isset($answer['error']['code'])) {
                $result['code'] = $answer['error']['code'];
            }

            Yii::$app->notification->send(
                Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR, [
                'order_id' => $data['fields']['order_id'],
                'country' => $data['fields']['country'],
                'error' => implode(' | ', $result['errors']),
            ],
                $model->order->country_id
            );
        }

        return $result;
    }

    /**
     * @param CallCenterRequest $request
     * @param array $response
     * @throws \Exception
     */
    protected static function saveResponseAfterSent($request, $response)
    {
        if ($response['success'] == false) {
            if (isset($response['code'])) {
                if ($response['code'] == 450) {
                    $request->order->status_id = OrderStatus::STATUS_CC_DOUBLE;
                    if (!$request->order->save(false, ['status_id'])) {
                        throw new \Exception($request->order->getFirstErrorAsString());
                    }
                } elseif ($response['code'] == 448) {
                    $request->order->status_id = OrderStatus::STATUS_CC_TRASH;
                    if (!$request->order->save(false, ['status_id'])) {
                        throw new \Exception($request->order->getFirstErrorAsString());
                    }
                }
            }

            if (!$response['request_error']) {
                $request->status = CallCenterRequest::STATUS_ERROR;
                $request->api_error = $response['errors'][0];
            }
        } else {
            $request->sent_at = time();
            $request->foreign_id = $response['foreign_id'];
            $request->foreign_status = $response['foreign_status'];
            $request->foreign_substatus = $response['foreign_substatus'];
            $request->status = CallCenterRequest::STATUS_IN_PROGRESS;
            $request->order->status_id = OrderStatus::STATUS_CC_POST_CALL;
            if (!$request->order->save(false, ['status_id'])) {
                throw new \Exception($request->order->getFirstErrorAsString());
            }
        }

        $request->route = Yii::$app->controller->route;

        $lastCcSendResponse = $request->lastSendResponse;
        if (!$lastCcSendResponse) {
            $lastCcSendResponse = new CallCenterRequestSendResponse([
                'call_center_request_id' => $request->id
            ]);
            $request->setLastSendResponse($lastCcSendResponse);
        }

        $lastCcSendResponse->response = json_encode($response, JSON_UNESCAPED_UNICODE);
        if (!$lastCcSendResponse->save()) {
            throw new \Exception($lastCcSendResponse->getFirstErrorAsString());
        }

        if (!$request->save()) {
            throw new \Exception($request->getFirstErrorAsString());
        }
    }

    /**
     * @param $log
     */
    public static function resendCallCenterRequests($log)
    {
        $offset = 0;
        $limit = 20;

        $query = Order::find()
            ->joinWith([
                'callCenterRequest',
                'deliveryRequest',
            ])
            ->where([
                Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_REJECTED,
                CallCenter::tableName() . '.is_amazon_query' => 0
            ])
            ->andWhere([DeliveryRequest::tableName() . '.status' => DeliveryRequest::STATUS_ERROR])
            ->andWhere([DeliveryRequest::tableName() . '.api_error_type' => DeliveryRequest::API_ERROR_COURIER])
            ->limit($limit)
            ->orderBy([CallCenterRequest::tableName() . '.id' => SORT_ASC]);
        $query->with(['country']);

        while ($requests = $query->offset($offset)->all()) {
            foreach ($requests as $request) {

                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($result = self::resendCallCenterRequest($request)) {
                        if ($result['status'] == self::STATUS_SUCCESS) {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                        } else {
                            $logAnswerAnswer['errors'] = $result['message'];
                        }
                    } else {
                        $logAnswerAnswer['errors'][] = 'Не удалось повторно отправить.';
                    }

                    $logAnswer->answer = json_encode($result, JSON_UNESCAPED_UNICODE);
                    $logAnswer->data = json_encode(['order_id' => $request->id], JSON_UNESCAPED_UNICODE);
                    $logAnswer->save();

                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                    $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                    $logAnswer->data = json_encode(['order_id' => $request->id], JSON_UNESCAPED_UNICODE);
                    $logAnswer->save();
                    $logAnswerAnswer['errors'][] = $e->getMessage();
                }

                if (isset($logAnswerAnswer['errors'])) {
                    Yii::$app->notification->send(
                        Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR,
                        [
                            'order_id' => $request->id,
                            'country' => $request->country->name,
                            'error' => is_array($logAnswerAnswer['errors']) ? implode(' | ',
                                $logAnswerAnswer['errors']) : $logAnswerAnswer['errors'],
                        ],
                        $request->country_id
                    );
                }
            }

            $offset += $limit;

            sleep(rand(1, 3));
        }
    }

    /**
     * @param Order $order
     * @return array
     */
    public static function resendCallCenterRequest($order)
    {
        $response = [
            'status' => self::STATUS_FAIL,
            'message' => '',
        ];

        /**
         * Order was rejected by courier service
         * or
         * approve date is older than 1 month
         */

        $isOldApprove = self::isOldApprove($order);
        if (($order->status_id == OrderStatus::STATUS_DELIVERY_REJECTED
                && isset($order->deliveryRequest)
                && $order->deliveryRequest->status == DeliveryRequest::STATUS_ERROR)
            || $isOldApprove
        ) {
            // Если заявка не существует
            if (!$order->callCenterRequest) {
                $resultCreate = Lead::createCallCenterRequest($order);

                if ($resultCreate['status'] == self::STATUS_SUCCESS) {
                    $response['status'] = self::STATUS_SUCCESS;
                } else {
                    $response['message'] = $resultCreate['message'];
                }
            } else {
                $order->route = Yii::$app->controller->route;
                $order->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                $order->call_center_type = Order::TYPE_CC_NEW_RETURN;

                /**
                 * Workflow off!!!
                 */
                $order->shouldValidateStatusId = false;

                $order->callCenterRequest->route = Yii::$app->controller->route;
                $order->callCenterRequest->status = CallCenterRequest::STATUS_PENDING;

                if ($isOldApprove) {
                    $order->callCenterRequest->delivery_error = 'Since the approval of the order more than a month';
                }

                if (isset($order->deliveryRequest) && $order->deliveryRequest->status == DeliveryRequest::STATUS_ERROR && $order->deliveryRequest->api_error) {
                    // @todo: Необходима будет дополнительная проверка на $order->deliveryRequest->api_error_type == DeliveryRequest::API_ERROR_COURIER
                    $order->callCenterRequest->delivery_error = $order->deliveryRequest->api_error;
                }

                if ($order->save() && $order->callCenterRequest->save()) {
                    $response['status'] = self::STATUS_SUCCESS;
                } else {
                    $response['message'] = Yii::t('common', 'Не удалось сменить статусы.');
                }
            }
        } else {
            $response['message'] = Yii::t('common',
                'Выполнится при условии апрува заказа больше месяца или ошибке при отправке в курьерку.');
        }

        if ($response['status'] == self::STATUS_FAIL) {
            Yii::$app->notification->send(
                Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR,
                [
                    'order_id' => $order->id,
                    'country' => $order->country->name,
                    'error' => $response['message'],
                ],
                $order->country_id
            );
        }

        return $response;
    }

    /**
     * @param Order $order
     * @return array
     */
    public static function resendCallCenterRequestReturnNoProd($order)
    {
        $response = [
            'status' => self::STATUS_FAIL,
            'message' => '',
        ];


        if (in_array($order->status_id, [
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_CC_APPROVED,
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM
            ]) && (empty($order->deliveryRequest) || (in_array($order->deliveryRequest->status,
                    [DeliveryRequest::STATUS_DONE, DeliveryRequest::STATUS_ERROR])))
        ) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                // Если заявка не существует
                if (!$order->callCenterRequest) {
                    $resultCreate = Lead::createCallCenterRequest($order);

                    if ($resultCreate['status'] == self::STATUS_SUCCESS) {
                        $response['status'] = self::STATUS_SUCCESS;
                    } else {
                        throw new \Exception(Yii::t('common',
                            'Не удалось создать заявку в Колл-центре у заказа {order} причина: {reason}',
                            ['order' => $order->id, 'reason' => $resultCreate['message']]));
                    }
                } else {
                    if (!in_array($order->status_id,
                        [OrderStatus::STATUS_SOURCE_LONG_FORM, OrderStatus::STATUS_SOURCE_SHORT_FORM])
                    ) {
                        $order->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                        /**
                         * Workflow off!!!
                         */
                        $order->shouldValidateStatusId = false;
                    }

                    $order->callCenterRequest->route = Yii::$app->controller->route;
                    $order->callCenterRequest->status = CallCenterRequest::STATUS_PENDING;

                    if (isset($order->deliveryRequest) && $order->deliveryRequest->status == DeliveryRequest::STATUS_ERROR && $order->deliveryRequest->api_error) {
                        // @todo: Необходима будет дополнительная проверка на $order->deliveryRequest->api_error_type == DeliveryRequest::API_ERROR_COURIER
                        $order->callCenterRequest->delivery_error = $order->deliveryRequest->api_error;
                    }

                    if (!$order->callCenterRequest->save()) {
                        throw new \Exception(Yii::t('common',
                            'Не удалось изменить заявку в Колл-центре у заказа {order} причина: {reason}',
                            ['order' => $order->id, 'reason' => $order->callCenterRequest->getFirstErrorAsString()]));
                    }
                }

                $order->route = Yii::$app->controller->route;
                $order->call_center_type = Order::TYPE_CC_RETURN_NO_PROD;

                if ($order->save() && $order->callCenterRequest->save()) {
                    $response['status'] = self::STATUS_SUCCESS;
                } else {
                    throw new \Exception(Yii::t('common', 'Не удалось сменить статусы.'));
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
                $response['status'] = self::STATUS_FAIL;
            }

        } else {
            $response['message'] = Yii::t('common',
                'Заказ должен быть в статусе 1,2,6 или 19, при этом заявка в службе доставки должна быть в статусе "Ошибка" или "Завершена".');
        }

        if ($response['status'] == self::STATUS_FAIL) {
            Yii::$app->notification->send(
                Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR,
                [
                    'order_id' => $order->id,
                    'country' => $order->country->name,
                    'error' => $response['message'],
                ],
                $order->country_id
            );
        }

        return $response;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function isOldApprove($order)
    {
        if (!$callCenterRequest = $order->callCenterRequest) {
            return false;
        }
        if (time() - $callCenterRequest->approved_at > 2629743) {
            return true;
        }
        return false;
    }

    /**
     * @param CrontabTaskLog $log
     * @param null $cc_id
     * @param null $orderId
     * @param string $type
     * @param integer $offsetFrom
     * @param integer $offsetTo
     * @param integer $lastUpdateOffset
     * @param integer $limit
     * @param integer $batchSize
     */
    public static function getStatusesFromCallCenter(
        $log,
        $cc_id = null,
        $orderId = null,
        $type = null,
        $offsetFrom = 0,
        $offsetTo = 0,
        $lastUpdateOffset = 5,
        $limit = 3000,
        $batchSize = 100
    )
    {

        $offsetFrom = intval($offsetFrom);
        $offsetTo = intval($offsetTo);
        $lastUpdateOffset = intval($lastUpdateOffset);
        $limit = intval($limit);

        if (extension_loaded('newrelic')) {
            newrelic_name_transaction("Console/GetCallcenterStatuses");
            newrelic_background_job(true);

            newrelic_add_custom_parameter("cc_id", (string)$cc_id);
        }

        self::$callCenters = self::getCallCentersToGetStatus($type, $offsetFrom, $offsetTo, $lastUpdateOffset);
        self::$deliveries = self::getDeliveriesToGetStatus();

        $queueTypes = Order::getCallCenterTypesToCheckStatus();
        $statusTypes = self::getTypesForGetStatus();

        $fp = [];
        foreach (self::$callCenters as $callCenter) {
            if (is_numeric($cc_id) && !empty($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            // Обновляем один ордер вручную
            if (!is_null($orderId)) {
                $requests = self::getSingleRequestsToGetStatus($orderId);

                $cc_type = Order::find()
                    ->select('call_center_type')
                    ->where([Order::tableName() . ".id" => $orderId])
                    ->asArray()
                    ->one();
                $type = $cc_type['call_center_type'];
                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $logAnswer->url = self::prepareUrlToGetStatus($callCenter, $requests, $type);
                self::getStatusByCallCenterRequests($logAnswer, $requests);
                break;
            }

            foreach ($statusTypes as $statusType) {

                if (!empty($type) && $type != $statusType) {
                    continue;
                }

                // Блокируем обновление статусов для одного КЦ
                if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                    mkdir(Yii::getAlias('@runtime') . '/blocks/');
                }

                $file_lock = "/blocks/call_center_get_statuses_{$callCenter->id}_{$callCenter->country->char_code}_{$statusType}_{$offsetFrom}_{$offsetTo}.lock";

                $fp["{$callCenter->id}_{$statusType}_{$offsetFrom}_{$offsetTo}"] = fopen(Yii::getAlias('@runtime') . $file_lock,
                    'w');

                if (!flock($fp["{$callCenter->id}_{$statusType}_{$offsetFrom}_{$offsetTo}"], LOCK_EX | LOCK_NB)) {
                    fclose($fp["{$callCenter->id}_{$statusType}_{$offsetFrom}_{$offsetTo}"]);
                    continue;
                }

                $cronLaunchedAt = time();

                // Обновляем по крону
                foreach ($queueTypes as $queueType) {
                    $totalOffset = 0;
                    $offset = 0;
                    $minCronLaunchedAt = null;
                    while ($requests = self::getPartRequestsToGetStatus($callCenter, $queueType, $cronLaunchedAt,
                        $offset, $batchSize, $type, $offsetFrom, $offsetTo, $lastUpdateOffset, $minCronLaunchedAt, ['order'])) {

                        $minCronLaunchedAt = max(ArrayHelper::getColumn($requests, 'cron_launched_at'));
                        if (!$minCronLaunchedAt) {
                            $minCronLaunchedAt = 1;
                        }
                        $requestIds = ArrayHelper::getColumn($requests, 'id');
                        // Обновляем сразу cron_launched_at
                        $nowTime = time();
                        CallCenterRequest::updateAll(['cron_launched_at' => $nowTime], ['id' => $requestIds]);

                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                        $logAnswer->url = self::prepareUrlToGetStatus($callCenter, $requests, $queueType);

                        $result = self::getStatusByCallCenterRequests($logAnswer, $requests);

                        $totalOffset += $batchSize - $result['updatedCount'];
                        $offset += $batchSize - $result['updatedCount'];

                        if ($totalOffset >= $limit) {
                            break;
                        }

                        sleep(rand(1, 3));
                    }
                }

                fclose($fp["{$callCenter->id}_{$statusType}_{$offsetFrom}_{$offsetTo}"]); // освобождаем лок сразу.
            }

        }
        unset($fp);
    }

    /**
     * @param CrontabTaskLogAnswer $logAnswer
     * @param CallCenterRequest[] $requests
     * @return array
     */
    public static function getStatusByCallCenterRequests($logAnswer, $requests)
    {
        $updatedCount = 0;
        $logAnswerAnswer = [
            'answer' => '',
            'errors' => [],
        ];

        $logAnswerData = [
            'ids' => ArrayHelper::getColumn($requests, 'foreign_id'),
        ];
        try {
            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logAnswer->url);
            $answer = $curl->query();
        } catch (\Throwable $e) {
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswerAnswer['errors'][] = $e->getMessage();
            $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
            $logAnswer->save();
            return ['success' => false, 'updatedCount' => $updatedCount];
        }


        Yii::$app->db->createCommand('SELECT 1')
            ->execute(); // никто не знает зачем это, рабочее предположение что это такой самопальный keep-alive

        $answer = json_decode($answer, true);

        if (isset($answer['error']['msg'])) {
            $logAnswerAnswer['errors'][] = $answer['error']['msg'];
        }

        if (isset($answer['success']) && $answer['success'] == true) {
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
            $logAnswer->save();

            foreach ($answer['response'] as $orderId => $data) {
                if (isset($data['address_components']) && is_string($data['address_components'])) {
                    $data['address_components'] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;",
                        $data['address_components']), ENT_NOQUOTES, 'UTF-8');
                    $data['address_components'] = json_decode($data['address_components'], true);
                }

                if (array_key_exists($orderId, $requests)) {
                    $request = $requests[$orderId];

                    if (!$request->callCenter->is_amazon_query) {
                        if ($request->order_id == $orderId) {
                            if (self::compareAndSaveRequest($request, $data)) {
                                $updatedCount++;
                            }
                        }
                    }
                }
            }
        } else {
            $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
            $logAnswerAnswer['answer'] = $answer;
        }

        $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
        $logAnswer->save();

        if (!empty($logAnswerAnswer['errors'])) {
            $request = array_shift($requests);

            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_GET_STATUSES_ERROR,
                [
                    'order_id' => is_array($logAnswerData['ids']) ? implode(',',
                        $logAnswerData['ids']) : $logAnswerData['ids'],
                    'country' => $request->order->country->name,
                    'error' => implode(' | ', array_unique($logAnswerAnswer['errors'])),
                ],
                $request->order->country_id
            );

            if (extension_loaded('newrelic')) {
                newrelic_notice_error("non-success answer from call center ({$request->order->country->name})" . $logAnswerAnswer['answer']);

                newrelic_record_custom_event("callcenter_status_response_error", [
                    'order_ids' => is_array($logAnswerData['ids']) ? implode(',',
                        $logAnswerData['ids']) : (string)$logAnswerData['ids'],
                    'country_name' => $request->order->country->name,
                    'errors' => implode(' | ', array_unique($logAnswerAnswer['errors'])),
                    'log_answer_url' => $logAnswer->url,
                ]);
            }


        }

        return ['success' => empty($logAnswerAnswer['errors']), 'updatedCount' => $updatedCount];
    }

    /**
     * @param CallCenter $callCenter
     * @param CallCenterRequest[]|\app\modules\callcenter\models\CallCenterCheckRequest[] $requests
     * @param string $type
     * @return string
     */
    public static function prepareUrlToGetStatus($callCenter, $requests, $type)
    {
        $ids = ArrayHelper::getColumn($requests, 'order_id');
        $ids = implode(',', $ids);

        $time = time();
        $token = md5($callCenter->api_key . $time . $callCenter->api_pid);

        return rtrim($callCenter->url,
                '/') . '/api/method/GetOrdersByIds?token=' . $token . '&time=' . $time . '&pid=' . $callCenter->api_pid . '&order_ids=' . $ids . '&type=' . $type;
    }

    /**
     * @param CallCenterRequest|\app\modules\callcenter\models\CallCenterCheckRequest $request
     * @param bool $checkProduct //проверка наличия продукта на складе
     * @return bool | array
     */
    public static function prepareLeadToSendInCallCenter($request, $checkProduct = true)
    {
        $lead = $request->order;
        $callCenter = $request->callCenter;

        if ($callCenter && $callCenter->url) {
            $data['callCenter'] = $callCenter;

            $trimmedAddress = preg_replace("/(^\s+)|(\s+$)/us", "", $lead->customer_address);

            $data['fields'] = [
                'source' => $lead->sourceModel->unique_system_name,
                'order_id' => $lead->id,
                'order_date' => $lead->created_at,
                'date_created' => $lead->creationDate,
                'fio' => $lead->customer_full_name,
                'address' => !empty($trimmedAddress) ? $trimmedAddress : '(empty)',
                'phone' => $lead->customer_phone,
                'email' => $lead->customer_email,
                'ip' => $lead->customer_ip,
                'price' => $lead->price,
                'currency' => $lead->country->currency ? $lead->country->currency->char_code : '',
                'delivery' => empty($lead->delivery) ? 0 : $lead->delivery,
                'country' => $lead->country->char_code,
                'type' => $lead->call_center_type,
                'resending' => 0,
                'products' => [],
            ];

            if ($lead->call_center_type == Order::TYPE_CC_NEW_RETURN && $request instanceof CallCenterRequest) {
                $data['fields']['comment'] = $request->delivery_error;
            } else {
                $data['fields']['comment'] = $lead->comment;
            }

            foreach ($lead->orderProducts as $orderProduct) {
                //Проверяем, есть ли такой вид товаров на складе
                if ($checkProduct === false || (isset(self::$storagesPart[$lead->country_id]) && in_array($orderProduct->product_id,
                            self::$storagesPart[$lead->country_id]))
                ) {
                    $data['fields']['products'][] = [
                        'product_id' => $orderProduct->product_id,
                        'quantity' => $orderProduct->quantity,
                        'price' => $orderProduct->price,
                    ];
                } else {
                    $request->api_error = "Отсутствуют товары на складе.";
                    $request->save(false, ['api_error']);
                    return false;
                }
            }

            return $data;
        }

        return false;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public static function checkIfProductsAvailable($order)
    {
        foreach ($order->orderProducts as $orderProduct) {
            //Проверяем, есть ли такой вид товаров на складе
            if (isset(self::$storagesPart[$order->country_id]) && in_array($orderProduct->product_id,
                    self::$storagesPart[$order->country_id])
            ) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * @param CrontabTaskLog $log
     */
    public static function getNewOrdersFromCallCenter($log)
    {
        $callCenters = CallCenter::find()
            ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 0])
            ->active()
            ->all();

        foreach ($callCenters as $callCenter) {
            $logResponse = new CrontabTaskLogAnswer();
            $logResponse->log_id = $log->id;
            $logResponse->url = self::prepareUrlToGetNewOrdersFromCallCenter($callCenter);
            $logResponse->data = json_encode([
                'country_id' => $callCenter->country_id,
                'call_center_id' => $callCenter->id
            ], JSON_UNESCAPED_UNICODE);
            $logResponse->status = CrontabTaskLogAnswer::STATUS_FAIL;

            try {
                $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logResponse->url);
                $response = $curl->query();
            } catch (\Throwable $e) {
                $logResponse->answer = json_encode(['error' => $e->getMessage(), JSON_UNESCAPED_UNICODE]);
                $logResponse->save();
                continue;
            }
            $logResponse->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
            $logResponse->answer = $response;

            $response = json_decode($response, true);

            // Если колл-центр ответил
            if (isset($response['success'])) {
                $logResponse->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                if ($response['success'] == true) {
                    if (isset($response['response']) && is_array($response['response'])) {
                        $response['response'] = array_reverse($response['response']);

                        foreach ($response['response'] as $item) {
                            $logSubResponseData = [];

                            $order = Order::find()
                                ->joinWith([
                                    'callCenterRequest',
                                    'callCenterRequest.callCenter',
                                ])
                                ->where([CallCenterRequest::tableName() . '.foreign_id' => $item['id']])
                                ->andWhere(["=", CallCenter::tableName() . '.is_amazon_query', 0])
                                ->byCountryId($callCenter->country_id)
                                ->one();

                            $logSubResponseData['country_id'] = $callCenter->country_id;

                            if (!$order) {
                                $transaction = Yii::$app->db->beginTransaction();
                                try {
                                    $order = new Order();
                                    $order->country_id = $callCenter->country_id;
                                    $order->source_id = $callCenter->source->id;
                                    $order->foreign_id = $item['id'];
                                    $order->customer_full_name = $item['name'];
                                    $order->customer_phone = $item['phone'];
                                    $order->customer_address = $item['address'];
                                    $order->status_id = OrderStatus::STATUS_CC_INPUT_QUEUE;
                                    $order->comment = $item['comment'];
                                    $order->delivery = $item['delivery'];
                                    $order->call_center_create = 1;

                                    $success = true;

                                    // Создаем заказ
                                    if (!$order->save()) {
                                        $success = false;
                                        $logSubResponseData['errors'] = $order->getErrorsAsArray();
                                    }

                                    // Привязываем товары к заказу
                                    if ($success) {
                                        foreach ($item['products'] as $product) {
                                            $orderProduct = new OrderProduct();
                                            $orderProduct->order_id = $order->id;
                                            $orderProduct->product_id = $product['product_id'];
                                            $orderProduct->price = $product['price'];
                                            $orderProduct->quantity = $product['quantity'];

                                            if (!$orderProduct->save()) {
                                                $success = false;
                                                $logSubResponseData['errors'] = $orderProduct->getErrorsAsArray();
                                                $logSubResponseData['errors'][] = json_encode($product,
                                                    JSON_UNESCAPED_UNICODE);
                                            }
                                        }
                                    }

                                    // Создаем заявку в колл-центр
                                    if ($success) {
                                        $request = new CallCenterRequest();
                                        $request->call_center_id = $callCenter->id;
                                        $request->order_id = $order->id;
                                        $request->status = CallCenterRequest::STATUS_IN_PROGRESS;
                                        $request->comment = $item['comment'];
                                        $request->sent_at = time();

                                        $request->foreign_id = $item['id'];
                                        $request->foreign_waiting = 1;

                                        if (!$request->save()) {
                                            $success = false;
                                            $logSubResponseData['errors'] = $request->getErrorsAsArray();
                                        }
                                    }

                                    // Меняем статус заказу, чтобы потом запросить данные из колл-центра
                                    if ($success) {
                                        $order->status_id = OrderStatus::STATUS_CC_POST_CALL;

                                        if (!$order->save()) {
                                            $success = false;
                                            $logSubResponseData['errors'] = $order->getErrorsAsArray();
                                        }
                                    }

                                    if ($success) {
                                        $logSubResponseData['order_id'] = $order->id;

                                        $transaction->commit();
                                    } else {
                                        $transaction->rollBack();
                                    }
                                } catch (\Throwable $e) {
                                    $transaction->rollBack();
                                    $logSubResponseData['errors'] = $e->getMessage();
                                }
                            } else {
                                $logSubResponseData['order_id'] = $order->id;
                                $logSubResponseData['errors'] = ['Такой заказ уже существует.'];
                            }

                            if (isset($logSubResponseData['errors'])) {
                                Yii::$app->notification->send(Notification::TRIGGER_LEAD_NOT_ADDED, [
                                    'exception' => is_array($logSubResponseData['errors']) ? implode(' | ',
                                        array_unique($logSubResponseData['errors'])) : $logSubResponseData['errors'],
                                    'source' => $callCenter->source->name ?? 'Call center get new orders by API',
                                    'foreign_id' => $item['id'] ?? 'Not found'
                                ], $callCenter->country_id);
                            }
                        }
                    }
                } else {

                }
            }

            $logResponse->save();
        }
    }

    /**
     * @param $log
     * @param integer $callCenterId
     * @param integer $limit
     * @param string $selectedQueue
     */
    public static function sendCheckOrder($log, $callCenterId = null, $limit = 100, $selectedQueue = null)
    {
        $query = CallCenterCheckRequest::find()
            ->where([CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_PENDING])
            ->joinWith(['callCenter'])
            ->with(['order.orderProducts', 'order.country.currency'])
            ->andWhere([CallCenter::tableName() . '.active' => 1, CallCenter::tableName() . '.is_amazon_query' => 0])
            ->orderBy([
                CallCenterCheckRequest::tableName() . '.cron_launched_at' => SORT_ASC,
                CallCenterCheckRequest::tableName() . '.created_at' => SORT_ASC
            ])
            ->limit($limit);

        if (!empty($callCenterId)) {
            $query->andWhere([CallCenterCheckRequest::tableName() . '.call_center_id' => $callCenterId]);
        }
        if (!empty($selectedQueue)) {
            $query->andWhere([CallCenterCheckRequest::tableName() . '.type' => $selectedQueue]);
        }

        /**
         * @var \app\modules\callcenter\models\CallCenterCheckRequest[] $requests
         */
        $requests = $query->all();
        foreach ($requests as $request) {

            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

            $logAnswerData = [];

            $logAnswerAnswer = [
                'order_id' => $request->order_id,
                'request_id' => $request->id,
                'answer' => '',
                'errors' => [],
            ];

            $request->cron_launched_at = time();
            $request->save(false, ['cron_launched_at']);

            try {
                if ($data = self::prepareLeadToSendInCallCenter($request, false)) {
                    $logAnswer->url = self::prepareUrlToSendInCallCenter($data['callCenter']);

                    $data['fields']['type'] = $request->type;

                    $logAnswerData = $data['fields'];

                    $result = self::sendCallCenterRequest($data, $request);
                    $logAnswerAnswer['answer'] = $result['answer'];

                    $logAction = new CallCenterCheckRequestActionLog();
                    $logAction->request_id = $request->id;
                    $logAction->type = CallCenterCheckRequestActionLog::TYPE_SEND;
                    $logAction->request = $result['request'] ?? '';
                    $logAction->response = $result['answer'] ?? '';
                    $logAction->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                    $logAction->save();

                    if ($result['success'] == false) {
                        if (!$result['request_error']) {
                            $request->status = CallCenterCheckRequest::STATUS_ERROR;
                            $request->api_error = $result['errors'][0];
                            $logAnswerAnswer['errors'] = $result['errors'];
                        }
                    } else {
                        $logAnswer->status = self::STATUS_SUCCESS;
                        $request->foreign_id = $result['foreign_id'];
                        $request->foreign_status = $result['foreign_status'];
                        $request->status = CallCenterCheckRequest::STATUS_IN_PROGRESS;
                        $request->sent_at = time();
                    }

                    $request->save();
                } else {
                    $logAnswerAnswer['errors'][] = 'Не удалось подготовить лид на отправку в колл-центр.';
                }

                $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                $logAnswer->save();

            } catch (\Throwable $e) {
                $logAnswerAnswer['errors'][] = $e->getMessage();
                $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                $logAnswer->save();
            }
        }
    }

    /**
     * @param $log
     * @param null $callCenterId
     * @param null $queue
     * @param int $limit
     * @param int $batchSize
     */
    public static function getInformationForCheckingOrders(
        $log,
        $callCenterId = null,
        $queue = null,
        $limit = 3000,
        $batchSize = 100
    )
    {
        $callCenters = self::getCallCentersWithCheckingOrders();
        $types = array_keys(CallCenterCheckRequest::getTypes());

        foreach ($callCenters as $callCenter) {
            if (!empty($callCenterId) && $callCenter->id != $callCenterId) {
                continue;
            }

            foreach ($types as $type) {
                if (!empty($queue) && $type != $queue) {
                    continue;
                }
                $totalOffset = 0;
                $offset = 0;
                while ($requests = self::getPartRequestsToCheckOrders($callCenter, $type, null, $offset,
                    $batchSize)) {
                    $logAnswer = new CrontabTaskLogAnswer();
                    $logAnswer->log_id = $log->id;
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                    $logAnswer->url = self::prepareUrlToGetStatus($callCenter, $requests, $type);

                    $result = self::getInformationForCheckingByRequests($logAnswer, $requests);

                    $totalOffset += $batchSize;
                    $offset += $batchSize - $result['updatedCount'];

                    if ($totalOffset >= $limit) {
                        break;
                    }

                    sleep(rand(1, 3));
                }
            }
        }
    }

    /**
     * @param CrontabTaskLogAnswer $logAnswer
     * @param \app\modules\callcenter\models\CallCenterCheckRequest[] $requests
     * @return array
     */
    public static function getInformationForCheckingByRequests($logAnswer, $requests)
    {
        $updatedCount = 0;
        $logAnswerAnswer = [
            'answer' => '',
            'errors' => [],
        ];

        $logAnswerData = [
            'ids' => ArrayHelper::getColumn($requests, 'foreign_id'),
        ];
        try {
            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logAnswer->url);
            $answer = $curl->query();
        } catch (\Throwable $e) {
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswerAnswer['errors'][] = $e->getMessage();
            $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
            $logAnswer->save();
            return ['success' => false, 'updatedCount' => $updatedCount];
        }

        $answer = json_decode($answer, true);

        if (isset($answer['error']['msg'])) {
            $logAnswerAnswer['errors'][] = $answer['error']['msg'];
        }

        if (isset($answer['success']) && $answer['success'] == true) {
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

            foreach ($answer['response'] as $orderId => $data) {
                if (array_key_exists($orderId, $requests)) {
                    $request = $requests[$orderId];

                    $logAction = new CallCenterCheckRequestActionLog();
                    $logAction->request_id = $request->id;
                    $logAction->type = CallCenterCheckRequestActionLog::TYPE_UPDATE;
                    $logAction->request = '';
                    $logAction->response = $data ?? '';
                    $logAction->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                    $logAction->save();

                    if ($request->order_id == $orderId) {
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            if (is_numeric($data['id']) && $request->foreign_id != $data['id']) {
                                $request->foreign_id = $data['id'];
                                $request->save(false, ['foreign_id']);
                            }

                            $request->cron_launched_at = time();
                            $request->save(false, ['cron_launched_at']);

                            if ($request->foreign_status != $data['status']) {
                                $request->foreign_status = $data['status'];
                                $attributes = ['foreign_status'];

                                if ($request->foreign_substatus != $data['sub_status']) {
                                    $request->foreign_substatus = $data['sub_status'];
                                    $attributes[] = 'foreign_substatus';
                                }

                                if (in_array($request->foreign_status, array_keys(CallCenterCheckRequest::$mapStatuses))) {
                                    $request->status = CallCenterCheckRequest::$mapStatuses[$request->foreign_status];
                                    $attributes[] = 'status';

                                    if ($request->status == CallCenterCheckRequest::STATUS_DONE) {

                                        $request->done_at = time();
                                        $attributes[] = 'done_at';

                                        // В зависимости от типа очереди производим какие-то действия
                                        switch ($request->type) {
                                            case CallCenterCheckRequest::TYPE_CHECKUNDELIVERY:
                                            case CallCenterCheckRequest::TYPE_CHECKDELIVERY: {
                                                if (isset($data['history'])) {
                                                    $data['history'] = array_reverse($data['history']);
                                                    foreach ($data['history'] as $item) {
                                                        if (isset($item['sub_status_id'])) {
                                                            $request->foreign_information = $item['sub_status_id'];
                                                            break;
                                                        }
                                                    }
                                                }
                                                $request->comment = $data['comment'];
                                                $attributes[] = 'foreign_information';
                                                $attributes[] = 'comment';
                                                break;
                                            }

                                            case CallCenterCheckRequest::TYPE_TRASH_CHECK: {
                                                if (Lead::$mapStatuses[$data['status']] == OrderStatus::STATUS_CC_APPROVED) {
                                                    $orderCopy = new Order;
                                                    $orderCopy->attributes = $request->order->attributes;
                                                    $orderCopy->status_id = OrderStatus::STATUS_CC_APPROVED;
                                                    $orderCopy->source_id = Source::find()->where(['unique_system_name' => Source::SOURCE_WTRADE_TRASH])->select(['id'])->scalar();
                                                    $orderCopy->income = null;
                                                    $orderCopy->call_center_type = Order::TYPE_CC_ORDER;
                                                    $orderCopy->foreign_id = $request->order->id;
                                                    if (!$orderCopy->save()) {
                                                        $logAnswerAnswer['errors'][] = $orderCopy->getFirstErrorAsString();
                                                        $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                                        $request->api_error = 'Не удалось создать заказ. Причина: ' . $orderCopy->getFirstErrorAsString();
                                                        $attributes[] = 'api_error';
                                                    } else {
                                                        $request->foreign_information = 'New Order: ' . $orderCopy->id;
                                                        $request->comment = 'Лид создан';
                                                        $attributes[] = 'foreign_information';
                                                        $attributes[] = 'comment';
                                                    }
                                                } else {
                                                    $request->comment = isset($data['comment']) ? $data['comment'] : 'Лид не создан';
                                                    $attributes[] = 'comment';
                                                }
                                                break;
                                            }

                                            case CallCenterCheckRequest::TYPE_CHECK_ADDRESS: {
                                                if (Lead::$mapStatuses[$data['status']] == OrderStatus::STATUS_CC_APPROVED) {
                                                    // брокер - отправка в КС

                                                    // выставим статус на прозвоне
                                                    $request->order->callCenterRequest->foreign_status = 1;
                                                    $request->order->callCenterRequest->foreign_substatus = null;
                                                    self::compareAndSaveRequest($request->order->callCenterRequest, $data);


                                                    if ($request->order->getPreviousStatus() == OrderStatus::STATUS_DELIVERY_REJECTED) {
                                                        self::compareAndSaveRequest($request->order->callCenterRequest, $data, true, false);
                                                    }
                                                    elseif (
                                                        $request->order->getPreviousStatus() == OrderStatus::STATUS_DELIVERY_DENIAL ||
                                                        $request->order->getPreviousStatus() == OrderStatus::STATUS_DELIVERY_RETURNED ||
                                                        $request->order->getPreviousStatus() == OrderStatus::STATUS_DELIVERY_REFUND
                                                    ) {
                                                        $orderCopy = $request->order->duplicate(true);
                                                        if (!$orderCopy->save()) {
                                                            throw new \Exception (Yii::t('common', 'Не удалось создать заказ') . ' ' . $orderCopy->getFirstErrorAsString());
                                                        } else {
                                                            $request->order->id = $orderCopy->id;
                                                            self::compareAndSaveRequest($request->order->callCenterRequest, $data, true, false);
                                                        }
                                                    }
                                                    else {
                                                        //self::saveResponse($response, $request->order, false, false);
                                                        self::compareAndSaveRequest($request->order->callCenterRequest, $data, false, false);
                                                    }


                                                } else {
                                                    // в 36 статус
                                                    if ($request->order->canChangeStatusTo(OrderStatus::STATUS_CLIENT_REFUSED)) {
                                                        $request->order->status_id = OrderStatus::STATUS_CLIENT_REFUSED;
                                                        if (!$request->order->save(false, ['status_id'])) {
                                                            $logAnswerAnswer['errors'][] = $request->order->getFirstErrorAsString();
                                                            $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                                            $request->api_error = 'Не удалось обновить заказ. Причина: ' . $request->order->getFirstErrorAsString();
                                                            $attributes[] = 'api_error';
                                                        }
                                                    }
                                                }
                                                break;
                                            }

                                            case CallCenterCheckRequest::TYPE_STILL_WAITING: {
                                                if (Lead::$mapStatuses[$data['status']] == OrderStatus::STATUS_CC_TRASH
                                                    || Lead::$mapStatuses[$data['status']] == OrderStatus::STATUS_CC_REJECTED
                                                ) {
                                                    switch ($request->order->status_id) {
                                                        case OrderStatus::STATUS_CC_CHECKING:
                                                        case OrderStatus::STATUS_DELIVERY_LOST:
                                                        case OrderStatus::STATUS_CC_APPROVED:
                                                        case OrderStatus::STATUS_LOG_ACCEPTED:
                                                        case OrderStatus::STATUS_DELIVERY_REJECTED:
                                                        case OrderStatus::STATUS_DELIVERY_PENDING:
                                                        case OrderStatus::STATUS_LOG_DEFERRED:
                                                            if ($request->order->callCenterRequest) {
                                                                if ($request->order->callCenterRequest->status != CallCenterRequest::STATUS_DONE) {
                                                                    $request->order->callCenterRequest->status = CallCenterRequest::STATUS_DONE;
                                                                    if (!$request->order->callCenterRequest->save()) {
                                                                        $logAnswerAnswer['errors'][] = $request->order->callCenterRequest->getFirstErrorAsString();
                                                                        $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                                                        $request->api_error = 'Не удалось сохранить callCenterRequest. Причина: ' . $request->order->callCenterRequest->getFirstErrorAsString();
                                                                        $attributes[] = 'api_error';
                                                                    }
                                                                }
                                                            }
                                                            if ($request->order->deliveryRequest) {
                                                                if ($request->order->deliveryRequest->status != DeliveryRequest::STATUS_DONE) {
                                                                    $request->order->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                                                                    if (!$request->order->deliveryRequest->save()) {
                                                                        $logAnswerAnswer['errors'][] = $request->order->deliveryRequest->getFirstErrorAsString();
                                                                        $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                                                        $request->api_error = 'Не удалось сохранить deliveryRequest. Причина: ' . $request->order->deliveryRequest->getFirstErrorAsString();
                                                                        $attributes[] = 'api_error';
                                                                    }
                                                                }
                                                            }
                                                            $request->order->status_id = OrderStatus::STATUS_CLIENT_REFUSED;
                                                            break;
                                                        case OrderStatus::STATUS_DELIVERY_RETURNED:
                                                        case OrderStatus::STATUS_DELIVERY_REFUND:
                                                        case OrderStatus::STATUS_CLIENT_REFUSED:
                                                        case OrderStatus::STATUS_DELIVERY_BUYOUT:
                                                        case OrderStatus::STATUS_FINANCE_MONEY_RECEIVED:
                                                            break;
                                                        default:
                                                            $request->order->status_id = OrderStatus::STATUS_DELIVERY_DENIAL;
                                                            break;
                                                    }
                                                } elseif (Lead::$mapStatuses[$data['status']] == OrderStatus::STATUS_CC_APPROVED) {
                                                    switch ($request->order->status_id) {
                                                        case OrderStatus::STATUS_CC_CHECKING:
                                                        case OrderStatus::STATUS_CLIENT_REFUSED:
                                                        case OrderStatus::STATUS_DELIVERY_REJECTED:

                                                            self::compareAndSaveRequest($request->order->callCenterRequest, $data, false, false);
                                                        
                                                            $request->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                                                            $request->order->pending_delivery_id = null;
                                                            // скинуть $request->order->deliveryRequest в отдельн таблицу
                                                            $request->order->deliveryRequest->delete();
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }
                                                if (!$request->order->save()) {
                                                    $logAnswerAnswer['errors'][] = $request->order->getFirstErrorAsString();
                                                    $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                                    $request->api_error = 'Не удалось обновить заказ. Причина: ' . $request->order->getFirstErrorAsString();
                                                    $attributes[] = 'api_error';
                                                    $attributes[] = 'status';
                                                } else {
                                                    $request->foreign_information = 'New Order Status: ' . $request->order->status_id;
                                                    $request->comment = 'Заказ обновлен';
                                                    $attributes[] = 'foreign_information';
                                                    $attributes[] = 'comment';
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!$request->save(true, $attributes)) {
                                    $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                    $request->api_error = 'Не удалось сохранить изменения. Причина: ' . $request->getFirstErrorAsString();
                                    $request->save(true, ['status', 'api_error']);
                                }
                                if ($request->status == CallCenterCheckRequest::STATUS_DONE || $request->status == CallCenterCheckRequest::STATUS_ERROR) {
                                    $updatedCount++;
                                }
                            }
                            $transaction->commit();
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            $logAnswerAnswer['errors'][] = $e->getMessage();
                        }
                    }
                }
            }
        } else {
            $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
            $logAnswerAnswer['answer'] = $answer;
        }

        $logAnswer->data = json_encode($logAnswerData, JSON_UNESCAPED_UNICODE);
        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
        $logAnswer->save();

        return ['success' => empty($logAnswerAnswer['errors']), 'updatedCount' => $updatedCount];
    }

    /**
     * @return CallCenter[]
     */
    protected static function getCallCentersWithCheckingOrders()
    {
        $subQuery = CallCenterCheckRequest::find()
            ->where([CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_IN_PROGRESS])
            ->select('call_center_id')
            ->distinct();
        $query = CallCenter::find()
            ->innerJoin(['ccr' => $subQuery], CallCenter::tableName() . '.id = `ccr`.call_center_id')
            ->where([CallCenter::tableName() . '.is_amazon_query' => 0, CallCenter::tableName() . '.active' => 1])
            ->with('country')
            ->indexBy('id');
        $callCenters = $query->all();
        return $callCenters;
    }

    /**
     * @param $callCenter
     * @param $type
     * @param $cronLaunchedAt
     * @param int $offset
     * @param int $limit
     * @return \app\modules\callcenter\models\CallCenterCheckRequest[]
     */
    protected static function getPartRequestsToCheckOrders(
        $callCenter,
        $type,
        $cronLaunchedAt,
        $offset = 0,
        $limit = 100
    )
    {
        $query = CallCenterCheckRequest::find()
            ->where([CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_IN_PROGRESS]);
        $query->andWhere([CallCenterCheckRequest::tableName() . '.call_center_id' => $callCenter->id]);
        $query->andWhere([CallCenterCheckRequest::tableName() . '.type' => $type]);
        if (!empty($cronLaunchedAt)) {
            $query->andWhere(['<=', CallCenterCheckRequest::tableName() . '.cron_launched_at', $cronLaunchedAt]);
        }
        $query->offset($offset);
        $query->limit($limit);
        $query->with(['order']);
        $query->orderBy([CallCenterCheckRequest::tableName() . '.cron_launched_at' => SORT_ASC]);

        $requests = $query->all();

        return ArrayHelper::index($requests, 'order_id');
    }

    /**
     * @param Order $lead
     * @return CallCenter|boolean
     */
    protected static function getCallCenterByLead($lead)
    {
        // todo: на страну назначается один колл-центр (временная мера)

        if (!array_key_exists($lead->country_id, self::$callCenters)) {
            self::$callCenters[$lead->country_id] = CallCenter::find()
                ->byCountryId($lead->country_id)
                ->active()
                ->orderBy(['id' => SORT_ASC])
                ->one();
        }

        return self::$callCenters[$lead->country_id] ? self::$callCenters[$lead->country_id] : false;
    }

    /**
     * Подготовка складов с товарами по странам
     * @param integer $country_id
     */
    public static function prepareStoragesPart($country_id = null)
    {
        /** @var StorageProduct[] $storagePart */
        $query = StorageProduct::find()
            ->joinWith('storage');
        if (!is_null($country_id)) {
            $query->andWhere([Storage::tableName() . '.country_id' => $country_id]);
        }
        $storagePart = $query->all();
        if ($storagePart) {
            self::$storagesPart = [];
            foreach ($storagePart as $part) {
                self::$storagesPart[$part->storage->country_id][] = $part->product_id;
            }
        }
    }

    /**
     * Подготовка стран с активными колл-центрами
     */
    protected static function prepareCountriesWithCallCenters()
    {
        /** @var CallCenter[] $callCenters */
        $callCenters = CallCenter::find()
            ->active()
            ->groupBy('country_id')
            ->all();

        foreach ($callCenters as $callCenter) {
            if (!in_array($callCenter->country_id, self::$countriesWithCallCenters)) {
                self::$countriesWithCallCenters[] = $callCenter->country_id;
            }
        }
    }

    /**
     * @param CallCenter $callCenter
     * @param string $queueType
     * @param integer $cronLaunchedAt
     * @param integer $offset
     * @param integer $limit
     * @param string $type
     * @param integer $offsetFrom
     * @param integer $offsetTo
     * @param integer $lastUpdateOffset
     * @param integer $minUpdateOffset
     * @param array $with
     * @return CallCenterRequest[]
     */
    public static function getPartRequestsToGetStatus(
        $callCenter,
        $queueType,
        $cronLaunchedAt,
        $offset = 0,
        $limit = 100,
        $type = null,
        $offsetFrom = 0,
        $offsetTo = 0,
        $lastUpdateOffset = 0,
        $minUpdateOffset = null,
        $with = null
    )
    {
        $subQuery = CallCenterRequest::find()->select([
            'order_id',
            'cron_launched_at',
            'foreign_waiting',
            'created_at',
            'id'
        ])->where(['call_center_id' => $callCenter->id, 'status' => CallCenterRequest::STATUS_IN_PROGRESS]);

        $query = CallCenterRequest::find()
            ->innerJoin(['preparedFilter' => $subQuery], CallCenterRequest::tableName() . '.id = preparedFilter.id')
            ->joinWith(['order'], false)
            ->where([Order::tableName() . '.call_center_type' => $queueType])
            ->andWhere(['<', 'preparedFilter.foreign_waiting', 1])
            ->orderBy(['preparedFilter.cron_launched_at' => SORT_ASC])
            ->offset($offset)
            ->limit($limit)
            ->indexBy('order_id');
        if (is_array($with) || is_string($with)) {
            $query->with($with);
        }

        if (!empty($cronLaunchedAt)) {
            $query->andWhere([
                'or',
                ['is', 'preparedFilter.cron_launched_at', null],
                ['<=', 'preparedFilter.cron_launched_at', $cronLaunchedAt]
            ]);
        }

        if ($minUpdateOffset) {
            $query->andWhere(['>', 'preparedFilter.cron_launched_at', $minUpdateOffset]);
        }

        if (!empty($type)) {
            $query->andWhere(self::buildConditionForGetType($type, Order::tableName() . '.status_id'));
        }
        if (!empty($offsetFrom)) {
            $time = strtotime("-{$offsetFrom} days", $cronLaunchedAt ?: time());
            $query->andWhere(['<=', 'preparedFilter.created_at', $time]);
        }
        if (!empty($offsetTo)) {
            $time = strtotime("-{$offsetTo} days", $cronLaunchedAt ?: time());
            // Чтобы не было такого, что два крона с одинаковыми граничными офсетами запустились с разницей в несколько секунд и заказ умудрился попасть в оба крона, ставим 20 минутный буфер
            $time += 60 * 10;
            $query->andWhere(['>', 'preparedFilter.created_at', $time]);
        }

        if (!empty($lastUpdateOffset)) {
            $time = strtotime("-{$lastUpdateOffset} minutes", $cronLaunchedAt ?: time());
            $query->andWhere([
                'or',
                ['is', 'preparedFilter.cron_launched_at', null],
                ['<=', 'preparedFilter.cron_launched_at', $time]
            ]);
        }

        return $query->all();
    }

    /**
     * @param $orderId
     * @return CallCenterRequest[]
     */
    protected static function getSingleRequestsToGetStatus($orderId)
    {
        if (is_numeric($orderId)) {
            $requests = CallCenterRequest::find()
                ->joinWith('order')
                ->where([CallCenterRequest::tableName() . '.order_id' => $orderId])
                ->andWhere([CallCenterRequest::tableName() . '.foreign_waiting' => 0])
                ->orderBy(['cron_launched_at' => SORT_ASC])
                ->all();

            if (!empty($requests)) {
                return ArrayHelper::index($requests, 'order_id');
            }
        }

        return null;
    }

    /**
     * @param CallCenter $callCenter
     * @return string
     */
    protected static function prepareUrlToSendInCallCenter($callCenter)
    {
        $time = time();
        $token = md5($callCenter->api_key . $time . $callCenter->api_pid);

        return rtrim($callCenter->url,
                '/') . '/api/method/OrderNew?token=' . $token . '&time=' . $time . '&pid=' . $callCenter->api_pid;
    }

    /**
     * @param CallCenter $callCenter
     * @return string
     */
    protected static function prepareUrlToGetNewOrdersFromCallCenter($callCenter)
    {
        $time = time();
        $token = md5($callCenter->api_key . $time . $callCenter->api_pid);

        return rtrim($callCenter->url,
                '/') . '/api/method/GetOrdersAfterSales?token=' . $token . '&time=' . $time . '&pid=' . $callCenter->api_pid;
    }

    /**
     * @param string $type
     * @param integer $offsetFrom
     * @param integer $offsetTo
     * @param integer $lastUpdateOffset
     * @return CallCenter[]
     */
    public static function getCallCentersToGetStatus(
        $type = null,
        $offsetFrom = 0,
        $offsetTo = 0,
        $lastUpdateOffset = 0
    )
    {
        $offsetFrom = intval($offsetFrom);
        $offsetTo = intval($offsetTo);
        $lastUpdateOffset = intval($lastUpdateOffset);

        $subQuery = CallCenterRequest::find()
            ->select([
                'call_center_id',
                'cron_launched_at' => 'MIN(cron_launched_at)'
            ])
            ->where([CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS])
            ->orderBy([CallCenterRequest::tableName() . '.cron_launched_at' => SORT_ASC])
            ->groupBy([CallCenterRequest::tableName().'.call_center_id']);

        if (!empty($type)) {
            $subQuery->joinWith('order');
            $subQuery->andWhere(self::buildConditionForGetType($type, Order::tableName() . '.status_id'));
        }
        if (!empty($offsetFrom)) {
            $time = strtotime("-{$offsetFrom} days");
            $subQuery->andWhere(['<=', CallCenterRequest::tableName() . '.created_at', $time]);
        }
        if (!empty($offsetTo)) {
            $time = strtotime("-{$offsetTo} days");
            $subQuery->andWhere(['>', CallCenterRequest::tableName() . '.created_at', $time]);
        }
        if (!empty($lastUpdateOffset)) {
            $time = strtotime("-{$lastUpdateOffset} minutes");
            $subQuery->having([
                'or',
                ['is', 'cron_launched_at', null],
                ['<=', 'cron_launched_at', $time]
            ]);
        }

        $query = CallCenter::find()
            ->innerJoin([CallCenterRequest::tableName() => $subQuery], CallCenterRequest::tableName() . '.call_center_id = ' . CallCenter::tableName() . '.id')
            ->where([
                CallCenter::tableName() . '.is_amazon_query' => 0
            ])
            ->indexBy('id');

        $callCenters = $query->all();

        return $callCenters;
    }

    /**
     * @param CallCenter $callCenter
     * @param integer $cronLaunchedAt
     * @param null $type
     * @param int $offsetFrom
     * @param int $offsetTo
     * @param int $lastUpdateOffset
     * @return array
     */
    public static function getCallCenterTypesToGetStatus($callCenter, $cronLaunchedAt, $type = null, $offsetFrom = 0, $offsetTo = 0, $lastUpdateOffset = 0)
    {
        $subQuery = CallCenterRequest::find()->select([
            'order_id',
            'status',
            'foreign_waiting',
            'cron_launched_at',
            'created_at'
        ])->where([
            CallCenterRequest::tableName() . '.call_center_id' => $callCenter->id,
            CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS
        ]);
        $query = new Query();
        $query->select(['type' => Order::tableName() . '.call_center_type']);
        $query->from(['preparedFilter' => $subQuery]);
        $query->join('JOIN', Order::tableName(), Order::tableName() . '.id = preparedFilter.order_id');
        $query->groupBy([Order::tableName() . '.call_center_type']);
        // Хак, т.к. на foreign_waiting висит индекс, а поле бинарное, то поиск по нему очень долгий, поэтому берем минимум (0, максимум будет - 1)
        $query->having(['MIN(preparedFilter.foreign_waiting)' => 0]);

        if (!empty($cronLaunchedAt)) {
            $query->andWhere([
                'or',
                ['is', 'preparedFilter.cron_launched_at', null],
                ['<=', 'preparedFilter.cron_launched_at', $cronLaunchedAt]
            ]);
        }

        if (!empty($type)) {
            $query->andWhere(self::buildConditionForGetType($type, Order::tableName() . '.status_id'));
        }
        if (!empty($offsetFrom)) {
            $time = strtotime("-{$offsetFrom} days", $cronLaunchedAt ?: time());
            $query->andWhere(['<=', 'preparedFilter.created_at', $time]);
        }
        if (!empty($offsetTo)) {
            $time = strtotime("-{$offsetTo} days", $cronLaunchedAt ?: time());
            $time += 60 * 10;
            $query->andWhere(['>', 'preparedFilter.created_at', $time]);
        }

        if (!empty($lastUpdateOffset)) {
            $time = strtotime("-{$lastUpdateOffset} minutes", $cronLaunchedAt ?: time());
            $query->andWhere([
                'or',
                ['is', 'preparedFilter.cron_launched_at', null],
                ['<=', 'preparedFilter.cron_launched_at', $time]
            ]);
        }

        $types = $query->column();

        $response = [];

        foreach ($types as $queueType) {
            if (!isset($response[$callCenter->id])) {
                $response[$callCenter->id] = [];
            }
            $response[$callCenter->id][] = $queueType;
        }

        return $response;
    }

    /**
     * @var bool $toAmazonQueue
     * @return CallCenter[]
     */
    protected static function getCallCentersToSendRequests()
    {
        $subQuery = CallCenterRequest::find()->where(['status' => CallCenterRequest::STATUS_PENDING])->select('call_center_id')->distinct();
        $callCenters = CallCenter::find()
            ->innerJoin(['ccr' => $subQuery], 'ccr.call_center_id = ' . CallCenter::tableName() . '.id')
            ->where([
                CallCenter::tableName() . '.active' => 1,
                CallCenter::tableName() . '.is_amazon_query' => 0,
            ])
            ->with('country')
            ->indexBy('id')
            ->all();

        return $callCenters;
    }

    /**
     * @return Delivery[]
     */
    protected static function getDeliveriesToGetStatus()
    {
        $activeDeliveries = [];

        $deliveries = Delivery::find()
            ->joinWith(['workflow'])
            ->autoSending()
            ->active()
            ->all();

        foreach ($deliveries as $delivery) {
            if (!array_key_exists($delivery->country_id, $activeDeliveries)) {
                $activeDeliveries[$delivery->country_id] = [];
            }

            $activeDeliveries[$delivery->country_id][] = $delivery;
        }

        $allowDeliveries = [];

        foreach ($activeDeliveries as $countryId => $deliveries) {
            // решили в случае если много $deliveries возвращать первую, а не только если одна, чтобы не останавливать процесс отправки
            $allowDeliveries[$countryId] = $deliveries[0];
        }

        return $allowDeliveries;
    }

    /**
     * @param CallCenterRequest $request
     * @param array $data
     * @param bool $changeStatus
     * @param bool $changeDelivery
     * @return bool
     */
    public static function compareAndSaveRequest($request, $data, $changeStatus = true, $changeDelivery = true)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (is_numeric($data['id']) && $request->foreign_id != $data['id']) {
                $request->foreign_id = $data['id'];
                $request->save(false, ['foreign_id']);
            }

            $foreignStatus = $request->foreign_status;

            if ($request->foreign_status != $data['status']) {
                $order = $request->order;

                $status = array_key_exists($data['status'],
                    self::$mapStatuses) ? self::$mapStatuses[$data['status']] : false;

                $success = true;
                $compareError = '';

                if ($success) {
                    if ($status === false) {
                        $success = false;
                        $compareError = 'Неверное значение статуса.';
                    }
                }

                if ($success) {
                    if ($order->status_id != $status && $changeStatus) {
                        if (!$order->canChangeStatusTo($status)) {
                            $success = false;
                            $compareError = 'Неразрешенное значение статуса.';
                        }
                    }
                    if ($changeStatus) {
                        $success = true;
                    }
                }

                if ($success) {
                    $saveAttributes = [];

                    $changeStatuses = [
                        OrderStatus::STATUS_CC_APPROVED,
                        OrderStatus::STATUS_CC_REJECTED,
                        OrderStatus::STATUS_CC_TRASH,
                        OrderStatus::STATUS_CC_DOUBLE,
                    ];

                    if (in_array($status, $changeStatuses)) {
                        $request->status = CallCenterRequest::STATUS_DONE;
                        $saveAttributes[] = 'status';

                        if (
                            ($status == OrderStatus::STATUS_CC_APPROVED) &&
                            isset($data['history']) &&
                            is_array($data['history'])
                        ) {
                            foreach ($data['history'] as $history) {
                                if (isset($history['status_id']) && $history['status_id'] == 4 && isset($history['oe_changed'])) {
                                    $date_changed = strtotime($history['oe_changed']);
                                    $request->approved_at = ($date_changed !== false) ? ($date_changed) : null;
                                    $saveAttributes[] = 'approved_at';
                                }
                            }
                        }
                    }

                    if (isset($data['autocheck_address'])) {
                        $request->autocheck_address = $data['autocheck_address'];
                        $saveAttributes[] = 'autocheck_address';
                    }

                    if (!empty($data['history'])) {
                        CallCenterRequestHistory::deleteAll(['call_center_id' => $request->id]);
                        $lastOperatorId = 0;
                        foreach ($data['history'] as $historyItem) {
                            $requestHistory = new CallCenterRequestHistory([
                                'call_center_request_id' => $request->id,
                                'call_center_id' => $request->call_center_id,
                                'operator_id' => $historyItem['oper'],
                                'foreign_status' => $historyItem['status_id'],
                                'called_at' => strtotime($historyItem['oe_changed']),
                            ]);
                            $requestHistory->save();
                            if (Lead::$mapStatuses[$historyItem['status_id']] == OrderStatus::STATUS_CC_APPROVED) {
                                $lastOperatorId = $historyItem['oper'];
                            }
                        }
                        $request->last_foreign_operator = $lastOperatorId;
                        $saveAttributes[] = 'last_foreign_operator';
                    }

                    if (isset($data['call_data'])) {
                        CallCenterRequestCallData::saveCallData($data['call_data'], $request->id);
                    }

                    $request->foreign_status = $data['status'];
                    $saveAttributes[] = 'foreign_status';

                    $request->foreign_substatus = $data['sub_status'];
                    $saveAttributes[] = 'foreign_substatus';

                    $request->comment = $data['comment'];
                    $saveAttributes[] = 'comment';

                    if (!$request->save(true, $saveAttributes)) {
                        $success = false;
                        $compareError = $request->getFirstErrorAsString();
                    }
                    // END

                    if (($lastUpdateResponse = $request->addUpdateResponse(json_encode($data, JSON_UNESCAPED_UNICODE))) && !$lastUpdateResponse->save()) {
                        $success = false;
                        $compareError = $lastUpdateResponse->getFirstErrorAsString();
                    }
                }

                if ($success) {

                    // Если из колл-центра пришел признак число звонков
                    if (isset($data['call_count'])) {
                        $callsCount = (int)$data['call_count'];
                        if ($request->extra instanceof CallCenterRequestExtra) {
                            $request->extra->calls_count = $callsCount;
                            if (!$request->extra->save()) {
                                $compareError = $request->extra->getFirstErrorAsString();
                            }
                        } else {
                            $callCenterRequestExtra = new CallCenterRequestExtra();
                            $callCenterRequestExtra->call_center_request_id = $request->id;
                            $callCenterRequestExtra->calls_count = $callsCount;
                            if (!$callCenterRequestExtra->save()) {
                                $compareError = $callCenterRequestExtra->getFirstErrorAsString();
                            }
                        }
                    }

                    if ($changeStatus) {
                        $order->status_id = $status;
                    }

                    $mapAttributes = [
                        'name' => 'customer_full_name',
                        'phone' => 'customer_phone',
                        'mobile' => 'customer_mobile',
                        'address' => 'customer_address',
                        'delivery' => 'delivery',
                        'email' => 'customer_email',
                    ];

                    foreach ($mapAttributes as $attributeCallCenter => $attributeOrder) {
                        if($attributeCallCenter == 'name' && empty($data[$attributeCallCenter])){
                            $data[$attributeCallCenter] = $order->customer_full_name;
                        }
                        $order->$attributeOrder = $data[$attributeCallCenter];
                        foreach ($order->getActiveValidators($attributeOrder) as $validator) {
                            if ($validator instanceof StringValidator && $validator->max !== null) {
                                $order->$attributeOrder = mb_strcut($order->$attributeOrder, 0, $validator->max);
                                break;
                            }
                        }
                    }

                    $mapAddressAttributes = [
                        'street_number' => 'customer_house_number',
                        'street_address' => 'customer_street',
                        'street' => 'customer_street',
                        'road' => 'customer_street',
                        'state_province' => 'customer_province',
                        'province' => 'customer_province',
                        'customer_district' => 'customer_district',
                        'customer_subdistrict' => 'customer_subdistrict',
                        'city' => 'customer_city',
                        'customer_city_code' => 'customer_city_code',
                        'zip' => 'customer_zip',
                        'customer_zip' => 'customer_zip',
                        'google_lon' => 'longitude',
                        'google_lat' => 'latitude',
                        'customer_street' => 'customer_street',
                        'district' => 'customer_district',
                        'customer_city' => 'customer_city',
                        'customer_house_number' => 'customer_house_number',
                        'customer_province' => 'customer_province',
                        'customer_email' => 'customer_email',
                        'email' => 'customer_email',
                        'state' => 'customer_province',
                        'customer_address_add' => 'customer_address_additional',
                        'house_number' => 'customer_house_number',
                    ];

                    foreach ($mapAddressAttributes as $attributeCallCenter => $attributeOrder) {
                        if (array_key_exists($attributeCallCenter, $data['address_components'])) {
                            if (
                                ($attributeOrder !== 'customer_house_number') ||
                                ($attributeOrder === 'customer_house_number' && trim($data['address_components'][$attributeCallCenter]) != '' && $data['address_components'][$attributeCallCenter] != null)
                            ) {
                                $order->$attributeOrder = $data['address_components'][$attributeCallCenter];
                                unset($data['address_components'][$attributeCallCenter]);
                            }
                            foreach ($order->getActiveValidators($attributeOrder) as $validator) {
                                if ($validator instanceof StringValidator && $validator->max !== null) {
                                    $order->$attributeOrder = mb_strcut($order->$attributeOrder, 0, $validator->max);
                                    break;
                                }
                            }
                        }
                    }
                    $addressAddition = $data['address_components'];


                    /**
                     * Запись времени доставки string вида 6am-9pm
                     * в customer_address_add чтобы не заводить еще одного поля в ордере
                     */
                    if (isset($data['shipping']['delivery_time'])) {
                        $addressAddition['delivery_time'] = $data['shipping']['delivery_time'];
                    }
                    $order->customer_address_add = json_encode($addressAddition);

                    if ($order->save()) {
                        // Пересохранение товаров
                        foreach ($order->orderProducts as $orderProduct) {
                            $orderProduct->delete();
                        }

                        $products = [];
                        foreach ($data['products'] as $product) {
                            $orderProduct = new OrderProduct();
                            $orderProduct->order_id = $order->id;
                            $orderProduct->product_id = $product['product_id'];
                            $orderProduct->price = $product['price'];
                            $orderProduct->quantity = $product['quantity'];

                            if (!$orderProduct->save()) {
                                throw new \Exception('Не удалось сохранить товар заказа. Ошибка: ' . $orderProduct->getFirstErrorAsString());
                            }

                            $products[] = $orderProduct;
                        }

                        // Если колл-центр принял заказ
                        if ($order->status_id == OrderStatus::STATUS_CC_APPROVED) {
                            // Если из колл-центра пришло время доставки
                            if (isset($data['shipping']['time_from'])) {
                                $time_from = strtotime($data['shipping']['time_from']);
                                $order->delivery_time_from = ($time_from !== false) ? ($time_from) : null;
                                $order->save(false, ['delivery_time_from']);
                            }

                            if (isset($data['shipping']['time_to'])) {
                                $time_to = strtotime($data['shipping']['time_to']);
                                $order->delivery_time_to = ($time_to !== false) ? ($time_to) : null;
                                $order->save(false, ['delivery_time_to']);
                            }


                            if ($changeDelivery) {
                                // Если из колл-центра пришла служба доставки
                                $deliveryId = isset($data['shipping']['id']) ? $data['shipping']['id'] : null;

                                // Если из колл-центра пришел признак экспресс доставки
                                $expressDelivery = (isset($data['express_delivery']) && $data['express_delivery']) ? true : false;

                                $haveAnyDelivery = $deliveryId ? true : false;
                                $canDelivery = false;

                                // выставить стоимость доставки
                                $deliveryPrice = isset($data['delivery']) ? $data['delivery'] : null;

                                // Если из колл-центра пришел признак акции
                                $packageId = $data['package_id'] ?? \app\modules\order\models\Lead::detectPackageByProducts($data['products']);

                                // брокер
                                $brokerResult = $order->findDeliveryByBroker($products, $packageId, $deliveryPrice);
                                if ($brokerResult) {
                                    if ($brokerResult['deliveryId']) {
                                        $deliveryId = $brokerResult['deliveryId'];
                                        $haveAnyDelivery = true;
                                        // Если брокер определил КС, то она точно может доставлять
                                        $canDelivery = true;
                                    }
                                }
                                if (!is_null($deliveryId) && $haveAnyDelivery === true) {
                                    $delivery = Delivery::findOne($deliveryId);
                                } else {
                                    $delivery = array_key_exists($order->country_id, self::$deliveries) ? self::$deliveries[$order->country_id] : null;
                                }

                                if ($delivery) {
                                    if (!$canDelivery) {
                                        // брокер не смог определить КС, проверяем ту, что пришла из КЦ
                                        $canDelivery = $delivery->canDeliveryOrder($order, $data['products'], $packageId);
                                    }
                                    if ($canDelivery) {
                                        $delivery->prepareOrderToSend($order, $deliveryPrice);
                                    }
                                }
                                // сохраним признак express_delivery для заказа
                                OrderExpressDelivery::saveIt($order->id, $expressDelivery);
                            }
                        }
                        // Кэшим даты  для быстрой выдачи статусов по лидам,
                        $request->cacheLastAttemptDates();
                    } else {
                        $request->status = CallCenterRequest::STATUS_ERROR;
                        $request->foreign_status = $foreignStatus;
                        $request->api_error = 'Не удалось сохранить заказ. Ошибка: ' . $order->getFirstErrorAsString();
                        $request->save(false, ['status', 'api_error', 'foreign_status']);

                        $compareError = $order->getFirstErrorAsString();
                    }
                }

                if ($compareError) {
                    Yii::$app->notification->send(
                        Notification::TRIGGER_CALL_CENTER_GET_STATUSES_ERROR, [
                        'order_id' => $order->id,
                        'country' => $order->country->name,
                        'error' => $compareError,
                    ],
                        $order->country_id
                    );
                }
            }

            $transaction->commit();

        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->notification->send(
                Notification::TRIGGER_EXCEPTION_GET_STATUSES_FROM_CALL_CENTER, [
                'order_id' => $request->order_id,
                'error' => $e->getMessage(),
            ],
                $request->order->country_id
            );

            return false;
        }
        return true;
    }

    /**
     * @param integer|Delivery $delivery
     * @param Order $order
     * @return array
     */
    protected static function getDeliveryNextStatuses($delivery, $order)
    {
        $nextStatuses = [];

        if (is_integer($delivery)) {
            $delivery = Delivery::find()
                ->joinWith(['workflow'])
                ->where([Delivery::tableName() . '.id' => $delivery])
                ->active()
                ->one();
        }

        if ($delivery && $delivery->workflow) {
            $nextStatuses = $delivery->workflow->getNextStatuses($order->status_id);
        }

        return $nextStatuses;
    }

    /**
     * @param null $cc_id Call Center ID
     */
    public static function getOperFromCallCenter($cc_id = null)
    {
        $taken = 0;
        $updated = 0;
        $notfound = 0;

        $callCenters = CallCenter::find()->where([CallCenter::tableName() . '.is_amazon_query' => 0])->active()->all();
        foreach ($callCenters as $callCenter) {
            if (is_numeric($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            $types = array_keys(Order::getTypesCallCenter());
            foreach ($types as $type) {

                $offset = 0;
                $limit = 30;

                while ($requests = self::getPartRequestsToGetOper($callCenter, $type, $offset, $limit)) {

                    $taken = $taken + sizeof($requests);

                    $url = self::prepareUrlToGetStatus($callCenter, $requests, $type);

                    //Console::stdout($url . PHP_EOL);

                    $curl = CurlFactory::build(CurlFactory::METHOD_GET, $url);
                    $answer = $curl->query();
                    $answer = json_decode($answer, true);

                    foreach ($requests as $request) {

                        if (isset($answer['response'][$request->order_id]) && !empty($answer['response'][$request->order_id]['history'])) {
                            $operId = $answer['response'][$request->order_id]['history'][count($answer['response'][$request->order_id]['history']) - 1]['oper'];
                        } else {
                            //пометить запросы для которых не найден оператор
                            $operId = -1;
                            $notfound++;
                        }

                        $request->last_foreign_operator = $operId;
                        $saveAttributes = [];
                        $saveAttributes[] = 'last_foreign_operator';
                        if ($request->save(false, $saveAttributes)) {
                            $updated++;
                        } else {
                            //по какойто причине не сохранилось
                            Console::stdout("save request error" . PHP_EOL);
                            die();
                        }
                    }

                    //offset не нужен т.к. мы делаем update тех же данных что и попалдают в выборку
                    //$offset += $limit;
                    usleep(rand(500, 1000));

                }
                usleep(rand(500, 1000));
            }
        }
        return [$taken, $updated, $notfound];
    }


    /**
     * @param CallCenter $callCenter
     * @param string $type
     * @param integer $offset
     * @param integer $limit
     * @return CallCenterRequest[]
     */
    protected static function getPartRequestsToGetOper($callCenter, $type, $offset = 0, $limit = 30)
    {
        $requests = CallCenterRequest::find()
            ->joinWith('order')
            ->byCallCenterId($callCenter->id)
            ->andWhere(CallCenterRequest::tableName() . '.last_foreign_operator is null')
            ->andWhere([Order::tableName() . '.call_center_type' => $type])
            ->orderBy([CallCenterRequest::tableName() . '.id' => SORT_DESC])
            ->offset($offset)
            ->limit($limit)
            ->all();
        return ArrayHelper::index($requests, 'order_id');
    }

    /**
     * @param string $type
     * @param string $column
     * @return array
     */
    protected static function buildConditionForGetType($type, $column)
    {
        switch ($type) {
            case self::GET_STATUSES_TYPE_NOT_DEFERRED: {
                return ['in', $column, [OrderStatus::STATUS_CC_POST_CALL]];
            }
            case self::GET_STATUSES_TYPE_DEFERRED: {
                return ['not in', $column, [OrderStatus::STATUS_CC_POST_CALL]];
            }
            default:
                return [];
        }
    }

    protected static function getTypesForGetStatus()
    {
        return [
            self::GET_STATUSES_TYPE_NOT_DEFERRED,
            self::GET_STATUSES_TYPE_DEFERRED,
        ];
    }

    /**
     * Формирование ссылки
     * @param CallCenter $callCenter
     * @return string
     */
    public static function prepareUrlToGetUsers($callCenter)
    {
        if ($callCenter->is_amazon_query) {
            $token = base64_encode(Yii::$app->params['api2wcallIdentity']);
            $url = rtrim(Yii::$app->params['api2wcallUrl'], '/') . '/v1/user/get-users?token=' . $token;
        } else {
            $time = time();
            $token = md5($callCenter->api_key . $time . $callCenter->api_pid);
            $url = rtrim($callCenter->url, '/') . '/api/method/GetUsers?token=' . $token . '&time=' . $time . '&pid=' . $callCenter->api_pid;
        }

        return $url;
    }

    /**
     * Формирование ссылки для получения данных об отработанном времени операторов
     * @param $callCenter
     * @param $dateFrom
     * @param $dateTo
     * @param string $login
     * @return string
     */
    public static function prepareUrlToGetOpersActivityData($callCenter, $dateFrom, $dateTo, $login = '')
    {
        if ($callCenter->is_amazon_query) {
            $token = base64_encode(Yii::$app->params['api2wcallIdentity']);
            $url = rtrim(Yii::$app->params['api2wcallUrl'], '/') . "/v1/user/get-opers-activity-data?token={$token}&date_from={$dateFrom}&date_to={$dateTo}&country={$callCenter->country->char_code}";
            if ($login != '') {
                $url.= "&login={$login}";
            }
        } else {
            $time = time();
            $token = md5($callCenter->api_key . $time . $callCenter->api_pid);
            $url = rtrim($callCenter->url, '/') . '/api/method/GetOpersActivityData?token=' . $token . '&time=' . $time . '&date_from=' . $dateFrom . '&date_to=' . $dateTo . '&pid=' . $callCenter->api_pid;
        }

        return $url;
    }

    /**
     * Формирование ссылки для получения данных об количестве звонков операторов
     * @param CallCenter $callCenter
     * @param string $dateFrom
     * @param string $dateTo
     * @return string
     */
    public static function prepareUrlToGetOpersNumberOfCalls($callCenter, $dateFrom, $dateTo)
    {
        if ($callCenter->is_amazon_query) {
            $token = base64_encode(Yii::$app->params['api2wcallIdentity']);
            $url = rtrim(Yii::$app->params['api2wcallUrl'], '/') . "/v1/user/get-call-stat?token={$token}&date_from={$dateFrom}&date_to={$dateTo}";
        } else {
            $time = time();
            $token = md5($callCenter->api_key . $time . $callCenter->api_pid);
            $url = rtrim($callCenter->url,
                    '/') . '/api/method/GetCallStat?token=' . $token . '&time=' . $time . '&date_from=' . $dateFrom . '&date_to=' . $dateTo . '&pid=' . $callCenter->api_pid;
        }

        return $url;
    }

    /**
     * Формирование ссылки для получения данных о кол-ве звонков по заказам
     * @return string
     */
    public static function prepareUrlToGetUserOrder()
    {
        return rtrim(Yii::$app->params['api2wcallUrl'], '/') . "/v1/order/opers";
    }

    /**
     * Формирование ссылки для получения данных о кол-ве звонков по товарам
     * @return string
     */
    public static function prepareUrlToGetProductCall()
    {
        return rtrim(Yii::$app->params['api2wcallUrl'], '/') . "/v1/order/prod-calls";
    }

    /**
     * Получение списка пользователей из колл-центра
     * @param CrontabTaskLog $log
     * @param null $cc_id
     */
    public static function getUsersFromCallCenter($log, $cc_id = null)
    {
        $callCenters = CallCenter::find()->active()->all();
        foreach ($callCenters as $callCenter) {

            if (is_numeric($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

            $logAnswer->url = self::prepareUrlToGetUsers($callCenter);

            $logAnswerAnswer = [
                'answer' => '',
                'errors' => [],
            ];

            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logAnswer->url);
            $answer = $curl->query();
            //$logAnswerAnswer['answer'] = $answer;

            $answer = json_decode($answer, true);

            if (isset($answer['error']['msg'])) {
                $logAnswerAnswer['errors'][] = $answer['error']['msg'];
            }

            $savedUsers = 0;
            $savedPersons = 0;
            $savedPhotos = 0;
            if (isset($answer['success']) && $answer['success'] == true) {
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $logAnswer->save();

                $rolesMapping = [
                    CallCenterUser::USER_ROLE_OPER => 5,
                    CallCenterUser::USER_ROLE_CURATOR => 3,
                    CallCenterUser::USER_ROLE_NOTACTIVE => 5,
                    CallCenterUser::USER_ROLE_SUPERVISOR => 2,
                    CallCenterUser::USER_ROLE_HR => 6,
                    CallCenterUser::USER_ROLE_IT => 7,
                    CallCenterUser::USER_ROLE_ADMIN_DIRECTOR => 1,
                    CallCenterUser::USER_ROLE_CLEANER => 8,

                    /*
                     * TODO новый КЦ замапить
                     * вообще этот скрипт сейчас отключен, т.к. мы не можем правильно привязать физ лицо
                     *
                    'controller' => 77,
                    'operator' => 5,
                    'supervisor' => 2,
                    'auditor' => 76,
                    'team_lead' => 3,
                    'curator' => 78,
                    */
                ];


                foreach ($answer['response']['users'] as $data) {
                    if (isset($data['user_id']) && isset($data['user_login'])) {
                        $callCenterUser = CallCenterUser::find()
                            ->byCallCenterId($callCenter->id)
                            ->byUserId($data['user_id'])
                            ->one();
                        if (!$callCenterUser instanceof CallCenterUser) {
                            $callCenterUser = new CallCenterUser();
                            $callCenterUser->callcenter_id = $callCenter->id;
                            $callCenterUser->user_id = (string)$data['user_id'];
                        }
                        $callCenterUser->user_login = $data['user_login'];
                        $callCenterUser->user_role = $data['user_role'] ?? '';
                        if ($data['user_role'] != CallCenterUser::USER_ROLE_NOTACTIVE) {
                            $callCenterUser->active = CallCenterUser::ACTIVE;
                        }
                        if ($callCenterUser->save()) {

                            if ($callCenterUser->user_role != CallCenterUser::USER_ROLE_ADMIN) {

                                // берем первый из офисов привязанных к направлению/КЦ
                                $officeId = $callCenter->offices[0]->id ?? 0;

                                $person = null;
                                if ($callCenterUser->person_id) {
                                    $person = Person::findOne($callCenterUser->person_id);
                                }
                                if (!$person instanceof Person && !empty($data['user_email'])) {
                                    $person = Person::find()
                                        ->where(['corporate_email' => $data['user_email']])
                                        ->byOfficeId($officeId)
                                        ->one();
                                }
                                if (!$person instanceof Person && !empty($data['user_name'])) {
                                    $person = Person::find()
                                        ->where(['name' => $data['user_name']])
                                        ->byOfficeId($officeId)
                                        ->one();
                                }
                                if (!$person instanceof Person) {
                                    $person = new Person();
                                }

                                $savePerson = false;
                                if (!empty($data['user_scan']) && !$person->photo) {
                                    $foreignPath = $callCenter->url . $data['user_scan'];
                                    $ext = pathinfo($data['user_scan'], PATHINFO_EXTENSION);
                                    $fileName = $person->getUniqueName($data['user_scan']) . '.' . $ext;
                                    $savePath = person::getPathFile($fileName);
                                    $content = file_get_contents($foreignPath);
                                    if ($content) {
                                        if (file_put_contents($savePath, $content)) {
                                            $person->photo = $fileName;
                                            $savePerson = true;
                                            $savedPhotos++;
                                        } else {
                                            $logAnswerAnswer['errors'][] = 'File save error. savePath = ' . $savePath . ' foreignPath = ' . $foreignPath;
                                        }
                                    } else {
                                        $logAnswerAnswer['errors'][] = 'File get contents error. foreignPath = ' . $foreignPath;
                                    }
                                }
                                if (!empty($data['user_name'])) {
                                    $person->name = $data['user_name'];
                                    $savePerson = true;
                                }
                                if (!empty($data['user_email'])) {
                                    $person->corporate_email = $data['user_email'];
                                    $savePerson = true;
                                }

                                if ($savePerson) {
                                    if (!$person->office_id && $officeId) {
                                        $person->office_id = $officeId;
                                    }
                                    if ($data['user_role'] == CallCenterUser::USER_ROLE_NOTACTIVE) {
                                        $person->active = Person::NOT_ACTIVE;
                                    } else {
                                        $person->active = Person::ACTIVE;
                                    }

                                    if (!is_null($data['user_role'])) {
                                        if (isset($rolesMapping[$data['user_role']])) {
                                            $person->designation_id = $rolesMapping[$data['user_role']];
                                        }
                                    }

                                    if (!$person->save()) {
                                        $logAnswerAnswer['errors'][] = $person->getFirstErrorAsString() . ' ' . print_r($data, true);
                                    } else {
                                        $callCenterUser->person_id = $person->id;
                                        $savedPersons++;
                                        if (!$callCenterUser->save(false, ['person_id'])) {
                                            $logAnswerAnswer['errors'][] = $callCenterUser->getFirstErrorAsString();
                                        }
                                    }
                                }
                                $savedUsers++;
                            }
                        } else {
                            $logAnswerAnswer['errors'][] = $callCenterUser->getFirstErrorAsString();
                        }
                    } else {
                        $logAnswerAnswer['errors'][] = 'Нет логина и ID пользователя';
                    }
                }
            } else {
                $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
            }

            $logAnswer->data = json_encode([
                'savedUsers' => $savedUsers,
                'savedPersons' => $savedPersons,
                'savedPhotos' => $savedPhotos,
                'errors' => sizeof($logAnswerAnswer['errors']),
            ], JSON_UNESCAPED_UNICODE);
            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
            $logAnswer->save();

            usleep(rand(500, 1000));
        }
    }

    /**
     * Получение списка пользователей из колл-центра
     * @param CrontabTaskLog $log
     * @param integer $cc_id
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $login
     * @throws \yii\db\Exception
     */
    public static function getOpersActivityDataFromCallCenter($log, $cc_id = null, $dateFrom = null, $dateTo, $login)
    {
        $callCenters = CallCenter::find()
            ->joinWith(['country'])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->canGetOperatorWorkTimeByApi()
            ->active()
            ->all();

        foreach ($callCenters as $callCenter) {

            if (is_numeric($cc_id) && $cc_id>0) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            $_dateFrom = $dateFrom;
            if (!$dateFrom) {
                $dateFrom = CallCenterWorkTime::getTimeLastDate($callCenter->id) ?: date('Y-01-01');
            }

            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->url = self::prepareUrlToGetOpersActivityData($callCenter, $dateFrom, $dateTo, $login);

            $dateFrom = $_dateFrom;

            $logAnswerAnswer = [
                'answer' => '',
                'errors' => [],
            ];

            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logAnswer->url);
            $answer = $curl->query();
            $logAnswerAnswer['answer'] = $answer;

            $answer = json_decode($answer, true);

            if (isset($answer['error']['msg'])) {
                $logAnswerAnswer['errors'][] = $answer['error']['msg'];
            }

            $saved_times = 0;
            $saved_users = 0;
            if (isset($answer['success']) && $answer['success'] == true) {

                if (!empty($answer['response']) && is_array($answer['response'])) {
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                    $logAnswer->save();

                    // идентификаторы пользователей в КЦ
                    $foreignIds = [];
                    foreach ($answer['response'] as $data) {
                        $foreignIds[$data['id']] = $data['id'];
                    }

                    // выбираем наши идентификаторы пачками по 500
                    $userIds = [];
                    foreach (array_chunk($foreignIds, 500) as $chunk) {
                        $userIds += ArrayHelper::map(CallCenterUser::find()
                            ->byCallCenterId($callCenter->id)
                            ->andWhere(['user_id' => $chunk])
                            ->asArray()
                            ->all(), 'user_id', 'id');
                    }

                    foreach ($answer['response'] as $data) {
                        if (isset($data['id']) && $data['id']) {

                            if (!isset($userIds[$data['id']])) {
                                // нет такого в нашей базе, добавим
                                $callCenterUser = new CallCenterUser();
                                $callCenterUser->callcenter_id = $callCenter->id;
                                $callCenterUser->user_id = (string)$data['id'];
                                $callCenterUser->active = CallCenterUser::ACTIVE;
                                $callCenterUser->user_login = $data['login'];

                                if (isset($data['user_role']) && $data['user_role'] == 'operator') {
                                    $callCenterUser->user_role = CallCenterUser::USER_ROLE_OPER;
                                }
                                if ($callCenterUser->save()) {
                                    $saved_users++;
                                    $userIds[$data['id']] = $callCenterUser->id;
                                } else {
                                    if ($logAnswerAnswer['errors']) {
                                        $logAnswerAnswer['errors'] = array_merge($logAnswerAnswer['errors'], $callCenterUser->getErrorsAsArray());
                                    } else {
                                        $logAnswerAnswer['errors'] = $callCenterUser->getErrorsAsArray();
                                    }

                                    // не будем сохранять время при ошибке
                                    continue;
                                }
                            }

                            if (isset($data['date']) && $data['date']) {
                                $work = CallCenterWorkTime::findOne([
                                    'call_center_user_id' => $userIds[$data['id']],
                                    'date' => date('Y-m-d', strtotime($data['date']))
                                ]);
                                if (!$work) {
                                    $work = new CallCenterWorkTime();
                                }
                                $work->call_center_user_id = $userIds[$data['id']];
                                $work->date = date('Y-m-d', strtotime($data['date']));
                                $work->time = (int)$data['work_time'];
                                $work->first_action = isset($data['first_action_utc']) ? strtotime($data['first_action_utc']) : null;
                                $work->first_action = $work->first_action ?: null;
                                $work->last_action = isset($data['last_action_utc']) ? strtotime($data['last_action_utc']) : null;
                                $work->last_action = $work->last_action ?: null;

                                if ($work->time > 16 * 60 * 60) {
                                    // если больше 16 часов в день работает, то это явно косяк и пишем 8 часов
                                    $work->time = 8 * 60 * 60;
                                }

                                if ($work->save()) {
                                    $saved_times++;
                                } else {
                                    if ($logAnswerAnswer['errors']) {
                                        $logAnswerAnswer['errors'] = array_merge($logAnswerAnswer['errors'], $work->getErrorsAsArray());
                                    } else {
                                        $logAnswerAnswer['errors'] = $work->getErrorsAsArray();
                                    }
                                }
                            }
                        } else {
                            $logAnswerAnswer['errors'][] = 'Неожиданный формат строки: ' . print_r($data, true);
                        }
                    }
                } else {
                    $logAnswerAnswer['errors'][] = 'Колл-центр не передал данные.';
                }

            } else {
                $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
            }

            if (is_array($logAnswerAnswer['errors']) && !empty($logAnswerAnswer['errors'])) {
                $error = reset($logAnswerAnswer['errors']);
                $errorCode = key($logAnswerAnswer['errors']);
                if (is_string($errorCode)) {
                    $error = '[' . $errorCode . '] ' . $error;
                }
                Yii::$app->notification->send(
                    Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_WORK_TIME_ERROR,
                    ['error' => $error],
                    $callCenter->country_id
                );
            }

            $logAnswer->data = json_encode([
                'saved_users' => $saved_users,
                'saved_times' => $saved_times,
                'callCenterId' => $callCenter->id,
                'error' => is_array($logAnswerAnswer['errors']) ? implode(' | ', $logAnswerAnswer['errors']) : $logAnswerAnswer['errors'],
            ], JSON_UNESCAPED_UNICODE);
            $logAnswer->save();

            usleep(rand(500, 1000));
        }
    }

    /**
     * Получение список с кол-вом звонков пользователей колл-центра
     * @param CrontabTaskLog $log
     * @param string $dateFrom
     * @param string $dateTo
     * @param null $cc_id
     */
    public static function getOpersNumberOfCallsFromCallCenter($log, $cc_id = null, $dateFrom = null, $dateTo)
    {
        $callCenters = CallCenter::find()
            ->joinWith(['country'])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->canGetOperatorNumberOfCallByApi()
            ->active()
            ->all();

        foreach ($callCenters as $callCenter) {

            if (is_numeric($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            if (!$dateFrom) {
                $dateFrom = CallCenterWorkTime::getNumberOfCallsLastDate($callCenter->id) ?: date('Y-01-01');
            }

            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->url = self::prepareUrlToGetOpersNumberOfCalls($callCenter, $dateFrom, $dateTo);

            $logAnswerAnswer = [
                'answer' => '',
                'errors' => [],
            ];

            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $logAnswer->url);
            $answer = $curl->query();
            $logAnswerAnswer['answer'] = $answer;

            $answer = json_decode($answer, true);
            if (isset($answer['error']['msg'])) {
                $logAnswerAnswer['errors'][] = $answer['error']['msg'];
            }

            $saved_records = 0;
            $saved_users = 0;
            if (isset($answer['success']) && $answer['success'] == true) {

                if (!empty($answer['response']) && is_array($answer['response'])) {
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                    $logAnswer->save();

                    // идентификаторы пользователей в КЦ
                    $foreignIds = [];
                    foreach ($answer['response'] as $data) {
                        $foreignIds[$data['user_id']] = $data['user_id'];
                    }

                    // выбираем наши идентификаторы пачками по 500
                    $userIds = [];
                    foreach (array_chunk($foreignIds, 500) as $chunk) {
                        $userIds += ArrayHelper::map(CallCenterUser::find()
                            ->byCallCenterId($callCenter->id)
                            ->andWhere(['user_id' => $chunk])
                            ->asArray()
                            ->all(), 'user_id', 'id');
                    }

                    foreach ($answer['response'] as $data) {
                        if (isset($data['user_id']) && isset($data['amount_calls']) && isset($data['date'])) {

                            if (!isset($userIds[$data['user_id']])) {
                                $callCenterUser = new CallCenterUser();
                                $callCenterUser->callcenter_id = $callCenter->id;
                                $callCenterUser->user_id = (string)$data['user_id'];
                                $callCenterUser->active = CallCenterUser::ACTIVE;
                                $callCenterUser->user_login = $data['user_login'] ?? $data['login'] ?? '';
                                if ($callCenterUser->save()) {
                                    $saved_users++;
                                    $userIds[$data['user_id']] = $callCenterUser->id;
                                } else {
                                    if ($logAnswerAnswer['errors']) {
                                        $logAnswerAnswer['errors'] = array_merge($logAnswerAnswer['errors'], $callCenterUser->getErrorsAsArray());
                                    } else {
                                        $logAnswerAnswer['errors'] = $callCenterUser->getErrorsAsArray();
                                    }

                                    // не будем сохранять время при ошибке
                                    continue;
                                }
                            }

                            $work = CallCenterWorkTime::findOne([
                                'call_center_user_id' => $userIds[$data['user_id']],
                                'date' => date('Y-m-d', strtotime($data['date']))
                            ]);
                            if (!$work) {
                                $work = new CallCenterWorkTime();
                            }
                            $work->call_center_user_id = $userIds[$data['user_id']];
                            $work->date = $data['date'];
                            $work->number_of_calls = $data['amount_calls'];

                            if ($work->save()) {
                                $saved_records++;
                            } else {
                                if ($logAnswerAnswer['errors']) {
                                    $logAnswerAnswer['errors'] = array_merge($logAnswerAnswer['errors'], $work->getErrorsAsArray());
                                } else {
                                    $logAnswerAnswer['errors'] = $work->getErrorsAsArray();
                                }
                            }
                        } else {
                            $logAnswerAnswer['errors'][] = 'Неожиданный формат строки: ' . print_r($data, true);
                        }
                    }

                } else {
                    $logAnswerAnswer['errors'][] = 'Колл-центр не передал данные.';
                }

            } else {
                $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
            }

            if (is_array($logAnswerAnswer['errors']) && !empty($logAnswerAnswer['errors'])) {
                $error = reset($logAnswerAnswer['errors']);
                $errorCode = key($logAnswerAnswer['errors']);
                if (is_string($errorCode)) {
                    $error = '[' . $errorCode . '] ' . $error;
                }
                Yii::$app->notification->send(
                    Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_NUMBER_OF_CALL_ERROR,
                    ['error' => $error],
                    $callCenter->country_id
                );
            }

            $logAnswer->data = json_encode([
                'saved_users' => $saved_users,
                'saved_records' => $saved_records,
                'error' => is_array($logAnswerAnswer['errors']) ? implode(' | ', $logAnswerAnswer['errors']) : $logAnswerAnswer['errors'],
            ], JSON_UNESCAPED_UNICODE);
            $logAnswer->save();

            usleep(rand(500, 1000));
        }
    }

    /**
     * @param $log
     * @param int $limit
     */
    public static function tryToSaveSentLeads($log, $limit = 1000)
    {
        $query = OrderSendError::find()->where(['type' => OrderSendError::TYPE_CALL_CENTER_SEND])
            ->limit($limit)
            ->orderBy(['cron_launched_at' => SORT_ASC])
            ->with(['order.callCenterRequest.order']);

        foreach ($query->batch(500) as $records) {
            /**
             * @var OrderSendError[] $records
             */
            foreach ($records as $record) {
                $request = $record->order->callCenterRequest;
                $response = json_decode($record->response, true);
                $requestLog = $log("processing single request...", ["order_id" => $request->order->id]);
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    self::saveResponseAfterSent($request, $response);
                    $record->delete();
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    $record->error = $e->getMessage();
                    $record->cron_launched_at = time();
                    $requestLog("Error: " . $e->getMessage());
                    $record->save();
                }
            }
        }
    }

    /**
     * @param array $urls
     * @return array
     */
    public static function getStatusesByUrls($urls)
    {
        self::$deliveries = self::getDeliveriesToGetStatus();
        $errors = [];
        $callCenters = CallCenter::find()
            ->where(['id' => ArrayHelper::getColumn($urls, 'call_center_id')])
            ->with(['country'])
            ->indexBy('id')
            ->all();
        foreach ($urls as $url) {
            try {
                $response = self::getStatusByUrl($url['url'], $callCenters[$url['call_center_id']]);
                if ($response) {
                    $errors[] = [
                        'errors' => implode(' | ', array_unique($response)),
                        'url' => $url['url'],
                        'call_center_id' => $url['call_center_id']
                    ];
                }
            } catch (\Throwable $e) {
                $errors[] = [
                    'exception' => $e->getMessage(),
                    'url' => $url['url'],
                    'call_center_id' => $url['call_center_id']
                ];
            }
        }
        return $errors;
    }

    /**
     * @param string $url
     * @param CallCenter $callCenter
     * @return array
     */
    public static function getStatusByUrl($url, $callCenter)
    {
        $updatedCount = 0;
        $curl = CurlFactory::build(CurlFactory::METHOD_GET, $url);
        $response = $curl->query();

        $answer = json_decode($response, true);
        $errors = [];

        if (isset($answer['error']['msg'])) {
            $errors[] = $answer['error']['msg'];
        }

        if (isset($answer['success']) && $answer['success'] == true) {
            $requests = CallCenterRequest::find()->where([
                'order_id' => array_keys($answer['response']),
                'status' => CallCenterRequest::STATUS_IN_PROGRESS
            ])
                ->with(['order.country', 'order.orderProducts', 'order.country.deliveries', 'order.deliveryRequest'])
                ->indexBy('order_id')
                ->all();

            foreach ($answer['response'] as $orderId => $data) {
                if (isset($data['address_components']) && is_string($data['address_components'])) {
                    $data['address_components'] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;",
                        $data['address_components']), ENT_NOQUOTES, 'UTF-8');
                    $data['address_components'] = json_decode($data['address_components'], true);
                }

                if (array_key_exists($orderId, $requests)) {
                    $request = $requests[$orderId];

                    if ($request->order_id == $orderId) {
                        if (self::compareAndSaveRequest($request, $data)) {
                            $updatedCount++;
                        }
                    }
                }
            }
        } else {
            $errors[] = 'Колл-центр ответил с ошибкой.';
        }

        if (!empty($errors)) {

            $parts = parse_url($url);
            parse_str($parts['query'], $query);
            $orderIds = $query['order_ids'];
            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_GET_STATUSES_ERROR,
                [
                    'order_id' => $orderIds,
                    'country' => $callCenter->country->name,
                    'error' => implode(' | ', array_unique($errors)),
                ],
                $callCenter->country_id
            );

            if (extension_loaded('newrelic')) {
                newrelic_notice_error("non-success answer from call center ({$callCenter->country->name})" . $response);

                newrelic_record_custom_event("callcenter_status_response_error", [
                    'order_ids' => $orderIds,
                    'country_name' => $callCenter->country->name,
                    'errors' => implode(' | ', array_unique($errors)),
                    'log_answer_url' => $url,
                ]);
            }


        }

        return $errors;
    }

    /**
     * Получение кол-ва звонков по заказам из Колл Центра
     * @param CrontabTaskLog $log
     * @param integer $daysAgo - кол-во дней назад, за которые нужно запросить данные
     */
    public static function getUserOrderFromCallCenter($log, $daysAgo = null)
    {
        $countForInsert = 300;
        $days = null;
        $days[] = 0;
        $dateTo = time();
        if (!$daysAgo) { // за последние сутки тогда
            $dateFrom = strtotime('-1 days');
        } else {
            $dateFrom = strtotime('-' . $daysAgo . ' days');
        }

        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $log->id;
        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $logAnswer->url = self::prepareUrlToGetUserOrder();

        $logAnswerAnswer = [
            'answer' => '',
            'errors' => [],
        ];

        $curl = CurlFactory::build(CurlFactory::METHOD_POST, $logAnswer->url, ['Authorization' => 'Basic ' . base64_encode(array_search(HttpBearerAuth::ACCESS_TYPE_CALLCENTER, HttpBearerAuth::$accessTokens))]);
        $answer = $curl->query(['call_from' => $dateFrom, 'call_till' => $dateTo]);
        $logAnswerAnswer['answer'] = $answer;

        $answer = json_decode($answer, true);

        if (isset($answer['status']) && $answer['status'] == self::STATUS_FAIL) {
            $logAnswerAnswer['errors'] = $answer['data']['messages'];
        }

        $countOrders = 0;
        if (isset($answer['status']) && $answer['status'] == self::STATUS_SUCCESS) {

            if (!empty($answer['data']) && is_array($answer['data'])) {
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $logAnswer->save();

                $now = time();
                $arrayForInsert = null;
                $arrayOrders = null;
                $i = 0;
                foreach ($answer['data'] as $data) {
                    if (is_array($data)) {
                        foreach ($data as $order => $operators) {
                            $countOrders++;
                            if (is_array($operators)) {
                                foreach ($operators as $operator => $calls) {
                                    $arrayForInsert[] = [
                                        $operator,
                                        $order,
                                        $calls,
                                        $now,
                                        $now
                                    ];
                                    $arrayOrders[] = $order;
                                    $i++;
                                }
                            }
                            if ($i >= $countForInsert) {
                                CallCenterUserOrder::deleteAll(['order_id' => $arrayOrders]);
                                Yii::$app->db->createCommand()->batchInsert(
                                    CallCenterUserOrder::tableName(),
                                    [
                                        'user_id',
                                        'order_id',
                                        'number_of_calls',
                                        'created_at',
                                        'updated_at'
                                    ],
                                    $arrayForInsert
                                )->execute();
                                $arrayForInsert = null;
                                $arrayOrders = null;
                                $i = 0;
                            }
                        }
                    }
                }

                if (is_array($arrayForInsert)) {
                    CallCenterUserOrder::deleteAll(['order_id' => $arrayOrders]);
                    Yii::$app->db->createCommand()->batchInsert(
                        CallCenterUserOrder::tableName(),
                        [
                            'user_id',
                            'order_id',
                            'number_of_calls',
                            'created_at',
                            'updated_at'
                        ],
                        $arrayForInsert
                    )->execute();
                }
            } else {
                $logAnswerAnswer['errors'][] = 'Колл-центр не передал данные.';
            }

        } else {
            $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
        }

        $logAnswer->data = json_encode([
            'got_orders' => $countOrders,
            'error' => is_array($logAnswerAnswer['errors']) ? implode(' | ', $logAnswerAnswer['errors']) : $logAnswerAnswer['errors'],
        ], JSON_UNESCAPED_UNICODE);
        $logAnswer->save();

        usleep(rand(500, 1000));
    }

    /**
     * Получение кол-ва звонков по заказам из Колл Центра
     * @param CrontabTaskLog $log
     * @param integer $daysAgo - кол-во дней назад, за которые нужно запросить данные
     * @param integer $cc_id
     */
    public static function getProductCallFromCallCenter($log, $daysAgo = null, $cc_id = null)
    {
        $days = null;
        $days[] = 0;
        if (!$daysAgo) { // за последние сутки тогда
            $days[] = 1;
        } else {
            for ($j = 1; $j <= $daysAgo; $j++) {
                $days[] = $j;
            }
        }

        $callCenters = CallCenter::find()
            ->joinWith(['country'])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->amazonQuery()
            ->active();

        if (is_numeric($cc_id)) {
            $callCenters = $callCenters->byId($cc_id);
        }

        $callCenters = $callCenters->all();

        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $log->id;
        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $logAnswer->url = self::prepareUrlToGetProductCall();

        $logAnswerAnswer = [
            'answer' => '',
            'errors' => [],
        ];

        if (is_array($callCenters)) {
            $countCountries = 0;
            foreach ($callCenters as $callCenter) {
                foreach ($days as $day) {
                    $date = strtotime('-' . $day . ' days');
                    if ($day === 0) {
                        $date = time();
                    }
                    $curl = CurlFactory::build(CurlFactory::METHOD_POST, $logAnswer->url, ['Authorization' => 'Basic ' . base64_encode(array_search(HttpBearerAuth::ACCESS_TYPE_CALLCENTER, HttpBearerAuth::$accessTokens))]);
                    $answer = $curl->query([
                        'call_date' => $date,
                        'country' => strtoupper($callCenter->country->char_code)
                    ]);
                    $logAnswerAnswer['answer'] = $answer;

                    $answer = json_decode($answer, true);

                    if (isset($answer['status']) && $answer['status'] == self::STATUS_FAIL) {
                        if (isset($answer['data']['messages'])) {
                            $logAnswerAnswer['errors'] = $answer['data']['messages'];
                        }
                    }

                    if (isset($answer['status']) && $answer['status'] == self::STATUS_SUCCESS) {

                        if (!empty($answer['data']) && is_array($answer['data'])) {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                            $logAnswer->save();

                            CallCenterProductCall::deleteAll([
                                'date' => date('Y-m-d', $date),
                                'country_id' => $callCenter->country_id
                            ]);

                            $now = time();
                            $arrayForInsert = null;
                            foreach ($answer['data'] as $data) {
                                if (is_array($data)) {
                                    foreach ($data as $country => $products) {
                                        $countCountries++;
                                        if (is_array($products)) {
                                            foreach ($products as $product => $calls) {
                                                $arrayForInsert[] = [
                                                    $product,
                                                    $callCenter->country_id,
                                                    $calls,
                                                    date('Y-m-d', $date),
                                                    $now,
                                                    $now
                                                ];
                                            }
                                        }
                                    }
                                }
                            }

                            if (is_array($arrayForInsert)) {
                                Yii::$app->db->createCommand()->batchInsert(
                                    CallCenterProductCall::tableName(),
                                    [
                                        'product_id',
                                        'country_id',
                                        'number_of_calls',
                                        'date',
                                        'created_at',
                                        'updated_at'
                                    ],
                                    $arrayForInsert
                                )->execute();
                            }
                        } else {
                            $logAnswerAnswer['errors'][] = 'Колл-центр не передал данные.';
                        }

                    } else {
                        $logAnswerAnswer['errors'][] = 'Колл-центр ответил с ошибкой.';
                    }
                }
            }

            $logAnswer->data = json_encode([
                'got_countries' => $countCountries,
                'error' => is_array($logAnswerAnswer['errors']) ? implode(' | ', $logAnswerAnswer['errors']) : $logAnswerAnswer['errors'],
            ], JSON_UNESCAPED_UNICODE);

        } else {
            $logAnswer->data = 'Нет активных Колл-центров.';
        }

        $logAnswer->save();

        usleep(rand(500, 1000));
    }
}
