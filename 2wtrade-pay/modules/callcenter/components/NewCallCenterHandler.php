<?php

namespace app\modules\callcenter\components;

use app\components\amazon\sqs\SqsClient;
use app\components\base\Component;
use app\models\Country;
use app\models\Notification;
use app\models\Product;
use app\models\Source;
use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\callcenter\exceptions\CallCenterRequestAlreadyDone;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterCheckRequestActionLog;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestCallData;
use app\modules\callcenter\models\CallCenterRequestExtra;
use app\modules\callcenter\models\CallCenterRequestSendResponse;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\logger\components\log\Logger;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use Aws\Exception\AwsException;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\validators\StringValidator;

/**
 * Class NewCallCenterHandler
 * @package app\modules\callcenter\components
 */
class NewCallCenterHandler extends Component
{
    // Количество секунд, через которое мы должны получить ответ от КЦ о получении заказа
    // Если за это время КЦ не передал информацию, то отсылаем заявку в очередь повторно
    const TIME_LIMIT_TO_RECEIVE_RESPONSE_FROM_CC_AFTER_SEND = 600;

    const QUEUE_NOT_ORDER = 'not_order';

    /** @var array */
    public static $storagesPart = [];

    /**
     * @var Delivery[]
     */
    protected static $deliveries = [];

    /**
     * @var Delivery[]
     */
    protected static $countryDelivery = [];

    /**
     * @var array
     */
    protected static $countryIds = [];

    /**
     * @var CallCenter[]
     */
    protected static $callCenters = [];

    /**
     * Мапинг с новым коллом
     * @var array
     */
    protected static $mapCallCenterStatuses = [
        2 => OrderStatus::STATUS_CC_RECALL, // Перезвон
        3 => OrderStatus::STATUS_CC_FAIL_CALL, // Недозвон
        4 => OrderStatus::STATUS_CC_APPROVED, // Одобрен
        5 => OrderStatus::STATUS_CC_REJECTED, // Отклонен
        6 => OrderStatus::STATUS_CC_TRASH, // Треш
        7 => OrderStatus::STATUS_CC_DOUBLE, // Дубль
        8 => OrderStatus::STATUS_CC_TRASH, // system trash
        1 => OrderStatus::STATUS_CC_POST_CALL // sent to cc
    ];

    const CC_ERROR_TYPE_UNKNOWN_PARTNER = 1;
    const CC_ERROR_TYPE_UNKNOWN_QUEUE = 2;
    const CC_ERROR_TYPE_ORDER_NOT_FOUND = 3;

    /**
     * @param $callCenterStatus
     * @return mixed bool|integer
     */
    protected static function getMappedStatuses($callCenterStatus)
    {
        return self::$mapCallCenterStatuses[$callCenterStatus] ?? false;
    }

    /**
     * @return SqsClient
     */
    public static function getClientSQS()
    {
        // @TODO При переписывании на нестатическое использование заменить на вызов $this->sqsClient
        return Yii::$app->sqsClient;
    }

    /**
     * Очистка статичных переменных
     */
    public static function clearStaticVars()
    {
        self::$deliveries = [];
        self::$countryDelivery = [];
        self::$callCenters = [];
    }


    /**
     * Метод для отправки заявки в очередь для Амазона на новый колл-центр
     * @param CrontabTaskLog $log
     * @param integer $cc_id
     * @param string $queue
     * @param integer $limit
     */
    public static function sendCallCenterQueueRequests($log, $cc_id = null, $queue = null, $limit = 1000)
    {
        $callCenters = self::getCallCentersToSendRequests();

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");


        $cronLog = $logger->getLogger([
            'route' => Yii::$app->controller->route,
            'process_id' => getmypid(),
            'crontabLog' => $log->id,
        ]);

        $fp = [];

        foreach ($callCenters as $callCenter) {

            if (is_numeric($cc_id)) {
                if ($callCenter->id != $cc_id) {
                    continue;
                }
            }

            // Блокируем отправку для одного КЦ
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }

            $file_lock = "/blocks/call_center_send_requests_{$callCenter->id}_{$callCenter->country->slug}.lock";

            $fp[$callCenter->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');

            if (!flock($fp[$callCenter->id], LOCK_EX | LOCK_NB)) {
                fclose($fp[$callCenter->id]);
                continue;
            }

            self::prepareStoragesPart($callCenter->country_id);

            // С целью оптимизации дробим на два запроса и гоним через union
            $pendingQuery = CallCenterRequest::find()
                ->select(CallCenterRequest::tableName() . '.id')
                ->where([
                    CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_PENDING,
                    CallCenterRequest::tableName() . '.call_center_id' => $callCenter->id
                ]);

            $subQuery = CallCenterRequest::find()
                ->select(CallCenterRequest::tableName() . '.id')
                ->where([
                        'AND',
                        [CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS],
                        [CallCenterRequest::tableName() . '.call_center_id' => $callCenter->id],
                        [
                            '<=',
                            CallCenterRequest::tableName() . '.sent_at',
                            (time() - static::TIME_LIMIT_TO_RECEIVE_RESPONSE_FROM_CC_AFTER_SEND)
                        ],
                        [
                            'OR',
                            [CallCenterRequest::tableName() . '.foreign_id' => 0],
                            [CallCenterRequest::tableName() . '.foreign_id' => null]
                        ]
                    ])->union($pendingQuery);

            $query = CallCenterRequest::find()
                ->innerJoin(['ccr' => $subQuery], CallCenterRequest::tableName().'.id = ccr.id')
                ->with([
                    'order.country',
                    'order.country.currency',
                    'order.orderProducts',
                ])
                ->limit($limit)
                ->orderBy([CallCenterRequest::tableName() . '.cron_launched_at' => SORT_ASC]);

            if ($queue) {
                $query->joinWith('order');
                if ($queue == self::QUEUE_NOT_ORDER) {
                    $query->andWhere(['!=', Order::tableName() . '.call_center_type', Order::TYPE_CC_ORDER]);
                } else {
                    $query->andWhere([Order::tableName() . '.call_center_type' => $queue]);
                }
            }

            foreach ($query->batch(500) as $requests) {
                foreach (array_chunk($requests, 10) as $requestsBatch) {
                    /** @var CallCenterRequest[] $requestsBatch */
                    CallCenterRequest::updateAll([
                        'cron_launched_at' => time(),
                        'updated_at' => time()
                    ], ['id' => ArrayHelper::getColumn($requestsBatch, 'id')]);
                    $preparedRequests = [];
                    try {
                        $ignoredRequests = [];
                        foreach ($requestsBatch as $request) {
                            $checkProduct = true;

                            if (in_array($request->order->call_center_type, [Order::TYPE_CC_RETURN_NO_PROD])) {
                                $checkProduct = false;
                            }

                            if ($data = self::prepareLeadToSendInCallCenterQueue($request, $checkProduct)) {
                                $ccSendResponse = new CallCenterRequestSendResponse([
                                    'call_center_request_id' => $request->id
                                ]);
                                $ccSendResponse->request = json_encode($data, JSON_UNESCAPED_UNICODE);
                                $request->setLastSendResponse($ccSendResponse);

                                $preparedRequests[] = [
                                    'Id' => $request->id,
                                    'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE)
                                ];
                            } else {
                                // Если не удалось подготовить отправку лида в КЦ, то пропускаем дальнейшие действия для него
                                $ignoredRequests[] = $request->id;
                            }
                        }

                        $result = self::sendBatchCallCenterRequestsToQueue($preparedRequests);
                        $successfulRequests = [];
                        $failedRequests = [];
                        if ($result['answer']) {
                            $failedRequests = ArrayHelper::index($result['answer']->get('Failed'), 'Id');
                            $successfulRequests = ArrayHelper::index($result['answer']->get('Successful'), 'Id');
                        }
                        foreach ($requestsBatch as $request) {
                            if (in_array($request->id, $ignoredRequests)) {
                                continue;
                            }
                            $transaction = CallCenterRequest::getDb()->beginTransaction();
                            try {
                                if ($result['success'] == false || isset($failedRequests[$request->id]) || !isset($successfulRequests[$request->id])) {
                                    $request->status = CallCenterRequest::STATUS_ERROR;
                                    $request->api_error = $failedRequests[$request->id]['Message'] ?? $result['error'] ?? 'Order was sent to sqs queue, but not found in successful responses.';
                                } else {
                                    $request->sent_at = time();
                                    if ($request->status == CallCenterRequest::STATUS_PENDING) {
                                        $request->foreign_id = 0;
                                        $request->foreign_status = 0;
                                        $request->foreign_substatus = null;
                                        $request->status = CallCenterRequest::STATUS_IN_PROGRESS;
                                        $request->order->status_id = OrderStatus::STATUS_CC_POST_CALL;
                                        if (!$request->order->save(false, ['status_id'])) {
                                            throw new \Exception($request->order->getFirstErrorAsString());
                                        }
                                    }
                                }
                                if ($request->lastSendResponse) {
                                    $request->lastSendResponse->response = json_encode($successfulRequests[$request->id] ?? $failedRequests[$request->id] ?? $result['answer']);
                                    $request->lastSendResponse->save();
                                }
                                if (!$request->save()) {
                                    throw new \Exception($request->getFirstErrorAsString());
                                }
                                $transaction->commit();
                            } catch (\Throwable $e) {
                                $transaction->rollBack();
                                $request->status = CallCenterRequest::STATUS_ERROR;
                                $request->api_error = $e->getMessage();
                                $request->save(true, ['status', 'api_error', 'updated_at']);
                                throw $e;
                            }
                        }
                    } catch (\Throwable $e) {
                        $cronLog("Error: " . $e->getMessage(), ['order_ids' => implode(',', ArrayHelper::getColumn($requestsBatch, 'order_id'))]);
                    }
                }
            }

            fclose($fp[$callCenter->id]); // освобождаем лок сразу.
        }
        unset($fp);
    }

    /**
     * Метод отправляет заказы в очередь на Амазон
     * @param $data
     * @param CallCenterRequest $model | null
     * @return array
     */
    public static function sendCallCenterQueueRequest($data, $model = null)
    {
        $result = [
            'success' => false,
            'error' => null,
            'answer' => null,
            'request' => null,
        ];

        $client = self::getClientSQS();

        $result['request'] = [
            'QueueUrl' => $client->getSqsQueueUrl('amazonCallCenterSendQueueUrl'),
            'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
        ];

        try {
            $result['answer'] = $client->sendMessage($result['request']);
            $result['success'] = true;
        } catch (AwsException $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Метод отправляет заказы в очередь на Амазон
     * @param array $entries
     * @return array
     */
    public static function sendBatchCallCenterRequestsToQueue($entries)
    {
        $result = [
            'success' => false,
            'error' => null,
            'answer' => null,
        ];

        if (!empty($entries)) {
            $client = self::getClientSQS();

            $sendingData = [
                'QueueUrl' => $client->getSqsQueueUrl('amazonCallCenterSendQueueUrl'),
                'Entries' => $entries,
            ];

            try {
                $result['answer'] = $client->sendMessageBatch($sendingData);
                $result['success'] = true;
            } catch (AwsException $e) {
                $result['error'] = $e->getMessage();
            }
        }

        return $result;
    }


    /**
     * @param CallCenterRequest|\app\modules\callcenter\models\CallCenterCheckRequest $request
     * @return mixed|string
     */
    public static function getOrderCommentForQueue($request)
    {
        if ($request instanceof CallCenterRequest) {
            if ($request->order->call_center_type == Order::TYPE_CC_NEW_RETURN) {
                return $request->api_error;
            }
        }
        if ($request instanceof CallCenterCheckRequest) {
            if ($request->type == CallCenterCheckRequest::TYPE_CHECK_ADDRESS && $request->api_error) {
                return $request->api_error;
            }
            if ($request->type == CallCenterCheckRequest::TYPE_INFORMATION) {
                return $request->request_comment;
            }
        }
        return $request->order->comment;
    }
    /**
     * Метод отправки заказов в очередь на Амазон
     * @param CallCenterRequest|\app\modules\callcenter\models\CallCenterCheckRequest $request
     * @param bool $checkProduct //проверка наличия продукта на складе
     * @return bool | array
     */
    public static function prepareLeadToSendInCallCenterQueue($request, $checkProduct = true)
    {
        $lead = $request->order;

        $trimmedAddress = preg_replace("/(^\s+)|(\s+$)/us", "", $lead->customer_address);

        // Если реквест уже находится в статусе "in_progress", то возможно мы его отправляем повторно, о чем уведомляем КЦ
        $isSecondSent = ($request instanceof CallCenterRequest && $request->status == CallCenterRequest::STATUS_IN_PROGRESS) || ($request instanceof CallCenterCheckRequest && $request->status == CallCenterCheckRequest::STATUS_IN_PROGRESS);

        $data = [
            'country' => $lead->country->char_code,
            'phone' => $lead->customer_phone,
            'mobile' => $lead->customer_mobile,
            'address' => $trimmedAddress,
            'shipping_price' => $lead->delivery,
            'customer_email' => $lead->customer_email,
            'customer_full_name' => $lead->customer_full_name,
            'source_uri' => $lead->landing->url,
            'ip' => $lead->customer_ip,
            'id' => $lead->id,
            'request_id' => $request->id,
            'ordered_at' => $lead->lead ? $lead->lead->ts_spawn : $lead->created_at,
            'queue_type' => $lead->call_center_type,
            'partner' => $request->callCenter->token,
            'order_comment' => self::getOrderCommentForQueue($request),
            'products' => [],
            'is_second_sent' => $isSecondSent
        ];

        foreach ($lead->orderProducts as $orderProduct) {
            //Проверяем, есть ли такой вид товаров на складе
            if ($checkProduct === false ||
                (isset(self::$storagesPart[$lead->country_id]) && in_array($orderProduct->product_id, self::$storagesPart[$lead->country_id]))
            ) {
                $data['products'][] = [
                    'id' => $orderProduct->product_id,
                    'quantity' => $orderProduct->quantity,
                    'price' => $orderProduct->price,
                ];
            } else {
                $request->api_error = "Отсутствуют товары на складе.";
                $request->save(false, ['api_error']);
                return false;
            }
        }

        return $data;
    }

    /**
     * @var bool $toAmazonQueue
     * @return CallCenter[]
     */
    protected static function getCallCentersToSendRequests()
    {
        $subQuery = CallCenterRequest::find()->where([
                'AND',
                [CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS],
                ['<=', CallCenterRequest::tableName() . '.sent_at', (time() - static::TIME_LIMIT_TO_RECEIVE_RESPONSE_FROM_CC_AFTER_SEND)],
                [
                    'OR',
                    [CallCenterRequest::tableName() . '.foreign_id' => 0],
                    [CallCenterRequest::tableName() . '.foreign_id' => null]
                ]
        ])->select('call_center_id')->distinct();
        $subQuery->union(CallCenterRequest::find()->where([
            CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_PENDING
        ])->select('call_center_id')->distinct());
        $query = CallCenter::find()
            ->innerJoin(['ccr' => $subQuery], 'ccr.call_center_id = ' . CallCenter::tableName() . '.id')
            ->andWhere([
                CallCenter::tableName() . '.active' => 1,
                CallCenter::tableName() . '.is_amazon_query' => 1,
            ])
            ->with('country')
            ->indexBy('id');

        $callCenters = $query->all();

        return $callCenters;
    }

    /**
     * Подготовка складов с товарами по странам
     * @param integer $country_id
     */
    public static function prepareStoragesPart($country_id = null)
    {
        /** @var StorageProduct[] $storagePart */
        $query = StorageProduct::find()
            ->joinWith('storage')
            ->where([
                'OR',
                ['>', 'balance', 0],
                ['unlimit' => 1]
                ]);
        if (!is_null($country_id)) {
            $query->andWhere([Storage::tableName() . '.country_id' => $country_id]);
        }
        $storagePart = $query->all();
        if ($storagePart) {
            self::$storagesPart = [];
            foreach ($storagePart as $part) {
                self::$storagesPart[$part->storage->country_id][] = $part->product_id;
            }
        }
    }

    /**
     * Принимаем данные из $response для Order и сохраняем
     * @param array $response
     * @param Order $order
     * @throws \Exception|\Throwable
     */
    public static function saveResponseToOrder($response, Order &$order)
    {
        try {
            if (!($statusId = self::getMappedStatuses($response['status']))) {
                throw new \Exception("Unknown status");
            }

            $order->route = Yii::$app->controller->route;

            self::prepareAndLoadValuesToOrder($response, $order);

            if (!$order->save()) {
                throw new \Exception($order->getFirstErrorAsString());
            }

            $products = self::updateOrderProducts($response, $order);

            self::saveResponseDeliveryToOrder($response, $order, $products);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Принимаем данные из $response относящиеся к службе доставки для Order и сохраняем
     * @param array $response
     * @param Order $order
     * @param OrderProduct[] $products
     * @throws \Exception|\Throwable
     */
    public static function saveResponseDeliveryToOrder($response, Order &$order, $products)
    {
        try {
            $order->pending_delivery_id = null;
            if (!$order->save(true, ['pending_delivery_id'])) {
                throw new \Exception("Can't save order: {$order->getFirstErrorAsString()}");
            }
            if ($order->deliveryRequest && $order->deliveryRequest->delete() === false) {
                throw new \Exception(Yii::t('common', 'Не удалось удалить заявку в службе доставки.'));
            }
            unset($order->deliveryRequest);
            // Если из колл-центра пришла служба доставки
            $deliveryId = isset($response['shipping_id']) ? $response['shipping_id'] : null;
            $deliveryPrice = $response['shipping_price'] + ($response['extra_price'] ?? 0);

            $haveAnyDelivery = $deliveryId ? true : false;
            $canDelivery = false;

            // Если из колл-центра пришел признак акции
            $packageId = $response['package_id'] ?? Lead::detectPackageByProducts($response['products']);

            if (!isset($response['disable_broker']) || !$response['disable_broker']) {
                // брокер
                $brokerResult = $order->findDeliveryByBroker($products, $packageId, $deliveryPrice);
                if ($brokerResult) {
                    if ($brokerResult['deliveryId']) {
                        $deliveryId = $brokerResult['deliveryId'];
                        $haveAnyDelivery = true;
                        // Если брокер определил КС, то она точно может доставлять
                        $canDelivery = true;
                    }
                }
            } else {
                //КС была выставлена в КЦ - на основании данных брокера
                $canDelivery = true;
            }

            if (!is_null($deliveryId) && $haveAnyDelivery === true) {
                if (!isset(self::$deliveries[$deliveryId])) {
                    self::$deliveries[$deliveryId] = Delivery::findOne($deliveryId);
                }
                $delivery = self::$deliveries[$deliveryId];
            } else {
                $delivery = null;
            }

            if ($delivery) {
                if (!$canDelivery) {
                    // брокер не смог определить КС, проверяем ту, что пришла из КЦ
                    $canDelivery = $delivery->canDeliveryOrder($order, $response['products'], $packageId);
                }
                if ($canDelivery) {
                    $delivery->prepareOrderToSend($order, $deliveryPrice, self::getIsHold($response));
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /*
    * @param $response
    * @return bool
    * @throws \Exception
    */
    public static function getIsHold($response)
    {
        $isHold = false;
        // Проверка поля на подтверждение адреса
        if (isset($response['confirm_address'])) {
            if ($response['confirm_address'] == 1 && isset($response['hold_to'])) {
                if (time() < $response['hold_to']) {
                    $isHold = true;
                }
            }
        }
        return $isHold;
    }

    /**
     * @param $response
     * @param $callCenterRequest
     * @throws \Exception
     */
    public static function saveIsHold($response, CallCenterRequest &$callCenterRequest) {
        if (self::getIsHold($response)) {
            $extra = $callCenterRequest->extra;
            if (!$extra) {
                $extra = new CallCenterRequestExtra([
                    'call_center_request_id' => $callCenterRequest->id,
                ]);
            }
            $extra->hold_to = $response['hold_to'];
            if (!$extra->save()) {
                throw new \Exception("Can't save extra for callCenterRequest: {$extra->getFirstErrorAsString()}");
            }
        }
    }

    /**
     * @param array $response
     * @param Order $order
     * @param bool $changeStatus
     * @param bool $changeDelivery
     * @return mixed
     * @throws Exception
     * @throws \Exception
     * @throws CallCenterRequestAlreadyDone
     * @throws \Throwable
     */
    public static function saveResponse($response, $order, $changeStatus = true, $changeDelivery = true)
    {
        $mainTransaction = CallCenterRequest::getDb()->beginTransaction();
        try {
            // Блокируем Order и CallCenterRequest на время обновления, чтобы было все корректно
            $order = Order::findBySql(Order::find()
                    ->where(['id' => $order->id])
                    ->createCommand()->rawSql . ' FOR UPDATE')->one();
            $callCenterRequest = CallCenterRequest::findBySql(CallCenterRequest::find()
                    ->where(['order_id' => $order->id])
                    ->createCommand()->rawSql . ' FOR UPDATE')->one();

            if (!$callCenterRequest) {
                $callCenterId = CallCenter::find()
                    ->select('id')
                    ->where([
                        CallCenter::tableName() . '.country_id' => $order->country_id,
                        CallCenter::tableName() . '.is_amazon_query' => 1
                    ])->scalar();
                if (!$callCenterId) {
                    throw new \Exception("Country hasn't correct call center");
                }
                $callCenterRequest = new CallCenterRequest([
                    'order_id' => $order->id,
                    'call_center_id' => $callCenterId,
                    'sent_at' => time(),
                    'foreign_waiting' => 1,
                    'status' => CallCenterRequest::STATUS_IN_PROGRESS,
                ]);
                if (!$callCenterRequest->save()) {
                    throw new \Exception("Can't save callCenterRequest: {$callCenterRequest->getFirstErrorAsString()}");
                }
            }

            $callCenterRequest->cron_launched_at = time();
            $callCenterRequest->save(false, ['cron_launched_at']);

            if (!($statusId = self::getMappedStatuses($response['status']))) {
                throw new \Exception("Unknown status");
            }

            // Проверяем закрыта ли заявка в КЦ (если закрыта, но из КЦ пришел ответ, что адрес подтвержден)
            if ($callCenterRequest->status == CallCenterRequest::STATUS_DONE && (!isset($response['confirm_address']) || $response['confirm_address'] != 2 || $order->status_id != OrderStatus::STATUS_LOG_DEFERRED)) {
                throw new CallCenterRequestAlreadyDone('Call center request already done.');
            }

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $callCenterRequest->foreign_status = $response['status'];
                $callCenterRequest->foreign_substatus = $response['sub_status'];
                $callCenterRequest->foreign_id = $response['id'];
                $callCenterRequest->status = CallCenterRequest::getRequestStatusByOrderStatus($statusId);
                $callCenterRequest->comment = is_array($response['comment']) ? implode(', ', $response['comment']) : $response['comment'];
                $callCenterRequest->last_foreign_operator = $response['last_operator_id'];

                if (!$callCenterRequest->save()) {
                    throw new \Exception("Can't save callCenterRequest: {$callCenterRequest->getFirstErrorAsString()}");
                }

                if (($lastUpdateResponse = $callCenterRequest->addUpdateResponse(json_encode($response, JSON_UNESCAPED_UNICODE))) && !$lastUpdateResponse->save()) {
                    throw new \Exception("Can't save lastUpdateResponse: {$lastUpdateResponse->getFirstErrorAsString()}");
                }

                self::prepareAndLoadValuesToOrder($response, $order);
                $products = self::updateOrderProducts($response, $order);

                if ($changeStatus) {
                    $order->status_id = $statusId;
                }

                if (!$order->save()) {
                    throw new \Exception("Can't save order: {$order->getFirstErrorAsString()}");
                }

                if ($order->status_id == OrderStatus::STATUS_CC_APPROVED) {
                    $callCenterRequest->approved_at = $response['updated_at'];
                    if (!$callCenterRequest->save(true, ['approved_at'])) {
                        throw new \Exception("Can't save callCenterRequest: {$callCenterRequest->getFirstErrorAsString()}");
                    }

                    self::saveIsHold($response, $callCenterRequest);

                    if ($changeDelivery) {
                        self::saveResponseDeliveryToOrder($response, $order, $products);
                    }
                }

                if (isset($response['call_data'])) {
                    CallCenterRequestCallData::saveCallData($response['call_data'], $callCenterRequest->id);
                }

                $transaction->commit();
                // Кэшим даты  для быстрой выдачи статусов по лидам,
                $callCenterRequest->cacheLastAttemptDates();
            } catch (Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                try {
                    $callCenterRequest->status = CallCenterRequest::STATUS_ERROR;
                    $callCenterRequest->api_error = $e->getMessage() . $e->getTraceAsString();
                    $callCenterRequest->save(false, ['status', 'api_error']);
                } catch (\Throwable $e2) {
                    throw new Exception($e2->getMessage());
                }
                throw $e;
            }
        } finally {
            $mainTransaction->commit();
        }
        return $order;
    }

    /**
     * @param array $response
     * @return Order
     * @throws \Throwable
     */
    public static function createNewOrderFromResponse($response)
    {
        $callCenter = static::getCallCenterByCountryCharCodeAndToken($response['country_char_code'], $response['partner_token'] ?? null);
        try {
            $order = new Order([
                'foreign_id' => (string)$response['id'],
                'source_id' => $callCenter->source->id,
                'status_id' => OrderStatus::STATUS_CC_POST_CALL,
                'country_id' => $callCenter->country_id,
                'call_center_create' => 1,
                'customer_full_name' => $response['customer_full_name'],
                'customer_phone' => $response['customer_phone'],
            ]);

            static::prepareAndLoadValuesToOrder($response, $order);

            if (!$order->save()) {
                throw new \Exception("Can't create order: " . $order->getFirstErrorAsString());
            }

            return static::saveResponse($response, $order);
        } catch (\Throwable $e) {
            Yii::$app->notification->send(Notification::TRIGGER_LEAD_NOT_ADDED, ['exception' => $e->getMessage(), 'source' => $callCenter->source->name ?? 'Call center amazon queue', 'foreign_id' => $response['id'] ?? 'Not found'], $callCenter->country_id);
            throw $e;
        }
    }

    /**
     * @param array $response
     * @param Order $order
     * @throws \Exception
     */
    public static function prepareAndLoadValuesToOrder($response, Order &$order)
    {
        if (!isset($response['customer_components'])) {
            throw new \Exception("The information about customer not found");
        }
        $additionalComponents = [];
        $customerComponents = json_decode($response['customer_components'], true);

        /**
         * [
         *      call_center_field => our_field
         * ]
         */
        $mapping = [
            'customer_state' => 'customer_province'
        ];

        $unchangeableFields = [
            'customer_address_add'
        ];

        foreach ($customerComponents as $component) {
            foreach ($component as $key => $value) {
                if (!($order->hasAttribute($key) && !in_array($key, $unchangeableFields)) && isset($mapping[$key]) && isset($order[$mapping[$key]]) && !isset($component[$mapping[$key]])) {
                    $key = $mapping[$key];
                }
                if ($order->hasAttribute($key) && !in_array($key, $unchangeableFields)) {
                    $order[$key] = $value;
                    foreach ($order->getActiveValidators($key) as $validator) {
                        if ($validator instanceof StringValidator && $validator->max !== null && mb_strlen($order[$key]) > $validator->max) {
                            $order[$key] = mb_strcut($order[$key], 0, $validator->max);
                            $additionalComponents[$key] = $value;
                        }
                    }
                } else {
                    $additionalComponents[$key] = $value;
                }
            }
        }

        $order['customer_address_add'] = json_encode($additionalComponents);

        if ($order->status_id != OrderStatus::STATUS_CC_APPROVED && empty($order->customer_full_name)) {
            $order->customer_full_name = '(empty)';
        }

        if (isset($response['shipping_price'])) {
            $order->delivery = $response['shipping_price'] + ($response['extra_price'] ?? 0);
        }

        if (isset($response['init_price'])) {
            $order->price_total = $response['init_price'];
        }

        if (isset($response['delivery_from'])) {
            $order->delivery_time_from = $response['delivery_from'];
        }
        if (isset($response['delivery_to'])) {
            $order->delivery_time_to = $response['delivery_to'];
        }
    }

    /**
     * @param array $response
     * @param Order $order
     * @throws \Exception
     * @return array
     * @throws \Throwable
     */
    protected static function updateOrderProducts($response, $order)
    {
        $orderProducts = $order->getOrderProducts()->all();
        if (!empty($response['orderProducts'])) {
            list($added, $updated, $removed, $notChanged) = static::mergeOrderProducts($orderProducts, $response['orderProducts']);
            $orderProducts = $updated;
            foreach ($added as $item) {
                $orderProducts[] = new OrderProduct([
                    'order_id' => $order->id,
                    'product_id' => $item['product_id'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity']
                ]);
            }

            foreach ($orderProducts as $orderProduct) {
                /** @var OrderProduct $orderProduct */
                if (!$orderProduct->save()) {
                    throw new \Exception(strtr("Can't {action} order product: {error}", [
                        '{action}' => $orderProduct->isNewRecord ? 'add' : 'update',
                        'error' => $orderProduct->getFirstErrorAsString()
                    ]));
                }
            }

            foreach ($removed as $orderProduct) {
                /** @var OrderProduct $orderProduct */
                if ($orderProduct->delete() === false) {
                    throw new \Exception("Can't delete order product: {$orderProduct->getFirstErrorAsString()}");
                }
            }

            $orderProducts = array_merge($orderProducts, $notChanged);
        }
        return $orderProducts;
    }

    /**
     * Слияние старых продуктов заказа и новых с поиском подходящих для обновления
     * @param array $oldProducts
     * @param array $newProducts
     * @return array
     */
    protected static function mergeOrderProducts($oldProducts, $newProducts)
    {
        $sortLambda = function ($a, $b) {
            if ($a['product_id'] != $b['product_id']) {
                return $a['product_id'] < $b['product_id'] ? -1 : 1;
            }
            if (!$a['quantity'] != $b['quantity']) {
                return $a['quantity'] < $b['quantity'] ? -1 : 1;
            }
            if ($a['price'] == $b['price']) {
                return 0;
            }
            return $a['price'] < $b['price'] ? -1 : 1;
        };

        usort($oldProducts, $sortLambda);
        usort($newProducts, $sortLambda);

        $removed = [];
        $updated = [];
        $added = [];
        $notChanged = [];
        foreach ($newProducts as $product) {
            $selectedOrderProductKey = null;
            foreach (array_keys($oldProducts) as $key) {
                if ($oldProducts[$key]['product_id'] == $product['product_id']) {
                    if ($oldProducts[$key]['quantity'] < $product['quantity'] || $oldProducts[$key]['price'] < $product['price']) {
                        $selectedOrderProductKey = $key;
                        continue;
                    } elseif (is_null($selectedOrderProductKey)) {
                        $selectedOrderProductKey = $key;
                    }
                } elseif ($oldProducts[$key]['product_id'] < $product['product_id']) {
                    $removed[] = $oldProducts[$key];
                    unset($oldProducts[$key]);
                } else {
                    break;
                }
            }
            if (!is_null($selectedOrderProductKey)) {
                if ($oldProducts[$selectedOrderProductKey]['quantity'] != $product['quantity'] || $oldProducts[$selectedOrderProductKey]['price'] != $product['price']) {
                    $oldProducts[$selectedOrderProductKey]['quantity'] = $product['quantity'];
                    $oldProducts[$selectedOrderProductKey]['price'] = $product['price'];
                    $updated[] = $oldProducts[$selectedOrderProductKey];
                } else {
                    $notChanged[] = $oldProducts[$selectedOrderProductKey];
                }
                unset($oldProducts[$selectedOrderProductKey]);
            } else {
                $added[] = [
                    'product_id' => $product['product_id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity']
                ];
            }
        }

        // Оставшиеся продукты удаляем, ибо их нет в новых
        if (!empty($oldProducts)) {
            $removed = array_merge($removed, $oldProducts);
        }

        return [$added, $updated, $removed, $notChanged];
    }

    /**
     * @param integer | Delivery $delivery
     * @param Order $order
     * @return array
     */
    protected static function getNextStatuses($delivery, $order)
    {
        $nextStatuses = [];
        if (is_integer($delivery)) {
            if (!isset(self::$deliveries[$delivery])) {
                self::$deliveries[$delivery] = Delivery::findOne($delivery);
            }
            $delivery = self::$deliveries[$delivery];
        }

        if ($delivery && $delivery->workflow) {
            $nextStatuses = $delivery->workflow->getNextStatuses($order->status_id);
        }

        return $nextStatuses;
    }

    /**
     * @param $countryId
     * @return Delivery
     */
    protected static function getDeliveryByCountry($countryId)
    {
        if (!isset(self::$countryDelivery[$countryId])) {
            if (Delivery::find()->active()->byCountryId($countryId)->count() == 1) {
                self::$countryDelivery[$countryId] = Delivery::find()->byCountryId($countryId)->active()->one();
            } else {
                self::$countryDelivery[$countryId] = false;
            }
        }

        return self::$countryDelivery[$countryId];
    }

    /**
     * @param $charCode
     * @param $token
     * @return CallCenter
     * @throws \Exception
     */
    protected static function getCallCenterByCountryCharCodeAndToken($charCode, $token)
    {
        $charCode = mb_strtolower($charCode);
        $key = mb_strtolower($charCode . $token ?? '');
        if (!isset(self::$callCenters[$key])) {
            $query = CallCenter::find()
                ->joinWith('country')
                ->where([
                    CallCenter::tableName() . '.is_amazon_query' => 1,
                    Country::tableName() . '.char_code' => $charCode
                ]);
            if ($token) {
                $query->andWhere([CallCenter::tableName() . '.token' => $token]);
            }
            $callCenter = $query->one();
            if (!$callCenter) {
                throw new \Exception("Call center with charcode and token \"{$key}\" not found.");
            }
            self::$callCenters[$key] = $callCenter;
        }
        return self::$callCenters[$key];
    }

    /**
     * @param string $charCode
     * @param string $token
     * @return integer
     * @throws \Exception
     */
    protected static function getCountryIdByCharCodeAndToken($charCode, $token)
    {
        $charCode = mb_strtolower($charCode);
        $key = mb_strtolower($charCode . $token ?? '');
        if (!isset(self::$countryIds[$key])) {
            $query = CallCenter::find()
                ->select(Country::tableName() . '.id')
                ->joinWith('country')
                ->where([
                    CallCenter::tableName() . '.is_amazon_query' => 1,
                    Country::tableName() . '.char_code' => $charCode
                ]);
            if ($token) {
                $query->andWhere([CallCenter::tableName() . '.token' => $token]);
            }
            $countryId = $query->scalar();
            if (!$countryId) {
                throw new \Exception("Country with charcode and token \"{$key}\" not found.");
            }
            self::$countryIds[$key] = $countryId;
        }
        return self::$countryIds[$key];
    }

    /**
     * @param array $data
     * @return array
     */
    public static function sendOurOrderIdThroughApi($data)
    {
        $apiUrl = yii::$app->params['api2wcallSendForeignId'];
        $identity = yii::$app->params['api2wcallIdentity'];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($apiUrl)
            ->setData($data)
            ->setHeaders(['Authorization' => 'Basic ' . base64_encode($identity)])
            ->send();

        if (!$response->isOk) {
            return [
                'success' => false,
                'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
            ];
        }
        return ['success' => true];
    }

    /**
     * @param $response
     * @param Order $order
     * @throws \Throwable
     */
    public static function saveSendError($response, $order)
    {
        if ($order->callCenterRequest->status == CallCenterRequest::STATUS_IN_PROGRESS || $order->callCenterRequest->status == CallCenterRequest::STATUS_PENDING) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (isset($response['error_type']) && $response['error_type'] == static::CC_ERROR_TYPE_ORDER_NOT_FOUND) {
                    $order->callCenterRequest->status = CallCenterRequest::STATUS_PENDING;
                } else {
                    $error = 'Error not found';
                    if (isset($response['errors']) && !empty($response['errors'])) {
                        if (is_string($response['errors'])) {
                            $error = $response['errors'];
                        } elseif (is_array($response['errors'])) {
                            $errors = [];
                            foreach ($response['errors'] as $fieldErrors) {
                                $errors = array_merge($errors, $fieldErrors);
                            }
                            $error = implode(';' . PHP_EOL, $errors);
                        }
                    }
                    $order->callCenterRequest->api_error = $error;
                    $order->callCenterRequest->status = CallCenterRequest::STATUS_ERROR;
                }

                /** Сохранение ответа от КЦ в отдельную таблицу */
                $lastCcSendResponse = $order->callCenterRequest->lastSendResponse;
                if (!$lastCcSendResponse) {
                    $lastCcSendResponse = new CallCenterRequestSendResponse([
                        'call_center_request_id' => $order->callCenterRequest->id
                    ]);
                    $order->callCenterRequest->setLastSendResponse($lastCcSendResponse);
                }
                $lastCcSendResponse->response = json_encode($response, JSON_UNESCAPED_UNICODE);
                if(!$lastCcSendResponse->save()) {
                    throw new \Exception($lastCcSendResponse->getFirstErrorAsString());
                }

                if (!$order->callCenterRequest->save(false, ['api_error', 'status'])) {
                    throw new \Exception($order->callCenterRequest->getFirstErrorAsString());
                }
                $order->status_id = $order->source_form == Order::TYPE_FORM_SHORT ? OrderStatus::STATUS_SOURCE_SHORT_FORM : OrderStatus::STATUS_SOURCE_LONG_FORM;
                if (!$order->save(false, ['status_id'])) {
                    throw new \Exception($order->getFirstErrorAsString());
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param $response
     * @param \app\modules\callcenter\models\CallCenterCheckRequest $request
     * @throws \Throwable
     */
    public static function saveSendToCheckError($response, $request)
    {
        if ($request->status == CallCenterCheckRequest::STATUS_IN_PROGRESS || $request->status == CallCenterCheckRequest::STATUS_PENDING) {
            if (isset($response['error_type']) && $response['error_type'] == static::CC_ERROR_TYPE_ORDER_NOT_FOUND) {
                $request->status = CallCenterCheckRequest::STATUS_PENDING;
            } else {
                $error = 'Error not found';
                if (isset($response['errors']) && !empty($response['errors'])) {
                    if (is_string($response['errors'])) {
                        $error = $response['errors'];
                    } elseif (is_array($response['errors'])) {
                        $errors = [];
                        foreach ($response['errors'] as $fieldErrors) {
                            $errors = array_merge($errors, $fieldErrors);
                        }
                        $error = implode(';' . PHP_EOL, $errors);
                    }
                }
                $request->api_error = $error;
                $request->status = CallCenterCheckRequest::STATUS_ERROR;
            }
            if (!$request->save(false, ['api_error', 'status'])) {
                throw new \Exception($request->getFirstErrorAsString());
            }
        }
    }

    /**
     * @param CrontabTaskLog $log
     * @param integer|null $callCenterId
     * @param string|null $selectedQueue
     * @throws \yii\base\InvalidConfigException
     */
    public static function sendCheckRequests($log, $callCenterId = null, $selectedQueue = null)
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => Yii::$app->controller->route,
            'process_id' => getmypid(),
            'crontabLog' => $log->id,
        ]);

        $query = CallCenterCheckRequest::find()
            ->where([
                'OR',
                [CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_PENDING],
                [
                    'AND',
                    [CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_IN_PROGRESS],
                    ['<=', CallCenterCheckRequest::tableName() . '.sent_at', (time() - static::TIME_LIMIT_TO_RECEIVE_RESPONSE_FROM_CC_AFTER_SEND)],
                    [
                        'OR',
                        [CallCenterCheckRequest::tableName() . '.foreign_id' => 0],
                        [CallCenterCheckRequest::tableName() . '.foreign_id' => null]
                    ]
                ]
            ])
            ->joinWith(['callCenter'])
            ->with(['order.orderProducts', 'order.country.currency'])
            ->andWhere([CallCenter::tableName() . '.active' => 1, CallCenter::tableName() . '.is_amazon_query' => 1])
            ->orderBy([
                CallCenterCheckRequest::tableName() . '.cron_launched_at' => SORT_ASC,
                CallCenterCheckRequest::tableName() . '.created_at' => SORT_ASC
            ]);

        if (!empty($callCenterId)) {
            $query->andWhere([CallCenterCheckRequest::tableName() . '.call_center_id' => $callCenterId]);
        }
        if (!empty($selectedQueue)) {
            $query->andWhere([CallCenterCheckRequest::tableName() . '.type' => $selectedQueue]);
        }

        foreach ($query->batch(500) as $requests) {
            /**
             * @var \app\modules\callcenter\models\CallCenterCheckRequest[] $requests
             */
            foreach ($requests as $request) {

                $request->cron_launched_at = time();
                $request->save(false, ['cron_launched_at']);

                try {
                    if ($data = self::prepareLeadToSendInCallCenterQueue($request, false)) {

                        $data['queue_type'] = $request->type;

                        $result = self::sendCallCenterQueueRequest($data, $request);

                        $logAction = new CallCenterCheckRequestActionLog();
                        $logAction->request_id = $request->id;
                        $logAction->type = CallCenterCheckRequestActionLog::TYPE_SEND;
                        $logAction->request = $result['request'] ?? '';
                        $logAction->response = $result['answer'] ?? '';
                        $logAction->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                        $logAction->save();

                        if ($result['success'] == false) {
                            $request->status = CallCenterCheckRequest::STATUS_ERROR;
                            $request->api_error = $result['error'];
                        } else {
                            if ($request->status == CallCenterCheckRequest::STATUS_PENDING) {
                                $request->foreign_id = 0;
                                $request->foreign_status = 0;
                                //$request->foreign_information = null;
                                $request->status = CallCenterCheckRequest::STATUS_IN_PROGRESS;
                            }
                            $request->sent_at = time();
                        }

                        if (!$request->save()) {
                            /**TODO надо сохранять статус system_error
                             * Либо какой-то статус который нам скажет, что заказ был отправлен в очередь, но мы не смогли обновить саму заявку
                             */
                            if(isset($request->id) && is_numeric($request->id)) {
                                $request->status = CallCenterCheckRequest::STATUS_ERROR;
                                $request->save(false, ['status']);
                            }
                            throw new \Exception($request->getFirstErrorAsString());
                        }
                    }

                } catch (\Throwable $e) {
                    $cronLog("Error: " . $e->getMessage(), [
                        'order_id' => $request->order_id,
                        'request_id' => $request->id,
                        'queue_type' => $request->type
                    ]);
                }
            }
        }
    }

    /**
     * @param array $response
     * @param CallCenterCheckRequest $request
     * @throws \Throwable
     */
    public static function saveCheckResponse($response, $request)
    {
        $logAction = new CallCenterCheckRequestActionLog();
        $logAction->request_id = $request->id;
        $logAction->type = CallCenterCheckRequestActionLog::TYPE_UPDATE;
        $logAction->response = $response;
        $logAction->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
        $logAction->save();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (is_numeric($response['id']) && $request->foreign_id != $response['id']) {
                $request->foreign_id = $response['id'];
            }

            $request->cron_launched_at = time();
            $request->save(false, ['cron_launched_at', 'foreign_id']);

            if ($request->foreign_status != $response['status']) {
                $request->foreign_status = $response['status'];
                $attributes = ['foreign_status'];

                if (in_array($request->foreign_status, array_keys(CallCenterCheckRequest::$mapStatuses))) {
                    $request->status = CallCenterCheckRequest::$mapStatuses[$request->foreign_status];
                    $attributes[] = 'status';

                    if ($request->status == CallCenterCheckRequest::STATUS_DONE) {

                        $request->done_at = time();
                        $attributes[] = 'done_at';

                        // В зависимости от типа очереди производим какие-то действия
                        switch ($request->type) {
                            case CallCenterCheckRequest::TYPE_CHECKUNDELIVERY: {
                                $request->foreign_information = $response['sub_status'];
                                $request->comment = $response['comment'];
                                $attributes[] = 'foreign_information';
                                $attributes[] = 'comment';

                                if (static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_APPROVED) {

                                    if ($response['sub_status'] == CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_STILL_WAITING) {
                                        $request->order->makeFinancePretension(OrderFinancePretension::PRETENSION_TYPE_STILL_WAITING);
                                    }

                                    self::saveResponse($response, $request->order, false, false);
                                }

                                if ($response['sub_status'] == CallCenterCheckRequest::FOREIGN_SUB_STATUS_ALREADY_RECEIVED) {
                                    $request->order->makeFinancePretension(OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS);
                                }

                                break;
                            }
                            case CallCenterCheckRequest::TYPE_CHECKDELIVERY: {
                                $request->foreign_information = $response['sub_status'];
                                $request->comment = $response['comment'];
                                $attributes[] = 'foreign_information';
                                $attributes[] = 'comment';
                                break;
                            }

                            case CallCenterCheckRequest::TYPE_TRASH_CHECK: {
                                if (static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_APPROVED) {
                                    $orderCopy = new Order;
                                    $orderCopy->attributes = $request->order->attributes;
                                    $orderCopy->status_id = OrderStatus::STATUS_CC_APPROVED;
                                    $orderCopy->source_id = Source::find()->where(['unique_system_name' => Source::SOURCE_WTRADE_TRASH])->select(['id'])->scalar();
                                    $orderCopy->income = null;
                                    $orderCopy->call_center_type = Order::TYPE_CC_ORDER;
                                    $orderCopy->foreign_id = $request->order->id;
                                    if (!$orderCopy->save()) {
                                        throw new \Exception('Не удалось создать заказ. Причина: ' . $orderCopy->getFirstErrorAsString());
                                    } else {
                                        $request->foreign_information = 'New Order: ' . $orderCopy->id;
                                        $request->comment = 'Лид создан';
                                        $attributes[] = 'foreign_information';
                                        $attributes[] = 'comment';
                                    }
                                } else {
                                    $request->comment = isset($response['comment']) ? $response['comment'] : 'Лид не создан';
                                    $attributes[] = 'comment';
                                }
                                break;
                            }

                            case CallCenterCheckRequest::TYPE_CHECK_ADDRESS: {
                                if (static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_APPROVED) {

                                    // выставим статус в прогрессе, если есть callCenterRequest
                                    if (isset($request->order->callCenterRequest->status) && $request->order->callCenterRequest->status == CallCenterRequest::STATUS_DONE) {
                                        $request->order->callCenterRequest->status = CallCenterRequest::STATUS_IN_PROGRESS;
                                    }

                                    $orderPreviousStatus = $request->order->getPreviousStatus();
                                    if ($orderPreviousStatus == OrderStatus::STATUS_DELIVERY_REJECTED) {
                                        self::saveResponse($response, $request->order);
                                    }
                                    elseif (
                                        $orderPreviousStatus == OrderStatus::STATUS_DELIVERY_DENIAL ||
                                        $orderPreviousStatus == OrderStatus::STATUS_DELIVERY_RETURNED ||
                                        $orderPreviousStatus == OrderStatus::STATUS_DELIVERY_REFUND
                                    ) {
                                        $orderCopy = $request->order->duplicate(true);
                                        if (!$orderCopy->save()) {
                                            throw new \Exception (Yii::t('common', 'Не удалось создать заказ') . ' ' . $orderCopy->getFirstErrorAsString());
                                        } else {
                                            self::saveResponse($response, $orderCopy);
                                        }
                                    }
                                    else {
                                        $request->need_correction = CallCenterCheckRequest::NEED_CORRECTION;
                                        $attributes[] = 'need_correction';
                                        self::saveResponse($response, $request->order, false, false);
                                    }

                                } else {
                                    // в 36 статус
                                    if ($request->order->canChangeStatusTo(OrderStatus::STATUS_CLIENT_REFUSED)) {
                                        $request->order->status_id = OrderStatus::STATUS_CLIENT_REFUSED;
                                        if (!$request->order->save(false, ['status_id'])) {
                                            throw new \Exception('Не удалось обновить заказ. Причина: ' . $request->order->getFirstErrorAsString());
                                        }
                                    }
                                }
                                break;
                            }

                            case CallCenterCheckRequest::TYPE_STILL_WAITING: {
                                $errorDeliveryRequestDeleted = false;
                                if (static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_TRASH
                                    || static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_REJECTED
                                    || static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_DOUBLE
                                ) {
                                    switch ($request->order->status_id) {
                                        case OrderStatus::STATUS_CC_CHECKING:
                                        case OrderStatus::STATUS_DELIVERY_LOST:
                                        case OrderStatus::STATUS_CC_APPROVED:
                                        case OrderStatus::STATUS_LOG_ACCEPTED:
                                        case OrderStatus::STATUS_DELIVERY_REJECTED:
                                        case OrderStatus::STATUS_DELIVERY_PENDING:
                                        case OrderStatus::STATUS_LOG_DEFERRED:
                                            if ($request->order->callCenterRequest) {
                                                if ($request->order->callCenterRequest->status != CallCenterRequest::STATUS_DONE) {
                                                    $request->order->callCenterRequest->status = CallCenterRequest::STATUS_DONE;
                                                    if (!$request->order->callCenterRequest->save()) {
                                                        throw new \Exception('Не удалось сохранить callCenterRequest. Причина: ' . $request->order->callCenterRequest->getFirstErrorAsString());
                                                    }
                                                }
                                            }
                                            if ($request->order->deliveryRequest) {
                                                if ($request->order->deliveryRequest->status != DeliveryRequest::STATUS_DONE) {
                                                    $request->order->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                                                    if (!$request->order->deliveryRequest->save()) {
                                                        throw new \Exception('Не удалось сохранить deliveryRequest. Причина: ' . $request->order->deliveryRequest->getFirstErrorAsString());
                                                    }
                                                }
                                            }
                                            $request->order->status_id = OrderStatus::STATUS_CLIENT_REFUSED;
                                            break;
                                        case OrderStatus::STATUS_DELIVERY_RETURNED:
                                        case OrderStatus::STATUS_DELIVERY_REFUND:
                                        case OrderStatus::STATUS_CLIENT_REFUSED:
                                        case OrderStatus::STATUS_DELIVERY_BUYOUT:
                                        case OrderStatus::STATUS_FINANCE_MONEY_RECEIVED:
                                            break;
                                        default:
                                            $request->order->status_id = OrderStatus::STATUS_DELIVERY_DENIAL;
                                            break;
                                    }
                                } elseif (static::getMappedStatuses($response['status']) == OrderStatus::STATUS_CC_APPROVED) {
                                    switch ($request->order->status_id) {
                                        case OrderStatus::STATUS_CC_CHECKING:
                                        case OrderStatus::STATUS_CLIENT_REFUSED:
                                        case OrderStatus::STATUS_DELIVERY_REJECTED:
                                            $request->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                                            $request->order->pending_delivery_id = null;

                                            $request->order->callCenterRequest->comment = $response['comment'];
                                            if (!$request->order->callCenterRequest->save()) {
                                                throw new \Exception("Не удалось обновить оригинальную заявку в КЦ: " . $request->order->callCenterRequest->getFirstErrorAsString());
                                            }

                                            // скинуть $request->order->deliveryRequest в отдельн таблицу
                                            //todo не всегда есть order->deliveryRequest, скорее всего где-то по логике косяк и мы еще раньше удалили этот реквест
                                            if($request->order->getDeliveryRequest()->exists()){
                                                if (!$request->order->deliveryRequest->delete()) {
                                                    $errorDeliveryRequestDeleted = true;
                                                }
                                            }

                                            break;
                                        default:
                                            break;
                                    }
                                    self::saveResponseToOrder($response, $request->order);
                                    static::stillWaitingAfterActions($request);
                                }
                                if ($errorDeliveryRequestDeleted) {
                                    throw new \Exception('Не удалось удалить заявку в КС');
                                } else {
                                    if (!$request->order->save()) {
                                        throw new \Exception('Не удалось обновить заказ. Причина: ' . $request->order->getFirstErrorAsString());
                                    } else {
                                        $request->foreign_information = 'New Order Status: ' . $request->order->status_id;
                                        $request->comment = 'Заказ обновлен';
                                        $attributes[] = 'foreign_information';
                                        $attributes[] = 'comment';
                                    }
                                }
                                break;
                            }

                            case CallCenterCheckRequest::TYPE_INFORMATION: {

                                // загружаем данные в заказ, но не сохраняем его, а только для выявления изменений
                                self::prepareAndLoadValuesToOrder($response, $request->order);

                                $labels = $request->order->attributeLabels();
                                $comment = [];
                                foreach ($request->order->getDirtyAttributes() as $key => $value) {
                                    $comment[] = $labels[$key] . ' : ' . $value;
                                }

                                $newProducts = self::getProductListIfChanged($response, $request->order);
                                if ($newProducts) {
                                    $comment[] = 'Products: '. implode('; ', $newProducts);
                                }

                                $request->comment = $comment ? implode(PHP_EOL, $comment) : '';
                                $attributes[] = 'comment';
                                break;
                            }
                        }
                    }
                }
                if (!$request->save(true, $attributes)) {
                    throw new \Exception($request->getFirstErrorAsString());
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            try {
                $request->status = CallCenterCheckRequest::STATUS_ERROR;
                $request->api_error = $e->getMessage().$e->getTraceAsString();
                $request->save(true, ['status', 'api_error']);
            } catch (\Throwable $e2) {
                throw new Exception($e->getMessage());
            }
            throw $e;
        }
    }

    /**
     * Действия после обработки заказов в still_waiting
     * @param CallCenterCheckRequest $request
     * @throws \Exception
     */
    protected static function stillWaitingAfterActions(CallCenterCheckRequest &$request)
    {
        switch ($request->after_action_id) {
            case CallCenterCheckRequest::AFTER_ACTION_STILL_WAITING_SEND_CORRECTION_LIST_TO_COURIER:
                if (in_array($request->order->status_id, OrderStatus::getProcessDeliveryAndLogisticList())) {
                    $request->need_correction = CallCenterCheckRequest::NEED_CORRECTION;
                    if (!$request->save(true, ['need_correction'])) {
                        throw new \Exception("Can't save request correction: " . $request->getFirstErrorAsString());
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param array $response
     * @param Order $order
     * @return false|array
     */
    public static function getProductListIfChanged($response, $order)
    {
        if (!$response['orderProducts']) {
            return false;
        }
        $newProducts = [];
        foreach ($response['orderProducts'] as $responseProduct) {
            $newProducts[$responseProduct['product_id']][$responseProduct['price'] ? 1 : 0] = $responseProduct['quantity'];
        }

        $anyProductChanges = false;
        foreach ($order->orderProducts as $orderProduct) {
            if (isset($newProducts[$orderProduct->product_id][$orderProduct->price ? 1 : 0])) {
                if ($newProducts[$orderProduct->product_id][$orderProduct->price ? 1 : 0] != $orderProduct->quantity) {
                    // изменилось кол-во
                    $anyProductChanges = true;
                }
                unset($newProducts[$orderProduct->product_id][$orderProduct->price ? 1 : 0]);
            }
            else {
                // был продукт и не пришел в обновлении
                $anyProductChanges = true;
            }
        }
        if ($newProducts) {
            // есть новые продукты, которых не было ранее и не выполнили для них unset
            $anyProductChanges = true;
        }

        if ($anyProductChanges) {
            $newProducts = [];
            foreach ($response['orderProducts'] as $responseProduct) {
                $product = Product::findOne($responseProduct['product_id']);
                if ($product) {
                    $newProducts[] = $product->name . ' - ' . $responseProduct['quantity'] . (!$responseProduct['price'] ? ' GIFT' : '');
                }
            }
            if ($newProducts) {
                return $newProducts;
            }
        }
        return false;
    }

    /**
     * Автоматическая повторная отправка заявок на обзвон
     * @param $log
     * @param integer $common Количество секунд, прошедших с последнего обновления статуса обычной заявки, по умолчанию - 3600 секунд
     * @param integer $check Количество секунд, прошедших с последнего обновления статуса проверочной заявки, по умолчанию - 43200 секунд
     * @param integer $cc_id Идентификатор КЦ
     * @param null $dateFrom С какой даты проверять по созданию, формат Y-m-d
     * @param null $dateTo По какую дату создания заявки проверять, формат Y-m-d
     */
    public static function checkingStatus($log, $common = 3600, $check = 43200, $cc_id = null, $dateFrom = null, $dateTo = null)
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => Yii::$app->controller->route,
            'process_id' => getmypid(),
            'crontabLog' => $log->id,
        ]);

        $callCenterIds = CallCenter::find()->where([CallCenter::tableName() . '.is_amazon_query' => 1, CallCenter::tableName() . '.active' => 1])->select('id')->column();
        foreach ($callCenterIds as $callCenterId) {
            if (!empty($cc_id) && $callCenterId != $cc_id) {
                continue;
            }
            $query = CallCenterRequest::find()
                ->where([
                    CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS,
                    CallCenterRequest::tableName() . '.call_center_id' => $callCenterId
                ])
                ->andWhere(['<=', CallCenterRequest::tableName() . '.updated_at', time() - $common])
                ->andWhere(['is not', CallCenterRequest::tableName() . '.foreign_id', null])
                ->andWhere(['<>', CallCenterRequest::tableName() . '.foreign_id', 0]);

            $queryCheck = CallCenterCheckRequest::find()
                ->where([
                    CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_IN_PROGRESS,
                    CallCenterCheckRequest::tableName() . '.call_center_id' => $callCenterId,
                ])
                ->andWhere(['<=', CallCenterCheckRequest::tableName() . '.updated_at', time() - $check])
                ->andWhere(['is not', CallCenterCheckRequest::tableName() . '.foreign_id', null])
                ->andWhere(['<>', CallCenterCheckRequest::tableName() . '.foreign_id', 0]);

            if ($dateFrom) {
                $query->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', strtotime($dateFrom)]);
                $queryCheck->andWhere(['>=', CallCenterCheckRequest::tableName() . '.sent_at', strtotime($dateFrom)]);
            }

            if ($dateTo) {
                $query->andWhere(['<=', CallCenterRequest::tableName() . '.sent_at', strtotime($dateTo)]);
                $queryCheck->andWhere(['<=', CallCenterCheckRequest::tableName() . '.sent_at', strtotime($dateTo)]);
            }

            foreach ($query->batch(100) as $requests) {
                /**
                 * @var \app\modules\callcenter\models\CallCenterRequest[] $requests
                 */
                $data = [];
                foreach ($requests as $request) {

                    $lastUpdateResponse = $request->getLastUpdateResponse();

                    $data[$request->id] = [
                        'order_id' => $request->order_id,
                        'foreign_id' => $request->foreign_id,
                        'queue' => 'order',
                        'hash' => $lastUpdateResponse ? hash('sha256', $lastUpdateResponse->response) : ''
                    ];
                }

                if ($data) {

                    $client = self::getClientSQS();

                    $queryData = [
                        'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
                        'QueueUrl' => $client->getSqsQueueUrl('amazonCallCenterCheckingStatusQueueUrl'),
                    ];

                    try {
                        CallCenterRequest::updateAll(['updated_at' => time()], ['id' => array_keys($data)]);

                        $client->sendMessage($queryData);
                    } catch (\Throwable $e) {
                        $cronLog("Error: " . $e->getMessage(), ['data' => $queryData['MessageBody']]);
                    }
                }
            }

            foreach ($queryCheck->batch(100) as $requests) {
                /**
                 * @var \app\modules\callcenter\models\CallCenterCheckRequest[] $requests
                 */
                $data = [];
                foreach ($requests as $request) {

                    $lastUpdateResponse = $request->getLastUpdateResponse();

                    $data[$request->id] = [
                        'order_id' => $request->order_id,
                        'foreign_id' => $request->foreign_id,
                        'queue' => $request->type,
                        'hash' => $lastUpdateResponse ? hash('sha256', $lastUpdateResponse->response) : ''
                    ];
                }

                if ($data) {

                    $client = self::getClientSQS();

                    $queryData = [
                        'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
                        'QueueUrl' => $client->getSqsQueueUrl('amazonCallCenterCheckingStatusQueueUrl'),
                    ];

                    try {
                        CallCenterCheckRequest::updateAll(['updated_at' => time()], ['id' => array_keys($data)]);

                        $client->sendMessage($queryData);
                    } catch (\Throwable $e) {
                        $cronLog("Error: " . $e->getMessage(), ['data' => $queryData['MessageBody']]);
                    }
                }
            }
        }
    }

    /**
     * @param CrontabTaskLog $log
     */
    public static function actualizeOpersDirections($log)
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => Yii::$app->controller->route,
            'process_id' => getmypid(),
            'crontabLog' => $log->id,
        ]);


        $query = Order::find()
            ->select([
                'last_foreign_operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
            ])
            ->joinWith('callCenterRequest', false)
            ->joinWith('callCenterRequest.callCenter', false)
            ->where([Order::tableName() . '.status_id' => OrderStatus::getApproveList()])
            ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 1]);

        //текущем месяце или за последние 7 дней если дата с 1 по 7 число.
        if (date('j') < 7) {
            $border = strtotime('-7 days');
        } else {
            $border = strtotime('first day of this month 00:00:00', time());
        }
        $query->andWhere(['>=', CallCenterRequest::tableName() . '.approved_at', $border]);
        $query->groupBy(['call_center_id', 'last_foreign_operator']);

        $approves = ArrayHelper::index($query->asArray()->all(), 'last_foreign_operator', ['call_center_id']);

        // работаем с all_center_user, выберем все привязки
        $callCenterUsers = CallCenterUser::find()
            ->joinWith('callCenter', false)
            ->where([CallCenter::tableName() . '.is_amazon_query' => 1])
            ->andWhere(['is not', CallCenterUser::tableName() . '.person_id', null])
            ->all();

        $toClean = [];
        $persons = [];
        foreach ($callCenterUsers as $centerUser) {
            if (!isset($approves[$centerUser->callcenter_id][$centerUser->user_id])) {
                if ($centerUser->person->isOperator() &&
                    strpos($centerUser->person->name, 'Технический оператор') === false
                ) {
                    $toClean[] = $centerUser->id;
                }
            }
            else {
                unset($approves[$centerUser->callcenter_id][$centerUser->user_id]);
                $persons[$centerUser->user_id] = $centerUser->person_id;
            }
        }

        // тех кто не апрувил снимаем - ставим call_center_user.person_id = null
        CallCenterUser::updateAll(['person_id' => null], ['id' => $toClean]);

        // апрувы для которых у нас нет привязки
        $updated = 0;
        foreach ($approves as $callCenterId => $data) {
            if ($data) {
                foreach ($data as $row) {
                    //добавляем к физ лицу, если оно у нас есть
                    if (isset($persons[$row['last_foreign_operator']])) {
                        $updated += CallCenterUser::updateAll(['person_id' => $persons[$row['last_foreign_operator']]], [
                            'callcenter_id' => $row['call_center_id'],
                            'user_id' => $row['last_foreign_operator'],
                        ]);
                    }
                }
            }
        }

        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $log->id;
        $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $logAnswer->data = json_encode([
            'toClean' => $toClean,
            'updated' => $updated,
        ]);
        $logAnswer->save();
    }
}