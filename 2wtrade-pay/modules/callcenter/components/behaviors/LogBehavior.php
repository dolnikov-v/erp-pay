<?php

namespace app\modules\callcenter\components\behaviors;


use app\modules\callcenter\components\Sender;
use app\modules\callcenter\models\events\SendRequestErrorEvent;
use app\modules\callcenter\models\events\SendRequestEvent;
use snapsuzun\yii2logger\LoggerInterface;
use yii\base\Behavior;
use yii\di\Instance;

class LogBehavior extends Behavior
{
    /**
     * @var LoggerInterface|string|array
     */
    public $logger = 'logger';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->logger = Instance::ensure($this->logger, LoggerInterface::class);
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Sender::EVENT_SEND_ERROR => 'sendError',
            Sender::EVENT_AFTER_SEND_REQUEST => 'afterSend'
        ];
    }

    /**
     * @param SendRequestErrorEvent $event
     */
    public function sendError(SendRequestErrorEvent $event)
    {
        $this->logger->warning([
            'type' => 'callCenterRequestSendError',
            'message' => $event->message,
            'order_id' => $event->orderId,
        ]);
    }

    /**
     * @param SendRequestEvent $event
     */
    public function afterSend(SendRequestEvent $event)
    {
        $this->logger->info([
            'type' => 'callCenterRequestAfterSend',
            'order_id' => $event->request->order_id,
            'request_id' => $event->request->id,
            'requestData' => $event->requestData,
            'responseData' => $event->responseData,
            'response' => $event->response
        ]);
    }
}