<?php

namespace app\modules\callcenter\components\behaviors;


use app\models\Country;
use app\models\Notification;
use app\modules\callcenter\components\Sender;
use app\modules\callcenter\models\events\SendRequestErrorEvent;
use app\modules\order\models\Order;
use yii\base\Behavior;
use yii\di\Instance;

/**
 * Class NotificationBehavior
 * @package app\modules\callcenter\components\behaviors
 */
class NotificationBehavior extends Behavior
{
    /**
     * @var \app\components\Notification|string|array
     */
    public $notification = 'notification';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->notification = Instance::ensure($this->notification, \app\components\Notification::class);
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Sender::EVENT_SEND_ERROR => 'sendError'
        ];
    }

    /**
     * При ошибку отправки в КЦ - создаем соответствующее уведомление
     * @param SendRequestErrorEvent $event
     * @throws \yii\db\Exception
     */
    public function sendError(SendRequestErrorEvent $event)
    {
        $countryData = Order::find()->joinWith('country')->select([
            'country_id' => Country::tableName() . '.id',
            'char_code' => Country::tableName() . '.char_code'
        ])->byId($event->orderId)->createCommand()->queryOne();
        $this->notification->send(
            Notification::TRIGGER_CALLCENTER_RESPONSE_ERROR, [
            'order_id' => $event->orderId,
            'country' => $countryData['char_code'],
            'error' => $event->message,
        ],
            $countryData['country_id']
        );
    }
}