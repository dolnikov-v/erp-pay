<?php

namespace app\modules\callcenter\components;


use app\components\amazon\sqs\SqsClient;
use app\components\base\Component;
use app\components\curl\CurlFactory;
use app\helpers\StringHelper;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestSendResponse;
use app\modules\callcenter\models\events\CreateCallCenterRequestEvent;
use app\modules\callcenter\models\events\NoProductInStockEvent;
use app\modules\callcenter\models\events\SendRequestErrorEvent;
use app\modules\callcenter\models\events\SendRequestEvent;
use app\modules\catalog\controllers\AutotrashController;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderSendError;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use Aws\Exception\AwsException;
use Yii;
use yii\db\Expression;
use yii\di\Instance;

/**
 * Компонент, занимающийся созданием заявок и отправкой их в КЦ
 * Class Sender
 * @package app\modules\callcenter\components
 */
class Sender extends Component
{
    const EVENT_ERROR = 'error';
    const EVENT_NO_PRODUCTS_IN_STOCK = 'noProductsInStock';
    const EVENT_BEFORE_SEND_REQUEST = 'beforeSendRequest';
    const EVENT_AFTER_SEND_REQUEST = 'afterSendRequest';
    const EVENT_AFTER_SAVE_SEND_RESPONSE = 'afterSaveSendResponse';
    const EVENT_SEND_ERROR = 'sendError';
    const EVENT_BEFORE_CREATE_CALL_CENTER_REQUEST = 'beforeCreateCallCenterRequest';
    const EVENT_AFTER_CREATE_CALL_CENTER_REQUEST = 'afterCreateCallCenterRequest';

    /**
     * @var SqsClient|string|array|callable
     */
    public $sqsClient = 'sqsClient';

    /**
     * Название очереди в амазоне, в которую улетают новые лиды
     * @var string
     */
    public $amazonSqsLeadQueue = 'amazonCallCenterSendQueueUrl';

    /**
     * Количество секунд, через которое мы должны получить ответ от КЦ о получении заказа
     * Если за это время КЦ не передал информацию, то отсылаем заявку в очередь повторно
     * @var int
     */
    public $timeLimitToReceiveSendResponseFromCallCenter = 600;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (is_callable($this->sqsClient)) {
            $this->sqsClient = call_user_func($this->sqsClient);
        }
        $this->sqsClient = Instance::ensure($this->sqsClient, SqsClient::class);
    }

    /**
     * Отправка заявок в КЦ на обзвон
     * @param Order $order
     * @return bool
     * @throws \yii\db\Exception
     */
    public function sendCallCenterRequest(Order $order): bool
    {
        // Если заказ отправляли ранее и изменения не были сохранены, то пробуем их сохранить еще раз
        if (($sendErrorModel = OrderSendError::find()->where([
                'order_id' => $order->id,
                'type' => OrderSendError::TYPE_CALL_CENTER_SEND
            ])->one()) && $order->callCenterRequest && $this->canSendCallCenterRequest($order->callCenterRequest)) {
            $responseData = json_decode($sendErrorModel->response, true);
            if ($order->callCenterRequest->callCenter->is_amazon_query) {
                if ($this->saveResponseAfterSentCallCenterRequestToAmazonQueue($order->callCenterRequest, $responseData)) {
                    $this->trigger(static::EVENT_AFTER_SAVE_SEND_RESPONSE, new SendRequestEvent([
                        'request' => $order->callCenterRequest,
                        'responseData' => $responseData,
                        'response' => $sendErrorModel->response
                    ]));
                    return true;
                }
            } else {
                if ($this->saveResponseAfterSentCallCenterRequestByApi($order->callCenterRequest, $responseData)) {
                    $this->trigger(static::EVENT_AFTER_SAVE_SEND_RESPONSE, new SendRequestEvent([
                        'request' => $order->callCenterRequest,
                        'responseData' => $responseData,
                        'response' => $sendErrorModel->response
                    ]));
                    return true;
                }
            }
            return false;
        }

        $request = $order->getCallCenterRequest()->one();
        if (!$request && !($request = $this->createCallCenterRequestForOrder($order))) {
            return false;
        }

        $request->cron_launched_at = time();
        $request->save(false, ['cron_launched_at']);

        $checkProduct = $this->shouldCheckAvailabilityOfOrderProductWhenSend($request);
        if ($checkProduct && !$this->checkAvailabilityOfOrderProductsOnStorage($order)) {
            /*
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'message' => Yii::t('common', 'Отсутствуют товары на складе.'),
                'orderId' => $order->id
            ]));
            */
            return false;
        }

        if (!$this->canSendCallCenterRequest($request)) {
            return false;
        }

        return $request->callCenter->is_amazon_query ? $this->sendCallCenterRequestToAmazonQueue($request) : $this->sendCallCenterRequestByApi($request);
    }

    /**
     * @param CallCenterRequest $request
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function sendCallCenterRequestByApi(CallCenterRequest $request): bool
    {
        if (empty($request->callCenter->url)) {
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'message' => Yii::t('common', 'Не указан URL адрес колл-центра'),
                'orderId' => $request->order_id
            ]));
            return false;
        }

        $data = $this->prepareDataForSendToCallCenterByApi($request);

        if (!$this->beforeSendRequest($request, $data)) {
            return false;
        }

        $response = null;
        $url = $this->prepareUrlToSendByApi($request->callCenter);
        $curl = CurlFactory::build(CurlFactory::METHOD_POST, $url);
        $response = $curl->query(['data' => json_encode($data, JSON_UNESCAPED_UNICODE)]);
        try {
            $responseData = json_decode($response, true);
            if (!$this->afterSendRequest($request, $data, is_array($responseData) ? $responseData : null, $response)) {
                return false;
            }

            if ($this->saveResponseAfterSentCallCenterRequestByApi($request, $responseData)) {
                $this->trigger(static::EVENT_AFTER_SAVE_SEND_RESPONSE, new SendRequestEvent([
                    'request' => $request,
                    'requestData' => $data,
                    'responseData' => $responseData,
                    'response' => $response
                ]));
                return true;
            }
        } finally {
            $this->createSendRequestLog($request, $data, $response);
        }
        return false;
    }

    /**
     * @param CallCenterRequest $request
     * @param array|null $response
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function saveResponseAfterSentCallCenterRequestByApi(CallCenterRequest $request, ?array $response): bool
    {
        $transaction = CallCenterRequest::getDb()->beginTransaction();
        try {
            if (isset($response['success']) && $response['success'] == true) {
                $request->sent_at = time();
                $request->foreign_id = $response['response']['order_id'];
                $request->foreign_status = $response['response']['status'];
                $request->foreign_substatus = $response['response']['substatus'] ?? null;
                $request->status = CallCenterRequest::STATUS_IN_PROGRESS;
                $request->order->status_id = OrderStatus::STATUS_CC_POST_CALL;
                if (!$request->order->save(true, ['status_id'])) {
                    throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заказа: {error}', ['error' => $request->order->getFirstErrorAsString()]));
                }
            } else {
                $request->status = CallCenterRequest::STATUS_ERROR;
                $request->api_error = $response['error']['msg'] ?? Yii::t('common', 'Колл-центр ответил с ошибкой.');
                if (isset($response['error']['code'])) {
                    switch ($response['error']['code']) {
                        case 450:
                            $request->order->status_id = OrderStatus::STATUS_CC_DOUBLE;
                            if (!$request->order->save(true, ['status_id'])) {
                                throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заказа: {error}', ['error' => $request->order->getFirstErrorAsString()]));
                            }
                            break;
                        case 448:
                            $request->order->status_id = OrderStatus::STATUS_CC_TRASH;
                            if (!$request->order->save(true, ['status_id'])) {
                                throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заказа: {error}', ['error' => $request->order->getFirstErrorAsString()]));
                            }
                            break;
                    }
                }
                $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                    'orderId' => $request->order_id,
                    'message' => Yii::t('common', 'Колл-центр ответил с ошибкой.') . (isset($responseData['error']['msg']) ? ' ' . $response['error']['msg'] : '')
                ]));
            }
            if (!$request->save()) {
                throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заявки: {error}', ['error' => $request->getFirstErrorAsString()]));
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $request->order_id,
                'message' => $e->getMessage()
            ]));
            $this->saveOrderSendErrorAfterSentRequest($request, $response, $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param CallCenter $callCenter
     * @return string
     */
    protected function prepareUrlToSendByApi(CallCenter $callCenter)
    {
        $time = time();
        $token = md5($callCenter->api_key . $time . $callCenter->api_pid);
        return rtrim($callCenter->url, '/') . '/api/method/OrderNew?token=' . $token . '&time=' . $time . '&pid=' . $callCenter->api_pid;
    }

    /**
     * @param CallCenterRequest $request
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function sendCallCenterRequestToAmazonQueue(CallCenterRequest $request): bool
    {
        $data = $this->prepareDataForSendToCallCenterByAmazonQueue($request);

        if (!$this->beforeSendRequest($request, $data)) {
            return false;
        }

        $sendingData = [
            'QueueUrl' => $this->sqsClient->getSqsQueueUrl($this->amazonSqsLeadQueue),
            'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
        ];

        $responseJson = null;
        $responseData = null;
        try {
            $responseObject = $this->sqsClient->sendMessage($sendingData);
            $responseJson = json_encode($responseObject->toArray(), JSON_UNESCAPED_UNICODE);
            $responseData = $responseObject->toArray();
        } catch (AwsException $e) {
            $this->afterSendRequest($request, $data, $e->getResult()->toArray(), $e->getMessage());
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $request->order_id,
                'message' => $e->getMessage()
            ]));
            return false;
        }

        try {
            if (!$this->afterSendRequest($request, $data, $responseData, $responseJson)) {
                return false;
            }

            if ($this->saveResponseAfterSentCallCenterRequestToAmazonQueue($request, $responseData)) {
                $this->trigger(static::EVENT_AFTER_SAVE_SEND_RESPONSE, new SendRequestEvent([
                    'request' => $request,
                    'requestData' => $data,
                    'responseData' => $responseData,
                    'response' => $responseJson
                ]));
                return true;
            }
        } finally {
            $this->createSendRequestLog($request, $data, $responseJson);
        }
        return false;
    }

    /**
     * @param CallCenterRequest $request
     * @param array $requestData
     * @return bool
     */
    protected function beforeSendRequest(CallCenterRequest $request, array $requestData): bool
    {
        $event = new SendRequestEvent(['request' => &$request, 'requestData' => $requestData]);
        $this->trigger(static::EVENT_BEFORE_SEND_REQUEST, $event);
        if ($event->handled) {
            return false;
        }
        return true;
    }

    /**
     * @param CallCenterRequest $request
     * @param array $requestData
     * @param array $responseData
     * @param $response
     * @return bool
     */
    protected function afterSendRequest(CallCenterRequest $request, array $requestData, ?array $responseData, $response): bool
    {
        $event = new SendRequestEvent([
            'request' => &$request,
            'requestData' => $requestData,
            'responseData' => $responseData,
            'response' => $response
        ]);
        $this->trigger(static::EVENT_AFTER_SEND_REQUEST, $event);
        if ($event->handled) {
            return false;
        }
        return true;
    }

    /**
     * @param CallCenterRequest $request
     * @param array|null $response
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function saveResponseAfterSentCallCenterRequestToAmazonQueue(CallCenterRequest $request, ?array $response): bool
    {
        $transaction = CallCenterRequest::getDb()->beginTransaction();
        try {
            $request = CallCenterRequest::findBySql(CallCenterRequest::find()
                    ->where(['id' => $request->id])
                    ->createCommand()->rawSql . ' FOR UPDATE')->one();
            $request->sent_at = time();
            if ($request->status == CallCenterRequest::STATUS_PENDING) {
                $request->foreign_id = 0;
                $request->foreign_status = 0;
                $request->foreign_substatus = null;
                $request->status = CallCenterRequest::STATUS_IN_PROGRESS;
                $order = Order::findBySql(Order::find()
                        ->where(['id' => $request->order_id])
                        ->createCommand()->rawSql . ' FOR UPDATE')->one();
                $order->status_id = OrderStatus::STATUS_CC_POST_CALL;
                if (!$order->save(true, ['status_id'])) {
                    throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заказа: {error}', ['error' => $order->getFirstErrorAsString()]));
                }
            }
            if (!$request->save()) {
                throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заявки: {error}', ['error' => $request->getFirstErrorAsString()]));
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $request->order_id,
                'message' => $e->getMessage()
            ]));
            $this->saveOrderSendErrorAfterSentRequest($request, $response, $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Сохраняем ответ от КЦ, если произошли ошибки при сохранении изменений заявки после успешной отправки в КЦ
     * @param CallCenterRequest $request
     * @param array $response
     * @param string $errorMessage
     * @return bool
     */
    protected function saveOrderSendErrorAfterSentRequest(CallCenterRequest $request, array $response, string $errorMessage)
    {
        $sendErrorModel = OrderSendError::find()->where([
            'order_id' => $request->order_id,
            'type' => OrderSendError::TYPE_CALL_CENTER_SEND
        ])->one();
        if (!$sendErrorModel) {
            $sendErrorModel = new OrderSendError([
                'type' => OrderSendError::TYPE_CALL_CENTER_SEND,
                'order_id' => $request->order_id,
                'response' => json_encode($response, JSON_UNESCAPED_UNICODE),
            ]);
        }
        $sendErrorModel->error = $errorMessage;
        return $sendErrorModel->save();
    }

    /**
     * @param CallCenterRequest $request
     * @return bool
     */
    protected function shouldCheckAvailabilityOfOrderProductWhenSend(CallCenterRequest $request): bool
    {
        return !in_array($request->order->call_center_type, [Order::TYPE_CC_RETURN_NO_PROD]);
    }

    /**
     * Проверка реквеста на возможность отправки в КЦ
     * @param CallCenterRequest $request
     * @return bool
     */
    protected function canSendCallCenterRequest(CallCenterRequest $request): bool
    {
        return $request->status == CallCenterRequest::STATUS_PENDING
            || ($request->callCenter->is_amazon_query
                && $request->status == CallCenterRequest::STATUS_IN_PROGRESS
                && $request->sent_at <= (time() + $this->timeLimitToReceiveSendResponseFromCallCenter)
                && empty($request->foreign_id));
    }

    /**
     * Создание заявки на первичный обзвон
     * @param Order $order
     * @return CallCenterRequest|null
     */
    public function createCallCenterRequestForOrder(Order $order): ?CallCenterRequest
    {
        if (!$this->canCreateCallCenterRequestForOrder($order)) {
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $order->id,
                'message' => Yii::t('common', 'Создание заявки для указанного заказа запрещено.')
            ]));
            return null;
        }
        if ($order->getCallCenterRequest()->exists()) {
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $order->id,
                'message' => Yii::t('common', 'Заявка уже существует. Требуется повторная отправка заявки в колл-центр.')
            ]));
            return null;
        }
        try {
            // Если длинная форма и можно перейти в отправку КС, то пробуем отправлять
            if ($order->status_id == OrderStatus::STATUS_SOURCE_LONG_FORM && ($order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_PENDING) || $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED))) {
                $brokerResult = $order->findDeliveryByBroker($order->orderProducts);
                if (!$brokerResult) {
                    throw new \Exception(Yii::t('common', "Не удалось определить службу доставки при создании заявки на доставку."));
                }
                $delivery = Delivery::find()->where(['id' => $brokerResult['deliveryId']])->one();
                $delivery->prepareOrderToSend($order);
                return null;
            }

            // Если не разрешен следующий статус "Обзвон", то все, не создаем заявку
            if (!$order->canChangeStatusTo(OrderStatus::STATUS_CC_POST_CALL)) {
                return null;
            }

            // Пробуем затрешить заказ
            if ($this->autoTrashOrder($order)) {
                return null;
            }
            if (($request = $this->prepareCallCenterRequest($order))) {
                $event = new CreateCallCenterRequestEvent(['request' => $request]);
                $this->trigger(static::EVENT_BEFORE_CREATE_CALL_CENTER_REQUEST, $event);
                if ($event->handled) {
                    return null;
                }
                if (!$request->save()) {
                    throw new \Exception(Yii::t('common', 'Ошибка при сохранении заявки: {error}', ['error' => $request->getFirstErrorAsString()]));
                }
                $this->trigger(static::EVENT_AFTER_CREATE_CALL_CENTER_REQUEST, new CreateCallCenterRequestEvent(['request' => $request]));
            }
        } catch (\Throwable $e) {
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'orderId' => $order->id,
                'message' => $e->getMessage()
            ]));
            return null;
        }
        return $request;
    }

    /**
     * Проверка на возможность создания заявки для КЦ
     * @param Order $order
     * @return bool
     */
    protected function canCreateCallCenterRequestForOrder(Order $order)
    {
        return in_array($order->status_id, [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL
        ]);
    }

    /**
     * Автоматический треш заказа по правилам и дублям
     * @param Order $order
     * @return bool
     * @throws \Exception
     */
    protected function autoTrashOrder(Order $order): bool
    {
        // Если включен автотреш NPAY-724
        if (AutotrashController::getStateAutotrash($order->country_id)) {
            //проверка полей заказа по фильтрам автотреша
            if (AutotrashController::classifyOrder($order)) {
                $order->status_id = OrderStatus::STATUS_AUTOTRASH;
                if (!$order->save()) {
                    throw new \Exception(Yii::t('common', 'Не удалось изменить заказ {order} причина: {reason}', [
                        'order' => $order->id,
                        'reason' => $order->getFirstErrorAsString()
                    ]));
                }
                return true;
            }
        }

        // если галка Трешить дубли включена в стране и еще не сработал автотреш
        if ($order->country->trash_doubles_enabled) {
            // Ищем дубли по номеру телефона (апрувы за последние 30 дней)
            $hasApprove = Order::find()
                ->byCountryId($order->country_id)
                ->andWhere(['<>', 'id', $order->id])
                ->andWhere(['>=', 'created_at', strtotime('-30 days')])
                ->andWhere(['customer_phone' => $order->customer_phone])
                ->andWhere(['status_id' => OrderStatus::getProcessList()])
                ->count();

            if ($hasApprove) {
                $order->status_id = OrderStatus::STATUS_AUTOTRASH_DOUBLE;
                if (!$order->save()) {
                    throw new \Exception(Yii::t('common', 'Не удалось изменить заказ {order} причина: {reason}', [
                        'order' => $order->id,
                        'reason' => $order->getFirstErrorAsString()
                    ]));
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Подготовка нового реквеста для КЦ
     * @param Order $order
     * @return CallCenterRequest
     * @throws \Exception
     */
    protected function prepareCallCenterRequest(Order $order): CallCenterRequest
    {
        if (!($callCenter = $this->getCallCenterForCountryByWeight($order->country_id))) {
            throw new \Exception(Yii::t('common', 'Не удалось определить колл-центр.'));
        }
        $request = new CallCenterRequest();
        $request->call_center_id = $callCenter->id;
        $request->order_id = $order->id;
        $request->status = CallCenterRequest::STATUS_PENDING;
        $request->cron_launched_at = time();

        if ($order->call_center_type == Order::TYPE_CC_NEW_RETURN) {
            if (isset($model->deliveryRequest)) {
                if ($order->deliveryRequest->status == DeliveryRequest::STATUS_ERROR && $order->deliveryRequest->api_error) {
                    $request->delivery_error = $order->deliveryRequest->api_error;
                    // @todo: Необходима будет дополнительная проверка на $order->deliveryRequest->api_error_type == DeliveryRequest::API_ERROR_COURIER

                }
            }
        }
        return $request;
    }

    /**
     * Поиск рандомного КЦ с учетом их весов
     * @param int $countryId
     * @return CallCenter|null
     */
    protected function getCallCenterForCountryByWeight(int $countryId): ?CallCenter
    {
        $callCenters = CallCenter::find()
            ->where([CallCenter::tableName() . '.country_id' => $countryId])
            ->andWhere([CallCenter::tableName() . '.active' => 1])
            ->orderBy([CallCenter::tableName() . '.weight' => SORT_ASC])
            ->all();

        $total = 0;
        foreach ($callCenters as $callCenter) {
            $total += $callCenter->weight;
        }

        if ($total <= 0) {
            return $callCenters[0] ?? null;
        }

        $scale = 100 / $total;
        $r = rand(0, 100);

        foreach ($callCenters as $callCenter) {
            if (empty($callCenter->weight)) {
                continue;
            }
            $weight = $callCenter->weight * $scale;
            if ($weight >= $r) {
                return $callCenter;
            }
            $r -= $weight;
        }
        return null;
    }

    /**
     * Проверка наличия достаточного количества продуктов на складе для определенного заказа
     * @param Order $order
     * @return bool
     */
    public function checkAvailabilityOfOrderProductsOnStorage(Order $order): bool
    {
        $products = [];
        foreach ($order->orderProducts as $orderProduct) {
            if (!isset($products[$orderProduct->product_id])) {
                $products[$orderProduct->product_id] = 0;
            }
            $products[$orderProduct->product_id] += $orderProduct->quantity;
        }
        foreach ($products as $productId => $quantity) {
            if (!StorageProduct::find()
                ->joinWith('storage', false)
                ->where([
                    Storage::tableName() . '.country_id' => $order->country_id,
                    StorageProduct::tableName() . '.product_id' => $productId
                ])
                ->andWhere([
                    'OR',
                    ['>=', StorageProduct::tableName() . '.balance', $quantity],
                    [StorageProduct::tableName() . '.unlimit' => 1]
                ])->exists()) {
                $this->trigger(static::EVENT_NO_PRODUCTS_IN_STOCK, new NoProductInStockEvent(['productId' => $productId]));
                return false;
            }
        }
        return true;
    }

    /**
     * @param CallCenterRequest|CallCenterCheckRequest $request
     * @return array
     */
    protected function prepareDataForSendToCallCenterByAmazonQueue($request): array
    {
        if (!is_a($request, CallCenterRequest::class) && !is_a($request, CallCenterCheckRequest::class)) {
            throw new \InvalidArgumentException("\$request should be instance of " . CallCenterRequest::class . ' or ' . CallCenterCheckRequest::class);
        }

        $lead = $request->order;

        $trimmedAddress = $lead->customer_address ? StringHelper::trim($lead->customer_address) : $lead->customer_address;

        // Если реквест уже находится в статусе "in_progress", то возможно мы его отправляем повторно, о чем уведомляем КЦ
        $isSecondSent = ($request instanceof CallCenterRequest && $request->status == CallCenterRequest::STATUS_IN_PROGRESS) || ($request instanceof CallCenterCheckRequest && $request->status == CallCenterCheckRequest::STATUS_IN_PROGRESS);

        $data = [
            'country' => $lead->country->char_code,
            'phone' => $lead->customer_phone,
            'mobile' => $lead->customer_mobile,
            'address' => $trimmedAddress,
            'shipping_price' => $lead->delivery,
            'customer_email' => $lead->customer_email,
            'customer_full_name' => $lead->customer_full_name,
            'source_uri' => $lead->landing->url,
            'ip' => $lead->customer_ip,
            'id' => $lead->id,
            'request_id' => $request->id,
            'ordered_at' => $lead->lead ? $lead->lead->ts_spawn : $lead->created_at,
            'queue_type' => $lead->call_center_type,
            'partner' => $request->callCenter->token,
            'order_comment' => $this->getOrderCommentForQueue($request),
            'products' => [],
            'is_second_sent' => $isSecondSent
        ];

        foreach ($lead->orderProducts as $orderProduct) {
            $data['products'][] = [
                'id' => $orderProduct->product_id,
                'quantity' => $orderProduct->quantity,
                'price' => $orderProduct->price,
            ];
        }

        return $data;
    }

    /**
     * @param CallCenterRequest|CallCenterCheckRequest $request
     * @return mixed|string
     */
    protected function getOrderCommentForQueue($request)
    {
        if ($request instanceof CallCenterRequest) {
            if ($request->order->call_center_type == Order::TYPE_CC_NEW_RETURN) {
                return $request->api_error;
            }
        }
        if ($request instanceof CallCenterCheckRequest) {
            if ($request->type == CallCenterCheckRequest::TYPE_CHECK_ADDRESS && $request->api_error) {
                return $request->api_error;
            }
            if ($request->type == CallCenterCheckRequest::TYPE_INFORMATION) {
                return $request->request_comment;
            }
        }
        return $request->order->comment;
    }

    /**
     * @param CallCenterRequest|CallCenterCheckRequest $request
     * @return array
     */
    protected function prepareDataForSendToCallCenterByApi($request): array
    {
        if (!is_a($request, CallCenterRequest::class) && !is_a($request, CallCenterCheckRequest::class)) {
            throw new \InvalidArgumentException("\$request should be instance of " . CallCenterRequest::class . ' or ' . CallCenterCheckRequest::class);
        }

        $lead = $request->order;
        $trimmedAddress = StringHelper::trim($lead->customer_address);
        $data = [
            'source' => $lead->sourceModel->name,
            'order_id' => $lead->id,
            'order_date' => $lead->created_at,
            'date_created' => $lead->creationDate,
            'fio' => $lead->customer_full_name,
            'address' => !empty($trimmedAddress) ? $trimmedAddress : '(empty)',
            'phone' => $lead->customer_phone,
            'email' => $lead->customer_email,
            'ip' => $lead->customer_ip,
            'price' => $lead->price,
            'currency' => $lead->country->currency ? $lead->country->currency->char_code : '',
            'delivery' => empty($lead->delivery) ? 0 : $lead->delivery,
            'country' => $lead->country->char_code,
            'type' => $lead->call_center_type,
            'resending' => 0,
            'products' => [],
        ];

        if ($lead->call_center_type == Order::TYPE_CC_NEW_RETURN && $request instanceof CallCenterRequest) {
            $data['comment'] = $request->delivery_error;
        } else {
            $data['comment'] = $lead->comment;
        }

        foreach ($lead->orderProducts as $orderProduct) {
            $data['products'][] = [
                'product_id' => $orderProduct->product_id,
                'quantity' => $orderProduct->quantity,
                'price' => $orderProduct->price,
            ];
        }

        return $data;
    }

    /**
     * @param CallCenterRequest $request
     * @param array $data
     * @param string|null $response
     */
    protected function createSendRequestLog(CallCenterRequest &$request, array $data, string $response = null)
    {
        $lastCcSendResponse = new CallCenterRequestSendResponse([
            'call_center_request_id' => $request->id,
            'request' => json_encode($data, JSON_UNESCAPED_UNICODE)
        ]);
        if (!is_null($response)) {
            $lastCcSendResponse->response = $response;
        }
        if (!$lastCcSendResponse->save()) {
            $this->trigger(static::EVENT_SEND_ERROR, new SendRequestErrorEvent([
                'message' => Yii::t('common', 'Не удалось сохранить в логи ответ от КЦ: {error}', ['error' => $lastCcSendResponse->getFirstErrorAsString()]),
                'orderId' => $request->order_id
            ]));
        }
        $request->setLastSendResponse($lastCcSendResponse);
    }

    /**
     * @param null|int $countryId
     * @param int $timeBetweenRepeats Время между повторными попытками отправки заявок [секунды]
     * @param int $batchSize
     * @return \Generator|Order[]
     * @yield Order
     */
    public function getOrdersWaitingForSendToCallCenter(int $countryId = null, int $timeBetweenRepeats = 60, int $batchSize = 1000)
    {
        $timeBorder = time() - $timeBetweenRepeats;
        $query = Order::find()
            ->joinWith(['callCenterRequest', 'country'], false)
            ->where([
                'in',
                Order::tableName() . '.status_id',
                [
                    OrderStatus::STATUS_SOURCE_LONG_FORM,
                    OrderStatus::STATUS_SOURCE_SHORT_FORM,
                ]
            ])
            ->andWhere(['is', CallCenterRequest::tableName() . '.id', null])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(['<=', Order::tableName() . '.created_at', $timeBorder])
            ->andWhere([
                'exists',
                CallCenter::find()
                    ->where(['active' => 1])
                    ->andWhere([CallCenter::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ])
            ->orderBy([Order::tableName() . '.created_at' => SORT_ASC]);
        $secondQuery = Order::find()
            ->joinWith(['callCenterRequest.callCenter', 'country'], false)
            ->where([
                CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_PENDING,
                CallCenter::tableName() . '.active' => 1
            ])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(['<=', CallCenterRequest::tableName() . '.cron_launched_at', $timeBorder])
            ->orderBy([CallCenterRequest::tableName() . '.cron_launched_at' => SORT_ASC]);
        $thirdQuery = Order::find()
            ->joinWith(['callCenterRequest.callCenter', 'country'], false)
            ->where([
                'AND',
                [CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_IN_PROGRESS],
                [
                    '<=',
                    CallCenterRequest::tableName() . '.sent_at',
                    (time() - $this->timeLimitToReceiveSendResponseFromCallCenter)
                ],
                [
                    'OR',
                    [CallCenterRequest::tableName() . '.foreign_id' => 0],
                    [CallCenterRequest::tableName() . '.foreign_id' => null]
                ]
            ])
            ->andWhere([CallCenter::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(['<=', CallCenterRequest::tableName() . '.cron_launched_at', $timeBorder])
            ->orderBy([CallCenterRequest::tableName() . '.cron_launched_at' => SORT_ASC]);

        if ($countryId) {
            $query->andWhere([Order::tableName() . '.country_id' => $countryId]);
            $secondQuery->andWhere([Order::tableName() . '.country_id' => $countryId]);
            $thirdQuery->andWhere([Order::tableName() . '.country_id' => $countryId]);
        }
        $query->union($secondQuery)->union($thirdQuery);

        foreach ($query->batch($batchSize) as $orders) {
            foreach ($orders as $order) {
                yield $order;
            }
        }
    }
}