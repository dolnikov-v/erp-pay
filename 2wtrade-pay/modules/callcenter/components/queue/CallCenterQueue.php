<?php

namespace app\modules\callcenter\components\queue;


use app\components\queue\BaseQueue;
use app\modules\callcenter\components\Sender;
use yii\di\Instance;
use yii\mutex\Mutex;

/**
 * Class CallCenterQueue
 * @package app\modules\callcenter\components\queue
 */
class CallCenterQueue extends BaseQueue
{
    /**
     * @var Sender|string|array|callable
     */
    public $requestSender = Sender::class;

    /**
     * @var Mutex|string|array
     */
    public $mutex = 'mutex';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (is_callable($this->requestSender)) {
            $this->requestSender = call_user_func($this->requestSender);
        }
        $this->requestSender = Instance::ensure($this->requestSender, Sender::class);
        $this->mutex = Instance::ensure($this->mutex, Mutex::class);

    }
}