<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class PersonLinkUserAsset
 * @package app\modules\callcenter\assets
 */
class PersonLinkUserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/call-center/person';

    public $js = [
        'link-user.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
