<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class ActivateUsersAsset
 * @package app\modules\order\assets
 */
class ActivateUsersAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/call-center/activate-users';

    public $js = [
        'activate-users.js',
    ];
}