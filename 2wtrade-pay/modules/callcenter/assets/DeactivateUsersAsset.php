<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class DeactivateUsersAsset
 * @package app\modules\order\assets
 */
class DeactivateUsersAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/call-center/deactivate-users';

    public $js = [
        'deactivate-users.js',
    ];
}