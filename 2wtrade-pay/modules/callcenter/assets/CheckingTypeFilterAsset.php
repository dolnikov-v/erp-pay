<?php

namespace app\modules\callcenter\assets;


use yii\web\AssetBundle;

/**
 * Class CheckingTypeFilterAsset
 * @package app\modules\callcenter\assets
 */
class CheckingTypeFilterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/call-center/checking-type-filter';

    public $css = [
    ];

    public $js = [
        'checking-type-filter.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
