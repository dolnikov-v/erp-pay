<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class SourceSwitcheryAsset
 * @package app\modules\callcenter\assets
 */
class SourceSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/call-center/source-switchery';

    public $js = [
        'source-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}