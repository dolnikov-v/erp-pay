<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class UserAsset
 * @package app\modules\callcenter\assets
 */
class UserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/call-center';

    public $js = [
        'user.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
