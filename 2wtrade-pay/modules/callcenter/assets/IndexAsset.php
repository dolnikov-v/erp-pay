<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package app\modules\order\assets
 */
class IndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/call-center/index';

    public $css = [
        'index.css',
        'edit.css',
    ];

    public $js = [
        'index.js',
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
