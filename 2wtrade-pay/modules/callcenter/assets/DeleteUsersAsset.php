<?php
namespace app\modules\callcenter\assets;

use yii\web\AssetBundle;

/**
 * Class DeleteUsersAsset
 * @package app\modules\order\assets
 */
class DeleteUsersAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/call-center/delete-users';

    public $js = [
        'delete-users.js',
    ];
}