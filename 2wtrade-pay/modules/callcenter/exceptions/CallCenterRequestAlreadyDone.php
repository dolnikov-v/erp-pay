<?php

namespace app\modules\callcenter\exceptions;

/**
 * Class CallCenterRequestAlreadyDone
 * @package app\modules\callcenter\exceptions
 */
class CallCenterRequestAlreadyDone extends \Exception
{

}