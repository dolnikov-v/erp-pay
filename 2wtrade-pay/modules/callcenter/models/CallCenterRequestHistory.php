<?php

namespace app\modules\callcenter\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class CallCenterRequestHistory
 * @package app\modules\callcenter\models
 *
 * @property integer $id
 * @property integer $call_center_request_id
 * @property integer $call_center_id
 * @property integer $operator_id
 * @property integer $foreign_status
 * @property integer $called_at
 *
 * @property CallCenterRequest $callCenterRequest
 * @property CallCenter $callCenter
 * @property CallCenterUser $callCenterUser
 */
class CallCenterRequestHistory extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return "{{%call_center_request_history}}";
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['call_center_request_id', 'call_center_id', 'operator_id', 'called_at'], 'required'],
            [['call_center_request_id', 'call_center_id', 'operator_id', 'called_at', 'foreign_status'], 'integer'],
            [
                'call_center_id',
                'exist',
                'targetClass' => CallCenter::className(),
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение колл-центра.'),
            ],
            [
                'call_center_request_id',
                'exist',
                'targetClass' => CallCenterRequest::className(),
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение заявки.'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'call_center_request_id' => Yii::t('common', 'Номер заявки'),
            'call_center_id' => Yii::t('common', 'Колл-центр'),
            'operator_id' => Yii::t('common', 'Номер оператора'),
            'foreign_status' => Yii::t('common', 'Внешний статус'),
            'called_at' => Yii::t('common', 'Дата звонка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(), ['id' => 'call_center_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterUser()
    {
        return $this->hasOne(CallCenterUser::className(), ['user_id' => 'operator_id', 'callcenter_id' => 'call_center_id']);
    }
}