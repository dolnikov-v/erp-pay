<?php
namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\Product;

/**
 * This is the model class for table "call_center_product_call".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $country_id
 * @property integer $number_of_calls
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property /app/models/Country $country
 * @property /app/models/Product $product
 */
class CallCenterProductCall extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_product_call}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'country_id'], 'required'],
            [['product_id', 'country_id', 'number_of_calls', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            //[['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            //[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'country_id' => 'Country ID',
            'number_of_calls' => 'Number Of Calls',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
