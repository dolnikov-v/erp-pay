<?php
namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;

/**
 * This is the model class for table "call_center_user_order".
 *
 * @property integer $id
 * @property string $user_id
 * @property integer $order_id
 * @property integer $number_of_calls
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property /app/modules/order/models/Order $order
 */
class CallCenterUserOrder extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_user_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'number_of_calls', 'created_at', 'updated_at'], 'integer'],
            [['user_id'], 'string', 'max' => 255],
            //[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'order_id' => 'Order ID',
            'number_of_calls' => 'Number Of Calls',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}