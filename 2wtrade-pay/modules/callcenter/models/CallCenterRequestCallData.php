<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "call_center_request_call_data".
 *
 * @property integer $call_center_request_id
 * @property integer $operator_id
 * @property integer $called_at
 *
 * @property CallCenterRequest $callCenterRequest
 */
class CallCenterRequestCallData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_request_call_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_request_id', 'called_at'], 'required'],
            [['call_center_request_id', 'operator_id', 'called_at'], 'integer'],
            [
                ['call_center_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CallCenterRequest::className(),
                'targetAttribute' => ['call_center_request_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'call_center_request_id' => Yii::t('common', 'Номер заявки'),
            'operator_id' => Yii::t('common', 'Номер оператора'),
            'called_at' => Yii::t('common', 'Дата звонка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(), ['id' => 'call_center_request_id']);
    }

    /**
     * @param array $data
     * @param int $requestId
     */
    public static function saveCallData(array $data, int $requestId)
    {
        foreach ($data as $row) {
            if (!isset($row['record_date'])) {
                continue;
            }
            $calledAt = strtotime($row['record_date']);
            if (!CallCenterRequestCallData::find()->where([
                'called_at' => $calledAt,
                'call_center_request_id' => $requestId
            ])->exists()) {
                $callCenterRequestCallData = new CallCenterRequestCallData();
                $callCenterRequestCallData->call_center_request_id = $requestId;
                $callCenterRequestCallData->called_at = $calledAt;
                $callCenterRequestCallData->operator_id = $row['user_sip'] ?? null;
                $callCenterRequestCallData->save();
            }
        }
    }
}
