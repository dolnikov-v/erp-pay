<?php
namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\modules\logger\components\log\Logger;
use app\modules\callcenter\components\NewCallCenterHandler;
use app\modules\callcenter\models\query\CallCenterUserQuery;
use app\modules\salary\models\Person;
use Yii;

/**
 * Class CallCenterUser
 * @package app\modules\callcenter\models
 * @property integer $id
 * @property integer $parent_id
 * @property integer $callcenter_id
 * @property string $user_id
 * @property string $user_login
 * @property string $user_password
 * @property string $user_role
 * @property integer $active
 * @property integer $person_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property CallCenter $callCenter
 * @property Person $person
 */
class CallCenterUser extends ActiveRecordLogUpdateTime
{

    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    public $linkData = null;
    /**
     * @inheritdoc
     */
    protected function prepareAttributesUpdate(?array $changedAttributes, bool $compare = true): ?array
    {
        $this->linkData = [new LinkData(['field' => 'person_id', 'value' => (empty($this->person_id) && !empty($changedAttributes['person_id'])) ? (int)$changedAttributes['person_id'] : (int)$this->person_id ?? null])];
        return parent::prepareAttributesUpdate($changedAttributes, $compare);
    }

    const USER_ROLE_ANY = -1;
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_OPER = 0;
    const USER_ROLE_CURATOR = 2;
    const USER_ROLE_NOTACTIVE = 9;

    const USER_ROLE_SUPERVISOR = 3;
    const USER_ROLE_HR = 4;
    const USER_ROLE_IT = 5;
    const USER_ROLE_ADMIN_DIRECTOR = 6;
    const USER_ROLE_CLEANER = 7;

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_user}}';
    }

    /**
     * @return CallCenterUserQuery
     */
    public static function find()
    {
        return new CallCenterUserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['callcenter_id'], 'required'],
            [
                'callcenter_id',
                'exist',
                'targetClass' => '\app\modules\callcenter\models\CallCenter',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение Колл-центра.'),
            ],
            [
                'person_id',
                'exist',
                'targetClass' => '\app\modules\salary\models\Person',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение сотрудника.'),
            ],
            [['callcenter_id', 'parent_id'], 'integer'],
            [['user_id', 'user_login', 'user_role'], 'string', 'max' => 255],
            ['active', 'default', 'value' => 0],
            ['active', 'number', 'min' => 0, 'max' => 1],
            [['user_password'], 'string'],
            [['user_id'], 'unique', 'targetAttribute' => ['user_id', 'callcenter_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'parent_id' => Yii::t('common', 'Руководитель'),
            'callcenter_id' => Yii::t('common', 'Направление'),
            'user_id' => Yii::t('common', 'Идентификатор пользователя в КЦ'),
            'user_login' => Yii::t('common', 'Логин пользователя в КЦ'),
            'user_password' => Yii::t('common', 'Пароль пользователя в КЦ'),
            'user_role' => Yii::t('common', 'Должность'),
            'active' => Yii::t('common', 'Активен'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'callcenter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @param boolean $empty
     * @return array
     */
    public static function getRoles($empty = false)
    {
        $roles = [
            self::USER_ROLE_ANY => '-',
            self::USER_ROLE_ADMIN => Yii::t('common', 'Админ'),
            self::USER_ROLE_OPER => Yii::t('common', 'Оператор'),
            self::USER_ROLE_CURATOR => Yii::t('common', 'Тимлид'),
            self::USER_ROLE_SUPERVISOR => Yii::t('common', 'Супервайзер'),
            self::USER_ROLE_HR => Yii::t('common', 'Кадровик'),
            self::USER_ROLE_IT => Yii::t('common', 'Айтишник'),
            self::USER_ROLE_ADMIN_DIRECTOR => Yii::t('common', 'Административный директор'),
            self::USER_ROLE_CLEANER => Yii::t('common', 'Уборщик'),
            self::USER_ROLE_NOTACTIVE => Yii::t('common', 'Неактивен')
        ];
        if (!$empty) {
            unset($roles[self::USER_ROLE_ANY]);
        }
        return $roles;
    }

    /**
     * @param integer $callCenterId
     * @return array
     */
    public static function getPatentsCollection($callCenterId = null)
    {
        $query = CallCenterUser::find()->select(
            [
                'id',
                'name' => 'user_login'
            ])
            ->where([
                'in',
                'user_role',
                [
                    self::USER_ROLE_ADMIN,
                    self::USER_ROLE_CURATOR
                ]
            ])
            ->orderBy([
                'name' => SORT_ASC,
                'user_login' => SORT_DESC
            ]);

        if ($callCenterId) {
            $query->andWhere(['callcenter_id' => $callCenterId]);
        }

        return $query->collection();
    }


    /**
     * По сотруднику костыльно выбрать логины, с учетом особенностей нового КЦ
     * выбираем только один из всех
     *
     * @param $personId
     * @return array
     */
    public static function getUniqueIdList($personId)
    {
        $callCenterUses = CallCenterUser::find()
            ->with('callCenter')
            ->byPersonId($personId)
            ->all();

        $ids = [];
        $newCCuserAdded = false;
        foreach ($callCenterUses as $ccuser) {
            if ($ccuser->callCenter->is_amazon_query) {
                if (!$newCCuserAdded) {
                    // возьмем для расчета первый логин из новых КЦ
                    $ids[] = $ccuser->id;
                    $newCCuserAdded = true;
                }
            } else {
                $ids[] = $ccuser->id;
            }
        }

        return $ids;
    }

    /**
     * @return bool|string
     */
    public function getPingInfo()
    {
        return $this->hasOne(CallCenterWorkTime::className(), ['call_center_user_id' => 'id'])
            ->select('date')
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.time', 0])
            ->orderBy([CallCenterWorkTime::tableName() . '.date' => SORT_DESC])
            ->limit(1)
            ->scalar();
    }

    /**
     * Отправка в очередь на блокировку аккаунтов в CRM
     * @param $foreignIds
     */
    public static function sendToBlockInCallCenterQueue($foreignIds)
    {
        $client = NewCallCenterHandler::getClientSQS();

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $actionLog = $logger->getLogger([
            'route' => Yii::$app->controller->getRoute(),
            'process_id' => getmypid(),
        ]);

        $actionData = [
            'action' => 'dismissal',
            'user_id' => array_values($foreignIds)
        ];

        $queryData = [
            'MessageBody' => json_encode($actionData, JSON_UNESCAPED_UNICODE),
            'QueueUrl' => $client->getSqsQueueUrl('amazonActionWithUserCC'),
        ];

        $actionLog('Request: ' . print_r($queryData, true));

        try {
            $result = $client->sendMessage($queryData);
            $actionLog('Result: ' . print_r($result, true));
        } catch (\Throwable $e) {
            $actionLog('Error: ' . $e->getMessage());
        }
    }
}