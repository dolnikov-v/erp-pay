<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class CallCenterRequestReserve
 * copy CallCenterRequest
 * @package app\modules\callcenter\models
 */
class CallCenterRequestReserve extends CallCenterRequest
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_request_reserve}}';
    }

    /**
     * @param CallCenterRequest $callCenterRequest
     * @return bool
     */
    public static function copy($callCenterRequest)
    {
        $callCenterRequestReserve = new self();
        $callCenterRequestReserve->copyAttributes($callCenterRequest);
        return $callCenterRequestReserve->save(false);
    }
}