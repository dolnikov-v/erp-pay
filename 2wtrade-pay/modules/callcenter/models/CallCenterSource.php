<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Source;
use app\modules\callcenter\models\query\CallCenterSourceQuery;
use Yii;

/**
 * Class CallCenterSource
 * @package app\modules\callcenter\models
 * @property integer $call_center_id
 * @property integer $source_id
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property Source $source
 */
class CallCenterSource extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_source}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_id', 'source_id'], 'required'],
            [['call_center_id', 'active', 'created_at', 'updated_at'], 'integer'],
            [
                'source_id',
                'exist',
                'targetClass' => Source::class,
                'targetAttribute' => 'id'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'call_center_id' => Yii::t('common', 'Колл-центр'),
            'source_id' => Yii::t('common', 'Источник'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @inheritdoc
     * @return CallCenterSourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CallCenterSourceQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }
}