<?php

namespace app\modules\callcenter\models;

use app\components\ModelTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "call_center_request_extra".
 *
 * @property integer $call_center_request_id
 * @property integer $calls_count
 * @property integer $hold_to
 *
 * @property CallCenterRequest $callCenterRequest
 */
class CallCenterRequestExtra extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_request_extra}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_request_id'], 'required'],
            [['call_center_request_id', 'calls_count', 'hold_to'], 'integer'],
            [['call_center_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallCenterRequest::className(), 'targetAttribute' => ['call_center_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'call_center_request_id' => Yii::t('common', 'Номер'),
            'calls_count' => Yii::t('common', 'Число звонков'),
            'hold_to' => Yii::t('common', 'Отложен в апруве')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(), ['id' => 'call_center_request_id']);
    }
}
