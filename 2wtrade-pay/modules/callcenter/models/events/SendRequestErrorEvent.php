<?php

namespace app\modules\callcenter\models\events;


use yii\base\Event;

/**
 * Class SendRequestErrorEvent
 * @package app\modules\callcenter\models\events
 */
class SendRequestErrorEvent extends Event
{
    /**
     * @var string
     */
    public $message;

    /**
     * @var integer
     */
    public $orderId;
}