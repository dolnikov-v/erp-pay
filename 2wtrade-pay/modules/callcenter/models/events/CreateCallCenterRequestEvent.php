<?php

namespace app\modules\callcenter\models\events;


use app\modules\callcenter\models\CallCenterRequest;
use yii\base\Event;

/**
 * Class CreateCallCenterRequestEvent
 * @package app\modules\callcenter\models\events
 */
class CreateCallCenterRequestEvent extends Event
{
    /**
     * @var CallCenterRequest
     */
    public $request;
}