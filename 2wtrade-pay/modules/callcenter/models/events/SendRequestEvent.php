<?php

namespace app\modules\callcenter\models\events;


use app\modules\callcenter\models\CallCenterRequest;
use yii\base\Event;

/**
 * Class BeforeSendRequestEvent
 * @package app\modules\callcenter\models\events
 */
class SendRequestEvent extends Event
{
    /**
     * @var array
     */
    public $requestData;

    /**
     * @var array|null
     */
    public $responseData;

    /**
     * @var string|null
     */
    public $response;

    /**
     * @var CallCenterRequest
     */
    public $request;
}