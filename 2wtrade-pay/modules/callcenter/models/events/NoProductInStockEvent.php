<?php

namespace app\modules\callcenter\models\events;

use yii\base\Event;

/**
 * Class NoProductInStockEvent
 * @package app\modules\callcenter\models\events
 */
class NoProductInStockEvent extends Event
{
    /**
     * @var integer
     */
    public $productId;
}