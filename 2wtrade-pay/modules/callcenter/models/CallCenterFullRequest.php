<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportCallCenterComparator;
use Yii;

/**
 * Class CallCenterFullRequest
 *
 * @property integer $id
 * @property integer $call_center_id
 * @property integer $order_id
 * @property integer $foreign_id
 * @property integer $status
 * @property integer $sub_status
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property string $address_components
 * @property string $products
 * @property string $shipping
 * @property string $history
 * @property double $delivery
 * @property string $comment
 * @property integer $last_update
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $response_message
 *
 * @property integer $price
 * @property string $customerCity
 * @property string $customerProvince
 * @property string $customerStreet
 * @property Order $order
 * @property CallCenter $callCenter
 * @property OrderProduct[] $orderProducts
 * @property string $responseMessage
 */
class CallCenterFullRequest extends ActiveRecordLogUpdateTime
{
    const STATUS_POST_CALL = 1;
    const STATUS_APPROVED = 4;
    const STATUS_FAIL_CALL = 5;
    const STATUS_RECALL = 6;
    const STATUS_REJECTED = 7;
    const STATUS_DOUBLE = 15;
    const STATUS_TRASH = 19;

    /** @var  array */
    protected $orderProducts;

    /**
     * @var array
     */
    protected $addressAdditional;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_full_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foreign_id', 'call_center_id'], 'required'],
            [
                [
                    'call_center_id',
                    'order_id',
                    'foreign_id',
                    'status',
                    'sub_status',
                    'created_at',
                    'updated_at',
                    'last_update'
                ],
                'integer'
            ],
            [
                ['address', 'address_components', 'products', 'shipping', 'history', 'comment', 'response_message'],
                'string'
            ],
            [['delivery'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'foreign_id' => Yii::t('common', 'Внешний номер'),
            'status' => Yii::t('common', 'Статус'),
            'sub_status' => Yii::t('common', 'Подстатус'),
            'name' => Yii::t('common', 'Имя покупателя'),
            'phone' => Yii::t('common', 'Номер телефона'),
            'address' => Yii::t('common', 'Адрес'),
            'products' => Yii::t('common', 'Продукты'),
            'history' => Yii::t('common', 'История'),
            'delivery' => Yii::t('common', 'Цена доставки'),
            'comment' => Yii::t('common', 'Комментарий'),
            'last_update' => Yii::t('common', 'Последнее обновление'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_POST_CALL => Yii::t('common', 'На обзвоне'),
            self::STATUS_APPROVED => Yii::t('common', 'Одобрено'),
            self::STATUS_FAIL_CALL => Yii::t('common', 'Недозвон'),
            self::STATUS_RECALL => Yii::t('common', 'Перезвон'),
            self::STATUS_REJECTED => Yii::t('common', 'Отклонено'),
            self::STATUS_DOUBLE => Yii::t('common', 'Дубль'),
            self::STATUS_TRASH => Yii::t('common', 'Мусор'),
        ];
    }

    /**
     * @return int|mixed
     */
    public function getStatusName()
    {
        $labels = self::statusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return $this->status;
    }

    /**
     * @return int|mixed|null
     */
    public function getOrderStateName()
    {
        if (!empty($this->order)) {
            $status = self::STATUS_APPROVED;
            if (!is_null($this->order->callCenterRequest)) {
                $status = $this->order->callCenterRequest->foreign_status;
            } elseif (isset(ReportCallCenterComparator::$mapStatuses[$this->order->status_id])) {
                $status = ReportCallCenterComparator::$mapStatuses[$this->order->status_id];
            }
            $labels = self::statusLabels();
            if (isset($labels[$status])) {
                return $labels[$status];
            }
            return $status;
        }
        return null;
    }

    /**
     * @return OrderProduct[]|mixed
     */
    public function getOrderProducts()
    {
        if (is_null($this->orderProducts)) {
            $this->orderProducts = [];
            $buffer = json_decode($this->products, true);
            if (is_array($buffer)) {
                foreach ($buffer as $item) {
                    $prod = new OrderProduct();
                    $prod->load($item, '');
                    $this->orderProducts[] = $prod;
                }
            }
        }

        return $this->orderProducts;
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        $this->getOrderProducts();

        $price = 0;

        foreach ($this->orderProducts as $prod) {
            $price += $prod->price * $prod->quantity;
        }

        return $price;
    }

    /**
     * @return array|mixed
     */
    public function getAddressComponents()
    {
        if (is_null($this->addressAdditional)) {
            $this->addressAdditional = json_decode($this->address_components, true);
            if (!is_array($this->addressAdditional)) {
                $this->addressAdditional = [];
            }
        }
        return $this->addressAdditional;
    }

    /**
     * @return mixed|null
     */
    public function getCustomerCity()
    {
        $this->getAddressComponents();

        if (isset($this->addressAdditional['customer_city'])) {
            return $this->addressAdditional['customer_city'];
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getCustomerProvince()
    {
        $this->getAddressComponents();

        if (isset($this->addressAdditional['customer_province'])) {
            return $this->addressAdditional['customer_province'];
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getCustomerStreet()
    {
        $this->getAddressComponents();

        if (isset($this->addressAdditional['customer_street'])) {
            return $this->addressAdditional['customer_street'];
        }
        return null;
    }

    /**
     * @return string
     */
    public function getResponseMessage()
    {
        $msg = json_decode($this->response_message, true);
        if (empty($msg)) {
            $msg = $this->response_message;
        } else {
            $msg = print_r($msg, true);
        }
        return $msg;
    }
}
