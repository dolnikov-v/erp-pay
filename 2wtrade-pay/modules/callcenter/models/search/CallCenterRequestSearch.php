<?php
namespace app\modules\callcenter\models\search;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CallCenterRequestSearch
 * @package app\modules\callcenter\models\search
 */
class CallCenterRequestSearch extends CallCenterRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_id', 'order_id'], 'integer'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = CallCenterRequest::find()
            ->joinWith('callCenter')
            ->where([CallCenter::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['call_center_id' => $this->call_center_id]);
        $query->andFilterWhere(['order_id' => $this->order_id]);
        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
