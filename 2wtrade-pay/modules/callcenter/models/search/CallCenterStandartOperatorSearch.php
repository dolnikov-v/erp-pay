<?php
namespace app\modules\callcenter\models\search;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterStandartOperator;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CallCenterStandartOperatorSearch
 * @package app\modules\callcenter\models\search
 */
class CallCenterStandartOperatorSearch extends CallCenterStandartOperator
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = CallCenterStandartOperatorSearch::find()
            ->with([CallCenter::tableName()])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
