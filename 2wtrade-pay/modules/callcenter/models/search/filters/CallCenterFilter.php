<?php

namespace app\modules\callcenter\models\search\filters;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class CallCenterFilter
 * @package app\modules\callcenter\models\search\filters
 */
class CallCenterFilter extends Filter
{
    const ENTITY_CHECKING_REQUEST = 'checking_request';

    /**
     * @var string
     */
    public $entity = self::ENTITY_CHECKING_REQUEST;
    /**
     * @var array
     */
    public $callCenter;

    /**
     * @var array
     */
    public $callCenters = [];

    /**
     * @var integer
     */
    public $countryId;


    public function init()
    {
        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }
        $this->callCenters = CallCenter::find()
            ->byCountryId($this->countryId)
            ->active()
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['callCenter', 'in', 'range' => array_keys($this->callCenters), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([$this->getEntityAttribute($this->entity) => $this->callCenter]);
        }
    }

    /**
     * @param $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_CHECKING_REQUEST:
                $attribute = CallCenterCheckRequest::tableName() . '.call_center_id';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'callCenter')->select2List($this->callCenters, [
            'prompt' => '—',
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'callCenter' => Yii::t('common', 'Колл-центр'),
        ];
    }
}
