<?php

namespace app\modules\callcenter\models\search\filters;

use Yii;

/**
 * Class ForeignIdFilter
 * @package app\modules\callcenter\models\search\filters
 */
class ForeignIdFilter extends NumberFilter
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('common', 'Внешний номер'),
        ];
    }
}
