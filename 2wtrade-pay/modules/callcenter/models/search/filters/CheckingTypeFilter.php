<?php

namespace app\modules\callcenter\models\search\filters;

use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\widgets\CheckingTypeFilter as CheckingTypeWidget;

/**
 * Class CheckingTypeFilter
 * @package app\modules\callcenter\models\search\filters
 */
class CheckingTypeFilter extends Filter
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $informationType;

    /**
     * @var array
     */
    public $types = [];
    /**
     * @var array
     */
    public $informationTypes = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types)],
            ['informationType', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->types = CallCenterCheckRequest::getTypes();
    }


    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([CallCenterCheckRequest::tableName() . '.type' => $this->type]);
            if (!empty($this->informationType)) {
                $query->andFilterWhere([CallCenterCheckRequest::tableName() . '.foreign_information' => $this->informationType]);
            }
        }

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CheckingTypeWidget::widget([
            'types' => $this->types,
            'form' => $form,
            'informations' => CallCenterCheckRequest::getInformationCollection(),
            'filter' => $this,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => \Yii::t('common', 'Тип очереди'),
        ];
    }
}
