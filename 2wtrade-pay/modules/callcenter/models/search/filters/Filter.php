<?php
namespace app\modules\callcenter\models\search\filters;

use yii\base\Model;

/**
 * Class Filter
 * @package app\modules\callcenter\models\filters
 */
abstract class Filter extends Model
{
    public $not = false;

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public abstract function apply($query, $params);

    /**
     * @param \app\components\widgets\ActiveForm $form
     */
    public abstract function restore($form);
}
