<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.04.17
 * Time: 18:19
 */

namespace app\modules\callcenter\models\search\filters;

use app\modules\callcenter\models\CallCenterCheckRequest;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Html;

/**
 * Class NumberFilter
 * @package app\modules\callcenter\models\search\filters
 */
class NumberFilter extends Filter
{
    const ENTITY_CHECKING_REQUEST_ORDER = 'checking_request_order';
    const ENTITY_CHECKING_REQUEST_FOREIGN_ID = 'checking_request_foreign_id';

    /**
     * @var string|array
     */
    public $entity = self::ENTITY_CHECKING_REQUEST_ORDER;

    /**
     * @var string
     */
    public $number = '';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['number', 'string'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $attribute = $this->getEntityAttribute($this->entity);
            $query->andFilterWhere([$attribute => $this->number]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        if (is_array($this->number)) {
            $this->number = implode(', ', $this->number);
        }
        $output = $form->field($this, 'number')->textInput();
        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_CHECKING_REQUEST_ORDER:
                $attribute = CallCenterCheckRequest::tableName() . '.order_id';
                break;
            case self::ENTITY_CHECKING_REQUEST_FOREIGN_ID:
                $attribute = CallCenterCheckRequest::tableName() . '.foreign_id';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!empty($this->number) && is_string($this->number)) {
            $number = explode(',', $this->number);
            $this->number = array_map('intval', $number);
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('common', 'Номер заказа'),
        ];
    }
}
