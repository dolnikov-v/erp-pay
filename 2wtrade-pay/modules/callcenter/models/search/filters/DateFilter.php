<?php

namespace app\modules\callcenter\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\widgets\filters\DateFilter as DateFilterWidget;
use Yii;
use yii\base\InvalidParamException;
use app\models\Timezone;

/**
 * Class DateFilter
 * @package app\modules\callcenter\models\search\filters
 */
class DateFilter extends Filter
{
    const TYPE_IGNORE = '';
    const TYPE_CREATED_AT = 'created_at';
    const TYPE_UPDATED_AT = 'updated_at';
    const TYPE_CALL_CENTER_SENT_AT = 'c_sent_at';
    const TYPE_CALL_CENTER_APPROVED_AT = 'c_approved_at';
    const TYPE_DELIVERY_CREATED_AT = 'd_created_at';
    const TYPE_DELIVERY_SENT_AT = 'd_sent_at';
    const TYPE_DELIVERY_RETURNED_AT = 'd_returned_at';
    const TYPE_DELIVERY_APPROVED_AT = 'd_approved_at';
    const TYPE_DELIVERY_ACCEPTED_AT = 'd_accepted_at';
    const TYPE_DELIVERY_PAID_AT = 'd_paid_at';
    const TYPE_CHECK_SENT_AT = 'check_sent_at';
    const TYPE_CHECK_DONE_AT = 'check_done_at';
    const TYPE_CHECK_CREATED_AT = 'check_created_at';
    const TYPE_CHECK_UPDATED_AT = 'check_updated_at';

    public $from;
    public $to;
    public $type = self::TYPE_IGNORE;
    public $timezone = Timezone::DEFAULT_OFFSET;

    public $types = [];
    public $timezones = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->types = [
            self::TYPE_IGNORE => Yii::t('common', 'Игнорировать'),
            self::TYPE_CHECK_CREATED_AT => Yii::t('common', 'Дата создания заявки'),
            self::TYPE_CHECK_UPDATED_AT => Yii::t('common', 'Дата обновления заявки'),
            self::TYPE_CHECK_SENT_AT => Yii::t('common', 'Дата отправки на проверку (Колл-центр)'),
            self::TYPE_CHECK_DONE_AT => Yii::t('common', 'Дата завершения проверки (Колл-центр)'),
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания заказа'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления заказа'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата основной отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
        ];

        $this->timezones = Timezone::find()->customCollection('time_offset', 'name');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types)],
            [['from', 'to', 'timezone'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'type' => Yii::t('common', 'Тип'),
            'timezone' => Yii::t('common', 'Временная зона'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->validate()) {
            if ($this->type) {
                if ($this->from && $dateFrom = Yii::$app->formatter->asTimestamp($this->from) + $this->timezone) {
                    $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                }

                if ($this->to && $dateTo = Yii::$app->formatter->asTimestamp($this->to) + $this->timezone) {
                    $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo + 86399]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     *
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        return DateFilterWidget::widget([
            'form' => $form,
            'model' => $this,
            'showSelectType' => true,
        ]);
    }

    /**
     * @return string
     */
    public function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CREATED_AT => Order::tableName() . '.created_at',
            self::TYPE_UPDATED_AT => Order::tableName() . '.updated_at',
            self::TYPE_CALL_CENTER_SENT_AT => CallCenterRequest::tableName() . '.sent_at',
            self::TYPE_CALL_CENTER_APPROVED_AT => CallCenterRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_CREATED_AT => DeliveryRequest::tableName() . '.created_at',
            self::TYPE_DELIVERY_SENT_AT => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at)',
            self::TYPE_DELIVERY_RETURNED_AT => DeliveryRequest::tableName() . '.returned_at',
            self::TYPE_DELIVERY_APPROVED_AT => DeliveryRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_ACCEPTED_AT => DeliveryRequest::tableName() . '.accepted_at',
            self::TYPE_DELIVERY_PAID_AT => DeliveryRequest::tableName() . '.paid_at',
            self::TYPE_CHECK_DONE_AT => CallCenterCheckRequest::tableName() . '.done_at',
            self::TYPE_CHECK_SENT_AT => CallCenterCheckRequest::tableName() . '.sent_at',
            self::TYPE_CHECK_CREATED_AT => CallCenterCheckRequest::tableName() . '.created_at',
            self::TYPE_CHECK_UPDATED_AT => CallCenterCheckRequest::tableName() . '.updated_at',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
