<?php
namespace app\modules\callcenter\models\search;

use app\modules\callcenter\models\CallCenterUser;
use yii\data\ActiveDataProvider;

/**
 * Class CallCenterUserSearch
 * @package app\modules\callcenter\models\search
 */
class CallCenterUserSearch extends CallCenterUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_login', 'callcenter_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = CallCenterUser::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'user_id', $this->user_id]);
        $query->andFilterWhere(['like', 'user_login', $this->user_login]);
        $query->andFilterWhere(['callcenter_id' => $this->callcenter_id]);
        return $dataProvider;
    }
}
