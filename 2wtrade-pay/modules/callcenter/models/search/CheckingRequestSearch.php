<?php

namespace app\modules\callcenter\models\search;

use app\helpers\DataProvider;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\search\filters\CallCenterFilter;
use app\modules\callcenter\models\search\filters\CheckingTypeFilter;
use app\modules\callcenter\models\search\filters\DateFilter;
use app\modules\callcenter\models\search\filters\ForeignIdFilter;
use app\modules\callcenter\models\search\filters\NumberFilter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class CheckingRequestSearch
 * @package app\modules\callcenter\models\search
 */
class CheckingRequestSearch extends CallCenterCheckRequest
{
    /**
     * @var null|CallCenterFilter
     */
    protected $callCenterFilter = null;
    /**
     * @var null|NumberFilter
     */
    protected $orderIdFilter = null;
    /**
     * @var null|ForeignIdFilter
     */
    protected $foreignIdFilter = null;

    /**
     * @var null| DateFilter
     */
    protected $dateFilter = null;

    /**
     * @var null| CheckingTypeFilter
     */
    protected $checkingTypeFilter = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'status'], 'string'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = CallCenterCheckRequest::find();
        $query->joinWith(['callCenter', 'order.callCenterRequest', 'order.deliveryRequest']);
        $query->where([CallCenter::tableName() . '.country_id' => \Yii::$app->user->country->id])
            ->orderBy($sort);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if ($this->validate()) {
            $query->andFilterWhere([CallCenterCheckRequest::tableName() . '.status' => $this->status]);
            $this->applyFilters($query, $params);

        }

        return $dataProvider;
    }


    /**
     * @param ActiveQuery $query
     * @param array $params
     */
    protected function applyFilters($query, $params)
    {
        $this->getDateFilter()->apply($query, $params);
        $this->getCallCenterFilter()->apply($query, $params);
        $this->getOrderIdFilter()->apply($query, $params);
        $this->getForeignIdFilter()->apply($query, $params);
        $this->getCheckingTypeFilter()->apply($query, $params);
    }

    /**
     * @return CallCenterFilter|null
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter(['entity' => CallCenterFilter::ENTITY_CHECKING_REQUEST]);
        }
        return $this->callCenterFilter;
    }

    /**
     * @return NumberFilter|null
     */
    public function getOrderIdFilter()
    {
        if (is_null($this->orderIdFilter)) {
            $this->orderIdFilter = new NumberFilter(['entity' => NumberFilter::ENTITY_CHECKING_REQUEST_ORDER]);
        }
        return $this->orderIdFilter;
    }

    /**
     * @return ForeignIdFilter|null
     */
    public function getForeignIdFilter()
    {
        if (is_null($this->foreignIdFilter)) {
            $this->foreignIdFilter = new ForeignIdFilter(['entity' => NumberFilter::ENTITY_CHECKING_REQUEST_FOREIGN_ID]);
        }
        return $this->foreignIdFilter;
    }

    /**
     * @return DateFilter|null
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
        }
        return $this->dateFilter;
    }

    /**
     * @return CheckingTypeFilter|null
     */
    public function getCheckingTypeFilter()
    {
        if (is_null($this->checkingTypeFilter)) {
            $this->checkingTypeFilter = new CheckingTypeFilter();
        }
        return $this->checkingTypeFilter;
    }
}
