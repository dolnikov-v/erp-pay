<?php
namespace app\modules\callcenter\models\search;

use app\modules\callcenter\models\CallCenter;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class StorageSearch
 * @package app\modules\callcenter\models\search
 */
class CallCenterSearch extends CallCenter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = CallCenter::find()
            ->where(['country_id' => Yii::$app->user->country->id])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
