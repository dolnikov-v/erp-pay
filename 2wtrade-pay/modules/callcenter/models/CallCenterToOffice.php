<?php

namespace app\modules\callcenter\models;

use app\modules\salary\models\Office;
use Yii;

/**
 * This is the model class for table "call_center_to_office".
 *
 * @property integer $call_center_id
 * @property integer $office_id
 *
 * @property CallCenter $callCenter
 * @property Office $office
 */
class CallCenterToOffice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_to_office}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_id', 'office_id'], 'integer'],
            [['call_center_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallCenter::className(), 'targetAttribute' => ['call_center_id' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'call_center_id' => 'Call Center ID',
            'office_id' => 'Office ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }
}
