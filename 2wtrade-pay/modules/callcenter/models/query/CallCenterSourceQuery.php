<?php

namespace app\modules\callcenter\models\query;

use app\components\db\ActiveQuery;
use app\modules\callcenter\models\CallCenterSource;

/**
 * Class CallCenterSourceQuery
 * @package app\modules\callcenter\models\query
 */
class CallCenterSourceQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byCallCenterId($id)
    {
        return $this->andWhere(['call_center_id' => $id]);
    }

    /**
     * @param $sourceId
     * @return $this
     */
    public function bySourceId($sourceId)
    {
        return $this->andWhere([CallCenterSource::tableName() . '.source_id' => $sourceId]);
    }

    /**
     * @inheritdoc
     * @return CallCenterSource[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

        /**
         * @inheritdoc
         * @return CallCenterSource|array|null
         */
        public function one($db = null)
    {
        return parent::one($db);
    }
}