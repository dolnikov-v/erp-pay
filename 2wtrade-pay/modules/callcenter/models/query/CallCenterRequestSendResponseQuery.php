<?php

namespace app\modules\callcenter\models\query;


use app\components\mongodb\ResponseActiveQuery;

/**
 * Class CallCenterRequestSendResponseQuery
 * @package app\modules\callcenter\models\query
 */
class CallCenterRequestSendResponseQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'call_center_request_id';
}