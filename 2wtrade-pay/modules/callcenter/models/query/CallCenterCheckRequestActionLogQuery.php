<?php

namespace app\modules\callcenter\models\query;

use app\components\mongodb\ResponseActiveQuery;

/**
 * Class CallCenterCheckRequestActionLogQuery
 * @package app\modules\callcenter\models\query
 */
class CallCenterCheckRequestActionLogQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'request_id';

}