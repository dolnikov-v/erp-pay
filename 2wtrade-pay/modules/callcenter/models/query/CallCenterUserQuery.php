<?php
namespace app\modules\callcenter\models\query;

use app\components\db\ActiveQuery;
use app\modules\callcenter\models\CallCenterUser;
use yii\helpers\ArrayHelper;

/**
 * Class CallCenterUserQuery
 * @package app\modules\callcenter\models\query
 * @method CallCenterUser one($db = null)
 * @method CallCenterUser[] all($db = null)
 */
class CallCenterUserQuery extends ActiveQuery
{
    /**
     * @param integer $callCenterId
     * @return $this
     */
    public function byCallCenterId($callCenterId)
    {
        return $this->andWhere(['callcenter_id' => $callCenterId]);
    }

    /**
     * @param integer $userId
     * @return $this
     */
    public function byUserId($userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    /**
     * @param integer $personId
     * @return $this
     */
    public function byPersonId($personId)
    {
        return $this->andWhere(['person_id' => $personId]);
    }

    /**
     * @return $this
     */
    public function byFreePersonId()
    {
        return $this->andWhere(['person_id' => null]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([CallCenterUser::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        $result = [];
        foreach ($this->all() as $element) {
            $key = $element->id;
            $value = null;
            if (!$value && $element->user_login != '') {
                $value = $element->user_login;
            }
            if (!$value && $element->user_id != '') {
                $value = $element->user_id;
            }
            $result[$key] = $value;
        }
        return $result;
    }
}
