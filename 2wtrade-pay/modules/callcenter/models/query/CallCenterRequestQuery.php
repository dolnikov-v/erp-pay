<?php
namespace app\modules\callcenter\models\query;

use app\components\db\ActiveQuery;
use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class CallCenterRequestQuery
 * @package app\modules\callcenter\models\query
 * @method CallCenterRequest one($db = null)
 * @method CallCenterRequest[] all($db = null)
 */
class CallCenterRequestQuery extends ActiveQuery
{
    /**
     * @param integer $callCenterId
     * @return $this
     */
    public function byCallCenterId($callCenterId)
    {
        return $this->andWhere(['call_center_id' => $callCenterId]);
    }

    /**
     * @param integer $orderId
     * @return $this
     */
    public function byOrderId($orderId)
    {
        return $this->andWhere(['order_id' => $orderId]);
    }

    /**
     * @param integer $orderPid
     * @return $this
     */
    public function byOrderPid($orderPid)
    {
        return $this->andWhere(['order_pid' => $orderPid]);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere(['status' => $status]);
    }
}
