<?php
namespace app\modules\callcenter\models\query;

use app\components\db\ActiveQuery;
use app\models\User;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Office;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class CallCenterQuery
 * @package app\modules\callcenter\models\query
 * @method CallCenter one($db = null)
 * @method CallCenter[] all($db = null)
 */
class CallCenterQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }

    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        return $this->andWhere([CallCenter::tableName() . '.country_id' => array_keys(User::getAllowCountries())]);
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        if ($officeId) {
            return $this->joinWith('offices')->andWhere([Office::tableName() . '.id' => $officeId]);
        } else {
            return $this;
        }
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([CallCenter::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }

    /**
     * @return $this
     */
    public function amazonQuery()
    {
        return $this->andWhere([CallCenter::tableName() . '.is_amazon_query' => 1]);
    }

    /**
     * @return $this
     */
    public function notAmazonQuery()
    {
        return $this->andWhere(['is_amazon_query' => 0]);
    }

    /**
     * @return $this
     */
    public function canGetOperatorWorkTimeByApi()
    {
        return $this->andWhere([CallCenter::tableName() . '.get_operator_work_time_by_api' => 1]);
    }

    /**
     * @return $this
     */
    public function canGetOperatorNumberOfCallByApi()
    {
        return $this->andWhere([CallCenter::tableName() . '.get_operator_number_of_call_by_api' => 1]);
    }

    /**
     * @param bool $withCountry
     * @return array
     */
    public function collection($withCountry = false)
    {
        if ($withCountry) {
            $result = [];
            foreach ($this->all() as $element) {
                $result[$element->id] = Yii::t('common', $element->country->name) . ' | ' . Yii::t('common', $element->name);
            }
            return $result;
        } else {
            return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
        }
    }

    /**
     * @return array
     */
    public function list()
    {
        $result = [];
        foreach ($this->all() as $element) {
            $result[] = [
                'id' => $element->id,
                'name' => Yii::t('common', $element->country->name) . ' | ' .  Yii::t('common', $element->name)
            ];
        }
        return $result;
    }
}
