<?php

namespace app\modules\callcenter\models\query;


use app\components\mongodb\ResponseActiveQuery;

/**
 * Class CallCenterRequestUpdateResponseQuery
 * @package app\modules\callcenter\models\query
 */
class CallCenterRequestUpdateResponseQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'call_center_request_id';
}