<?php
namespace app\modules\callcenter\models\query;

use app\components\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class CallCenterStandartOperatorQuery
 * @package app\modules\callcenter\models\query
 * @method CallCenterStandartOperatorQuery one($db = null)
 * @method CallCenterStandartOperatorQuery[] all($db = null)
 */
class CallCenterStandartOperatorQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param null $language
     * @return array []
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }
}
