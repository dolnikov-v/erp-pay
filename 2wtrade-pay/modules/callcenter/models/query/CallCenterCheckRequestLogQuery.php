<?php

namespace app\modules\callcenter\models\query;

use app\components\mongodb\ResponseActiveQuery;

/**
 * Class CallCenterCheckRequestLogQuery
 * @package app\modules\callcenter\models\query
 */
class CallCenterCheckRequestLogQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'request_id';
}