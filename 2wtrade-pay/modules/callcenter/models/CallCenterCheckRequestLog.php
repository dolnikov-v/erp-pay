<?php

namespace app\modules\callcenter\models;

use app\models\User;
use app\modules\callcenter\models\query\CallCenterCheckRequestLogQuery;
use app\components\mongodb\ActiveRecord;
use Yii;

/**
 * Class CallCenterCheckRequestLog
 * @package app\modules\callcenter\models
 *
 * @property integer $request_id
 * @property integer $user_id
 * @property string $route
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 */
class CallCenterCheckRequestLog extends ActiveRecord
{
    /**
     * @return CallCenterCheckRequestLogQuery
     */
    public static function find()
    {
        return new CallCenterCheckRequestLogQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'call_center_check_request_log';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'request_id', 'user_id', 'route', 'group_id', 'field', 'old', 'new', 'created_at'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['request_id', 'created_at', 'user_id'], 'integer'],
            [['route', 'group_id', 'field', 'old', 'new'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('common', 'ID'),
            'request_id' => Yii::t('common', 'Номер запроса'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'route' => Yii::t('common', 'Маршрут'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->old && !is_string($this->old)) {
            $this->old = json_encode($this->old, JSON_UNESCAPED_UNICODE);
        }
        if ($this->new && !is_string($this->new)) {
            $this->new = json_encode($this->new, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }

    public function getUser()
    {
        return User::findOne($this->user_id);
    }
}