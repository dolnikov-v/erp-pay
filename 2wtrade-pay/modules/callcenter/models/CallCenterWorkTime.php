<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "call_center_work_time".
 *
 * @property integer $id
 * @property integer $call_center_user_id
 * @property string $date
 * @property integer $time
 * @property integer $number_of_calls
 * @property integer $first_action
 * @property integer $last_action
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CallCenterUser $callCenterUser
 */
class CallCenterWorkTime extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_work_time}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_user_id'], 'required'],
            [['call_center_user_id', 'time', 'number_of_calls', 'created_at', 'updated_at', 'first_action', 'last_action'], 'integer'],
            [['date'], 'safe'],
            [['call_center_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallCenterUser::className(), 'targetAttribute' => ['call_center_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'call_center_user_id' => Yii::t('common', 'Пользователь'),
            'date' => Yii::t('common', 'Дата'),
            'time' => Yii::t('common', 'Отработанное время'),
            'number_of_calls' => Yii::t('common', 'Кол-во звонков'),
            'first_action' => Yii::t('common', 'Время начала работы'),
            'last_action' => Yii::t('common', 'Время окончания работы'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterUser()
    {
        return $this->hasOne(CallCenterUser::className(), ['id' => 'call_center_user_id']);
    }

    /**
     * @param int $callCenterId
     * @return bool|string
     */
    public static function getTimeLastDate($callCenterId)
    {
        $record = CallCenterWorkTime::find()
            ->joinWith('callCenterUser')
            ->andWhere([CallCenterUser::tableName() . '.callcenter_id' => $callCenterId])
            ->andWhere(['is not', CallCenterWorkTime::tableName() . '.time', null])
            ->orderBy(['date' => SORT_DESC])
            ->limit(1)
            ->one();

        return ($record instanceof CallCenterWorkTime) ? $record->date : false;
    }

    /**
     * @param int $callCenterId
     * @return bool|string
     */
    public static function getNumberOfCallsLastDate($callCenterId)
    {
        $record = CallCenterWorkTime::find()
            ->joinWith('callCenterUser')
            ->andWhere([CallCenterUser::tableName() . '.callcenter_id' => $callCenterId])
            ->andWhere(['is not', CallCenterWorkTime::tableName() . '.number_of_calls', null])
            ->orderBy(['date' => SORT_DESC])
            ->limit(1)
            ->one();

        return ($record instanceof CallCenterWorkTime) ? $record->date : false;
    }


    /**
     * время отработки по дням
     *
     * @param $ids
     * @param $dateFrom
     * @param $dateTo
     * @param $dateStart
     * @param $dateDismissal
     * @return array
     */
    public static function getWorkTimes($ids, $dateFrom, $dateTo, $dateStart = null, $dateDismissal = null) {

        if (!$ids) {
            return [];
        }

        $queryTime = CallCenterWorkTime::find()
            ->select([
                'time' => 'SUM(' . CallCenterWorkTime::tableName() . '.time)/3600',
                'date' => CallCenterWorkTime::tableName() . '.date',
            ])
            ->where(['in', 'call_center_user_id', $ids])
            ->andWhere(['>=', 'date', date('Y-m-d', strtotime($dateFrom))])
            ->andWhere(['<=', 'date', date('Y-m-d', strtotime($dateTo))])
            ->groupBy([
                CallCenterWorkTime::tableName() . '.date',
            ]);
        if ($dateStart) {
            $queryTime->andWhere(['>=', 'date', $dateStart]);
        }
        if ($dateDismissal) {
            $queryTime->andWhere(['<=', 'date', $dateDismissal]);
        }

        return ArrayHelper::map($queryTime
            ->asArray()
            ->all(), 'date', 'time');
    }
}
