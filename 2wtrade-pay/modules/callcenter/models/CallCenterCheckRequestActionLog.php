<?php

namespace app\modules\callcenter\models;

use app\modules\callcenter\models\query\CallCenterCheckRequestActionLogQuery;
use app\components\mongodb\ActiveRecord;
use Yii;

/**
 * Class CallCenterCheckRequestActionLog
 * @package app\modules\callcenter\models
 *
 * @property integer $request_id
 * @property integer request
 * @property string $route
 * @property string response
 * @property integer $created_at
 * @property string $type only one of send/take/update
 */
class CallCenterCheckRequestActionLog extends ActiveRecord
{
    const TYPE_SEND = 'send';
    const TYPE_UPDATE = 'update';
    /**
     * @return CallCenterCheckRequestActionLogQuery
     */
    public static function find()
    {
        return new CallCenterCheckRequestActionLogQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'call_center_check_request_action_log';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['request_id', 'created_at'], 'integer'],
            [['route', 'request', 'response'], 'string'],
            ['type', 'in', 'range' => [self::TYPE_SEND, self::TYPE_UPDATE]],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'request_id', 'request', 'route', 'response', 'type', 'created_at'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('common', 'ID'),
            'request_id' => Yii::t('common', 'Номер запроса'),
            'request' => Yii::t('common', 'Запрос'),
            'route' => Yii::t('common', 'Маршрут'),
            'response' => Yii::t('common', 'Ответ'),
            'type' => Yii::t('common', 'Тип действия'),
            'created_at' => Yii::t('common', 'Дата создания'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->request && !is_string($this->request)) {
            $this->request = json_encode($this->request, JSON_UNESCAPED_UNICODE);
        }
        if ($this->response && !is_string($this->response)) {
            $this->response = json_encode($this->response, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
   public function save($runValidation = true, $attributeNames = null)
   {
       if ($this->type == static::TYPE_UPDATE && static::find()->where(['request_id' => $this->request_id, 'response' => $this->response, 'type' => static::TYPE_UPDATE])->exists()) {
           return true;
       }
       return parent::save($runValidation, $attributeNames);
   }
}