<?php

namespace app\modules\callcenter\models;


use app\components\ModelTrait;
use app\modules\callcenter\models\query\CallCenterRequestUpdateResponseQuery;
use MongoDB\BSON\ObjectId;
use yii\mongodb\ActiveRecord;

/**
 * Class CallCenterRequestUpdateResponse
 * @package app\modules\callcenter\models
 *
 * @property ObjectId $_id
 * @property integer $call_center_request_id
 * @property integer $created_at
 * @property string $response
 */
class CallCenterRequestUpdateResponse extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return CallCenterRequestUpdateResponseQuery
     */
    public static function find()
    {
        return new CallCenterRequestUpdateResponseQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'call_center_request_update_response';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'call_center_request_id', 'created_at', 'response'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['call_center_request_id', 'created_at'], 'integer'],
            [['response'], 'string']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            // Обновление времени последнего получения респонса
            CallCenterRequest::updateAll(['cc_update_response_at' => $this->created_at], ['id' => $this->call_center_request_id]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Принудительно конвертируем респонсы в строку
        if ($this->response && !is_string($this->response)) {
            $this->response = json_encode($this->response, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }
}