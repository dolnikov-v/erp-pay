<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\callcenter\models\query\CallCenterRequestQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class CallCenterRequest
 * @package app\modules\callcenter\models
 * @property integer $id
 * @property integer $call_center_id
 * @property integer $order_id
 * @property integer $foreign_id
 * @property string $status
 * @property integer $foreign_status
 * @property integer $foreign_substatus
 * @property integer $foreign_waiting
 * @property integer $last_foreign_operator
 * @property string $api_error
 * @property string $delivery_error
 * @property string $comment
 * @property string $autocheck_address
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $cron_launched_at
 * @property integer $sent_at
 * @property integer $approved_at
 * @property integer $cc_send_response_at
 * @property integer $cc_update_response_at
 * @property integer $analysis_trash_at
 * @property string $analysis_trash_comment
 * @property CallCenter $callCenter
 * @property Order $order
 * @property Order $orderWithOrderProducts
 * @property CallCenterRequest[] $logs
 * @property CallCenterRequestExtra $extra
 * @property string $autoCheckAddressLabel
 *
 * @property CallCenterRequestSendResponse $lastSendResponse
 * @property CallCenterRequestUpdateResponse $lastUpdateResponse
 * @property CallCenterRequestCallData[] $callCenterRequestCallData
 * @property integer $callsCount
 *
 */
class CallCenterRequest extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';
    const STATUS_ERROR = 'error';

    const AUTO_CHECK_ADDRESS_NO_CHECK = 'no_check';
    const AUTO_CHECK_ADDRESS_SUCCESS = 'success';
    const AUTO_CHECK_ADDRESS_FAIL = 'fail';

    const NEED_ANALYSIS_TRASH = -1;

    //Используем его для адкомбо т.к. им нужны не все саб статусы
    const ADCOMBO_REJECT_REASON_EXPENSIVE = '1)Expensive';
    const ADCOMBO_REJECT_REASON_CHANGED_HIS_MIND = '2)Changed his mind';
    const ADCOMBO_REJECT_REASON_HEALTH_ISSUES = '3)Health issues';
    const ADCOMBO_REJECT_REASON_CONSULTATION = '4)Consultation';
    const ADCOMBO_REJECT_REASON_COULD_NOT_REACH_CLIENT = '5)Couldn\'t reach client';
    const ADCOMBO_REJECT_REASON_OTHER_REASON = '6)Other cancellation reason';
    const ADCOMBO_TRASH_REASON_DUPLICATE_ORDER = '7)Duplicate order, double';
    const ADCOMBO_TRASH_REASON_NON_EXISTENT_PHONE_NUMBER = '8)Non-existent phone number';
    const ADCOMBO_TRASH_REASON_DIDNT_PLACE_ORDER = '9)Didn\'t place order';
    const ADCOMBO_TRASH_REASON_OTHER_REASON = '10)Other trash reason';

    /**
     * @var string Выполняемый экшен, при изменение заявки
     */
    public $route;

    /**
     * @var string Комментарий для лога
     */
    public $commentForLog;

    /**
     * @var boolean Создание логов
     */
    public $enableLog = true;

    /**
     * @var null|CallCenterRequestSendResponse
     */
    protected $_lastSendResponse = null;

    /**
     * @var null|CallCenterRequestUpdateResponse
     */
    protected $_lastUpdateResponse = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_request}}';
    }

    /**
     * @return CallCenterRequestQuery
     */
    public static function find()
    {
        return new CallCenterRequestQuery(get_called_class());
    }

    public static function adcomboMappingStatus()
    {
        return [
            402 => CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON,
            403 => CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON,
            501 => CallCenterRequest::ADCOMBO_REJECT_REASON_EXPENSIVE,
            502 => CallCenterRequest::ADCOMBO_REJECT_REASON_CHANGED_HIS_MIND,
            503 => CallCenterRequest::ADCOMBO_REJECT_REASON_HEALTH_ISSUES,
            504 => CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON,
            505 => CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON,
            514 => CallCenterRequest::ADCOMBO_REJECT_REASON_CONSULTATION,
            601 => CallCenterRequest::ADCOMBO_TRASH_REASON_NON_EXISTENT_PHONE_NUMBER,
            602 => CallCenterRequest::ADCOMBO_TRASH_REASON_DUPLICATE_ORDER,
            604 => CallCenterRequest::ADCOMBO_REJECT_REASON_CONSULTATION,
            608 => CallCenterRequest::ADCOMBO_TRASH_REASON_NON_EXISTENT_PHONE_NUMBER,
            609 => CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON,
            610 => CallCenterRequest::ADCOMBO_TRASH_REASON_OTHER_REASON
        ];
    }

    public static function getAdcomboMappingStatus($sub_status = false)
    {
        if (!$sub_status) {
            return false;
        }

        return isset(CallCenterRequest::adcomboMappingStatus()[$sub_status]) ? CallCenterRequest::adcomboMappingStatus()[$sub_status] : false;
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает'),
            self::STATUS_IN_PROGRESS => Yii::t('common', 'Выполняется'),
            self::STATUS_DONE => Yii::t('common', 'Завершена'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_id', 'order_id', 'status'], 'required'],
            [
                'call_center_id',
                'exist',
                'targetClass' => '\app\modules\callcenter\models\CallCenter',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение колл-центра.'),
            ],
            [
                'order_id',
                'exist',
                'targetClass' => '\app\modules\order\models\Order',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение заказа.'),
            ],
            [
                [
                    'foreign_id',
                    'foreign_status',
                    'foreign_substatus',
                    'foreign_waiting',
                    'last_foreign_operator',
                    'analysis_trash_at',
                    'cron_launched_at',
                    'sent_at',
                    'approved_at',
                    'cc_send_response_at',
                    'cc_update_response_at'
                ],
                'integer'
            ],
            ['status', 'default', 'value' => self::STATUS_IN_PROGRESS],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['delivery_error', 'analysis_trash_comment'], 'string', 'max' => 255],
            [['api_error'], 'string', 'max' => 1000],
            [['comment', 'api_error', 'delivery_error'], 'filter', 'filter' => 'trim'],
            ['foreign_waiting', 'number', 'min' => 0, 'max' => 1],
            ['autocheck_address', 'string', 'max' => 50],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public static function mapForeignSubStatuses()
    {
        return [
            402 => "Buyout", //"Выкуплено",
            403 => "No buyout", //"Не выкуплено"
            404 => "Client is awaiting an order",//"Ожидает заказ",
            501 => "Too expensive",//"Слишком дорого"
            502 => "Changed mind", //"Передумал"
            503 => "Medical", //"Медицинские противопоказания"
            504 => "No comments", //"Без комментариев"
            505 => "Auto reject", //"Автоматическое отклонение"
            506 => "Product unknown", //"Не известный продукт"
            507 => "Another shop", //"Совершил покупку в другом магазине"
            508 => "Bad feedback",//"Плохие отзывы в интернете",
            509 => "Personal reason",//"Личная причина не хочет обсуждать",
            510 => "Can`t afford", //"Не может позволить себе",
            511 => "Order already received",//"Заказ доставлен",
            512 => "No longer needed",//"Клиент отказался",
            513 => "Delivery service did not contact the customer",// Служба доставки не связывалась с клиентом
            514 => "Consulting",//"Консультация",
            601 => "Wrong number",//,"Несуществующий номер",
            602 => "Double",//"Дубль",
            603 => "Customer does`t know about the order",//"Клиент ничего не знает о заказе",
            604 => "Consulting",//"Консультация",
            605 => "Ordered from a competitor",//"Заказано у конкурентов",
            606 => "Returned",//"Возврат",
            607 => "Can`t delivered there",//"Не отправляем туда",
            608 => "Incorrect format phone number",//"Не правильный формат телефона",
            609 => "Joke",//"Шуточный заказ",
            2001 => "Delivered",//"Доставлено",
            2002 => "Client not response",//"Клиент не отвечает",
            2003 => "Clients phone number is busy",//"Номе занят",
            2004 => "Clients phone number not is serviced",//"Номер не обслуживается",
            2005 => "Automatic machine response",//"Автоответчик",
            2006 => "Client is not enough money",//,"Не хватает денег",
            2007 => "Changed mind",//"Передумал",
            2008 => "client is not make order",//"Не заказывал",
            2009 => "Bad feedback",//"Плохой обзор",
            2010 => "Wrong address"//"Ошибочный адрес"
        ];
    }

    /**
     * @param $foreign_substatus
     * @return mixed|string
     */
    public static function getTextForeignSubStatuses($foreign_substatus)
    {
        $map = self::mapForeignSubStatuses();

        return isset($map[$foreign_substatus]) ? $map[$foreign_substatus] : 'Unknown';
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->enableLog) {
            $groupId = uniqid();

            $dirtyAttributes = $this->getDirtyAttributes();

            foreach ($dirtyAttributes as $key => $item) {
                if (in_array($key, ['cron_launched_at', 'updated_at'])) {
                    continue;
                }

                if ($item != $this->getOldAttribute($key)) {
                    $old = (string)$this->getOldAttribute($key);

                    $old = is_array($old) ? json_encode($old) : $old;
                    $item = is_array($item) ? json_encode($item) : $item;

                    $log = new CallCenterRequestLog();
                    $log->call_center_request_id = $this->id;
                    $log->group_id = $groupId;
                    $log->field = $key;
                    $log->old = mb_substr($old, 0, 255);
                    $log->new = mb_substr($item, 0, 255);
                    $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                    $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                    $log->comment = $this->commentForLog;
                    $log->save();
                }
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'call_center_id' => Yii::t('common', 'Колл-центр'),
            'api_error' => Yii::t('common', 'Ошибка от API'),
            'delivery_error' => Yii::t('common', 'Ошибка от курьерки'),
            'order_id' => Yii::t('common', 'Заказ'),
            'foreign_id' => Yii::t('common', 'Внешний номер'),
            'status' => Yii::t('common', 'Статус'),
            'foreign_status' => Yii::t('common', 'Внутренний статус'),
            'foreign_substatus' => Yii::t('common', 'Внутренний sub-статус'),
            'foreign_waiting' => Yii::t('common', 'Ожидание от колл-центра'),
            'comment' => Yii::t('common', 'Комментарий КЦ'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'cron_launched_at' => Yii::t('common', 'Дата запуска крона'),
            'sent_at' => Yii::t('common', 'Дата отправки в КЦ'),
            'approved_at' => Yii::t('common', 'Дата апрува КЦ'),
            'callCenter.name' => Yii::t('common', 'Колл-центр'),
            'last_foreign_operator' => Yii::t('common', 'Внешний оператор'),
            'cc_send_response_at' => Yii::t('common', 'Время ответа КЦ при отправке лида'),
            'cc_update_response_at' => Yii::t('common', 'Время ответа КЦ при обновлении статуса'),
            'analysis_trash_at' => Yii::t('common', 'Дата проверки треша в КЦ'),
            // переводим в CallCenterCheckRequest
            'analysis_trash_comment' => Yii::t('common', 'Результат проверки треша в КЦ'),
            // переводим в CallCenterCheckRequest
            'autocheck_address' => Yii::t('common', 'Автоматическая проверка адреса')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderWithOrderProducts()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id'])->with('orderProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(CallCenterRequestLog::className(), ['call_center_request_id' => 'id'])
            ->orderBy([
                CallCenterRequestLog::tableName() . '.created_at' => SORT_DESC,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtra()
    {
        return $this->hasOne(CallCenterRequestExtra::className(), ['call_center_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequestCallData()
    {
        return $this->hasMany(CallCenterRequestCallData::className(), ['call_center_request_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getCallsCount()
    {
        return CallCenterRequestCallData::find()->where(['call_center_request_id' => $this->id])->count();
    }

    /**
     * @return mixed|string
     */
    public function getAutoCheckAddressLabel()
    {
        $labels = self::autoCheckAddressLabels();
        return isset($labels[$this->autocheck_address]) ? $labels[$this->autocheck_address] : $this->autocheck_address;
    }

    /**
     * @return array
     */
    public static function autoCheckAddressLabels()
    {
        return [
            self::AUTO_CHECK_ADDRESS_NO_CHECK => Yii::t('common', 'Не проверялся'),
            self::AUTO_CHECK_ADDRESS_SUCCESS => Yii::t('common', 'Прошел проверку'),
            self::AUTO_CHECK_ADDRESS_FAIL => Yii::t('common', 'Не прошел проверку'),
        ];
    }

    public static function getRequestStatusByOrderStatus($statusId)
    {
        $status = CallCenterRequest::STATUS_DONE;

        $pendingStatuses = [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM
        ];

        $progressStatuses = [
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
        ];

        if (in_array($statusId, $pendingStatuses)) {
            $status = CallCenterRequest::STATUS_PENDING;
        } elseif (in_array($statusId, $progressStatuses)) {
            $status = CallCenterRequest::STATUS_IN_PROGRESS;
        }

        return $status;
    }

    /**
     * @param CallCenterRequest $callCenterRequest
     * @return boolean
     */
    public function copyAttributes($callCenterRequest)
    {
        $changed = false;
        foreach ($callCenterRequest->attributes() as $attribute) {
            if ($callCenterRequest->getOldAttribute($attribute) != $this->$attribute) {
                $this->setAttribute($attribute, $callCenterRequest->getOldAttribute($attribute));
                $changed = true;
            }
        }
        return $changed;
    }

    /**
     * @return CallCenterRequestSendResponse
     */
    public function getLastSendResponse()
    {
        if (is_null($this->_lastSendResponse)) {
            $this->_lastSendResponse = CallCenterRequestSendResponse::find()
                ->where(['call_center_request_id' => $this->id])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }
        return $this->_lastSendResponse;
    }

    /**
     * @return CallCenterRequestUpdateResponse
     */
    public function getLastUpdateResponse()
    {
        if (is_null($this->_lastUpdateResponse)) {
            $this->_lastUpdateResponse = CallCenterRequestUpdateResponse::find()
                ->where(['call_center_request_id' => $this->id])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }
        return $this->_lastUpdateResponse;
    }

    /**
     * @return CallCenterRequestUpdateResponse[]
     */
    public function getUpdateResponses()
    {
        return CallCenterRequestUpdateResponse::find()
            ->where(['call_center_request_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @return CallCenterRequestSendResponse[]
     */
    public function getSendResponses()
    {
        return CallCenterRequestSendResponse::find()
            ->where(['call_center_request_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @param CallCenterRequestSendResponse $response
     * @return $this
     */
    public function setLastSendResponse(CallCenterRequestSendResponse $response)
    {
        $this->_lastSendResponse = $response;
        return $this;
    }

    /**
     * @param CallCenterRequestUpdateResponse $response
     * @return $this
     */
    public function setLastUpdateResponse(CallCenterRequestUpdateResponse $response)
    {
        $this->_lastUpdateResponse = $response;
        return $this;
    }

    /**
     * Добавление нового апдейт респонса
     *
     * @param string $response
     * @return CallCenterRequestUpdateResponse
     */
    public function addUpdateResponse(string $response): ?CallCenterRequestUpdateResponse
    {
        if (!CallCenterRequestUpdateResponse::find()->where(['call_center_request_id' => $this->id, 'response' => $response])->exists()) {
            $lastUpdateResponse = new CallCenterRequestUpdateResponse([
                'call_center_request_id' => $this->id,
                'response' => $response
            ]);
            return $lastUpdateResponse;
        }
        return null;
    }

    /**
     * Добавление нового сенд респонса
     *
     * @param string $response
     * @return CallCenterRequestSendResponse
     */
    public function addSendResponse(string $response): ?CallCenterRequestSendResponse
    {
        if (!CallCenterRequestSendResponse::find()->where(['call_center_request_id' => $this->id, 'response' => $response])->exists()) {
            $lastSendResponse = new CallCenterRequestSendResponse([
                'call_center_request_id' => $this->id,
                'response' => $response
            ]);
            return $lastSendResponse;
        }
        return null;
    }

    /**
     * @return array
     */
    public function getAttemptDates()
    {
        if ($cacheData = $this->cache->get($this->getAttemptDatesCacheKey())) {
            $data = json_decode($cacheData, true);
        } else {
            $data = $this->cacheLastAttemptDates();
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getAttemptDatesCacheKey()
    {
        return "callCenterRequest.{$this->id}.attemptDates.{$this->cc_update_response_at}";
    }

    /**
     * @param int $duration
     * @return array
     */
    public function cacheLastAttemptDates($duration = 86400)
    {
        $data = [];
        if ($this->getLastUpdateResponse()) {
            $updateResponse = json_decode($this->getLastUpdateResponse()->response, true);
            if (isset($updateResponse['call_data'])) {
                foreach ($updateResponse['call_data'] as $record) {
                    $data[] = $record['record_date'];
                }
            }
            $this->cache->set($this->getAttemptDatesCacheKey(), json_encode($data), $duration);
        }

        return $data;
    }
}
