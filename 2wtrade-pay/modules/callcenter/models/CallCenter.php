<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\Source;
use app\modules\callcenter\models\query\CallCenterQuery;
use app\modules\salary\models\Office;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CallCenter
 * @package app\modules\callcenter\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $source_id
 * @property string $name
 * @property string $url
 * @property string $api_key
 * @property string $api_pid
 * @property string $token
 * @property integer $weight
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $get_operator_work_time_by_api
 * @property integer $get_operator_number_of_call_by_api
 * @property boolean $is_amazon_query
 * @property string $jira_issue_collector
 * @property Country $country
 * @property Office[] $offices
 * @property CallCenterToOffice[] $callCenterToOffices
 * @property CallCenterRequest[] $callCenterRequests
 * @property Source $source
 */
class CallCenter extends ActiveRecordLogUpdateTime
{
    public $office_ids;

    protected $insertLog = true;
    protected $updateLog = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center}}';
    }

    /**
     * @return CallCenterQuery
     */
    public static function find()
    {
        return new CallCenterQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение страны.'),
            ],
            [
                'source_id',
                'exist',
                'targetClass' => Source::class,
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение источника.'),
            ],
            [['name', 'url', 'api_key', 'api_pid', 'comment'], 'filter', 'filter' => 'trim'],
            [['name', 'url'], 'string', 'max' => 100],
            [['jira_issue_collector', 'token'], 'string', 'max' => 255],
            ['api_key', 'string', 'max' => 84],
            ['api_pid', 'string', 'max' => 12],
            [['weight'], 'integer', 'min' => 0],
            ['comment', 'string', 'max' => 1000],
            ['active', 'default', 'value' => 0],
            ['active', 'number', 'min' => 0, 'max' => 1],
            ['is_amazon_query', 'default', 'value' => 0],
            ['is_amazon_query', 'number', 'min' => 0, 'max' => 1],
            [['get_operator_number_of_call_by_api', 'get_operator_work_time_by_api'], 'default', 'value' => 1],
            [['get_operator_number_of_call_by_api', 'get_operator_work_time_by_api'], 'number', 'min' => 0, 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'name' => Yii::t('common', 'Название'),
            'url' => Yii::t('common', 'Ссылка'),
            'api_key' => Yii::t('common', 'API-ключ'),
            'api_pid' => Yii::t('common', 'API-pid'),
            'weight' => Yii::t('common', 'Вес'),
            'comment' => Yii::t('common', 'Комментарий'),
            'active' => Yii::t('common', 'Доступность'),
            'get_operator_work_time_by_api' => Yii::t('common', 'Получать отработанное время операторов по API'),
            'get_operator_number_of_call_by_api' => Yii::t('common', 'Получать кол-во звонков операторов по API'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'is_amazon_query' => Yii::t('common', 'Amazon очередь'),
            'jira_issue_collector' => Yii::t('common', 'ID сборщика проблем в Jira'),
            'token' => Yii::t('common', 'Токен'),
            'source_id' => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['id' => 'office_id'])
            ->via('callCenterToOffices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterToOffices()
    {
        return $this->hasMany(CallCenterToOffice::className(), ['call_center_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequests()
    {
        return $this->hasMany(CallCenterRequest::class, ['call_center_id' => 'id']);
    }

    /**
     * @param $callCenterId
     * @return array
     */
    public static function getSources($callCenterId)
    {
        return CallCenterSource::find()
            ->where([CallCenterSource::tableName() . '.call_center_id' => $callCenterId])
            ->andWhere([CallCenterSource::tableName() . '.active' => 1])
            ->select('source_id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }
}
