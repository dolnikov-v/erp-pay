<?php

namespace app\modules\callcenter\models;


use app\components\ModelTrait;
use app\modules\callcenter\models\query\CallCenterRequestSendResponseQuery;
use yii\mongodb\ActiveRecord;

/**
 * Class CallCenterRequestSendResponse
 * @package app\modules\callcenter\models
 *
 * @property integer $call_center_request_id
 * @property integer $created_at
 * @property string $request
 * @property string $response
 */
class CallCenterRequestSendResponse extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return CallCenterRequestSendResponseQuery
     */
    public static function find()
    {
        return new CallCenterRequestSendResponseQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'call_center_request_send_response';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['call_center_request_id', 'created_at'], 'integer'],
            [['request', 'response'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'call_center_request_id', 'created_at', 'response', 'request'];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($insert) {
            // При добавлении нового респонса обновляем время последнего его получения
            CallCenterRequest::updateAll(['cc_send_response_at' => $this->created_at], ['id' => $this->call_center_request_id]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Принудительно конвертируем респонсы в строку
        if ($this->response && !is_string($this->response)) {
            $this->response = json_encode($this->response, JSON_UNESCAPED_UNICODE);
        }
        if ($this->request && !is_string($this->request)) {
            $this->request = json_encode($this->request, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }
}