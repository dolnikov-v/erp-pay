<?php
namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\callcenter\models\query\CallCenterStandartOperatorQuery;
use Yii;

/**
 * Class CallCenterStandartOperator
 * @package app\modules\callcenter\models
 * @property integer $id
 * @property integer $call_center_id
 * @property string $operator_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class CallCenterStandartOperator extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_center_standart_operator}}';
    }

    /**
     * @return CallCenterStandartOperatorQuery
     */
    public static function find()
    {
        return new CallCenterStandartOperatorQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_center_id'], 'required'],
            [['operator_id', 'call_center_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'call_center_id' => Yii::t('common', 'Колл центр'),
            'operator_id' => Yii::t('common', 'Операторы'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }
}
