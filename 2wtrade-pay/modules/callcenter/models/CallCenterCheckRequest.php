<?php

namespace app\modules\callcenter\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class CallCenterCheckRequest
 *
 * @property integer $id
 * @property integer $call_center_id
 * @property string $type
 * @property integer $order_id
 * @property string $status
 * @property integer $foreign_id
 * @property integer $foreign_status
 * @property string $foreign_information
 * @property string $comment
 * @property string $request_comment
 * @property string $api_error
 * @property integer $user_id
 * @property integer $status_id
 * @property integer $need_correction
 * @property integer $after_action_id
 * @property integer $cron_launched_at
 * @property integer $sent_at
 * @property integer $done_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 * @property User $user
 * @property OrderStatus $orderStatus
 * @property CallCenterRequest $callCenterRequest
 * @property CallCenter $callCenter
 *
 */
class CallCenterCheckRequest extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_ERROR = 'error';
    const STATUS_DONE = 'done';

    const TYPE_CHECKUNDELIVERY = 'check_undelivery';
    const TYPE_CHECKDELIVERY = 'check_delivery';
    const TYPE_RETURN = 'return';
    const TYPE_TRASH_CHECK = 'trash_check';
    const TYPE_STILL_WAITING = 'still_waiting';
    const TYPE_CHECK_ADDRESS = 'check_address';
    const TYPE_INFORMATION = 'information';

    const FOREIGN_SUB_STATUS_REJECT_STILL_WAITING = 404;
    const FOREIGN_SUB_STATUS_ALREADY_RECEIVED = 511;
    const FOREIGN_SUB_STATUS_REJECT_NO_LONGER_NEEDED = 512;
    const FOREIGN_SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER = 513;
    const FOREIGN_SUB_STATUS_REJECT_AUTO = 505;

    const FOREIGN_SUB_STATUS_EMPTY = -1;
    const NEED_CORRECTION = 1;

    /**********************
     * Действия после обзвона
     **************************/

    /**
     * Отправить в службу доставки
     */
    const AFTER_ACTION_STILL_WAITING_SEND_CORRECTION_LIST_TO_COURIER = 1;

    /** ***************************************************** */

    /** @var array */
    public static $mapStatuses = [
        4 => self::STATUS_DONE,
        9 => self::STATUS_DONE,
        12 => self::STATUS_DONE,
        1 => self::STATUS_IN_PROGRESS,
        5 => self::STATUS_DONE,
        6 => self::STATUS_DONE,
        7 => self::STATUS_DONE,
        8 => self::STATUS_DONE,
        10 => self::STATUS_DONE,
        11 => self::STATUS_DONE,
        13 => self::STATUS_DONE,
        15 => self::STATUS_DONE,
        17 => self::STATUS_DONE,
        19 => self::STATUS_DONE,
        33 => self::STATUS_DONE,
        20 => self::STATUS_DONE,
    ];

    /**
     * @var null|CallCenterCheckRequestActionLog
     */
    protected $_lastUpdateResponse = null;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%call_center_check_request}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'status'], 'required'],
            [
                [
                    'order_id',
                    'user_id',
                    'status_id',
                    'foreign_id',
                    'foreign_status',
                    'cron_launched_at',
                    'call_center_id',
                    'sent_at',
                    'done_at',
                    'need_correction',
                    'after_action_id',
                ],
                'integer'
            ],
            [['type', 'status'], 'string', 'max' => 50],
            [['api_error'], 'string', 'max' => 255],
            [['comment'], 'string'],
            [['request_comment'], 'string', 'max' => 1000],
            [['foreign_information'], 'string', 'max' => 100],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'status_id' => Yii::t('common', 'Статус'),
            'type' => Yii::t('common', 'Тип'),
            'foreign_id' => Yii::t('common', 'Внешний номер'),
            'status' => Yii::t('common', 'Статус заявки'),
            'foreign_status' => Yii::t('common', 'Внешний статус'),
            'foreign_information' => Yii::t('common', 'Информация'),
            'api_error' => Yii::t('common', 'Ошибка от API'),
            'comment' => Yii::t('common', 'Комментарий'),
            'request_comment' => Yii::t('common', 'Комментарий к заявке'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'done_at' => Yii::t('common', 'Дата завершения заявки'),
            'need_correction' => Yii::t('common', 'Необходимо отправить на корректировку в КС')
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает'),
            self::STATUS_IN_PROGRESS => Yii::t('common', 'Выполняется'),
            self::STATUS_DONE => Yii::t('common', 'Завершена'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @param $status
     * @return bool|mixed
     */
    public static function getStatusByName($status)
    {
        $statuses = self::getStatuses();

        return isset($statuses[$status]) ? $statuses[$status] : false;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_CHECKUNDELIVERY => Yii::t('common', 'check_undelivery'),
            self::TYPE_RETURN => Yii::t('common', 'return'),
            self::TYPE_TRASH_CHECK => Yii::t('common', 'trash_check'),
            self::TYPE_STILL_WAITING => Yii::t('common', 'still_waiting'),
            self::TYPE_CHECKDELIVERY => Yii::t('common', 'check_delivery'),
            self::TYPE_CHECK_ADDRESS => Yii::t('common', 'check_address'),
            self::TYPE_INFORMATION => Yii::t('common', 'information'),
        ];
    }

    /***
     * @param bool $withEmpty
     * @return array
     */
    public static function getInformationCollection($withEmpty = false)
    {
        return [
            self::TYPE_CHECKUNDELIVERY => self::getSubStatusesForCheckUndelivery($withEmpty)
        ];
    }

    /**
     * @return array
     */
    public static function getStillWaitingAfterActionCollection()
    {
        return [
            static::AFTER_ACTION_STILL_WAITING_SEND_CORRECTION_LIST_TO_COURIER => Yii::t('common', 'Отправить коррекционный лист в службу доставки')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @param bool $withEmpty
     * @return array
     */
    public static function getSubStatusesForCheckUndelivery($withEmpty = false)
    {
        // сейчас это такие подстатусы
        $return = [];

        if ($withEmpty) {
            $return[self::FOREIGN_SUB_STATUS_EMPTY] = Yii::t('common', 'Не обработано');
        }
        $return[self::FOREIGN_SUB_STATUS_REJECT_STILL_WAITING] = Yii::t('common', 'Ожидает заказ');
        $return[self::FOREIGN_SUB_STATUS_ALREADY_RECEIVED] = Yii::t('common', 'Заказ доставлен');
        $return[self::FOREIGN_SUB_STATUS_REJECT_NO_LONGER_NEEDED] = Yii::t('common', 'Клиент отказался');
        $return[self::FOREIGN_SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER] = Yii::t('common', 'КС не связалась с клиентом');
        $return[self::FOREIGN_SUB_STATUS_REJECT_AUTO] = Yii::t('common', 'Автоматическое отклонение');

        return $return;

        // такое не приходит уже давно
        return [
            2001 => Yii::t('common', 'Заказ доставлен'),
            2002 => Yii::t('common', 'Клиент не отвечает'),
            2003 => Yii::t('common', 'Номер занят'),
            2004 => Yii::t('common', 'Номер не обслуживается'),
            2005 => Yii::t('common', 'Автоответчик'),
            2006 => Yii::t('common', 'У клиента не хватает денег'),
            2007 => Yii::t('common', 'Клиент изменил свое решение'),
            2008 => Yii::t('common', 'Клиент ничего не заказывал'),
            2009 => Yii::t('common', 'Клиент прочитал плохие отзывы'),
            2010 => Yii::t('common', 'Некорректный адрес'),
        ];
    }

    public static function sendToCheckDelivery()
    {
        $subOquery = new Query();
        $subOquery->select([new Expression('1')]);
        $subOquery->from([self::tableName()]);
        $subOquery->where([self::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')]);
        $subOquery->limit(1);

        $orders = Order::find()
            ->innerJoinWith(['callCenterRequest'])
            ->where(['status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT])
            ->andWhere(['not exists', $subOquery])
            ->orderBy('created_at desc')
            ->limit(10)
            ->all();

        foreach ($orders as $order) {
            $orderCheckHistory = new self;
            $orderCheckHistory->order_id = $order->id;
            $orderCheckHistory->status_id = $order->status_id;
            $orderCheckHistory->type = CallCenterCheckRequest::TYPE_CHECKDELIVERY;
            $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
            $orderCheckHistory->call_center_id = $order->callCenterRequest->call_center_id;
            $orderCheckHistory->save();
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $groupId = uniqid();
        foreach ($changedAttributes as $key => $item) {
            if (!in_array($key, [
                    'created_at',
                    'updated_at',
                    'cron_launched_at'
                ]) && $item != $this->getAttribute($key)) {
                $log = new CallCenterCheckRequestLog();
                $log->request_id = $this->id;
                $log->group_id = $groupId;
                $log->field = $key;
                $log->old = $item;
                $log->new = (string)$this->getAttribute($key);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return CallCenterCheckRequestActionLog
     */
    public function getLastUpdateResponse()
    {
        if (is_null($this->_lastUpdateResponse)) {
            $this->_lastUpdateResponse = CallCenterCheckRequestActionLog::find()
                ->where([
                    'request_id' => $this->id,
                    'type' => CallCenterCheckRequestActionLog::TYPE_UPDATE
                ])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }
        return $this->_lastUpdateResponse;
    }
}
