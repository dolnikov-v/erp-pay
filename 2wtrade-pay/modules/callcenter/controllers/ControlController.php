<?php
namespace app\modules\callcenter\controllers;

use app\models\logs\TableLog;
use app\components\web\Controller;
use app\models\Source;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterSource;
use app\modules\salary\models\Office;
use app\modules\callcenter\models\CallCenterStandartOperator;
use app\modules\callcenter\models\search\CallCenterStandartOperatorSearch;
use app\modules\callcenter\models\search\CallCenterSearch;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;
use app\modules\order\models\Order;
use yii\web\BadRequestHttpException;
use app\components\filters\AjaxFilter;

/**
 * Class ControlController
 * @package app\modules\callcenter\controllers
 */
class ControlController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-source'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CallCenterSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $modelStandartOperatorSearch = new CallCenterStandartOperatorSearch();
        $dataProviderStandartOperator = $modelStandartOperatorSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'dataProviderStandartOperator' => $dataProviderStandartOperator,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $changeLog = TableLog::find()->byTable(CallCenter::clearTableName())->byID($id)->allSorted();

        return $this->render('log', [
            'changeLog' => $changeLog,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $officeId = Yii::$app->request->get('office_id');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new CallCenter();
        }

        /**
         *  Общая информация
         */
        $post = Yii::$app->request->post();
        if ($model->load($post, 'CallCenter')) {
            $isNewRecord = $model->isNewRecord;

            $model->is_amazon_query = ArrayHelper::getValue($post['CallCenter'], 'is_amazon_query', 0);
            $model->get_operator_work_time_by_api = ArrayHelper::getValue($post['CallCenter'], 'get_operator_work_time_by_api', 0);
            $model->get_operator_number_of_call_by_api = ArrayHelper::getValue($post['CallCenter'], 'get_operator_number_of_call_by_api', 0);

            if ($isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
            }

            if ($model->save()) {
                $model->unlinkAll('offices', true);
                if (isset($post['CallCenter']['office_ids'])) {
                    foreach ($post['CallCenter']['office_ids'] as $oId) {
                        $office = Office::findOne($oId);
                        if ($office) {
                            $model->link('offices', $office);
                        }
                    }
                }

                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Колл-центр успешно добавлен.') : Yii::t('common', 'Колл-центр успешно сохранен.'), 'success');

                if ($officeId) {
                    return $this->redirect(Url::toRoute(['/salary/office/edit-direction/', 'id' => $officeId]));
                }
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        /**
         *  Эталонные продажи
         */
        if (!empty(Yii::$app->request->post('CallCenterStandartOperator'))) {

            $operator_ids = Yii::$app->request->post('CallCenterStandartOperator');
            $transaction = Yii::$app->db->beginTransaction();
            $transactionError = true;
            $modelStandartOperator = new CallCenterStandartOperator();
            $listOperators = $modelStandartOperator::findAll([CallCenterStandartOperator::tableName() . '.call_center_id' => $model->id]);

            foreach ($listOperators as $operator) {
                $operator->delete();
            }

            if (isset($operator_ids['operator_id'])) {
                foreach ($operator_ids['operator_id'] as $operator_id) {
                    $params = [
                        'CallCenterStandartOperator' => [
                            'operator_id' => $operator_id,
                            'call_center_id' => $operator_ids['call_center_id']
                        ]
                    ];

                    $modelStandartOperator = new CallCenterStandartOperator();

                    if ($modelStandartOperator->load($params, 'CallCenterStandartOperator')) {
                        $modelStandartOperator->call_center_id = $operator_ids['call_center_id'];
                        $modelStandartOperator->operator_id = $operator_id;

                        if (!$modelStandartOperator->save()) {
                            $transactionError = false;
                        }
                    }

                }
            }

            if ($transactionError === true) {
                $transaction->commit();
                Yii::$app->notifier->addNotification(Yii::t('common', 'Операторы успешно сохранены.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                $transaction->rollBack();
                Yii::$app->notifier->addNotificationsByModel($modelStandartOperator);
            }
        }
        /**
         * end save operator_ids
         */

        $offices = Office::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'modelStandartOperator' => new CallCenterStandartOperator(),
            'callCenterStandartOperatorsData' => $this->getOperatorIds($model->id),
            'offices' => $offices,
            'officeId' => $officeId,
            'sources' => $model->isNewRecord ? [] : Source::find()->collection(),
            'sourcesByCallCenter' => $model->isNewRecord ? [] : CallCenter::getSources($model->id),
        ]);
    }

    public function getOperatorIds($call_center_id)
    {
        $query = CallCenterStandartOperator::find()
            ->select([
                'operator_id' => new Expression("GROUP_CONCAT(DISTINCT " . CallCenterStandartOperator::tableName() . ".operator_id ORDER BY " . CallCenterStandartOperator::tableName() . ".operator_id ASC SEPARATOR ', ')"),
                CallCenterStandartOperator::tableName() . '.call_center_id'
            ])
            ->where(['call_center_id' => $call_center_id])
            ->asArray()
            ->all();

        return $query;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Колл-центр успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Колл-центр успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Колл-центр успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['index']);
    }

    /**
     * Ajax установка источника
     * @throws \yii\web\HttpException
     */
    public function actionSetSource()
    {
        $callCenterId = (int)Yii::$app->request->post('callcenter_id');
        $sourceId = Yii::$app->request->post('source_id');

        $callCenter = CallCenter::find()->where(['id' => $callCenterId])->one();

        if (!$callCenter || !$sourceId) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущему источнику или пользователю.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                if ($this->setSourceActive($callCenterId, $sourceId, 1)) {
                    break;
                }
                throw new BadRequestHttpException(Yii::t('common', 'Не удалось разрешить источник'));
            case 'off':
                if ($this->setSourceActive($callCenterId, $sourceId, 0)) {
                    break;
                }
                throw new BadRequestHttpException(Yii::t('common', 'Не удалось запретить источник'));
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param integer $id
     * @return CallCenter
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = CallCenter::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Колл-центр не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return CallCenterStandartOperator
     * @throws HttpException
     */
    private function getModelStandartOperator($call_center_id)
    {
        $model = CallCenterStandartOperator::find()
            ->where([CallCenterStandartOperator::tableName() . '.call_center_id' => $call_center_id])
            ->orderBy([CallCenterStandartOperator::tableName() . '.call_center_id' => SORT_DESC])
            ->all();

        if (!$model) {
            return new CallCenterStandartOperator();
        }

        return $model;
    }

    /**
     * @param $callCenterId
     * @param $sourceId
     * @param $active
     * @return bool
     */
    private function setSourceActive($callCenterId, $sourceId, $active) {
        $source = CallCenterSource::find()
            ->byCallCenterId($callCenterId)
            ->bySourceId($sourceId)
            ->one();
        if (empty($source)) {
            $source = new CallCenterSource();
            $source->call_center_id = $callCenterId;
            $source->source_id = $sourceId;
            $source->created_at = time();
        }

        $source->active = $active;
        $source->updated_at = time();

        return $source->save();
    }
}
