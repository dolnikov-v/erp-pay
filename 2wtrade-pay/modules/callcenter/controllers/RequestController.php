<?php
namespace app\modules\callcenter\controllers;

use app\components\web\Controller;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestLog;
use app\modules\callcenter\models\search\CallCenterRequestSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class RequestController
 * @package app\modules\callcenter\controllers
 */
class RequestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CallCenterRequestSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $callCentersCollection = CallCenter::find()
            ->where([CallCenter::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->collection();

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'callCentersCollection' => $callCentersCollection,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs($id)
    {
        $model = $this->findModel($id);

        $logs = $this->findModelLogs($model->id);

        return $this->render('logs', [
            'logs' => $logs
        ]);
    }

    /**
     * @param $id
     * @return CallCenterRequest
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = CallCenterRequest::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Заявка не найдена.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return CallCenterRequestLog[]
     */
    protected function findModelLogs($id)
    {
        return CallCenterRequestLog::find()
            ->joinWith('user')
            ->where(['call_center_request_id' => $id])
            ->all();
    }

}
