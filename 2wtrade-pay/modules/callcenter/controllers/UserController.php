<?php
namespace app\modules\callcenter\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\search\CallCenterUserSearch;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class UserController
 * @package app\modules\callcenter\controllers
 */
class UserController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-users',
                    'get-parents',
                    'change_team_leader',
                    'activate_users',
                    'deactivate_users',
                    'delete_users',
                ],
            ]
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {

        $callCenters = CallCenter::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->collection();

        /** @var array $callCenters */
        reset($callCenters);
        $firstCallCenter = key($callCenters);

        $inputRequest = Yii::$app->request->queryParams;

        if (!isset($inputRequest['CallCenterUserSearch']['callcenter_id'])) {
            $inputRequest['CallCenterUserSearch']['callcenter_id'] = $firstCallCenter;
        }

        $modelSearch = new CallCenterUserSearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');


        /** @var array $callCenters */
        reset($callCenters);
        $firstCallCenter = key($callCenters);

        $roles = CallCenterUser::getRoles(true);
        $parentUsers = CallCenterUser::getPatentsCollection($firstCallCenter);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'callCenters' => $callCenters,
            'roles' => $roles,
            'parentUsers' => $parentUsers,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $callCenters = CallCenter::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->collection();

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new CallCenterUser();

            /** @var array $callCenters */
            reset($callCenters);
            $model->callcenter_id = key($callCenters);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Пользователь успешно добавлен.') : Yii::t('common', 'Пользователь успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $roles = CallCenterUser::getRoles();
        $parentUsers = CallCenterUser::getPatentsCollection($model->callcenter_id);
        $persons = Person::getCollection();

        return $this->render('edit', [
            'model' => $model,
            'callCenters' => $callCenters,
            'roles' => $roles,
            'parentUsers' => $parentUsers,
            'persons' => $persons
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return CallCenterUser
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = CallCenterUser::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Пользователь не найден.'));
        }

        return $model;
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionGetParents()
    {
        $callCenterId = Yii::$app->request->get('callcenter_id');

        $callCenter = CallCenter::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->byId($callCenterId)
            ->one();

        if ($callCenter) {
            return CallCenterUser::getPatentsCollection($callCenter->id);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Колл-центр не найден.'));
        }
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionGetUsers()
    {
        $callCenterId = Yii::$app->request->get('callcenter_id');
        $personId = Yii::$app->request->get('free_person_id');

        $callCenter = CallCenter::find()
            //->byCountryId(Yii::$app->user->country->id)
            ->byId($callCenterId)
            ->one();

        if ($callCenter) {

            $callCenterUserQuery = CallCenterUser::find()->byCallCenterId($callCenter->id)->active();

            if ($personId) {
                $callCenterUserQuery->byFreePersonId();
            }

            if ($personId && $callCenter->is_amazon_query) {
                // для привязки к сотруднику у которого уже есть логины в новом КЦ, логин должен совпадать
                $onlyOneLoginInNewCallCenter = CallCenterUser::find()
                    ->select('user_login')
                    ->joinWith('callCenter')
                    ->byPersonId($personId)
                    ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 1])
                    ->limit(1)
                    ->scalar();

                if ($onlyOneLoginInNewCallCenter) {
                    $callCenterUserQuery->andWhere(['user_login' => $onlyOneLoginInNewCallCenter]);
                }
            }

            return $callCenterUserQuery->collection();
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Колл-центр не найден.'));
        }
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function actionChangeTeamLeader()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $parent_id = Yii::$app->request->post('teamLeader');

        $teamLeaderCallCenter = CallCenterUser::find()->where(['user_id' => $parent_id])->one();

        $model = CallCenterUser::find()
            ->where(['id' => $id])
            ->andWhere(['callcenter_id' => $teamLeaderCallCenter->callcenter_id])
            ->one();

        //нет прав
        if (!Yii::$app->user->can("callcenter.user.changeteamleader")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        $success = true;

        if (is_null($model)) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось сменить руководителя, т.к. у пользователей разные КЦ');
        } else {
            $model->parent_id = $teamLeaderCallCenter->id;

            if (!$model->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось сменить руководителя');
            }

            if ($success) {
                $response['status'] = 'success';
                $response['message'] = Yii::t('common', 'Операция выполнена успешно');
            }
        }
        echo json_encode($response);
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function actionDeactivateUsers()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = CallCenterUser::findOne($id);

        //нет прав
        if (!Yii::$app->user->can("callcenter.user.deactivateusers")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        $success = true;

        $model->active = CallCenterUser::NOT_ACTIVE;
        $model->user_role = (string)CallCenterUser::USER_ROLE_NOTACTIVE;

        if (!$model->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось деактивировать пользователей');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        echo json_encode($response);
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function actionActivateUsers()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = CallCenterUser::findOne($id);

        //нет прав
        if (!Yii::$app->user->can("callcenter.user.activateusers")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        $success = true;

        $model->active = CallCenterUser::ACTIVE;

        if (!$model->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось активировать пользователей');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        echo json_encode($response);
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function actionDeleteUsers()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = CallCenterUser::findOne($id);

        //нет прав
        if (!Yii::$app->user->can("callcenter.user.deleteusers")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        $success = true;

        if (!$model->delete()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось удалить пользователей');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        echo json_encode($response);
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function actionSetRole()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = CallCenterUser::findOne($id);

        //нет прав
        if (!Yii::$app->user->can("callcenter.user.setrole")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        $success = true;

        $model->user_role = Yii::$app->request->post('role');

        if (!$model->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось установить роль');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        echo json_encode($response);
    }

}
