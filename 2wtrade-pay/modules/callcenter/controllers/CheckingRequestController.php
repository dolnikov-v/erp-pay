<?php

namespace app\modules\callcenter\controllers;

use app\components\web\Controller;
use app\modules\callcenter\models\search\CheckingRequestSearch;
use app\modules\callcenter\models\CallCenterCheckRequestActionLog;
use app\modules\callcenter\models\CallCenterCheckRequestLog;

/**
 * Class CheckingRequestController
 * @package app\modules\delivery\controllers
 */
class CheckingRequestController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new CheckingRequestSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionView(int $id)
    {
        $changeLog = CallCenterCheckRequestLog::find()->where(['request_id' => (int)$id])->orderBy(['created_at' => SORT_DESC])->all();
        $sendLog = CallCenterCheckRequestActionLog::find()->where(['request_id' => (int)$id, 'type' => CallCenterCheckRequestActionLog::TYPE_SEND])->orderBy(['created_at' => SORT_DESC])->all();
        $updateLog = CallCenterCheckRequestActionLog::find()->where(['request_id' => (int)$id, 'type' => CallCenterCheckRequestActionLog::TYPE_UPDATE])->orderBy(['created_at' => SORT_DESC])->all();

        return $this->render('view', [
            'changeLog' => $changeLog,
            'sendLog' => $sendLog,
            'updateLog' => $updateLog,
        ]);
    }
}
