<?php

namespace app\modules\checklist\models;

use app\modules\checklist\models\query\CheckListItemQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "check_list_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $position
 * @property integer $days
 * @property integer $active
 * @property integer $auto
 * @property integer $explainable
 * @property string $trigger
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CheckList[] $checkLists
 * @property CheckListCategory $category
 */
class CheckListItem extends ActiveRecordLogUpdateTime
{

    const TYPE_PAY = 'pay';
    const TYPE_CALL = 'call';
    const TYPE_AUDITOR = 'auditor';
    const TYPE_TM = 'tm';

    const NO_TRIGGER = '';
    const TRIGGER_ONE = 'one';
    const TRIGGER_TWO = 'two';
    const TRIGGER_THREE = 'three';
    const TRIGGER_FOUR = 'four';
    const TRIGGER_FIVE = 'five';
    const TRIGGER_SIX = 'six';
    const TRIGGER_SEVEN = 'seven';
    const TRIGGER_EIGHT = 'eight';
    const TRIGGER_NINE = 'nine';
    const TRIGGER_TEN = 'ten';
    const TRIGGER_ELEVEN = 'eleven';
    const TRIGGER_TWELVE = 'twelve';
    const TRIGGER_THIRTEEN = 'thirteen';
    const TRIGGER_FOURTEEN = 'fourteen';
    const TRIGGER_FIFTEEN = 'fifteen';
    const TRIGGER_SIXTEEN = 'sixteen';
    const TRIGGER_SEVENTEEN = 'seventeen';
    const TRIGGER_EIGHTEEN = 'eighteen';
    const TRIGGER_TWENTY_SIX = 'twenty_six';

    public $checkedLists;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%check_list_item}}';
    }

    /**
     * @return CheckListItemQuery
     */
    public static function find()
    {
        return new CheckListItemQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getTriggersCollection()
    {
        return [
            self::NO_TRIGGER => '-',
            self::TRIGGER_ONE => Yii::t('common', '1. Нет заказов в статусах 1,2 созданных более 5 минут назад'),
            self::TRIGGER_TWO => Yii::t('common', '2. Нет заказов в статусах 1,2,3. Средний чек аппрувов выше 65 USD. Аппрув от свежих лидов не менее 35 %'),
            self::TRIGGER_THREE => Yii::t('common', '3. Нет заказов в статусах 6,19,31'),
            self::TRIGGER_FOUR => Yii::t('common', '4. КС имеет достаточное кол-во товара на 21 день при текущем траффике'),
            self::TRIGGER_FIVE => Yii::t('common', '5. Сводная таблица по времени последнего обновления данных в отчете Доставка'),
            self::TRIGGER_SIX => Yii::t('common', '6. Выкуп за последние 7 дней больше аналогичного показателя 24 часа назад, если меньше то дано объяснение'),
            self::TRIGGER_SEVEN => Yii::t('common', '7. Долг каждой курьерки по COD не более месяца и не более 10 000 USD'),
            self::TRIGGER_EIGHT => Yii::t('common', '8. Заказы в статусах 16,17,27 отправлены на проверку в КЦ'),
            self::TRIGGER_NINE => Yii::t('common', '9. Есть сертификаты на все товары продаваемые в стране'),
            self::TRIGGER_TEN => Yii::t('common', '10. Проводились корректировки товара на складе за последние 7 дней'),
            self::TRIGGER_ELEVEN => Yii::t('common', '11. Невыкупов по причине проблем с адресом не более 2%'),
            self::TRIGGER_TWELVE => Yii::t('common', '12. Невыкупов по причине «клиент не заказывал» не более 2%'),
            self::TRIGGER_THIRTEEN => Yii::t('common', '13. Все активные сотрудники в ЗП проекте имеют полностью заполненные данные'),
            self::TRIGGER_FOURTEEN => Yii::t('common', '14. Все уволенные операторы, в ЗП проекте отмечены как уволенные'),
            self::TRIGGER_FIFTEEN => Yii::t('common', '15. Продаваемые товары имеют все не обходимые данные для работы системы'),
            self::TRIGGER_SIXTEEN => Yii::t('common', '16. Все отправленные более 30 дней назад заказы доставлены и деньги получены'),
            self::TRIGGER_SEVENTEEN => Yii::t('common', '17. Нет открытых задач в Jira в проекте Order editing requests'),
            self::TRIGGER_EIGHTEEN => Yii::t('common', '18. На конец рабочего дня в очереди заказов с проблемами адреса (вернувшихся от КС) должно быть пусто'),
            self::TRIGGER_TWENTY_SIX => Yii::t('common', '26. Колл-центр оборудован микротиком, видеокамерами и 2 линиями интернета'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypeCollection()
    {
        return [
            self::TYPE_PAY => Yii::t('common', '2WTRADE'),
            self::TYPE_CALL => Yii::t('common', 'Колл Центр'),
            self::TYPE_AUDITOR => Yii::t('common', 'Аудитор КЦ'),
            self::TYPE_TM => Yii::t('common', 'Территориальный Менеджер'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'created_at', 'updated_at', 'days', 'category_id'], 'integer'],
            [['description', 'trigger', 'type'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['days'], 'default', 'value' => 1],
            [['days'], 'integer', 'min' => 1, 'max' => 999],
            [['active', 'auto', 'explainable'], 'boolean'],
            [['active'], 'default', 'value' => true],
            [['auto', 'explainable'], 'default', 'value' => false],
            ['trigger', 'default', 'value' => self::NO_TRIGGER],
            ['type', 'default', 'value' => self::TYPE_PAY],
            ['trigger', 'in', 'range' => array_keys(self::getTriggersCollection())],
            ['type', 'in', 'range' => array_keys(self::getTypeCollection())],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckListCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Подсказка'),
            'position' => Yii::t('common', 'Позиция'),
            'days' => Yii::t('common', 'Периодичность, дней'),
            'active' => Yii::t('common', 'Активность'),
            'auto' => Yii::t('common', 'Автоматический'),
            'explainable' => Yii::t('common', 'Закрытие с объяснением причины'),
            'trigger' => Yii::t('common', 'Автоматический триггер'),
            'type' => Yii::t('common', 'Тип'),
            'category_id' => Yii::t('common', 'Категория'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckLists()
    {
        return $this->hasMany(CheckList::className(), ['check_list_item_id' => 'id']);
    }

    public function beforeDelete()
    {
        if ($this->checkLists) {
            $this->addError('id', Yii::t('common', 'Нельзя удалить пункт, т.к. по нему есть отметки, снимите активность'));
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CheckListCategory::className(), ['id' => 'category_id']);
    }

}
