<?php
namespace app\modules\checklist\models\query;

use app\components\db\ActiveQuery;
use app\modules\checklist\models\CheckListCategory;

/**
 * Class CheckListCategoryQuery
 * @package app\modules\checklist\models\query
 * @method CheckListCategory one($db = null)
 * @method CheckListCategory[] all($db = null)
 */
class CheckListCategoryQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([CheckListCategory::tableName() . '.active' => 1]);
    }

    /**
     * @param $type string
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere([CheckListCategory::tableName() . '.type' => $type]);
    }

    /**
     * @return $this
     */
    public function sortPosition()
    {
        return $this->orderBy([CheckListCategory::tableName() . '.position' => SORT_ASC]);
    }
}
