<?php
namespace app\modules\checklist\models\query;

use app\components\db\ActiveQuery;
use app\modules\checklist\models\CheckList;

/**
 * Class CheckListQuery
 * @package app\modules\checklist\models\query
 * @method CheckList one($db = null)
 * @method CheckList[] all($db = null)
 */
class CheckListQuery extends ActiveQuery
{
    /**
     * @param integer $id
     * @return $this
     */
    public function byUserId($id)
    {
        return $this->andWhere([CheckList::tableName() . '.user_id' => $id]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byTeamId($id)
    {
        return $this->andWhere([CheckList::tableName() . '.team_id' => $id]);
    }

    /**
     * @param integer | array $id
     * @return $this
     */
    public function byCountry($id)
    {
        return $this->andWhere([CheckList::tableName() . '.country_id' => $id]);
    }

    /**
     * @return $this
     */
    public function isCommented()
    {
        return $this->andWhere(
            ['and',
                ['is not', CheckList::tableName() . '.comment', null],
                ['<>', CheckList::tableName() . '.comment', ''],
            ]
        );
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byItem($id)
    {
        return $this->andWhere([CheckList::tableName() . '.check_list_item_id' => $id]);
    }

    /**
     * @param integer $days
     * @param string $selectedDate
     * @return $this
     */
    public function byDate($days, $selectedDate = '')
    {
        $selectedTime = time();
        if ($selectedDate) {
            $selectedTime = strtotime($selectedDate);
        }
        if ($days == 1) {
            $selectedDate = date("Y-m-d", $selectedTime);
            return $this->andWhere([CheckList::tableName() . '.date' => $selectedDate]);
        }
        $dateFrom = date("Y-m-d", $selectedTime - ($days - 1) * 86400);
        return $this->andWhere(['between', CheckList::tableName() . '.date', $dateFrom, date('Y-m-d', $selectedTime)]);
    }

    /**
     * @param integer | array $id
     * @return $this
     */
    public function byOfficeId($id)
    {
        return $this->andWhere([CheckList::tableName() . '.office_id' => $id]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byTeamleadId($id)
    {
        if ($id) {
            return $this->andWhere([CheckList::tableName() . '.teamlead_id' => $id]);
        }
        return $this;
    }
}
