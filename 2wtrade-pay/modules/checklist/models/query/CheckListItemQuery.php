<?php
namespace app\modules\checklist\models\query;

use app\components\db\ActiveQuery;
use app\modules\checklist\models\CheckListItem;
use Yii;

/**
 * Class CheckListQuery
 * @package app\modules\checklist\models\query
 * @method CheckListItem one($db = null)
 * @method CheckListItem[] all($db = null)
 */
class CheckListItemQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([CheckListItem::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function auto()
    {
        return $this->andWhere([CheckListItem::tableName() . '.auto' => 1]);
    }

    /**
     * @param $trigger string
     * @return $this
     */
    public function byTrigger($trigger)
    {
        return $this->andWhere([CheckListItem::tableName() . '.trigger' => $trigger]);
    }

    /**
     * @param $type string
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere([CheckListItem::tableName() . '.type' => $type]);
    }

    /**
     * @return $this
     */
    public function sortPosition()
    {
        return $this->orderBy([CheckListItem::tableName() . '.position' => SORT_ASC]);
    }
}
