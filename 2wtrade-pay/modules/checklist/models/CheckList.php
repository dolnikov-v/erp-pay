<?php

namespace app\modules\checklist\models;

use app\models\Country;
use app\models\User;
use app\modules\checklist\models\query\CheckListQuery;
use app\modules\reportoperational\models\Team;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "check_list".
 *
 * @property integer $id
 * @property integer $check_list_item_id
 * @property integer $country_id
 * @property string $date
 * @property string $status
 * @property integer $user_id
 * @property integer $team_id
 * @property string $data_info
 * @property string $comment
 * @property integer $office_id
 * @property integer $teamlead_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CheckListItem $checkListItem
 * @property Country $country
 * @property User $updatedBy
 * @property Team $team
 * @property Office $office
 * @property Person $teamlead
 */
class CheckList extends ActiveRecordLogUpdateTime
{
    const STATUS_NEW = '';
    const STATUS_OK = 'ok';
    const STATUS_NO = 'no';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%check_list}}';
    }

    /**
     * @return CheckListQuery
     */
    public static function find()
    {
        return new CheckListQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_NEW => Yii::t('common', ''),
            self::STATUS_OK => Yii::t('common', '+'),
            self::STATUS_NO => Yii::t('common', '-'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['check_list_item_id', 'country_id', 'user_id', 'team_id', 'office_id', 'teamlead_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['comment', 'data_info'], 'string'],
            [['check_list_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckListItem::className(), 'targetAttribute' => ['check_list_item_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['teamlead_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['teamlead_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'check_list_item_id' => Yii::t('common', 'Пункт'),
            'country_id' => Yii::t('common', 'Страна'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'date' => Yii::t('common', 'Дата'),
            'status' => Yii::t('common', 'Статус'),
            'team_id' => Yii::t('common', 'Регион'),
            'office_id' => Yii::t('common', 'Физический офис КЦ'),
            'teamlead_id' => Yii::t('common', 'Тимлид'),
            'comment' => Yii::t('common', 'Комментарий'),
            'data_info' => Yii::t('common', 'Данные'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckListItem()
    {
        return $this->hasOne(CheckListItem::className(), ['id' => 'check_list_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamlead()
    {
        return $this->hasOne(Person::className(), ['id' => 'teamlead_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if (is_a(Yii::$app, 'yii\web\Application')) {
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }
}