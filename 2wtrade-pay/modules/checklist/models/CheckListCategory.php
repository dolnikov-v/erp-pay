<?php

namespace app\modules\checklist\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\checklist\models\query\CheckListCategoryQuery;
use Yii;

/**
 * This is the model class for table "check_list_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $active
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CheckListItem[] $checkListItems
 */
class CheckListCategory extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%check_list_category}}';
    }

    /**
     * @return CheckListCategoryQuery
     */
    public static function find()
    {
        return new CheckListCategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'created_at', 'updated_at'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
            [['active'], 'boolean'],
            [['active'], 'default', 'value' => true],
            ['type', 'in', 'range' => array_keys(CheckListItem::getTypeCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'type' => Yii::t('common', 'Тип'),
            'active' => Yii::t('common', 'Активность'),
            'position' => Yii::t('common', 'Позиция'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckListItems()
    {
        return $this->hasMany(CheckListItem::className(), ['category_id' => 'id']);
    }
}
