<?php

namespace app\modules\checklist\models\search;

use app\modules\checklist\models\CheckList;
use Yii;

/**
 * Class CheckListSearch
 * @package modules\checklist\models\search
 */
class CheckListSearch extends CheckList
{
    public $date;
    public $user_id;
    public $team_id;
    public $office_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'user_id', 'team_id', 'office_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'team_id' => Yii::t('common', 'Регион'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'office_id' => Yii::t('common', 'Офис'),
        ];
    }
}
