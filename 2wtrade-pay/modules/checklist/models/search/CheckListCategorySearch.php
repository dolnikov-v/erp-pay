<?php

namespace app\modules\checklist\models\search;

use app\modules\checklist\models\CheckListCategory;
use app\modules\checklist\models\CheckListItem;
use yii\data\ActiveDataProvider;

/**
 * Class CheckListCategorySearch
 * @package modules\checklist\models\search
 */
class CheckListCategorySearch extends CheckListCategory
{
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = CheckListCategory::find()->orderBy(['position' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!$this->type) {
            $this->type = CheckListItem::TYPE_PAY;
        }

        $query->byType($this->type);

        return $dataProvider;
    }
}
