<?php
namespace app\modules\checklist\components;

use app\components\jira\Jira;
use app\models\Certificate;
use app\models\CertificateFile;
use app\models\CurrencyRate;
use app\models\Language;
use app\models\Product;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterToOffice;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use yii\db\Expression;
use yii\db\Query;
use app\modules\report\extensions\Query as ReportQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\StorageDocument;
use app\models\Country;
use app\modules\reportoperational\models\TeamCountry;
use yii\helpers\ArrayHelper;

class AutoCheck extends \yii\base\Component
{
    /**
     * @var array
     */
    public static $currencies;

    /**
     * @var string
     */
    public static $type = CheckListItem::TYPE_PAY;

    /**
     * Проверить пункт
     *
     * @param mixed $item
     * @param integer|null $countryId
     * @param integer|null $teamId
     * @param string $comment
     * @return array
     */
    public static function check($item, $countryId = null, $teamId = null, $comment = '')
    {
        $errors = [];

        // проверяемый пункт
        $selectedItems = []; // при Cron запуске теперь может быть сразу несколько пунктов с одним тригерром

        if ($item instanceof CheckListItem) {
            $selectedItems[] = $item;
        } else {
            if (is_integer($item)) {
                $selectedItems[] = CheckListItem::findOne($item);
            } else {
                $selectedItems = CheckListItem::find()->byTrigger($item)->active()->auto()->all();
            }
        }

        if (!$selectedItems) {
            $errors[] = 'no check item found';
            return [
                'status' => false,
                'errors' => $errors
            ];
        }

        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);

            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }

        // страны команд
        $query = TeamCountry::find()->asArray();
        if ($countryId) {
            $query->andWhere(['country_id' => $countryId]);
        }
        if ($teamId) {
            $query->andWhere(['team_id' => $teamId]);
        }
        $teamCountries = ArrayHelper::map($query->all(), 'country_id', 'team_id');

        $teamCountriesIds = array_keys($teamCountries);

        $date = date('Y-m-d');

        // исключить уже откомментированные, только если не ручной запуск
        foreach ($selectedItems as $selectedItem) {
            if ($selectedItem->explainable && is_array($teamCountries) && !$countryId && !$teamId) {
                $commentedCheckList = CheckList::find()
                    ->byDate($selectedItem->days, $date)
                    ->byItem($selectedItem->id)
                    ->byCountry($teamCountriesIds)
                    ->isCommented();
                $commentedCountries = ArrayHelper::map($commentedCheckList->all(), 'country_id', 'country_id');

                if (is_array($commentedCountries)) {
                    foreach ($commentedCountries as $commentedCountry) {
                        unset($teamCountries[$commentedCountry]);
                    }
                }
            }
        }

        // получить критерии в массив типа $totalCounts[$countryID] = 5 (число косяков)
        // или = array (массив из проблем, где число элементов это число проблем)
        // или = array (масиив из всех данных с проблемами, где есть cnt - число проблем, orders - пример проблемных заказов / первые 1024 символ)

        $trigger = $selectedItems[0]->trigger ?? '';    // теперь может быть массив, но тригер одинаковый

        $totalCounts = self::switchTotals($trigger, $teamCountriesIds);

        if ($totalCounts === false) {
            $errors[] = 'no function';
            return [
                'status' => false,
                'errors' => $errors
            ];
        }

        $count = 0;
        $info = [];

        // в некоторых случаях $selectedItems массив, но обычно из одного элемента $selectedItems[0]
        foreach ($selectedItems as $selectedItem) {

            foreach ($teamCountries as $countryID => $teamId) {

                $totalCount = $totalCounts[$countryID] ?? 0;

                $dataInfo = [];
                if ($trigger == CheckListItem::TRIGGER_SIX) {

                    // сохранить текущее значение выкупа
                    $dataInfo['buyout'] = $totalCounts[$countryID] ?? 0;

                    // выберем показатель прошлого дня
                    $prevCheckList = CheckList::find()
                        ->byTeamId($teamId)
                        ->byDate($selectedItem->days, date('Y-m-d', strtotime('-1day')))
                        ->byItem($selectedItem->id)
                        ->byCountry($countryID)
                        ->one();

                    $prevDataInfo = ($prevCheckList && $prevCheckList->data_info) ? json_decode($prevCheckList->data_info, true) : [];
                    // выкуп прошлого дня
                    $prevDataInfo['buyout'] = $prevDataInfo['buyout'] ?? 0;

                    // сравнить выкуп с показателями за вчерашний день
                    // 0 - нет проблемы, 1 - проблема
                    $totalCount = ($dataInfo['buyout'] > $prevDataInfo['buyout']) ? [] : [
                        'buyout' => $dataInfo['buyout'],
                        'prevBuyout' => $prevDataInfo['buyout'],
                    ];
                }

                if ($totalCount) {

                    if (is_array($totalCount)) {
                        $dataInfo['details'] = $totalCount;
                        $info[] = sizeof($totalCount);
                    } else {
                        $info[] = $totalCount;
                    }
                }

                if (!$teamId) {
                    $errors[] = 'no team ID for country';
                    continue;
                }

                $checkList = CheckList::find()
                    ->byTeamId($teamId)
                    ->byDate($selectedItem->days, $date)
                    ->byItem($selectedItem->id)
                    ->byCountry($countryID)
                    ->one();

                if (!$checkList instanceof CheckList) {
                    $checkList = new CheckList();
                    $checkList->team_id = $teamId;
                    $checkList->date = $date;
                    $checkList->check_list_item_id = $selectedItem->id;
                    $checkList->country_id = $countryID;
                }
                $checkList->status = $totalCount ? CheckList::STATUS_NO : CheckList::STATUS_OK;

                if ($selectedItem->explainable) {
                    $checkList->comment = $comment;
                    if ($checkList->comment && $totalCount) {
                        // пропускаем откомментированный пункт
                        $checkList->status = CheckList::STATUS_OK;
                        $info = [];
                    }
                }

                $checkList->data_info = $dataInfo ? json_encode($dataInfo) : null;
                $checkList->updated_at = time();

                if (!$checkList->save()) {
                    $errors[] = $checkList->getFirstErrorAsString();
                } else {
                    $count++;
                }
            }

        }
        return [
            'status' => $info ? false : true,
            'errors' => $errors,
            'count' => $count,
            'info' => $info
        ];
    }


    /**
     * @param $trigger
     * @param $teamCountriesIds
     * @return array
     */
    public static function switchTotals($trigger, $teamCountriesIds) {
        switch ($trigger) {
            case CheckListItem::TRIGGER_ONE:
                $totalCounts = self::getTotalsOne($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_TWO:
                $totalCounts = self::getTotalsTwo($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_THREE:
                $totalCounts = self::getTotalsThree($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_FOUR:
                $totalCounts = self::getTotalsFour($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_FIVE:
                $totalCounts = self::getTotalsFive($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_SIX:
                $totalCounts = self::getTotalsSix($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_SEVEN:
                $totalCounts = self::getTotalsSeven($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_EIGHT:
                $totalCounts = self::getTotalsEight($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_NINE:
                $totalCounts = self::getTotalsNine($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_TEN:
                $totalCounts = self::getTotalsTen($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_ELEVEN:
                $totalCounts = self::getTotalsUnBuyoutReason($teamCountriesIds, 'address');
                break;
            case CheckListItem::TRIGGER_TWELVE:
                $totalCounts = self::getTotalsUnBuyoutReason($teamCountriesIds, 'client');
                break;
            case CheckListItem::TRIGGER_THIRTEEN:
                $totalCounts = self::getTotalsSalaryPersonsNotComplete($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_FOURTEEN:
                $totalCounts = self::getTotalsSalaryPersonsNotActive($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_FIFTEEN:
                $totalCounts = self::getTotalsProductsNotComplete($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_SIXTEEN:
                $totalCounts = self::getTotalsOrdersInProcess($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_SEVENTEEN:
                $totalCounts = self::getTotalsJiraTask($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_EIGHTEEN:
                $totalCounts = self::getTotalsOrdersRejected($teamCountriesIds);
                break;
            case CheckListItem::TRIGGER_TWENTY_SIX:
                $totalCounts = self::getOfficesNoTechnicalEquipment($teamCountriesIds);
                break;
            default:
                $totalCounts = false;
                break;
        }
        return $totalCounts;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsOne($countryIds)
    {
        $timeOffset = 5 * 60; // 5 минут

        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => 'COUNT(' . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
        ]);
        $query->where(['<', Order::tableName() . '.created_at', time() - $timeOffset]);
        $query->andWhere([Order::tableName() . '.status_id' => [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM
        ]]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);
        return self::indexCountryOrders($query->all());
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsTwo($countryIds)
    {
        // TODO может потом вынести эти цифры в конфиг/настройки
        $timeOffset = 60 * 60; // 5 минут
        $avgCheck = 65; // средний чек
        $approvePercent = 35; // процент апрува

        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => "COUNT(" . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
        ]);
        $query->where(['<', Order::tableName() . '.created_at', time() - $timeOffset]);
        $query->andWhere([Order::tableName() . '.status_id' => [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL,
        ]]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);

        $totals = self::indexCountryOrders($query->all());

        $query = new ReportQuery();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
        ]);
        $query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getApproveList(), 'approve');
        $query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getAllList(), 'total');

        $case_approve = $query->buildCaseCondition(
            Order::tableName() . '.price_total/ ' . CurrencyRate::tableName() . '.rate',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );
        $query->addAvgCondition($case_approve, 'check');

        $query->leftJoin(Country::tableName(), Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
        $query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        $query->where(['<', Order::tableName() . '.created_at', time() - $timeOffset]);
        $query->andWhere(['>=', Order::tableName() . '.created_at', strtotime('today midnight')]);
        $query->andWhere([Country::tableName() . '.id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);

        $approves = $query->all();
        foreach ($approves as $approve) {

            if (is_null($approve['check'])) {
                $approve['check'] = 0;
            }

            if ($approve['check'] < $avgCheck) {
                $totals[$approve['country_id']]['avg_check'] = $approve['check'];
            }

            $percent = ($approve['total'] ? $approve['approve'] / $approve['total'] * 100 : 0);
            if ($percent < $approvePercent) {
                $totals[$approve['country_id']]['approve_percent'] = $percent;
            }
        }
        return $totals;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsThree($countryIds)
    {
        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => "COUNT(" . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
        ]);
        $query->where([Order::tableName() . '.status_id' => [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_PENDING,
        ]]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);
        return self::indexCountryOrders($query->all());
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsFour($countryIds)
    {
        $period = 7; // 7 дней
        $minDays = 21; // минимум дней, на сколько должно хватить товара
        $query = new Query();
        $query->from(StorageProduct::tableName());
        $query->select([
            'country_id' => Storage::tableName() . '.country_id',
            'delivery_id' => Storage::tableName() . '.delivery_id',
            'product_id' => StorageProduct::tableName() . '.product_id',
            'balance' => "SUM(" . StorageProduct::tableName() . '.balance)',
            // возьмем по каждой стране число товаров за неделю, которые ушли в КС / 7 дней
            'products_day' => 'products.sum'
        ]);
        $query->leftJoin(Storage::tableName(), Storage::tableName() . '.id = ' . StorageProduct::tableName() . '.storage_id');
        $query->where(['is not', Storage::tableName() . '.delivery_id', null]);
        $query->andWhere([Storage::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Storage::tableName() . '.country_id', Storage::tableName() . '.delivery_id', StorageProduct::tableName() . '.product_id']);

        $subQuery = OrderProduct::find()
            ->joinWith(['order', 'order.deliveryRequest'], false)
            ->select(['sum' => 'SUM(' . OrderProduct::tableName() . '.quantity / ' . $period . ')', 'delivery_id' => DeliveryRequest::tableName().'.delivery_id', 'product_id' => OrderProduct::tableName().'.product_id'])
            ->andWhere(['>=', Order::tableName() . '.created_at', (time() - $period * 86400)])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getInDeliveryAndFinanceList()])
            ->groupBy([DeliveryRequest::tableName() . '.delivery_id', OrderProduct::tableName() . '.product_id']);

        $query->leftJoin(['products' => $subQuery], "products.delivery_id = " . Storage::tableName() . '.delivery_id AND products.product_id = ' . StorageProduct::tableName() . '.product_id AND ' . StorageProduct::tableName() . '.unlimit < 1');

        $dataArray = $query->all();
        $return = null;
        if (is_array($dataArray)) {
            foreach ($dataArray as $dataItem) {
                if (is_null($dataItem['products_day'])) {
                    $dataItem['products_day'] = 0;
                }
                if (!isset($dataItem['products_day']) || $dataItem['products_day'] == 0) {
                    continue;
                }
                $days = $dataItem['balance'] / $dataItem['products_day'];
                if ($days < $minDays) {
                    $return[$dataItem['country_id']][] = [
                        'days' => $days,
                        'delivery' => $dataItem['delivery_id'],
                        'product' => $dataItem['product_id'],
                    ];
                }
            }
        }
        return $return;
    }

    /**
     * Считаем выкуп за последние 7 дней
     * @param array $countryIds
     * @return array массив с выкупом по стране
     */
    public static function getTotalsSix($countryIds)
    {
        $query = new ReportQuery();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
        ]);
        $query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getBuyoutList(), 'buyout');
        $query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getAllList(), 'total');
        $query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->andWhere(['>=', DeliveryRequest::tableName() . '.sent_at', strtotime('-7 days', strtotime('midnight'))]);
        $query->groupBy([Order::tableName() . '.country_id']);

        $buyouts = $query->all();

        $totals = array_fill_keys($countryIds, 0);
        foreach ($buyouts as $buyout) {
            $totals[$buyout['country_id']] = $buyout['total'] ? $buyout['buyout'] / $buyout['total'] * 100 : 0;
        }
        return $totals;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsSeven($countryIds)
    {
        $limitDate = time() - 30 * 86400; // 30 дней - разграничение между старыми и новыми
        $limitSum = 10000; // лимит долга одной КС в стране
        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'cnt' => 'SUM(IFNULL(' . Order::tableName() . '.price_total, 0) + IFNULL(' . Order::tableName() . '.delivery, 0))',
        ])
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT])
            ->andWhere([Order::tableName() . '.country_id' => $countryIds])
            ->andWhere(['<=', Order::tableName() . '.created_at', $limitDate])
            ->groupBy([Order::tableName() . '.country_id', DeliveryRequest::tableName() . '.delivery_id']);

        $dataOld = $query->all();
        $dataResult = null;
        if (is_array($dataOld)) {
            foreach ($dataOld as $dataOldItem) {
                $sum = 0;
                if (isset(self::$currencies[$dataOldItem['country_id']]) && self::$currencies[$dataOldItem['country_id']] > 0) {
                    $sum = $dataOldItem['cnt'] / self::$currencies[$dataOldItem['country_id']];
                }
                if ($sum > 0) {
                    $dataResult[$dataOldItem['country_id']][$dataOldItem['delivery_id']] = [
                        'delivery' => $dataOldItem['delivery_id'],
                        'old' => $sum
                    ];
                }
            }
        }

        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'cnt' => 'SUM(IFNULL(' . Order::tableName() . '.price_total, 0) + IFNULL(' . Order::tableName() . '.delivery, 0))',
        ])
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT])
            ->andWhere([Order::tableName() . '.country_id' => $countryIds])
            ->andWhere(['>', Order::tableName() . '.created_at', $limitDate])
            ->groupBy([Order::tableName() . '.country_id', DeliveryRequest::tableName() . '.delivery_id']);

        $dataNew = $query->all();
        if (is_array($dataNew)) {
            foreach ($dataNew as $dataNewItem) {
                $sum = 0;
                if (isset(self::$currencies[$dataNewItem['country_id']]) && self::$currencies[$dataNewItem['country_id']] > 0) {
                    $sum = $dataNewItem['cnt'] / self::$currencies[$dataNewItem['country_id']];
                }
                if ($sum > $limitSum) {
                    $dataResult[$dataNewItem['country_id']][$dataNewItem['delivery_id']] = [
                        'delivery' => $dataNewItem['delivery_id'],
                        'new' => $sum
                    ];
                }
            }
        }
        return $dataResult;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsFive($countryIds)
    {
        $query = DeliveryRequest::find()->joinWith('delivery.country');
        $query->select([
            'country_id' => Country::tableName() . '.id',
            'delivery_id' => Delivery::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'delivery_name' => Delivery::tableName() . '.name',
            'last_api_update' => "MIN(" . DeliveryRequest::tableName() . '.cron_launched_at)',
            'last_report_update' => 'MIN(COALESCE(' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
        ]);
        $query->where([
            DeliveryRequest::tableName() . '.status' => [DeliveryRequest::STATUS_IN_PROGRESS, DeliveryRequest::STATUS_IN_LIST],
            Delivery::tableName() . '.active' => 1,
            Country::tableName() . '.active' => 1,
        ]);
        $query->andWhere([Country::tableName() . '.id' => $countryIds]);
        $query->groupBy([DeliveryRequest::tableName() . '.delivery_id']);

        $models = $query->createCommand()->queryAll();
        $models = ArrayHelper::index($models, 'delivery_id');

        $query = DeliveryReport::find()->joinWith(['country', 'delivery']);
        $query->select([
            'country_id' => Country::tableName() . '.id',
            'delivery_id' => Delivery::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'delivery_name' => Delivery::tableName() . '.name',
            'last_report_update' => "MAX(COALESCE(" . DeliveryReport::tableName() . '.period_to, ' . DeliveryReport::tableName() . '.created_at))',
            'last_api_update' => new Expression('NULL'),
        ]);
        $query->where([
            Delivery::tableName() . '.active' => 1,
            Country::tableName() . '.active' => 1,
        ]);
        $query->andWhere([Country::tableName() . '.id' => $countryIds]);
        $query->groupBy([DeliveryReport::tableName() . '.delivery_id']);

        $reportModels = $query->createCommand()->queryAll();
        foreach ($reportModels as $model) {
            if (isset($models[$model['delivery_id']])) {
                $models[$model['delivery_id']]['last_report_update'] = $model['last_report_update'];
            } else {
                $models[] = $model;
            }
        }

        $return = [];
        foreach ($models as $model) {
            $badDelivery = true;
            if (($model['last_api_update'] && $model['last_api_update'] > strtotime('-3 days', strtotime('midnight'))) ||
                ($model['last_report_update'] && $model['last_report_update'] > strtotime('-7 days', strtotime('midnight')))
            ) {
                $badDelivery = false;
            }
            if ($badDelivery) {
                $return[$model['country_id']][] = [
                    'delivery' => $model['delivery_id'],
                    'last_api_update' => ($model['last_api_update'] && $model['last_api_update'] < strtotime('-3 days', strtotime('midnight')) ? $model['last_api_update'] : ''),
                    'last_report_update' => ($model['last_report_update'] && $model['last_report_update'] < strtotime('-7 days', strtotime('midnight')) ? $model['last_report_update'] : ''),
                ];
            }
        }
        return $return;
    }

    /**
     * Все возвраты (статус 16,17,27) за последние 3 дня (DeliveryRequest.returned_at) прозвонены КЦ (CallCenterCheckRequest.check_undelivery done)
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsEight($countryIds)
    {
        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => "COUNT(" . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
            'checked' => 'if(' . CallCenterCheckRequest::tableName() . '.id,1,0)',
        ]);
        $query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
        $query->leftJoin(CallCenterCheckRequest::tableName(),
            CallCenterCheckRequest::tableName() . '.order_id = ' . Order::tableName() . '.id and ' .
            CallCenterCheckRequest::tableName() . '.type="' . CallCenterCheckRequest::TYPE_CHECKUNDELIVERY . '" and ' .
            CallCenterCheckRequest::tableName() . '.status="' . CallCenterCheckRequest::STATUS_DONE . '"');
        $query->where([Order::tableName() . '.status_id' => [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND,
        ]]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->andWhere(
            [
                'or',
                ['>=', DeliveryRequest::tableName() . '.returned_at', strtotime('-3 days', strtotime('midnight'))],
                ['>=', DeliveryRequest::tableName() . '.approved_at', strtotime('-3 days', strtotime('midnight'))],
                ['>=', DeliveryRequest::tableName() . '.done_at', strtotime('-3 days', strtotime('midnight'))]
            ]
        );
        $query->groupBy([Order::tableName() . '.country_id', 'checked']);
        $query->having(['checked' => 0]);
        return self::indexCountryOrders($query->all());
    }

    /**
     * Есть сертификаты (есть файл сертификата или партнерский сертификат или галка сертификат не нужен) на все товары продаваемые в стране (наличие товаров на складе).
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsNine($countryIds)
    {
        $query = new Query();
        $query->from(StorageProduct::tableName());
        $query->select([
            'country_id' => Storage::tableName() . '.country_id',
            'product_id' => StorageProduct::tableName() . '.product_id',
            'has' => 'IF(' . Certificate::tableName() . '.is_partner=1 OR 
                         ' . Certificate::tableName() . '.certificate_not_needed = 1 OR
                         ' . CertificateFile::tableName() . '.id,1,0)',
        ]);
        $query->leftJoin(Storage::tableName(), StorageProduct::tableName() . '.storage_id=' . Storage::tableName() . '.id');
        $query->leftJoin(Certificate::tableName(),
            Certificate::tableName() . '.product_id=' . StorageProduct::tableName() . '.product_id and ' .
            Certificate::tableName() . '.country_id=' . Storage::tableName() . '.country_id');
        $query->leftJoin(CertificateFile::tableName(), CertificateFile::tableName() . '.product_certificate_id=' . Certificate::tableName() . '.id');
        $query->andWhere([Storage::tableName() . '.country_id' => $countryIds]);
        $query->andWhere(['>', StorageProduct::tableName() . '.balance', 0]);
        $query->groupBy([
            Storage::tableName() . '.country_id',
            StorageProduct::tableName() . '.product_id',
            'has'
        ]);
        $query->having(['has' => 0]);

        $return = $tmp = [];
        foreach ($query->all() as $item) {
            $tmp[$item['country_id']][] = $item['product_id'];
        }
        foreach ($tmp as $key => $value) {
            $return[$key] = [
                'cnt' => sizeof($value),
                'products' => implode(',', $value),
            ];
        }
        return $return;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsTen($countryIds)
    {
        $minDays = 7; // min дней с момента последней инвентаризации склада
        $activeDeliveries = Delivery::find()
            ->select('id')
            ->active()
            ->andWhere(['id' => new Expression(Storage::tableName() . '.delivery_id')])
            ->limit(1);

        $query = new Query();
        $query->from(StorageDocument::tableName());
        $query->select([
            'country_id' => Storage::tableName() . '.country_id',
            'storage_id' => Storage::tableName() . '.id',
            'max_date' => 'MAX(' . StorageDocument::tableName() . '.created_at)',
        ]);
        $query->where([StorageDocument::tableName() . '.type' => [
            StorageDocument::TYPE_MOVEMENT,
            StorageDocument::TYPE_CORRECTION_POSITIVE,
            StorageDocument::TYPE_CORRECTION_NEGATIVE,
            StorageDocument::TYPE_DELETE,
        ]]);

        $query->andWhere([
            'or',
            [Storage::tableName() . '.delivery_id' => null],
            ['exists', $activeDeliveries],
        ]);
        $query->andWhere([Storage::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Storage::tableName() . '.country_id', Storage::tableName() . '.id']);

        $queryFrom = clone $query;
        $queryTo = clone $query;
        $queryFrom->leftJoin(Storage::tableName(), Storage::tableName() . '.id = ' . StorageDocument::tableName() . '.storage_id_from');
        $queryTo->leftJoin(Storage::tableName(), Storage::tableName() . '.id = ' . StorageDocument::tableName() . '.storage_id_to');

        $dataArray1 = $queryFrom->all();
        $dataArray2 = $queryTo->all();

        $data = [];
        foreach ($dataArray1 as $item) {
            $data[$item['country_id']][$item['storage_id']] = $item['max_date'];
        }
        unset($item);
        foreach ($dataArray2 as $item) {
            $data[$item['country_id']][$item['storage_id']] = isset($data[$item['country_id']][$item['storage_id']]) ?
                max($data[$item['country_id']][$item['storage_id']], $item['max_date']) : $item['max_date'];
        }
        unset($item);

        $return = [];
        foreach ($data as $countryId => $storageData) {
            foreach ($storageData as $storageID => $maxDate) {
                if ($maxDate < strtotime('-' . $minDays . 'day')) {
                    $return[$countryId][$storageID] = $maxDate;
                }
            }
        }
        return $return;
    }

    /**
     * Все активные сотрудники в ЗП проекте имеют полностью заполненные данные:
     *
     * Номер ID/Паспорта, Номер счета, Дата рождения, Дата приема на работу, Должность, Схема начисления з/п, Сумма з/п
     * Контакты (Корпоративная почта или Телефон или Skype)
     * Логин (для Операторов)
     *
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsSalaryPersonsNotComplete($countryIds)
    {
        $query = new Query();
        $query->from(Person::tableName());
        $query->select([
            'cnt' => "COUNT(DISTINCT(" . Person::tableName() . '.id))',
            'country_id' => Office::tableName() . '.country_id',
            'persons' => 'GROUP_CONCAT(DISTINCT ' . Person::tableName() . '.id)',
        ]);
        $query->leftJoin(Office::tableName(), Office::tableName() . '.id = ' . Person::tableName() . '.office_id');
        $query->leftJoin(Designation::tableName(), Designation::tableName() . '.id = ' . Person::tableName() . '.designation_id');
        $query->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id = ' . Person::tableName() . '.id');
        $query->where(
            ['or',
                [Person::tableName() . '.passport_id_number' => ''],
                [Person::tableName() . '.account_number' => ''],
                ['is', Person::tableName() . '.passport_id_number', null],
                ['is', Person::tableName() . '.account_number', null],
                ['is', Person::tableName() . '.birth_date', null],
                ['is', Person::tableName() . '.start_date', null],
                ['is', Person::tableName() . '.designation_id', null],
                ['is', Person::tableName() . '.salary_scheme', null],
                ['and',
                    ['or',
                        [Person::tableName() . '.corporate_email' => ''],
                        ['is', Person::tableName() . '.corporate_email', null],
                    ],
                    ['or',
                        [Person::tableName() . '.phone' => ''],
                        ['is', Person::tableName() . '.phone', null],
                    ],
                    ['or',
                        [Person::tableName() . '.skype' => ''],
                        ['is', Person::tableName() . '.skype', null],
                    ],
                ],
                ['and',
                    [Person::tableName() . '.salary_scheme' => Person::SALARY_SCHEME_PER_MONTH],
                    ['or',
                        [Person::tableName() . '.salary_local' => ''],
                        ['is', Person::tableName() . '.salary_local', null],
                    ],
                    ['or',
                        [Person::tableName() . '.salary_usd' => ''],
                        ['is', Person::tableName() . '.salary_usd', null],
                    ],
                ],
                ['and',
                    [Person::tableName() . '.salary_scheme' => Person::SALARY_SCHEME_PER_DAY],
                    ['or',
                        [Person::tableName() . '.salary_hour_local' => ''],
                        ['is', Person::tableName() . '.salary_hour_local', null],
                    ],
                    ['or',
                        [Person::tableName() . '.salary_hour_usd' => ''],
                        ['is', Person::tableName() . '.salary_hour_usd', null],
                    ],
                ],
                ['and',
                    [Designation::tableName() . '.calc_work_time' => 1],
                    ['is', CallCenterUser::tableName() . '.id', null],
                ]
            ]
        );
        $query->andWhere([Person::tableName() . '.active' => Person::ACTIVE]);
        $query->andWhere([Office::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Office::tableName() . '.country_id']);
        return self::indexCountryData($query->all(), 'persons');
    }

    /**
     * Все уволенные операторы, в ЗП проекте отмечены как уволенные
     *
     * проверяется наличие операторов которые не активны в системе 7 и более дней.
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsSalaryPersonsNotActive($countryIds)
    {

        $query = new Query();
        $query->from(Person::tableName());
        $query->select([
            'person_id' => Person::tableName() . '.id',
            'country_id' => Office::tableName() . '.country_id',
            'last_time_person' => 'MAX(time.last_time_user)',
        ]);
        $query->leftJoin(Office::tableName(), Office::tableName() . '.id = ' . Person::tableName() . '.office_id');
        $query->leftJoin(Designation::tableName(), Designation::tableName() . '.id = ' . Person::tableName() . '.designation_id');
        $query->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id = ' . Person::tableName() . '.id');

        $subQueryTime = CallCenterWorkTime::find()
            ->select([
                'last_time_user' => 'MAX(' . CallCenterWorkTime::tableName() . '.date)',
                'call_center_user_id' => CallCenterWorkTime::tableName() . '.call_center_user_id'
            ])
            ->groupBy('call_center_user_id');
        $subQueryTime->where(['>', CallCenterWorkTime::tableName() . '.time', 0]);
        $query->leftJoin(['time' => $subQueryTime], 'time.call_center_user_id = ' . CallCenterUser::tableName() . '.id');
        $query->andWhere([Office::tableName() . '.country_id' => $countryIds]);
        $query->andWhere([Designation::tableName() . '.calc_work_time' => 1]);
        $query->andWhere([Person::tableName() . '.active' => Person::ACTIVE]);
        $query->groupBy([Person::tableName() . '.id']);

        $return = [];
        foreach ($query->all() as $item) {
            if ($item['last_time_person'] < date('Y-m-d', strtotime('-6day')) || !$item['last_time_person']) {
                $return[$item['country_id']][$item['person_id']] = $item['last_time_person'] ? $item['last_time_person'] : 0;
            }
        }
        return $return;
    }

    /**
     * Продаваемые товары имеют все не обходимые данные для работы системы
     *
     * Проверяется заполненность полей у товаров в справочнике: размеры, описание, цена, картинка.
     * Также проверяется наличие перевода описания и местной цены для товаров которые есть на складах соответствующих стран.
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsProductsNotComplete($countryIds)
    {
        $query = new Query();
        $query->from(StorageProduct::tableName());
        $query->select([
            'country_id' => Storage::tableName() . '.country_id',
            'product_id' => StorageProduct::tableName() . '.product_id',
        ]);
        $query->leftJoin(Storage::tableName(), StorageProduct::tableName() . '.storage_id=' . Storage::tableName() . '.id');
        $query->leftJoin(Product::tableName(), StorageProduct::tableName() . '.product_id=' . Product::tableName() . '.id');
        $query->leftJoin(Country::tableName(), Country::tableName() . '.id = ' . Storage::tableName() . '.country_id');
        $query->leftJoin(Language::tableName(), Language::tableName() . '.id = ' . Country::tableName() . '.language_id');
        $query->leftJoin(ProductPrice::tableName(),
            ProductPrice::tableName() . '.currency_id = ' . Country::tableName() . '.currency_id and ' .
            ProductPrice::tableName() . '.product_id = ' . Product::tableName() . '.id and ' .
            ProductPrice::tableName() . '.country_id = ' . Country::tableName() . '.id'
        );
        $query->leftJoin(ProductTranslation::tableName(),
            ProductTranslation::tableName() . '.language_id = ' . Language::tableName() . '.id and ' .
            ProductTranslation::tableName() . '.product_id = ' . Product::tableName() . '.id and ' .
            ProductTranslation::tableName() . '.country_id = ' . Country::tableName() . '.id'
        );

        $query->where(
            ['or',
                ['is', Product::tableName() . '.weight', null],
                ['is', Product::tableName() . '.width', null],
                ['is', Product::tableName() . '.height', null],
                ['is', Product::tableName() . '.image', null],
                ['is', Product::tableName() . '.description', null],
                [Product::tableName() . '.description' => ''],
                [Product::tableName() . '.image' => ''],
                ['is', ProductPrice::tableName() . '.id', null],
                ['is', ProductTranslation::tableName() . '.id', null],
            ]
        );
        $query->andWhere([Storage::tableName() . '.country_id' => $countryIds]);
        $query->andWhere(['>', StorageProduct::tableName() . '.balance', 0]);
        $query->groupBy([
            Storage::tableName() . '.country_id',
            StorageProduct::tableName() . '.product_id',
        ]);

        $return = [];
        foreach ($query->all() as $item) {
            $return[$item['country_id']][] = $item['product_id'];
        }
        return $return;
    }

    /**
     * Все отправленные более 30 дней назад заказы доставлены и деньги получены
     *
     * Проверяются заказы по дате отправки более 30 дней, не должно быть заказов в процессе,
     * должны быть конечные статусы: отказ клиента, выкуп клиента, деньги получены, отказ КЦ.
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsOrdersInProcess($countryIds)
    {
        $query = new Query();
        $query->from(DeliveryRequest::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => 'COUNT(' . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
        ]);
        $query->leftJoin(Order::tableName(), Order::tableName() . '.id = ' . DeliveryRequest::tableName() . '.order_id');
        $query->where(['<', DeliveryRequest::tableName() . '.sent_at', strtotime('-30 days', strtotime('midnight'))]);
        $query->andWhere([Order::tableName() . '.status_id' => OrderStatus::getProcessDeliveryList()]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);
        return self::indexCountryOrders($query->all());
    }

    /**
     * Нет заказов в 19 статусе
     * на всякий случай проверяются заказы не старше месяца
     *
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsOrdersRejected($countryIds)
    {
        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => "COUNT(" . Order::tableName() . '.id)',
            'orders' => 'GROUP_CONCAT(DISTINCT ' . Order::tableName() . '.id)',
        ]);
        $query->where(['>', Order::tableName() . '.created_at', strtotime('-30 days', strtotime('midnight'))]);
        $query->andWhere([Order::tableName() . '.status_id' => [
            OrderStatus::STATUS_DELIVERY_REJECTED,
        ]]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);
        return self::indexCountryOrders($query->all());
    }

    /**
     * Не более 2% невыкупов в определенных причинах
     * отнимаем 14 дней и смотрим за последние 30 дней
     *
     * @param array $countryIds
     * @param string $reason
     * @return array
     */
    public static function getTotalsUnBuyoutReason($countryIds, $reason)
    {
        $percentLimit = 2; // до 2х % норма

        $reasonId = null;
        switch ($reason) {
            case 'address':
                $reasonId = UnBuyoutReason::ADDRESS_ERROR_ID;
                break;
            case 'client':
                $reasonId = UnBuyoutReason::CLIENT_DID_NOT_ORDER_ID;
                break;
            default:
                return [];
                break;
        }

        $query = new Query();
        $query->from(DeliveryRequest::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt_all' => 'COUNT(' . Order::tableName() . '.id)',
            'cnt_reason' => 'COUNT(' . UnBuyoutReasonMapping::tableName() . '.reason_id in (' . $reasonId . ') OR NULL)',
        ]);
        $query->leftJoin(Order::tableName(), Order::tableName() . '.id = ' . DeliveryRequest::tableName() . '.order_id');
        $query->leftJoin(DeliveryRequestUnBuyoutReason::tableName(), DeliveryRequestUnBuyoutReason::tableName() . '.delivery_request_id=' . DeliveryRequest::tableName() . '.id');
        $query->leftJoin(UnBuyoutReasonMapping::tableName(), UnBuyoutReasonMapping::tableName() . '.id=' . DeliveryRequestUnBuyoutReason::tableName() . '.reason_mapping_id');
        $query->where(['>', 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)', strtotime('-44 days', strtotime('midnight'))]);
        $query->andWhere(['<', 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)', strtotime('-14 days', strtotime('midnight'))]);
        $query->andWhere([Order::tableName() . '.status_id' =>
            OrderStatus::getNotBuyoutDeliveryList(),
        ]);
        $query->andWhere([Order::tableName() . '.country_id' => $countryIds]);
        $query->groupBy([Order::tableName() . '.country_id']);
        $query->having('cnt_reason > 0');

        $return = [];
        $data = $query->all();
        foreach ($data as $line) {
            if ($line['cnt_all']) {
                $percent = $line['cnt_reason'] * 100 / $line['cnt_all'];
                if ($percent > $percentLimit) {
                    $return[$line['country_id']] = [
                        'percent' => $percent,
                        'cnt_reason' => $line['cnt_reason']
                    ];
                }
            }
        }
        return $return;
    }

    /**
     * @param array $countryIds
     * @return array
     */
    public static function getTotalsJiraTask($countryIds)
    {
        $callCenters = CallCenter::find()
            ->joinWith('country')
            ->active()
            ->andwhere(['is not', CallCenter::tableName() . '.jira_issue_collector', null])
            ->andWhere(['<>', CallCenter::tableName() . '.jira_issue_collector', ''])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.id' => $countryIds])
            ->all();

        $return = [];
        $jira = new Jira();
        foreach ($callCenters as $callCenter) {
            try {
                $taskData = $jira->getTasks($callCenter->jira_issue_collector);
                if ($taskData && isset($taskData->total) && $taskData->total > 0) {
                    $details = [];
                    if (isset($taskData->issues) && is_array($taskData->issues)) {
                        foreach ($taskData->issues as $issue) {
                            if (isset($issue->key)) {
                                $details[] = $issue->key;
                            }
                        }
                    }
                    if (isset($return[$callCenter->country_id])) {
                        $return[$callCenter->country_id] = [
                            'cnt' => $return[$callCenter->country_id]['cnt'] + $taskData->total,
                            'jira' => array_merge($return[$callCenter->country_id]['jira'], $details)
                        ];
                    }
                    else {
                        $return[$callCenter->country_id] = [
                            'cnt' => $taskData->total,
                            'jira' => $details
                        ];
                    }
                }
                sleep(rand(1, 3));
            } catch (\Throwable $e) {
                $e->getMessage();
            }
        }
        return $return;
    }

    /**
     * тут страна в $countryIds это направление колл-центра
     * выбираем по каждому направлению офисы, которые обзванивают это направление и где нет микротика, видео или интернета
     *
     * @param array $countryIds
     * @return array
     */
    public static function getOfficesNoTechnicalEquipment($countryIds)
    {
        $query = new Query();
        $query->from(CallCenterToOffice::tableName());
        $query->select([
            'country_id' => CallCenter::tableName() . '.country_id',
            'cnt' => 'COUNT(' . Office::tableName() . '.id)',
            'offices' => 'GROUP_CONCAT(DISTINCT ' . Office::tableName() . '.id)',
        ]);
        $query->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id = ' . CallCenterToOffice::tableName() . '.call_center_id');
        $query->leftJoin(Office::tableName(), Office::tableName() . '.id = ' . CallCenterToOffice::tableName() . '.office_id');
        $query->where([Office::tableName() . '.active' => 1]);
        $query->andWhere([Office::tableName() . '.type' => Office::TYPE_CALL_CENTER]);
        $query->andWhere([CallCenter::tableName() . '.country_id' => $countryIds]);
        $query->andWhere([
                'or',
                [Office::tableName() . '.technical_equipment_mikrotik' => 0],
                [Office::tableName() . '.technical_equipment_video' => 0],
                [Office::tableName() . '.technical_equipment_internet' => 0],
            ]
        );
        $query->groupBy([CallCenter::tableName() . '.country_id']);
        return self::indexCountryData($query->all(), 'offices');
    }

    /**
     * Возвращает массив по индексу country_id
     * cnt - число проблем
     * orders - примеры заказов с вероятной обрезкой хвоста, т.к. мог быть обрезан до 1024 в запросе
     *
     * @param $rowData
     * @return array
     */
    public static function indexCountryOrders($rowData)
    {
        return self::indexCountryData($rowData, 'orders');
    }

    /*
    * @param $rowData
    * @return array
    */
    public static function indexCountryData($rowData, $data = 'orders')
    {
        $return = [];
        foreach ($rowData as $row) {
            $return[$row['country_id']] = [
                'cnt' => $row['cnt'],
                $data => strlen($row[$data]) > 1000 ? preg_replace('/,\d*$/', '', $row[$data]) : $row[$data],
            ];
        }
        return $return;
    }
}
