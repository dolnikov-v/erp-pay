<?php
namespace app\modules\checklist\components;

use app\models\Country;
use app\models\Product;
use app\modules\delivery\models\Delivery;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use app\modules\checklist\models\CheckListItem;
use yii\helpers\Html;
use yii\helpers\Url;

class DetailsFormatterCall extends \yii\base\Component
{
    /***
     * @param array $details
     * @param string $trigger
     * @param integer $updatedAt
     * @param string $comment
     * @return string
     */
    public static function format($details, $trigger, $updatedAt, $comment = '')
    {
        $return = Yii::t('common', 'Дата проверки') . ': ' . Yii::$app->formatter->asDatetime($updatedAt) . '<br>';

        if (!empty($comment)) {
            $return .= Yii::t('common', 'Комментарий') . ': ' . $comment . '<br>';
        }

        switch ($trigger) {

            case CheckListItem::TRIGGER_TWENTY_SIX:
                $office = new Office();
                $labels = $office->attributeLabels();
                foreach ($details as $key => $val) {
                    if ($key != 'id') {
                        $return .= ($labels['technical_equipment_' . $key] ?? '') . ': ' . ($val ? Yii::t('common', 'да') : Yii::t('common', 'нет')) . '<br>';
                    }
                }
                break;

            default:
                break;
        }
        return $return;
    }
}