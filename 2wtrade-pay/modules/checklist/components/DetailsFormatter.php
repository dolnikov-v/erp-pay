<?php
namespace app\modules\checklist\components;

use app\models\Country;
use app\models\Product;
use app\modules\delivery\models\Delivery;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\storage\models\Storage;
use Yii;
use app\modules\checklist\models\CheckListItem;
use yii\helpers\Html;
use yii\helpers\Url;

class DetailsFormatter extends \yii\base\Component
{
    /***
     * @param array $details
     * @param string $trigger
     * @param integer $updatedAt
     * @param string $countrySlug
     * @param integer $countryId
     * @param string $comment
     * @return string
     */
    public static function format($details, $trigger, $updatedAt, $countrySlug = '', $countryId = null, $comment = '')
    {
        $return = Yii::t('common', 'Дата проверки') . ': ' . Yii::$app->formatter->asDatetime($updatedAt) . '<br>';

        if (!empty($comment)) {
            $return .= Yii::t('common', 'Комментарий') . ': ' . $comment . '<br>';
        }

        $batchSize = 250;
        $showSize = 10;

        switch ($trigger) {
            case CheckListItem::TRIGGER_ONE:
            case CheckListItem::TRIGGER_TWO:
            case CheckListItem::TRIGGER_THREE:
            case CheckListItem::TRIGGER_EIGHT:
            case CheckListItem::TRIGGER_SIXTEEN:
            case CheckListItem::TRIGGER_EIGHTEEN:

                if (isset($details['avg_check'])) {
                    $return .= Yii::t('common', 'Средний чек') . ': ' . Yii::$app->formatter->asDecimal($details['avg_check'], 2) . ' $<br>';
                    unset($details['avg_check']);
                }

                if (isset($details['approve_percent'])) {
                    $return .= Yii::t('common', 'Аппрув') . ': ' . Yii::$app->formatter->asDecimal($details['approve_percent'], 2) . '%<br>';
                    unset($details['approve_percent']);
                }

                if ($details) {
                    $cnt = isset($details['cnt']) ? $details['cnt'] : sizeof($details);
                    $orders = isset($details['orders']) ? explode(',', $details['orders']) : $details;

                    $return .= Yii::t('common', 'Заказы') . ' (' . Yii::t('common', 'всего') . ' ' . $cnt . ')' . ($cnt > 100 ? Yii::t('common', ' примеры') : '') . ':<br>';
                    $batchedDetails = array_chunk($orders, $batchSize);
                    foreach ($batchedDetails as $batch) {
                        if (sizeof($batch) < $showSize) {
                            $linkBody = implode(", ", $batch);
                        } else {
                            $linkBody = implode(", ", array_slice($batch, 0, $showSize)) . '...';
                        }
                        $return .= Html::a($linkBody,
                                Url::to([
                                    '/order/index/index',
                                    'force_country_slug' => $countrySlug,
                                    'NumberFilter' => [
                                        'entity' => 'id',
                                        'number' => implode(',', $batch)
                                    ],
                                ]), [
                                    'target' => '_blank',
                                ]) . ' ';
                    }
                }
                break;

            case CheckListItem::TRIGGER_FOUR:
                foreach ($details as $detail) {
                    $line = '';
                    $delivery = null;
                    if (isset($detail['delivery'])) {
                        $delivery = Delivery::findOne($detail['delivery']);
                        if ($delivery instanceof Delivery) {
                            $line .= Yii::t('common', 'КС') . ': ' . $delivery->name . ' - ';
                        }
                    }
                    $product = null;
                    if (isset($detail['product'])) {
                        $product = Product::findOne($detail['product']);
                        if ($product instanceof Product) {
                            $line .= Yii::t('common', 'Товар') . ': ' . $product->name . ' - ';
                        }
                    }
                    if (isset($detail['days'])) {
                        $line .= Yii::t('common', 'Запас товара') . ': ' . Yii::$app->formatter->asDecimal($detail['days'], 2) . ' ' . Yii::t('common', 'дней');
                    }
                    if ($line != '') {
                        if (Yii::$app->user->can('storage.balance.index')) {
                            $StorageProductSearch = [];
                            if ($product instanceof Product) {
                                $StorageProductSearch['product_id'] = $product->id;
                            }
                            if ($delivery instanceof Delivery) {
                                $StorageProductSearch['delivery_id'] = $delivery->id;
                            }

                            $return .= Html::a($line,
                                Url::to([
                                    '/storage/balance/index',
                                    'force_country_slug' => $countrySlug,
                                    'StorageProductSearch' => $StorageProductSearch,
                                ]), [
                                    'target' => '_blank',
                                ]);
                        } else {
                            $return .= $line;
                        }
                        $return .= '<br>';
                    }
                }

                break;

            case CheckListItem::TRIGGER_FIVE:
                foreach ($details as $detail) {
                    $line = '';
                    if (isset($detail['delivery'])) {
                        $delivery = Delivery::findOne($detail['delivery']);
                        if ($delivery instanceof Delivery) {
                            $line .= Yii::t('common', 'КС') . ': ' . $delivery->name . ' - ';
                        }
                    }
                    if (!empty($detail['last_report_update'])) {
                        $line .= Yii::t('common', 'Обновление из отчетов') . ': ' . Yii::$app->formatter->asDatetime($detail['last_report_update']) . ' ';
                    }
                    if (!empty($detail['last_api_update'])) {
                        $line .= Yii::t('common', 'Обновление по API') . ': ' . Yii::$app->formatter->asDatetime($detail['last_api_update']);
                    }

                    if ($line != '') {
                        if (Yii::$app->user->can('report.delivery.index')) {
                            $return .= Html::a($line,
                                Url::to([
                                    '/report/delivery/index',
                                    'force_country_slug' => $countrySlug,
                                ]), [
                                    'target' => '_blank',
                                ]);
                        } else {
                            $return .= $line;
                        }
                        $return .= '<br>';
                    }
                }
                break;

            case CheckListItem::TRIGGER_SIX:
                $line = '';
                if (isset($details['buyout'])) {
                    $line .= Yii::t('common', 'Выкуп за последние 7 дней') . ': ' . Yii::$app->formatter->asDecimal($details['buyout'], 2) . '%<br>';
                }
                if (isset($details['prevBuyout'])) {
                    $line .= Yii::t('common', 'Выкуп 24 часа назад за 7 дней') . ': ' . Yii::$app->formatter->asDecimal($details['prevBuyout'], 2) . '%<br>';
                }
                if ($line) {
                    $return .= $line . '<br>';
                }
                break;

            case CheckListItem::TRIGGER_SEVEN:
                foreach ($details as $detail) {
                    $line = '';
                    if (isset($detail['delivery'])) {
                        $delivery = Delivery::findOne($detail['delivery']);
                        if ($delivery instanceof Delivery) {
                            $line .= Yii::t('common', 'КС') . ': ' . $delivery->name . ' - ';
                        }
                    }
                    if (!empty($detail['old'])) {
                        $line .= Yii::t('common', 'Долг более 1 мес') . ': ' . Yii::$app->formatter->asDecimal($detail['old'], 2) . ' $ ';
                    }
                    if (!empty($detail['new'])) {
                        $line .= Yii::t('common', 'Долг') . ': ' . Yii::$app->formatter->asDecimal($detail['new'], 2) . ' $ ';
                    }
                    if ($line) {
                        $return .= $line . '<br>';
                    }
                }
                break;

            case CheckListItem::TRIGGER_NINE:

                if ($details) {
                    $cnt = isset($details['cnt']) ? $details['cnt'] : sizeof($details);
                    $products = isset($details['products']) ? explode(',', $details['products']) : $details;
                    $products = Product::findAll($products);
                    $return .= Yii::t('common', 'Товары без сертификатов') . ' (' . Yii::t('common', 'всего') . ' ' . $cnt . ')' . ($cnt > 100 ? Yii::t('common', ' примеры') : '') . ':<br>';
                    $line = [];
                    foreach ($products as $product) {
                        $linkBody = $product->name;
                        if (Yii::$app->user->can('catalog.certificate.index')) {
                            $line[] = Html::a($linkBody,
                                Url::to([
                                    '/catalog/certificate/index',
                                    'CertificateSearch' => [
                                        'product_id' => $product->id,
                                        'country_id' => $countryId
                                    ],
                                ]), [
                                    'target' => '_blank',
                                ]);
                        } else {
                            $line[] = $linkBody;
                        }
                    }
                    if ($line) {
                        $return .= implode(', ', $line);
                    }
                }
                break;

            case CheckListItem::TRIGGER_TEN:
                if ($details) {
                    foreach ($details as $storageId => $maxDate) {
                        $line = '';
                        if ($storageId) {
                            $storage = Storage::findOne($storageId);
                            $line .= Yii::t('common', 'Склад') . ': ';
                            if ($storage instanceof Storage) {
                                $line .= $storage->name . ' - ';
                            } else {
                                $line .= '(ID = ' . $storageId . ') - ';
                            }
                        }
                        $line .= Yii::t('common', 'Дата последнего движения') . ': ' . Yii::$app->formatter->asDatetime($maxDate);

                        if (Yii::$app->user->can('storage.document.index')) {
                            $line = Html::a($line,
                                Url::to([
                                    '/storage/document/index',
                                    'force_country_slug' => $countrySlug,
                                ]), [
                                    'target' => '_blank',
                                ]);
                        }

                        if ($line) {
                            $return .= $line . '<br>';
                        }
                    }
                }
                break;

            case CheckListItem::TRIGGER_ELEVEN:
            case CheckListItem::TRIGGER_TWELVE:
                $line = Yii::t('common', 'Причины невыкупа') . ': ';
                if (!empty($details['cnt_reason'])) {
                    $line .= $details['cnt_reason'];
                }
                if (!empty($details['percent'])) {
                    $line .= '(' . Yii::$app->formatter->asDecimal($details['percent'], 2) . '%)';
                }
                if (Yii::$app->user->can('report.unshippingreasons.index')) {
                    $line = Html::a($line,
                        Url::to([
                            '/report/unshipping-reasons/index',
                            'force_country_slug' => $countrySlug,
                            's' => [
                                'from' => Yii::$app->formatter->asDate(strtotime('-44 days', strtotime('midnight'))),
                                'to' => Yii::$app->formatter->asDate(strtotime('-14 days', strtotime('midnight'))),
                                'country_ids' => $countryId
                            ],
                        ]), [
                            'target' => '_blank',
                        ]);
                }
                $return .= $line . '<br>';
                break;

            case CheckListItem::TRIGGER_THIRTEEN:

                if ($details && isset($details['cnt']) && isset($details['persons'])) {

                    $return .= Yii::t('common', 'Сотрудники') . ' (' . Yii::t('common', 'всего') . ' ' . $details['cnt'] . '):';

                    $persons = Person::find()
                        ->with('callCenterUsers')
                        ->where(['id' => explode(',', $details['persons'])])
                        ->all();
                    $groups = [];
                    foreach ($persons as $person) {
                        if (!$person->passport_id_number) {
                            $groups[Yii::t('common', 'Нет паспорта')][] = $person;
                        }
                        if (!$person->account_number) {
                            $groups[Yii::t('common', 'Нет счета')][] = $person;
                        }
                        if (!$person->birth_date) {
                            $groups[Yii::t('common', 'Нет даты рождения')][] = $person;
                        }
                        if (!$person->start_date) {
                            $groups[Yii::t('common', 'Нет даты устройства')][] = $person;
                        }
                        if (!$person->designation_id) {
                            $groups[Yii::t('common', 'Нет должности')][] = $person;
                        }
                        if (!$person->salary_scheme) {
                            $groups[Yii::t('common', 'Нет зарплатной схемы')][] = $person;
                        }
                        if (!$person->corporate_email && !$person->phone && !$person->skype) {
                            $groups[Yii::t('common', 'Нет контактов')][] = $person;
                        }
                        if ($person->salary_scheme == Person::SALARY_SCHEME_PER_MONTH) {
                            if (!$person->salary_local && !$person->salary_usd) {
                                $groups[Yii::t('common', 'Нет ЗП')][] = $person;
                            }
                        }
                        if ($person->salary_scheme == Person::SALARY_SCHEME_PER_DAY) {
                            if (!$person->salary_hour_local && !$person->salary_hour_usd) {
                                $groups[Yii::t('common', 'Нет ЗП')][] = $person;
                            }
                        }
                        if ($person->designation->calc_work_time && empty($person->callCenterUsers)) {
                            $groups[Yii::t('common', 'Нет пользователя КЦ')][] = $person;
                        }
                    }
                    unset($person);
                    if ($groups) {
                        $return .= '<ul>';
                        foreach ($groups as $groupKey => $groupList) {
                            $return .= '<li>' . $groupKey . ' (' . sizeof($groupList) . '): ';
                            foreach ($groupList as $key => $person) {
                                $link = $person->name;
                                if (Yii::$app->user->can('salary.person.edit')) {
                                    $link = Html::a($link,
                                        Url::to([
                                            '/salary/person/edit/' . $person->id,
                                        ]), [
                                            'target' => '_blank',
                                        ]);

                                }
                                $return .= $link;
                                if ($key >= $showSize) {
                                    $return .= '...';
                                    break;
                                }
                                if ($person !== end($groupList)) {
                                    $return .= ', ';
                                }
                            }
                            $return .= '</li>';
                        }
                        $return .= '</ul>';
                    }
                }
                break;

            case CheckListItem::TRIGGER_FOURTEEN:

                if ($details) {
                    $return .= Yii::t('common', 'Сотрудники') . ' (' . Yii::t('common', 'всего') . ' ' . sizeof($details) . '):<br>';
                    $i = 0;
                    foreach ($details as $personId => $date) {
                        $person = Person::findOne($personId);
                        if ($person instanceof Person) {
                            $link = $person->name;
                            if (Yii::$app->user->can('salary.person.edit')) {
                                $link = Html::a($link,
                                    Url::to([
                                        '/salary/person/edit/' . $person->id,
                                    ]), [
                                        'target' => '_blank',
                                    ]);
                            }
                            $return .= $link . ' ';
                            if ($date) {
                                $return .= Yii::t('common', 'не активен с') . ' ' . Yii::$app->formatter->asDate($date);
                            } else {
                                $return .= Yii::t('common', 'не было активности');
                            }
                            if ($i < sizeof($details) - 1) {
                                $return .= ', ';
                            }
                        }
                        $i++;
                    }
                }
                break;

            case CheckListItem::TRIGGER_FIFTEEN:

                if ($details) {

                    $return .= Yii::t('common', 'Товары') . ' (' . sizeof($details) . '):';

                    $country = Country::findOne($countryId);
                    $products = Product::findAll($details);

                    $groups = [];
                    foreach ($products as $product) {
                        if (!$product->weight) {
                            $groups[Yii::t('common', 'Нет веса')][] = $product;
                        }
                        if (!$product->width) {
                            $groups[Yii::t('common', 'Нет ширины')][] = $product;
                        }
                        if (!$product->height) {
                            $groups[Yii::t('common', 'Нет высоты')][] = $product;
                        }
                        if (!$product->image) {
                            $groups[Yii::t('common', 'Нет изображения')][] = $product;
                        }
                        if (!$product->description) {
                            $groups[Yii::t('common', 'Нет описания')][] = $product;
                        }
                        if ($country && !$product->getPriceByCurrency($country->currency_id, $country->id)) {
                            $groups[Yii::t('common', 'Нет цен')][] = $product;
                        }
                        if ($country && !$product->getTranslationByLanguage($country->language_id, $country->id)) {
                            $groups[Yii::t('common', 'Нет переводов')][] = $product;
                        }
                    }
                    unset($product);
                    if ($groups) {
                        $return .= '<ul>';
                        foreach ($groups as $groupKey => $groupList) {
                            $return .= '<li>' . $groupKey . ' (' . sizeof($groupList) . '): ';
                            foreach ($groupList as $key => $product) {
                                $link = $product->name;
                                if (Yii::$app->user->can('catalog.product.edit')) {
                                    $link = Html::a($link,
                                        Url::to([
                                            '/catalog/product/edit/' . $product->id,
                                        ]), [
                                            'target' => '_blank',
                                        ]);

                                }
                                $return .= $link;
                                if ($product !== end($groupList)) {
                                    $return .= ', ';
                                }
                            }
                            $return .= '</li>';
                        }
                        $return .= '</ul>';
                    }
                }
                break;

            case CheckListItem::TRIGGER_SEVENTEEN:

                if ($details) {
                    if (!empty($details['cnt'])) {
                        $return .= Yii::t('common', 'Заказы в Jira') . ' (' . Yii::t('common', 'всего') . ' ' . $details['cnt'] . ')' . ($details['cnt'] > 15 ? Yii::t('common', ' примеры') : '') . ':<br>';

                        if (isset($details['jira']) && !empty($details['jira'])) {
                            $i = 0;
                            foreach ($details['jira'] as $task) {
                                $link = Html::a($task, Yii::$app->params['jira']['host'] . '/browse/' . $task, [
                                    'target' => '_blank',
                                ]);
                                $return .= $link;
                                if ($i < sizeof($details['jira']) - 1) {
                                    $return .= ', ';
                                }
                                $i++;
                            }
                        }
                    }
                }
                break;


            case CheckListItem::TRIGGER_TWENTY_SIX:
                if (isset($details['offices'])) {
                    $offices = Office::find()->where(['id' => explode(',', $details['offices'])])->all();
                    foreach ($offices as $office) {
                        $line = Yii::t('common', 'Офис') . ': ' . Yii::t('common', $office->name);
                        $return .= $line . '<br>';
                    }
                }
                break;

            default:
                break;
        }

        return $return;
    }

}