<?php
namespace app\modules\checklist\components;

use app\components\jira\Jira;
use app\models\Certificate;
use app\models\CertificateFile;
use app\models\CurrencyRate;
use app\models\Language;
use app\models\Product;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use yii\db\Expression;
use yii\db\Query;
use app\modules\report\extensions\Query as ReportQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\StorageDocument;
use app\models\Country;
use app\modules\reportoperational\models\TeamCountry;
use yii\helpers\ArrayHelper;

class AutoCheckCall extends AutoCheck
{
    /**
     * @var string
     */
    public static $type = CheckListItem::TYPE_CALL;

    /**
     * Проверить пункт
     *
     * @param mixed $item
     * @param integer|null $countryId
     * @param integer|null $teamId
     * @param string $comment
     * @return array
     */
    public static function check($item, $countryId = null, $teamId = null, $comment = '')
    {
        $errors = [];

        // проверяемый пункт
        $selectedItems = []; // при Cron запуске теперь может быть сразу несколько пунктов с одним тригерром

        if ($item instanceof CheckListItem) {
            $selectedItems[] = $item;
        } else {
            if (is_integer($item)) {
                $selectedItems[] = CheckListItem::findOne($item);
            } else {
                $selectedItems = CheckListItem::find()->byTrigger($item)->byType(self::$type)->active()->auto()->all();
            }
        }

        if (!$selectedItems) {
            $errors[] = 'no check item found';
            return [
                'status' => false,
                'errors' => $errors
            ];
        }

        // физические КЦ
        $query = Office::find()->active()->callCenter()->asArray();
        if ($countryId) {
            $query->andWhere(['country_id' => $countryId]);
        }
        $callCenters = ArrayHelper::map($query->all(), 'id', 'id');

        $date = date('Y-m-d');

        // исключить уже откомментированные, только если не ручной запуск
        foreach ($selectedItems as $selectedItem) {
            if ($selectedItem->explainable && is_array($callCenters)) {
                $commentedCheckList = CheckList::find()
                    ->byDate($selectedItem->days, $date)
                    ->byItem($selectedItem->id)
                    ->byOfficeId($callCenters)
                    ->isCommented();
                $commentedOffices = ArrayHelper::map($commentedCheckList->all(), 'office_id', 'office_id');

                if (is_array($commentedOffices)) {
                    foreach ($commentedOffices as $commentedOffice) {
                        unset($callCenters[$commentedOffice]);
                    }
                }
            }
        }

        // получить критерии в массив типа $totalCounts[$officeID] = 5 (число косяков)
        // или = array (массив из проблем, где число элементов это число проблем)
        // или = array (масиив из всех данных с проблемами, где есть cnt - число проблем, orders - пример проблемных заказов / первые 1024 символ)

        $trigger = $selectedItems[0]->trigger ?? '';    // теперь может быть массив, но тригер одинаковый

        $totalCounts = self::switchTotals($trigger, array_keys($callCenters));

        if ($totalCounts === false) {
            $errors[] = 'no function';
            return [
                'status' => false,
                'errors' => $errors
            ];
        }

        $count = 0;
        $info = [];

        // в некоторых случаях $selectedItems массив, но обычно из одного элемента $selectedItems[0]
        foreach ($selectedItems as $selectedItem) {

            foreach ($callCenters as $officeID) {

                $totalCount = $totalCounts[$officeID] ?? 0;

                $dataInfo = [];

                if ($totalCount) {

                    if (is_array($totalCount)) {
                        $dataInfo['details'] = $totalCount;
                        $info[] = sizeof($totalCount);
                    } else {
                        $info[] = $totalCount;
                    }
                }

                $checkList = CheckList::find()
                    ->byDate($selectedItem->days, $date)
                    ->byItem($selectedItem->id)
                    ->byOfficeId($officeID)
                    ->one();

                if (!$checkList instanceof CheckList) {
                    $checkList = new CheckList();
                    $checkList->date = $date;
                    $checkList->check_list_item_id = $selectedItem->id;
                    $checkList->office_id = $officeID;
                }
                $checkList->status = $totalCount ? CheckList::STATUS_NO : CheckList::STATUS_OK;

                if ($selectedItem->explainable) {
                    $checkList->comment = $comment;
                    if ($checkList->comment && $totalCount) {
                        // пропускаем откомментированный пункт
                        $checkList->status = CheckList::STATUS_OK;
                        $info = [];
                    }
                }

                $checkList->data_info = $dataInfo ? json_encode($dataInfo) : null;
                $checkList->updated_at = time();

                if (!$checkList->save()) {
                    $errors[] = $checkList->getFirstErrorAsString();
                } else {
                    $count++;
                }
            }

        }
        return [
            'status' => $info ? false : true,
            'errors' => $errors,
            'count' => $count,
            'info' => $info
        ];
    }

    /**
     * @param $trigger
     * @param $officeIds
     * @return array
     */
    public static function switchTotals($trigger, $officeIds) {
        switch ($trigger) {
            case CheckListItem::TRIGGER_TWENTY_SIX:
                $totalCounts = self::getOfficesNoTechnicalEquipment($officeIds);
                break;
            default:
                $totalCounts = false;
                break;
        }
        return $totalCounts;
    }

    /**
     * @param array $officeIds
     * @return array
     */
    public static function getOfficesNoTechnicalEquipment($officeIds)
    {
        $query = new Query();
        $query->from(Office::tableName());
        $query->select([
            'id',
            'mikrotik' => 'technical_equipment_mikrotik',
            'video' => 'technical_equipment_video',
            'internet' => 'technical_equipment_internet',
        ]);
        $query->andWhere([Office::tableName() . '.id' => $officeIds]);
        $query->andWhere([
                'or',
                [Office::tableName() . '.technical_equipment_mikrotik' => 0],
                [Office::tableName() . '.technical_equipment_video' => 0],
                [Office::tableName() . '.technical_equipment_internet' => 0],
            ]
        );

        return ArrayHelper::index($query->all(), 'id');
    }
}
