<?php

namespace app\modules\checklist\assets;

use yii\web\AssetBundle;

/**
 * Class CheckListAsset
 * @package app\modules\checklist\assets
 */
class CheckListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/checklist';

    public $js = [
        'check-list.js',
    ];

    public $css = [
        'check-list.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
