<?php
namespace app\modules\checklist\widgets;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmCheckList
 * @package app\modules\checklist\widgets
 */
class ModalConfirmCheckList extends Modal
{

    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-check-list', [
            'id' => 'modal_confirm_check_list',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_SUCCESS,
            'title' => Yii::t('common', 'Подтверждение выполнения'),
            'close' => true,
            'url' => $this->url,
        ]);
    }
}
