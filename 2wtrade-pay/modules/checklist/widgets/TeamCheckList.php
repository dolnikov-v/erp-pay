<?php
namespace app\modules\checklist\widgets;

use yii\base\Widget;

/**
 * Class TeamCheckList
 * @package app\modules\checklist\widgets
 */
class TeamCheckList extends Widget
{
    /** @var \yii\data\ActiveDataProvider */
    public $dataProvider;

    /** @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
    public $modelSearch;

    /** @var array */
    public $teams;

    /** @var array */
    public $users;

    /** @var boolean */
    public $isCurrentDate;

    /** @var boolean */
    public $canCheck;


    public function run()
    {
        return $this->render('team-check-list', [
            'dataProvider' => $this->dataProvider,
            'modelSearch' => $this->modelSearch,
            'teams' => $this->teams,
            'users' => $this->users,
            'isCurrentDate' => $this->isCurrentDate,
            'canCheck' => $this->canCheck
        ]);
    }
}
