<?php
use yii\helpers\Html;
use app\widgets\Button;
use app\helpers\WbIcon;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\components\DetailsFormatter;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
/** @var array $teams */
/** @var array $users */
/** @var boolean $isCurrentDate */
/** @var boolean $canCheck */
$efficiencyCountries = null; // эффективность по странам
$efficiencyAll = 0; // эффективность общая
?>

<table id="checklist_content" class="table table-striped table-hover table-bordered">
    <tr>
        <th></th>
        <?php foreach ($users as $combineId => $combineData): ?>
            <th class="text-center"
                colspan="<?= sizeof($combineData) ?>"><?= implode("<br>", array_values(array_shift($combineData)['name'])) ?></th>
        <?php endforeach; ?>
    </tr>
    <tr class="countries">
        <th></th>
        <?php foreach ($users as $combineId => $combineData): ?>
            <?php foreach ($combineData as $combineItem): ?>
                <th class="text-center"><?= $combineItem['country_name'] ?></th>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tr>

    <?php $countCheck = 0; ?>
    <?php $lastCategory = 0; ?>
    <?php foreach ($dataProvider->allModels as $model): ?>
        <?php $countCheck++; ?>
        <?php if (!$lastCategory && $model->category_id || $lastCategory != $model->category_id): ?>
            <tr>
                <td class="text-center">
                    <h3><?= Yii::t('common', $model->category->name ?? '') ?></h3>
                </td>
            </tr>
        <?php endif; ?>
        <?php $lastCategory = $model->category_id; ?>
        <tr>
            <td>
                <?= Button::widget([
                    'style' => Button::SIZE_TINY . ' ' . ($model->auto ? Button::STYLE_DEFAULT : Button::STYLE_WARNING),
                    'label' => $model->position,
                    'attributes' => [
                        'title' => ($model->auto ? Yii::t('common', 'Автоматический') : Yii::t('common', 'Ручной')),
                        'style' => 'cursor: default'
                    ]
                ]);
                $name = Html::tag('span', $model->name, ['class' => 'check-item']);
                ?>
                <?php if (trim($model->description) != ''): ?>
                    <?= Html::a($name, '#', ['class' => 'more-info', 'title' => Yii::t('common', 'Подробнее')]) . ' [' .
                    Html::a('+', '#', [
                        'class' => 'more-info more-icon',
                        'title' => Yii::t('common', 'Подробнее')
                    ]) . ']' .
                    Html::tag('div', $model->description, ['class' => 'check-description']); ?>
                <?php else: ?>
                    <?= $name ?>
                <?php endif; ?>
            </td>
            <?php $cols = 0; ?>
            <?php foreach ($users as $combineId => $combineData): ?>
                <?php foreach ($combineData as $combineItem): ?>
                    <?php $cols++; ?>
                    <td class="text-center">
                        <?php
                        /** @var CheckListItem $model */

                        $countryId = $combineItem['country_id'];
                        $countryName = $combineItem['country_name'];
                        $countrySlug = $combineItem['country_slug'];
                        $userIds = $combineItem['id'];

                        $checkBox = $model->checkedLists[$countryId] ?? null;

                        $value = ($checkBox && $checkBox['status'] == CheckList::STATUS_OK ? 1 : 0);
                        /* подсчет эффективности */
                        if (!isset($efficiencyCountries[$countryId])) {
                            $efficiencyCountries[$countryId] = 0;
                        }
                        if ($value) {
                            $efficiencyCountries[$countryId]++;
                            $efficiencyAll++;
                        }

                        $icon = $value ? WbIcon::CHECK : WbIcon::CLOSE;
                        $color = $value ? Button::STYLE_SUCCESS : Button::STYLE_DANGER;
                        $comment = $checkBox['comment'] ? $checkBox['comment'] : '';

                        $checkable = 'disabled'; // не активен

                        $dataInfo = [];
                        if ($checkBox && isset($checkBox['data_info'])) {
                            $dataInfo = json_decode($checkBox['data_info'], true);
                            if (isset($dataInfo['details']) || !empty($comment)) {
                                $checkable = 'viewable'; // просмотр деталей
                            }
                        }

                        if ($isCurrentDate && in_array(Yii::$app->user->id, $userIds) && $canCheck) {
                            // сегодня можно ставить и если пользователь входит в список
                            $checkable = str_replace('disabled', '', $checkable);
                            if (!$model->auto) {
                                $checkable .= ' checkable'; // ручной
                            }
                            if ($model->explainable) {
                                $checkable .= ' checkable explainable'; // с комментарием
                            }
                        }
                        ?>
                        <?= Button::widget([
                            'style' => Button::SIZE_TINY . ' ' . $color . ' ' . $checkable,
                            'icon' => $icon,
                            'attributes' => [
                                'data-country' => $countryName,
                                'data-country_id' => $countryId,
                                'data-item' => $model->id,
                                'data-value' => $value,
                                'data-comment' => $comment,
                                'title' => strpos($checkable, 'viewable') !== false ? Yii::t('common', 'Подробнее') : '',
                            ]
                        ]);
                        ?>
                        <?php if (strpos($checkable, 'viewable') !== false) {
                            print Html::tag('div',
                                DetailsFormatter::format($dataInfo['details'], $checkBox['checkListItem']['trigger'], $checkBox['updated_at'], $countrySlug, $countryId, $comment),
                                ['class' => 'details_info']);
                        }
                        ?>
                    </td>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>
        <tr class="details_holder">
            <td colspan="<?= ($cols + 1) ?>">
                <div></div>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php if ($cols): ?>
        <tr class="warning total_efficiency">
            <td><?= Yii::t('common', 'Эффективность направления') ?></td>
            <?php foreach ($users as $combineId => $combineData): ?>
                <?php foreach ($combineData as $combineItem): ?>
                    <td class="text-center">
                        <?= Yii::$app->formatter->asDecimal(($countCheck ? $efficiencyCountries[$combineItem['country_id']] / $countCheck * 100 : 0), 2) . '%' ?>
                    </td>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tr>
        <tr class="warning total_efficiency">
            <td><?= Yii::t('common', 'Эффективность региона') ?></td>
            <td colspan="<?= ($cols) ?>" class="text-center">
                <?= Yii::$app->formatter->asDecimal((($countCheck * $cols) ? $efficiencyAll / $countCheck / $cols * 100 : 0), 2) . '%' ?>
            </td>
        </tr>
    <?php endif; ?>
</table>