<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
use yii\helpers\Html;

/** @var string $id */
/** @var string $size */
/** @var string $color */
/** @var string $title */
/** @var string $close */
/** @var string $url */
?>

<div id="<?= $id ?>" class="modal fade <?= $color ?> in" tabindex="-1" role="dialog" aria-hidden="true"
     data-url="<?= $url ?>">
    <div class="modal-dialog <?= $size ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><?= $title ?></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <span id="modal_confirm_check_true"><?= Yii::t('common', 'Вы действительно хотите отметить пункт чек листа?') ?></span>
                    <span id="modal_confirm_check_false"
                          style="display: none"><?= Yii::t('common', 'Вы действительно хотите снять отметку с пункта чек листа?') ?></span>
                </div>
                <div class="text-center">
                    <span id="modal_confirm_check_list_span"></span>
                    <p><?= Yii::t('common', 'Страна') ?>: <span id="modal_confirm_check_list_country"></span></p>
                    <p><?= Yii::t('common', 'Дата') ?>: <span id="modal_confirm_check_list_date"></span></p>
                </div>
                <div id="modal_confirm_check_explain_wrap" style="display: none">
                    <?= Html::textarea('modal_confirm_check_explain_text', '', [
                        'id' => 'modal_confirm_check_explain_text',
                        'placeholder' => Yii::t('common', 'Комментарий'),
                        'style' => 'width: 100%;'
                    ]) ?>
                </div>
            </div>
            <div class="modal-footer text-right">
                <?= Button::widget([
                    'label' => Yii::t('common', 'Отмена'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]) ?>

                <?= ButtonLink::widget([
                    'id' => 'modal_confirm_check_list_link',
                    'label' => Yii::t('common', 'ОК'),
                    'url' => '#',
                    'style' => ButtonLink::STYLE_DANGER,
                    'size' => Button::SIZE_SMALL,
                ]) ?>
            </div>
        </div>
    </div>
</div>
