<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\checklist\models\CheckListCategory $model */

$this->title = $model->isNewRecord ?  Yii::t('common', 'Добавление категории чек-листа') : Yii::t('common', 'Редактирование категории чек-листа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Категории чек-листа'), 'url' => Url::toRoute('/checklist/admin-category/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление категории чек-листа') : Yii::t('common', 'Редактирование категории чек-листа')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Категории чек-листа'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
