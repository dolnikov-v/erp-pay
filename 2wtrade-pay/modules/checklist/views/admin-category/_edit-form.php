<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\checklist\models\CheckListItem;

/** @var \app\modules\checklist\models\CheckListCategory $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'position')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'type')->select2List(CheckListItem::getTypeCollection()) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить категорию чек листа') : Yii::t('common', 'Сохранить категорию чек листа')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
