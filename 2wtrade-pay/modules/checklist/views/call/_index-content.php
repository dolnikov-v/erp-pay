<?php
use yii\helpers\Html;
use app\widgets\Button;
use app\helpers\WbIcon;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\components\DetailsFormatterCall;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
/** @var boolean $isCurrentDate */
/** @var array $selectedOffices */

?>

<table id="checklist_content" class="table table-striped table-hover table-bordered">
    <tr>
        <th></th>
        <?php foreach ($selectedOffices as $office): ?>
            <th class="text-center">
                <?= Yii::t('common', $office['name']) ?><br>
                <?php foreach ($office['users'] as $user): ?>
                    <?= $user->username ?><br>
                <?php endforeach; ?>
            </th>
        <?php endforeach; ?>
    </tr>

    <?php foreach ($dataProvider->allModels as $model): ?>
        <tr>
            <td>
                <?= Button::widget([
                    'style' => Button::SIZE_TINY . ' ' . ($model->auto ? Button::STYLE_DEFAULT : Button::STYLE_WARNING),
                    'label' => $model->position,
                    'attributes' => [
                        'title' => ($model->auto ? Yii::t('common', 'Автоматический') : Yii::t('common', 'Ручной')),
                        'style' => 'cursor: default'
                    ]
                ]);
                $name = Html::tag('span', $model->name, ['class' => 'check-item']);
                ?>
                <?php if (trim($model->description) != ''): ?>
                    <?= Html::a($name, '#', ['class' => 'more-info', 'title' => Yii::t('common', 'Подробнее')]) . ' [' .
                    Html::a('+', '#', ['class' => 'more-info more-icon', 'title' => Yii::t('common', 'Подробнее')]) . ']' .
                    Html::tag('div', $model->description, ['class' => 'check-description']); ?>
                <?php else: ?>
                    <?= $name ?>
                <?php endif; ?>
            </td>
            <?php $cols = 0; ?>
            <?php foreach ($selectedOffices as $office): ?>
                <?php $cols++; ?>
                <td class="text-center">
                    <?php
                    /** @var CheckListItem $model */

                    $checkBox = $model->checkedLists[$office['id']] ?? null;

                    $value = ($checkBox && $checkBox['status'] == CheckList::STATUS_OK ? 1 : 0);

                    $icon = $value ? WbIcon::CHECK : WbIcon::CLOSE;
                    $color = $value ? Button::STYLE_SUCCESS : Button::STYLE_DANGER;

                    $checkable = 'disabled'; // только просмотр

                    $dataInfo = [];
                    if ($checkBox && isset($checkBox['data_info'])) {
                        $dataInfo = json_decode($checkBox['data_info'], true);
                        if (isset($dataInfo['details']) || !empty($comment)) {
                            $checkable = 'viewable'; // просмотр деталей
                        }
                    }

                    if ($isCurrentDate && Yii::$app->user->can('checklist.call.set')) {
                        $checkable = str_replace('disabled', '', $checkable);
                        // сегодня можно ставить и если пользователь входит в список
                        if (!$model->auto) {
                            $checkable .= ' checkable'; // ручной
                        }
                        if ($model->explainable) {
                            $checkable .= ' checkable explainable'; // с комментарием
                        }
                    }
                    $comment = $checkBox['comment'] ? $checkBox['comment'] : '';
                    ?>
                    <?= Button::widget([
                        'style' => Button::SIZE_TINY . ' ' . $color . ' ' . $checkable,
                        'icon' => $icon,
                        'attributes' => [
                            'data-item' => $model->id,
                            'data-value' => $value,
                            'data-comment' => $comment,
                            'data-office_id' => $office['id'],
                            'title' => $checkBox ? ($comment ? $comment . ' ' : '') . Yii::$app->formatter->asDatetime($checkBox['updated_at']) : '',
                        ]
                    ]);
                    ?>
                    <?php if (strpos($checkable, 'viewable') !== false) {
                        print Html::tag('div',
                            DetailsFormatterCall::format($dataInfo['details'], $checkBox['checkListItem']['trigger'], $checkBox['updated_at'], $comment),
                            ['class' => 'details_info']);
                    }
                    ?>
                </td>
            <?php endforeach; ?>
        </tr>
        <tr class="details_holder">
            <td colspan="<?= ($cols + 1) ?>">
                <div></div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>