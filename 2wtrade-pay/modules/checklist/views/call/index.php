<?php
use app\widgets\Panel;
use app\modules\checklist\assets\CheckListAsset;
use app\modules\checklist\widgets\ModalConfirmCheckList;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
/** @var array $offices */
/** @var array $selectedOffices */
/** @var boolean $isCurrentDate */

CheckListAsset::register($this);

$this->title = Yii::t('common', 'Чек-лист Супервайзера КЦ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Чек-лист Супервайзера КЦ')];

?>

<?= Panel::widget([
    'title' => '',
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'offices' => $offices,
    ]),
]) ?>


<?= Panel::widget([
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'modelSearch' => $modelSearch,
        'dataProvider' => $dataProvider,
        'offices' => $offices,
        'selectedOffices' => $selectedOffices,
        'isCurrentDate' => $isCurrentDate,
    ])
])
?>

<?php if (Yii::$app->user->can('checklist.call.set')): ?>
    <?= ModalConfirmCheckList::widget(['url' => '/checklist/call/set']) ?>
<?php endif; ?>
