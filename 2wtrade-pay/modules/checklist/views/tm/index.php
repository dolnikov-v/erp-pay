<?php
use app\widgets\Panel;
use app\modules\checklist\assets\CheckListAsset;
use app\modules\checklist\widgets\ModalConfirmCheckList;
use app\modules\checklist\widgets\TeamCheckList;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
/** @var array $teams */
/** @var array $users */
/** @var boolean $isCurrentDate */

CheckListAsset::register($this);

$this->title = Yii::t('common', 'Чек-лист Руководителя группы стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Чек-лист Руководителя группы стран')];

?>

<?= Panel::widget([
    'title' => '',
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'teams' => $teams,
    ]),
]) ?>


<?= Panel::widget([
    'withBody' => false,
    'content' => TeamCheckList::widget([
        'dataProvider' => $dataProvider,
        'modelSearch' => $modelSearch,
        'teams' => $teams,
        'users' => $users,
        'isCurrentDate' => $isCurrentDate,
        'canCheck' => Yii::$app->user->can('checklist.tm.set'),
    ])
])
?>

<?php if (Yii::$app->user->can('checklist.tm.set')): ?>
    <?= ModalConfirmCheckList::widget(['url' => '/checklist/tm/set']) ?>
<?php endif; ?>
