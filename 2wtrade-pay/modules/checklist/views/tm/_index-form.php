<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/* @var yii\web\View $this */
/* @var \app\modules\checklist\models\search\CheckListSearch $modelSearch */
/* @var array $teams */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'date')->datePicker() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'team_id')->select2List($teams) ?>
        </div>
        <div class="col-md-6">
            <div class="control-label"><label>&nbsp;</label></div>
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>