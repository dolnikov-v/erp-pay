<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use kartik\grid\GridView;;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Label;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\checklist\models\CheckListItem $model */
/** @var \app\modules\checklist\models\search\CheckListItemSearch $modelSearch */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список пунктов чек-листа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Пункты чек-листа')];
?>

<?= Panel::widget([
    'title' => '',
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>


<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пунктами чек листа'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'responsive' => false,
        'bordered' => false,
        'columns' => [
            [
                'attribute' => 'position',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'category_id',
                'enableSorting' => false,
                'group' => true,
                'groupedRow' => true,
                'content' => function ($model) {
                    return $model->category->name ?? '-';
                },
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'days',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'auto',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->auto ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->auto ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'trigger',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'explainable',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->explainable ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->explainable ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('checklist.admin.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('checklist.admin.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('checklist.admin.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить пункт чек листа'),
            'url' => Url::toRoute(['edit', 'type' => $modelSearch->type]),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
