<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\checklist\models\CheckListItem $model */
/** @var array $categories */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'days')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'position')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'description')->textarea() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
        ]) ?>
        <?= $form->field($model, 'auto')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto,
            'uncheckedValue' => 0,
        ]) ?>
        <?= $form->field($model, 'explainable')->checkboxCustom([
            'value' => 1,
            'checked' => $model->explainable,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'trigger')->select2List($model::getTriggersCollection()) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'type')->select2List($model::getTypeCollection()) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'category_id')->select2List($categories[$model->type] ?? [], ['prompt' => '-']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить пункт чек листа') : Yii::t('common', 'Сохранить пункт чек листа')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
