<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\salary\assets\CheckListItemEditAsset;
use yii\web\View;

/** @var \yii\web\View $this */
/** @var \app\modules\checklist\models\CheckListItem $model */
/** @var array $categories */

CheckListItemEditAsset::register($this);

$this->registerJs("var itemCategories = {};", View::POS_HEAD);
foreach ($categories as $categoryType => $categoryList) {
    $this->registerJs("itemCategories['" . $categoryType . "'] = " . json_encode($categoryList) . ";", View::POS_HEAD);
}

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление пункта чек-листа') : Yii::t('common', 'Редактирование пункт чек-листа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Пункт чек-листа'),
    'url' => Url::toRoute('/checklist/admin/index')
];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление пункта чек-листа') : Yii::t('common', 'Редактирование пункта чек-листа')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Пункт чек-листа'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'categories' => $categories,
    ])
]); ?>