<?php

namespace app\modules\checklist\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\models\User;
use app\models\UserCountry;
use app\modules\checklist\components\AutoCheck;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\models\search\CheckListSearch;
use app\modules\reportoperational\models\Team;
use app\modules\reportoperational\models\TeamCountry;
use app\modules\reportoperational\models\TeamCountryUser;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;


/**
 * Class CheckController
 * @package app\modules\checklist\controllers
 */
class CheckController extends Controller
{

    public $type = CheckListItem::TYPE_PAY;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set',
                ],
            ]
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {

        $modelSearch = new CheckListSearch();
        $modelSearch->date = date("Y-m-d");

        $arrayTeam = Team::find()
            ->where(['or',
                ['leader_id' => Yii::$app->user->id],
                ['exists', TeamCountryUser::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->limit(1)
                ]
            ])
            ->groupBy(['id'])->all();

        if (empty($arrayTeam)) { /* все остальные пользователи видят все команды */
            $arrayTeam = Team::find()->groupBy(['id'])->all();
        }
        $teams = ArrayHelper::map($arrayTeam, 'id', 'name');

        $modelSearch->team_id = array_keys($teams)[0];
        $modelSearch->load(Yii::$app->request->queryParams);

        $isCurrentDate = date('Y-m-d', strtotime($modelSearch->date)) == date('Y-m-d');

        $team = Team::findOne($modelSearch->team_id);
        $users = [];
        if ($team) {
            $arrayteamUsers = TeamCountryUser::find()->byTeam($team->id)->groupBy(['user_id'])->all();
            $teamUsers = ArrayHelper::map($arrayteamUsers, 'user_id', 'user_id');
            $teamUsers[$team->leader_id] = $team->leader_id;
            $userQuery = User::find()
                ->where(['id' => $teamUsers])
                ->active()
                ->orderBy(['username' => SORT_ASC]);
            $users = $userQuery->collection();
        }

        // страны выбранной команды
        $teamCountries = ArrayHelper::getColumn(TeamCountry::find()->where(['team_id' => $modelSearch->team_id])->asArray()->all(), 'country_id');

        // распределим их по пользователям
        $tmp = [];
        foreach ($teamCountries as $country) {
            $query = UserCountry::find()
                ->joinWith('country')
                ->joinWith('user')
                ->where([Country::tableName() . '.active' => 1])
                ->andWhere([Country::tableName() . '.id' => $country])
                ->andWhere([UserCountry::tableName() . '.user_id' => array_keys($users)])
                ->orderBy(['name' => SORT_ASC])
                ->asArray()
                ->all();

            if ($query) {
                $relatedCountries = ArrayHelper::map($query, 'country_id', 'country.name');
                $relatedCountriesSlug = ArrayHelper::map($query, 'country_id', 'country.slug');
                $relatedUsers = ArrayHelper::map($query, 'user_id', 'user.username');

                $tmp[] = [
                    'id' => array_keys($relatedUsers),
                    'name' => array_values($relatedUsers),
                    'combine_id' => implode('_', array_keys($relatedUsers)),
                    'country_name' => array_values($relatedCountries)[0],
                    'country_slug' => array_values($relatedCountriesSlug)[0],
                    'country_id' => array_keys($relatedCountries)[0]
                ];
            }
        }

        $users = ArrayHelper::index($tmp, 'country_id', 'combine_id');

        $checkListItems = CheckListItem::find()->with('category')->active()->byType($this->type)->sortPosition()->all();
        foreach ($checkListItems as &$item) {

            $item->checkedLists =
                ArrayHelper::index(
                    CheckList::find()
                        ->with('checkListItem')
                        ->byTeamId($modelSearch->team_id)
                        ->byDate($item->days, $modelSearch->date)
                        ->byItem($item->id)
                        ->asArray()->all(),
                    'country_id'
                );
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => $checkListItems
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'teams' => $teams,
            'users' => $users,
            'isCurrentDate' => $isCurrentDate
        ]);
    }

    /**
     * @throws HttpException
     * @return array
     */
    public function actionSet()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $country_id = Yii::$app->request->post('country_id');
        $team_id = Yii::$app->request->post('team_id');
        $item_id = Yii::$app->request->post('item_id');
        $value = Yii::$app->request->post('value') ?? 0;
        $comment = Yii::$app->request->post('text') ?? '';

        if (!$item_id) {
            $response['message'] = Yii::t('common', 'Пункт не задан');
            return $response;
        }

        if (!$team_id) {
            $response['message'] = Yii::t('common', 'Регион не задан');
            return $response;
        }

        $team = Team::findOne($team_id);
        if (!$team instanceof Team) {
            $response['message'] = Yii::t('common', 'Регион не найден');
            return $response;
        }

        $checkListItem = CheckListItem::findOne($item_id);
        if (!$checkListItem instanceof CheckListItem) {
            $response['message'] = Yii::t('common', 'Пункт не найден');
            return $response;
        }

        if (!$country_id) {
            $response['message'] = Yii::t('common', 'Страна не задана');
            return $response;
        }

        $teamCountries = ArrayHelper::getColumn(TeamCountry::find()->where(['team_id' => $team->id])->asArray()->all(), 'country_id');

        if (!in_array($country_id, $teamCountries)) {
            $response['message'] = Yii::t('common', 'Недопустимая страна для региона');
            return $response;
        }

        if (!array_key_exists($country_id, User::getAllowCountries())) {
            $response['message'] = Yii::t('common', 'Недопустимая страна для пользователя');
            return $response;
        }

        // только сегодняшние ставим
        $date = date('Y-m-d');

        $success = false;
        if ($checkListItem->auto && $checkListItem->trigger) {
            $result = AutoCheck::check($checkListItem->trigger, $country_id, $team_id, $comment);
            $success = $result['status'];
            if (!$success) {
                if (isset($result['errors']) && $result['errors']) {
                    $response['message'] = implode(' ', $result['errors']);
                }
            }
            if (isset($result['info']) && $result['info']) {
                $response['message'] = Yii::t('common', 'Число не соответствий критерию: ') . implode(' ', $result['info']);
            }
        }

        $checkList = CheckList::find()
            ->byTeamId($team->id)
            ->byDate($checkListItem->days, $date)
            ->byCountry($country_id)
            ->byItem($checkListItem->id)
            ->one();

        if ($checkList) {
            $response['value'] = $checkList->status;
            $response['date'] = Yii::$app->formatter->asDatetime($checkList->updated_at);
            $response['comment'] = $checkList->comment;
        }


        if (!$checkListItem->auto) {
            $success = false;
            if (!$checkList instanceof CheckList) {
                $checkList = new CheckList();
                $checkList->team_id = $team->id;
                $checkList->user_id = Yii::$app->user->id;
                $checkList->date = $date;
                $checkList->country_id = $country_id;
                $checkList->check_list_item_id = $checkListItem->id;
            }

            $checkList->status = $value ? CheckList::STATUS_OK : CheckList::STATUS_NEW;

            if ($checkListItem->explainable) {
                $checkList->comment = $comment;
            }

            if ($checkList->save()) {
                $success = true;
                $response['value'] = $checkList->status;
            } else {
                $response['message'] = $checkList->getFirstErrorAsString();
            }
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }
}
