<?php

namespace app\modules\checklist\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\User;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\models\search\CheckListSearch;
use app\modules\salary\models\Office;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;


/**
 * Class AuditorController
 * @package app\modules\checklist\controllers
 */
class AuditorController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set',
                ],
            ]
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {

        $modelSearch = new CheckListSearch();
        $modelSearch->date = date("Y-m-d");

        $officeId = 0;

        $queryOffice = Office::find()->where(['type' => Office::TYPE_CALL_CENTER])->active()->andWhere(['country_id' => array_keys(User::getAllowCountries())]);
        if ($officeId) {
            $queryOffice->byId($officeId);
        }

        $offices = ArrayHelper::map($queryOffice->all(), 'id', 'name');
        $modelSearch->office_id = $selectedTeam = array_keys($offices)[0];
        $modelSearch->load(Yii::$app->request->queryParams);

        $isCurrentDate = date('Y-m-d', strtotime($modelSearch->date)) == date('Y-m-d');

        $selectedOffices = Office::find()->where(['id' => $modelSearch->office_id])->asArray()->all();

        foreach ($selectedOffices as &$office) {
            $office['users'] = User::find()->byRole('callcenter.controller')->byCountry($office['country_id'])->active()->all();
        }

        $checkListItems = CheckListItem::find()->active()->byType(CheckListItem::TYPE_AUDITOR)->sortPosition()->all();
        foreach ($checkListItems as &$item) {
            $item->checkedLists =
                ArrayHelper::index(
                    CheckList::find()
                        ->byOfficeId($modelSearch->office_id)
                        ->byDate($item->days, $modelSearch->date)
                        ->byItem($item->id)
                        ->asArray()->all(),
                    'office_id');
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $checkListItems
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'offices' => $offices,
            'selectedOffices' => $selectedOffices,
            'isCurrentDate' => $isCurrentDate,
        ]);
    }

    /**
     * @throws HttpException
     * @return array
     */
    public function actionSet()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $item_id = Yii::$app->request->post('item_id');
        $office_id = Yii::$app->request->post('office_id');
        $value = Yii::$app->request->post('value') ?? 0;
        $comment = Yii::$app->request->post('text') ?? '';

        if (!$item_id) {
            $response['message'] = Yii::t('common', 'Пункт не задан');
            return $response;
        }

        $checkListItem = CheckListItem::findOne($item_id);
        if (!$checkListItem instanceof CheckListItem) {
            $response['message'] = Yii::t('common', 'Пункт не найден');
            return $response;
        }

        // только сегодняшние ставим
        $date = date('Y-m-d');

        if (!$office_id) {
            $response['message'] = Yii::t('common', 'Офис не задан');
            return $response;
        }

        $office = Office::find()->where(['type' => Office::TYPE_CALL_CENTER])->active()->andWhere(['country_id' => array_keys(User::getAllowCountries())])->byId($office_id)->one();
        if (!$office instanceof Office) {
            $response['message'] = Yii::t('common', 'Офис не доступен');
            return $response;
        }


        $success = false;

        $checkList = CheckList::find()
            ->byOfficeId($office_id)
            ->byDate($checkListItem->days, $date)
            ->byItem($checkListItem->id)
            ->one();

        if ($checkList) {
            $response['value'] = $checkList->status;
            $response['date'] = Yii::$app->formatter->asDatetime($checkList->updated_at);
            $response['comment'] = $checkList->comment;
        }


        if (!$checkListItem->auto) {
            $success = false;
            if (!$checkList instanceof CheckList) {
                $checkList = new CheckList();
                $checkList->user_id = Yii::$app->user->id;
                $checkList->date = $date;
                $checkList->office_id = $office_id;
                $checkList->check_list_item_id = $checkListItem->id;
            }

            $checkList->status = $value ? CheckList::STATUS_OK : CheckList::STATUS_NEW;

            if ($checkListItem->explainable) {
                $checkList->comment = $comment;
            }

            if ($checkList->save()) {
                $success = true;
                $response['value'] = $checkList->status;
                $response['date'] = Yii::$app->formatter->asDatetime($checkList->updated_at);
            } else {
                $response['message'] = $checkList->getFirstErrorAsString();
            }
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }
}
