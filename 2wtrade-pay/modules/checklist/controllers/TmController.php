<?php

namespace app\modules\checklist\controllers;

use app\modules\checklist\models\CheckListItem;

/**
 * Class TmController
 * @package app\modules\checklist\controllers
 */
class TmController extends CheckController
{
    public $type = CheckListItem::TYPE_TM;
}
