<?php

namespace app\modules\checklist\controllers;

use app\components\web\Controller;
use app\modules\checklist\models\CheckListCategory;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\models\search\CheckListCategorySearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class AdminCategoryController
 * @package app\modules\checklist\controllers
 */
class AdminCategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CheckListCategorySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $type = Yii::$app->request->get('type');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new CheckListCategory();
            $model->type = $type;
        }

        if ($model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Категория успешно добавлена.') : Yii::t('common', 'Категория успешно отредактирована.'), 'success');

                return $this->redirect(Url::to([
                    'index',
                    'CheckListCategorySearch' => [
                        'type' => $model->type
                    ],
                ]));

            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $type = $model->type;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Категория успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        return $this->redirect(Url::to([
            'index',
            'CheckListCategorySearch' => [
                'type' => $type
            ],
        ]));
    }

    /**
     * @param integer $id
     * @return CheckListCategory
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = CheckListCategory::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Категория не найдена.'));
        }

        return $model;
    }
}
