<?php

namespace app\modules\checklist\controllers;

use app\components\web\Controller;
use app\modules\checklist\models\CheckListCategory;
use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\models\search\CheckListItemSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class AdminController
 * @package app\modules\checklist\controllers
 */
class AdminController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CheckListItemSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = $dataProvider->totalCount;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $type = Yii::$app->request->get('type');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new CheckListItem();
            $model->type = $type;
        }

        if ($model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Пункт успешно добавлен.') : Yii::t('common', 'Пункт успешно отредактирован.'), 'success');

                return $this->redirect(Url::to([
                    'index',
                    'CheckListItemSearch' => [
                        'type' => $model->type
                    ],
                ]));

            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $categories = ArrayHelper::map(CheckListCategory::find()
            ->orderBy(['type' => SORT_ASC, 'position' => SORT_ASC])
            ->asArray()
            ->all(),
            'id', 'name', 'type'
        );

        return $this->render('edit', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $type = $model->type;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пункт успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        return $this->redirect(Url::to([
            'index',
            'CheckListItemSearch' => [
                'type' => $type
            ],
        ]));
    }

    /**
     * @param integer $id
     * @return CheckListItem
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = CheckListItem::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Пункт не найден.'));
        }

        return $model;
    }
}
