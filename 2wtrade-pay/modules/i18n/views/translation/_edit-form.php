<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\i18n\models\Message $model */
/** @var array $translationForm */
/** @var \app\modules\i18n\models\Language[] $languages */
/** @var \app\modules\i18n\models\SourceMessage $sourceMessage */
?>

<?php $form = ActiveForm::begin(['action' => $translationForm['action']]); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'language')->select2List($languages) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($sourceMessage, 'message')->textarea(['disabled' => 'disabled']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'translation')->textarea() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить перевод') : Yii::t('common', 'Сохранить перевод')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
