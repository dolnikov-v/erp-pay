<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\i18n\models\search\MessageSearch $modelSearch */
/** @var \app\modules\i18n\models\Language[] $languages */
/** @var \app\modules\i18n\models\Language[] $languagesAll */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список переводов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Переводы')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'languages' => $languages,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с переводами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'language',
                'contentOptions' => ['class' => 'width-200'],
                'content' => function ($data) use ($languagesAll) {
                    $name = isset($languagesAll[$data->language]) ? $languagesAll[$data->language] : '';
                    return $name . ' (' . $data->language . ')';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'sourceMessage.message',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'translation',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id, 'language' => $data->language]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Добавить еще перевод'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
