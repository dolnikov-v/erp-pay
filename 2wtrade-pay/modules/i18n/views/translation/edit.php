<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\components\grid\GridView;
use app\components\grid\ActionColumn;

/** @var \yii\web\View $this */
/** @var \app\modules\i18n\models\Message $model */
/** @var array $translationForm */
/** @var \app\modules\i18n\models\Language[] $languages */
/** @var \app\modules\i18n\models\SourceMessage $sourceMessage */
/** @var \app\modules\i18n\models\Language[] $languagesAll */
/** @var \app\modules\i18n\models\Message[] $translations */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление перевода') : Yii::t('common', 'Редактирование перевода');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список переводов'), 'url' => Url::toRoute('/i18n/translation/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление перевода') : Yii::t('common', 'Редактирование перевода')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Перевод'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'translationForm' => $translationForm,
        'languages' => $languages,
        'sourceMessage' => $sourceMessage,
    ])
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Существующие переводы'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $translations,
        'columns' => [
            [
                'attribute' => 'language',
                'contentOptions' => ['class' => 'width-200'],
                'content' => function ($data) use ($languagesAll) {
                    $name = isset($languagesAll[$data->language]) ? $languagesAll[$data->language] : '';

                    return $name . ' (' . $data->language . ')';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'translation',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id, 'language' => $data->language]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                ]
            ]
        ],
    ])
]); ?>
