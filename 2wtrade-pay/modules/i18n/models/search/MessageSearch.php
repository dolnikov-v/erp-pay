<?php
namespace app\modules\i18n\models\search;

use app\modules\i18n\models\Language;
use app\modules\i18n\models\Message;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class MessageSearch
 * @package app\modules\i18n\models\search
 */
class MessageSearch extends Message
{
    public $phrase;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'translation', 'phrase'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['language'] = SORT_ASC;
        }

        $languagesKeys = array_keys(Language::find()->collection());

        $query = Message::find();

        $query->andFilterWhere(['in', 'language', $languagesKeys]);

        $query->joinWith('sourceMessage')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['language' => $this->language]);
        $query->andFilterWhere(['like', 'translation', $this->translation]);
        $query->andFilterWhere(['like', 'i18n_source_message.message', $this->phrase]);

        return $dataProvider;
    }
}
