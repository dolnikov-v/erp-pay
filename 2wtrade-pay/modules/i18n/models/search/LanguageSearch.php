<?php
namespace app\modules\i18n\models\search;

use app\modules\i18n\models\Language;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class LanguageSearch
 * @package app\modules\i18n\models\search
 */
class LanguageSearch extends Language
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [
            'active' => SORT_DESC,
        ];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Language::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
