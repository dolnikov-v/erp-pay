<?php

namespace app\modules\i18n\models\search;

use app\modules\i18n\models\Language;
use app\modules\i18n\models\Message;
use app\modules\i18n\models\SourceMessage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SourceMessageSearch
 * @package app\models\i18n
 */
class SourceMessageSearch extends SourceMessage
{
    /**
     * @var string
     */
    public $translationState = 'no';

    /**
     * @var array
     */
    public $activeLanguages = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->activeLanguages = Language::find()->collection();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'message'], 'safe'],
            ['translationState', 'in', 'range' => ArrayHelper::merge(['', 'no', 'yes', 'partial'], array_keys($this->activeLanguages))],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(['translationState' => Yii::t('common', 'Перевод')], parent::attributeLabels());
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['category'] = SORT_ASC;
            $sort['id'] = SORT_ASC;
        }

        $activeLanguages = array_keys($this->activeLanguages);
        $countActiveTranslations = count($activeLanguages);
        $str = implode("', '", $activeLanguages);

        $query = SourceMessage::find()
            ->select([
                SourceMessage::tableName() . '.*',
                'translation_count' => "(SELECT COUNT(*) FROM " . Message::tableName() . " WHERE language IN ('" . $str . "') AND id = " . SourceMessage::tableName() . ".id)",
            ])
            ->with('messages')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if ($params) {
            if (!$this->load($params) || !$this->validate()) {
                return $dataProvider;
            }
        }

        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);

        if ($this->translationState == 'yes') {
            $query->having('translation_count = ' . $countActiveTranslations);
        } elseif ($this->translationState == 'no') {
            $query->having('translation_count = 0');
        } elseif ($this->translationState == 'partial') {
            $query->having('translation_count > 0');
            $query->andHaving('translation_count < ' . $countActiveTranslations);
        } elseif (!empty($this->translationState)) {
            $query->addSelect(['translation_count' => "(SELECT COUNT(*) FROM " . Message::tableName() . " WHERE language = '" . $this->translationState . "' AND id = " . SourceMessage::tableName() . ".id)"]);
            $query->having('translation_count = 0');
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function translationStateList()
    {
        $tmpList = [];
        foreach ($this->activeLanguages as $languageId => $language) {
            $tmpList[$languageId] = Yii::t('common', 'Отсутствует {language}', ['language' => "{$language} ($languageId)"]);
        }
        return ArrayHelper::merge([
            'no' => Yii::t('common', 'Отсутствует'),
            'partial' => Yii::t('common', 'Частичный'),
            'yes' => Yii::t('common', 'Полный'),
        ], $tmpList);
    }
}
