<?php
namespace app\modules\i18n\models\query;

use app\components\db\ActiveQuery;
use app\modules\i18n\models\Language;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Class LanguageQuery
 * @package app\models\i18n
 *
 * @method Language one() one($db = null)
 * @method Language[] all() all($db = null)
 */
class LanguageQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'locale';

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this
            ->active()
            ->notSourceLanguage()
            ->orderBy(['active' => SORT_DESC, 'id' => SORT_ASC])
            ->all(),
            $this->collectionKey,
            $this->collectionValue);
    }

    /**
     * @return array
     */
    public function collectionAll()
    {
        return ArrayHelper::map($this
            ->orderBy(['active' => SORT_DESC, 'id' => SORT_ASC])
            ->all(),
            $this->collectionKey,
            $this->collectionValue);
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['active' => 1]);

        return $this;
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        $this->andWhere(['active' => 0]);

        return $this;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function byLocale($locale)
    {
        $this->andWhere(['locale' => $locale]);

        return $this;
    }

    /**
     * @return $this
     */
    public function sourceLanguage()
    {
        $this->andWhere(['source_language' => 1]);

        return $this;
    }

    /**
     * @return $this
     */
    public function notSourceLanguage()
    {
        $this->andWhere(['source_language' => 0]);

        return $this;
    }
}
