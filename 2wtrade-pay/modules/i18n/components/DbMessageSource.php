<?php
namespace app\modules\i18n\components;

use yii\db\Query;
use yii\i18n\MissingTranslationEvent;

/**
 * Class DbMessageSource
 * @package app\components\i18n
 */
class DbMessageSource extends \yii\i18n\DbMessageSource
{
    /**
     * @param MissingTranslationEvent $event
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $source = \Yii::$app->i18n->getMessageSource($event->category);
        if (($source instanceof \yii\i18n\DbMessageSource) && !(new Query())->from($source->sourceMessageTable)->where([
                'category' => $event->category,
                'message' => $event->message
            ])->exists($source->db)) {
            $source->db->createCommand()->insert($source->sourceMessageTable, [
                'category' => $event->category,
                'message' => $event->message
            ])->execute();
        }
    }
}
