<?php
use app\assets\vendor\AnimsitionAsset;
use app\modules\auth\assets\AuthAsset;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\auth\models\LoginForm $model */

AnimsitionAsset::register($this);
AuthAsset::register($this);
$this->title = Yii::t('common', 'Авторизация');
?>

<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <?= Panel::widget([
            'content' => $this->render('_login-form', [
                'model' => $model,
            ]),
        ]) ?>

        <div class="page-copyright page-copyright-inverse">
            <p>© <?= date('Y') ?>. All RIGHT RESERVED.</p>
        </div>
    </div>
</div>
