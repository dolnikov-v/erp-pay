<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\custom\Checkbox;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\auth\models\LoginForm $model */
?>

<div class="brand">
    <img class="brand-img" src="<?= $this->theme->getUrl('/images/logo-blue.png'); ?>" alt="2Wtrade"/>
    <h2 class="brand-text font-size-18">2Wtrade</h2>
</div>
<?php $form = ActiveForm::begin(['id' => 'login_form', 'action' => '/auth/login']); ?>
<div class="form-group form-material floating">
    <input class="form-control <?php if (empty($model->username)): ?>empty<?php endif; ?>" name="<?= Html::getInputName($model, 'username') ?>"
           value="<?= $model->username ?>"
           type="text"/>
    <label class="floating-label"><?= Yii::t('common', 'Имя пользователя') ?></label>
</div>
<div class="form-group form-material floating">
    <input class="form-control <?php if (empty($model->password)): ?>empty<?php endif; ?>" name="<?= Html::getInputName($model, 'password') ?>"
           value="<?= $model->password ?>"
           type="password"/>
    <label class="floating-label"><?= Yii::t('common', 'Пароль') ?></label>
</div>
<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'rememberMe'),
        'name' => Html::getInputName($model, 'rememberMe'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Запомнить'),
        'checked' => $model->rememberMe ? true : false,
    ]) ?>
</div>
<?= $form->submit(Yii::t('common', 'Войти'), [
    'size' => Button::SIZE_LARGE,
    'style' => Button::STYLE_PRIMARY . ' btn-block margin-top-40',
]) ?>
<?php ActiveForm::end(); ?>
