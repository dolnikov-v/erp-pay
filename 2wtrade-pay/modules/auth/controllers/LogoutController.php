<?php

namespace app\modules\auth\controllers;

use app\components\web\Controller;
use Yii;
use yii\filters\AccessControl;

/**
 * Class LogoutController
 * @package app\controllers\auth
 */
class LogoutController extends Controller
{
    public $layout = 'auth';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Выйти
     */
    public function actionIndex()
    {
        Yii::$app->user->logout();
        return $this->redirect('/auth/login');
    }
}
