<?php
namespace app\modules\auth\controllers;

use app\modules\auth\components\filters\AccessControl;
use app\components\web\Controller;
use app\modules\auth\models\LoginForm;
use Yii;

/**
 * Class LoginController
 * @package app\modules\auth\controllers
 */
class LoginController extends Controller
{
    public $layout = 'auth';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                if (Yii::$app->user->identity->force_logout) {
                    Yii::$app->user->identity->force_logout = 0;
                    Yii::$app->user->identity->save(false, ['force_logout']);
                }

                return $this->goBack();
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
