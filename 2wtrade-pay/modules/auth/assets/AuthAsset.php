<?php
namespace app\modules\auth\assets;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class AuthAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/examples';

    public $css = [
        'css/pages/login-v3.min.css',
        'css/pages/login-v3.custom.css',
    ];

    public $js = [
        'js/pages/login-v3.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
