<?php

namespace app\modules\auth\models;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Class LoginForm
 * @package app\models
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;

    /** @var \app\models\User */
    private $_user = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            ['username', 'validateUser'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'rememberMe' => Yii::t('common', 'Запомнить'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('common', 'Неверное имя пользователя или пароль.'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if ($user && $user->hasStatus(User::STATUS_BLOCKED)) {
                $this->addError($attribute, Yii::t('common', 'Пользователь {username} заблокирован.', ['username' => $this->username]));
            }
        }
    }

    /**
     * @return null|\app\models\User
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }
}
