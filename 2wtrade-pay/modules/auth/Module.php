<?php
namespace app\modules\auth;

/**
 * Class Module
 * @package app\modules\administration
 */
class Module extends \app\components\base\Module
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }
}
