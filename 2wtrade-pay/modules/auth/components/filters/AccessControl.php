<?php

namespace app\modules\auth\components\filters;

use Yii;
use yii\filters\AccessControl as YiiAccessControl;

/**
 * Class AccessControl
 * @package app\modules\auth\components\filters
 */
class AccessControl extends YiiAccessControl
{
    /**
     * @param \yii\web\User $user
     */
    protected function denyAccess($user)
    {
        if ($user->getIsGuest()) {
            $user->loginRequired();
        } else {
            Yii::$app->response->redirect(Yii::$app->getHomeUrl());
            Yii::$app->end();
        }
    }
}
