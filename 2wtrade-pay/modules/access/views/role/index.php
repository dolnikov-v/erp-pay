<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var \app\models\search\AuthItemSearch $searchModel */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список ролей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'searchModel' => $searchModel
                ]),
            ]
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с ролями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Название'),
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Описание'),
                'content' => function ($data) {
                    return Yii::t('common', $data->description);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'createdAt',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'name' => $data->name]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.view');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'name' => $data->name]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($data) {
                            return Url::toRoute(['log', 'id' => $data->name]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.log');
                        },
                    ],
                    [
                        'can' => function () {
                            return (Yii::$app->user->can('access.role.view')
                                || Yii::$app->user->can('access.role.edit'))
                            && Yii::$app->user->can('access.role.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить роль'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'name' => $data->name]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
