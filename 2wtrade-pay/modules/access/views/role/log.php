<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список ролей'), 'url' => Url::toRoute('/access/role/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab">AuthItem</a></li>
        <li><a href="#tabChild" aria-controls="tabChild" role="tab" data-toggle="tab">AuthItemChild</a></li>
        <li><a href="#tabTicket" aria-controls="tabTicket" role="tab" data-toggle="tab">TicketRole</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Log::widget([
                'data' => $changeLog,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabChild">
            <?= Log::widget([
                'data' => $childLog,
                'showLinks' => true,
                'showData' => false,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabTicket">
            <?= Log::widget([
                'data' => $ticketLog,
                'showLinks' => true,
            ]); ?>
        </div>
    </div>
</div>
