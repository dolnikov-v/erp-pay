<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\TicketGroup;
use yii\helpers\ArrayHelper;

/** @var app\models\AuthItem $model */
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::toRoute('edit') : Url::toRoute([
        'edit',
        'name' => $model->name
    ])
]); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <?= $form->field($model, 'description')->textInput(); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= $form->field($model, 'ticket_group')->select2List(ArrayHelper::map(TicketGroup::find()->all(),
                'prefix', 'name'), ['prompt' => Yii::t('common', 'Выберите группу')]) ?>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить роль') : Yii::t('common',
            'Сохранить роль')); ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
