<?php
use app\modules\access\widgets\Permission;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\AuthItemForm $model */
/** @var \yii\rbac\Permission[] $permissionList */
/** @var array $permissionsByRole */

$this->title = Yii::t('common', 'Просмотр роли');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список ролей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Роль'),
    'collapse' => true,
    'content' => $this->render('_view-form', [
        'model' => $model,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список операций'),
    'withBody' => false,
    'content' => Permission::widget([
        'model' => $model,
        'permissionList' => $permissionList,
        'permissionsByRole' => $permissionsByRole,
        'disabled' => true,
    ]),
]) ?>
