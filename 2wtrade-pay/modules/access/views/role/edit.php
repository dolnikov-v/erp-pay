<?php
use app\modules\access\assets\RoleAsset;
use app\modules\access\widgets\Permission;
use app\modules\widget\widgets\WidgetRole;
use app\widgets\grid\ActionsDropdown;
use app\widgets\Link;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\web\View;
use \app\modules\access\widgets\NotificationRole;
use app\modules\access\widgets\TicketRole;

/** @var \yii\web\View $this */
/** @var \yii\rbac\Permission[] $permissionList */
/** @var array $permissionsByRole */
/** @var array $widgets */
/** @var array $widgetsByRole */
/** @var \app\models\AuthItem $model */
/** @var app\models\search\AuthItemSearch $modelSearch */

RoleAsset::register($this);

Yii::$app->view->registerJs("var Vars = {};", View::POS_HEAD);
Yii::$app->view->registerJs("Vars['sections'] = " . json_encode($modelSearch->sections) . ";", View::POS_HEAD);

$this->title = Yii::t('common', $model->isNewRecord ? 'Добавление роли' : 'Редактирование роли');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список ролей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Роль'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

<?php if (!$model->isNewRecord): ?>
    <?php
    $links = [];

    foreach ($modelSearch->sectionsCollection as $sectionName => $sectionLabel) {
        $links[] = new Link([
            'url' => '#',
            'style' => 'permission-filter-section',
            'label' => $sectionLabel,
            'attributes' => [
                'role' => 'menuitem',
                'data-name' => $sectionName,
            ],
        ]);
    }
    ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список операций'),
        'withBody' => false,
        'actions' => ActionsDropdown::widget([
            'label' => Yii::t('common', 'Раздел'),
            'links' => $links,
            'attributes' => [
                'data-default-name' => Yii::t('common', 'Список операций'),
            ],
        ]),
        'content' => Permission::widget([
            'model' => $model,
            'permissionList' => $permissionList,
            'permissionsByRole' => $permissionsByRole,
        ]),
    ]) ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список виджетов'),
        'collapse' => true,
        'withBody' => false,
        'content' => WidgetRole::widget([
            'model' => $model,
            'widgets' => $widgets,
            'widgetsByRole' => $widgetsByRole,
        ]),
    ]) ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список оповещений'),
        'withBody' => false,
        'content' => NotificationRole::widget([
            'model' => $model,
        ]),
    ]) ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список просматриваемых тикетных групп'),
        'withBody' => false,
        'content' => TicketRole::widget([
            'model' => $model,
        ]),
    ]) ?>
<?php endif; ?>
