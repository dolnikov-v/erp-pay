<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\models\search\AuthItemSearch $searchModel */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'name')
                ->textInput(['placeholder' => Yii::t('common', 'Название')])
                ->label(Yii::t('common', 'Название')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>