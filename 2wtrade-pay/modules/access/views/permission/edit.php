<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\access\widgets\Role;

/** @var \yii\web\View $this */
/** @var \app\models\AuthItem $model */
/** @var \yii\rbac\Role $roleList */
/** @var array $rolesByPermission */

$this->title = Yii::t('common', $model->isNewRecord ? 'Добавление операции' : 'Редактирование операции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список операций'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Операция'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список ролей'),
        'withBody' => false,
        'content' => Role::widget([
            'model' => $model,
            'roleList' => $roleList,
            'rolesByPermission' => $rolesByPermission,
        ]),
    ]) ?>
<?php endif; ?>