<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\search\AuthItemSearch $searchModel */
/** @var \yii\data\ArrayDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список операций');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $searchModel,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с операциями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Операция'),
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Описание'),
                'content' => function ($data) {
                    return Yii::t('common', $data->description);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'createdAt',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'name' => urlencode($data->name)]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.edit');
                        },
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.edit') && Yii::$app->user->can('access.permission.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить операцию'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'name' => urlencode($data->name)]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider,
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
