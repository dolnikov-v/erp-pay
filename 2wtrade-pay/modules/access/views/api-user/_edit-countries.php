<?php
use app\modules\access\assets\ListGroupAsset;
use app\widgets\ListGroup;
use app\widgets\ListGroupItem;
use yii\helpers\Url;

/** @var app\models\api\ApiUser $model */
/** @var \app\models\Country[] $countries */
/** @var \yii\web\View $this */
/** @var array $activeDeliveries */

?>

<div class="list-group-block"
     data-user-id="<?= $model->id ?>"
     data-change-url="<?= Url::toRoute(['change-delivery']) ?>"
     data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
     data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>">

    <?php
    $headers = [];

    foreach ($countries as $country) {
        $items = [];

        foreach ($country->deliveries as $delivery) {
            /** @var \app\modules\delivery\models\Delivery $delivery */
            $items[] = new ListGroupItem([
                'content' => $this->render('_edit-countries-item', [
                    'delivery' => $delivery,
                    'activeDeliveries' => $activeDeliveries
                ])
            ]);
        }

        echo ListGroup::widget([
            'header' => $country->name,
            'items' => $items,
        ]);
    }
    ?>
</div>



