<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\models\api\ApiUser;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \app\models\search\ApiUserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var ApiUser $model */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список API пользователей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form.php', [
        'searchModel' => $searchModel,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с API пользователями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'username',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => ApiUser::getStatuses()[$model->status],
                        'style' => $model->status == ApiUser::STATUS_ACTIVE ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                    ]);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.apiuser.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/access/api-user/change-status', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('access.apiuser.changestatus') && !$model->status;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/access/api-user/change-status', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('access.apiuser.changestatus') && $model->status;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить пользователя'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.apiuser.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
