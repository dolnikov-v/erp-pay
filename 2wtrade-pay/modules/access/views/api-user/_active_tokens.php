<?php

use app\components\grid\GridView;
use app\modules\access\assets\ListGroupAsset;
use app\components\grid\ActionColumn;
use yii\helpers\Url;
use app\components\grid\DateColumn;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\Country[] $countries */
/** @var \yii\web\View $this */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model) {
    },
    'columns' => [
        [
            'attribute' => 'auth_token',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'lifetime',
            'enableSorting' => false,
            'headerOptions' => ['class' => 'th-date text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function ($model) {
                return empty($model->lifetime) ? Yii::t('common', 'Бессрочный') : Yii::$app->formatter->asDatetime($model->lifetime);
            }
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Сделать бессрочным'),
                    'url' => function ($model) {
                        return Url::toRoute(['make-token-unlimited', 'id' => $model->id]);
                    },
                    'can' => function ($model) {
                        return Yii::$app->user->can('access.apiuser.maketokenunlimited') && $model->lifetime;
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function ($model) {
                        return Url::toRoute(['delete-token', 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('access.apiuser.deletetoken');
                    },
                ],
            ]
        ],
    ]
])
?>



