<?php
use app\modules\access\widgets\ApiUserDeliverySwitchery;

/** @var \app\modules\delivery\models\Delivery $delivery */
/** @var array $activeDeliveries */

?>

<li class="list-group-item"
    data-delivery-id="<?= $delivery->id ?>">
    <?= $delivery->name ?>
    <span class="pull-right">
        <?= ApiUserDeliverySwitchery::widget([
            'checked' => in_array($delivery->id, $activeDeliveries),
        ]); ?>
    </span>
</li>
