<?php
use app\widgets\Panel;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\api\ApiUser $model */
/** @var ActiveDataProvider $tokens */
/** @var \app\models\Country[] $countries */
/** @var array $activeDeliveries */

$this->title = !$model->isNewRecord ? Yii::t('common', 'Редактирование пользователя') : Yii::t('common', 'Добавление пользователя');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные пользователя'),
    'alert' => !$model->isNewRecord ? Yii::t('common', 'Если вы не хотите менять пароль, то необходимо оставить поле «Пароль» пустым.') : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ]),
]) ?>

<?php if ($tokens->count): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Активные токены'),
        'content' => $this->render('_active_tokens', [
            'dataProvider' => $tokens
        ]),
    ]) ?>
<?php endif; ?>

<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Разрешенные страны'),
        'content' => $this->render('_edit-countries', [
            'activeDeliveries' => $activeDeliveries,
            'countries' => $countries,
            'model' => $model,
        ]),
    ]) ?>
<?php endif; ?>
