<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\models\search\ApiUserSearch $searchModel */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('/access/api-user/index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($searchModel, 'username')->textInput(['placeholder' => Yii::t('common', 'Имя пользователя')]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('/access/api-user/index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
