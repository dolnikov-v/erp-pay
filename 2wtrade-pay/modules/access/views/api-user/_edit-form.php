<?php

use app\components\widgets\ActiveForm;
use app\models\api\ApiUser;
use yii\helpers\Url;

/** @var ApiUser $model */
/** @var ActiveForm $form */

$form = ActiveForm::begin([
    'id' => 'user_form',
    'action' => $model->isNewRecord ? Url::toRoute('edit') : Url::toRoute(['edit', 'id' => $model->id])
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'password')->textGeneratePasswordInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'status')->select2List(ApiUser::getStatuses(), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'source_id')->select2List(\app\models\Source::find()
            ->collection(), ['prompt' => '—']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(!$model->isNewRecord ? Yii::t('common', 'Сохранить пользователя') : Yii::t('common', 'Добавить пользователя')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
