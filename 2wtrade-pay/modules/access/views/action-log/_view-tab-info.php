<?php
use app\components\widgets\ActiveForm;

/** @var \app\models\UserActionLog $model */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'url')->textInput(['disabled' => 'disabled']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($model, 'http_status')->textInput(['disabled' => 'disabled']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'created_at')->textInput([
            'value' => Yii::$app->formatter->asDatetime($model->created_at),
            'disabled' => 'disabled',
        ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
