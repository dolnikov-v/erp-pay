<?php
use yii\helpers\Html;

/** @var \app\models\UserActionLogQuery[] $queries */
?>

<table class="table table-striped margin-bottom-0">
    <?php if ($queries): ?>
        <thead>
        <tr>
            <th class="th-record-id">#</th>
            <th><?= Yii::t('common', 'SQL-запрос') ?></th>
            <th class="th-date"><?= Yii::t('common', 'Дата добавления') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($queries as $query): ?>
            <tr>
                <td><?= $query->id ?></td>
                <td><?= Html::encode($query->query) ?></td>
                <td class="text-center"><?= Yii::$app->formatter->asDatetime($query->created_at) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Данные отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
</table>
