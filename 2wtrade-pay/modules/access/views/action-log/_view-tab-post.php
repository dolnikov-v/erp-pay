<?php
use yii\helpers\Html;
use yii\helpers\VarDumper;

/** @var \app\models\UserActionLog $model */

?>
<table class="table table-striped margin-bottom-0">
    <?php if ($model->post_data = json_decode($model->post_data, true)): ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Переменная') ?></th>
            <th><?= Yii::t('common', 'Значение') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->post_data as $varName => $varValue): ?>
            <tr>
                <td><?= Html::encode($varName) ?></td>
                <td class="ellipsis"><?= VarDumper::dumpAsString($varValue) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Данные отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
</table>
