<?php
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\UserActionLog $model */
/** @var app\models\UserActionLogQuery[] $queries */

$this->title = Yii::t('common', 'Просмотр действия');

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Доступы'),
    'url' => '#'
];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Пользователь {name}', [
        'name' => $model->user->username
    ]),
    'url' => Url::toRoute(['user/view', 'id' => $model->user->id])
];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Просмотр действия'),
];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Информация о переходе'),
    'border' => false,
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Общая информация'),
                'content' => $this->render('_view-tab-info', [
                    'model' => $model,
                ]),
            ],
            [
                'label' => Yii::t('common', 'SQL-запросы'),
                'content' => $this->render('_view-tab-sql', [
                    'queries' => $queries,
                ]),
            ],
            [
                'label' => Yii::t('common', 'GET-параметры'),
                'content' => $this->render('_view-tab-get', [
                    'model' => $model
                ]),
            ],
            [
                'label' => Yii::t('common', 'POST-параметры'),
                'content' => $this->render('_view-tab-post', [
                    'model' => $model
                ]),
            ],
        ]
    ]),
]) ?>
