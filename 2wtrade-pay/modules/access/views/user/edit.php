<?php
use app\modules\access\widgets\Country;
use app\modules\access\widgets\Source;
use app\modules\access\widgets\Delivery;
use app\widgets\Panel;
use app\modules\access\widgets\NotificationUser;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\User $model */
/** @var app\modules\access\models\VpnUser $vpnUserModel */
/** @var app\models\Country[] $countries */
/** @var app\models\Country[] $partnerCountries */
/** @var app\models\Country[] $distributorCountries */
/** @var app\models\Source[] $sources */
/** @var app\modules\delivery\models\Delivery[] $deliveries */
/** @var array $currentRoles */
/** @var array $roles */
/** @var array $countriesByUser */
/** @var array $sourcesByUser */
/** @var array $deliveriesByUser */
/** @var array $userForNotification */
/** @var array $modelNotification */

$this->title = !$model->isNewRecord ? Yii::t('common', 'Редактирование пользователя') : Yii::t('common', 'Добавление пользователя');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные пользователя'),
    'alert' => !$model->isNewRecord ? Yii::t('common', 'Если вы не хотите менять пароль, то необходимо оставить поле «Пароль» пустым.') : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'vpnUserModel' => $vpnUserModel,
        'currentRoles' => $currentRoles,
        'roles' => $roles,
    ]),
]) ?>

<?php if (!$model->isNewRecord): ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список стран'),
        'collapse' => true,
        'withBody' => false,
        'content' => Country::widget([
            'model' => $model,
            'countryList' => $countries,
            'countriesByUser' => $countriesByUser,
        ]),
    ]) ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Страны - партнеры'),
        'collapse' => true,
        'withBody' => false,
        'content' => Country::widget([
            'model' => $model,
            'countryList' => $partnerCountries,
            'countriesByUser' => $countriesByUser,
        ]),
    ]) ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Страны - дистрибьюторы'),
        'collapse' => true,
        'withBody' => false,
        'content' => Country::widget([
            'model' => $model,
            'countryList' => $distributorCountries,
            'countriesByUser' => $countriesByUser,
        ]),
    ]) ?>

<?= Panel::widget([
        'title' => Yii::t('common', 'Список источников'),
        'collapse' => true,
        'withBody' => false,
        'content' => Source::widget([
            'model' => $model,
            'sourcesList' => $sources,
            'sourcesByUser' => $sourcesByUser,
        ]),
    ]) ?>

<?php   if (Yii::$app->user->can('access.user.setdelivery')) { ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список служб доставки'),
        'collapse' => true,
        'withBody' => false,
        'content' => Delivery::widget([
            'model' => $model,
            'deliveryList' => $deliveries,
            'deliveriesByUser' => $deliveriesByUser,
        ]),
    ]) ?>

<?php   } ?>

<?php   if (Yii::$app->user->can('access.user.setnotification')) { ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список оповещений'),
        'collapse' => true,
        'withBody' => false,
        'content' => NotificationUser::widget([
            'model' => $model,
        ]),
    ]) ?>

<?php   } ?>

<?php endif; ?>
