<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\models\search\UserSearch $searchModel */
/** @var app\models\Country[] $country */
/** @var array $roles */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('user/index'), 'method' => 'get']); ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($searchModel, 'role')->select2List($roles, ['prompt' => '—']); ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($searchModel, 'username')->textInput(['placeholder' => Yii::t('common', 'Имя пользователя')]); ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($searchModel, 'email')->textInput(); ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($searchModel, 'country')->select2List($country, ['prompt' => '—']); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')) ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('/access/user/index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
