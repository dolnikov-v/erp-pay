<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\models\User;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\access\assets\UserAsset;

/** @var yii\web\View $this */
/** @var app\models\search\UserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\Country[] $country */
/** @var array $roles */
/** @var User $model */

ModalConfirmDeleteAsset::register($this);
UserAsset::register($this);

$this->title = Yii::t('common', 'Список пользователей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form.php', [
        'searchModel' => $searchModel,
        'roles' => $roles,
        'country' => $country,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пользователями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            /** @var $model User */
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->hasStatus(User::STATUS_BLOCKED) ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'photo_id',
                'content' => function ($model) {
                    /** @var $model User */
                    return Html::img($model->getAvatarUrl(false));
                },
                'contentOptions' => ['class' => 'avatar-user'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'username',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-200'],
            ],
            [
                'attribute' => 'parent_id',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-200'],
                'value' => function ($model) {
                    return $model->parent->username ?? '';
                },
            ],
            [
                'attribute' => 'email',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-100'],
            ],
            [
                'attribute' => 'rolesNames',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    $pingInfo = User::getPingInfo($model);

                    return Label::widget([
                        'style' => $pingInfo['class'],
                        'label' => $pingInfo['label'],
                    ]);
                },
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'lastact_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($model) {
                            return Url::toRoute(['view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.view');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Разблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['unblock', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var $model User */
                            return Yii::$app->user->can('access.user.unblock') && $model->hasStatus(User::STATUS_BLOCKED);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Заблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['block', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var $model User */
                            return Yii::$app->user->can('access.user.block') && !$model->hasStatus(User::STATUS_BLOCKED);
                        },
                    ],
                    [
                        'can' => function () {
                            return (Yii::$app->user->can('access.user.view')
                                    || Yii::$app->user->can('access.user.edit')
                                    || Yii::$app->user->can('access.user.unblock')
                                    || Yii::$app->user->can('access.user.block'))
                                && Yii::$app->user->can('access.user.delete');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['log', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.log');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить пользователя'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
