<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\models\User $model */
/** @var app\models\search\UserActionLogSearch $searchModel */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['view', 'id' => $model->id]), 'method' => 'get']); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($searchModel, 'url')->textInput() ?>
    </div>
    <div class="col-lg-8">
        <?= $form->field($searchModel, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['/access/user/view', 'id' => $model->id])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
