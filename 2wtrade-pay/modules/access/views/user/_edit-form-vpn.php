<?php

/** @var \app\models\User $model */
/** @var app\modules\access\models\VpnUser $vpnUserModel */
?>

<div class="" id="vpn-form" hidden>
    <div class="row">
        <div class="col-lg-12">
            <h4><?= Yii::t('common', 'Запись пользователя в VPN') ?></h4>
        </div>
    </div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($vpnUserModel, 'user_fio')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($vpnUserModel, 'chief')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($vpnUserModel, 'user_skype')->textInput() ?>
    </div>
</div>
</div>
