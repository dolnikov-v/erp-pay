<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\access\assets\UserAsset;
use app\widgets\ButtonLink;
use app\widgets\Label;
use app\widgets\Link;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\models\User $model */
/** @var \app\models\search\UserActionLogSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

UserAsset::register($this);

$this->title = Yii::t('common', 'Просмотр пользователя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<div class="row">
    <div class="col-lg-3">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Информация'),
            'withBody' => false,
            'content' => \app\modules\access\widgets\User::widget([
                'model' => $model
            ]),
            'footer' => Yii::$app->user->can('access.user.edit') ? ButtonLink::widget([
                'url' => Url::toRoute(['/access/user/edit', 'id' => $model->id]),
                'size' => ButtonLink::SIZE_SMALL,
                'isBlock' => true,
                'label' => Yii::t('common', 'Перейти к редактированию'),
            ]) : null,
        ]) ?>
    </div>
    <div class="col-lg-9">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Фильтр поиска'),
            'content' => $this->render('_view-form', [
                'model' => $model,
                'searchModel' => $searchModel,
            ]),
            'collapse' => true,
        ]) ?>

        <?= Panel::widget([
            'title' => Yii::t('common', 'Таблица переходов'),
            'actions' => DataProvider::renderSummary($dataProvider),
            'withBody' => false,
            'content' => GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-hover table-sortable tl-fixed',
                ],
                'columns' => [
                    [
                        'class' => IdColumn::className(),
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'url',
                        'contentOptions' => ['class' => 'ellipsis'],
                        'content' => function ($data) {
                            return Link::widget([
                                'label' => $data->url,
                                'url' => $data->url,
                            ]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'headerOptions' => ['class' => 'width-150 text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'attribute' => 'http_status',
                        'content' => function ($data) {
                            return Label::widget([
                                'label' => $data->http_status,
                                'style' => ($data->http_status < 400) ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                            ]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'class' => DateColumn::className(),
                        'attribute' => 'created_at',
                        'enableSorting' => false,
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'items' => [
                            [
                                'label' => Yii::t('common', 'Просмотреть'),
                                'url' => function ($data) {
                                    return Url::toRoute(['action-log/view', 'id' => $data->id]);
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('access.actionlog.view');
                                },
                            ],
                        ]
                    ],
                ],
            ]),
            'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
        ]) ?>
    </div>
</div>
