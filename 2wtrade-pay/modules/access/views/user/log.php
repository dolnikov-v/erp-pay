<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute('/access/user/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab">User</a></li>
        <li><a href="#tabDelivery" aria-controls="tabDelivery" role="tab" data-toggle="tab">UserDelivery</a></li>
        <li><a href="#tabSource" aria-controls="tabSource" role="tab" data-toggle="tab">UserSource</a></li>
        <li><a href="#tabCountry" aria-controls="tabCountry" role="tab" data-toggle="tab">UserCountry</a></li>
        <li><a href="#tabNotification" aria-controls="tabNotification" role="tab" data-toggle="tab">UserNotification</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Log::widget([
                'data' => $changeLog,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabDelivery">
            <?= Log::widget([
                'data' => $deliveryLog,
                'showLinks' => true,
                'showData' => false,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabSource">
            <?= Log::widget([
                'data' => $sourceLog,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabCountry">
            <?= Log::widget([
                'data' => $countryLog,
                'showLinks' => true,
                'showData' => false,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabNotification">
            <?= Log::widget([
                'data' => $notificationLog,
                'showLinks' => true,
            ]); ?>
        </div>
    </div>
</div>
