<?php
use app\components\widgets\ActiveForm;
use app\modules\access\assets\UserAsset;
use app\widgets\Button;
use app\widgets\custom\Checkbox;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;

UserAsset::register($this);
/** @var \app\models\User $model */
/** @var app\modules\access\models\VpnUser $vpnUserModel */
/** @var array $currentRoles */
/** @var array $roles */
?>

<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'enableClientValidation' => true,
    'id' => 'user_form',
    'action' => $model->isNewRecord ? Url::toRoute('edit') : Url::toRoute(['edit', 'id' => $model->id])
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'fullname')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'password')->textGeneratePasswordInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'roles')->select2ListMultiple($roles, [
            'value' => $currentRoles,
        ])->label(Yii::t('common', 'Роли')) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'parent_id')->select2List(User::find()
            ->active()
            ->orderBy(['username' => SORT_ASC])
            ->collection(), ['prompt' => '-']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'email')
            ->textInput()
            ->hint('Если есть учетная запись в JIRA, то email должен совпадать') ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'phone')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'telegram_chat_id')->textInput() ?>
    </div>
    <div class="col-lg-3">
    </div>
</div>
<?php if ($model->isNewRecord): ?>
    <div class="row">
        <div class="col-lg-3 margin-bottom-15">
            <?= Checkbox::widget([
                'id' => 'vpn-checkbox',
                'name' => 'vpn-checkbox',
                'label' => Html::tag('strong', Yii::t('common', 'Создать VPN для пользователя?'))
            ]) ?>
        </div>
        <!--<div class="col-lg-3 margin-bottom-15" hidden>
            <? /*= $form->field($vpnUserModel, 'checked')->textInput(['id' => 'vpn-checkbox-hide', 'value' => 0]) */ ?>
        </div>-->
    </div>
    <?= $this->render('_edit-form-vpn', [
        'vpnUserModel' => $vpnUserModel,
        'form' => $form,
    ]);
    ?>
<?php endif; ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(!$model->isNewRecord ? Yii::t('common', 'Сохранить пользователя') : Yii::t('common', 'Добавить пользователя')) ?>
        <?php if ($model->isNewRecord): ?>
            <?= Button::widget([
                'id' => 'jira-bind',
                'label' => Yii::t('common', 'Найти пользователя в JIRA?'),
                'style' => Button::STYLE_DARK,
                'size' => Button::SIZE_SMALL,
            ]) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
