<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TicketGroup */
?>

<div class="ticket-group-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord ? Url::toRoute(['edit']) : Url::toRoute(['edit', 'name' => urlencode($model->prefix)])
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?= $form->field($model, 'name')->textInput(); ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= $form->field($model, 'prefix')->textInput(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить группу') : Yii::t('common', 'Сохранить группу')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
