<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TicketGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список тикетных групп');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>
<div class="ticket-group-index">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Таблица с группами'),
        'actions' => DataProvider::renderSummary($dataProvider),
        'withBody' => false,
        'content' => GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => Yii::t('common', 'Группа'),
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'prefix',
                    'label' => Yii::t('common', 'Префикс'),
                    'enableSorting' => false,
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'created_at',
                    'label' => Yii::t('common', 'Дата добавления'),
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($data) {
                                return Url::toRoute(['edit', 'name' => urlencode($data->prefix)]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('access.ticket.edit');
                            },
                        ],
                        [
                            'can' => function () {
                                return Yii::$app->user->can('access.ticket.edit') && Yii::$app->user->can('access.ticket.delete');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить группу'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($data) {
                                return [
                                    'data-href' => Url::toRoute(['delete', 'name' => urlencode($data->prefix)]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('access.ticket.delete');
                            },
                        ],
                    ]
                ],
            ]
        ]),
        'footer' => $this->render('_index-footer', [
            'dataProvider' => $dataProvider,
        ]),
    ]) ?>
</div>

<?= ModalConfirmDelete::widget() ?>
