<?php

use app\widgets\Panel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TicketGroup */

$this->title = Yii::t('common', $model->isNewRecord ? 'Добавление группы' : 'Редактирование группы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список тикетных групп'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Тикетная группа'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>
