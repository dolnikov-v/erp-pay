<?php
namespace app\modules\access\models;

use app\components\db\ActiveRecord;
use Yii;

/**
 * Class VpnUser
 * @package app\modules\webhook\models
 * @property string $user_id
 * @property string $user_pass
 * @property string $user_mail
 * @property string $user_fio
 * @property string $user_skype
 * @property integer $user_online
 * @property integer $user_enable
 * @property string $user_start_date
 * @property string $user_end_date
 * @property string $chief
 * @property mixed $dbConnection
 * @property integer $admin
 */
class VpnUser extends ActiveRecord
{
    /**
     * @return null|object
     */
    public static function getDb() {
        return Yii::$app->get('vpnDb');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_fio', 'user_skype', 'chief'], 'required','whenClient' => "function (attribute, value) { return $('#vpn-checkbox').is(':checked'); }"],
            [
                'user_id',
                'unique',
                'targetAttribute' => 'user_id',
                'message' => 'Имя {attribute} уже используется в VPN.'
            ],
            [['user_id','user_pass', 'user_fio', 'user_skype', 'chief', 'user_mail','user_start_date', 'user_end_date'], 'string'],
            [['user_online', 'user_enable','admin'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'Логин'),
            'user_mail' => Yii::t('common', 'E-mail'),
            'user_fio' => Yii::t('common', 'Фамилия, имя, отчество'),
            'user_skype' => Yii::t('common', 'Skype'),
            'chief' => Yii::t('common', 'Руководитель отдела'),
        ];
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function generatePassword()
    {
        return strtr(substr(base64_encode(Yii::$app->getSecurity()->generateRandomKey(8)), 0, 8), '+/', 'Mz');
    }

    /**
     * @param string $value
     * @return mixed
     */
    public static function formatUserId($value)
    {
        return str_replace(".", "", $value);
    }
}