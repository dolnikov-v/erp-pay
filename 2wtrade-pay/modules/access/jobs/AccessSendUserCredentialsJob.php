<?php

namespace app\modules\access\jobs;

use app\jobs\BaseJob;

/**
 * Class AccessSendUserCredentialsJob
 * @package app\modules\access\jobs
 */
class AccessSendUserCredentialsJob extends BaseJob
{
    /**
     * @var string
     */
    public $userEmail, $payUsername, $payPassword, $vpnUserId, $vpnPassword, $message;

    /**
     * @var bool
     */
    public $credentialsWasUpdated = false, $vpnError = false;

    /**
     * @var array|string
     */
    public $bcc;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        try {
            $this->mailer->compose('@app/mail/access/user-credentials', [
                'payUsername' => $this->payUsername,
                'payPassword' => $this->payPassword,
                'vpnUserId' => $this->vpnUserId,
                'vpnPassword' => $this->vpnPassword,
                'message' => $this->message,
                'updated' => $this->credentialsWasUpdated,
                'vpnError' => $this->vpnError
            ])->setFrom(\Yii::$app->params['noReplyEmail'])
                ->setTo($this->userEmail)
                ->setBcc($this->bcc)
                ->setSubject('2WTRADE Access credentials to 2wtrade-pay.com!')
                ->send();
            usleep(500000);
        } catch (\Throwable $e) {
            $this->logAddError($e->getMessage(), ['job' => static::class]);
        }
    }
}