<?php

namespace app\modules\access\jobs;


use app\jobs\BaseJob;
use app\models\User;
use app\modules\access\models\VpnUser;

/**
 * Class AccessFlushPasswordsJob
 * @package app\modules\access\jobs
 */
class AccessFlushPasswordsJob extends BaseJob
{
    /**
     * @var integer
     */
    public $userId;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function execute($queue)
    {
        $user = User::find()->where(['id' => $this->userId])->one();
        if ($user) {
            $userPassword = \Yii::$app->security->generateRandomString(rand(10, 15));
            $user->password = $userPassword;
            if (!$user->save(true, ['password', 'force_logout'])) {
                throw new \Exception("Can't save user: {$user->getFirstErrorAsString()}.");
            }
            $vpnUserModel = VpnUser::find()->where(['user_id' => VpnUser::formatUserId($user->username)])->one();
            if ($vpnUserModel) {
                $vpnUserModel->user_pass = $vpnUserModel->generatePassword();
                if (!$vpnUserModel->save(true, ['user_pass'])) {
                    $vpnUserModel = null;
                }
            }
            if ($user->email) {
                $this->queueTransport->push(new AccessSendUserCredentialsJob([
                    'payUsername' => $user->username,
                    'payPassword' => $userPassword,
                    'vpnUserId' => $vpnUserModel->user_id ?? null,
                    'vpnPassword' => $vpnUserModel->user_pass ?? null,
                    'credentialsWasUpdated' => true,
                    'userEmail' => $user->email,
                    'bcc' => \Yii::$app->params['vpn']['notifiers'],
                ]));
            }
        }
    }
}