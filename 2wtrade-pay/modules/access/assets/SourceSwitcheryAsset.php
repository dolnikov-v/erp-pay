<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class SourceSwitcheryAsset
 * @package app\modules\access\assets
 */
class SourceSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/source-switchery';

    public $js = [
        'source-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
