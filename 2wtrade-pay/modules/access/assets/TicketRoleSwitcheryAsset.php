<?php

namespace app\modules\access\assets;


use yii\web\AssetBundle;

/**
 * Class TicketRoleSwitcheryAsset
 * @package app\modules\access\assets
 */
class TicketRoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/ticket-role-switchery';

    public $js = [
        'ticket-role-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
