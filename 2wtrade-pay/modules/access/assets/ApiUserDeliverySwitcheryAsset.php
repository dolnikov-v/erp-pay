<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class ApiUserDeliverySwitcheryAsset
 * @package app\modules\access\assets
 */
class ApiUserDeliverySwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/api-user-delivery-switchery';

    public $js = [
        'api-user-delivery-switchery.js',
    ];

    public $css = [
        'api-user-delivery-switchery.css',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
