<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class RoleAsset
 * @package app\modules\access\assets
 */
class RoleAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/role';

    public $js = [
        'role.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
