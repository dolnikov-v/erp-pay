<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationRoleSwitcheryAsset
 * @package app\modules\access\assets
 */
class NotificationRoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/notification-role-switchery';

    public $js = [
        'notification-role-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
