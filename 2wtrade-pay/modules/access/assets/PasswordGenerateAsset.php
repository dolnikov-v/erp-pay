<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class PasswordAsset
 * @package app\modules\access\assets
 */
class PasswordGenerateAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/password-generate';

    public $js = [
        'password-generate.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
