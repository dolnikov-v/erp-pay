<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package app\modules\access\assets
 */
class CountrySwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/country-switchery';

    public $js = [
        'country-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
