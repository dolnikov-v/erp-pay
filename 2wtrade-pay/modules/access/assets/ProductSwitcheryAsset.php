<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package app\modules\access\assets
 */
class ProductSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/product-switchery';

    public $js = [
        'product-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
