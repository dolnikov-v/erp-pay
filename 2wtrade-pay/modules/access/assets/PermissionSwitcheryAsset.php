<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class PermissionSwitcheryAsset
 * @package app\modules\access\assets
 */
class PermissionSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/permission-switchery';

    public $js = [
        'permission-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
