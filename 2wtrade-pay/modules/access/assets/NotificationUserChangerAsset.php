<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationSwitcheryUserAsset
 * @package app\modules\access\assets
 */
class NotificationUserChangerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/notification-user-changer';

    public $js = [
        'notification-user-changer.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\assets\uikit\ButtonsAsset',
    ];
}
