<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class UserAsset
 * @package app\modules\access\assets
 */
class UserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/user';

    public $css = [
        'user.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'new.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
