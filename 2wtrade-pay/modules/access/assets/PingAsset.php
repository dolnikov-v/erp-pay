<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class PingAsset
 * @package app\modules\access\assets
 */
class PingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/ping';

    public $js = [
        'ping.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
