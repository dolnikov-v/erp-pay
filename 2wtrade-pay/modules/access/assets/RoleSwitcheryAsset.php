<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class RoleSwitcheryAsset
 * @package app\modules\access\assets
 */
class RoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/role-switchery';

    public $js = [
        'role-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
