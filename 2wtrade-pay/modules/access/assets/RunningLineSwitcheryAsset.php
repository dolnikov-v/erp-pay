<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class RunningLineSwitcheryAsset
 * @package app\modules\access\assets
 */
class RunningLineSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/running-line-switchery';

    public $js = [
        'running-line-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
