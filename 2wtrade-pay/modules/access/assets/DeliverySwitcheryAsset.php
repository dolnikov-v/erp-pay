<?php
namespace app\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class DeliverySwitcheryAsset
 * @package app\modules\access\assets
 */
class DeliverySwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/access/delivery-switchery';

    public $js = [
        'delivery-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
