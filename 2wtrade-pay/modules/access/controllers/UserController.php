<?php

namespace app\modules\access\controllers;

use app\components\filters\AjaxFilter;
use app\models\logs\InsertData;
use app\models\logs\LinkData;
use app\models\logs\TableLog;
use app\components\web\Controller;
use app\models\AuthAssignment;
use app\models\AuthItem;
use app\models\Country;
use app\models\Source;
use app\models\UserCountry;
use app\models\UserDelivery;
use app\models\UserSource;
use app\modules\access\jobs\AccessSendUserCredentialsJob;
use app\modules\access\models\VpnUser;
use app\modules\delivery\models\Delivery;
use app\models\Notification;
use app\models\NotificationUser;
use app\models\search\UserActionLogSearch;
use app\models\search\UserSearch;
use app\models\User;
use app\models\UserNotificationSetting;
use app\modules\access\widgets\NotificationUserChanger;
use app\modules\order\models\Order;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller
{
    private $emailMessage;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'ping',
                    'set-country',
                    'set-source',
                    'set-notification',
                    'set-running-line-notification',
                    'set-delivery',
                    'jira-bind'
                ],
            ]
        ];
    }

    /**
     * Просмотр списка пользователей
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,

            'dataProvider' => $dataProvider,
            'roles' => AuthItem::getCollection(Item::TYPE_ROLE),
            'country' => Country::find()->collection(),
        ]);
    }

    /**
     * Просмотр профиля пользователя
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new UserActionLogSearch();


        $queryParams = Yii::$app->request->queryParams;
        $queryParams['UserActionLogSearch']['user_id'] = $model->id;

        $dataProvider = $searchModel->search($queryParams);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $changeLog = TableLog::find()->byTable(User::clearTableName())->byID($id)->allSorted();
        $notificationLog = TableLog::find()->byTable(NotificationUser::clearTableName())->byID($id)->allSorted();
        $deliveryLog = TableLog::find()->byTable(UserDelivery::clearTableName())->byID($id)->allSorted();
        $sourceLog = TableLog::find()->byTable(UserSource::clearTableName())->byID($id)->allSorted();
        $countryLog = TableLog::find()->byTable(UserCountry::clearTableName())->byID($id)->allSorted();

        return $this->render('log', [
            'changeLog' => $changeLog,
            'notificationLog' => $notificationLog,
            'deliveryLog' => $deliveryLog,
            'sourceLog' => $sourceLog,
            'countryLog' => $countryLog,
        ]);
    }

    /**
     * Создание и редактирование пользователя
     * @param null $id
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionEdit($id = null)
    {
        $model = is_null($id) ? new User() : $this->findModel($id);
        $model->setScenario(is_null($id) ? 'insert' : 'update');

        $vpnUserModel = new VpnUser();

        $this->checkAccess($model);

        if ($model->load(Yii::$app->request->post())) {
            $userPassword = $model->password;
            $isNewRecord = $model->isNewRecord;

            $user = Yii::$app->request->post('User');
            $currentRoles = array_keys(Yii::$app->authManager->getRolesByUser($model->id));
            $newRoles = isset($user['roles']) ? $user['roles'] : [];

            $this->checkRulesAccess($currentRoles)
            && $this->checkRulesAccess($newRoles);

            $addRoles = array_diff($newRoles, $currentRoles);
            $deleteRoles = array_diff($currentRoles, $newRoles);

            if ($model->save()) {
                if ($isNewRecord) {
                    $isVpnUserChecked = Yii::$app->request->post('vpn-checkbox');
                    $vpnError = false;
                    if ($isVpnUserChecked) {
                        $vpnUserModel->load(Yii::$app->request->post());
                        /**
                         * Заводим запись пользователя в впн
                         */
                        if (!$vpnUser = $this->vpnUserCreate($vpnUserModel, $model)) {
                            $vpnError = true;
                        }
                    }
                    $this->queueTransport->push(new AccessSendUserCredentialsJob([
                        'payUsername' => $model->username,
                        'payPassword' => $userPassword,
                        'vpnUserId' => $vpnUser->user_id ?? null,
                        'vpnPassword' => $vpnUser->user_pass ?? null,
                        'vpnError' => $vpnError,
                        'bcc' => Yii::$app->params['vpn']['notifiers'],
                        'userEmail' => $model->email
                    ]));
                }

                if (empty($newRoles)) {
                    AuthAssignment::deleteAll([
                        'user_id' => $model->id
                    ]);
                } else {
                    if (!empty($deleteRoles)) {
                        AuthAssignment::deleteAll([
                            'AND',
                            'user_id = :user_id',
                            ['IN', 'item_name', $deleteRoles]
                        ],
                            [':user_id' => $model->id]
                        );
                    }
                }

                if (!empty($addRoles)) {
                    foreach ($addRoles as $role) {
                        $authRole = Yii::$app->authManager->getRole($role);
                        Yii::$app->authManager->assign($authRole, $model->id);
                    }
                }

                $message = $isNewRecord ? 'Пользователь успешно добавлен.' : 'Пользователь успешно отредактирован.';
                $message = Yii::t('common', $message, ['name' => $model->username]);
                Yii::$app->notifier->addNotification($message, 'success');

                return $this->redirect($isNewRecord ? Url::toRoute([
                    'edit',
                    'id' => $model->id
                ]) : Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'vpnUserModel' => $vpnUserModel,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
            'currentRoles' => ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($model->id), 'name'),
            'countries' => Country::find()
                ->active()
                ->notPartner()
                ->notDistributor()
                ->orderBy(['name' => SORT_ASC])
                ->all(),
            'partnerCountries' => Country::find()->active()->isPartner()->orderBy(['name' => SORT_ASC])->all(),
            'distributorCountries' => Country::find()->active()->isDistributor()->orderBy(['name' => SORT_ASC])->all(),
            'countriesByUser' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->countries, 'id'),
            'sources' => Source::find()->collection(),
            'sourcesByUser' => $model->isNewRecord ? [] : User::getRejectedSources($model->id),
            'deliveries' => Delivery::find()
                ->joinWith('country')
                ->orderBy([
                    Delivery::tableName() . '.name' => SORT_ASC,
                    Country::tableName() . '.name' => SORT_ASC
                ])
                ->all(),
            'deliveriesByUser' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->deliveries, 'id'),
        ]);
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;

        $this->checkAccess($model);

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь {name} удален.', ['name' => $model->username]), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Установка пользователю статуса заблокирован
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_BLOCKED;

        $this->checkAccess($model);

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь {name} заблокирован.', ['name' => $model->username]), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionJiraBind()
    {
        $email = Yii::$app->request->get('email');

        $userName = Yii::$app->params['jira']['credentials']['username'];
        $password = Yii::$app->params['jira']['credentials']['password'];

        $url = 'https://2wtrade-tasks.atlassian.net/rest/api/2/user/search?username=' . $email;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_USERPWD, $userName . ':' . $password);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response);
        if (!$result) {
            throw new BadRequestHttpException(Yii::t('common', 'Пользователь с почтой {email} не найден в JIRA', ['email' => $email]));
        }
        return [
            'name' => $result[0]->name,
            'message' => 'Пользователь найден, поле "Имя пользователя" заполнено.'
        ];

    }


    /**
     * Установка пользователю статуса разблокирован
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DEFAULT;

        $this->checkAccess($model);

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь {name} разблокирован.', ['name' => $model->username]), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Проверка наличия пользователя на сайте
     * @return array
     */
    public function actionPing()
    {
        $user = Yii::$app->user->identity;

        if (!Yii::$app->user->isGuest) {
            if (time() - $user->lastact_at > User::TIME_AFK) {
                $user->ping_at = time();
                $user->save(true, ['ping_at']);
            }
        }

        return $user->ping_at;
    }

    /**
     * Ajax установка страны
     * @throws \yii\web\HttpException
     * @todo Требует рефакторинга
     */
    public function actionSetCountry()
    {
        $userId = (int)Yii::$app->request->post('user_id');
        $countryId = (int)Yii::$app->request->post('country_id');

        $user = User::find()->where(['id' => $userId])->one();
        $country = Country::find()->where(['id' => $countryId])->one();

        if (!$user || !$country) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране или пользователю.'));
        }

        $this->checkAccess($user);

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $user->link('countries', $country);
                $log = new TableLog();
                $log->table = UserCountry::clearTableName();
                $log->id = $user->id;
                $log->data = [new InsertData(['field' => 'country_id', 'new' => $country->id ?? ''])];
                $log->links = [new LinkData(['field' => 'country_id', 'value' => $country->id ?? ''])];
                $log->type = TableLog::TYPE_INSERT;
                $log->save();
                break;
            case 'off':
                $user->unlink('countries', $country, true);
                $log = new TableLog();
                $log->table = UserCountry::clearTableName();
                $log->id = $user->id;
                $log->links = [new LinkData(['field' => 'country_id', 'value' => $country->id ?? ''])];
                $log->type = TableLog::TYPE_DELETE;
                $log->save();
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * Ajax установка источника
     * @throws \yii\web\HttpException
     */
    public function actionSetSource()
    {
        $userId = (int)Yii::$app->request->post('user_id');
        $sourceId = Yii::$app->request->post('source_id');

        $user = User::find()->where(['id' => $userId])->one();

        if (!$user || !$sourceId) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущему источнику или пользователю.'));
        }

        $this->checkAccess($user);

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                if ($this->setSourceActive($userId, $sourceId, 1)) {
                    break;
                }
                throw new BadRequestHttpException(Yii::t('common', 'Не удалось разрешить источник'));
            case 'off':
                if ($this->setSourceActive($userId, $sourceId, 0)) {
                    break;
                }
                throw new BadRequestHttpException(Yii::t('common', 'Не удалось запретить источник'));
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param integer $userId
     * @param integer $sourceId
     * @param bool $active
     * @return bool
     */
    private function setSourceActive($userId, $sourceId, $active)
    {
        $source = UserSource::find()
            ->byUserId($userId)
            ->bySourceId($sourceId)
            ->one();
        if (empty($source)) {
            $source = new UserSource();
            $source->user_id = $userId;
            $source->source_id = $sourceId;
            $source->created_at = time();
        }

        $source->active = $active;
        $source->updated_at = time();

        return $source->save();
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionSetDelivery()
    {
        $userId = (int)Yii::$app->request->post('user_id');
        $deliveryId = (int)Yii::$app->request->post('delivery_id');

        $user = User::findOne($userId);
        $delivery = Delivery::findOne($deliveryId);

        if (!$user || !$delivery) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей службе доставке или пользователю.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $user->link('deliveries', $delivery);
                $log = new TableLog();
                $log->table = UserDelivery::clearTableName();
                $log->id = $user->id;
                $log->data = [new InsertData(['field' => 'delivery_id', 'new' => $delivery->id ?? ''])];
                $log->links = [new LinkData(['field' => 'delivery_id', 'value' => $delivery->id ?? ''])];
                $log->type = TableLog::TYPE_INSERT;
                $log->save();
                break;
            case 'off':
                $user->unlink('deliveries', $delivery, true);
                $log = new TableLog();
                $log->table = UserDelivery::clearTableName();
                $log->id = $user->id;
                $log->links = [new LinkData(['field' => 'delivery_id', 'value' => $delivery->id ?? ''])];
                $log->type = TableLog::TYPE_DELETE;
                $log->save();
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * Ajax установка оповещения для пользователя
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionSetNotification()
    {
        /** @var User $user */
        $user = User::findIdentity(Yii::$app->request->post('user'));

        if (!$user) {
            throw new BadRequestHttpException(Yii::t('common', 'Пользователя не существует.'));
        }

        /** @var Notification $notification */
        $notification = Notification::find()
            ->where([
                'trigger' => Yii::$app->request->post('trigger'),
            ])
            ->one();

        if (!$notification) {
            throw new BadRequestHttpException(Yii::t('common', 'Оповещения не существует.'));
        }

        $value = Yii::$app->request->post('value');

        /** @var NotificationUser $link */
        $link = NotificationUser::find()
            ->where([
                'user_id' => $user->id,
                'trigger' => $notification->trigger,
            ])
            ->one();

        if (!$link) {
            $link = new NotificationUser([
                'user_id' => $user->id,
                'trigger' => $notification->trigger,
            ]);
        }

        $userSetting = UserNotificationSetting::find()->where([
            'user_id' => $user->id,
            'trigger' => $notification->trigger
        ])->one();

        switch ($value) {
            case NotificationUserChanger::TYPE_ON:
                $link->active = 1;
                $link->save();

                break;
            case NotificationUserChanger::TYPE_OFF:
                if ($userSetting) {
                    $userSetting->delete();
                }

                $link->active = 0;
                $link->save();

                break;
            case NotificationUserChanger::TYPE_PARENT:
                if (!$link->isNewRecord) {
                    $link->delete();
                }

                if ($userSetting) {
                    $userSetting->delete();
                }

                break;
        }

        return [];
    }

    /**
     * Ajax установка оповещения-бегущей строки для пользователя
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionSetRunningLineNotification()
    {
        /** @var User $user */
        $user = User::findIdentity(Yii::$app->request->post('user_id'));

        if (!$user) {
            throw new BadRequestHttpException(Yii::t('common', 'Пользователя не существует.'));
        }

        /** @var Notification $notification */
        $notification = Notification::find()
            ->where([
                'trigger' => Yii::$app->request->post('notification_name'),
            ])
            ->one();

        if (!$notification) {
            throw new BadRequestHttpException(Yii::t('common', 'Оповещения не существует.'));
        }

        $value = Yii::$app->request->post('value');

        /** @var NotificationUser $link */
        $link = NotificationUser::find()
            ->where([
                'user_id' => $user->id,
                'trigger' => $notification->trigger,
            ])
            ->one();

        if (!$link) {
            throw new BadRequestHttpException(Yii::t('common', 'Оповещения не существует или оно наследуется.'));
        }

        switch ($value) {
            case 'on':
                $link->running_line_active = 1;
                $link->save();
                break;
            case 'off':
                $link->running_line_active = 0;
                $link->save();
                break;
        }

        return [];
    }

    /**
     * Проверка на права работы с ролями
     * @param array $roles
     * @throws ForbiddenHttpException
     * @return boolean
     */

    protected function checkRulesAccess($roles)
    {
        $user = Yii::$app->user;

        if ($user->getIsSecurityManager()) { // этому пользователю можно сохранять после редактирования всех пользователей
            return true;
        }

        if ((in_array(User::ROLE_SUPERADMIN, $roles) && !$user->getIsSuperadmin()) ||
            (in_array(User::ROLE_ADMIN, $roles) && (!$user->can('can-edit-user-role-admin') || !$user->getIsSuperadmin()))
        ) {
            throw new ForbiddenHttpException(Yii::t('common', 'Вы не можете задавать роли "Админ" и "Суперадмин" пользователям.'));
        }

        return true;
    }

    /**
     * Проверка на права редактирования
     * @param User $model
     * @throws ForbiddenHttpException
     * @return boolean
     */

    protected function checkAccess($model)
    {
        $user = Yii::$app->user;

        if ($user->getIsSecurityManager()) { // этому пользователю можно редактировать всех пользователей
            return true;
        }

        if (($model->getIsSuperadmin() && !$user->getIsSuperadmin()) ||
            ($model->getIsAdmin() && (!$user->can('can-edit-user-role-admin') || !$user->getIsSuperadmin()))
        ) {
            throw new ForbiddenHttpException(Yii::t('common', 'Вы не имеете прав доступа редактировать этого пользователя.'));
        }

        return true;
    }

    /**
     * Поиск модели по id
     * @param $id
     * @return User
     * @throws NotFoundHttpException
     */

    protected function findModel($id)
    {
        /** @var User $model */
        $model = User::findOne($id);

        if ($model && !$model->hasStatus(User::STATUS_DELETED)) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Пользователь не найден.'));
        }
    }

    /**
     * @param VpnUser $vpnUserModel
     * @param User $model
     * @return VpnUser|null
     */
    private function vpnUserCreate($vpnUserModel, $model)
    {
        $vpnUserModel->user_enable = 1;
        $vpnUserModel->user_id = VpnUser::formatUserId($model->username);
        $vpnUserModel->user_mail = $model->email;
        $vpnUserModel->user_pass = $vpnUserModel->generatePassword();
        $vpnUserModel->user_start_date = date('Y-m-d');
        if ($vpnUserModel->save()) {
            $message = "Доступ впн для пользователя $vpnUserModel->user_id успешно создан";
            Yii::$app->notifier->addNotification($message, 'success');
            return $vpnUserModel;
        } else {
            $message = "Ошибка! Не удалось создать доступ впн для пользователя $vpnUserModel->user_id";
            Yii::$app->notifier->addNotification($message);
            Yii::$app->notifier->addNotificationsByModel($vpnUserModel);
            return null;
        }
    }

    /**
     * @param $userEmail
     * @return bool
     * @internal param $message
     */
    private function sendEmailToNewUser($userEmail)
    {
        $mailTo = Yii::$app->params['vpn']['notifiers'];
        $mailTo[] = $userEmail;
        $mailer = Yii::$app->mailer
            ->compose()
            ->setFrom(Yii::$app->params['noReplyEmail'])
            ->setTo($mailTo)
            ->setSubject('2WTRADE Access credentials to 2wtrade-pay.com!')
            ->setTextBody($this->emailMessage);
        return $result = $mailer->send();
    }

    /**
     * @param $payUser User
     * @param $payPassword string
     * @param $vpnUser VpnUser
     * @param string $message
     * @internal param \stdClass $blockedUser
     */
    private function wrapMessage($payUser, $payPassword, $vpnUser = null, $message = '')
    {
        $this->emailMessage = 'This message was sent automatically when user was created in 2wtrade-pay.com' . PHP_EOL . PHP_EOL;
        $this->emailMessage .= "Access : " . PHP_EOL;
        $this->emailMessage .= "    2wtrade-pay.com" . PHP_EOL;
        $this->emailMessage .= '---------------------------------' . PHP_EOL;
        $this->emailMessage .= "    Login: " . $payUser->username . PHP_EOL;
        $this->emailMessage .= "    Password: " . $payPassword . PHP_EOL . PHP_EOL;
        if ($vpnUser) {
            $this->emailMessage .= "    Instructions for VPN access" . PHP_EOL;
            $this->emailMessage .= '---------------------------------' . PHP_EOL;
            $this->emailMessage .= "    http://185.56.232.68/pay.pdf - instruction" . PHP_EOL;
            $this->emailMessage .= "    http://185.56.232.68/pay.ovpn - configuration (instruction also contains this link)" . PHP_EOL;
            $this->emailMessage .= "    Login: " . $vpnUser->user_id . PHP_EOL;
            $this->emailMessage .= "    Password: " . $vpnUser->user_pass . PHP_EOL;
        }
        $this->emailMessage .= $message . PHP_EOL;
        $this->emailMessage .= '---------------------------------' . PHP_EOL . PHP_EOL;
        $this->emailMessage .= "\"2Wtrade Rus\" security service" . PHP_EOL . PHP_EOL;
        $this->emailMessage .= "Best wishes!" . PHP_EOL;
    }
}
