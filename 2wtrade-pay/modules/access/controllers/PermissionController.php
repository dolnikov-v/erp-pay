<?php
namespace app\modules\access\controllers;

use app\components\Notifier;
use app\components\web\Controller;
use app\models\AuthItem;
use app\models\AuthItemChild;
use app\models\AuthItemForm;
use app\models\search\AuthItemSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;

/**
 * Class PermissionController
 * @package app\modules\access\controllers
 */
class PermissionController extends Controller
{
    /**
     * Список операций
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch(Item::TYPE_PERMISSION);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search(Yii::$app->request->get())
        ]);
    }

    /**
     * Редактирование операции
     *
     * @param $name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($name = null)
    {
        $model = is_null($name) ? new AuthItem(['type' => Item::TYPE_PERMISSION]) : $this->findModel($name);
        $searchModel = new AuthItemSearch(Item::TYPE_PERMISSION);

        if ($model->load(Yii::$app->request->post())) {
            $message = $model->isNewRecord ? 'Операция успешно добавлена.' : 'Операция успешно отредактирована.';

            if ($model->save()) {
                $message = Yii::t('common', $message, ['name' => $model->name]);
                Yii::$app->notifier->addNotification($message, Notifier::TYPE_SUCCESS);

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $rolesByPermission = [];
        if ($model->name) {
            $rolesByPermission = ArrayHelper::getColumn(AuthItemChild::find()
                ->where(['child' => $model->name])
                ->all(), 'parent');
        }

        return $this->render('edit', [
            'model' => $model,
            'roleList' => AuthItem::find()->where(['type' => Item::TYPE_ROLE])->orderBy(['name' => SORT_ASC])->all(),
            'rolesByPermission' => $rolesByPermission,
        ]);
    }

    /**
     * Удаление операции
     *
     * @param $name
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($name)
    {
        $model = $this->findModel($name);

        if (AuthItemForm::checkRelation($model->name)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Невозможно удалить запись. Есть зависимости.'));
        } else {
            $item = Yii::$app->authManager->getPermission($model->name);
            Yii::$app->authManager->remove($item);
            Yii::$app->notifier->addNotification(Yii::t('common', 'Запись успешно удалена.'), Notifier::TYPE_SUCCESS);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Поиск операции по имени
     *
     * @param $name
     * @return \yii\rbac\Role
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if ($model = AuthItem::find()->where(['name' => $name, 'type' => Item::TYPE_PERMISSION])->one()) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }
}
