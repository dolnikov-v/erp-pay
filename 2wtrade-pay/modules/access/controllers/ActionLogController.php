<?php
namespace app\modules\access\controllers;

use app\components\web\Controller;
use app\models\UserActionLog;
use app\models\UserActionLogQuery;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class ActionLogController
 * @package app\controllers
 */
class ActionLogController extends Controller
{
    /**
     * Список действий пользователя
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'queries' => UserActionLogQuery::find()->byActionLogId($model->id)->all()
        ]);
    }

    /**
     * @param $id
     * @return UserActionLog
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($model = UserActionLog::findOne($id)) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Действие не найдено.'));
        }
    }
}
