<?php
namespace app\modules\access\controllers;

use app\components\filters\AjaxFilter;
use app\models\logs\InsertData;
use app\models\logs\LinkData;
use app\models\logs\TableLog;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\AuthItem;
use app\models\AuthItemChild;
use app\models\AuthItemForm;
use app\models\Notification;
use app\models\NotificationRole;
use app\models\search\AuthItemSearch;
use app\models\TicketGroup;
use app\models\TicketRole;
use app\models\UserNotificationSetting;
use app\modules\widget\models\WidgetRole;
use app\modules\widget\models\WidgetType;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;


/**
 * Class RoleController
 * @package app\modules\access\controllers
 */
class RoleController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-permission',
                    'set-notification',
                    'set-running-line-notification',
                ],
            ]
        ];
    }

    /**
     * Список ролей
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch(Item::TYPE_ROLE);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $name
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($name)
    {
        $model = $this->findModel($name);
        $permissionsByRole = Yii::$app->authManager->getPermissionsByRole($model->name);

        return $this->render('view', [
            'model' => $model,
            'permissionList' => $permissionsByRole,
            'permissionsByRole' => array_keys($permissionsByRole),
        ]);
    }

    /**
     * Редактирование роли
     * @param null $name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($name = null)
    {
        $model = is_null($name) ? new AuthItem(['type' => Item::TYPE_ROLE]) : $this->findModel($name);
        $modelSearch = new AuthItemSearch(Item::TYPE_PERMISSION);

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $ticketRole = $model->outgoingTicketRole;
            if (!empty($model->ticket_group)) {
                if (!$ticketRole) {
                    $ticketRole = new TicketRole();
                    $ticketRole->role_name = $model->name;
                    $ticketRole->ticket_prefix = $model->ticket_group;
                    $ticketRole->type = TicketRole::TYPE_OUTGOING;
                } else {
                    $ticketRole->ticket_prefix = $model->ticket_group;
                }
                $ticketRole->save();
            } elseif ($ticketRole) {
                $ticketRole->delete();
            }

            if ($model->save()) {
                $message = Yii::t('common',
                    $model->isNewRecord ? 'Роль успешно добавлена.' : 'Роль успешно отредактирована.');
                Yii::$app->notifier->addNotification($message, Notifier::TYPE_SUCCESS);

                return $this->redirect($isNewRecord ? Url::toRoute([
                    'edit',
                    'name' => $model->name
                ]) : Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        } else {
            $ticketRole = $model->outgoingTicketRole;
            if ($ticketRole) {
                $model->ticket_group = $ticketRole->ticket_prefix;
            }
        }

        $widgets = WidgetType::find()->all();

        $widgetsByRole = WidgetRole::find()
            ->select(['type_id'])
            ->where([
                'role' => $name
            ])
            ->column();

        return $this->render('edit', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'widgets' => $widgets,
            'widgetsByRole' => $widgetsByRole,
        ]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionLog($id = null)
    {
        $changeLog = TableLog::find()->byTable(AuthItem::clearTableName())->byID($id)->allSorted();
        $ticketLog = TableLog::find()->byTable(TicketRole::clearTableName())->byID($id)->allSorted();
        $childLog = TableLog::find()->byTable(AuthItemChild::clearTableName())->byID($id)->allSorted();

        return $this->render('log', [
            'changeLog' => $changeLog,
            'ticketLog' => $ticketLog,
            'childLog' => $childLog,
        ]);
    }

    /**
     * Удаление роли
     * @param $name
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($name)
    {
        $model = $this->findModel($name);

        if (AuthItemForm::checkRelation($model->name)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Невозможно удалить запись. Есть зависимости.'));
        } else {
            $item = Yii::$app->authManager->getRole($model->name);
            Yii::$app->authManager->remove($item);
            Yii::$app->notifier->addNotification(Yii::t('common', 'Запись успешно удалена.'), Notifier::TYPE_SUCCESS);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * AJAX назначение правила к роли
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetPermission()
    {
        $role = Yii::$app->authManager->getRole(Yii::$app->request->post('role'));

        if (!$role) {
            throw new BadRequestHttpException(Yii::t('common', 'Роль не существует.'));
        }

        $permission = Yii::$app->authManager->getPermission(Yii::$app->request->post('permission'));

        if (!$permission) {
            throw new BadRequestHttpException(Yii::t('common', 'Правило не существует.'));
        }

        if ($allow = Yii::$app->request->post('value') == 'on' ? true : false) {
            if (!Yii::$app->authManager->hasChild($role, $permission)) {
                Yii::$app->authManager->addChild($role, $permission);
                $log = new TableLog();
                $log->table = 'auth_item_child';
                $log->id = $role->name ?? '';
                $log->data = [new InsertData(['field' => 'child', 'new' => $permission->name ?? ''])];
                $log->type = TableLog::TYPE_INSERT;
                $log->save();
            }
        } else {
            Yii::$app->authManager->removeChild($role, $permission);
            $log = new TableLog();
            $log->table = 'auth_item_child';
            $log->id = $role->name ?? '';
            $log->links = [new LinkData(['field' => 'child', 'value' => $permission->name ?? ''])];
            $log->type = TableLog::TYPE_DELETE;
            $log->save();
        }

        return [];
    }

    /**
     * AJAX назначение оповещений для роли
     * @throws BadRequestHttpException
     */
    public function actionSetNotification()
    {
        /** @var Notification $notification */
        $notification = Notification::find()
            ->joinWith(['notificationsRole'])
            ->where(['notification.trigger' => Yii::$app->request->post('trigger')])
            ->one();

        if (!$notification) {
            throw new BadRequestHttpException(Yii::t('common', 'Оповещение не существует.'));
        }

        $role = $this->findModel(Yii::$app->request->post('role'));

        if (!$role) {
            throw new BadRequestHttpException(Yii::t('common', 'Роль не существует.'));
        }

        $link = NotificationRole::find()
            ->where(['role' => $role->name, 'trigger' => $notification->trigger])->one();

        if ($link) {
            $link->delete();
            UserNotificationSetting::deleteAll([
                'active' => 1,
                'trigger' => $notification->trigger
            ]);

        } else {
            $record = new NotificationRole();
            $record->role = $role->name;
            $record->trigger = $notification->trigger;
            $record->save();
        }

        return true;
    }

    /**
     * Ajax установка оповещения-бегущей строки для роли
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionSetRunningLineNotification()
    {
        /** @var Notification $notification */
        $notification = Notification::find()
            ->joinWith(['notificationsRole'])
            ->where(['notification.trigger' => Yii::$app->request->post('notification_name')])
            ->one();

        if (!$notification) {
            throw new BadRequestHttpException(Yii::t('common', 'Оповещение не существует.'));
        }

        $role = $this->findModel(Yii::$app->request->post('role_name'));

        if (!$role) {
            throw new BadRequestHttpException(Yii::t('common', 'Роль не существует.'));
        }

        /** @var NotificationRole $link */
        $link = NotificationRole::find()
            ->where(['role' => $role->name, 'trigger' => $notification->trigger])
            ->one();

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $link->running_line_active = 1;
                $link->save();
                break;
            case 'off':
                $link->running_line_active = 0;
                $link->save();
                break;
        }

        return [];
    }

    /**
     * AJAX назначение входящих тикетных групп для роли
     * @throws BadRequestHttpException
     */
    public function actionSetTicketGroup()
    {
        $group = TicketGroup::find()->where(['prefix' => Yii::$app->request->post('trigger')])->one();

        if (!$group) {
            throw new BadRequestHttpException(Yii::t('common', 'Тикетная группа не существует.'));
        }

        $role = $this->findModel(Yii::$app->request->post('role'));

        if (!$role) {
            throw new BadRequestHttpException(Yii::t('common', 'Роль не существует.'));
        }

        $link = TicketRole::find()
            ->where([
                'role_name' => $role->name,
                'ticket_prefix' => $group->prefix,
                'type' => TicketRole::TYPE_INBOX
            ])->one();

        if ($link) {
            $link->delete();
        } else {
            $record = new TicketRole();
            $record->role_name = $role->name;
            $record->ticket_prefix = $group->prefix;
            $record->type = TicketRole::TYPE_INBOX;
            $record->save();
        }
        return true;
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSetWidgetForRole()
    {
        $request = Yii::$app->request;
        $value = $request->post('value');

        switch ($value) {
            case 'off':
                $model = WidgetRole::find()
                    ->where([
                        'role' => $request->post('role'),
                        'type_id' => $request->post('type_id'),
                    ])->one();
                $status = ($model && $model->delete());
                break;
            case 'on':
                $model = new WidgetRole();
                $model->load($request->post(), '');
                $status = $model->save();
                break;
        }

        if (empty($status)) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему виждету или роли пользователю.'));
        }

        return json_encode(['success' => true]);
    }

    /**
     * @param $name
     * @return \yii\rbac\Role
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if ($model = AuthItem::find()->joinWith(['notificationsRole'])->where([
            'name' => $name,
            'type' => Item::TYPE_ROLE
        ])->one()
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }


}
