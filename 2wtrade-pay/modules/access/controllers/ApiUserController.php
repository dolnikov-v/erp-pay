<?php
namespace app\modules\access\controllers;

use app\components\web\Controller;
use app\models\api\ApiUser;
use app\models\api\ApiUserDelivery;
use app\models\api\ApiUserToken;
use app\models\Country;
use app\models\search\ApiUserSearch;
use app\modules\delivery\models\Delivery;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\components\filters\AjaxFilter;

/**
 * Class ApiUserController
 * @package app\modules\access\controllers
 */
class ApiUserController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['change-delivery'],
            ]
        ];
    }

    /**
     * Просмотр списка пользователей
     */
    public function actionIndex()
    {
        $searchModel = new ApiUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        if ($model->status == ApiUser::STATUS_NO_ACTIVE) {
            $model->status = ApiUser::STATUS_ACTIVE;
        } else {
            $model->status = ApiUser::STATUS_NO_ACTIVE;
        }

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Статус пользователя изменён.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось изменить статус пользователя.'), 'fail');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionChangeDelivery()
    {
        $userId = Yii::$app->request->post('userId');
        $deliveryId = Yii::$app->request->post('deliveryId');

        $delivery = Delivery::findOne($deliveryId);
        $apiUser = ApiUser::findOne($userId);
        if ($delivery && $apiUser) {
            $apiUserDelivery = ApiUserDelivery::find()
                ->where([
                    'user_id' => $apiUser->id,
                    'delivery_id' => $delivery->id
                ])
                ->one();

            if (!$apiUserDelivery) {
                $userDelivery = new ApiUserDelivery();
                $userDelivery->user_id = $apiUser->id;
                $userDelivery->delivery_id = $delivery->id;
                $userDelivery->save();
            } else {
                $apiUserDelivery->delete();
            }

            return [];
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Такая служба доставки не найдена.'));
        }
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        $model = is_null($id) ? new ApiUser() : $this->findModel($id);
        $model->setScenario(is_null($id) ? 'insert' : 'update');

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                $message = $isNewRecord ? Yii::t('common', 'Пользователь успешно добавлен.') : Yii::t('common', 'Пользователь успешно отредактирован.');
                $message = Yii::t('common', $message, ['name' => $model->username]);
                Yii::$app->notifier->addNotification($message, 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'activeDeliveries' => $this->getActiveDeliveries($id),
            'countries' => $this->findCountries(),
            'tokens' => $this->findTokens($id)
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь {name} удален.', ['name' => $model->username]), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeleteToken($id)
    {
        $token = ApiUserToken::find()
            ->where([ApiUserToken::tableName() . '.id' => $id])
            ->one();

        if ($token) {
            if ($token->delete()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Токен {name} удален.', ['name' => $token->auth_token]), 'success');
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось удалить токен {name}.', ['name' => $token->auth_token]), 'fail');
            }
        }

        return $this->redirect(Url::toRoute(['edit', 'id' => $token->user_id]));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMakeTokenUnlimited($id)
    {
        $token = ApiUserToken::find()
            ->where([ApiUserToken::tableName() . '.id' => $id])
            ->one();
        if ($token) {
            $token->lifetime = null;
            if (!$token->save(true, ['lifetime'])) {
                Yii::$app->notifier->addNotificationsByModel($token);
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Токен {name} сделан бессрочынм.', ['name' => $token->auth_token]), 'success');
            }
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Не удалось найти токен.'));
        }
        return $this->redirect(Url::toRoute(['edit', 'id' => $token->user_id]));
    }

    /**
     * Поиск модели по id
     * @param $id
     * @return ApiUser
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var ApiUser $model */
        $model = ApiUser::findOne($id);

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Пользователь не найден.'));
        }
    }

    /**
     * @param $userId
     * @return ActiveDataProvider
     */
    protected function findTokens($userId)
    {
        $tokensQuery = ApiUserToken::find()
            ->where(['user_id' => $userId]);

        $tokens = new ActiveDataProvider(['query' => $tokensQuery]);

        return $tokens;
    }

    /**
     * @param $userId
     * @return array
     */
    protected function getActiveDeliveries($userId)
    {
        $tokensQuery = ApiUserDelivery::find()
            ->where(['user_id' => $userId])
            ->all();

        return ArrayHelper::getColumn($tokensQuery, 'delivery_id');
    }

    /**
     * @return \app\models\Country[]
     */
    protected function findCountries()
    {
        return Country::find()
            ->joinWith(['deliveries'])
            ->where(['<>', Delivery::tableName() . '.id', 'NULL'])
            ->active()
            ->all();
    }
}
