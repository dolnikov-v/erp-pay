<?php

namespace app\modules\access\controllers;

use Yii;
use app\models\TicketGroup;
use app\models\search\TicketGroupSearch;
use app\components\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\components\Notifier;

/**
 * Class TicketController
 * @package app\modules\access\controllers
 */
class TicketController extends Controller
{
    /**
     * * Список тикетов
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TicketGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Редактирование группы
     *
     * @param $name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($name = null)
    {
        $model = is_null($name) ? new TicketGroup() : $this->findModel($name);

        if ($model->load(Yii::$app->request->post())) {
            $message = $model->isNewRecord ? 'Группа успешно добавлена.' : 'Группа успешно отредактирована.';

            if ($model->save()) {
                $message = Yii::t('common', $message, ['name' => $model->name]);
                Yii::$app->notifier->addNotification($message, Notifier::TYPE_SUCCESS);

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Добавление группы
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TicketGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prefix]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Изменение группы
     *
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prefix]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Удаление группы
     *
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return TicketGroup
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = TicketGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }
}
