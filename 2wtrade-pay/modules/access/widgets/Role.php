<?php
namespace app\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Role
 * @package app\modules\access\widgets
 */
class Role extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $roleList;

    /**
     * @var
     */
    public $rolesByPermission;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('role', [
            'model' => $this->model,
            'roleList' => $this->roleList,
            'rolesByPermission' => $this->rolesByPermission,
            'disabled' => $this->disabled,
        ]);
    }
}
