<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class ProductSwitchery
 * @package app\modules\access\widgets
 */
class ProductSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('product-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'product-switchery',
                ],
            ]),
        ]);
    }
}
