<?php
namespace app\modules\access\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use app\models\Notification;


/**
 * Class NotificationRole
 * @package app\modules\access\widgets
 */
class NotificationRole extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        $triggers = ArrayHelper::getColumn($this->model->notifications, 'trigger');
        $roleRunningLineNotifications = \app\models\NotificationRole::find()
            ->andWhere(['trigger' => $triggers])
            ->all();
        $roleRunningLineNotifications = ArrayHelper::map($roleRunningLineNotifications, 'trigger', 'running_line_active');

        return $this->render('notification-role', [
            'model' => $this->model,
            'modelsNotification' => Notification::find()->all(),
            'triggers' => $triggers,
            'roleRunningLineNotifications' => $roleRunningLineNotifications,
        ]);
    }

}
