<?php
namespace app\modules\access\widgets;

use app\widgets\Button;
use app\widgets\InputGroup;
use app\widgets\InputPassword;
use yii\base\Widget;

/**
 * Class Password
 * @package app\widgets
 */
class PasswordGenerate extends Widget
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $name;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('password', [
            'widget' => InputGroup::widget([
                'input' => InputPassword::widget([
                    'id' => $this->id,
                    'name' => $this->name,
                    'hook' => true,
                ]),
                'left' => [
                    Button::widget([
                        'icon' => 'fa-random',
                        'attributes' => [
                            'data-widget' => 'password-generator'
                        ],
                    ]),
                ],
                'right' => [
                    Button::widget([
                        'icon' => 'fa-eye',
                        'attributes' => [
                            'data-widget' => 'password-showing'
                        ],
                    ]),
                ],
            ]),
        ]);
    }
}
