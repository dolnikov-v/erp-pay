<?php
namespace app\modules\access\widgets;

use app\models\Notification;
use app\models\User;
use Yii;
use yii\base\Widget;
use app\models\NotificationUser as NotificationUserModel;
use yii\helpers\ArrayHelper;

/**
 * Class NotificationUser
 * @package app\modules\access\widgets
 */
class NotificationUser extends Widget
{
    /**
     * @var User
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        $notifications = NotificationUserModel::find()
            ->where(['user_id' => $this->model->id])
            ->all();

        $userNotifications = ArrayHelper::map($notifications, 'trigger', 'active');
        $userRunningLineNotifications = ArrayHelper::map($notifications, 'trigger', 'running_line_active');

        return $this->render('notification-user', [
            'model' => $this->model,
            'notifications' => Notification::find()->all(),
            'userNotifications' => $userNotifications,
            'userRunningLineNotifications' => $userRunningLineNotifications,
        ]);
    }
}
