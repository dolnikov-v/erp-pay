<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class RunningLineSwitchery
 * @package app\modules\access\widgets
 */
class RunningLineSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('running-line-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'running-line-switchery',
                ],
            ]),
        ]);
    }
}
