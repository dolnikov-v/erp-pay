<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class DeliverySwitchery
 * @package app\modules\access\widgets
 */
class DeliverySwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('delivery-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'delivery-switchery',
                ],
            ]),
        ]);
    }
}
