<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class CountrySwitchery
 * @package app\modules\access\widgets
 */
class CountrySwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('country-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'country-switchery',
                ],
            ]),
        ]);
    }
}
