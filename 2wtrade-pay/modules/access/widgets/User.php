<?php
namespace app\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Button
 * @package app\widgets
 */
class User extends Widget
{
    /**
     * @var \app\models\User Пользователь
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('user', [
            'model' => $this->model,
        ]);
    }
}
