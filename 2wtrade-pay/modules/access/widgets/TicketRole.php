<?php

namespace app\modules\access\widgets;


use app\models\AuthItem;
use app\models\TicketGroup;
use app\widgets\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class TicketRole
 * @package app\modules\access\widgets
 */
class TicketRole extends Widget
{
    /**
     * @var AuthItem
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        $triggers = ArrayHelper::getColumn($this->model->ticketGroups, 'prefix');

        return $this->render('ticket-role', [
            'model' => $this->model,
            'modelsGroup' => TicketGroup::find()->all(),
            'triggers' => $triggers
        ]);
    }
}
