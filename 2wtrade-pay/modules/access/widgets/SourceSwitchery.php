<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class SourceSwitchery
 * @package app\modules\access\widgets
 */
class SourceSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('source-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'source-switchery',
                ],
            ]),
        ]);
    }
}
