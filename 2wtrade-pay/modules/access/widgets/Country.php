<?php
namespace app\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Country
 * @package app\modules\access\widgets
 */
class Country extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $countryList;

    /**
     * @var
     */
    public $countriesByUser;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('country', [
            'model' => $this->model,
            'countryList' => $this->countryList,
            'countriesByUser' => $this->countriesByUser,
        ]);
    }
}
