<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationRoleSwitchery
 * @package app\modules\access\widgets
 */
class NotificationRoleSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-role-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-role-switchery',
                ],
            ]),
        ]);
    }
}
