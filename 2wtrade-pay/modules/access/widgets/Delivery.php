<?php
namespace app\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Delivery
 * @package app\modules\access\widgets
 */
class Delivery extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $deliveryList;

    /**
     * @var
     */
    public $deliveriesByUser;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery', [
            'model' => $this->model,
            'deliveryList' => $this->deliveryList,
            'deliveriesByUser' => $this->deliveriesByUser,
        ]);
    }
}
