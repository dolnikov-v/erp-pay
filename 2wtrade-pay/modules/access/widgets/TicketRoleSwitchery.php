<?php

namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class TicketRoleSwitchery
 * @package app\modules\access\widgets
 */
class TicketRoleSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('ticket-role-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'ticket-role-switchery',
                ],
            ]),
        ]);
    }
}
