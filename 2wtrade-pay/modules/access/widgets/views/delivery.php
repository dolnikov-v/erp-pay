<?php
use app\modules\access\widgets\DeliverySwitchery;

/** @var \app\models\User $model */
/** @var array $deliveryList */
/** @var array $deliveriesByUser */

?>
<table class="table table-striped table-hover" data-user-id="<?= $model->id ?>">
    <?php if (!empty($deliveryList)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Служба доставки') ?></th>
            <th><?= Yii::t('common', 'Страна') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($deliveryList as $delivery) : ?>
            <tr>
                <td><?= $delivery->name ?></td>
                <td><?= $delivery->country->name ?></td>
                <td class="text-center" data-delivery-id="<?= $delivery->id ?>">
                    <?= DeliverySwitchery::widget([
                        'checked' => in_array($delivery->id, $deliveriesByUser),
                        'disabled' => !$delivery->active,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Службы доставки отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
