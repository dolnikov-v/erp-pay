<?php
use app\models\User;

/** @var \app\models\User $model */
?>

<table class="table table-striped table-hover">
    <tbody>
    <tr>
        <td><?= Yii::t('common', 'Имя пользователя') ?>:</td>
        <td><?= $model->username ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('common', 'Роли') ?>:</td>
        <td><?= $model->getRolesNames() ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('common', 'Дата добавления') ?>:</td>
        <td>
            <span title="<?= Yii::$app->formatter->asDatetime($model->created_at) ?>">
                <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('common', 'Активность') ?>:</td>
        <td>
            <?php if ($model->lastact_at > 0): ?>
                <span title="<?= Yii::$app->formatter->asDatetime($model->lastact_at) ?>"><?= Yii::$app->formatter->asRelativeTime($model->lastact_at) ?></span>
            <?php else: ?>
                <span title="<?= Yii::t('common', 'Неизвестно') ?>">—</span>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('common', 'Статус') ?>:</td>
        <td>
            <?php $pingInfo = User::getPingInfo($model); ?>
            <span class="label <?= $pingInfo['class'] ?>">
                <?= $pingInfo['label'] ?>
            </span>
        </td>
    </tr>
    </tbody>
</table>
