<?php
use app\modules\access\assets\SourceSwitcheryAsset;

/** @var string $widget */

SourceSwitcheryAsset::register($this);
?>

<?= $widget ?>
