<?php
use app\modules\access\widgets\CountrySwitchery;

/** @var \app\models\User $model */
/** @var array $countriesByUser */
?>
<table class="table table-striped table-hover" data-user-id="<?= $model->id ?>">
    <?php if (!empty($countryList)) : ?>
        <thead>
        <tr>
            <th width="50%"><?= Yii::t('common', 'Страна') ?></th>
            <th></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($countryList as $country) : ?>
            <?php /** @var \app\models\Country $country */ ?>
            <tr>
                <td><?= Yii::t('common', $country->name) ?></td>
                <td><?= ($country->is_partner ? Yii::t('common', 'Партнерство') : '') ?><?= ($country->is_distributor ? Yii::t('common', 'Дистрибьютор') : '') ?></td>
                <td class="text-center" data-country-id="<?= $country->id ?>">
                    <?= CountrySwitchery::widget([
                        'checked' => $model->isSuperadmin ? true : in_array($country->id, $countriesByUser),
                        'disabled' => $model->isSuperadmin,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Страны отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
