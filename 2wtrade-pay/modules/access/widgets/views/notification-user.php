<?php
use app\modules\access\widgets\NotificationUserChanger;
use app\modules\access\widgets\RunningLineSwitchery;
use yii\helpers\Url;

/** @var app\models\User $model
/** @var app\models\Notification[] $notifications */
/** @var array $userNotifications */
/** @var array $userRunningLineNotifications */
?>

<table id="table_allow_notifications" class="table table-striped table-hover"
       data-user="<?= $model->id ?>"
       data-url-set-notification="<?= Url::toRoute(['/access/user/set-notification']) ?>"
       data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
       data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>"
       data-controller-name="<?= Yii::$app->controller->id ?>">
    <?php if (!empty($notifications)): ?>
        <thead>
        <tr>
            <th class="width-200"><?= Yii::t('common', 'Имя') ?></th>
            <th><?= Yii::t('common', 'Описание') ?></th>
            <th class="text-center width-350"></th>
            <th class="text-center"><?= Yii::t('common', 'В бегущую строку') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($notifications as $notification): ?>
            <tr class="tr-vertical-align-middle tr-notification" data-name="<?= $notification->trigger ?>">
                <td><?= $notification->trigger ?></td>
                <td><?= Yii::t('common', $notification->description) ?></td>
                <td class="text-right">
                    <?= NotificationUserChanger::widget([
                        'trigger' => $notification->trigger,
                        'notifications' => $userNotifications,
                    ]) ?>
                </td>
                <td class="text-center" data-notification-name="<?= $notification->trigger ?>">
                    <?= RunningLineSwitchery::widget([
                            'checked' => isset($userRunningLineNotifications[$notification->trigger]) ? true : false,
                            'disabled' => false,
                        ]);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Оповещения отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
</table>
