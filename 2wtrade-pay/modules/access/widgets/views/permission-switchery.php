<?php
use app\modules\access\assets\PermissionSwitcheryAsset;

/** @var string $widget */

PermissionSwitcheryAsset::register($this);
?>

<?= $widget ?>
