<?php
use app\modules\access\widgets\RoleSwitchery;
use app\models\User;

/** @var \app\models\AuthItem $model */
/** @var \yii\rbac\Role[] $roleList */
/** @var array $rolesByPermission */
/** @var bool $disabled */
?>
<table id="table_allow_roles" class="table table-striped table-hover"
       data-permission="<?= $model->name ?>"
       data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
       data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>">
    <?php if (!empty($roleList)): ?>
        <thead>
        <tr>
            <th class="width-300"><?= Yii::t('common', 'Роль') ?></th>
            <th><?= Yii::t('common', 'Описание') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($roleList as $role): ?>
            <tr class="tr-vertical-align-middle tr-permission" data-name="<?= $role->name ?>">
                <td><?= $role->name ?></td>
                <td><?= Yii::t('common', $role->description) ?></td>
                <td class="text-center" data-role="<?= $role->name ?>">
                    <?= RoleSwitchery::widget([
                        'checked' => $role->name == User::ROLE_SUPERADMIN ? true : in_array($role->name, $rolesByPermission),
                        'disabled' => ($role->name == User::ROLE_SUPERADMIN || $disabled) ? true : false,
                    ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'список пуст') ?></td>
        </tr>
    <?php endif; ?>
</table>
