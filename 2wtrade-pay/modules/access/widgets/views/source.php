<?php
use app\modules\access\widgets\SourceSwitchery;

/** @var \app\models\User $model */
/** @var array $sourcesByUser */
?>
<table class="table table-striped table-hover" data-user-id="<?= $model->id ?>">
    <?php if (!empty($sourcesList)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Источник') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($sourcesList as $sourceId => $source) : ?>
            <tr>
                <td><?= Yii::t('common', $source) ?></td>
                <td class="text-center" data-source-id="<?= $sourceId ?>">
                    <?= SourceSwitchery::widget([
                        'checked' => $model->isSuperadmin ? true : !in_array($sourceId, $sourcesByUser),
                        'disabled' => false,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Источники отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
