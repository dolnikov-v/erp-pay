<?php
use app\modules\access\widgets\NotificationRoleSwitchery;
use app\modules\access\widgets\RunningLineSwitchery;
use yii\helpers\Url;

/** @var app\models\AuthItem $model */
/** @var app\models\Notification[] $modelsNotification */
/** @var array $roleRunningLineNotifications */
/** @var array $triggers */
?>
<table id="table_allow_notification" class="table table-striped table-hover"
       data-role="<?= $model->name ?>"
       data-url-set-notification="<?= Url::toRoute(['/access/role/set-notification']) ?>"
       data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
       data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>"
       data-controller-name="<?= Yii::$app->controller->id ?>">
    <?php if (!empty($modelsNotification)): ?>
        <thead>
        <tr>
            <th class="width-300"><?= Yii::t('common', 'Имя') ?></th>
            <th><?= Yii::t('common', 'Описание') ?></th>
            <th class="width-150"></th>
            <th class="text-center"><?= Yii::t('common', 'В бегущую строку') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($modelsNotification as $notification): ?>
            <tr class="tr-vertical-align-middle tr-notification" data-trigger="<?= $notification->trigger ?>">
                <td data-trigger="<?= $notification->trigger ?>"><?= $notification->trigger ?></td>
                <td><?= Yii::t('common', $notification->description) ?></td>
                <td class="text-right" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationRoleSwitchery::widget([
                        'checked' => in_array($notification->trigger, $triggers)
                    ]) ?>
                </td>
                <td class="text-center" data-notification-name="<?= $notification->trigger ?>">
                    <?= RunningLineSwitchery::widget([
                        'checked' => isset($roleRunningLineNotifications[$notification->trigger]) ? true : false,
                        'disabled' => false,
                    ]);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Оповещения отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
</table>
