<?php
use yii\helpers\Url;
use app\modules\access\widgets\TicketRoleSwitchery;

/** @var app\models\AuthItem $model */
/** @var app\models\TicketGroup[] $modelsGroup */
/** @var array $triggers */
?>
<table id="table_allow_ticket_group" class="table table-striped table-hover"
       data-role="<?= $model->name ?>"
       data-url-set-ticket-group="<?= Url::toRoute(['/access/role/set-ticket-group']) ?>"
       data-i18n-success="<?= Yii::t('common', 'Действие выполнено.') ?>"
       data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено.') ?>">
    <?php if (!empty($modelsGroup)): ?>
        <thead>
        <tr>
            <th class="width-300"><?= Yii::t('common', 'Группа') ?></th>
            <th><?= Yii::t('common', 'Префикс') ?></th>
            <th class="width-150"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($modelsGroup as $group): ?>
            <tr class="tr-vertical-align-middle tr-notification" data-trigger="<?= $group->prefix ?>">
                <td data-trigger="<?= $group->prefix ?>"><?= Yii::t('common', $group->name) ?></td>
                <td><?= $group->prefix ?></td>
                <td class="text-right" data-notification="<?= $group->prefix ?>">
                    <?= TicketRoleSwitchery::widget([
                        'checked' => in_array($group->prefix, $triggers)
                    ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Тикетные группы отсутствуют.') ?></td>
        </tr>
    <?php endif; ?>
</table>
