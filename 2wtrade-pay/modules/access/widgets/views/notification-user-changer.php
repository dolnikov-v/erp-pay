<?php
use app\modules\access\assets\NotificationUserChangerAsset;
use app\widgets\uikit\ButtonGroup;

/** @var string $trigger */
/** @var string $type */
/** @var \app\widgets\Button[] $buttons */

NotificationUserChangerAsset::register($this);
?>

<div class="notification-user-changer">
    <?= ButtonGroup::widget([
        'size' => ButtonGroup::SIZE_SMALL,
        'name' => $trigger,
        'value' => $type,
        'withIcon' => false,
        'buttons' => $buttons,
    ]) ?>
</div>
