<?php
namespace app\modules\access\widgets;

use app\widgets\Button;
use app\widgets\Widget;
use Yii;

/**
 * Class NotificationUserChanger
 * @package app\modules\access\widgets
 */
class NotificationUserChanger extends Widget
{
    const TYPE_ON = 'on';
    const TYPE_OFF = 'off';
    const TYPE_PARENT = 'parent';

    public static $typeLabels = [];

    public $trigger;
    public $notifications = [];

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->initTypeLabels();

        $type = self::TYPE_PARENT;

        if (array_key_exists($this->trigger, $this->notifications)) {
            $type = $this->notifications[$this->trigger] ? self::TYPE_ON : self::TYPE_OFF;
        }

        $buttons = [];

        foreach (self::$typeLabels as $value => $label) {
            $buttons[] = new Button([
                'label' => $label,
                'value' => $value,
            ]);
        }

        return $this->render('notification-user-changer', [
            'trigger' => $this->trigger,
            'type' => $type,
            'buttons' => $buttons,
        ]);
    }

    /**
     * Инициализация меток
     */
    protected function initTypeLabels()
    {
        if (empty(self::$typeLabels)) {
            self::$typeLabels = [
                self::TYPE_ON => Yii::t('common', 'Включено'),
                self::TYPE_OFF => Yii::t('common', 'Выключено'),
                self::TYPE_PARENT => Yii::t('common', 'Наследуется'),
            ];
        }
    }
}
