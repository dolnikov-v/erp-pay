<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class RoleSwitchery
 * @package app\modules\access\widgets
 */
class RoleSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('role-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'role-switchery',
                ],
            ]),
        ]);
    }
}
