<?php
namespace app\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Source
 * @package app\modules\access\widgets
 */
class Source extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $sourcesList;

    /**
     * @var
     */
    public $sourcesByUser;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('source', [
            'model' => $this->model,
            'sourcesList' => $this->sourcesList,
            'sourcesByUser' => $this->sourcesByUser,
        ]);
    }
}
