<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class PermissionSwitchery
 * @package app\modules\access\widgets
 */
class PermissionSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('permission-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'permission-switchery',
                ],
            ]),
        ]);
    }
}
