<?php
namespace app\modules\access\widgets;

use app\widgets\Switchery;

/**
 * Class ApiUserDeliverySwitchery
 * @package app\modules\access\widgets
 */
class ApiUserDeliverySwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('api-user-delivery-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'api-user-delivery-switchery',
                ],
            ]),
        ]);
    }
}
