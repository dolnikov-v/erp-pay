<?php

namespace app\modules\telegram\components;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class TelegramService
 * @package app\modules\telegram\components
 */
class TelegramService
{
    /**
     * @param string|integer $chatId
     * @param string $message
     * @return array
     */
    public static function sendNotification($chatId, $message)
    {
        $return = [];
        $maxSize = 4000;
        $size = mb_strlen($message, 'utf-8');
        if ($size > $maxSize) {
            $lines = preg_split('/$\R?^/m', $message);
            $splitMessage = '';
            foreach ($lines as $line) {
                if (mb_strlen($splitMessage . PHP_EOL . $line, 'utf-8') < $maxSize) {
                    $splitMessage .= $line . PHP_EOL;
                } else {
                    $return = self::call('sendMessage', [
                        'chat_id' => $chatId,
                        'text' => $splitMessage
                    ]);;
                    $splitMessage = $line;
                }
            }
        } else {
            $return = self::call('sendMessage', [
                'chat_id' => $chatId,
                'text' => $message
            ]);
        }

        return $return;
    }

    /**
     * @return array
     */
    public static function getChats()
    {
        return self::call('getUpdates');
    }

    /**
     * @param string $method
     * @param array $params
     * @return array
     */
    protected function call($method, $params = array())
    {
        $return = [];

        $return['url'] = Yii::$app->params['telegram']['api_url'] . Yii::$app->params['telegram']['token'] . '/' . $method;

        if ($params) {
            $return['url'] .= '?' . http_build_query($params);
        }

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $return['url']);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($c);
        $resultHttpCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
        $resultErrorNo = curl_errno($c);

        if ($resultErrorNo) {
            $return['resultCurlError'] = curl_error($c);
        }

        curl_close($c);

        $resultData = json_decode($result);

        $return['resultHttpCode'] = $resultHttpCode;
        $return['data'] = $resultData;

        return $return;
    }
}