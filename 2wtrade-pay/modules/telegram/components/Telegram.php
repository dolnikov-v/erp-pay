<?php

namespace app\modules\telegram\components;

use wtrade\TelegramApi\base\API;
use wtrade\TelegramApi\method\getWebhookInfo;
use wtrade\TelegramApi\method\setWebhook;
use wtrade\TelegramApi\response\Error;
use wtrade\TelegramApi\response\Message;
use app\models\User;
use app\modules\telegram\components\commands\Cron;
use app\modules\telegram\components\commands\Start;
use app\modules\telegram\models\TelegramConnection;
use wtrade\TelegramApi\response\Update;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class Telegram
 * @package app\modules\telegram\components
 */
class Telegram
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var API
     */
    private $api;
    /**
     * @var \wtrade\TelegramApi\response\Update
     */
    private $update;
    /**
     * @var User
     */
    protected $user;

    /**
     * Telegram constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
        $this->api = new API($this->token);
        $this->update = new Update(
            json_decode(
                file_get_contents('php://input'),
                true
            )
        );
    }

//    Если SSL самоподписной - выполнить следующее:
//    curl -F "url=https://fionov.2wtrade-pay.com/webhook/telegram/<token>" -F "certificate=@telegramPublic.pem" https://api.telegram.org/bot<botData>/setwebhook
//    Если нормальный - то можно запустить actionSetWebhook или:
//    https://api.telegram.org/bot<botData>/setwebhook?url=https://fionov.2wtrade-pay.com/webhook/telegram/<token>
//    где <token> - Yii::$app->controller->module->params['telegram']['token'] (произвольный, нужен для авторизации на нашей стороне)
//    <botData> - получаем при регистрации бота (для моего бота '490778274:AAF2Pal760NlMTbPPsVmJ48M1ySTCZSzpaE')

    /**
     * SetWebhook
     * @param int $max
     * @return Error|array|true
     */
    public function setWebhook(int $max = 40)
    {
        $url = Url::to(['telegram/index', 'token' => $this->token], true);
        $response = (new setWebhook($this->token))
            ->setMaxConnections($max)
            ->setUrl($url)
            ->send();
        if (!($response instanceof Error)) {
            $info = (new getWebhookInfo($this->token))
                ->send();
            return $info->__array();
        }
        return $response->__array();
    }

    /**
     * @return array
     */
    protected function commandList()
    {
        return [
            [
                'pattern' => Start::COMMAND_START,
                'callback' => [Start::class, 'commandStart'],
                'hidden' => true
            ],
            [
                'pattern' => Start::COMMAND_CONTACT,
                'callback' => [Start::class, 'commandContact'],
                'hidden' => true
            ],
            [
                'name' => 'Список доступных действий',
                'pattern' => Start::COMMAND_LIST,
                'callback' => [Start::class, 'commandList'],
                'hidden' => true
            ],
            [
                'name' => 'Обновление VOIP от zorra',
                'pattern' => Cron::COMMAND_VOIP_EXPENSE,
                'callback' => [Cron::class, 'commandVoip'],
                'permission' => 'telegram.voip.expense'
            ],
            [
                'name' => 'Пересчет доставки (калькуляторы)',
                'pattern' => Cron::COMMAND_DELIVERY_CALCULATE_CHARGS,
                'callback' => [Cron::class, 'commandDeliveryCalculateCharges'],
                'permission' => 'telegram.delivery.calculate.charges'
            ]
        ];
    }

    /**
     * @return bool
     */
    public function runCommand(): bool
    {
        $this->auth();
        foreach ($this->commandList() as $command) {
            if ($this->canUserRunCommand($command)) {
                if ($this->command($command['pattern'], $command['callback'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param string $basic
     * @return string
     */
    private function createPattern($basic)
    {
        return preg_replace_callback(
            '/(.)?\$\{([\w._-]+)(\?)?\,?\s?([^\}]+)?\}/',
            function ($match) {
                $end = '';
                $name = $match[2];
                $pattern = isset($match[4]) ?
                    $match[4] :
                    '[^\/\s]+';
                $optional = isset($match[3]) &&
                $match[3] == '?' ?
                    true : false;

                if ($optional && isset($match[1]))
                    $start = $match[1] == ' ' ?
                        '\s?' : $match[1];
                else
                    $start = isset($match[1]) ?
                        $match[1] : '';

                if ($optional) {
                    $end = ')?';
                    $start = '(?:' . $start;
                }

                return "$start(?P<$name>$pattern)$end";
            },
            str_replace(
                ['\/', '/'], ['/', '\/'],
                $basic
            )
        );
    }

    /**
     * @param callable $func
     * @param array $params
     * @return mixed
     */
    private function applyCallback($func, $params = [])
    {
        if (!is_array($params))
            $params = (array)$params;

        if (isset($params['msg'])) {
            $msg = $params['msg'];
            unset($params['msg']);
            $func($this->api, $msg, $params);
//            TODO: доделать обработку ошибок, возвращаемых от $func()
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $pattern
     * @param callable $callback
     * @return bool|mixed
     */
    private function command($pattern, $callback)
    {
        $pattern = $this->createPattern($pattern);
        $result = null;

        if ($this->update->hasMessage() && $this->update->message->getContact()
            && $this->update->message->getContact()->getUserId() == $this->update->message->getChat()->getId()) {
            $params[0] = Start::COMMAND_CONTACT;
            $params['msg'] = $this->update->message;
            $params['contact'] = $this->update->message->getContact();
            $result = $this->applyCallback([Start::class, 'commandContact'], $params);
        } elseif (($this->update->hasMessage() &&
                $this->update->message->hasText()) || !empty($this->update->getCallbackQuery()->data)) {
            $text = $this->update->message->text ?? $this->update->getCallbackQuery()->data;
            if (preg_match('/^' . $pattern . '( .+|$)/', $text, $matches) !== false) {
                if (sizeof($matches) > 0) {
                    $params = [];
                    $params['inputOptions'] = $text;
                    if (stripcslashes($pattern) == Start::COMMAND_LIST) {
                        $params['commandList'] = [];
                        foreach ($this->commandList() as $command) {
                            if ($this->canUserRunCommand($command) && empty($command['hidden'])) {
                                $params['commandList'][] = [
                                    'name' => $command['name'] ?? '',
                                    'action' => $command['pattern']
                                ];
                            }
                        }
                    } elseif (preg_match_all('/ \-([a-z_]+)=([^ ]+)/', $matches[1], $data)) {
                        foreach ($data[1] as $key => $value) {
                            if (is_string($value) && !empty($data[2])) {
                                $params[$value] = $data[2][$key] ?? null;
                            }
                        }
                    }
                    $params['msg'] = $this->update->message ?? $this->update->getCallbackQuery()->getMessage();
                    $result = $this->applyCallback($callback, $params);
                }
            }
        }

        return $result == null ? false : $result;
    }

    /**
     * Загрузка прав для юзера
     */
    private function auth()
    {
        $msg = $this->update->getMessage() ?? $this->update->getCallbackQuery()->getMessage();
        $this->user = User::find()
            ->innerJoin(TelegramConnection::tableName(), 'user_id = id and chat_id = :chat_id', [
                ':chat_id' => $msg->getChat()
                    ->getId()
            ])
            ->asArray()
            ->one();


        if (!$this->user || empty($this->user['id'])) {
            $this->user['assignment'] = [];
            $this->user['isSuperadmin'] = false;
        } else {
            $this->user['assignment'] = ArrayHelper::getColumn(\Yii::$app->authManager->getPermissionsByUser($this->user['id']), 'name');
            $this->user['isSuperadmin'] = \Yii::$app->authManager->getAssignment(User::ROLE_SUPERADMIN, $this->user['id']);
        }
    }

    /**
     * Краткая отправка сообщения
     * @param int $chatID
     * @param string $text
     * @return Error|Message
     */
    public function sendMessage(int $chatID, string $text)
    {
        return $this->api->sendMessage()->setChatId($chatID)->setText($text)->send();
    }

    /**
     * @param array $command
     * @return bool
     */
    protected function canUserRunCommand(array $command): bool
    {
        return !empty($this->user['isSuperadmin']) || !isset($command['permission']) || $command['permission'] === true || in_array($command['permission'], $this->user['assignment']);
    }
}