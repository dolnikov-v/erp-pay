<?php
namespace app\modules\telegram\components\commands;

use wtrade\TelegramApi\base\API;
use wtrade\TelegramApi\response\Message;

/**
 * Trait HelpTrait
 * @package app\modules\telegram\components\commands
 */
trait HelpTrait
{
    /**
     * @param API $api
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param string $jobName
     * @param array $keys
     */
    public static function sendHelp(API $api, Message $msg, string $jobName, array $keys = [])
    {
        $params = trim(implode(' ', $keys));
        if (!$params) {
            $params = 'не поддерживает доп.параметры';
        }
        if ($msg->message_id && $msg->from->is_bot) {
            $api->editMessageReplyMarkup()
                ->setChatId($msg->chat->id)
                ->setMessageId($msg->message_id)
                ->remReplyMarkup()
                ->send();
            $api->editMessageText()
                ->setChatId($msg->chat->id)
                ->setMessageId($msg->message_id)
                ->setText('`' . $jobName . ' ' . $params . '`')->setParseMode('markdown')
                ->send();
        } else {
            $api->sendMessage()
                ->setChatId($msg->chat->id)
                ->setText($jobName . ' ' . $params)
                ->send();
        }
    }
}