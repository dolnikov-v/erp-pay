<?php
namespace app\modules\telegram\components\commands;

use wtrade\TelegramApi\keyboard\button\InlineKeyboardButton;
use wtrade\TelegramApi\keyboard\button\KeyboardButton;
use wtrade\TelegramApi\keyboard\InlineKeyboardMarkup;
use wtrade\TelegramApi\keyboard\ReplyKeyboardMarkup;
use wtrade\TelegramApi\method\sendMessage;
use wtrade\TelegramApi\response\Contact;
use wtrade\TelegramApi\base\API;
use app\models\User;
use app\modules\telegram\models\TelegramBot;
use app\modules\telegram\models\TelegramConnection;
use wtrade\TelegramApi\response\Message;

/**
 * Class Start
 * @package app\modules\telegram\components\commands
 */
class Start
{
    const COMMAND_START = '/start';
    const COMMAND_CONTACT = '/contact';
    const COMMAND_LIST = '/commandList';

    /**
     * @param sendMessage $message
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param array $params
     * @return \wtrade\TelegramApi\response\Error|\wtrade\TelegramApi\response\Message
     */
    public static function commandStart(API $api, \wtrade\TelegramApi\response\Message $msg, $params = [])
    {
        $markup = null;
        $bot = $api->getMe()->send();

        if ($bot->getId()) {
            $telegram = TelegramBot::find()->where(['id' => $bot->getId()])->one();
            if ($telegram) {
                $markup = new ReplyKeyboardMarkup();
                $markup->resize_keyboard = true;

                if (TelegramConnection::find()
                    ->where(['chat_id' => $msg->chat->getId(), 'bot_id' => $telegram->getAttribute('id')])
                    ->count()) {
                    $NumberBtn = new KeyboardButton();
                    $NumberBtn->text = '/commandList';
                    $markup->addButton($NumberBtn, 1);
                } else {
                    $NumberBtn = new KeyboardButton();
                    $NumberBtn->text = 'Отправить свой номер телефона';
                    $NumberBtn->request_contact = true;
                    $markup->addButton($NumberBtn, 1);
                }
            }
        }

        return $api->sendMessage()->setChatId($msg->chat->id)
            ->setText('Hello!' . PHP_EOL . 'Твой ID ' . $msg->chat->id)
            ->setReplyMarkup($markup)->send();
    }

    /**
     * @param sendMessage $message
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param array $params
     * @return \wtrade\TelegramApi\response\Error|\wtrade\TelegramApi\response\Message
     */
    public static function commandContact(API $api, Message $msg, $params = [])
    {
        /**
         * @var Contact $contact
         */
        $text = 'Поврежденные данные!';
        $markup = null;

        if (isset($params['contact'])) {
            $contact = $params['contact'];
            $phone = trim(str_replace(' ', '', $contact->getPhoneNumber()), '+');
            $user = User::find()->where(['phone' => $phone])->one();
            $bot = $api->getMe()->send();
            $text = 'Нам не удалось найти вас в системе' . PHP_EOL . 'Обратитесь за помощью в тех.поддержку. Ваш ID ' . $msg->chat->getId();

            if ($user && $user->getId() && $bot->getId()) {
                $telegram = TelegramBot::find()->where(['id' => $bot->getId()])->one();
                if (!$telegram) {
                    $text = 'Неизвестный бот!';
                } elseif (TelegramConnection::find()->where(['user_id' => $user->getId(), 'bot_id' => $telegram->getAttribute('id')])->count() <= 0) {
                    $connection = new TelegramConnection();
                    $connection->user_id = $user->getId();
                    $connection->bot_id = $telegram->getAttribute('id');
                    $connection->chat_id = $msg->chat->getId();

                    if ($connection->save()) {
                        $text = 'Теперь Вы авторизованы как ' . $user->getAttribute('username') . '.';
                        $markup = new ReplyKeyboardMarkup();
                        $markup->resize_keyboard = true;
                        $NumberBtn = new KeyboardButton();
                        $NumberBtn->text = '/commandList';
                        $markup->addButton($NumberBtn, 1);
                    } else {
                        $text = 'Ошибка сохранения связи!';
                    }
                } else {
                    $text = 'Вы уже авторизованы в одном из чатов данного бота!';
                }
            }
        }
        return $api->sendMessage()->setChatId($msg->chat->id)->setReplyMarkup($markup)->setText($text)->send();
    }

    /**
     * @param API $api
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param array $params
     * @return \wtrade\TelegramApi\response\Error|\wtrade\TelegramApi\response\Message
     */
    public static function commandList(API $api, \wtrade\TelegramApi\response\Message $msg, $params = [])
    {
        if (!empty($params['commandList']) && is_array($params['commandList'])) {
            $markup = new InlineKeyboardMarkup();
            foreach ($params['commandList'] as $key => $command) {
                $callbackBtn = new InlineKeyboardButton();
                $callbackBtn->text = $command['name'];
                $callbackBtn->callback_data = $command['action'];
                $markup->addButton($callbackBtn, $key + 1, 1);
                $callbackBtn = new InlineKeyboardButton();
                $callbackBtn->text = 'доп.парам.';
                $callbackBtn->callback_data = $command['action'] . ' -help=true';
                $markup->addButton($callbackBtn, $key + 1, 2);
            }
            return $api->sendMessage()->setChatId($msg->chat->id)
                ->setText('Доступные действия:')
                ->setReplyMarkup($markup)->send();
        } else {
            return $api->sendMessage()->setChatId($msg->chat->id)
                ->setText('У вас нет доступных действий')
                ->send();
        }
    }
}