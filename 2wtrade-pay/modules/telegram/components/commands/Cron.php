<?php
namespace app\modules\telegram\components\commands;

use wtrade\TelegramApi\base\API;
use Yii;

/**
 * Class Cron
 * @package app\modules\telegram\components\commands
 */
class Cron
{
    const COMMAND_VOIP_EXPENSE = '/voip';
    const COMMAND_DELIVERY_CALCULATE_CHARGS = '/deliveryCalculate';

    use HelpTrait;

    /**
     * @param API $api
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param array $params
     */
    public static function commandVoip(API $api, \wtrade\TelegramApi\response\Message $msg, $params = [])
    {
        if (!empty($params['help'])) {
            $keys = array_keys(\app\jobs\UpdateVoipExpense::optionAliases());
            foreach ($keys as &$item) {
                $item = '-' . $item . '=...';
            }
            self::sendHelp($api, $msg, self::COMMAND_VOIP_EXPENSE, $keys);
            return true;
        }
        foreach (\app\jobs\UpdateVoipExpense::optionAliases() as $key => $val) {
            if (isset($params[$key])) {
                $params[$val] = $params[$key];
                unset($params[$key]);
            }
        }
        $params['jobTelegramID'] = $msg->chat->id;
        Yii::$app->queueTelegram->push(new \app\jobs\UpdateVoipExpense($params));

        if ($msg->message_id && $msg->from->is_bot) {
            $api->editMessageReplyMarkup()
                ->setChatId($msg->chat->id)
                ->setMessageId($msg->message_id)
                ->remReplyMarkup()
                ->send();
            $api->editMessageText()->setChatId($msg->chat->id)->setMessageId($msg->message_id)->setText('Cron VoipExpense отправлен на исполнение.')->send();
        } else {
            $api->sendMessage()
                ->setChatId($msg->chat->id)
                ->setText('Cron VoipExpense отправлен на исполнение.')
                ->send();
        }
    }

    /**
     * @param API $api
     * @param \wtrade\TelegramApi\response\Message $msg
     * @param array $params
     */
    public static function commandDeliveryCalculateCharges(API $api, \wtrade\TelegramApi\response\Message $msg, array $params = [])
    {
        $nullParam = true;
        foreach (\app\jobs\DeliveryCalculateCharges::optionAliases() as $key => $val) {
            if (isset($params[$key])) {
                $nullParam = false;
                $params[$val] = $params[$key];
                unset($params[$key]);
            }
        }
        if (!empty($params['help']) || $nullParam) {
            $keys = array_keys(\app\jobs\DeliveryCalculateCharges::optionAliases());
            foreach ($keys as &$item) {
                $item = '-' . $item . '=...';
            }
            self::sendHelp($api, $msg, self::COMMAND_DELIVERY_CALCULATE_CHARGS, $keys);
            return true;
        }

        $params['jobTelegramID'] = $msg->chat->id;
        Yii::$app->queueTelegram->push(new \app\jobs\DeliveryCalculateCharges($params));

        if ($msg->message_id && $msg->from->is_bot) {
            $api->editMessageReplyMarkup()
                ->setChatId($msg->chat->id)
                ->setMessageId($msg->message_id)
                ->remReplyMarkup()
                ->send();
            $api->editMessageText()->setChatId($msg->chat->id)->setMessageId($msg->message_id)->setText('Cron deliveryCalculate отправлен на исполнение.')->send();
        } else {
            $api->sendMessage()
                ->setChatId($msg->chat->id)
                ->setText('Cron deliveryCalculate отправлен на исполнение.')
                ->send();
        }
    }

}