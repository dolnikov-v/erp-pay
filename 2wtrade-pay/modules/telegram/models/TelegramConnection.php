<?php
namespace app\modules\telegram\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class TelegramConnection
 * @package app\modules\telegram\models
 * @property int $bot_id
 * @property int $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class TelegramConnection extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%telegram_connection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'bot_id', 'user_id', 'chat_id'], 'integer'],
            [['bot_id', 'user_id', 'chat_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bot_id' => Yii::t('common', 'Бот'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения')
        ];
    }
}