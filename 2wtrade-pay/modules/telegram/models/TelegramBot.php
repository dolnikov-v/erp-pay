<?php
namespace app\modules\telegram\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Боты и данные для авторизации
 * Class TelegramBot
 * @package app\modules\telegram\models
 * @property int $id
 * @property string $name
 * @property string $bot_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class TelegramBot extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     * return string
     */
    public static function tableName()
    {
        return '{{%telegram_bot}}';
    }

    /**
     * @inheritdoc
     * return array
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'bot_token'], 'string', 'max' => 255],
            [['id', 'token'], 'required'],
            ['description', 'string']
        ];
    }

    /**
     * @inheritdoc
     * return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Имя'),
            'token' => Yii::t('common', 'Токен'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения')
        ];
    }
}