<?php

namespace app\modules\systemstatus\models\query;

use app\components\db\ActiveQuery;
use app\modules\systemstatus\models\OrderStats;

/**
 * @package app\modules\systemstatus\models\query
 * @method OrderStats one($db = null)
 * @method OrderStats[] all($db = null)
 */
class OrderStatsQuery extends ActiveQuery
{

}
