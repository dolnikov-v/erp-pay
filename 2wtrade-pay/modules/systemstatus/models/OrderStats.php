<?php

namespace app\modules\systemstatus\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Статистика по количеству заказов в различных статусах
 *
 * @property integer $id
 * @property string $metric
 * @property integer $value
 * @property integer $created_at
 */
class OrderStats extends ActiveRecord
{
    /**
     * @inheritdoc
     * @return \app\modules\systemstatus\models\query\OrderStatsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\systemstatus\models\query\OrderStatsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_stats}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'], // stats are not supposed to be updated
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'metric' => 'Metric',
            'value' => 'Value',
            'created_at' => 'Created At',
        ];
    }
}