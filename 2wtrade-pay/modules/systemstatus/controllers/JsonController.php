<?php

namespace app\modules\systemstatus\controllers;

use yii\rest\Controller;
use yii\db\Query;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Цифры про здоровье системы
 */
class JsonController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors ['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $last_order_info = (new Query())
            ->select('o.id, o.created_at')
            ->from('`order` o')
            ->orderBy(['o.created_at' => SORT_DESC])
            ->limit(1)
            ->one();


        return [
            'last_order_id' => $last_order_info['id'],
            'last_order_age' => time () - intval($last_order_info['created_at']),
        ];
    }
}
