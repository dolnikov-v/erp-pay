<?php

use miloschuman\highcharts\Highstock;
use yii\web\JsExpression;

use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $series */

$this->title = Yii::t('common', 'Статистика и мониторинг');
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>


<?= Highstock::widget([
    'options' => [
        'rangeSelector' => [
            'inputEnabled' => new JsExpression('$("#container").width() > 480'),
            'selected' => 1
        ],
        'title' => [
            'text' => Yii::t('common', 'Одобрено КЦ, но ещё не в доставке'),
        ],
        'series' => $series,
        'plotOptions' => [
            'area' => [
                'stacking' => 'normal',
            ],
        ],
    ],
]);


?>

