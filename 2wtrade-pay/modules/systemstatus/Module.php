<?php

namespace app\modules\systemstatus;

use Yii;
use app\modules\auth\components\filters\AccessControl;


/**
 * systemstatus module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @return array
     */
    public function behaviors()
    {

        Yii::$app->user->enableSession = false;

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];

    }

}
