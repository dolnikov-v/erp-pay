<?php

namespace app\modules\logger\controllers;

use app\modules\logger\components\log\filters\TagFilter;
use app\modules\logger\components\log\filters\DateFilter;
use app\components\web\Controller;
use yii\db\Query;
use Yii;
use yii\base\Model;

/**
 * Просмотр логов. Пока пустышка.
 */
class BrowserController extends Controller
{
    const PAGE_SIZE = 20;

    /**
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * @var $tagFilter
     */
    public $tagFilter;


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
//        $log = $log("запись без тегов");
//        $log = $log("вторая запись без тегов");
//        $log = $log("запись с тегом про ордерид", ['order_id' => 1234]);
//        $log = $log("запись с тем же тегом");
//        $log = $log("добавляем новый тег", ['marketing_list_id'=>43245]);
//        $log = $log("пытаемся переписать старый и добавить новый тег", ['order_id' => 'hueraga', 'user_id' => 432443]);
//        $log("без новых тегов");

        $queryParams = Yii::$app->request->queryParams;
        $tags = $this->prepareData($queryParams);


        if (isset($queryParams['s']['from'], $queryParams['s']['to'])) {
            $dateParams = [
                'from' => $queryParams['s']['from'],
                'to' => $queryParams['s']['to'],
            ];
        }

        if (empty($dateParams['from']) && empty($dateParams['to'])) {
            $dateParams = $this->getDefault();
        }

        $params = [
            'tags' => $tags
        ];

        $params = array_merge($params, $dateParams);

        $dataProvider = $this->module->logger->find($params);
        $dataProvider->pagination->pageSize = self::PAGE_SIZE;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'logger' => $this->module->logger,
        ]);
    }

    /**
     * Старый вариант с урлом так же остаётся рабочим, для фильтров - подготовка данных
     * @param $tags []
     * @return array
     */
    public function prepareData($tags)
    {
        $preparedTags = [];

        if (isset($tags['s'])) {
            if (isset($tags['s']['tag'], $tags['s']['value'])) {
                foreach ($tags['s']['tag'] as $k => $v) {
                    if (isset($tags['s']['value'][$k])) {
                        $preparedTags[$v] = $tags['s']['value'][$k];
                    }
                }
            }
        } else {
            return $tags;
        }

        return $preparedTags;
    }

    /**
     * @return TagFilter
     */
    public function getTagFilter()
    {
        if (is_null($this->tagFilter)) {
            $this->tagFilter = new TagFilter();
        }

        return $this->tagFilter;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getTagFilter()->apply($params);
        $this->getDateFilter()->apply($params);

        return $this;
    }

    /**
     * @param $form
     * @return string
     */
    public function restore($form)
    {
        return $output = $this->getDateFilter()->restore($form);
    }

    public function getDefault()
    {
        $s = [];
        $s['to'] = date('d.m.Y', time());
        $s['from'] = date('d.m.Y', time());
        return $s;
    }

}
