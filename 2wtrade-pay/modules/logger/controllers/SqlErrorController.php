<?php

namespace app\modules\logger\controllers;

use app\models\SqlErrorLog;
use app\components\web\Controller;
use yii\data\ActiveDataProvider;

class SqlErrorController extends Controller
{
    public function actionIndex()
    {

        $query = SqlErrorLog::find()->orderBy(['created_at' => SORT_DESC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => SqlErrorLog::findOne($id),
        ]);
    }
}
