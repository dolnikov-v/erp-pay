<?php
namespace app\modules\logger\components\log\filters;

use app\modules\logger\widgets\filters\DateFilter as DateFilterWidget;
use Yii;

/**
 * Class DateFilter
 * @package app\modules\logger\components\log\filters
 */
class DateFilter extends Filter
{
    public $from;
    public $to;
    public $params;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $params = yii::$app->request->get($this->formName());
        $from = isset($params['from']) && !empty($params['from']) && yii::$app->formatter->asDate($params['from']) ? $params['from'] : time();
        $to = isset($params['to']) && !empty($params['to']) && yii::$app->formatter->asDate($params['to']) ? $params['to'] : time();

        $this->from = Yii::$app->formatter->asDate($from);
        $this->to = Yii::$app->formatter->asDate($to);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param array $params
     */
    public function apply($params)
    {
        $this->load($params);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DateFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
