<?php
namespace app\modules\logger\components\log\filters;

use app\modules\administration\components\crontab\Logger;
use app\modules\logger\widgets\filters\TagFilter as TagFilterWidget;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class TagFilter
 * @package app\modules\logger\components\log\filters
 */
class TagFilter extends Filter
{
    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $value;

    /**
     * @var TagFilter
     */
    public $tagFilter;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['tag', 'value'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'tag' => Yii::t('common', 'Тег'),
            'value' => Yii::t('common', 'Значение'),
        ];
    }

    /**
     * @param array $params
     */
    public function apply($params)
    {
        $this->load($params);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return TagFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
