<?php
namespace app\modules\logger\components\log\filters;

use Yii;
use yii\base\Model;

/**
 * Class Filter
 * @package app\modules\logger\components\log\filters
 */
abstract class Filter extends Model
{
    /**
     * @param array $params
     */
    public abstract function apply($params);

    /**
     * @param \app\components\widgets\ActiveForm $form
     */
    public abstract function restore($form);

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }
}
