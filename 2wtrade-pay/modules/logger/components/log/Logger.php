<?php
namespace app\modules\logger\components\log;

use yii\di\Instance;
use yii\base\Component;
use yii\db\Connection;
use yii\db\Query;
use Yii;


/**
 * Class Logger
 * @package app\modules\logger\components\log
 */
class Logger extends Component
{
    /**
     * @var Connection
     */
    public $db = 'db';

    /**
     * подтягивает $this->db если он задан не в виде готового инстанса
     *
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::className());
        $this->db->getSchema()->refresh();
    }

    /**
     * Рабочий прототип. Отдает записи помеченные всеми указанными тегами
     *
     * @param $params
     * @return object dataProvider
     */
    public function find($params)
    {
        $tags = isset($params['tags']) ? $params['tags'] : [];

        $query = new Query ();
        // todo: кастомный query уже с джойнами?
        $query->select(['log.id', 'log.created_at', 'log.message', 'log.tags'])
            ->from('log')
            ->leftJoin('tag', 'tag.log_id = log.id');

        $anyTag = false;
        foreach ($tags as $name => $value) {
            if ($name && $value) {
                $anyTag = true;
                $query->orWhere(['tag.name' => $name, 'tag.value' => $value]);
            }
        }

        if (!empty($params['from'])) {
            $query->andWhere(['>=', 'log.created_at', strtotime($params['from'].' 00:00:00')]);
        }

        if (!empty($params['to'])) {
            $query->andWhere(['<=', 'log.created_at', strtotime($params['to'].' 23:59:59')]);
        }

        $query->groupBy('log.id');
        if ($anyTag) {
            $query->having("count(1) = " . count($tags)); // нам нужны только записи содержащие все запрошенные теги
        }
        $query->orderBy(['log.id' => SORT_DESC]);

        $dataProvider = new LoggerDataProvider();
        $dataProvider->db = $this->db;
        $dataProvider->query = $query;

        return $dataProvider;
    }

    /**
     * Пишет в базу
     *
     * @param string $message
     * @param array $tags
     * @return array
     */
    public function write($message, $tags)
    {

        // make sure tags are strings
        $tags = array_map('strval', $tags);

        $tagsJson = json_encode($tags);
        if (strlen($tagsJson) > 4000) {
            $tagsJson = json_encode("serialized tags string was too long");
        }

        if (strlen($message) > 4000) {
            $message = substr($message, 0, 4000) . " [message is cropped]";
        }

        try {
            if (!$this->db->createCommand()->insert('{{log}}', ['created_at' => time(), 'message' => $message, 'tags' => $tagsJson])->execute()) {
                throw new \Exception ("zero rows inserted while trying to save log message");
            }

            $logId = $this->db->getLastInsertID();

            if (!empty($tags)) {
                $tagsData = [];
                foreach ($tags as $name => $value) {
                    $tagsData[] = [$name, $value, $logId];
                }
                if (!$this->db->createCommand()->batchInsert('{{tag}}', ['[[name]]', '[[value]]', '[[log_id]]'], $tagsData)->execute()) {
                    throw new \Exception ("zero rows inserted while trying to attach tags to the log record: " . print_r($tagsData, true));
                }
            }

        } catch (\Exception $e) {
            // todo:: писать в аварийный лог на диск?
            return false;
        }

        return true;
    }

    /**
     * создает новую логгер-функцию с прошитым набором тегов
     *
     * @param array $bakedInTags
     * @return \Closure
     */
    public function getLogger($bakedInTags = [])
    {
        /**
         * Записывает лог с указанными тегами, и возвращает новую логгер-функцию, при вызове которой эти теги указывать уже не обязательно
         * @val $logger \Closure
         * @param string $message
         * @param array $tags
         */

        $logger = function ($message, $tags = []) use ($bakedInTags) {
            // todo: разрешить менять уже существующие теги?
            $mergedTags = $bakedInTags + $tags;
            $this->write($message, $mergedTags);
            return $this->getLogger($mergedTags);
        };

        return $logger;
    }
}
