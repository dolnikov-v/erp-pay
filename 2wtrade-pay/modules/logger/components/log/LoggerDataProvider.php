<?php
namespace app\modules\logger\components\log;

use Yii;
use yii\db\ActiveQueryInterface;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Connection;
use yii\db\QueryInterface;
use yii\di\Instance;

class LoggerDataProvider extends ActiveDataProvider
{
    protected function prepareModels()
    {
        $result = parent::prepareModels();

        $preparedResult = [];

        if (!empty($result)) {
            foreach ($result as $index => $data) {
                $preparedResult[$index] = [];
                foreach ($data as $k => $v) {
                    if ($k == 'tags') {
                        $array = json_decode($v, 1);

                        $preparedResult[$index][$k] = is_array($array) ? $array : [$v];
                    } else {
                        $preparedResult[$index][$k] = $v;
                    }
                }
            }
            return $preparedResult;
        } else {
            return $result;
        }
    }
}
