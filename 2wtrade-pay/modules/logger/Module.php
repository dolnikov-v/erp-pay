<?php

namespace app\modules\logger;

use Yii\db\Connection;
use yii\di\Instance;
use app\modules\logger\components\log\Logger;


/**
 * logger module definition class
 */
class Module extends \app\components\base\Module
{
    /**
     * @var Logger
     */
    public $logger;

    /**
     * подтягивает $this->logger если он задан не в виде готового инстанса
     *
     */
    public function init()
    {
        parent::init();
        $this->logger = Instance::ensure($this->logger, Logger::className());
    }

}
