<?php

namespace app\modules\logger\assets;

use yii\web\AssetBundle;

/**
 * Class TagFilterAsset
 * @package app\modules\logger\assets
 */
class TagFilterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/logger/tag-filter';

    public $js = [
        'tag-filter.js',
    ];

    public $css = [
        'tag-filter.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
