<?php

use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/** @var \yii\web\View $this */
/** @var \app\models\SqlErrorLog $model */

$this->title = Yii::t('common', 'Просмотр ошибки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Ошибки SQL запросов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Просмотр ошибки')];
?>

<?= Panel::widget([
    'content' => DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'sql'
            ],
            [
                'attribute' => 'message'
            ],
            [
                'attribute' => 'route'
            ],
            [
                'attribute' => 'trace',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::tag('pre', $model->trace);
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at);
                }
            ],
        ]
    ])
]) ?>
