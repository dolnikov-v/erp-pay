<?php

use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\widgets\Panel;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Ошибки SQL запросов');
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с SQL ошибками'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'message',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'route',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Подробнее'),
                'content' => function ($model) {
                    return Html::a(Yii::t('common', 'Посмотреть'), Url::toRoute(['view', 'id' => (string)$model->_id]));
                }
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>




