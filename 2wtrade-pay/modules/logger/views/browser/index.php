<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\api\models\ApiLog;
use app\widgets\assets\DatePickerAsset;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\modules\logger\widgets\LoggerFilterTag;
use app\modules\logger\components\log\Logger;

/** @var yii\web\View $this */
/** @var  DataProvider $records */
/** @var  app\modules\logger\components\log\Logger $dataProvider */
/** @var  app\modules\logger\components\log\Logger $logger */

$this->title = Yii::t('common', 'Просмотр логов процессинга');
?>

<?= LoggerFilterTag::widget([
    'dataProvider' => $dataProvider,
    'logger' => $logger
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover tl-fixed',
        ],
        'columns' => [
            [
                'attribute' => 'tags',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-350'],
                'content' => function ($data) {

                    $content = '';

                    foreach ($data['tags'] as $k => $v) {
                        $filterParams = yii::$app->request->get('s');
                        $urlto = [];
                        $filterParams['tag'][] = $k;
                        $filterParams['value'][] = $v;
                        $urlto = array_merge(['browser/index'], ['s' => $filterParams]);
                        $filterUrl = Url::to($urlto);
                        $content .= Html::a($k . ' => ' . $v, $filterUrl) . '<br/>';
                    }

                    return $content;
                }
            ],
            [
                'attribute' => 'message',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>




