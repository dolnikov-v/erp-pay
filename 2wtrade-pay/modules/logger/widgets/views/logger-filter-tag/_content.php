<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\logger\controllers\BrowserController $browser */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <?= $browser->getDateFilter()->restore($form) ?>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $browser->getTagFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
