<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\logger\components\log\Logger $logger */
/** @var app\modules\logger\controllers\BrowserController $browser */
?>

<?= Panel::widget([
    'id' => 'logger_filter_tag',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'browser' => $browser,
        'logger' => $logger
    ]),
    'collapse' => true,
]) ?>

