<?php

namespace app\modules\logger\widgets;

use app\modules\logger\components\log\Logger;
use app\modules\logger\controllers\BrowserController;


/**
 * ClassLoggerFiltertag
 * @package app\modules\logger\widgets
 */
class LoggerFilterTag extends LoggerFilter
{
    /** @var Logger */
    public $logger;

    /** @var $dataProvider */
    public $dataProvider;

    /** @var $browser */
    public $browser;

    /** @var   $form */
    public $form;

    /** @var  integer */
    public $pages;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('logger-filter-tag/filter', [
            'logger' => $this->logger,
            'browser' => new BrowserController('Browser', $this->logger)
        ]);
    }
}
