<?php
namespace app\modules\logger\widgets;

use app\modules\logger\components\log\Logger;
use yii\base\InvalidParamException;
use yii\base\Widget;
use Yii;

/**
 * Class LoggerFilter
 * @package app\modules\logger\widgets
 */
abstract class LoggerFilter extends Widget
{
    /** @var Logger */
    public $logger;

    /**
     * @return string
     */
    public abstract function apply();

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->logger instanceof Logger) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать корректную форму.'));
        }

        return $this->apply();
    }
}
