<?php
use yii\helpers\Html;
use app\widgets\Button;
use app\helpers\WbIcon;
use app\modules\logger\assets\TagFilterAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\logger\components\log\filters\TagFilter $model */
/** @var app\modules\logger\assets\TagFilterAsset TagFilterAsset */

TagFilterAsset::register($this);

$filterParam = Yii::$app->request->get('s');
?>

<div class="row container-tag-filter">

    <?php if (!empty($filterParam) && isset($filterParam['tag']) && isset($filterParam['value'])): ?>

        <?php foreach ($filterParam['tag'] as $k => $v) : ?>

            <?php end($filterParam['tag']); ?>
            <div class="row-tag-filter">
                <div class="col-md-5">
                    <?= $form->field($model, 'tag')->textInput([
                        'name' => Html::getInputName($model, 'tag') . '[]',
                        'value' => $v,
                        'placeholder' => ''
                    ]); ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'value')->textInput([
                        'name' => Html::getInputName($model, 'value') . '[]',
                        'value' => isset($filterParam['value'][$k]) ? $filterParam['value'][$k] : '',
                        'placeholder' => ''
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="control-label">
                            <label>&nbsp;</label>
                        </div>
                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::MINUS,
                            'style' => Button::STYLE_DANGER . ($k == key($filterParam['tag']) ? ' hidden ' : ' ') . 'delete-tag-filter',
                        ]) ?>

                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::PLUS,
                            'style' => Button::STYLE_SUCCESS . ($k < key($filterParam['tag']) ? ' hidden ' : ' ') . ' add-tag-filter',
                        ]) ?>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    <?php else : ?>
        <div class="row-tag-filter">
            <div class="col-md-5">
                <?= $form->field($model, 'tag')->textInput([
                    'name' => Html::getInputName($model, 'tag') . '[]',
                    'placeholder' => ''
                ]); ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'value')->textInput([
                    'name' => Html::getInputName($model, 'value') . '[]',
                    'placeholder' => ''
                ]); ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <div class="control-label">
                        <label>&nbsp;</label>
                    </div>
                    <?= Button::widget([
                        'type' => 'button',
                        'icon' => WbIcon::MINUS,
                        'style' => Button::STYLE_DANGER . ' delete-tag-filter',
                    ]) ?>

                    <?= Button::widget([
                        'type' => 'button',
                        'icon' => WbIcon::PLUS,
                        'style' => Button::STYLE_SUCCESS . ' add-tag-filter',
                    ]) ?>

                </div>
            </div>
        </div>
    <?php endif; ?>


</div>




