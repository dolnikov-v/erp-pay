<?php
namespace app\modules\logger\widgets\filters;

use yii\base\Widget;

/**
 * Class TagFilter
 * @package app\modules\logger\widgets\filters
 */
class TagFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\logger\components\log\filters\TagFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('tag-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
