<?php
namespace app\modules\logger\widgets\filters;

use yii\base\Widget;

/**
 * Class DateFilter
 * @package app\modules\logger\widgets\filters
 */
class DateFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\logger\components\log\filters\DateFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
