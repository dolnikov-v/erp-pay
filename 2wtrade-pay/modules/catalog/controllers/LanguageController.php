<?php
namespace app\modules\catalog\controllers;

use yii\helpers\Url;
use app\components\web\Controller;
use app\models\Language;
use app\models\search\LanguageSearch;
use Yii;
use yii\web\HttpException;

/**
 * Class LanguageController
 * @package app\modules\catalog\controllers
 */
class LanguageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new LanguageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Language();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Язык успешно добавлена.') : Yii::t('common', 'Язык успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));

            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \app\models\Language
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Language::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Язык не найден.'));
        }

        return $model;
    }
}
