<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\Country;
use app\modules\catalog\models\search\SmsPollQuestionsSearch;
use app\modules\order\models\SmsPollQuestions;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class SmsPollQuestionsController
 * @package app\controllers\catalog
 */
class SmsPollQuestionsController extends Controller
{
    const SUCCESS_ADD_QUESTION = 'Вопрос успешно добавлен';
    const SUCCESS_SAVE_QUESTION = 'Вопрос успешно сохранён';
    const SUCCESS_DELETE_QUESTION = 'Вопрос успешно удалён';
    const QUESTION_NOT_FOUND = 'Вопрос не найден';

    /**
     * @param integer $id
     * @return SmsPollQuestions
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = SmsPollQuestions::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', self::QUESTION_NOT_FOUND));
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new SmsPollQuestions();
        $modelSearch = new SmsPollQuestionsSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        $countryList = Country::find()->collection();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'model' => $model,
            'countryList' => $countryList
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new SmsPollQuestions();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', self::SUCCESS_ADD_QUESTION) : Yii::t('common', self::SUCCESS_SAVE_QUESTION), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;
        $data['countryList'] = Country::find()->collection();

        return $this->render('edit', $data);
    }


    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);
        $modelSearch = new SmsPollQuestionsSearch();
        $queryParams['SmsPollQuestionsSearch']['id'] = $model->id;
        $dataProvider = $modelSearch->search($queryParams);

        return $this->render('view', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', self::SUCCESS_DELETE_QUESTION), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }
}