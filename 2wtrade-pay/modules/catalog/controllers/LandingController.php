<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\Landing;
use app\models\search\LandingSearch;
use app\models\search\ProductLandingSearch;
use app\models\search\ProductSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class LandingController
 * @package app\controllers\catalog
 */
class LandingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new LandingSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Landing();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Лендинг успешно добавлен.') : Yii::t('common', 'Лендинг успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionProducts($id)
    {
        $model = $this->getModel($id);

        $modelSearch = new ProductLandingSearch();

        $queryParams['ProductLandingSearch']['landing_id'] = $model->id;

        $dataProvider = $modelSearch->search($queryParams);

        return $this->render('products', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return Landing
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Landing::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Лендинг не найден.'));
        }

        return $model;
    }
}
