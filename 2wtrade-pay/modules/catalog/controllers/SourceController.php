<?php
namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Product;
use app\models\Source;
use app\models\Country;
use app\models\search\SourceSearch;
use app\models\SourceCountry;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class SourceController
 * @package app\controllers\catalog
 */
class SourceController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-country',
                    'set-product',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new SourceSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Source();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Источник успешно добавлен.') : Yii::t('common', 'Источник успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $controller = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/';

        return $this->render('edit', [
            'model' => $model,
            'controller' => $controller,
            'countries' => Country::find()->active()->all(),
            'products' => Product::find()->all(),
            'countriesBySource' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->countries, 'id'),
            'productsBySource' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->products, 'id'),
        ]);
    }

    /**
     * Ajax Связь источник - страна
     * @throws \yii\web\HttpException
     * @todo Требует рефакторинга
     */
    public function actionSetCountry()
    {

        $source_id = (int)Yii::$app->request->post('model_id');
        $country_id = (int)Yii::$app->request->post('country_id');

        $source = Source::find()->where(['id' => $source_id])->one();
        $country = Country::find()->where(['id' => $country_id])->one();

        if (!$source || !$country) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране или источнику.'));
        }
        $sourceCountry = SourceCountry::findOne(['source_id' => $source_id, 'country_id' => $country_id,]);

        //Если нет модели, то создаем новую
        if (!$sourceCountry) {
            $sourceCountry = new SourceCountry();
            //$sourceCountry->setScenario('insert');
            $sourceCountry->source_id = $source_id;
            $sourceCountry->country_id = $country_id;
        }

        $value = Yii::$app->request->post('value');
        switch ($value) {
            case 'on':
                $sourceCountry->active = 1;
                break;
            case 'off':
                $sourceCountry->active = 0;
                break;
        }

        if (!$sourceCountry->save()) {
            Yii::$app->notifier->addNotificationsByModel($sourceCountry);
        };

        return [
            'success' => true,
        ];
    }

    /**
     * Ajax Связь источник - продукт
     * @throws \yii\web\HttpException
     */
    public function actionSetProduct()
    {

        $source_id = (int)Yii::$app->request->post('model_id');
        $product_id = (int)Yii::$app->request->post('product_id');

        $source = Source::find()->where(['id' => $source_id])->one();
        $product = Product::find()->where(['id' => $product_id])->one();

        if (!$source || !$product) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему продукту или источнику.'));
        }

        $value = Yii::$app->request->post('value');
        try {
            switch ($value) {
                case 'on':
                    $source->link('products', $product);
                    break;
                case 'off':
                    $source->unlink('products', $product, true);
                    break;
            }
        } catch (\Exception $e) {
            Yii::$app->notifier->addNotification(print_r($e->getMessage()));
        }
        return [
            'success' => true,
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Источник активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Источник деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Source
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Source::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Источник не найден.'));
        }

        return $model;
    }
}