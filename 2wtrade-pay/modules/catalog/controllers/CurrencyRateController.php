<?php

namespace app\modules\catalog\controllers;


use app\components\web\Controller;
use app\models\search\CurrencyRateHistorySearch;

/**
 * Class CurrencyRateController
 * @package app\modules\catalog\controllers
 */
class CurrencyRateController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new CurrencyRateHistorySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
}