<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\catalog\models\search\UnBuyoutReasonSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class UnBuyoutReasonController
 * @package app\controllers\catalog
 */
class UnBuyoutReasonController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new UnBuyoutReasonSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @return string
     */
    public function actionForeignIndex()
    {
        $query = UnBuyoutReasonMapping::find()
            ->where(['is', 'reason_id', null])
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        return $this->render('foreign-index', [
            'foreignDataProvider' => new ActiveDataProvider($config),
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new UnBuyoutReason();
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Причина успешно добавлена.') : Yii::t('common', 'Причина успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $renderParams['model'] = $model;

        if ($id) {
            $query = UnBuyoutReasonMapping::find()
                ->where(['reason_id' => $id])
                ->orderBy(['id' => SORT_ASC]);
            $config = [
                'query' => $query,
            ];
            $renderParams['foreignDataProvider'] = new ActiveDataProvider($config);
        }

        return $this->render('edit', $renderParams);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Причина успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Причина успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Причина успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionForeignDelete($id)
    {
        $model = $this->getForeignModel($id);
        $reason_id = $model->reason_id;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Внешняя причина успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        if ($reason_id) {
            return $this->redirect(Url::toRoute('un-buyout-reason/edit/' . $reason_id));
        }
        else {
            return $this->redirect(Url::toRoute('un-buyout-reason/foreign-index'));
        }
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionForeignEdit($id = null)
    {
        if ($id) {
            $model = $this->getForeignModel($id);
        } else {
            $model = new UnBuyoutReasonMapping();
            $model->reason_id = Yii::$app->request->get('reason_id');
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Внешняя причина успешно добавлена.') : Yii::t('common', 'Внешняя причина успешно отредактирована.'), 'success');

                if ($model->reason_id) {
                    return $this->redirect(Url::toRoute('un-buyout-reason/edit/' . $model->reason_id));
                }
                else {
                    return $this->redirect(Url::toRoute('un-buyout-reason/foreign-index'));
                }
                
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $renderParams['model'] = $model;
        $renderParams['reasons'] = UnBuyoutReason::find()->collection();
        return $this->render('foreign-edit', $renderParams);
    }


    /**
     * @param integer $id
     * @return UnBuyoutReason
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = UnBuyoutReason::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Причина не найдена.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return UnBuyoutReasonMapping
     * @throws HttpException
     */
    private function getForeignModel($id)
    {
        $model = UnBuyoutReasonMapping::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Внешняя причина не найдена.'));
        }

        return $model;
    }

}
