<?php

namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\search\ExternalSourceSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ExternalSourceController
 * @package app\modules\catalog\controllers
 */
class ExternalSourceController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ExternalSourceSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ExternalSource();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Сторонний источник успешно добавлен.') : Yii::t('common', 'Сторонний источник успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сторонний источник успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return ExternalSource
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = ExternalSource::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Сторонний источник не найден.'));
        }

        return $model;
    }
}