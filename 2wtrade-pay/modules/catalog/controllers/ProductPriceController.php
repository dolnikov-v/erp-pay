<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\Product;
use app\models\ProductPrice;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ProductPriceController
 * @package app\modules\catalog\controllers
 */
class ProductPriceController extends Controller
{
    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $productId = Yii::$app->request->get('product_id');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ProductPrice();
            if ($productId) {
                $model->product_id = $productId;
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Цена успешно добавлена.') : Yii::t('common', 'Цена успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('product/prices/' . $model->product_id));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'productId' => $productId,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public
    function actionDelete($id)
    {
        $model = $this->getModel($id);

        $productId = $model->product_id;
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Цена успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('product/prices/' . $productId));
    }

    /**
     * @param integer $id
     * @return ProductPrice
     * @throws HttpException
     */
    private
    function getModel($id)
    {
        $model = ProductPrice::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Цена не найдена.'));
        }

        return $model;
    }
}
