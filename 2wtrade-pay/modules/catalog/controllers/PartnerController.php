<?php
namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Partner;
use app\models\Country;
use app\models\PartnerCountry;
use app\models\search\PartnerSearch;
use app\models\Timezone;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class PartnerController
 * @package app\controllers\catalog
 */
class PartnerController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-country'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PartnerSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Partner();
            $model->timezone_id = Timezone::find()->select('id')->byTimezoneId('UTC')->scalar();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Партнер успешно добавлен.') : Yii::t('common', 'Партнер успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        $controller = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/';
        return $this->render('edit', [
            'model' => $model,
            'controller' => $controller,
            'countries' => Country::find()->active()->all(),
            'countriesByPartner' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->countries, 'id'),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Партнер успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Ajax установка страны
     * @throws \yii\web\HttpException
     * @todo Требует рефакторинга
     */
    public function actionSetCountry()
    {
        $partnerId = (int)Yii::$app->request->post('model_id');
        $countryId = (int)Yii::$app->request->post('country_id');

        $partner = Partner::find()->where(['id' => $partnerId])->one();
        $country = Country::find()->where(['id' => $countryId])->one();

        if (!$partner || !$country) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране или пользователю.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $partner->link('countries', $country);
                break;
            case 'off':
                $partner->unlink('countries', $country, true);
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDefault($id)
    {
        //Partner::updateAll(['default' => 0]);
        $model = $this->getModel($id);

        $model->default = 1;

        if ($model->save(true, ['default'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Партнер по умолчанию успешно выбран.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Партнер активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Партнер деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Partner
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Partner::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Партнер не найден.'));
        }

        return $model;
    }
}
