<?php

namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\Country;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\modules\catalog\models\search\RequisiteDeliverySearch;
use app\modules\delivery\models\Delivery;
use app\modules\report\models\Invoice;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class RequisiteDeliveryController
 * @package app\modules\catalog\controllers
 */
class RequisiteDeliveryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new RequisiteDeliverySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new RequisiteDelivery();
        }

        $deliveryCollection = $this->getDelivery();
        $deliveryData = $this->buidDeliveryData($deliveryCollection);

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $transaction = yii::$app->db->beginTransaction();

            if ($model->save()) {
                $country_ids = yii::$app->request->post('country_id');
                $delivery_ids = yii::$app->request->post('delivery_id');
                $contract_id = yii::$app->request->post('contract_id');
                $data = [];

                foreach ($country_ids as $key => $country_id) {
                    if (isset($delivery_ids[$key]) && !empty($delivery_ids[$key]) && isset($contract_id[$key]) && !empty($contract_id[$key])) {
                        $data[] = [
                            'RequisiteDeliveryLink' => [
                                'requisite_delivery_id' => $model->id,
                                'delivery_id' => $delivery_ids[$key],
                                'delivery_contract_id' => $contract_id[$key],
                                'country_id' => $country_id,
                                'created_at' => time()
                            ]
                        ];
                    }
                }
                //подчистим линки
                if ($id) {
                    //если они есть
                    if (!empty($model->getRequisiteDeliveryLink()->all())) {
                        $delete = RequisiteDeliveryLink::deleteAll(['requisite_delivery_id' => $id]);

                        if (!$delete) {
                            Yii::$app->notifier->addNotificationsByModel($model);
                            $error = yii::t('common', 'Ошибка обновления прикреплённых КС');
                        }
                    }
                }

                //запишем новые линки
                foreach ($data as $link) {
                    $modelLink = new RequisiteDeliveryLink();
                    if (!$modelLink->load($link) || !$modelLink->save()) {
                        $error = yii::t('common', 'Ошибка добавления КС: {error}', ['error' => json_encode($modelLink->getErrors(), JSON_UNESCAPED_UNICODE)]);
                        break;
                    }
                }

                if (!isset($error)) {
                    Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Реквизит успешно добавлен.') : Yii::t('common', 'Реквизит успешно отредактирован.'), 'success');

                    $transaction->commit();
                    return $this->redirect(Url::toRoute('index'));
                } else {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($error, Notifier::TYPE_DANGER);

                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data = [
            'model' => $model,
            'deliveryList' => json_encode($deliveryData, JSON_UNESCAPED_UNICODE),
            'RequisiteDeliveryLink' => new RequisiteDeliveryLink(),
            'requisiteData' => json_encode($model->requisiteDataCollection(), 256)
        ];

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);
        $searchModel = new RequisiteDeliverySearch();

        $queryParams = Yii::$app->request->queryParams;
        $queryParams['RequisiteDeliverySearch']['id'] = $model->id;

        $dataProvider = $searchModel->search($queryParams);

        $deliveryCollection = $this->getDelivery();
        $deliveryData = $this->buidDeliveryData($deliveryCollection);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'deliveryList' => json_encode($deliveryData, JSON_UNESCAPED_UNICODE),
            'RequisiteDeliveryLink' => new RequisiteDeliveryLink()
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Реквизит успешно отредактирован. успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return RequisiteDelivery
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = RequisiteDelivery::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Сторонний источник не найден.'));
        }

        return $model;
    }

    /**
     * @return Delivery[]
     */
    public function getDelivery()
    {
        return Delivery::find()
            ->joinWith(['deliveryContract', 'country'])
            ->all();
    }

    /**
     * @param $deliveries
     * @return array
     */
    public function buidDeliveryData($deliveries)
    {
        $countryList = [];
        $deliveryList = [];
        $contractList = [];

        foreach ($deliveries as $delivery) {
            //если есть контракты у курьерки - то в путь
            if (count($delivery->deliveryContract) > 0) {
                $countryList[$delivery->country->id] = [
                    'name' => $delivery->country->name,
                    'id' => $delivery->country->id,
                ];

                if (!isset($deliveryList[$delivery->country->id])) {
                    $deliveryList[$delivery->country->id] = [];
                }

                $deliveryList[$delivery->country->id][$delivery->id] = [
                    'name' => $delivery->name,
                    'id' => $delivery->id,
                ];

                if (!isset($contractList[$delivery->country->id])) {
                    $contractList[$delivery->country->id] = [];
                }

                if (!isset($contractList[$delivery->country->id][$delivery->id])) {
                    $contractList[$delivery->country->id][$delivery->id] = [];
                }

                foreach ($delivery->deliveryContract as $contract) {
                    $contractList[$delivery->country->id][$delivery->id][$contract->id] = [
                        'id' => $contract->id,
                        'name' => (!$contract->date_from || !$contract->date_to || empty($contract->date_from) || empty($contract->date_to))
                            ? yii::t('common', 'Бессрочный')
                            : $contract->date_from . ' - ' . $contract->date_to
                    ];
                }
            }
        }

        return [
            'country' => $countryList,
            'delivery' => $deliveryList,
            'contract' => $contractList,
        ];
    }

    /**
     * @return string
     */
    public function actionGetRequisiteByInvoice()
    {
        $delivery_id = yii::$app->request->post('delivery_id');
        $country_id = yii::$app->request->post('country_id');
        $from = yii::$app->request->post('from');
        $to = yii::$app->request->post('to');

        $delivery = Delivery::find()->where(['id' => $delivery_id])->one();
        $country = Country::find()->where(['id' => $country_id])->one();

        $requisitesContractsQuery = RequisiteDeliveryLink::find()->where([
            'delivery_id' => $delivery_id,
            'country_id' => $country_id
        ]);

        if (!$requisitesContractsQuery->exists()) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Контракты для реквизитов у КС {delivery} в стране {country} не найдены', [
                    'delivery' => $delivery->name,
                    'country' => $country->name
                ])
            ];
        } else {
            $requisitesQuery = RequisiteDelivery::find()->where([
                'id' => ArrayHelper::getColumn($requisitesContractsQuery->all(), 'requisite_delivery_id')
            ]);

            if (!$requisitesQuery->exists()) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Реквизиты не найдены')
                ];
            } else {
                $requisites = $requisitesQuery->asArray()->all();


                $activeContract = $delivery->getActiveContractByDelivery($delivery->id, $from, $to);

                $result = [
                    'success' => true,
                    'message' => $requisites,
                    'default_requisite_delivery_id' => ($activeContract) ? $activeContract->requisite_delivery_id : null
                ];
            }
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

}