<?php

namespace app\modules\catalog\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\components\filters\AjaxFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use app\components\web\Controller;
use app\models\Product;
use app\models\ProductCategory;


/**
 * Class ProductCategoryController
 * @package app\controllers\catalog
 */
class ProductCategoryController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-product',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $sort['id'] = SORT_ASC;
        $query = ProductCategory::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];
        $dataProvider = new ActiveDataProvider($config);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ProductCategory();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Категория успешно добавлена.') : Yii::t('common', 'Категория успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        $controller = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/';

        $data['model'] = $model;

        //return $this->render('edit', $data);
        return $this->render('edit', [
            'model' => $model,
            'controller' => $controller,
            'products' => Product::find()->all(),
            'productsCategory' => $model->isNewRecord ? [] : ArrayHelper::getColumn($model->products, 'id'),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Категория успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return ProductCategory
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = ProductCategory::findOne($id);
        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Категория не найдена.'));
        }

        return $model;
    }

    /**
     * Ajax установка страны
     * @throws \yii\web\HttpException
     */
    public function actionSetProduct()
    {
        $categoryId = (int)Yii::$app->request->post('model_id');
        $productId = (int)Yii::$app->request->post('product_id');

        $category = ProductCategory::findOne($categoryId);
        $product = Product::findOne($productId);

        if (!$category || !$product) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей категории или продукту.'));
        }

        $value = Yii::$app->request->post('value');
        try {

        switch ($value) {
            case 'on':
                $category->link('products', $product);
                break;
            case 'off':
                $category->unlink('products', $product, true);
                break;
        }
        } catch (\Exception $e) {
            Yii::$app->notifier->addNotification(print_r($e->getMessage()));
        }
        return [
            'success' => true,
        ];
    }
}
