<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\modules\catalog\models\search\ExternalSourceRoleSearch;
use Yii;

/**
 * Class ExternalSourceRolesController
 * @package app\modules\catalog\controllers
 */
class ExternalSourceRoleController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ExternalSourceRoleSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }
}