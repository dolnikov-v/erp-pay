<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\ProductPhoto;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class ProductPhotoController
 * @package app\modules\catalog\controllers
 */
class ProductPhotoController extends Controller
{
    /**
     * @param int|null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function actionEdit(?int $id = null)
    {
        $productId = Yii::$app->request->get('product_id');
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ProductPhoto();
            if ($productId) {
                $model->product_id = $productId;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->image = $model->upload()) {
                $model->imageFile = null;
            } else {
                $model->image = $model->getOldAttribute('image');
            }
            if ($model->save()) {
                Yii::$app->notifier->addNotification($model->isNewRecord ? Yii::t('common', 'Фото успешно добавлено.') : Yii::t('common', 'Фото успешно сохранено.'), 'success');
                return $this->redirect(Url::toRoute('product/photo/' . $model->product_id));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $model = $this->getModel($id);
        $productId = $model->product_id;
        if (is_file($model::getPathFile($model->image))) {
            unlink($model::getPathFile($model->image));
        }
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно отредактирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('product/photo/' . $productId));
    }

    /**
     * @param int $id
     * @return ProductPhoto
     * @throws HttpException
     */
    private function getModel(int $id): ProductPhoto
    {
        $model = ProductPhoto::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Изображение не найдено.'));
        }

        return $model;
    }
}
