<?php
namespace app\modules\catalog\controllers;

use yii\helpers\Url;
use app\components\web\Controller;
use app\models\Currency;
use app\models\search\CurrencySearch;
use Yii;
use yii\web\HttpException;

/**
 * Class CurrencyController
 * @package app\modules\catalog\controllers
 */
class CurrencyController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CurrencySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Currency();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Валюта успешно добавлена.') : Yii::t('common', 'Валюта успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));

            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \app\models\Currency
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Currency::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Валюта не найдена.'));
        }

        return $model;
    }
}
