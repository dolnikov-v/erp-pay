<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\Notification;
use app\models\search\NotificationSearch;
use HttpException;
use Yii;
use yii\helpers\Url;

/**
 * Class NotificationController
 * @package app\modules\catalog\controllers
 */
class NotificationController extends Controller
{
    /**
     * @return mixed
     */
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new NotificationSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);
        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Оповещение успешно активировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);
        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Оповещение успешно деактивировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Notification::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Оповещение не найдено.'));
        }

        return $model;
    }

}
