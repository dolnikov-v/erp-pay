<?php

namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\models\Country;
use app\modules\catalog\models\Autotrash;
use app\modules\catalog\models\search\AutotrashSearch;
use app\components\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;
use yii;
use yii\web\NotFoundHttpException;

/**
 * Class AutoTrashController
 * Контроллер справочника "стоповых" фраз для заказов
 * Авто треш NPAY-724
 *
 * @package app\modules\catalog\controllers
 */
class AutotrashController extends Controller
{
    const FILTER_NOT_FOUND = 'Фильтр не найден';
    const IS_ACTIVE = 1;
    const IS_NOT_ACTIVE = 0;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'autotrash-switch',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Autotrash();
        $modelSearch = new AutotrashSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'modelSearch' => $modelSearch,
            'stateAutotrash' => self::getStateAutotrash(),
            'fieldsCollection' => Autotrash::getFieldsCollection(),
            'filtersCollection' => Autotrash::getFiltersCollection()
        ]);
    }

    /**
     * Метод определяет - является ли заказ трешовым или нет
     * @param $order object
     * @return bool  (true changes status to 33)
     */
    public static function classifyOrder($order)
    {
        $filters = Autotrash::find()
            ->where([
                'is_active' => self::IS_ACTIVE
            ])
            ->andWhere([
                'or',
                ['country_id' => $order->country_id],
                ['country_id' => null]      // старые правила пусть видят все и правят кому надо
            ])
            ->asArray()
            ->all();

        //перебор свойств заказа
        foreach ($order as $field => $value) {
            //перебор всех фильтров автотреша
            foreach ($filters as $filter) {
                //если есть фильтр для данного свойства
                if ($filter['field'] == $field) {
                    switch ($filter['filter']) {
                        case Autotrash::FILTER_REGEXP :
                            if (preg_match($filter['template'], trim($value))) {
                                return true;
                            }
                            break;
                        case Autotrash::FILTER_PHONE :
                            //почистить номер, оставить только цифры
                            $garbage = [' ' => '', '+' => '', '-' => '', '(' => '', ')' => ''];
                            if (preg_match("#^" . strtr($filter['template'], $garbage) . "$#", strtr($value, $garbage))) {
                                return true;
                            }
                            break;
                        case Autotrash::FILTER_MIN_LENGTH :
                            if ((int)$filter['template'] >= mb_strlen(trim($value), 'utf-8')) {
                                return true;
                            }
                            break;
                        case Autotrash::FILTER_MAX_LENGTH :
                            if ((int)$filter['template'] <= mb_strlen(trim($value), 'utf-8')) {
                                return true;
                            }
                            break;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Определение состояния автотреша по стране
     * @param $countryId integer
     * @return bool
     */
    public static function getStateAutotrash($countryId = null)
    {
        if (!$countryId) {
            $countryId = Yii::$app->user->country->id ?? 0;
        }

        if ($countryId) {
            $country = Country::findOne($countryId);
            return $country->auto_trash;
        }
        return false;
    }

    /**
     * @return array
     */
    public function actionAutotrashSwitch()
    {
        $country = Country::findOne(Yii::$app->user->country->id);
        $country->auto_trash = yii::$app->request->post('stateAutotrash') ?? 0;

        if ($country->update(true, ['auto_trash'])) {
            $result = [
                'success' => true,
                'message' => Yii::t('common', "Статуса автотреша изменён.")
            ];
        } else {
            $result = [
                'success' => false,
                'message' => Yii::t('common', "Ошибка смены статуса автотреша.")
            ];
        }

        return $result;
    }

    /**
     * @param integer $id
     * @return Autotrash
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Autotrash::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', self::FILTER_NOT_FOUND));
        }

        return $model;
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Autotrash();
            $model->country_id = Yii::$app->user->country->id;
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            if ($post['Autotrash']['filter'] == Autotrash::FILTER_REGEXP) {
                if (@preg_match($post['Autotrash']['template'], $post['Autotrash']['template']) === false) {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Использован некорректный паттерн регулярного выражения.'));
                    return $this->redirect(Url::toRoute('index'));
                }
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Фильтр успешно добавлен.') : Yii::t('common', 'Фильтр успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data = [
            'model' => $model,
            'modelSearch' => new AutoTrashSearch(),
            'fieldsCollection' => Autotrash::getFieldsCollection(),
            'filtersCollection' => Autotrash::getFiltersCollection()
        ];

        return $this->render('edit', $data);

    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Фильтр удален.'), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param string $id
     * @return Autotrash
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Autotrash::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Фильтр не найден.'));
        }
    }
}