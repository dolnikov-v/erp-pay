<?php

namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductPhoto;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use app\models\search\ProductLandingSearch;
use app\models\search\ProductSearch;
use app\models\Source;
use app\models\SourceProduct;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class ProductController
 * @package app\controllers\catalog
 */
class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ProductSearch();

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);

        $sources = Source::find()->collection();
        $categories = ProductCategory::find()->collection();
        $data['model'] = $model;
        $data['sources'] = $sources;
        $data['categories'] = $categories;

        return $this->render('view', $data);
    }


    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id)
    {
        $model = $this->getModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $categoryList = Yii::$app->request->post('Product')['categoryList'];

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->image = $model->upload()) {
                $model->imageFile = null;
            } else {
                $model->image = $model->getOldAttribute('image');
            }

            if ($model->save()) {
                if ($model->sources) {
                    foreach ($model->sources as $source) {
                        $sourceProduct = new SourceProduct();
                        $sourceProduct->source_id = $source;
                        $sourceProduct->product_id = $model->id;
                        $sourceProduct->save();
                    }
                }
                $model->unlinkAll('categories', true);
                foreach ($categoryList as $categoryId) {
                    $category = ProductCategory::findOne($categoryId);
                    $model->link('categories', $category);
                }
                Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $sources = Source::find()->collection();
        $categories = ProductCategory::find()->collection();
        $data['model'] = $model;
        $data['sources'] = $sources;
        $data['categories'] = $categories;

        return $this->render('edit', $data);
    }

    /**
     * @param int $id
     * @return string
     * @throws HttpException
     */
    public function actionPrices(int $id)
    {
        $model = $this->getModel($id);

        if ($model) {
            $query = ProductPrice::find()
                ->where([
                    'product_id' => $id,
                    'country_id' => array_keys(Yii::$app->user->identity->getAllowCountries())
                ])
                ->orderBy(['id' => SORT_ASC]);

            $config = [
                'query' => $query,
            ];
            $data['priceDataProvider'] = new ActiveDataProvider($config);
            $data['model'] = $model;
        }

        return $this->render('_prices', $data);
    }

    /**
     * @param int $id
     * @return string
     * @throws HttpException
     */
    public function actionTranslate(int $id)
    {
        $model = $this->getModel($id);
        $data = [];
        if ($model) {
            $query = ProductTranslation::find()
                ->where([
                    'product_id' => $id,
                    'country_id' => array_keys(Yii::$app->user->identity->getAllowCountries())
                ])
                ->orderBy(['id' => SORT_ASC]);

            $config = [
                'query' => $query,
            ];
            $data['translateDataProvider'] = new ActiveDataProvider($config);
            $data['model'] = $model;
        }

        return $this->render('_translate', $data);
    }

    /**
     * @param int $id
     * @return string
     * @throws HttpException
     */
    public function actionPhoto(int $id)
    {
        $model = $this->getModel($id);
        $data = [];
        if ($model) {
            $query = ProductPhoto::find()
                ->where([
                    'product_id' => $id,
                    'country_id' => array_keys(Yii::$app->user->identity->getAllowCountries())
                ])
                ->orderBy(['id' => SORT_ASC]);

            $config = [
                'query' => $query,
            ];
            $data['photoDataProvider'] = new ActiveDataProvider($config);
            $data['model'] = $model;
        }

        return $this->render('_photo', $data);
    }


    /**
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionAdd()
    {
        $model = new Product();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            $categoryList = Yii::$app->request->post('Product')['categoryList'];

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->image = $model->upload()) {
                $model->imageFile = null;
            } else {
                $model->image = null;
            }

            if ($model->save()) {
                if ($model->sources) {
                    foreach ($model->sources as $source) {
                        $sourceProduct = new SourceProduct();
                        $sourceProduct->source_id = $source;
                        $sourceProduct->product_id = $model->id;
                        $sourceProduct->save();
                    }
                }
                $model->unlinkAll('categories', true);
                foreach ($categoryList as $categoryId) {
                    $category = ProductCategory::findOne($categoryId);
                    $model->link('categories', $category);
                }
                Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно добавлен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $sources = Source::find()->collection();
        $categories = ProductCategory::find()->collection();
        $data['model'] = $model;
        $data['sources'] = $sources;
        $data['categories'] = $categories;

        return $this->render('edit', $data);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionLandings($id)
    {
        $model = $this->getModel($id);

        $modelSearch = new ProductLandingSearch();

        $queryParams['ProductLandingSearch']['product_id'] = $model->id;

        $dataProvider = $modelSearch->search($queryParams);

        return $this->render('landings', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return Product
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Product::findOne($id);
        $model->sources = $model->getSourcesByProduct();
//        $model->categories = $model->getCategoriesByProduct();
        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }

        return $model;
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeleteImage($id)
    {
        $model = $this->getModel($id);
        if (is_file(Product::getPathFile($model->image))) {
            unlink(Product::getPathFile($model->image));
        }
        $model->image = null;
        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар успешно отредактирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('product/edit/' . $id));
    }

}
