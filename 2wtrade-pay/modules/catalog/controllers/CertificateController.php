<?php
namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Certificate;
use app\models\Country;
use app\models\search\CertificateFileSearch;
use app\models\search\CertificateSearch;
use app\models\CertificateFile;
use app\models\Product;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\web\Request;

/**
 * Class CountryController
 * @package app\controllers\catalog
 */
class CertificateController extends Controller
{
    const UPLOADFILES_SUCCESS = 'Файлы сертификата успешно загружены.';
    const UPLOADFILES_FAIL = 'Файл сертификата не соответствуют требованиям.';
    const ADD_CERTIFICATE_SUCCESS = 'Сертификат успешно добавлен.';
    const ADD_CERTIFICATE_FAIL = 'Произошла ошибка при создании сертификата.';
    const UPDATE_CERTIFICATE_SUCCESS = 'Сертификат успешно отредактирован.';
    const UPDATE_CERTIFICATE_FAIL = 'Произошла ошибка при обновлении сертификата.';
    const FILE_NOT_FOUND = 'Файл сертификата не найден.';
    const CERTIFICATE_NOT_FOUND = 'Сертификат не найден';
    const DELETE_FILE_FAIL = 'Ошибка удаления файла сертификата';
    const DELETE_FILE_SUCCESS = 'Файл сертификата успешно удалён.';
    const DELETE_CERTIFICATE_SUCCESS = 'Сертфикат удалён';
    const DELETE_CERTIFICATE_FAIL = 'Ошибка удаления сертификата';

    /**
     * @return bool
     */
    private $allowToDeleteFiles = false;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Certificate();
        $modelSearch = new CertificateSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        $productsList = Product::find()->orderBy(['name' => SORT_ASC])->collection();
        $countryList = Country::find()->bySystemUserCountries()->collection();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'model' => $model,
            'productsList' => $productsList,
            'countryList' => $countryList
        ]);
    }

    /**
     * @param integer $id
     * @return Certificate
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Certificate::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', self::CERTIFICATE_NOT_FOUND));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return CertificateFile
     * @throws HttpException
     */
    private function getFileModel($id)
    {
        $model = CertificateFile::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', self::FILE_NOT_FOUND));
        }

        return $model;
    }


    /**
     * @param integer $id
     * @return CertificateFileSearch
     * @throws HttpException
     */
    private function getFilesModel($id)
    {
        $modelFilesSearch = new CertificateFileSearch(['product_certificate_id' => $id]);

        if (!$modelFilesSearch) {
            throw new HttpException(404, Yii::t('common', self::CERTIFICATE_NOT_FOUND));
        }

        return $modelFilesSearch;
    }

    /**
     * @return string
     */
    public static function formName()
    {
        return get_called_class();
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Certificate();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $this->uploadFiles($model);

            $postdata = Yii::$app->request->post(ucfirst(Yii::$app->controller->id));

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $certificate = $isNewRecord ? new Certificate : $model::findOne(['id' => $id]);
                $certificate->name = isset($postdata['name']) ? $postdata['name'] : Yii::t('common', 'Сертификат не нужен');
                $certificate->description = $postdata['description'];
                $certificate->product_id = $postdata['product_id'];
                $certificate->country_id = $postdata['country_id'];
                $certificate->is_partner = isset($postdata['is_partner']) ? 1 : 0;
                $certificate->certificate_not_needed = isset($postdata['certificate_not_needed']) ? 1 : 0;

                if ($certificate->save()) {
                    if (!empty($model->uploadedFilesNames)) {
                        foreach ($model->uploadedFilesNames as $k => $file) {
                            $files = new CertificateFile;
                            $files->product_certificate_id = $certificate->id;
                            $files->origfilename = $file['origfilename'];
                            $files->unicfilename = $file['unicfilename'];
                            $files->save();
                        }
                    }

                    Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', self::ADD_CERTIFICATE_SUCCESS) : Yii::t('common', self::UPDATE_CERTIFICATE_SUCCESS), 'success');

                    $transaction->commit();

                    return $this->redirect(Url::toRoute('index'));
                } else {
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

            } catch (Exception $e) {
                $transaction->rollback();

                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', self::UPDATE_CERTIFICATE_SUCCESS) : Yii::t('common', self::UPDATE_CERTIFICATE_FAIL), 'danger');

                return $this->redirect(Url::toRoute('edit'));
            }
        }

        $productsList = Product::find()->orderBy(['name' => SORT_ASC])->collection();
        $countryList = Country::find()->bySystemUserCountries()->collection();

        //Create certificate template
        if ($model->isNewRecord) {
            return $this->render('edit', [
                'model' => $model,
                'productsList' => $productsList,
                'countryList' => $countryList,
                'modelFilesSearch' => null,
                'dataProviderFiles' => null
            ]);
        } //update certificate template
        else {
            $modelFiles = $this->getFilesModel($id);
            $queryParams['CertificateSearch']['id'] = $model->id;
            $queryParamsFiles['CertificateFileSearch']['product_certificate_id'] = $modelFiles->product_certificate_id;

            $modelFilesSearch = new CertificateFileSearch;
            $dataProviderFiles = $modelFilesSearch->search($queryParamsFiles);

            return $this->render('edit', [
                'model' => $model,
                'productsList' => $productsList,
                'countryList' => $countryList,
                'modelFilesSearch' => $modelFilesSearch,
                'dataProviderFiles' => $dataProviderFiles
            ]);
        }
    }

    /**
     * @param object
     * @return string|boolean
     */
    public function uploadFiles($model)
    {
        //TODO неадекватное формирование массива $_FILES, в каждый элемент добавляется под индекс с использование класса UploadedFile
        //по факту поле называется file[] - но массив $_FILES формируется по названию контроллера Certificate / ucfirst(Yii::$app->controller->id)
        $model->files = UploadedFile::getInstancesByName(ucfirst(Yii::$app->controller->id));

        if ($model->upload()) {
            if (!empty($model->uploadedFilesNames)) {
                Yii::$app->notifier->addNotification(Yii::t('common', self::UPLOADFILES_SUCCESS), 'success');
            }
            return $this->redirect(Url::toRoute('index'));
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', self::UPLOADFILES_FAIL), 'danger');
        }
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);
        $modelSearch = new CertificateSearch();
        $queryParams['CertificateSearch']['id'] = $model->id;

        $modelFiles = $this->getFilesModel($id);
        $modelFilesSearch = new CertificateFileSearch();
        $queryParamsFiles['CertificateFileSearch']['product_certificate_id'] = $modelFiles->product_certificate_id;

        $dataProvider = $modelSearch->search($queryParams);
        $dataProviderFiles = $modelFilesSearch->search($queryParamsFiles);

        return $this->render('view', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'modelFilesSearch' => $modelFilesSearch,
            'dataProvider' => $dataProvider,
            'dataProviderFiles' => $dataProviderFiles
        ]);
    }

    /**
     * @param $id
     * @return void
     * @throws HttpException
     */
    public function actionDownload($id)
    {
        $file = $this->loadCertificateFileModel($id);

        if (file_exists(yii::getAlias('@certificatesFolder') . DIRECTORY_SEPARATOR . $file->unicfilename)) {
            Yii::$app->response->sendFile(yii::getAlias('@certificatesFolder') . DIRECTORY_SEPARATOR . $file->unicfilename);
        } else {
            throw new HttpException(404, Yii::t('common', self::FILE_NOT_FOUND));
        }
    }

    /**
     * @param $id
     * @return \yii\db\ActiveQuery
     * @throws HttpException
     */
    public function loadCertificateFileModel($id)
    {
        $modelFile = new CertificateFile;
        $file = $modelFile::findOne($id);

        if ($file === null)
            throw new HttpException(404, Yii::t('common', self::FILE_NOT_FOUND));

        return $file;
    }

    /**
     * @param integer
     * @return \yii\web\Response
     * @trhows NotFoundHttpExeption
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $modelFiles = new CertificateFileSearch;
        $listFiles = $modelFiles::findAll(['product_certificate_id' => $id]);
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model::deleteAll(['id' => $id]);
            $modelFiles::deleteAll(['product_certificate_id' => $id]);

            $transaction->commit();

            Yii::$app->notifier->addNotification(Yii::t('common', self::DELETE_CERTIFICATE_SUCCESS), 'success');
            $this->allowToDeleteFiles = true;
        } catch (Exception $e) {
            $transaction->rollback();

            Yii::$app->notifier->addNotification(Yii::t('common', self::DELETE_CERTIFICATE_FAIL), 'danger');
            $this->allowToDeleteFiles = false;
        }

        //удаление из бд прошло успешно, удаляем файлы
        if ($this->allowToDeleteFiles) {
            foreach ($listFiles as $k => $file) {
                unlink(yii::getAlias('@certificatesFolder') . DIRECTORY_SEPARATOR . $file->unicfilename);
                Yii::$app->notifier->addNotification(Yii::t('common', self::DELETE_FILE_SUCCESS), 'success');
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param string
     * @return \yii\web\Response
     * @trhows NotFoundHttpExeption
     */
    public function actionDeleteFile($id)
    {
        $modelFile = $this->getFileModel($id);
        $modelFilesSearch = new CertificateFileSearch;
        $file = $modelFilesSearch::findOne($id);

        if ($modelFile->delete()) {
            unlink(yii::getAlias('@certificatesFolder') . DIRECTORY_SEPARATOR . $file->unicfilename);
            Yii::$app->notifier->addNotification(Yii::t('common', self::DELETE_FILE_SUCCESS), 'success');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', self::DELETE_FILE_FAIL), 'danger');
            return $this->redirect(Yii::$app->request->referrer);
        }

    }

    /**
     * Проверка наличия товара в стране (через склад)
     * @param integer
     * @param integer
     * @return integer
     */
    public function checkProductInCountry($product_id, $country_id)
    {
        $model = new Certificate;
        return $model->checkProductInCountry($product_id, $country_id);
    }
}