<?php
namespace app\modules\catalog\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\models\CountryOrderWorkflow;
use app\models\search\CountrySearch;
use app\modules\order\models\OrderWorkflow;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;

/**
 * Class CountryController
 * @package app\controllers\catalog
 */
class CountryController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-workflow',
                    'set-sms-workflow',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CountrySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Country();
        }

        if ($model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Страна успешно добавлена.') : Yii::t('common', 'Страна успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Страна успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Страна успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Страна успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionSetWorkflow()
    {
        $countryId = (int)Yii::$app->request->post('country_id');
        $workflowId = (int)Yii::$app->request->post('workflow_id');

        /** @var Country $country */
        $country = Country::findOne($countryId);

        /** @var OrderWorkflow $workflow */
        $workflow = OrderWorkflow::findOne($workflowId);

        if (!$country || !$workflow) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране или workflow.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $exists = CountryOrderWorkflow::find()
                    ->byCountryId($country->id)
                    ->byWorkflowId($workflow->id)
                    ->exists();

                if (!$exists) {
                    $country->link('workflows', $workflow);
                }
                break;
            case 'off':
                $country->unlink('workflows', $workflow, true);
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param integer $id
     * @return Country
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Country::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страна не найдена.'));
        }

        return $model;
    }
}
