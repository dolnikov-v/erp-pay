<?php

namespace app\modules\catalog\controllers;

use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use app\modules\catalog\models\OperatorsPenalty;
use app\modules\catalog\models\search\OperatorsPenaltySearch;
use app\components\web\Controller;


/**
 * Class OperatorsPenaltyController
 * @package app\modules\catalog\controllers
 */
class OperatorsPenaltyController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new OperatorsPenalty();
        $modelSearch = new OperatorsPenaltySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param integer $id
     * @return OperatorsPenalty $model
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = OperatorsPenalty::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Штрафы операторов не найдены'));
        }

        return $model;
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new OperatorsPenalty();
            $model->created_at = time();
        }
        $model->country_id = Yii::$app->user->country->id;
        $model->updated_at = time();

        $callcenterList = $model->getCallCenterCollection();
        if (empty($callcenterList)) {
            throw new HttpException(404, Yii::t('common', 'Не найдено ни одного колл-центра в стране'));
        }

        $penaltyList = OperatorsPenalty::getPenaltyTypeCollection();

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Штраф успешно добавлен.') : Yii::t('common', 'Штраф успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data = [
            'model' => $model,
            'modelSearch' => new OperatorsPenaltySearch(),
            'callcenterList' => $callcenterList,
            'penaltyList' => $penaltyList,
        ];

        return $this->render('edit', $data);

    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Штраф удален.'), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param string $id
     * @return OperatorsPenalty $model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = OperatorsPenalty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Штраф не найден.'));
        }
    }

}