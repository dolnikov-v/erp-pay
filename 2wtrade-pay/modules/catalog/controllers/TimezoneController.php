<?php
namespace app\modules\catalog\controllers;

use app\components\web\Controller;
use app\models\search\TimezoneSearch;
use app\modules\media\components\Image;
use app\modules\media\models\Uploader;
use Yii;

/**
 * Class TimezoneController
 * @package app\controllers\catalog
 */
class TimezoneController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new TimezoneSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
