<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\models\SmsPollQuestions $model */
/** @var \app\models\search\SmsPollQuestionsSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Просмотр вопроса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список вопросов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сведения о вопросе'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'content' => function ($model) {
                    return $model->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'question_text',
                'content' => function ($model) {
                    return $model->question_text;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return Html::decode($model->country_id);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => '',
]) ?>

<?= ButtonLink::widget([
    'label' => Yii::t('common', 'Вернуться назад'),
    'url' => Url::toRoute('index'),
    'style' => ButtonLink::STYLE_DEFAULT,
    'size' => ButtonLink::SIZE_SMALL,
]) . LinkPager::widget(['pagination' => $dataProvider->getPagination()]); ?>
