<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\search\CertificateSearch $modelSearch */
/** @var array $productsList */
/** @var array $countryList */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-8">
        <?= $form->field($modelSearch, 'question_text')->textInput() ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'country_id')->select2List(
            $countryList, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
