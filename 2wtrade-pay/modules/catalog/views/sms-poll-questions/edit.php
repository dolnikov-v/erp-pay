<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\SmsPollQuestions $model */
/** @var \app\models\Country $countryList */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление вопроса') : Yii::t('common', 'Редактирование вопроса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'СМС опрос'), 'url' => Url::toRoute('/catalog/smspollquestions/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление вопроса') : Yii::t('common', 'Редактирование вопроса')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные вопроса'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'countryList' => $countryList
    ])
]) ?>



