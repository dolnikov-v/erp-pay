<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\modules\catalog\assets\ATimerAsset;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var $modelSearch \app\modules\catalog\models\search\SmsPollQuestionsSearch */
/** @var $countryList array */

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'СМС опрос');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'СМС опрос')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'countryList' => $countryList
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица вопросов'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->is_active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'question_text',
                'headerOptions' => ['class' => 'width-600'],
                'content' => function ($data) {
                    return Yii::t('common', $data->question_text);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_name',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($data) {
                    return Yii::t('common', $data->country->name);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['class' => 'width-150'],
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.smspollquestions.index');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.smspollquestions.index');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.smspollquestions.index');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.smspollquestions.index') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить вопрос'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>