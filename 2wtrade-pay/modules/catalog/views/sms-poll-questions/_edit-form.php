<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

/** @var \app\models\SmsPollQuestions $model */
/** @var array $countryList */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */

ModalConfirmDeleteAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'question_text')->textarea() ?>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'country_id')->select2List(
            $countryList, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить вопрос') : Yii::t('common', 'Сохранить вопрос')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?= ModalConfirmDelete::widget() ?>

