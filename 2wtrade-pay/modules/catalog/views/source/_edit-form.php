<?php

use app\components\widgets\ActiveForm;
use app\modules\order\models\Order;
use yii\helpers\Url;
use app\models\Source;

/** @var app\models\Source $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-5">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-5">
        <?= $form->field($model, 'unique_system_name')
            ->select2List(array_combine(Source::systemSourceList(), Source::systemSourceList()), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'default_call_center_type')->select2List(Order::getTypesCallCenter()) ?>
    </div>
</div>
<div class="row form-group">
    <div class="col-lg-4">
        <?= $form->field($model, 'use_pseudo_approve')->checkboxCustom([
            'checked' => $model->use_pseudo_approve,
            'value' => 1,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить источник') : Yii::t('common', 'Сохранить источник')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
