<?php
use app\modules\catalog\widgets\Product;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\catalog\widgets\Country;

/** @var \yii\web\View $this */
/** @var \app\models\Source $model */
/** @var string $controller */
/** @var app\models\Country[] $countries */
/** @var app\models\Product[] $products */
/** @var array $countriesBySource */
/** @var array $productsBySource */

$this->title = $model->isNewRecord ?  Yii::t('common', 'Добавление источник') : Yii::t('common', 'Редактирование источника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Источники'), 'url' => Url::toRoute('/catalog/source/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление источника') : Yii::t('common', 'Редактирование источника')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Источник'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>


<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        //'title' => Yii::t('common', 'Список стран'),
        'collapse' => true,
        'fullScreenBody' => true,
        'nav' => new Nav([
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Список стран'),
                    'content' => Country::widget([
                        'model' => $model,
                        'controller' => $controller,
                        'countryList' => $countries,
                        'countriesByModel' => $countriesBySource,
                    ]),
                ],
                [
                    'label' => Yii::t('common', 'Список товаров'),
                    'content' => Product::widget([
                        'model' => $model,
                        'controller' => $controller,
                        'productList' => $products,
                        'productsByModel' => $productsBySource,
                    ]),
                ],

                ],
        ]),
    ]) ?>
<?php endif; ?>
