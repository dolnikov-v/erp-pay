<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\catalog\assets\ATimerAsset;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\search\CertificateSearch $modelSearch */
/** @var array $productsList */
/** @var array $countryList */

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список сертификатов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сертификаты')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'productsList' => $productsList,
        'countryList' => $countryList
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с сертификатами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . (!$model->is_active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($data) {
                    return Yii::t('common', $data->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product_id',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($data) {
                    return Yii::t('common', $data->product);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($data) {
                    return Yii::t('common', $data->country);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'fileCount',
                'headerOptions' => ['class' => 'width-150 text-cente'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    return Label::widget([
                        'label' => $data->fileCount > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $data->fileCount > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_partner',
                'headerOptions' => ['class' => 'width-250 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    return Label::widget([
                        'label' => $data->is_partner ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $data->is_partner ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'availability',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    /** @var $data \app\models\Certificate  */
                    return Label::widget([
                        'label' => $data->checkProductInCountry($data->product_id, $data->country_id) > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $data->checkProductInCountry($data->product_id, $data->country_id) > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['class' => 'width-150'],
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.certificate.view');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.certificate.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.certificate.delete');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.certificate.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить сертификат'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>