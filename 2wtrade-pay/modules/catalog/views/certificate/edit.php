<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\Certificate $model */
/** @var array $countryList */
/** @var array $productsList */
/** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление сертификата') : Yii::t('common', 'Редактирование сертификата');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сертификаты'), 'url' => Url::toRoute('/catalog/certificate/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление сертификата') : Yii::t('common', 'Редактирование сертификата')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные сертификата'),
    'alert' => Yii::t('common', 'Допустимые типы файлов сертификата: {format}', ['format' => 'JPEG, PNG, PDF']),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'productsList' => $productsList,
        'countryList' => $countryList,
        'modelFilesSearch' => $modelFilesSearch,
        'dataProviderFiles' => $dataProviderFiles
    ])
]) ?>


