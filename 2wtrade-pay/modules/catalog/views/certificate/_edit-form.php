<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\modules\catalog\assets\CertificateNotNeededAsset;

/** @var \app\models\Certificate $model */
/** @var array $productsList */
/** @var array $countryList */
/** @var array $productsList */
/** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */

ModalConfirmDeleteAsset::register($this);
CertificateNotNeededAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id]), 'options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'hidden')->hiddenInput(['id' => 'hidden', 'value' => yii::t('common', 'Сертификат не нужен')])->label(false); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'description')->textarea() ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'product_id')->select2List(
            $productsList, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>

    <div class="col-lg-6">
        <?= $form->field($model, 'country_id')->select2List(
            $countryList, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'files[]')->fileInput([
            'id' => 'CertificateFiles',
            'multiple' => true,
            'accept' => 'image/jpeg, image/png, application/pdf',
            'disabled' => $model->certificate_not_needed ? true : false
        ]) ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($model, 'is_partner')->checkboxCustom([
            'value' => 1,
            'id' => 'is_partner',
            'checked' => $model->is_partner,
            'disabled' => $model->certificate_not_needed ? true : false
        ]) ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($model, 'certificate_not_needed')->checkboxCustom([
            'value' => 1,
            'id' => 'certificate_not_needed',
            'checked' => $model->certificate_not_needed,
        ]) ?>
    </div>
</div>

<?php if (!$model->isNewRecord): ?>

    <div class="row">
        <div class="col-lg-12">

            <?= Panel::widget([
                'title' => '',
                'actions' => DataProvider::renderSummary($dataProviderFiles),
                'withBody' => false,
                'content' => GridView::widget([
                    'dataProvider' => $dataProviderFiles,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'width-150 text-cente'],
                            'content' => function ($modelFilesSearch) {
                                /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                                return $modelFilesSearch->id;
                            },
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'origfilename',
                            'content' => function ($modelFilesSearch) {
                                /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                                return HTML::a($modelFilesSearch->origfilename, Url::toRoute('certificate/download/' . $modelFilesSearch->id), ['target' => '_blank']);
                            },
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'unicfilename',
                            'content' => function ($modelFilesSearch) {
                                /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                                return $modelFilesSearch->unicfilename;
                            },
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'created_at',
                            'headerOptions' => ['class' => 'width-150 text-center'],
                            'class' => DateColumn::className(),
                            'enableSorting' => false,
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'headerOptions' => ['class' => 'width-150'],
                            'items' => [
                                [
                                    'label' => Yii::t('common', 'Удалить'),
                                    'url' => function () {
                                        return '#';
                                    },
                                    'style' => 'confirm-delete-link',
                                    'attributes' => function ($modelFilesSearch) {
                                        /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                                        return [
                                            'data-href' => Url::toRoute(['delete-file', 'id' => $modelFilesSearch->id]),
                                        ];
                                    },
                                    'can' => function () {
                                        return Yii::$app->user->can('catalog.certificate.deletefile');
                                    },
                                ],
                            ]
                        ],
                    ],
                ]),
                'footer' => '',
            ]) ?>
        </div>
    </div>

<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить сертификат') : Yii::t('common', 'Сохранить сертификат')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?= ModalConfirmDelete::widget() ?>

