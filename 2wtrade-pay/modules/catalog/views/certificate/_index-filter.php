<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\search\CertificateSearch $modelSearch */
/** @var array $productsList */
/** @var array $countryList */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'product_id')->select2List(
            $productsList, [
                'prompt' => '—',
                'id' => 'product_id_dropdown',
                'name' => Html::getInputName($modelSearch, 'product_id'),
                'length' => false,
            ]
        ) ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'country_id')->select2List(
            $countryList, [
                'prompt' => '—',
                'id' => 'country_id_dropdown',
                'name' => Html::getInputName($modelSearch, 'country_id'),
                'length' => false,
            ]
        ) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
