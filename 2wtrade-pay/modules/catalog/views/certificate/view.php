<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\models\Certificate $model */
/** @var \app\models\search\CertificateSearch $modelSearch */
/** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */

$this->title = Yii::t('common', 'Просмотр сертификата');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список сертификатов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сведения о сертификате'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return $model->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return $model->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return $model->description;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product_id',
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return Html::decode($model->product);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return Html::decode($model->country);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_partner',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    /** @var \app\models\Certificate $model */
                    return Label::widget([
                        'label' => $model->is_partner ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_partner ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'availability',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    /** @var $data \app\models\Certificate */
                    return Label::widget([
                        'label' => $data->checkProductInCountry($data->product_id, $data->country_id) > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $data->checkProductInCountry($data->product_id, $data->country_id) > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => '',
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Файлы сертификата'),
    'actions' => DataProvider::renderSummary($dataProviderFiles),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProviderFiles,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-150 text-cente'],
                'content' => function ($modelFilesSearch) {
                    /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                    return $modelFilesSearch->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'origfilename',
                'content' => function ($modelFilesSearch) {
                    /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                    return HTML::a($modelFilesSearch->origfilename, Url::toRoute('certificate/download/' . $modelFilesSearch->id), ['target' => '_blank']);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'unicfilename',
                'content' => function ($modelFilesSearch) {
                    /** @var \app\models\search\CertificateFileSearch $modelFilesSearch */
                    return $modelFilesSearch->unicfilename;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => '',
]) ?>

<?= ButtonLink::widget([
    'label' => Yii::t('common', 'Вернуться назад'),
    'url' => Url::toRoute('index'),
    'style' => ButtonLink::STYLE_DEFAULT,
    'size' => ButtonLink::SIZE_SMALL,
]) . LinkPager::widget(['pagination' => $dataProvider->getPagination()]); ?>
