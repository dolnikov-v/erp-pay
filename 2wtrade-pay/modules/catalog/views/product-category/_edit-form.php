<?php
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
use app\models\ProductCategory;

/** @var ProductCategory $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить категорию') : Yii::t('common', 'Сохранить категорию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
