<?php
use app\modules\catalog\widgets\Product;
use app\widgets\Panel;
use yii\helpers\Url;

use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;

/** @var yii\web\View $this */
/** @var app\models\Product $model */
/** @var string $controller */
/** @var array $productsCategory */

ModalConfirmDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление категории товара') : Yii::t('common', 'Редактирование');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Категории товаров'), 'url' => Url::toRoute('/catalog/product-category/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление категории товара') : Yii::t('common', 'Редактирование')];

//ProductAsset::register($this);
?>

<?= Panel::widget([
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>

<?php if ($model->id): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список товаров'),
        'collapse' => true,
        'withBody' => false,
        'content' => Product::widget([
            'model' => $model,
            'controller' => $controller,
            'productList' => $products,
            'productsByModel' => $productsCategory,
        ]),
    ]) ?>

<?php endif; ?>

<?= ModalConfirmDelete::widget() ?>
