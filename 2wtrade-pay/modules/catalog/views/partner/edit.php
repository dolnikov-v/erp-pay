<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\catalog\widgets\Country;

/** @var \yii\web\View $this */
/** @var \app\models\Partner $model */
/** @var string $controller */
/** @var app\models\Country[] $countries */
/** @var array $countriesByPartner */

$this->title = $model->isNewRecord ?  Yii::t('common', 'Добавление партнера') : Yii::t('common', 'Редактирование партнера');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => Url::toRoute('/catalog/partner/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление партнера') : Yii::t('common', 'Редактирование партнера')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Партнер'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>


<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список стран'),
        'collapse' => true,
        'withBody' => false,
        'content' => Country::widget([
            'model' => $model,
            'controller' => $controller,
            'countryList' => $countries,
            'countriesByModel' => $countriesByPartner,
        ]),
    ]) ?>
<?php endif; ?>
