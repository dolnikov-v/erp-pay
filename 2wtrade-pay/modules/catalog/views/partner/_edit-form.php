<?php
use app\components\widgets\ActiveForm;
use app\modules\order\models\Order;
use yii\helpers\Url;
use app\models\Timezone;

/** @var app\models\Partner $model */
?>

<?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'source_id')->select2List(
                \app\models\Source::find()->collection(),
            [
                'prompt' => '—',
            ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'foreign_id')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'timezone_id')->select2List(Timezone::find()
            ->orderBy(['name' => SORT_ASC])
            ->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить партнера') : Yii::t('common', 'Сохранить партнера')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
