<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\models\search\CountrySearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'char_code')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'is_partner')->select2List([
                Yii::t('common', 'На прямую'),
                Yii::t('common', 'Через партнера'),
        ],
            ['prompt' => '-'])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
