<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\helpers\i18n\Moment;
use app\modules\catalog\assets\ATimerAsset;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var app\models\search\CountrySearch $modelSearch */

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Страны')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со странами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($data) {
                    return Yii::t('common', $data->name);
                },
            ],
            [
                'attribute' => 'is_partner',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_partner ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_partner ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_distributor',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_distributor ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_distributor ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'char_code',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'timezone.timezone_id',
                'headerOptions' => ['class' => 'width-200 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'header' => Yii::t('common', 'Валюта'),
                'attribute' => 'currency.char_code',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'header' => Yii::t('common', 'Ответственный куратор'),
                'attribute' => 'curatorUser.username',
                'headerOptions' => ['class' => 'width-150 text-center'],
            ],
            [
                'header' => Yii::t('common', 'Местное время'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    $time = $data->timezone ? time() - Yii::$app->user->timezone->time_offset + $data->timezone->time_offset : time();

                    return Html::tag('span', Yii::$app->formatter->asFullTime($time), ['class' => 'atimer-auto-init', 'data-format' => Moment::getFullTimeFormat()]);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.country.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/country/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/country/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.deactivate') && $model->active;
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.delete')
                            || (Yii::$app->user->can('catalog.country.activate') && !$model->active)
                            || (Yii::$app->user->can('catalog.country.deactivate') && $model->active);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.country.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.country.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить страну'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
