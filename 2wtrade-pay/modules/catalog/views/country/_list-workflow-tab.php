<?php
use app\modules\catalog\widgets\Workflow;
use app\modules\order\models\OrderWorkflow;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \app\models\Country $model */

?>
<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список workflows'),
        'withBody' => false,
        'content' => Workflow::widget([
            'model' => $model,
            'workflows' => OrderWorkflow::find()->active()->all(),
            'workflowsByCountry' => $model->workflows,
            'url' => Url::toRoute(['set-workflow']),
        ]),
    ]) ?>
<?php endif; ?>