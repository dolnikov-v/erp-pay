<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\Nav;
use app\widgets\Panel;


/** @var \app\models\Country $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>

<?= Panel::widget([
    'title' => '',
    'border' => false,
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => yii::t('common', 'Основные настройки'),
                'content' => $this->render('_main-settings-tab', [
                    'form' => $form,
                    'model' => $model
                ])
            ],
            [
                'label' => yii::t('common', 'Список workflows'),
                'content' => $this->render('_list-workflow-tab', [
                    'model' => $model
                ])
            ],
            [
                'label' => yii::t('common', 'Автоаппрув'),
                'content' => $this->render('_auto-approve-tab', [
                    'form' => $form,
                    'model' => $model
                ]),
                'visible' => yii::$app->user->can('catalog.country.autoapprove')
            ]
        ],
    ])
]); ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить страну') : Yii::t('common', 'Сохранить страну')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
