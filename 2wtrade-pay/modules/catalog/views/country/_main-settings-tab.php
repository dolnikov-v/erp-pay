<?php

use app\models\User;
use app\models\Language;
use app\models\Currency;
use app\models\Timezone;

/** @var \app\models\Country $model */
/** @var  \app\components\widgets\ActiveForm $form */
?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'language_id')->select2List(Language::find()
            ->orderBy(['name' => SORT_ASC])
            ->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'slug')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'char_code')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'timezone_id')->select2List(Timezone::find()
            ->orderBy(['name' => SORT_ASC])
            ->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'currency_id')->select2List(Currency::find()
            ->orderBy(['name' => SORT_ASC])
            ->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'curator_user_id')->select2List(User::find()
            ->active()
            ->byCountry($model->id)
            ->byRole('country.curator')
            ->orderBy(['username' => SORT_ASC])
            ->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'sms_notifier_phone_from')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'cc_operator_daily_approve_plan')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'order_notification_reply_email')->textInput() ?>
    </div>
</div>

<div class="row form-group">
    <div class="col-lg-2">
        <?= $form->field($model, 'brokerage_enabled')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->brokerage_enabled,
        ]) ?>
        <?= $form->field($model, 'is_partner')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->is_partner,
        ]) ?>
        <?= $form->field($model, 'is_stop_list')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->is_stop_list,
        ]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'trash_doubles_enabled')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->trash_doubles_enabled,
        ]) ?>
        <?= $form->field($model, 'is_distributor')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->is_distributor,
        ]) ?>
        <?= $form->field($model, 'auto_trash')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->auto_trash,
        ]) ?>
    </div>
</div>