<?php
/** @var Country $model */

use app\helpers\WbIcon;
use app\models\Country;
use app\models\Product;
use app\modules\catalog\models\ProductCriticalApprove;
use app\widgets\Button;
use app\modules\catalog\assets\ProductCriticalApproveAsset;

ProductCriticalApproveAsset::register($this);
?>
<div class="alert alert-info">
    <span class="fa fa-bolt"></span>
    <?=yii::t('common', 'Если указан критичный апрув по стране, данные по товарам будут игнорироваться. Удалите значение апрува для страны, чтобы использовать апрув по товарам');?>
</div>

<div class="row">
    <div class="col-lg-4">
        <?=$form->field($model, 'approve_type')->select2List([
            Country::TYPE_COUNTRY => yii::t('common', 'по стране'),
            Country::TYPE_PRODUCT => yii::t('common', 'по товару')
        ], [
                'value' => $model->isNewRecord || empty($model->productCriticalApproves) ? Country::TYPE_COUNTRY : Country::TYPE_PRODUCT,
                'name' => 'approve_type'
        ])->label('<h4>' . yii::t('common', 'Вид автоаппрува') . '</h4>');?>
    </div>
</div>

<fieldset class="by-country  <?=(empty($model->productCriticalApproves) || $model->isNewRecord) ? '' : 'hidden';?>">
    <legend class="hidden"><h4><?= yii::t('common', 'По стране') ?></h4></legend>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'critical_approve_percent')->textInput() ?>
        </div>
    </div>
</fieldset>
<br/>
<fieldset class="by-products <?=(empty($model->productCriticalApproves)) ? 'hidden' : '';?>">
    <legend  class="hidden"><h4><?= yii::t('common', 'По товарам') ?></h4></legend>
    <div class="products_percent">

        <?php if (!empty($model->productCriticalApproves)): ?>

            <?php foreach ($model->productCriticalApproves as $criticalApprove) : ?>

                <div class="row product_percent">
                    <div class="col-lg-4">
                        <?= $form->field($criticalApprove, 'product_id')->select2List(Product::find()->collection(), [
                            'prompt' => '-',
                            'name' => 'ProductCriticalApprove[product_id][]'
                        ])
                        ?>
                    </div>
                    <div class="col-lg-4">
                        <?= $form->field($criticalApprove, 'percent')->textInput([
                            'type' => 'number',
                            'name' => 'ProductCriticalApprove[percent][]'
                        ]) ?>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group buttons">
                            <?= Button::widget([
                                'type' => 'button',
                                'icon' => WbIcon::MINUS,
                                'style' => Button::STYLE_DANGER . ' btn-critical-approve-minus',
                            ]) ?>

                            <?= Button::widget([
                                'type' => 'button',
                                'icon' => WbIcon::PLUS,
                                'style' => Button::STYLE_SUCCESS . ' btn-critical-approve-plus',
                            ]) ?>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <?= $form->field($criticalApprove, 'country_id')->hiddenInput([
                            'value' => $model->id,
                            'name' => 'ProductCriticalApprove[country_id][]'
                        ])->label(false) ?>
                    </div>
                </div>

            <?php endforeach; ?>

        <?php else: ?>

            <div class="row product_percent">
                <div class="col-lg-4">
                    <?= $form->field(new ProductCriticalApprove(), 'product_id')->select2List(Product::find()->collection(), [
                        'prompt' => '-',
                        'name' => 'ProductCriticalApprove[product_id][]'
                    ])
                    ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field(new ProductCriticalApprove(), 'percent')->textInput([
                        'type' => 'number',
                        'name' => 'ProductCriticalApprove[percent][]'
                    ]) ?>
                </div>
                <div class="col-lg-4">
                    <div class="form-group buttons">
                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::MINUS,
                            'style' => Button::STYLE_DANGER . ' btn-critical-approve-minus',
                        ]) ?>

                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::PLUS,
                            'style' => Button::STYLE_SUCCESS . ' btn-critical-approve-plus',
                        ]) ?>

                    </div>
                </div>
                <div class="col-lg-4">
                    <?= $form->field(new ProductCriticalApprove(), 'country_id')->hiddenInput([
                        'value' => $model->id,
                        'name' => 'ProductCriticalApprove[country_id][]'
                    ])->label(false) ?>
                </div>
            </div>

        <?php endif; ?>

    </div>
</fieldset>
