<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Currency;

/** @var app\models\ProductPrice $model */
/** @var integer $productId */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($model->getFreeCountries(), ['length' => false]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'currency_id')->select2List(Currency::find()->collection(), ['prompt' => '—', 'length' => false]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'price')->textInput() ?>
        <?= $form->field($model, 'product_id')->hiddenInput(['value' => ($productId ?? $model->product_id)])->label(false); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить цену') : Yii::t('common', 'Сохранить цену')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('product/prices/' . ($productId ?? $model->product_id))) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
