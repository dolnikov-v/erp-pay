<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\ProductPrice $model */
/** @var integer $productId */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление цены') : Yii::t('common', 'Редактирование цены');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Редактирование товара'), 'url' => Url::toRoute('/catalog/product/prices/' . $model->product_id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Цена'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'productId' => $productId,
    ])
]) ?>