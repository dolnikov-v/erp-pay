<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

?>

<?= \app\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => \yii\grid\SerialColumn::class,
            'headerOptions' => ['class' => 'th-record-id'],
            'contentOptions' => ['class' => 'td-record-id']
        ],
        [
            'attribute' => 'currency.name',
            'content' => function ($data) {
                return Yii::t('common', $data->currency->name);
            },
        ],
        [
            'attribute' => 'currency.char_code',
            'headerOptions' => ['class' => 'width-150 text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ],
        [
            'class' => \app\components\grid\DateColumn::class,
            'formatType' => 'Date',
            'attribute' => 'created_at',
            'label' => Yii::t('common', 'Дата'),
        ],
        [
            'attribute' => 'rate',
            'label' => Yii::t('common', 'Стоимость 1 USD'),
            'headerOptions' => ['class' => 'width-150 text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ]
    ]
])?>
