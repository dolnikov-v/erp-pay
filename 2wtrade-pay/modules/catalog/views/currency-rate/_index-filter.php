<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\search\CurrencyRateHistorySearch $searchModel
 */

use app\components\widgets\ActiveForm;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($searchModel, 'currency_id')->select2List(\app\models\Currency::find()->collection(), [
            'prompt' => '—',
            'length' => 1
        ])?>
    </div>
    <div class="col-md-3">
        <?= $form->field($searchModel, 'created_at')->datePicker()->label(Yii::t('common', 'Дата')) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
