<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\search\CurrencyRateHistorySearch $searchModel
 */

$this->title = Yii::t('common', 'Курсы валют');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= \app\widgets\Panel::widget([
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_index-filter', ['searchModel' => $searchModel]),
    'collapse' => true,
]) ?>

<?= \app\widgets\Panel::widget([
    'title' => Yii::t('common', 'Курсы валют'),
    'actions' => \app\helpers\DataProvider::renderSummary($dataProvider),
    'content' => $this->render('_index-content', ['dataProvider' => $dataProvider]),
    'withBody' => false,
    'footer' => \app\widgets\LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => \app\widgets\LinkPager::COUNTER_POSITION_RIGHT
    ])
]) ?>