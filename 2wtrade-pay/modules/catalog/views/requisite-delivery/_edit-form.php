<?php

use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\modules\catalog\assets\RequisiteDeliveryLinkAsset;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\widgets\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\catalog\assets\RequisiteDeliveryAsset;

/** @var app\modules\catalog\models\RequisiteDelivery $model */
/** @var boolean $disabled */
/** @var \app\modules\catalog\models\RequisiteDeliveryLink $RequisiteDeliveryLink */
/** @var string $deliveryList */
/** @var string $requisiteData */

RequisiteDeliveryLinkAsset::register($this);
RequisiteDeliveryAsset::register($this);

?>
<div class="requisite-data hidden"><?= isset($requisiteData) ? $requisiteData : $deliveryList; ?></div>

<div class="delivery_data hidden" data-view="<?= (int)$disabled; ?>"><?= $deliveryList; ?></div>

<fieldset <?= ($disabled ? "disabled='disabled'" : ''); ?>>

    <?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>

    <fieldset>
        <legend>
            <small><?= Yii::t('common', 'Реквизит'); ?></small>
            <hr/>
        </legend>

        <div class="row">

            <div class="col-lg-3">
                <?= $form->field($model, 'name')->textInput() ?>
            </div>
            <div class="col-lg-5">
                <?= $form->field($model, 'address')->textInput() ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'city')->textInput() ?>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-3">
                <?= $form->field($model, 'beneficiary_bank')->textInput() ?>
            </div>
            <div class="col-lg-5">
                <?= $form->field($model, 'bank_address')->textInput() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'bank_account')->textInput() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'swift_code')->textInput() ?>
            </div>
        </div>

    </fieldset>

    <br/>

    <fieldset>
        <legend>
            <small><?= Yii::t('common', 'Шаблон инвойса'); ?></small>
            <hr/>
        </legend>

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'invoice_tpl')->select2List(\app\modules\catalog\models\RequisiteDelivery::getInvoiceTPLSCollection(), [
                    'prompt' => '-'
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <label for="stamp-invoice_tpl">Печать</label>
                <div class="stamp">
                    <?php foreach (['' => '-'] + $model->InvoiceSTAMPScollection() as $tpl_num => $stamp): ?>

                        <?php if (!empty($tpl_num)) : ?>

                            <img height="306px"
                                 class="stamp  index-<?= $tpl_num ?> <?= $model->invoice_tpl != $tpl_num ? 'hidden' : '' ?>"
                                 src="data:image/png;base64,<?= base64_encode(file_get_contents($stamp)) ?>"/>

                        <?php else: ?>

                            <?= Html::tag('i', '- ' . yii::t('common', 'Стандартная')); ?>

                        <?php endif; ?>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </fieldset>

    <br/>
    <br/>

    <fieldset>
        <legend>
            <small><?= yii::t('common', 'Список контрактов курьерских служб'); ?></small>
            <hr/>
        </legend>

        <div class="requisite-delivery-links">

            <?php if ($model->getRequisiteDeliveryLink()->all()) : ?>

                <?php foreach ($model->getRequisiteDeliveryLink()->all() as $link) : ?>

                    <div
                            data-index="<?= $link->id; ?>"
                            data-country_id="<?= $link->country_id; ?>"
                            data-delivery_id="<?= $link->delivery_id; ?>"
                            data-delivery_contract_id="<?= $link->delivery_contract_id; ?>"
                            class="row requisite-delivery-link">
                        <div class="col-lg-2">
                            <?= $form->field($RequisiteDeliveryLink, 'country_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'country_id[' . $link->id . ']',
                                'disabled' => $disabled
                            ]) ?>
                        </div>
                        <div class="col-lg-2">
                            <?= $form->field($RequisiteDeliveryLink, 'delivery_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'delivery_id[' . $link->id . ']',
                                'disabled' => $disabled
                            ]) ?>
                        </div>
                        <div class="col-lg-2">
                            <?= $form->field($RequisiteDeliveryLink, 'delivery_contract_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'contract_id[' . $link->id . ']',
                                'disabled' => $disabled
                            ]) ?>
                        </div>

                        <div class="col-lg-2 <?= ($disabled === true ? 'hidden' : ''); ?>">
                            <div class="form-group">
                                <div class="control-label">
                                    <label>&nbsp;</label>
                                </div>
                                <?= Button::widget([
                                    'type' => 'button',
                                    'icon' => WbIcon::MINUS,
                                    'style' => Button::STYLE_DANGER . ' btn-requisite-delivery-link-minus',
                                ]) ?>

                                <?= Button::widget([
                                    'type' => 'button',
                                    'icon' => WbIcon::PLUS,
                                    'style' => Button::STYLE_SUCCESS . ' btn-requisite-delivery-link-plus',
                                ]) ?>

                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>

            <?php else: ?>

                <?php if (!$disabled): ?>

                    <div class="row requisite-delivery-link">
                        <div class="col-lg-2">
                            <?= $form->field(new RequisiteDeliveryLink(), 'country_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'country_id[0]'
                            ]) ?>
                        </div>
                        <div class="col-lg-2">
                            <?= $form->field(new RequisiteDeliveryLink(), 'delivery_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'delivery_id[0]'
                            ]) ?>
                        </div>
                        <div class="col-lg-2">
                            <?= $form->field(new RequisiteDeliveryLink(), 'delivery_contract_id')->select2List([], [
                                'prompt' => '—',
                                'name' => 'contract_id[0]'
                            ]) ?>
                        </div>

                        <div class="col-lg-">
                            <div class="form-group">
                                <div class="control-label">
                                    <label>&nbsp;</label>
                                </div>
                                <?= Button::widget([
                                    'type' => 'button',
                                    'icon' => WbIcon::MINUS,
                                    'style' => Button::STYLE_DANGER . ' btn-requisite-delivery-link-minus',
                                ]) ?>

                                <?= Button::widget([
                                    'type' => 'button',
                                    'icon' => WbIcon::PLUS,
                                    'style' => Button::STYLE_SUCCESS . ' btn-requisite-delivery-link-plus',
                                ]) ?>

                            </div>
                        </div>
                    </div>

                <?php endif; ?>

            <?php endif; ?>

        </div>

    </fieldset>

    <br/>
    <br/>

    <?php if (!$disabled) : ?>

        <div class="row">
            <div class="col-lg-12">
                <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить реквизит') : Yii::t('common', 'Сохранить реквизит')) ?>
                <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
            </div>
        </div>

    <?php endif; ?>

    <?php ActiveForm::end(); ?>

</fieldset>
