<?php

use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\catalog\models\RequisiteDelivery $model */
/** @var boolean $disabled */
/** @var string $deliveryList */
/** @var \app\modules\catalog\models\RequisiteDeliveryLink $RequisiteDeliveryLink */
/** @var string $requisiteData */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление реквизита') : Yii::t('common', 'Редактирование реквизита');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Реквизиты курьерских служб'), 'url' => Url::toRoute('/catalog/requisite-delivery/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'RequisiteDeliveryLink' => $RequisiteDeliveryLink,
        'deliveryList' => $deliveryList,
        'requisiteData' => $requisiteData,
        'disabled' => false
    ]),
]); ?>