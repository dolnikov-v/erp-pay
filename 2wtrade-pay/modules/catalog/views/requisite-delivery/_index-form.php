<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\catalog\models\RequisiteDelivery $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'name')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'city')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'beneficiary_bank')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'swift_code')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>