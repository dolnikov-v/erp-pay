<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\grid;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\catalog\models\search\RequisiteDeliverySearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Реквизиты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с реквизитами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($model) {
                    return $model->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'city',
                'content' => function ($model) {
                    return $model->city;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'beneficiary_bank',
                'content' => function ($model) {
                    return $model->beneficiary_bank;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'bank_account',
                'content' => function ($model) {
                    return $model->bank_account;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'swift_code',
                'content' => function ($model) {
                    return $model->swift_code;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'invoice_tpl',
                'content' => function ($model) {
                    /** @var \app\modules\catalog\models\RequisiteDelivery $model */
                    $invoiceCollection = \app\modules\catalog\models\RequisiteDelivery::getInvoiceTPLSCollection();
                    return $model->invoice_tpl ? $invoiceCollection[$model->invoice_tpl] : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'requisite_delivery_link',
                'content' => function ($model) {
                    /** @var \app\modules\catalog\models\RequisiteDelivery $model */
                    return count($model->getRequisiteDeliveryLink()->all());
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['view', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.requisitedelivery.view');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.requisitedelivery.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.requisitedelivery.delete');
                        },
                    ],
                ],
            ],
        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.requisitedelivery.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить реквизит'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>