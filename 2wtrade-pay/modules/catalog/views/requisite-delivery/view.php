<?php

use app\widgets\Panel;
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var \app\modules\catalog\models\RequisiteDelivery $model */
/** @var \app\modules\catalog\models\search\RequisiteDeliverySearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var boolean $disabled */
/** @var string $deliveryList */
/** @var \app\modules\catalog\models\RequisiteDeliveryLink $RequisiteDeliveryLink */

$this->title = Yii::t('common', 'Просмотр реквизитов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочникик'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Реквизиты'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'disabled' => true,
        'deliveryList' => $deliveryList,
        'RequisiteDeliveryLink' => $RequisiteDeliveryLink
    ]),
]); ?>

