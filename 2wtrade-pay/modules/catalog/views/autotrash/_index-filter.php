<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\catalog\models\search\AutotrashSearch $modelSearch */
/** @var app\modules\catalog\models\Autotrash $fieldsCollection */
/** @var app\modules\catalog\models\Autotrash $filtersCollection */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'template')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'field')->select2List(
            $fieldsCollection, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'filter')->select2List(
            $filtersCollection, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
