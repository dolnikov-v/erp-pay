<?php
use app\widgets\custom\Checkbox;
use yii\helpers\Html;
?>

<?= Checkbox::widget([
    'id' => Html::getInputId($model, 'autotrash_status'),
    'name' => Html::getInputName($model, 'autotrash_status'),
    'value' => 1,
    'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
    'label' => Yii::t('common', 'Использовать автотреш'),
    'checked' => $stateAutotrash,
]) ?>