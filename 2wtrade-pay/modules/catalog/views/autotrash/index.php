<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\catalog\assets\ATimerAsset;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\modules\catalog\assets\AutotrashSwitcheryAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use app\widgets\Nav;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\DateColumn;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\catalog\models\Autotrash $model */
/** @var app\modules\catalog\models\search\AutotrashSearch $modelSearch */
/** @var app\modules\catalog\models\Autotrash $fieldsCollection */
/** @var app\modules\catalog\models\Autotrash $filtersCollection */
/** @var app\modules\catalog\controllers\AutotrashController $stateAutotrash */

AutotrashSwitcheryAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Автотреш');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Автотреш')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'model' => $model,
        'modelSearch' => $modelSearch,
        'fieldsCollection' => $fieldsCollection,
        'filtersCollection' => $filtersCollection,
    ]),
    'collapse' => true,
]) ?>

<div class="form-group clearfix">
    <?= Panel::widget([
        'content' => $this->render('autotrash-switch', [
            'model' => $model,
            'stateAutotrash' => $stateAutotrash
        ]),
        'collapse' => false,
    ]) ?>
</div>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с фильтрами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->is_active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'template',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($data) {
                    return Yii::t('common', $data->template);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'field',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($data) use ($fieldsCollection) {
                    return Yii::t('common', $fieldsCollection[$data->field]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'filter',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($data) use ($filtersCollection) {
                    return Yii::t('common', $filtersCollection[$data->filter]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'headerOptions' => ['class' => 'width-250 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['class' => 'width-150'],
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.autotrash.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.autotrash.delete');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.autotrash.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить фильтр'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
