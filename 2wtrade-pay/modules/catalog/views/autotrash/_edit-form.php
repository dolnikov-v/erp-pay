<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\modules\catalog\assets\CertificateNotNeededAsset;

/** @var app\modules\catalog\models\Autotrash $model */
/** @var app\modules\catalog\models\search\AutotrashSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id]), 'options' => []]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'template')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'field')->select2List(
            $fieldsCollection, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'filter')->select2List(
            $filtersCollection, [
                'prompt' => '—',
                'length' => false,
            ]
        ) ?>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить фильтр') : Yii::t('common', 'Сохранить фильтр')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?= ModalConfirmDelete::widget() ?>

