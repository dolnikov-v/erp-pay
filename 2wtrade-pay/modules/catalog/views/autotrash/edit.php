<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var app\modules\catalog\models\Authotrash $model */
/** @var app\modules\catalog\models\search\AuthotrashSearch $modelSearch */
/** @var app\modules\catalog\models\Authotrash $fieldsCollection */
/** @var app\modules\catalog\models\Authotrash $filtersCollection */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление фильтра') : Yii::t('common', 'Редактирование фильтра');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Автотреш'), 'url' => Url::toRoute('/catalog/autotrash/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление фильтра') : Yii::t('common', 'Редактирование фильтра')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные фильтра'),
    'alert' => Yii::t('common', 'При использовании правил для длины строки, в поле "Шаблон" использовать целые числовые значения (в противном случае все значения приравниваются к нулю).'
        . ' <br/> При использовании правил "Регулярное выражение", в поле "Шаблон", используйте делимитеры и по необходимости модификаторы (экранирование метасимволов обязательно). Пример регулярного выражения для поиска по номеру телефона #^81431123321$#, где # выступает в качестве делимитеров паттерна.'
        .'<a href="https://goo.gl/ssDVaX" target="_blank">Подробнее о паттернах </a>, <a href="https://goo.gl/UeWwzJ" target="_blank">тестирование</a>'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'modelSearch' => $modelSearch,
        'fieldsCollection' => $fieldsCollection,
        'filtersCollection' => $filtersCollection
    ])
]) ?>