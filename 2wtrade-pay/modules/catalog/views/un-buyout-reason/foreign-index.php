<?php
use app\widgets\Panel;
use app\helpers\DataProvider;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

ModalConfirmDeleteAsset::register($this);
/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $foreignDataProvider */

$this->title = Yii::t('common', 'Внешние причины без привязки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Причины невыкупа'), 'url' => Url::toRoute('/catalog/un-buyout-reason/index')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Внешние причины'),
    'actions' => DataProvider::renderSummary($foreignDataProvider),
    'withBody' => false,
    'content' => $this->render('_foreign-list', ['foreignDataProvider' => $foreignDataProvider]),
    'footer' => LinkPager::widget(['pagination' => $foreignDataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>