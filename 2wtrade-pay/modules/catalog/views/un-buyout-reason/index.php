<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\DateColumn;
use app\modules\catalog\models\search\UnBuyoutReasonSearch;


/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\catalog\models\search\UnBuyoutReasonSearch $modelSearch */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список причин');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Причины невыкупа')];

$categories = UnBuyoutReasonSearch::getCategories();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со причинами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' =>
        '<div class="add-wrap pull-right">' . ButtonLink::widget([
            'label' => Yii::t('common', 'Внешние причины без привязки'),
            'url' => Url::toRoute('foreign-index'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) . '</div>' .
        GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model) {
                return [
                    'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
                ];
            },
            'columns' => [
                [
                    'class' => IdColumn::className(),
                ],
                [
                    'attribute' => 'name',
                    'content' => function ($data) {
                        return Yii::t('common', $data->name);
                    },
                ],
                [
                    'attribute' => 'category',
                    'content' => function ($data) use ($categories) {
                        return Yii::t('common', $categories[$data->category]) ?? '-';
                    },
                ],
                [
                    'attribute' => 'active',
                    'headerOptions' => ['class' => 'width-150 text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($model) {
                        return Label::widget([
                            'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                            'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                        ]);
                    },
                    'enableSorting' => false,
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'created_at',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'updated_at',
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($data) {
                                return Url::toRoute(['edit', 'id' => $data->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.unbuyoutreason.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Активировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['/catalog/un-buyout-reason/activate', 'id' => $model->id]);
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('catalog.unbuyoutreason.activate') && !$model->active;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Деактивировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['/catalog/un-buyout-reason/deactivate', 'id' => $model->id]);
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('catalog.unbuyoutreason.deactivate') && $model->active;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($data) {
                                return [
                                    'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                                ];
                            },
                            'can' => function ($data) {
                                /** @var $data \app\modules\catalog\models\UnBuyoutReason */
                                return !$data->foreignReasons && Yii::$app->user->can('catalog.unbuyoutreason.delete');
                            },
                        ],
                    ]
                ]
            ],
        ]),
    'footer' => (Yii::$app->user->can('catalog.unbuyoutreason.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить причину'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
