<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\catalog\models\UnBuyoutReasonMapping $model */
/** @var array $reasons */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление внешней причины') : Yii::t('common', 'Редактирование внешней причины');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Причины невыкупа'), 'url' => Url::toRoute('/catalog/un-buyout-reason/index')];
$this->params['breadcrumbs'][] = [
    'label' => $model->reason_id ? $model->reason->name : Yii::t('common', 'Причины без привязки'),
    'url' => Url::toRoute($model->reason_id ? '/catalog/un-buyout-reason/edit/' . $model->reason_id : '/catalog/un-buyout-reason/foreign-index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление внешней причины') : Yii::t('common', 'Редактирование внешней причины')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные внешней причины'),
    'content' => $this->render('_edit-foreign-form', [
        'model' => $model,
        'reasons' => $reasons,
    ])
]) ?>
