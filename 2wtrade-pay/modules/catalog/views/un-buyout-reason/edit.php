<?php
use app\widgets\Panel;
use app\widgets\ButtonLink;
use app\helpers\DataProvider;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\custom\ModalConfirmDelete;

/** @var \yii\web\View $this */
/** @var \app\modules\catalog\models\UnBuyoutReason $model */
/** @var \yii\data\ActiveDataProvider $foreignDataProvider */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление причины') : Yii::t('common', 'Редактирование причины');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Причины невыкупа'), 'url' => Url::toRoute('/catalog/un-buyout-reason/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление причины') : Yii::t('common', 'Редактирование причины')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные причины'),
    'content' => $this->render('_edit-form', ['model' => $model])
]) ?>

<?php if ($model->id): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Внешние причины'),
        'actions' => DataProvider::renderSummary($foreignDataProvider),
        'withBody' => false,
        'content' => $this->render('_foreign-list', ['foreignDataProvider' => $foreignDataProvider]),
        'footer' => (Yii::$app->user->can('catalog.workingshift.foreign-edit') ?
                ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить внешнюю причину'),
                    'url' => Url::toRoute(['/catalog/un-buyout-reason/foreign-edit', 'reason_id' => $model->id]),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ])
                : '') . LinkPager::widget(['pagination' => $foreignDataProvider->getPagination()]),
    ]) ?>

    <?= ModalConfirmDelete::widget() ?>

<?php endif; ?>