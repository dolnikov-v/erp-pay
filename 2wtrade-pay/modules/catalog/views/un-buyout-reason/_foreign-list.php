<?php
use app\components\grid\GridView;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $foreignDataProvider */

print GridView::widget([
    'dataProvider' => $foreignDataProvider,
    'columns' => [
        [
            'attribute' => 'foreign_reason',
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'updated_at',
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($foreign) {
                        return Url::toRoute(['/catalog/un-buyout-reason/foreign-edit', 'id' => $foreign->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('catalog.unbuyoutreason.foreignedit');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($foreign) {
                        return [
                            'data-href' => Url::toRoute(['/catalog/un-buyout-reason/foreign-delete', 'id' => $foreign->id, 'reason_id' => $foreign->reason_id])
                        ];
                    },
                    'can' => function () {
                        return Yii::$app->user->can('catalog.unbuyoutreason.foreigndelete');
                    },
                ],
            ]
        ]
    ],
]);