<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\catalog\models\UnBuyoutReason $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'category')->select2List($model::getCategories()) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->active,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить причину') : Yii::t('common', 'Сохранить причину')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
