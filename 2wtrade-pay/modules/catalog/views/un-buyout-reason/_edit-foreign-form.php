<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\catalog\models\UnBuyoutReasonMapping $model */
/** @var array $reasons */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['foreign-edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-10">
        <?= $form->field($model, 'foreign_reason')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <?= $form->field($model, 'reason_id')->select2List($reasons, ['prompt' => '-']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить причину') : Yii::t('common', 'Сохранить причину')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('un-buyout-reason/edit/' . $model->reason_id)); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
