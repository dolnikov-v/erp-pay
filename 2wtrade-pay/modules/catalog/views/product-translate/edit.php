<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\ProductTranslation $model */
/** @var integer $productId */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление перевода') : Yii::t('common', 'Редактирование перевода');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Редактирование товара'), 'url' => Url::toRoute('/catalog/product/translate/' . $model->product_id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Перевод'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'productId' => $productId,
    ])
]) ?>