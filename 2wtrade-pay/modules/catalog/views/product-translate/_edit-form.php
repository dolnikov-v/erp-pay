<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Language;

/** @var app\models\ProductTranslation $model */
/** @var integer $productId */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($model->getFreeCountries(), ['length' => false]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'language_id')->select2List($model->getFreeLanguage(), ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'product_id')->hiddenInput(['value' => ($productId ?? $model->product_id)])->label(false); ?>
    </div>

</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить перевод') : Yii::t('common', 'Сохранить перевод')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('product/translate/' . ($productId ?? $model->product_id))) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
