<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\grid;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\catalog\models\search\ExternalSourceSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список сторонних источников');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с источниками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($model) {
                    return $model->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'count_roles',
                'content' => function ($model) {
                    return count($model->externalSourceRole);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url',
                'content' => function ($model) {
                    return Html::a($model->url, $model->url);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'content' => function ($model) {
                    return $model->description;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.externalsource.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Список ролей'),
                        'url' => function ($model) {
                            return Url::to(['/catalog/external-source-role', 'ExternalSourceRoleSearch[external_source_id]' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.externalsourcerole.index');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.externalsource.delete');
                        },
                    ],
                ],
            ],
        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.landing.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить источник'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>