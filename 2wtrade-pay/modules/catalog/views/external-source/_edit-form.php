<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\catalog\models\ExternalSource $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'description')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'incoming_amazon_queue_name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'outbound_amazon_queue_name')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить источник') : Yii::t('common', 'Сохранить источник')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>