<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\catalog\models\ExternalSource $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'url')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'description')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>