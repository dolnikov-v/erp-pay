<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Landing $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление стороннего источника') : Yii::t('common', 'Редактирование стороннего источника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сторонние источики'), 'url' => Url::toRoute('/catalog/external-source/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сторонние источики'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ]),
    'alert' => yii::t('common', 'Наименование источника используется для идентификации при получении ролей'),
    'alertStyle' => Panel::ALERT_INFO
]); ?>