<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление изображения') : Yii::t('common', 'Редактирование изображения');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Редактирование товара'), 'url' => Url::toRoute('/catalog/product/photo/' . $model->product_id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фото'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>