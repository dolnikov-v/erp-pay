<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;

/** @var \app\models\ProductPhoto $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<?=$form->field($model, 'product_id')->hiddenInput()->label(false);?>
<?=$form->field($model, 'id')->hiddenInput()->label(false);?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'country_id')->select2List($model->getFreeCountries(), ['length' => 1]);?>
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'type' => 'file',
                'name' => Html::getInputName($model, 'imageFile'),
            ]),
        ]) ?>
        <br>
        <?php if ($model->image): ?>
            <div class="catalog-image">
                <?= Html::img('/media/product/' . $model->image, ['class' => 'image-100']) ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить фото') : Yii::t('common', 'Сохранить фото')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('/catalog/product/photo/' . $model->product_id)); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
