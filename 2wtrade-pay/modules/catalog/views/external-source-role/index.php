<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\grid;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\catalog\models\search\ExternalSourceRoleSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список ролей сторонних источников');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с ролями источников'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'external_source_id',
                'content' => function ($model) {
                    return $model->externalSource->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'content' => function ($model) {
                    return $model->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'content' => function ($model) {
                    return $model->description;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>