<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\grid;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\models\Landing $model */
/** @var \app\models\search\ProductLandingSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Товары лендинга');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список лендингов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Товары лендинга'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'landing_id',
                'content' => function($model) {
                    /** @var \app\models\ProductLanding $model */
                    return Html::a($model->landing->url, $model->landing->url);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product_id',
                'content' => function($model) {
                    /** @var \app\models\ProductLanding $model */
                    return $model->product->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => ButtonLink::widget([
        'label' => Yii::t('common', 'Вернуться назад'),
        'url' => Url::toRoute('index'),
        'style' => ButtonLink::STYLE_DEFAULT,
        'size' => ButtonLink::SIZE_SMALL,
    ]) . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
