<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Landing $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление лендинга') : Yii::t('common', 'Редактирование лендинга');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Лендинги'), 'url' => Url::toRoute('/catalog/landing/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Лендинг'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
