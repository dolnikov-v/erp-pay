<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\modules\catalog\assets\ATimerAsset;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\DateColumn;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\catalog\models\OperatorsPenalty $model */
/** @var app\modules\catalog\models\search\OperatorsPenaltySearch $modelSearch */

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Штрафы операторов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Штрафы операторов')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со штрафами операторов'),
//    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => [
            'class' => 'tr-vertical-align-middle ',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'label' => Yii::t('common', 'Колл-центр'),
                'attribute' => 'call_center_id',
                'content' => function ($data) use ($model) {
                    return Yii::t('common', $model->callCenters[$data->call_center_id]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'content' => function ($data) use ($model) {
                    return Yii::t('common', $model::getPenaltyTypeCollection()[$data->name]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'penalty_value',
                'content' => function ($data) {
                    return Yii::$app->formatter->asDecimal($data->penalty_value, 2);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => DateColumn::className(),
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'attribute' => 'updated_at',
                'label' => Yii::t('common', 'Дата изменения'),
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['class' => 'width-150'],
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('superadmin');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('superadmin');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => (Yii::$app->user->can('superadmin') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить штраф'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
