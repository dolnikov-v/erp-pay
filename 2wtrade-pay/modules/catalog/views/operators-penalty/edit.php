<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var app\modules\catalog\models\OperatorsPenalty $model */
/** @var app\modules\catalog\models\search\OperatorsPenaltySearch $modelSearch */
/** @var array $callcenterList */
/** @var array $penaltyList */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление штрафа') : Yii::t('common', 'Редактирование штрафа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Штрафы операторов'), 'url' => Url::toRoute('/catalog/operators-penalty/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление штрафа') : Yii::t('common', 'Редактирование штрафа')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Параметры штрафа'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'modelSearch' => $modelSearch,
        'callcenterList' => $callcenterList,
        'penaltyList' => $penaltyList,
    ])
]) ?>