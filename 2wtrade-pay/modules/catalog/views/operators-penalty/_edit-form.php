<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\modules\catalog\assets\CertificateNotNeededAsset;

/** @var app\modules\catalog\models\OperatorsPenalty $model */
/** @var app\modules\catalog\models\search\OperatorsPenaltySearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $callcenterList */
/** @var array $penaltyList */

ModalConfirmDeleteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['edit', 'id' => $model->id]),
        'method' => 'post',
        'options' => []
      ]);
?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'call_center_id')->select2List($callcenterList) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->select2List($penaltyList) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'penalty_value')->textInput() ?>
    </div>

</div>


<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить штраф') : Yii::t('common', 'Сохранить изменения')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?= ModalConfirmDelete::widget() ?>

