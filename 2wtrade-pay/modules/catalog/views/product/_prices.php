<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

ModalConfirmDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара')];

/** @var DataProvider $priceDataProvider */
?>

<?= Panel::widget([
    'title' => '',
    'nav' => new \app\modules\catalog\widgets\ProductLinkNav([
        'product' => $model
    ]),
    'content' => Panel::widget([
        'title' => Yii::t('common', 'Цены'),
        'actions' => DataProvider::renderSummary($priceDataProvider),
        'withBody' => false,
        'content' => GridView::widget([
            'dataProvider' => $priceDataProvider,
            'columns' => [
                [
                    'attribute' => Yii::t('common', 'country.name'),
                    'label' => Yii::t('common', 'Страна'),
                ],
                [
                    'attribute' => 'price',
                    'format' => ['decimal', 2],
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'currency_id',
                    'content' => function ($model) {
                        return $model->currency_id ? $model->currency->char_code . ' ' . Yii::t('common', $model->currency->name) : '-';
                    },
                    'enableSorting' => false,
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['/catalog/product-price/edit', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.productprice.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($price) {
                                return [
                                    'data-href' => Url::toRoute(['/catalog/product-price/delete', 'id' => $price->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.productprice.delete');
                            }
                        ],
                    ]
                ]
            ],
        ]),
        'footer' => (Yii::$app->user->can('catalog.productprice.edit') ?
                ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить цену'),
                    'url' => Url::toRoute(['/catalog/product-price/edit', 'product_id' => $model->id]),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ])
                : '') . LinkPager::widget(['pagination' => $priceDataProvider->getPagination()]),
    ]),
]) ?>

<?= \app\widgets\custom\ModalConfirmDelete::widget() ?>
