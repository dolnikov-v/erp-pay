<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\models\search\ProductSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список товаров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с товарами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'name',
            'sku',
            'source',
	        [
		        'attribute' => 'active',
		        'headerOptions' => ['class' => 'width-150 text-center'],
		        'contentOptions' => ['class' => 'text-center'],
		        'content' => function ($model) {
			        return Label::widget([
				        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
				        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
			        ]);
		        },
		        'enableSorting' => false,
	        ],
	        [
		        'attribute' => 'shell_on_market',
		        'headerOptions' => ['class' => 'width-150 text-center'],
		        'contentOptions' => ['class' => 'text-center'],
		        'content' => function ($model) {
			        return Label::widget([
				        'label' => $model->shell_on_market ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
				        'style' => $model->shell_on_market ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
			        ]);
		        },
	        ],
            'use_days',
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['view', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.product.view');
                        }
                    ],
	                [
		                'label' => Yii::t('common', 'Активировать'),
		                'url' => function ($model) {
			                return Url::toRoute(['activate', 'id' => $model->id]);
		                },
		                'can' => function ($model) {
			                return Yii::$app->user->can('catalog.product.activate') && !$model->active;
		                }
	                ],
	                [
		                'label' => Yii::t('common', 'Деактивировать'),
		                'url' => function ($model) {
			                return Url::toRoute(['deactivate', 'id' => $model->id]);
		                },
		                'can' => function ($model) {
			                return Yii::$app->user->can('catalog.product.deactivate') && $model->active;
		                }
	                ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.product.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Лендинги товара'),
                        'url' => function ($model) {
                            return Url::toRoute(['landings', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.product.landings');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Цены'),
                        'url' => function ($model) {
                            return Url::toRoute(['prices', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.product.prices');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Переводы'),
                        'url' => function ($model) {
                            return Url::toRoute(['translate', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.product.translate');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.product.add') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить товар'),
            'url' => Url::toRoute('add'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
            LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
