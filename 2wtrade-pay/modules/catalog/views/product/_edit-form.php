<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;

/** @var \app\models\Product $model */
/** @var \app\models\Source[] $sources */
/** @var \app\models\ProductCategory[] $categories */
/** @var bool $isView */

?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::toRoute(['add']) : Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'sku')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'use_days')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'shell_on_market')->checkbox(['disabled' => $isView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'weight')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'width')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'height')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'length')->textInput(['disabled' => $isView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'categoryList'/*, ['hidden' => $model->isNewRecord ? false : true]*/)
                ->select2ListMultiple($categories, [
                    //'disabled' => $model->isNewRecord ? false : true,
                    'value' => $model->isNewRecord ? null : array_keys($model->categoriesByProduct),
                    'disabled' => $isView
                ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'sources'/*, ['hidden' => $model->isNewRecord ? false : true]*/)
                ->select2ListMultiple($sources, [
                    'disabled' => $isView || (($model->isNewRecord || Yii::$app->user->can('catalog.product.edit.source')) ? false : true),
                    'value' => $model->isNewRecord ? null : array_keys($model->sources),
                ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'disabled' => $isView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php if (!$isView): ?>
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => Html::getInputName($model, 'imageFile'),
                ]),
            ]) ?>
            <br>
        <?php endif; ?>
        <?php if ($model->image): ?>
            <div class="catalog-image">
                <?= Html::img('/media/product/' . $model->image) ?>
                <?php if (!$isView): ?>
                    <?= Html::a(
                        Yii::t('common', 'Удалить'),
                        Url::toRoute(['/catalog/product/delete-image', 'id' => $model->id])
                    );
                    ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить товар') : Yii::t('common', 'Сохранить товар')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
