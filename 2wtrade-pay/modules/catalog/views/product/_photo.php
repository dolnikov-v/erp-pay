<?php

use app\widgets\Panel;
use yii\helpers\Url;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

ModalConfirmDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара')];

/** @var DataProvider $photoDataProvider */
?>

<?= Panel::widget([
    'title' => '',
    'nav' => new \app\modules\catalog\widgets\ProductLinkNav([
        'product' => $model
    ]),
    'content' => Panel::widget([
        'title' => Yii::t('common', 'Фото'),
        'actions' => DataProvider::renderSummary($photoDataProvider),
        'withBody' => false,
        'content' => GridView::widget([
            'dataProvider' => $photoDataProvider,
            'columns' => [
                [
                    'attribute' => Yii::t('common', 'country.name'),
                    'label' => Yii::t('common', 'Страна'),
                ],
                [
                    'attribute' => 'imageFile',
                    'label' => Yii::t('common', 'Изображение'),
                    'format' => 'image',
                    'contentOptions' => ['class' => 'image-50'],
                    'value' => function (\app\models\ProductPhoto $photo) {
                        return '/media/product/' . $photo->image;
                    }
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($photo) {
                                return Url::toRoute(['/catalog/product-photo/edit', 'id' => $photo->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.productphoto.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($photo) {
                                return [
                                    'data-href' => Url::toRoute(['/catalog/product-photo/delete', 'id' => $photo->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.productphoto.delete');
                            }
                        ],
                    ],
                ],
            ],
        ]),
        'footer' => (Yii::$app->user->can('catalog.productphoto.edit') ?
                ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить фото'),
                    'url' => Url::toRoute(['/catalog/product-photo/edit', 'product_id' => $model->id]),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ])
                : '') . LinkPager::widget(['pagination' => $photoDataProvider->getPagination()]),
    ]),
]) ?>

<?= \app\widgets\custom\ModalConfirmDelete::widget() ?>
