<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

ModalConfirmDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара')];

/** @var DataProvider $translateDataProvider */
?>

<?= Panel::widget([
    'title' => '',
    'nav' => new \app\modules\catalog\widgets\ProductLinkNav([
        'product' => $model
    ]),
    'content' => Panel::widget([
        'title' => Yii::t('common', 'Переводы'),
        'actions' => DataProvider::renderSummary($translateDataProvider),
        'withBody' => false,
        'content' => GridView::widget([
            'dataProvider' => $translateDataProvider,
            'columns' => [
                [
                    'attribute' => Yii::t('common', 'country.name'),
                    'label' => Yii::t('common', 'Страна'),
                ],
                [
                    'attribute' => 'name',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'language_id',
                    'content' => function ($model) {
                        return $model->language_id ? Yii::t('common', $model->language->name) : '-';
                    },
                    'enableSorting' => false,
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($translate) {
                                return Url::toRoute(['/catalog/product-translate/edit', 'id' => $translate->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.producttranslate.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($translate) {
                                return [
                                    'data-href' => Url::toRoute(['/catalog/product-translate/delete', 'id' => $translate->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('catalog.producttranslate.delete');
                            }
                        ],
                    ]
                ]
            ],
        ]),
        'footer' => (Yii::$app->user->can('catalog.producttranslate.edit') ?
                ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить перевод'),
                    'url' => Url::toRoute(['/catalog/product-translate/edit', 'product_id' => $model->id]),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ])
                : '') . LinkPager::widget(['pagination' => $translateDataProvider->getPagination()]),
    ]),
]) ?>

<?= \app\widgets\custom\ModalConfirmDelete::widget() ?>
