<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\catalog\assets\ProductAsset;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;

/** @var yii\web\View $this */
/** @var app\models\Product $model */
/** @var \app\models\Source[] $sources */
/** @var \app\models\ProductCategory[] $categories */
/** @var yii\data\ActiveDataProvider $translateDataProvider */
/** @var yii\data\ActiveDataProvider $priceDataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Просмотр товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/product/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Просмотр товара')];

ProductAsset::register($this);
?>

<?= Panel::widget([
    'title' => '',
    'nav' => new \app\modules\catalog\widgets\ProductLinkNav([
        'product' => $model,
        'isView' => true,
    ]),
    'content' => Panel::widget([
        'title' => Yii::t('common', 'Товар'),
        'content' => $this->render('_edit-form', [
            'model' => $model,
            'sources' => $sources,
            'categories' => $categories,
            'isView' => true,
        ])
    ]),
]) ?>