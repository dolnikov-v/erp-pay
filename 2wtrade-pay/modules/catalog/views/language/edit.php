<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\Language $model */
$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление языка') : Yii::t('common', 'Редактирование языка');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Языки'), 'url' => Url::toRoute('/catalog/language/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление языка') : Yii::t('common', 'Редактирование языка')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные языка'),
    'content' => $this->render('_edit-form', ['model' => $model]),
]); ?>
