<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\DateColumn;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Языки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Языки')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с языками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($data) {
                    return Yii::t('common', $data->name);
                },
            ],
            [
                'attribute' => 'locale',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.language.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.currency.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить язык'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
            LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


