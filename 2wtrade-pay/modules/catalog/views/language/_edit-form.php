<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\models\Language $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'locale')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить язык') : Yii::t('common', 'Сохранить язык')); ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
