<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class ATimerAsset
 * @package app\modules\catalog
 */
class ATimerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/atimer';

    public $js = [
        'jquery.atimer.js',
    ];

    public $depends = [
        'app\assets\vendor\MomentAsset',
        'app\assets\SiteAsset',
    ];
}
