<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class ProductAssetProductAsset
 * @package app\modules\catalog\assets
 */
class ProductAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/catalog';

    public $css = [
        'product.css'
    ];
}
