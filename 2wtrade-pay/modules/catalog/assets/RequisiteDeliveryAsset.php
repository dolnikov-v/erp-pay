<?php

namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class RequisiteDeliveryAsset
 * @package app\modules\catalog\assets
 */
class RequisiteDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/catalog/requisite-delivery/requisite-delivery';

    public $js = [
        'requisite-delivery.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}