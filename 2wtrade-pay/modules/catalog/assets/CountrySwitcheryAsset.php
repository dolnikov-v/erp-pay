<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package app\modules\catalog\assets
 */
class CountrySwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/catalog/country-switchery';

    public $js = [
        'country-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
