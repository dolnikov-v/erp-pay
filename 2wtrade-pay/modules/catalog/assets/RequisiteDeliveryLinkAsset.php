<?php

namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class RequisiteDeliveryLinkAsset
 * @package app\modules\catalog\assets
 */
class RequisiteDeliveryLinkAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/catalog/requisite-delivery/requisite-delivery-link';

    public $js = [
        'requisite-delivery-link.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}