<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class WorkflowSwitcheryAsset
 * @package app\modules\catalog\assets
 */
class WorkflowSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/catalog/workflow-switchery';

    public $js = [
        'workflow-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
