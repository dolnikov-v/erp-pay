<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class AutotrashSwitcheryAsset
 * @package app\modules\catalog\assets
 */
class AutotrashSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/catalog/autotrash-switchery';

    public $js = [
        'autotrash-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}