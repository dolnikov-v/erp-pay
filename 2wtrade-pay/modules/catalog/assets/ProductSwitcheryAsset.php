<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class ProductSwitcheryAsset
 * @package app\modules\catalog\assets
 */
class ProductSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/catalog/product-switchery';

    public $js = [
        'product-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
