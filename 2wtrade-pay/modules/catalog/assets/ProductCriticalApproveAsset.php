<?php

namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class ProductCriticalApproveAsset
 * @package app\modules\catalog\assets
 */
class ProductCriticalApproveAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/catalog/country';

    public $js = [
        'product-critical-approve.js'
    ];

    public $css = [
        'product-critical-approve.css'
    ];
}