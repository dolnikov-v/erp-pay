<?php
namespace app\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class BuyoutGoodsAsset
 * @package app\modules\access\assets
 */
class CertificateNotNeededAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/catalog/certificate';

    public $js = [
        'certificate_not_needed.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}