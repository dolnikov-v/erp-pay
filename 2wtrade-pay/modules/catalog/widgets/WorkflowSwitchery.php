<?php
namespace app\modules\catalog\widgets;

use app\widgets\Switchery;

/**
 * Class WorkflowSwitchery
 * @package app\modules\catalog\widgets
 */
class WorkflowSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('workflow-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'workflow-switchery',
                ],
            ]),
        ]);
    }
}
