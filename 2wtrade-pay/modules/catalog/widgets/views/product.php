<?php
use app\modules\catalog\widgets\ProductSwitchery;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\models\Partner || \app\models\Source $model */
/** @var array $productsByModel */
/** @var \app\models\Product[] $productList */
/** @var string $controller */
?>
<table class="table table-striped table-hover" data-model-id="<?= $model->id ?>" data-controller="<?= $controller ?>">
    <?php if (!empty($productList)) : ?>
        <thead>
        <tr>
            <th style="width: 100px"><?= Yii::t('common', 'Ид') ?></th>
            <th><?= Yii::t('common', 'Продукт') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($productList as $product) : ?>
            <tr>
                <td><?= Yii::t('common', $product->id) ?></td>
                <td><?= Yii::$app->user->can('catalog.product.edit') ? Html::a(Yii::t('common', $product->name), Url::to([
                        'catalog/product/edit',
                        'id' => $product->id
                    ])) : Yii::t('common', $product->name) ?></td>
                <td class="text-center" data-product-id="<?= $product->id ?>">
                    <?= ProductSwitchery::widget([
                        'checked' => in_array($product->id, $productsByModel),
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Продукты отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
