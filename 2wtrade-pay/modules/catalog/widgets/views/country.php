<?php
use app\modules\catalog\widgets\CountrySwitchery;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\models\Partner || \app\models\Source $model */
/** @var array $countriesByModel */
/** @var string $controller */
/** @var \app\models\Country[] $countryList */
?>
<table class="table table-striped table-hover" data-model-id="<?= $model->id ?>" data-controller="<?= $controller ?>">
    <?php if (!empty($countryList)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Страна') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($countryList as $country) : ?>
            <tr>
                <td><?= Yii::$app->user->can('catalog.country.edit') ? Html::a(Yii::t('common', $country->name), Url::to([
                        '/catalog/country/edit',
                        'id' => $country->id
                    ])) : Yii::t('common', $country->name) ?></td>
                <td class="text-center" data-country-id="<?= $country->id ?>">
                    <?= CountrySwitchery::widget([
                        'checked' => in_array($country->id, $countriesByModel),
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Страны отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
