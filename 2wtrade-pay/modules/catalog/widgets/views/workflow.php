<?php
use app\modules\catalog\widgets\WorkflowSwitchery;
use app\modules\order\models\OrderWorkflow;
use yii\helpers\Url;

/** @var app\models\Country $model */
/** @var OrderWorkflow[] $workflows */
/** @var OrderWorkflow[] $workflowsByCountry */
/** @var string $url */
?>
<table class="table table-striped table-hover" data-country-id="<?= $model->id ?>" data-request-url="<?= $url ?>">
    <thead>
    <tr>
        <th><?= Yii::t('common', 'Название') ?></th>
        <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($workflows)) : ?>
        <?php foreach ($workflows as $workflow) : ?>
            <tr>
                <td><?= Yii::t('common', $workflow->name) ?></td>
                <td class="text-center" data-workflow-id="<?= $workflow->id ?>">
                    <?= WorkflowSwitchery::widget([
                        'checked' => call_user_func(function () use ($workflow, $workflowsByCountry) {
                            foreach ($workflowsByCountry as $workflowByCountry) {
                                if ($workflowByCountry->id == $workflow->id) {
                                    return true;
                                }
                            }

                            return false;
                        }),
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="2"><?= Yii::t('common', 'Workflows отсутствуют') ?></td>
        </tr>
    <?php endif ?>
    </tbody>
</table>
