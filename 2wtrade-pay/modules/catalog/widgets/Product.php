<?php
namespace app\modules\catalog\widgets;

use Yii;
use yii\base\Widget;

/**
 * Class Product
 * @package app\modules\catalog\widgets
 */
class Product extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var \app\models\Product[]
     */
    public $productList;

    /**
     * @var
     */
    public $productsByModel;

    /**
     * @var
     */
    public $controller;


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('product', [
            'model' => $this->model,
            'controller' => $this->controller,
            'productList' => $this->productList,
            'productsByModel' => $this->productsByModel,
        ]);
    }
}
