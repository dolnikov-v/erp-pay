<?php
namespace app\modules\catalog\widgets;

use Yii;
use yii\base\Widget;

/**
 * Class Country
 * @package app\modules\catalog\widgets
 */
class Country extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var \app\models\Country[]
     */
    public $countryList;

    /**
     * @var
     */
    public $countriesByModel;

    /**
     * @var
     */
    public $controller;


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('country', [
            'model' => $this->model,
            'controller' => $this->controller,
            'countryList' => $this->countryList,
            'countriesByModel' => $this->countriesByModel,
        ]);
    }
}
