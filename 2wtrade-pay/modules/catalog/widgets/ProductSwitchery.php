<?php
namespace app\modules\catalog\widgets;

use app\widgets\Switchery;

/**
 * Class ProductSwitchery
 * @package app\modules\catalog\widgets
 */
class ProductSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('product-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'product-switchery',
                ],
            ]),
        ]);
    }
}
