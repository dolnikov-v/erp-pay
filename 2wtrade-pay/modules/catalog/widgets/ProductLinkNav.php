<?php

namespace app\modules\catalog\widgets;

use app\widgets\Nav;
use Yii;
use yii\helpers\Url;
use app\models\Product;

/**
 * Class ProductLinkNav
 * @package app\modules\catalog\widgets
 */
class ProductLinkNav extends Nav
{
    /**
     * @var Product;
     */
    public $product;

    /**
     * @var bool
     */
    public $isView = false;

    public function init()
    {
        $actionId = Yii::$app->controller->action->id;

        $this->tabs = [];

        $showView = false;
        if ($this->isView || (Yii::$app->user->can('catalog.product.view') && !Yii::$app->user->can('catalog.product.edit'))) {
            $showView = true;
        }

        if ($showView) {
            $this->tabs[] = [
                'label' => Yii::t('common', 'Общее'),
                'url' => Url::toRoute(['view', 'id' => $this->product->id]),
                'can' => Yii::$app->user->can('catalog.product.view'),
                'active' => $actionId == 'view'
            ];
        }
        else {
            $this->tabs[] = [
                'label' => Yii::t('common', 'Общее'),
                'url' => Url::toRoute(['edit', 'id' => $this->product->id]),
                'can' => Yii::$app->user->can('catalog.product.edit'),
                'active' => $actionId == 'edit'
            ];
        }

        $this->tabs[] = [
            'label' => Yii::t('common', 'Цены'),
            'url' => Url::toRoute(['prices', 'id' => $this->product->id]),
            'can' => Yii::$app->user->can('catalog.product.prices') && $this->product->id,
            'active' => $actionId == 'prices'
        ];
        $this->tabs[] = [
            'label' => Yii::t('common', 'Переводы'),
            'url' => Url::toRoute(['translate', 'id' => $this->product->id]),
            'can' => Yii::$app->user->can('catalog.product.translate') && $this->product->id,
            'active' => $actionId == 'translate'
        ];
        $this->tabs[] = [
            'label' => Yii::t('common', 'Фото'),
            'url' => Url::toRoute(['photo', 'id' => $this->product->id]),
            'can' => Yii::$app->user->can('catalog.product.photo') && $this->product->id,
            'active' => $actionId == 'photo'
        ];
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('product-link-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return '';
    }
}
