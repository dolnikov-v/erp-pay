<?php
namespace app\modules\catalog\widgets;

use app\models\Country;
use app\modules\order\models\OrderWorkflow;
use yii\base\Widget;

/**
 * Class Workflow
 * @package app\modules\catalog\widgets
 */
class Workflow extends Widget
{
    /**
     * @var Country
     */
    public $model;

    /**
     * @var OrderWorkflow[]
     */
    public $workflows;

    /**
     * @var OrderWorkflow[]
     */
    public $workflowsByCountry;

    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('workflow', [
            'model' => $this->model,
            'workflows' => $this->workflows,
            'workflowsByCountry' => $this->workflowsByCountry,
            'url' => $this->url,
        ]);
    }
}
