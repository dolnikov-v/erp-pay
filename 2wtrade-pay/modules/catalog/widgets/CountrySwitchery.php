<?php
namespace app\modules\catalog\widgets;

use app\widgets\Switchery;

/**
 * Class CountrySwitchery
 * @package app\modules\catalog\widgets
 */
class CountrySwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('country-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'country-switchery',
                ],
            ]),
        ]);
    }
}
