<?php

namespace app\modules\catalog\models;

use app\modules\catalog\models\query\UnBuyoutReasonQuery;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "unbuyout_reason".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UnBuyoutReasonMapping[] $foreignReasons
 * @property DeliveryRequestUnBuyoutReason[] $deliveryRequestUnBuyoutReasons
 */
class UnBuyoutReason extends ActiveRecordLogUpdateTime
{

    const ADDRESS_ERROR_ID = 8;
    const CLIENT_DID_NOT_ORDER_ID = 11;

    const CATEGORY_EMPTY = null;
    const CATEGORY_CLIENT = 1;
    const CATEGORY_PRODUCT = 2;
    const CATEGORY_FIN = 3;
    const CATEGORY_DELIVERY = 4;
    const CATEGORY_OPERATOR = 5;
    const CATEGORY_TM = 6;
    const CATEGORY_OTHER = 7;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%unbuyout_reason}}';
    }

    /**
     * @inheritdoc
     * @return UnBuyoutReasonQuery
     */
    public static function find()
    {
        return new UnBuyoutReasonQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'created_at', 'updated_at', 'category'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'category' => Yii::t('common', 'Категория'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return array
     */
    public static function getCategories()
    {
        return [
            self::CATEGORY_EMPTY => '-',
            self::CATEGORY_CLIENT => Yii::t('common', 'Клиент'),
            self::CATEGORY_PRODUCT => Yii::t('common', 'Товар'),
            self::CATEGORY_FIN => Yii::t('common', 'Финансы'),
            self::CATEGORY_DELIVERY => Yii::t('common', 'Проблемы КС'),
            self::CATEGORY_OPERATOR => Yii::t('common', 'Работа оператора'),
            self::CATEGORY_TM => Yii::t('common', 'ТМ'),
            self::CATEGORY_OTHER => Yii::t('common', 'Другое'),
        ];
    }

    public function beforeDelete()
    {
        if ($this->foreignReasons) {
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignReasons()
    {
        return $this->hasMany(UnBuyoutReasonMapping::className(), ['reason_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequestUnBuyoutReasons()
    {
        return $this->hasMany(DeliveryRequestUnBuyoutReason::className(), ['reason_id' => 'id']);
    }
}
