<?php

namespace app\modules\catalog\models\search;

use app\modules\catalog\models\RequisiteDelivery;
use yii\data\ActiveDataProvider;

/**
 * Class RequisiteDeliverySearch
 * @package app\modules\catalog\models\search
 */
class RequisiteDeliverySearch extends RequisiteDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city',  'beneficiary_bank', 'bank_account', 'swift_code'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = RequisiteDelivery::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->name) {
            $query->andWhere(['like', RequisiteDelivery::tableName() . '.name', $this->name]);
        }

        if ($this->city) {
            $query->andWhere(['like', RequisiteDelivery::tableName() . '.city', $this->city]);
        }

        if ($this->beneficiary_bank) {
            $query->andWhere(['like', RequisiteDelivery::tableName() . '.beneficiary_bank', $this->beneficiary_bank]);
        }

        if ($this->swift_code) {
            $query->andWhere(['like', RequisiteDelivery::tableName() . '.swift_code', $this->swift_code]);
        }

        return $dataProvider;
    }
}