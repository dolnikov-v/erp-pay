<?php
namespace app\modules\catalog\models\search;

use app\models\Country;
use app\modules\order\models\SmsPollQuestions;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class SmsPollQuestionsSearch
 * @package app\models\search
 */
class SmsPollQuestionsSearch extends SmsPollQuestions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['question_text', 'country_id'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = SmsPollQuestions::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }
        
        $query->with(['country']);
        $query->andFilterWhere([SmsPollQuestions::tableName() . '.id' => $this->id]);
        $query->andFilterWhere(['like', 'question_text', $this->question_text]);
        $query->andFilterWhere(['country_id' => $this->country_id]);
        $query->active();

        return $dataProvider;
    }

}