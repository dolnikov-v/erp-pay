<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 20.03.2018
 * Time: 13:45
 */

namespace app\modules\catalog\models\search;


use app\modules\catalog\models\ExternalSource;
use Yii;
use yii\data\ActiveDataProvider;

class ExternalSourceSearch extends ExternalSource
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'description'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = ExternalSource::find()
            ->joinWith('externalSourceRole')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->name) {
            $query->andWhere(['like', ExternalSource::tableName() . '.name', $this->name]);
        }

        if ($this->url) {
            $query->andWhere(['like', 'url', $this->url]);
        }

        if ($this->description) {
            $query->andWhere(['like', ExternalSource::tableName() . '.description', $this->description]);
        }

        return $dataProvider;
    }
}