<?php
namespace app\modules\catalog\models\search;

use app\modules\catalog\models\Autotrash;
use Yii;
use yii\data\ActiveDataProvider;


/**
 * Class AutotrashSearch
 * @package app\modules\catalog\models\search
 */
class AutotrashSearch extends Autotrash
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template', 'field', 'filter'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = Autotrash::find()
            ->where([
                'or',
                ['country_id' => Yii::$app->user->country->id],
                ['country_id' => null]      // старые правила пусть видят все и правят кому надо
            ])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Autotrash::tableName() . '.field' => $this->field]);
        $query->andFilterWhere([Autotrash::tableName() . '.filter' => $this->filter]);
        $query->andFilterWhere(['like', Autotrash::tableName() . '.template', $this->template]);

        return $dataProvider;
    }
}
