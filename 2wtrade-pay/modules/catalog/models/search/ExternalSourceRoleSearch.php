<?php

namespace app\modules\catalog\models\search;

use app\modules\catalog\models\ExternalSourceRole;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class ExternalSourceRolesSearch
 * @package app\modules\catalog\models\search
 */
class ExternalSourceRoleSearch extends ExternalSourceRole
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['external_source_id', 'name', 'description'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['external_source_id'] = SORT_DESC;
        }

        $query = ExternalSourceRole::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->external_source_id){
            $query->andWhere(['external_source_id' => $this->external_source_id]);
        }

        if($this->name){
            $query->andWhere(['like', 'name', $this->name]);
        }

        if($this->description){
            $query->andWhere(['like', 'description', $this->description]);
        }

        return $dataProvider;
    }
}