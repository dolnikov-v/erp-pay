<?php
namespace app\modules\catalog\models\search;

use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use yii\data\ActiveDataProvider;
use Yii;


/**
 * Class UnBuyoutReasonSearch
 * @package app\modules\catalog\models\search
 */
class UnBuyoutReasonSearch extends UnBuyoutReason
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Причина'),
            'category' => Yii::t('common', 'Категория'),
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = UnBuyoutReason::find()
            ->distinct()
            ->joinWith('foreignReasons')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'or',
            ['like', UnBuyoutReason::tableName() . '.name', $this->name],
            ['like', UnBuyoutReasonMapping::tableName() . '.foreign_reason', $this->name],
        ]);

        if ($this->category) {
            $query->andWhere([UnBuyoutReason::tableName() . '.category' => $this->category]);
        }


        return $dataProvider;
    }
}
