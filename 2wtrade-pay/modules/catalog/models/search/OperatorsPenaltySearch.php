<?php
namespace app\modules\catalog\models\search;

use app\modules\catalog\models\OperatorsPenalty;
use Yii;
use yii\data\ActiveDataProvider;


/**
 * Class OperatorsPenaltySearch
 * @package app\modules\catalog\models\search
 */
class OperatorsPenaltySearch extends OperatorsPenalty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'call_center_id', 'name', 'penalty_value'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = OperatorsPenalty::find()
            ->orderBy($sort);

        $query->andFilterWhere([OperatorsPenalty::tableName() . '.country_id' => Yii::$app->user->country->id]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
