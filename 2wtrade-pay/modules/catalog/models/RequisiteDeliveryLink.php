<?php

namespace app\modules\catalog\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\catalog\models\query\RequisiteDeliveryLinkQuery;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use Yii;

/**
 * This is the model class for table "requisite_delivery_link".
 * Class RequisiteDeliveryLink
 * @package app\modules\catalog\models

 * @property integer $id
 * @property integer $requisite_delivery_id
 * @property integer $country_id
 * @property integer $delivery_id
 * @property integer $delivery_contract_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property DeliveryContract $deliveryContract
 * @property Delivery $delivery
 * @property RequisiteDelivery $requisiteDelivery
 */
class RequisiteDeliveryLink extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%requisite_delivery_link}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requisite_delivery_id', 'country_id', 'delivery_id', 'delivery_contract_id', 'created_at', 'updated_at'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['delivery_contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryContract::className(), 'targetAttribute' => ['delivery_contract_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['requisite_delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequisiteDelivery::className(), 'targetAttribute' => ['requisite_delivery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'requisite_delivery_id' => Yii::t('common', 'Реквизит'),
            'country_id' => Yii::t('common', 'Страна'),
            'delivery_contract_id' => Yii::t('common', 'Контракт'),
            'delivery_id' => Yii::t('common', 'Курьерская служба'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
        ];
    }

    /**
     * @return RequisiteDeliveryLinkQuery
     */
    public static function find()
    {
        return new RequisiteDeliveryLinkQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisiteDelivery()
    {
        return $this->hasOne(RequisiteDelivery::className(), ['id' => 'requisite_delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContract()
    {
        return $this->hasOne(DeliveryContract::className(), ['id' => 'delivery_contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}