<?php

namespace app\modules\catalog\models;

use app\modules\catalog\models\query\RequisiteDeliveryQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use yii\db\Exception;

/**
 * This is the model class for table "requisite_delivery".
 *
 * Class RequisiteDelivery
 * @package app\modules\catalog\models
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $city
 * @property string $bank_address
 * @property string $beneficiary_bank
 * @property string $bank_account
 * @property string $swift_code
 * @property integer $invoice_tpl
 * @property integer $created_at
 * @property integer $updated_at
 * @property RequisiteDeliveryLink[] $requisiteDeliveryLinks
 */
class RequisiteDelivery extends ActiveRecordLogUpdateTime
{
    const INVOICE_TPL_NUM_2WTRADE_EUR = 1;
    const INVOICE_TPL_NUM_2WTRADE_USD = 2;
    const INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP_ASIA = 4;
    const INVOICE_TPL_NUM_ASAP_UNION_LLP = 3;

    const INVOICE_TPL_NAME_2WTRADE_EUR = '2Wtrade LLP EUR';
    const INVOICE_TPL_NAME_2WTRADE_USD = '2Wtrade LLP USD';
    const INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP = '2Wtrade LLP USD (no stamp)';
    const INVOICE_TPL_NAME_ASAP_UNION_LLP = 'ASAP Union LLP';

    const STAMP_ASAP = '/invoice/stamps/asap-union-llp.png';
    const STAMP_2WTRADE = '/invoice/stamps/2wtrade-llp.png';
    const STAMP_EMPTY = '/invoice/stamps/empty.png';

    const ASAP_UNION_LOGO = '/invoice/asap-union-logo.png';

    const SIGNER_DEFAULT = 'Jeffersson Duque';
    const SIGNER_ASIA = 'Department Asia Director Muraviev A.';

    public $requisite_delivery_link;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%requisite_delivery}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'address', 'city', 'bank_address', 'beneficiary_bank', 'bank_account', 'swift_code'], 'required'],
            [['name', 'address', 'city', 'bank_address', 'beneficiary_bank', 'bank_account', 'swift_code'], 'string'],
            [['invoice_tpl', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Наименование компании'),
            'address' => Yii::t('common', 'Адрес'),
            'city' => Yii::t('common', 'Город'),
            'bank_address' => Yii::t('common', 'Адрес банка'),
            'bank_account' => Yii::t('common', '№ счета'),
            'beneficiary_bank' => Yii::t('common', 'Банк получателя'),
            'swift_code' => Yii::t('common', 'SWIFT код'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
            'requisite_delivery_link' => Yii::t('common', 'Контракты'),
            'invoice_tpl' => Yii::t('common', 'Шаблон инвойса')
        ];
    }

    /**
     * @return string
     */
    public static function getTemplatesPath()
    {
        return yii::getAlias('@new_templates');
    }

    /**
     * @return array
     */
    public static function getInvoiceTPLSCollection()
    {
        return [
            self::INVOICE_TPL_NUM_2WTRADE_EUR => self::INVOICE_TPL_NAME_2WTRADE_EUR,
            self::INVOICE_TPL_NUM_2WTRADE_USD => self::INVOICE_TPL_NAME_2WTRADE_USD,
            self::INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP_ASIA => self::INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP,
            self::INVOICE_TPL_NUM_ASAP_UNION_LLP => self::INVOICE_TPL_NAME_ASAP_UNION_LLP
        ];
    }

    /**
     * @return array
     */
    public function InvoiceSTAMPScollection()
    {
        return [
            self::INVOICE_TPL_NUM_2WTRADE_EUR => self::getTemplatesPath() . self::STAMP_2WTRADE,
            self::INVOICE_TPL_NUM_2WTRADE_USD => self::getTemplatesPath() . self::STAMP_2WTRADE,
            self::INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP_ASIA => self::getTemplatesPath() . self::STAMP_EMPTY,
            self::INVOICE_TPL_NUM_ASAP_UNION_LLP => self::getTemplatesPath() . self::STAMP_ASAP
        ];
    }

    /**
     * @return string
     */
    public function getAsapUnionLogo()
    {
        return self::getTemplatesPath() . self::ASAP_UNION_LOGO;
    }

    /**
     * @return array
     */
    public function requisiteDataCollection()
    {
        return [
            self::INVOICE_TPL_NUM_2WTRADE_EUR => [
                'name' => "2WTRADE LLP",
                'city' => "London",
                'address' => "71-75 Shelton Street Covent Garden, London, WC2H9JQ, UNITED KINGDOM",
                'bank_account' => "LI8408801201622100814",
                'beneficiary_bank' => "BANK ALPINUM AG",
                'bank_address' => "VADUZ 17 STAEDTLE 9490, Liechtenstein",
                'swift_code' => "BALPLI22"
            ],
            self::INVOICE_TPL_NUM_2WTRADE_USD => [
                'name' => "2WTRADE LLP",
                'city' => "London",
                'address' => "71-75 Shelton Street Covent Garden, London, WC2H9JQ, UNITED KINGDOM",
                'bank_account' => "LI39 08801201622101333",
                'beneficiary_bank' => "BANK ALPINUM AG",
                'bank_address' => "VADUZ 17 STAEDTLE 9490",
                'swift_code' => "BALPLI22"
            ],
            self::INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP_ASIA => [
                'name' => "2WTRADE LLP",
                'city' => "London",
                'address' => "71-75 Shelton Street Covent Garden, London, WC2H9JQ, UNITED KINGDOM",
                'bank_account' => "LI39 08801201622101333",
                'beneficiary_bank' => "BANK ALPINUM AG",
                'bank_address' => "VADUZ 17 STAEDTLE 9490",
                'swift_code' => "BALPLI22"
            ],
            self::INVOICE_TPL_NUM_ASAP_UNION_LLP => [
                'name' => "Asap UnionLLP",
                'city' => "London",
                'address' => "71-75 Shelton StreetCovent Garden, London, WC2H9JQUNITED KINGDOM",
                'bank_account' => "CH6908822106458465000",
                'beneficiary_bank' => "CIMBANK",
                'bank_address' => "16 Rue Merle RUE MERLE D'AUBIGNE 1207 Geneve Suisse",
                'swift_code' => "CIMMCHGG"
            ]
        ];
    }

    /**
     * @param $template
     * @return bool|mixed
     */
    public function getStampByTemplate($template)
    {
        $collection = $this->InvoiceSTAMPScollection();

        return isset($collection[$template]) ? $collection[$template] : false;
    }

    /**
     * @return string
     */
    public function getSigner(): string
    {
        if ($this->invoice_tpl == self::INVOICE_TPL_NAME_2WTRADE_USD_NO_STAMP_ASIA) {
            return self::SIGNER_ASIA;
        }
        return self::SIGNER_DEFAULT;
    }

    /**
     * @return RequisiteDeliveryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RequisiteDeliveryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisiteDeliveryLink()
    {
        return $this->hasMany(RequisiteDeliveryLink::className(), ['requisite_delivery_id' => 'id']);
    }

}