<?php

namespace app\modules\catalog\models;

use app\modules\catalog\models\query\ExternalSourceRolesQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class ExternalSourceRole
 * @package app\modules\catalog\models
 */
class ExternalSourceRole extends ActiveRecordLogUpdateTime
{
    const STATE_ADD = 1;
    const STATE_REMOVE = 0;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%external_source_role}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['external_source_id', 'name', 'description'], 'required'],
            [['external_source_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'external_source_id' => Yii::t('common', 'Источник'),
            'name' => Yii::t('common', 'Наименование роли'),
            'description' => Yii::t('common', 'Описание'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternalSource()
    {
        return $this->hasOne(ExternalSource::className(), ['id' => 'external_source_id']);
    }

    /**
     * @return ExternalSourceRolesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExternalSourceRolesQuery(get_called_class());
    }
}