<?php
namespace app\modules\catalog\models\query;

use app\components\db\ActiveQuery;
use app\modules\catalog\models\SmsPollQuestions;

/**
 * Class SmsPollQuestionsQuery
 * @package app\models\query
 *
 * @method SmsPollQuestions one($db = null)
 * @method SmsPollQuestions[] all($db = null)
 */
class SmsPollQuestionsQuery extends ActiveQuery
{
    const IS_ACTIVE = 1;
    const IS_NOT_ACTIVE = 0;

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([SmsPollQuestions::tableName() . '.is_active' => self::IS_ACTIVE]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([SmsPollQuestions::tableName() . '.is_active' => self::IS_NOT_ACTIVE]);
    }
}