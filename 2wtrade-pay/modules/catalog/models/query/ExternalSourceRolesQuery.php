<?php
namespace app\modules\catalog\models\query;

use \yii\db\ActiveQuery;

/**
 * Class ExternalSourceRolesQuery
 * @package app\modules\catalog\models\query
 */
class ExternalSourceRolesQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function bySource($external_source_id)
    {
        return $this->andWhere(['external_source_id' => $external_source_id]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}