<?php
namespace app\modules\catalog\models\query;

use app\components\db\ActiveQuery;
use app\modules\catalog\models\UnBuyoutReason;

/**
 * Class BonusTypeQuery
 * @package app\modules\salary\models\query
 * @method UnBuyoutReason one($db = null)
 * @method UnBuyoutReason[] all($db = null)
 */
class UnBuyoutReasonQuery extends ActiveQuery
{

}
