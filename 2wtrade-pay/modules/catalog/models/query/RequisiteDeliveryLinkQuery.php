<?php

namespace app\modules\catalog\models\query;

use app\modules\catalog\models\RequisiteDeliveryLink;
use yii\db\ActiveQuery;

/**
 * Class RequisiteDeliveryLinkQuery
 * @package app\modules\catalog\models\query
 *
 * @method RequisiteDeliveryLink one($db = null)
 * @method RequisiteDeliveryLink[] all($db = null)
 */
class RequisiteDeliveryLinkQuery extends ActiveQuery
{
    /**
     * @param integer $contractId
     * @return $this
     */
    public function byContractId($contractId)
    {
        if ($contractId) {
            return $this->andWhere([RequisiteDeliveryLink::tableName() . '.delivery_contract_id' => $contractId]);
        }
        return $this;
    }
}