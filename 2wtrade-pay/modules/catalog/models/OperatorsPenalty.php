<?php

namespace app\modules\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\catalog\models\query\OperatorsPenaltyQuery;
use app\modules\callcenter\models\CallCenter;

/**
 * @property integer $id
 * @property integer $country_id
 * @property integer $call_center_id
 * @property string $name
 * @property double $penalty_value
 * @property integer $created_at
 * @property integer $updated_at
 */

class OperatorsPenalty extends ActiveRecord
{

    const INVALID_ADDRESS = 'invalid address';

    public $callCenters = [];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_operators_penalty}}';
    }


    public function init()
    {
        $callcenters = CallCenter::find()
            ->select([
                'id' => CallCenter::tableName() .'.id',
                'name' => CallCenter::tableName() .'.name',
            ])
            ->where(['=', CallCenter::tableName() .'.country_id', Yii::$app->user->country->id])
            ->asArray()
            ->all();

        foreach ($callcenters as $cc) {
            $this->callCenters[$cc['id']] = $cc['name'];
        }
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'call_center_id', 'name', 'created_at', 'updated_at'], 'required'],
            [['country_id', 'call_center_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['penalty_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Номер страны'),
            'call_center_id' => Yii::t('common', 'Номер колл-центра'),
            'name' => Yii::t('common', 'Тип штрафа'),
            'penalty_value' => Yii::t('common', 'Сумма штрафа, $'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
        ];
    }

    /**
     * @inheritdoc
     * @return OperatorsPenaltyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OperatorsPenaltyQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function getCallCenterCollection()
    {
        return $this->callCenters;
    }

    /**
     * @return array
     */
    public static function getPenaltyTypeCollection()
    {
        return [
            self::INVALID_ADDRESS => Yii::t('common', 'Внесен ошибочный адрес доставки'),
        ];
    }

}
