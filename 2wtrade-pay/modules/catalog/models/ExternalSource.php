<?php

namespace app\modules\catalog\models;

use app\modules\catalog\models\query\ExternalSourceQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class ExternalSource
 * @package app\modules\catalog\models
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property string $outbound_amazon_queue_name
 * @property string $incoming_amazon_queue_name
 */
class ExternalSource extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%external_source}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'url', 'description', 'outbound_amazon_queue_name', 'incoming_amazon_queue_name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'description', 'incoming_amazon_queue_name', 'outbound_amazon_queue_name'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Наименование'),
            'url' => Yii::t('common', 'Url источника'),
            'description' => Yii::t('common', 'Описание'),
            'count_roles' => Yii::t('common', 'Количество ролей'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
            'outbound_amazon_queue_name' => Yii::t('common', 'Название исходящей очереди Амазон'),
            'incoming_amazon_queue_name' => Yii::t('common', 'Название входящей очереди Amazon'),
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCollectionExternalSources()
    {
        return ExternalSource::find()
            ->joinWith('externalSourceRole')
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternalSourceRole()
    {
        return $this->hasMany(ExternalSourceRole::className(), ['external_source_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ExternalSourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExternalSourceQuery(get_called_class());
    }
}