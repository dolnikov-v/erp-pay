<?php
namespace app\modules\catalog\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use Yii;

/**
 * Class SmsPollQuestions
 * NPAY-476
 *
 * @package app\modules\catalog\models
 * @property integer $id
 * @property integer $country_id
 * @property string $question_text
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 */
class SmsPollQuestions extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_poll_questions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'question_text'], 'required'],
            ['country_id', 'integer'],
            ['question_text', 'text'],
            ['is_active', 'integer'],
            ['created_at', 'integer'],
            ['updated_at', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'question_text' => Yii::t('common', 'Текст опроса'),
            'country_name' => Yii::t('common', 'Страна')
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}