<?php

namespace app\modules\catalog\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\Product;
use Yii;

/**
 * This is the model class for table "product_critical_approve".
 * Class ProductCriticalApprove
 * @package app\modules\catalog\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $product_id
 * @property integer $percent
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property Product $product
 */
class ProductCriticalApprove extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_critical_approve}}';
    }

    public function beforeSave($insert)
    {
        if(is_null($this->country_id)){
            return false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'product_id', 'percent', 'created_at', 'updated_at'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'percent' => Yii::t('common', 'Процент'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}