<?php
namespace app\modules\catalog\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use Yii;

/**
 * Class Autotrash
 * Модель справочника фильтров "стоповых" фраз для заказов
 * Авто треш NPAY-724
 *
 * @package app\modules\catalog\models
 * @property integer $id
 * @property string $template
 * @property string $field
 * @property string $filter
 * @property integer $is_active
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 */
class Autotrash extends ActiveRecordLogUpdateTime
{
    const FILTER_MAX_LENGTH = 'max_length';
    const FILTER_MIN_LENGTH = 'min_length';
    const FILTER_REGEXP = 'regexp';
    const FILTER_PHONE = 'phone';

    const FIELD_CUSTOMER_FULL_NAME = 'customer_full_name';
    const FIELD_CUSTOMER_IP = 'customer_ip';
    const FIELD_CUSTOMER_PHONE = 'customer_phone';
    const FIELD_CUSTOMER_ADDRESS = 'customer_address';

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auto_trash}}';
    }

    /**
     * @return []
     */
    public static function getFiltersCollection()
    {
        return [
            self::FILTER_MAX_LENGTH => Yii::t('common', 'Максимальная длина'),
            self::FILTER_MIN_LENGTH => Yii::t('common', 'Минимальная длина'),
            self::FILTER_REGEXP => Yii::t('common', 'Регулярное выражение'),
            self::FILTER_PHONE => Yii::t('common', 'Номер телефона')
        ];
    }

    /**
     * @return []
     */
    public static function getFieldsCollection()
    {
        return [
            self::FIELD_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя'),
            self::FIELD_CUSTOMER_IP => Yii::t('common', 'IP адрес'),
            self::FIELD_CUSTOMER_PHONE => Yii::t('common', 'Телефон'),
            self::FIELD_CUSTOMER_ADDRESS => Yii::t('common', 'Адрес'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template', 'field', 'filter'], 'required'],
            [['field'], 'string'],
            [['template'], 'string', 'max' => 255],
            [['filter'], 'string', 'max' => 25],
            ['filter', 'in', 'range' => array_keys(self::getFiltersCollection())],
            ['field', 'in', 'range' => array_keys(self::getFieldsCollection())],
            [['is_active', 'country_id', 'created_at', 'updated_at'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'template' => Yii::t('common', 'Шаблон'),
            'field' => Yii::t('common', 'Поле заказа'),
            'filter' => Yii::t('common', 'Правило'),
            'is_active' => Yii::t('common', 'Активно'),
            'country_id' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}