<?php

namespace app\modules\catalog\models;

use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "unbuyout_reason_mapping".
 *
 * @property integer $id
 * @property integer $reason_id
 * @property string $foreign_reason
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UnBuyoutReason $reason
 * @property DeliveryRequestUnBuyoutReason[] $deliveryRequestUnBuyoutReasons
 */
class UnBuyoutReasonMapping extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%unbuyout_reason_mapping}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['foreign_reason', 'required'],
            ['foreign_reason', 'unique'],
            [['reason_id', 'created_at', 'updated_at'], 'integer'],
            [['foreign_reason'], 'string', 'max' => 255],
            [['reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnBuyoutReason::className(), 'targetAttribute' => ['reason_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'reason_id' => Yii::t('common', 'Причина'),
            'foreign_reason' => Yii::t('common', 'Причина в КС'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    public function beforeDelete()
    {
        if ($this->deliveryRequestUnBuyoutReasons) {
            $this->addError('id', Yii::t('common', 'Нельзя удалить причину, т.к. она есть в заказах'));
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(UnBuyoutReason::className(), ['id' => 'reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequestUnBuyoutReasons()
    {
        return $this->hasMany(DeliveryRequestUnBuyoutReason::className(), ['reason_mapping_id' => 'id']);
    }
}
