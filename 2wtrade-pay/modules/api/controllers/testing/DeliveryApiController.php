<?php
namespace app\modules\api\controllers\testing;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\api\models\testing\DeliveryApi;
use HttpInvalidParamException;
use Yii;

class DeliveryApiController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'execute'
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new DeliveryApi();

        return $this->render('index', [
            'model' => $model,
            'actions' => DeliveryApi::getActions(),
            'country' => $model->country
        ]);
    }

    /**
     * @return array|mixed|string|\yii\httpclient\Response
     * @throws HttpInvalidParamException
     */
    public function actionExecute()
    {
        $action = Yii::$app->request->post('action');
        $post = Yii::$app->request->post();

        $deliveryApi = new DeliveryApi();
        $deliveryApi->load($post);

        switch ($action) {
            case DeliveryApi::ACTION_AUTHORIZATION:
                return $deliveryApi->authorization();
            case DeliveryApi::ACTION_GET_ORDERS:
                return $deliveryApi->getOrders();
            case DeliveryApi::ACTION_UPDATE_ORDER_STATUS:
                return $deliveryApi->updateStatus();
            default:
                throw new HttpInvalidParamException(Yii::t('common', 'Такого действия не существует.'));
        }
    }
}
