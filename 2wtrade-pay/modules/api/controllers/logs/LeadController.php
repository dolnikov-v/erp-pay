<?php
namespace app\modules\api\controllers\logs;

use app\components\web\Controller;
use app\modules\api\models\ApiLog;
use app\modules\api\models\search\ApiLogSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class LeadController
 * @package app\modules\api\controllers\logs
 */
class LeadController extends CommonController
{
    /**
     * @var string
     */
    protected $logType = ApiLog::TYPE_LEAD;

}
