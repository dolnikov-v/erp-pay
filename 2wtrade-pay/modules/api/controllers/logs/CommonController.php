<?php
namespace app\modules\api\controllers\logs;

use app\components\web\Controller;
use app\modules\api\models\ApiLog;
use app\modules\api\models\search\ApiLogSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class CommonController
 * @package app\modules\api\controllers\logs
 */
class CommonController extends Controller
{
    /**
     * @var string
     */
    protected $logType = ApiLog::TYPE_LEAD;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ApiLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->logType);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'inputs' => json_decode($model->input_data, true),
            'outputs' => json_decode($model->output_data, true),
        ]);
    }

    /**
     * @param integer $id
     * @return ApiLog
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApiLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Лог не найден.'));
        }
    }
}
