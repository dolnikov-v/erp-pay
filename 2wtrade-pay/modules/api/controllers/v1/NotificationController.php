<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\models\User;
use app\modules\api\models\Notification;
use app\modules\order\models\Order;
use app\modules\order\models\OrderNotification;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NotificationController
 * @package app\modules\api\controllers\v1
 */
class NotificationController extends Controller
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    /**
     * @var array
     */
    public $errors = [];

    /**
     * Единственный метод доступный извне
     * Возвращает список последних заданных вопросов на заказ, вопросы не содержат ответа
     * @param $from integer timestamp
     * @return array
     */
    public function actionGetNotifications()
    {
        $notifications = Notification::find()
            ->leftJoin(OrderNotification::tableName(), OrderNotification::tableName() . '.id=' . Notification::tableName() . '.order_notification_id')
            ->andWhere(['=', Notification::tableName() . '.status', Notification::SMS_STATUS_NEW])
            ->andWhere(['=', OrderNotification::tableName() . '.answerable', OrderNotification::IS_ANSWERABLE])
            ->asArray()
            ->all();

        $ids = ArrayHelper::getColumn($notifications, 'id');

        $models = Notification::find()->where(['in', Notification::tableName() . '.id', $ids])->all();

        foreach ($models as $model) {
            $model->sms_notification_text = $model->orderNotification->getFilledSmsText($model->order);
            $model->email_notification_text = $model->orderNotification->getFilledEmailText($model->order);
            $model->status = Notification::SMS_STATUS_READY;
            $model->save();
        }

        //отправлять так же данные по заказам
        $order_ids = ArrayHelper::getColumn($notifications, 'order_id');
        $orders = Order::find()->where(['in', Order::tableName().'.id', $order_ids])->all();

        $orders_list = [];

        foreach($orders as $order){
            $products = $order->products;
            $order_products = $order->orderProducts;
            $products_list = [];

            foreach($products as $k=>$product){
                $products_list[] = [
                    'product_id' => $product->id,
                    'product_name' => $product->name,
                    'product_price' => $order_products[$k]->price,
                    'product_quantity' => $order_products[$k]->quantity
                ];
            }

            $orders_list[$order->id] = [
                'order_id' => $order->id,
                'customer_full_name' => $order->customer_full_name,
                'customer_address_add' => json_decode($order->customer_address_add),
                'products' => $products_list,
                'price_total' => $order->price_total,
                'customer_phone' => $order->customer_phone,
                'customer_mobile' => $order->customer_mobile
            ];
        }

        $result = [
            'status' => self::RESPONSE_STATUS_SUCCESS,
            'notifications' => $models,
            'orders' => $orders_list
        ];

        return $result;
    }
}

?>