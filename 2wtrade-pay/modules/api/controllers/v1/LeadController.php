<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\components\filters\auth\HttpBearerAuth;
use app\modules\order\models\Lead;
use app\modules\api\models\LeadStatus;
use app\modules\api\models\ApiLog;
use Yii;
use yii\web\ForbiddenHttpException;
use app\models\Notification;

/**
 * Class LeadController
 * @package app\modules\api\controllers\v1
 */
class LeadController extends Controller
{

    const RETURN_CODE_SUCCESS = 200;
    const RETURN_CODE_ERROR_SOURCE = 410;
    const RETURN_CODE_ERROR_LANDING = 420;
    const RETURN_CODE_ERROR_PRODUCT_LANDING = 421;
    const RETURN_CODE_ERROR_NO_ORDER = 430;
    const RETURN_CODE_ERROR_NOT_PREPAID = 431;
    const RETURN_CODE_ERROR_EDIT_STATUS = 432;
    const RETURN_CODE_ERROR_CANCEL_STATUS = 433;
    const RETURN_CODE_ERROR_ORDER_SAVE = 433;
    const RETURN_CODE_ERROR_NO_NUMBERS = 440;
    const RETURN_CODE_ERROR_CC_REQUEST = 441;
    const RETURN_CODE_ERROR_D_REQUEST = 442;
    const RETURN_CODE_ERROR_UNEXPECTED = 444;

    /**
     * Разрешенные доступы
     * @var array
     */
    protected $allowedAccessTypes = [
        HttpBearerAuth::ACCESS_TYPE_ADCOMBO,
        HttpBearerAuth::ACCESS_TYPE_JEEMPO,
        HttpBearerAuth::ACCESS_TYPE_2WSTORE,
        HttpBearerAuth::ACCESS_TYPE_DIAVITA,
        HttpBearerAuth::ACCESS_TYPE_2WDISTR,
        HttpBearerAuth::ACCESS_TYPE_WTRADE_POST_SALE,
        HttpBearerAuth::ACCESS_TYPE_TEST_1,
        HttpBearerAuth::ACCESS_TYPE_MY_EPC,
        HttpBearerAuth::ACCESS_TYPE_TEST_3,
        HttpBearerAuth::ACCESS_TYPE_TEST_4,
        HttpBearerAuth::ACCESS_TYPE_TEST_5,
        HttpBearerAuth::ACCESS_TYPE_TOPHOT,
    ];

    /**
     * @inheritdoc
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!Yii::$app->apiUser || !Yii::$app->apiUser->source) {
            throw new ForbiddenHttpException('Permission denied.');
        }

        return parent::beforeAction($action);
    }

    /**
     * @return array |  string
     */
    public function actionCreate()
    {
        $Notification = Yii::$app->get('notification');

        $model = null;

        try {
            $model = Lead::createFromPost(Yii::$app->request->post());

            $result = $model->create();

            switch ($result) {
                case ApiLog::STATUS_SUCCESS:
                    $this->responseData['data']['order_id'] = $model->order->id;
                    return $this->success();
                    break;
                case ApiLog::STATUS_DUPLICATE:
                    $this->responseData['data']['order_id'] = $model->order->id;
                    return $this->duplicate();
                    break;
                default:
                    $this->addMessagesByModel($model);
                    $Notification->send(Notification::TRIGGER_LEAD_NOT_ADDED, ['exception' => print_r($model->getErrors(), true), 'source' => Yii::$app->apiUser->source->name, 'foreign_id' => $model->order_id ?? 'Not found'], isset($model->order->country->id) ? $model->order->country->id : null);
                    return $this->fail();
                    break;
            }
        } catch (\Exception $e) {
            $Notification->send(Notification::TRIGGER_LEAD_NOT_ADDED, ['exception' => $e->getMessage(), 'source' => Yii::$app->apiUser->source->name, 'foreign_id' => $model->order_id ?? 'Not found'], null);
            $this->addMessage($e->getMessage());
            return $this->fail(500);
        }
    }

    /**
     * Редактирование предоплаченных заказов, не отправленных в КЦ и КС
     * @return array | string
     */
    public function actionEdit()
    {
        $model = null;
        try {
            $model = Lead::createFromPost(Yii::$app->request->post());
            $result = $model->edit();
            switch ($result['status']) {
                case ApiLog::STATUS_SUCCESS:
                    $this->addDataCode(self::RETURN_CODE_SUCCESS);
                    $this->responseData['data']['order_id'] = $model->order->id;
                    return $this->success();
                    break;
                default:
                    $this->addDataCode($result['code']);
                    $this->addMessagesByModel($model);
                    return $this->fail();
                    break;
            }
        } catch (\Exception $e) {
            $this->addMessage($e->getMessage());
            return $this->fail(500);
        }
    }

    /**
     * @return array
     */
    public function actionStatuses()
    {
        $foreignIds = Yii::$app->request->get('ids');

        if ($foreignIds) {
            $foreignIds = array_map('intval', explode(',', $foreignIds));
            set_time_limit(0);
            $this->responseData['data'] = LeadStatus::getStatusByForeignIds($foreignIds);
            return $this->success();
        } else {
            $this->addMessage("Необходимо указать номера заказов.");
            return $this->fail();
        }
    }

    /**
     * @return array
     */
    public function actionBuyouts()
    {
        $foreignIds = Yii::$app->request->get('ids');

        if ($foreignIds) {
            $foreignIds = array_map('intval', explode(',', $foreignIds));
            set_time_limit(0);
            $this->responseData['data'] = LeadStatus::getBuyoutByForeignIds($foreignIds);
            return $this->success();
        } else {
            $this->addMessage("Необходимо указать номера заказов.");
            return $this->fail();
        }
    }

    /**
     * @return array
     */
    public function actionCancelOrders()
    {
        $foreignIds = Yii::$app->request->get('ids');

        if ($foreignIds) {
            $foreignIds = array_map('intval', explode(',', $foreignIds));
            set_time_limit(0);
            $this->responseData['data'] = LeadStatus::cancelOrdersByForeignIds($foreignIds);
            return $this->success();
        } else {
            $this->addDataCode(self::RETURN_CODE_ERROR_NO_NUMBERS);
            $this->addMessage("Необходимо указать номера заказов");
            return $this->fail();
        }
    }

    /**
     * добавляем код в тело ответа
     * @param int $code
     */
    protected function addDataCode($code)
    {
        $this->responseData['data']['result_code'] = $code;
    }
}
