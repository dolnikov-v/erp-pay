<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\salary\models\Office;
use Yii;

/**
 * Class MercuryController
 * @package app\modules\api\controllers\v1
 */
class MercuryController extends Controller
{
    /**
     * @return array
     */
    public function actionGetOffices()
    {
        $data = Office::find()
            ->joinWith([
                'country',
                'workingShifts',
                'staffing',
            ])
            ->active()
            ->all();

        $offices = [];

        foreach ($data as $office) {
            $offices[] = [
                'id' => $office->id,
                'country_ru' => $office->country->name,
                'country_en' => $office->country->name_en,
                'country_char_code' => $office->country->char_code,
                'type' => $office->type,
                'name_ru' => $office->name,
                'shift' => $this->compareWorkingShifts($office->getWorkingShifts()->all()),
                'position' => $this->compareStaffings($office->staffing, $office->hide_staffing_salary)
            ];
        }

        $this->responseData['data'] = $offices;

        return $this->success();
    }

    /**
     * @param $shiftsData
     * @return array
     */
    public function compareWorkingShifts($shiftsData)
    {
        $shifts = [];

        foreach ($shiftsData as $shift) {
            $shifts[] = [
                'id' => $shift->id,
                'name' => $shift->name,
                'description' => [
                    'lunch_time' => $shift->lunch_time,
                    'working_mon' => $shift->working_mon,
                    'working_tue' => $shift->working_tue,
                    'working_wed' => $shift->working_wed,
                    'working_thu' => $shift->working_thu,
                    'working_fri' => $shift->working_fri,
                    'working_sat' => $shift->working_sat,
                    'working_sun' => $shift->working_sun
                ]
            ];

        }

        return $shifts;
    }

    /**
     * @param $staffings
     * @param $hide_staffing_salary
     * @return array
     */
    public function compareStaffings($staffings, $hide_staffing_salary)
    {
        $positions = [];

        foreach ($staffings as $staffing) {
            $positions[] = [
                'id' => $staffing->designation->id,
                'name_ru' => $staffing->designation->name,
                'name_en' => yii::t('common', $staffing->designation->name, [], 'en-US'),
                'salary_usd' => $hide_staffing_salary == 0 ? $staffing->salary_usd : '*',
                'salary_local' =>  $hide_staffing_salary == 0 ? $staffing->salary_local : '*',
                'lead' => (int)($staffing->designation->team_lead || $staffing->designation->supervisor)
            ];
        }

        return $positions;
    }


}