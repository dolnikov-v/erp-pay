<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\models\Delivery;
use Yii;

/**
 * Class DeliveryController
 * @package app\modules\api\controllers\v1
 */
class DeliveryController extends Controller
{
    /**
     * @return array
     */
    public function actionList()
    {
        $delivery = new Delivery();
        $delivery->setScenario(Delivery::SCENARIO_LIST);
        $delivery->load(['Delivery' => Yii::$app->request->get()]);

        $result = $delivery->getList();

        if ($delivery->hasErrors()) {
            $this->addMessagesByModel($delivery);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionCheck()
    {
        $delivery = new Delivery();
        $delivery->setScenario(Delivery::SCENARIO_CHECK);
        $delivery->load(['Delivery' => Yii::$app->request->get()]);

        $result = $delivery->checkPostal();

        if ($delivery->hasErrors()) {
            $this->addMessagesByModel($delivery);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }
}
