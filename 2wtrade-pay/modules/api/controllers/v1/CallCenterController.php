<?php

namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\models\Broker;
use app\modules\api\models\CallCenter;
use app\modules\delivery\abstracts\BrokerResultInterface;
use app\modules\delivery\components\broker\BrokerRequest;
use app\modules\delivery\DeliveryModule;
use app\modules\logger\components\log\Logger;
use app\modules\order\models\Order;
use Yii;

/**
 * Class CallCenterController
 * @package app\modules\api\controllers\v1
 */
class CallCenterController extends Controller
{
    /**
     * @return array
     */
    public function actionIds()
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_IDS);
        $callCenter->load(['CallCenter' => Yii::$app->request->get()]);

        $ids = $callCenter->getIds();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $ids;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionStatuses()
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_STATUSES);
        $callCenter->load(['CallCenter' => Yii::$app->request->get()]);

        $statuses = $callCenter->getStatuses();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $statuses;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionStatusesOnOrders()
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_STATUSES_ON_ORDERS);
        $callCenter->load(['CallCenter' => Yii::$app->request->get()]);

        $statuses = $callCenter->getStatusesOnOrders();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $statuses;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionCanUpdate()
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_CAN_UPDATE);
        $callCenter->load(['CallCenter' => Yii::$app->request->get()]);

        $result = $callCenter->getCanUpdate();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionUpdateOrder($id)
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_UPDATE_ORDER);
        $callCenter->id = $id;

        $result = $callCenter->updateOrder();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionList()
    {
        $callCenter = new CallCenter();
        $callCenter->setScenario(CallCenter::SCENARIO_LIST);
        $callCenter->load(['CallCenter' => Yii::$app->request->get()]);

        $result = $callCenter->getList();

        if ($callCenter->hasErrors()) {
            $this->addMessagesByModel($callCenter);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionOrder()
    {
        $postDataJson = Yii::$app->request->rawBody;
        if (!$postDataJson || !($postData = json_decode($postDataJson, true))) {
            $this->addMessage('Error data is not valid');
            //$logger = Yii::$app->get("processingLogger");
            /**
             * @var Logger $logger
             */
//            $cronLog = $logger->getLogger([
//                'route' => $this->getRoute(),
//            ]);
            $this->saveLog($postDataJson, true);
            //$cronLog(json_last_error());
            return $this->fail();
        }

        foreach ($postData as $orderId => $data) {
            $callCenter = new CallCenter();
            $callCenter->setScenario(CallCenter::SCENARIO_NEW_ORDER);
            $callCenter->id = $orderId;
            $callCenter->countryId = Order::find()->select('country_id')->byId($orderId)->scalar();

            if (!$callCenter->validate()) {
                $this->addData([$data['id'] => [
                    'success' => false,
                    'message' => $callCenter->getFirstErrorAsString()
                ]
                ]);
                $callCenter->clearErrors();
                continue;
            }

            if (isset($data['address_components']) && is_string($data['address_components'])) {
                $data['address_components'] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;",
                    $data['address_components']), ENT_NOQUOTES, 'UTF-8');
                $data['address_components'] = json_decode($data['address_components'], true);
            }
            if ($callCenter->Order($data)) {
                $this->addData([$data['id'] => ['success' => true]]);
            } else {
                $this->addData([$data['id'] => [
                    'success' => false,
                    'message' => $callCenter->getFirstErrorAsString()
                ]]);
                $callCenter->clearErrors();
            }
        }
        return $this->success();
    }


    /**
     * @return DeliveryModule|null|\yii\base\Module
     */
    protected function getDeliveryModule()
    {
        return \Yii::$app->getModule('delivery');
    }

    /**
     * @return array
     */
    public function actionBroker()
    {
        // загружаем и проверяем данные в старом объекте модернизированном для работы с кешем
        $broker = new Broker();
        $broker->load(Yii::$app->request->post(), '');
        $broker->validate();

        if ($broker->hasErrors()) {
            $this->addMessagesByModel($broker);
            return $this->fail();
        }

        $newVersion = Yii::$app->request->get('newVersion', 1);

        if ($newVersion) {
            // новая версия
            try {

                $brokerRequest = new BrokerRequest([
                    'country_id' => (int)$broker->getCountryID(),
                    'package_id' => $broker->package_id,
                    'products' => $broker->products,
                    'customer_zip' => $broker->customer_zip,
                    'customer_city' => $broker->customer_city,
                    'customer_province' => $broker->customer_province,
                    'partner_id' => $broker->partner_id,
                    'express' => $broker->express ? true : false,
                    'order_price' => $broker->order_price,
                    'shipping_price' => $broker->shipping_price
                ]);

                $brokerResult = $this->getDeliveryModule()->broker->getAvailableDeliveries($brokerRequest);

                $response = [];
                if ($brokerResult) {
                    foreach ($brokerResult as $result) {
                        /** @var BrokerResultInterface $result */
                        $response[] = [
                            'id' => $result->getDeliveryId(),
                            'name' => $result->getDeliveryName(),
                            'delivery_price' => $result->getDeliveryPrice(),
                            'express' => $result->getExpress(),
                            'min_delivery_days' => $result->getMinDeliveryDays(),
                            'max_delivery_days' => $result->getMaxDeliveryDays(),
                        ];
                    }
                }
                $this->responseData['data'] = $response;

            } catch (\Exception $e) {
                $this->addMessage($e->getMessage());
                return $this->fail(500);
            }
        }

        if (!$newVersion) {
            // старая версия:
            $this->responseData['data'] = $broker->findPriorityDeliveries();
        }

        return $this->success();
    }

    public function saveLog($msg, $err = false)
    {
        $path = \Yii::getAlias('@logs') . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . get_class($this) . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR;

        $fileName = $path . date('Y-m-d') . ($err ? '.err' : '.log');

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($msg)) {
            $msg = print_r($msg, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        if (file_put_contents($fileName, $msg, FILE_APPEND)) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function actionBuyouts()
    {
        $token = Yii::$app->request->get('token');
        $ids = Yii::$app->request->post('ids');

        if ($ids) {
            set_time_limit(0);
            $this->responseData['data'] = CallCenter::getBuyoutByCallCenterIds($token, $ids);
            return $this->success();
        } else {
            $this->addMessage("Необходимо указать номера заказов.");
            return $this->fail();
        }
    }
}
