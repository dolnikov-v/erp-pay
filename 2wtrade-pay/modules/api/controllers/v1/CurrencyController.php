<?php

namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\models\CurrencyRate;
use Yii;

/**
 * Class CurrencyController
 * @package app\modules\api\controllers\v1
 */
class CurrencyController extends Controller
{
    /**
     * @return array
     */
    public function actionGetRate()
    {
        $currency = new CurrencyRate();
        $currency->load(['CurrencyRate' => Yii::$app->request->get()]);

        $result = $currency->getRate();

        if ($currency->hasErrors()) {
            $this->addMessagesByModel($currency);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }
}