<?php
namespace app\modules\api\controllers\v1;

use yii\rest\Controller;
use app\modules\logger\components\log\Logger;
use Yii;
use app\modules\api\models\SystemNotification;

class SystemNotificationController extends Controller
{

    public function actionSend()
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);
        $request = Yii::$app->request->getRawBody();
        $cronLog(print_r($request, true));
        if (!$data = json_decode($request, true)) {
            return ['message' => 'Invalid json format'];
        }
        $model = new SystemNotification();
        $model->load($data, '');
        if (!$model->validate()) {
            return $model;
        }
        $model->send();
        return $model;
    }
}