<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\models\ApiLog;
use app\modules\api\models\Product;
use Yii;

/**
 * Class ProductController
 * @package app\modules\api\controllers\v1
 */
class ProductController extends Controller
{
    /**
     * @return array
     */
    public function actionProducts()
    {
        $product = new Product();
        $product->load(['Product' => Yii::$app->request->get()]);

        $result = $product->getProducts();

        if ($product->hasErrors()) {
            $this->addMessagesByModel($product);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionGetProduct()
    {
        $id = Yii::$app->request->get('id') ??  false;

        if ($id) {
            $product = Product::getProductById($id);

            $this->responseData['data'] = $product;
            return $this->success();

        } else {
            $this->addMessage(yii::t('common', 'Не найден обязательный параметр id'));
            return $this->fail();
        }
    }

    /**
     * @return array
     */
    public function actionStorageProducts()
    {
        $product = new Product();
        $product->setScenario(Product::SCENARIO_STORAGE_PRODUCTS);
        $product->load(['Product' => Yii::$app->request->get()]);

        $result = $product->getStorageProducts();

        if ($product->hasErrors()) {
            $this->addMessagesByModel($product);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionProductsOnDelivery()
    {
        $product = new Product();
        $product->setScenario(Product::SCENARIO_PRODUCTS_ON_DELIVERY);
        $product->load(['Product' => Yii::$app->request->get()]);

        $result = $product->getProductsOnDelivery();

        if ($product->hasErrors()) {
            $this->addMessagesByModel($product);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    /**
     * @return array
     */
    public function actionProductsReport()
    {
        $product = new Product();
        $product->setScenario(Product::SCENARIO_PRODUCTS_REPORT);
        $product->load(['Product' => Yii::$app->request->get()]);

        $result = $product->getProductsReport();

        if ($product->hasErrors()) {
            $this->addMessagesByModel($product);

            return $this->fail();
        } else {
            $this->responseData['data'] = $result;

            return $this->success();
        }
    }

    public function actionCreate()
    {
        $post = Yii::$app->request->getRawBody();
        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_PRODUCT;
        $apiLog->input_data = json_encode(Yii::$app->request->post(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if (!($products = json_decode($post, true)) || !isset($products[0])) {
            $this->addMessage('Not correct json/data format!');
            $apiLog->output_data = json_encode($this->respond(), JSON_UNESCAPED_UNICODE);
            $apiLog->save();
            return $this->fail();
        }

        foreach ($products as $productPost) {
            $product = new Product();
            if (!$productId = $product->create($productPost)) {
                $this->addDataValue('result', [
                    'name' => $product->name,
                    'status' => 'fail'
                ]);
                $this->addMessage($product->getErrors());
                continue;
            };
            $this->addDataValue('result', [
                'name' => $product->name,
                'status' => 'success',
                'id' => $productId
            ]);
        }

        $apiLog->status = ApiLog::STATUS_SUCCESS;
        $apiLog->output_data = json_encode($this->respond(), JSON_UNESCAPED_UNICODE);
        $apiLog->save();
        return $this->success();
    }
}
