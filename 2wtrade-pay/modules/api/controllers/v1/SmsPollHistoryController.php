<?php
namespace app\modules\api\controllers\v1;

use app\components\rest\Controller;
use app\modules\api\models\SmsPollHistory;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SmsPollHistoryController
 * @package app\modules\api\controllers\v1
 */
class SmsPollHistoryController extends Controller
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    const INCORRECT_TIME_FROM = "Incorrect Parameter 'from'. Must be integer timestamp";
    const EMPTY_TIME_FROM = "Parameter 'from' cannot be empty";
    const ERROR_SAVE_ANSWER = "Error saving answer";

    const GET_ANSWER_URL = 'http://...';
    /**
     * @var array
     */
    public $errors = [];

    /**
     * Единственный метод доступный извне
     * Возвращает список последних заданных вопросов на заказ, вопросы не содержат ответа
     * @param $from integer timestamp
     * @return array
     */
    public function actionGetHistory()
    {
        $from = Yii::$app->request->post('from');

        $questions = SmsPollHistory::find()
            ->andWhere(['=', SmsPollHistory::tableName() . '.status', SmsPollHistory::HISTORY_STATUS_NEW])
            ->asArray()
            ->all();

        if (empty($from)) {
            $this->errors[] = self::EMPTY_TIME_FROM;
        } elseif (!is_numeric($from)) {
            $this->errors[] = self::INCORRECT_TIME_FROM;
        }

        $result = [
            'status' => self::RESPONSE_STATUS_SUCCESS,
            'questions' => $questions
        ];

        $ids = ArrayHelper::getColumn($questions, 'id');

        $models = SmsPollHistory::find()->where(['in', SmsPollHistory::tableName() . '.id', $ids])->all();

        foreach($models as $model) {
            $model->status = SmsPollHistory::HISTORY_STATUS_READY;
            $model->save();
        }

        return $result;
    }

    /**
     * Метод подключения к стороннему сайту
     */
    private function connectOurApi($url, $from)
    {
        //Curl library
    }

    /**
     * Метод обращения к стороннему сервису и получения списка моделей ответов
     * решили что передавать будем order_id и question_id
     * с последующей попыткой сохранения ответов
     * @param $from integer timestamp
     */
    public function actionGetAnswers($from)
    {
        //курлом забрали всё с сайта - начиная с определённого времени, чтоб старые не брать
        //далее answers - маиccив моделей с ответами

        //узнаём когда мы почлучили последний ответ от стороннего апи
        $last_data = SmsPollHistory::find()
            ->where(['is not', SmsPollHistory::tableName() . '.answered_at', null])
            ->orderBy([SmsPollHistory::tableName() . '.id', SORT_DESC])
            ->one();

        $answers = $this->connectOurApi(self::GET_ANSWER_URL, $last_data->updated_at);

        if (!empty($answers)) {
            foreach ($answers as $answer) {
                $resultSave = $this->saveAnswer($answer);

                if ($resultSave['status'] == self::STATUS_FAIL) {
                    //логировать ошибку сохранения ответа
                    $this->errors[] = $resultSave['errors'];
                }
            }
        }

        //что-то делать с не пустым $this->errors[]
    }

    /**
     * @param object $answer
     * @return array
     */
    public function saveAnswer($answer)
    {
        $sms_poll_history_id = $answer->sms_poll_history_id;
        $answer_text = $answer->answer_text;
        $answered_at = $answer->answered_at;

        $model = SmsPollHistory::find()
            ->where([SmsPollHistory::tableName() . '.id' => $sms_poll_history_id]);
        $model->answer_text = $answer_text;
        $model->answered_at = $answered_at;
        $model->updated_at = time();

        if ($model->save()) {
            $result = ['status' => self::STATUS_SUCCESS];
        } else {
            $result = [
                'status' => self::STATUS_FAIL,
                'errors' => $model->getErrors()
            ];
        }

        return $result;
    }
}

?>