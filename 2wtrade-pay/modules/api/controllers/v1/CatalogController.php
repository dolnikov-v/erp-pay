<?php

namespace app\modules\api\controllers\v1;

use app\models\Currency;
use app\models\Language;
use app\models\ProductCategory;
use app\modules\storage\models\StorageProduct;
use Yii;
use app\models\Country;
use app\models\Product;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use app\models\ProductPhoto;
use app\components\rest\Controller;

/**
 * Class CatalogController
 * @package app\modules\api\controllers\v1
 */
class CatalogController extends Controller
{

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $with_image = Yii::$app->request->get('image', true);
        $country = Yii::$app->request->get('country', false);
        $query = Product::find()
            ->joinWith([
                'storageProducts',
                'storageProducts.storage',
                'storageProducts.storage.country',
                'storageProducts.storage.country.language',
                'storageProducts.storage.country.currency',
                'categories'
            ])
            ->innerJoin(ProductPrice::tableName() . ' pr', 'pr.product_id = ' . Product::tableName() . '.id and pr.country_id = ' . Country::tableName() . '.id and ' . Currency::tableName() . '.id = pr.currency_id')
            ->leftJoin(ProductPhoto::tableName() . ' pp', 'pp.product_id = ' . Product::tableName() . '.id and pp.country_id = ' . Country::tableName() . '.id')
            ->leftJoin(ProductTranslation::tableName() . ' pt', 'pt.product_id = ' . Product::tableName() . '.id and pt.country_id = ' . Country::tableName() . '.id and ' . Language::tableName() . '.id = pt.language_id')
            ->select([
                Product::tableName() . '.id',
                Product::tableName() . '.name',
                Product::tableName() . '.weight',
                Product::tableName() . '.width',
                Product::tableName() . '.height',
                Product::tableName() . '.length',
                'image' => 'IF(pp.image IS NOT NULL, pp.image, ' . Product::tableName() . '.image)',
                Language::tableName() . '.locale',
                'country_char' => Country::tableName() . '.char_code',
                'price' => 'pr.price',
                'translation' => 'pt.description',
                'categories' => 'GROUP_CONCAT(DISTINCT ' . ProductCategory::tableName() . '.name)',
                'currency_code' => Currency::tableName() . '.char_code',
                'currency_name' => Currency::tableName() . '.name',
                'quantity' => new Expression('SUM(' . StorageProduct::tableName() . '.balance)')
            ])
            ->where([Country::tableName() . '.is_partner' => 0])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Product::tableName() . '.active' => 1])
            ->andWhere([Product::tableName() . '.shell_on_market' => 1])
            ->andWhere(['IS NOT', 'pt.description', null])
            ->andWhere(['IS NOT', 'pr.price', null])
            ->andWhere(['IS NOT', Product::tableName() . '.image', null]);

        if ($country) {
            $query->andWhere([Country::tableName() . '.char_code' => $country]);
        }
        $countryProducts = $query
            ->groupBy([Country::tableName() . '.char_code', Product::tableName() . '.id'])
            ->orderBy('country_char')
            ->createCommand()
            ->queryAll();

        $countryProducts = ArrayHelper::index($countryProducts, null, 'country_char');

        foreach ($countryProducts as $country => $countryProduct) {
            foreach ($countryProduct as $key => $data) {
                $countryProducts[$country][$key]['lang'] = strstr($data['locale'], '-', -1);
                $countryProducts[$country][$key]['currency_name'] = Yii::t('common', $countryProducts[$country][$key]['currency_name'], 'en-US');
                if ($data['image'] != null && $with_image) {
                    $imagePath = $imagePath = Yii::getAlias('@media') . '/product/' . $data['image'];
                    if (is_file($imagePath)) {
                        $imageFile = file_get_contents($imagePath);
                        $countryProducts[$country][$key]['image'] = base64_encode($imageFile);
                    }
                }
            }
        }
        $this->responseData['data'] = $countryProducts;
        return $this->success();
    }

    /**
     * @return array
     */
    public function actionImage()
    {
        $request = Yii::$app->request->get();
        if (!isset($request['ids'])) {
            $this->addMessage('Error! Ids parameter is mandatory.');
            return $this->fail();
        }
        $ids = $request['ids'];
        $images = Product::find()->where(['id' => $ids])->select(['id', 'image'])->asArray()->all();
        $responseIds = array_fill_keys($ids, null);
        foreach ($images as $image) {
            if ($image['image'] != null && is_file(($fileName = __DIR__ . '/' . $image['image']))) {
                $imageFile = file_get_contents($fileName);
                $responseIds[$image['id']] = base64_encode($imageFile);
            }
        }
        $this->responseData['data'] = $responseIds;
        return $this->success();
    }
}