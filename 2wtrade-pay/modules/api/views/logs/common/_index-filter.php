<?php
use app\components\widgets\ActiveForm;
use app\modules\api\models\search\ApiLogSearch;
use yii\helpers\Url;
use app\models\Country;

/** @var yii\web\View $this */
/** @var app\modules\api\models\search\ApiLogSearch $model */
/** @var ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'country_id')->select2List(Country::find()->active()->collection(), [
            'prompt' => '—',
            'length' => 1,
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'status')->select2List(ApiLogSearch::getStatusesCollection(), [
            'prompt' => '—',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
