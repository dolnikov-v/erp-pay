<?php
use app\widgets\ButtonLink;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $inputs */
/** @var array $outputs */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Просмотр лога'),
    'content' => $this->render('../common/_view-content', [
        'inputs' => $inputs,
        'outputs' => $outputs,
    ]),
    'footer' => ButtonLink::widget([
        'label' => Yii::t('common', 'Вернуться назад'),
        'url' => Url::toRoute('index'),
        'style' => ButtonLink::STYLE_DEFAULT,
        'size' => ButtonLink::SIZE_SMALL,
    ])
]) ?>
