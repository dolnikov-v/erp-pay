<?php
/** @var yii\web\View $this */
/** @var array $inputs */
/** @var array $outputs */
?>

<div class="row">
    <div class="col-lg-6">
        <h4 class="example-title"><?= Yii::t('common', 'Входные данные') ?></h4>
        <?= $this->render('_view-content-table', ['messages' => $inputs]) ?>
    </div>
    <div class="col-lg-6">
        <h4 class="example-title"><?= Yii::t('common', 'Выходные данные') ?></h4>
        <?= $this->render('_view-content-table', ['messages' => $outputs]) ?>
    </div>
</div>
