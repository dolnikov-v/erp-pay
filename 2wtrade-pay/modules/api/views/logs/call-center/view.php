<?php
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\api\models\ApiLog $model */
/** @var array $inputs */
/** @var array $outputs */

$this->title = Yii::t('common', 'Просмотр лога');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Колл-центр'), 'url' => Url::toRoute('/api/logs/call-center')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= $this->render('../common/_view-panel', [
    'inputs' => $inputs,
    'outputs' => $outputs,
]) ?>
