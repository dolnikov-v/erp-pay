<?php

/** @var yii\web\View $this */
/** @var app\modules\api\models\search\ApiLogSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Лиды');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= $this->render('../common/_index-body', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'canView' => Yii::$app->user->can('api.logs.lead.view')
]) ?>
