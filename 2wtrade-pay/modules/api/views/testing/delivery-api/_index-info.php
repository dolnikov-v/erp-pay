<?php
use app\widgets\Panel;

/** @var array $actions */
/** @var \app\modules\api\models\testing\DeliveryApi $model */

?>

<div class="row">
    <div class="col-lg-12">
        <?= Panel::widget([
            'border' => false,
            'withBody' => false,
            'content' => $this->render('_change-action', [
                'actions' => $actions
            ])
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div id="panel_api_get_orders" class="col-lg-12 modified-panel">
        <?= $this->render('_get-orders', [
            'model' => $model
        ])
        ?>
    </div>
    <div id="panel_api_update_status" class="col-lg-12 modified-panel">
        <?= $this->render('_update-status', [
            'model' => $model
        ])
        ?>
    </div>
</div>
