<?php
use app\assets\vendor\BootstrapLaddaAsset;
use app\assets\vendor\JJsonViewerAsset;
use app\modules\api\assets\TestingDeliveryApiAsset;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\web\View;

/** @var array $actions */
/** @var \app\modules\api\models\testing\DeliveryApi $model */
/** @var \app\models\Country $country */

BootstrapLaddaAsset::register($this);
JJsonViewerAsset::register($this);
TestingDeliveryApiAsset::register($this);

$this->title = Yii::t('common', 'Тестирование API службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Тестирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("DeliveryApiTesting.options['linkExecute'] = '" . Url::to('delivery-api/execute') . "';", View::POS_END);
?>

<?php ?>
<div id="delivery_api_testing" class="row">
    <div class="col-lg-12">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Авторизация'),
            'id' => 'panel_api_authorization',
            'content' => $this->render('_authorization', [
                'model' => $model,
                'country' => $country,
            ])
        ]);
        ?>
    </div>
    <div class="col-lg-12">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Выберите действие'),
            'content' => $this->render('_index-info', [
                'actions' => $actions,
                'model' => $model,
            ])
        ]);
        ?>
    </div>
    <div class="col-lg-12">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Ответ'),
            'content' => $this->render('_answer')
        ]);
        ?>
    </div>
</div>
<?php ?>

