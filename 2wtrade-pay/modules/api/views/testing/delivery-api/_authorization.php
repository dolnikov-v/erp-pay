<?php
use app\components\widgets\ActiveForm;
use app\widgets\ButtonProgress;
use app\widgets\InputText;

/** @var \app\modules\api\models\testing\DeliveryApi $model */
/** @var \app\models\Country $country */

$form = ActiveForm::begin([
    'id' => 'form_authorization',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <div class="control-label">
                <label for="delivery_api_country"><?= Yii::t('common', 'Страна') ?></label>
            </div>
            <?= InputText::widget([
                'id' => 'delivery_api_country',
                'disabled' => true,
                'value' => $country->name
            ]) ?>
        </div>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'password')->textInput() ?>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="control-label">
                <label for="deliveryapi_base64_token"><?= Yii::t('common', 'Токен') ?></label>
            </div>
            <?= InputText::widget([
                'id' => 'deliveryapi_token',
                'placeholder' => Yii::t('common', 'Токен'),
                'readonly' => true
            ]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="control-label">
                <label for="deliveryapi_base64_token"><?= Yii::t('common', 'Токен(Base64)') ?></label>
            </div>
            <?= InputText::widget([
                'id' => 'deliveryapi_base64_token',
                'placeholder' => Yii::t('common', 'Токен(Base64)'),
                'readonly' => true
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= ButtonProgress::widget([
            'id' => 'action_authorization',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => ButtonProgress::STYLE_SUCCESS,
            'size' => ButtonProgress::SIZE_SMALL,
            'attributes' => [
                'data-action' => 'authorization',
            ]
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

