<?php
use app\widgets\ButtonProgress;
use app\widgets\InputGroup;
use app\widgets\Select2;

/** @var array $actions */
?>
<?= InputGroup::widget([
    'input' => Select2::widget([
        'id' => 'delivery_api_actions',
        'items' => $actions,
        'length' => -1
    ]),
    'right' => [
        ButtonProgress::widget(['id' => 'execute_testing_action',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => ButtonProgress::STYLE_SUCCESS,
        ])
    ]
]) ?>

