<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\api\models\testing\DeliveryApi $model */
$form = ActiveForm::begin([
    'id' => 'form_get_orders',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-md-12">
        <h4><?= Yii::t('common', 'Данные') ?></h4>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'page')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'perPage')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4><?= Yii::t('common', 'Заголовки') ?></h4>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'keyHeader')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'valueHeader')->textInput() ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

