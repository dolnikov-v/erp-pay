<?php

namespace app\modules\api\models;


use app\models\Country;
use app\models\Partner;
use app\models\PartnerCountry;
use app\modules\delivery\models\DeliveryContract;
use app\modules\order\models\OrderProduct;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class Broker
 * @package app\modules\api\models
 */
class Broker extends Model
{

    /**
     * @var string
     */
    public $cachePrefix = 'delivery.broker.request';

    /**
     * @var integer
     */
    public $cacheTime = 600;

    public $country;
    public $package_id;
    public $products;
    public $customer_zip = null;
    public $customer_city = null;
    public $customer_province = null;
    public $partner_id;
    public $express;
    public $order_price = null;
    public $shipping_price = null;

    /**
     * @var integer
     */
    protected $_country;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country'], 'required'],
            [['country', 'partner_id', 'customer_zip', 'customer_city', 'customer_province', 'package_id'], 'string'],
            [['express', 'order_price', 'shipping_price'], 'number'],
            [
                'country',
                'validateCountry',
                'when' => function ($model) {
                    return $model->partner_id == null;
                },
                'enableClientValidation' => false
            ],
            [
                'partner_id',
                'validatePartner',
            ],
            [
                'products',
                'safe'
            ]
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatePartner($attribute, $params)
    {
        $cacheKey = $this->cachePrefix . '.partners.foreign_id.list';
        $partners = Yii::$app->cache->get($cacheKey);
        if ($partners === false) {
            $partners = Partner::find()->where(['active' => 1])->customCollection('foreign_id', 'id');
            Yii::$app->cache->set($cacheKey, $partners, $this->cacheTime);
        }

        if (!isset($partners[$this->partner_id])) {
            $this->addError($attribute, Yii::t('common', 'Такого активного партнера нет.'));
            return;
        }
        $partnerId = $partners[$this->partner_id];


        $cacheKey = $this->cachePrefix . '.partners.country.list';
        $partnerCountry = Yii::$app->cache->get($cacheKey);
        if ($partnerCountry === false) {
            $partnerCountry = ArrayHelper::map(PartnerCountry::find()
                ->select(['partner_id', 'country.char_code', 'country_id'])
                ->innerJoinWith(['country'], false)
                ->asArray()
                ->all(), 'char_code', 'country_id', 'partner_id');

            Yii::$app->cache->set($cacheKey, $partnerCountry, $this->cacheTime);
        }

        if (!isset($partnerCountry[$partnerId][$this->country])) {
            $this->addError($attribute, Yii::t('common', 'Партнер не работает в стране ') . $this->country);
            return;
        }
        $this->_country = $partnerCountry[$partnerId][$this->country];
    }

    /**
     * @param $attribute
     */
    public function validateCountry($attribute)
    {
        $cacheKey = $this->cachePrefix . '.countries.char_code.list';
        $countries = Yii::$app->cache->get($cacheKey);
        if ($countries === false) {
            $countries = Country::find()->customCollection('char_code', 'id');
            Yii::$app->cache->set($cacheKey, $countries, $this->cacheTime);
        }

        if (!isset($countries[$this->country])) {
            $this->addError($attribute, Yii::t('common', 'Неизвестный код страны "{code}".', [
                'code' => $this->country,
            ]));
            return;
        }

        $cacheKey = $this->cachePrefix . '.partner_default.list';
        $partnerDefaultInCountry = Yii::$app->cache->get($cacheKey);
        if ($partnerDefaultInCountry === false) {
            $partnerDefaultInCountry = ArrayHelper::map(PartnerCountry::find()
                ->select(['char_code', 'country_id'])
                ->innerJoinWith(['partner', 'country'], false)
                ->where([Partner::tableName() . '.default' => 1])
                ->andWhere([Partner::tableName() . '.active' => 1])
                ->asArray()
                ->all(), 'char_code', 'country_id');
            Yii::$app->cache->set($cacheKey, $partnerDefaultInCountry, $this->cacheTime);
        }
        if (!isset($partnerDefaultInCountry[$this->country])) {
            $this->addError($attribute, Yii::t('common', 'Нет партнера по умолчанию в стране {code}', ['code' => $this->country]));
            return;
        }
        $this->_country = $partnerDefaultInCountry[$this->country];
    }

    /**
     * @return integer
     */
    public function getCountryID()
    {
        return $this->_country;
    }

    /**
     * Вывод списка приоритетных курьерок для страны и указанных параметров
     * @deprecated
     * @return array
     */
    public function findPriorityDeliveries()
    {
        $deliveries = Country::getDeliveriesByBroker($this->_country, $this->customer_zip, $this->customer_city, $this->prepareProducts(), $this->package_id, $this->express, $this->order_price);

        $response = [];

        foreach ($deliveries as $deliveryInfo) {
            /** @var \app\modules\delivery\models\Delivery $delivery */
            $delivery = $deliveryInfo['delivery'];

            $contract = $delivery->getActiveContract();

            if ($contract) {

                $deliveredTimes = $contract->getDeliveredTimesByRegion($this->customer_zip, $this->customer_city, $this->customer_province);

                $response[] = [
                    'id' => $delivery->id,
                    'name' => $delivery->name,
                    'delivery_price' => $deliveryInfo['deliveryPrice'],
                    'express' => $delivery->express_delivery,
                    'min_delivery_days' => $delivery->express_delivery ? 0 : (empty($deliveredTimes['delivered_from']) && $deliveredTimes['delivered_from'] !== 0 ? null : $deliveredTimes['delivered_from']),
                    'max_delivery_days' => empty($deliveredTimes['delivered_to']) && $deliveredTimes['delivered_to'] !== 0 ? null : $deliveredTimes['delivered_to'],
                ];
            }
        }

        return $response;
    }

    /**
     * @return array
     */
    protected function prepareProducts()
    {
        $products = [];
        if ($this->products) {
            foreach ($this->products as $product) {
                $orderProduct = new OrderProduct([
                    'product_id' => $product['product_id'],
                    'price' => $product['price'] ?? 0,
                    'quantity' => $product['quantity'] ?? 0
                ]);
                $products[] = $orderProduct;
            }
        }
        return $products;
    }
}