<?php
namespace app\modules\api\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\api\models\query\ApiLogQuery;
use Yii;

/**
 * Class ApiLog
 * @property integer $id
 * @property string $type
 * @property integer $country_id
 * @property string $status
 * @property string $input_data
 * @property string $output_data
 * @property integer $created_at
 * @property \yii\db\ActiveQuery $country
 * @property integer $updated_at
 */
class ApiLog extends ActiveRecordLogUpdateTime
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';
    const STATUS_DUPLICATE = 'duplicate';

    const TYPE_LEAD = 'lead';
    const TYPE_CALL_CENTER = 'callcenter';
    const TYPE_DELIVERY = 'delivery';
    const TYPE_PRODUCT = 'product';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%api_log}}';
    }

    /**
     * @return ApiLogQuery
     */
    public static function find()
    {
        return new ApiLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_FAIL => Yii::t('common', 'С ошибкой'),
            self::STATUS_DUPLICATE => Yii::t('common', 'С ошибкой дублирования'),
            self::STATUS_SUCCESS => Yii::t('common', 'Выполненно'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_LEAD => Yii::t('common', 'Лиды'),
            self::TYPE_CALL_CENTER => Yii::t('common', 'Колл-центр'),
            self::TYPE_DELIVERY => Yii::t('common', 'Службы доставки'),
            self::TYPE_PRODUCT => Yii::t('common', 'Продукты'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение страны.'),
            ],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            ['type', 'in', 'range' => array_keys(self::getTypesCollection())],
            [['input_data', 'output_data'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'type' => Yii::t('common', 'Тип'),
            'status' => Yii::t('common', 'Статус'),
            'country_id' => Yii::t('common', 'Страна'),
            'input_data' => Yii::t('common', 'Входные данные'),
            'output_data' => Yii::t('common', 'Выходные данных'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
