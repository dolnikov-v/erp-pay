<?php
namespace app\modules\api\models;

use app\components\ModelTrait;
use app\models\Country;
use app\models\Source;
use app\modules\order\models\Order;
use Yii;
use yii\base\Model;
use app\modules\delivery\models\Delivery as DeliveryModel;
use app\models\Partner;

/**
 * Class Delivery
 * @package app\modules\api\models
 */
class Delivery extends Model
{
    use ModelTrait;

    const SCENARIO_LIST = 'list';
    const SCENARIO_CHECK = 'check';

    /**
     * @var string
     */
    public $country;

    /**
     * @var integer
     */
    public $active = 1;

    /**
     * @var integer
     */
    public $partner_id;

    /**
     * @var Country
     */
    protected $countryModel;

    /**
     * @var Partner
     */
    protected $partner;

    /**
     * @var string
     */
    public $source;

    /**
     * @var string
     */
    public $postal_code;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['country', 'required', 'message' => Yii::t('common', 'Необходимо указать страну.'), 'on' =>
                [
                    self::SCENARIO_LIST,
                    self::SCENARIO_CHECK
                ],
            ],
            ['country', 'validateCountry', 'on' =>
                [
                    self::SCENARIO_LIST
                ],
            ],
            ['country', 'validateCountryBrokerage', 'on' =>
                [
                    self::SCENARIO_CHECK,
                ],
            ],
            ['active', 'integer', 'on' =>
                [
                    self::SCENARIO_LIST,
                ],
            ],
            ['source', 'in', 'range' => [Source::SOURCE_ADCOMBO, Source::SOURCE_JEEMPO, Source::SOURCE_2WSTORE], 'message' => Yii::t('common', 'Неверное значение ресурса'), 'on' =>
                [
                    self::SCENARIO_LIST,
                ],
            ],
            ['partner_id', 'validatePartner', 'on' =>
                [
                    self::SCENARIO_LIST,
                    self::SCENARIO_CHECK,
                ],
            ],
            ['postal_code', 'validatePostal', 'on' =>
                [
                    self::SCENARIO_CHECK
                ],
            ],
            ['postal_code', 'required', 'message' => Yii::t('common', 'Необходимо указать почтовый индекс.'), 'on' =>
                [
                    self::SCENARIO_CHECK
                ],
            ]
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateCountry($attribute)
    {
        $this->countryModel = Country::find()
            ->bySlug($this->country)
            ->one();

        if (!$this->countryModel) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение страны.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validateCountryBrokerage($attribute)
    {
        $this->countryModel = Country::find()
            ->bySlug($this->country)
            ->one();

        if (!$this->countryModel) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение страны.'));
        }

        if (!$this->countryModel->brokerage_enabled) {
            $this->addError($attribute, Yii::t('common', 'Брокер не включён.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validatePostal($attribute)
    {

    }

    /**
     * @param string $attribute
     */
    public function validatePartner($attribute)
    {
        if ($this->partner_id) {

            $this->partner = Partner::find()
                ->where([
                    'foreign_id' => $this->partner_id,
                    'source_id' => Source::find()->where(['unique_system_name' => $this->source ?: Source::SOURCE_ADCOMBO])->select('id')->scalar(),
                ])
                ->one();

            if (!$this->partner) {
                $this->addError($attribute, Yii::t('common', 'Неверное значение партнера или ресурса.'));
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Доступность'),
        ];
    }

    /**
     * @return array
     */
    public function getList()
    {
        $this->setScenario(self::SCENARIO_LIST);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_DELIVERY;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {

            if (!$this->partner) {
                $this->partner = Partner::find()->def()->one();
            }

            $query = DeliveryModel::find()
                ->byCountryId($this->countryModel->id)
                ->orderBy(['id' => SORT_ASC]);

            if ($this->partner) {
                $query->byPartnerId($this->partner->id);
            }

            if ($this->active) {
                $query->active();
            }

            $deliveries = $query->all();

            foreach ($deliveries as $delivery) {
                $result[$delivery->id] = [
                    'id' => $delivery->id,
                    'name' => $delivery->name,
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }


    /**
     * @return array
     */
    public function checkPostal()
    {
        $this->setScenario(self::SCENARIO_CHECK);

        $result = [];

        if ($this->validate()) {

            if (!$this->partner) {
                $this->partner = Partner::find()->def()->one();
            }

            $query = DeliveryModel::find()
                ->byCountryId($this->countryModel->id);

            if ($this->partner) {
                $query->byPartnerId($this->partner->id);
            }

            if ($this->active) {
                $query->active();
            }

            $query->andWhere([DeliveryModel::tableName() . '.brokerage_active' => 1]);
            $query->orderBy([DeliveryModel::tableName() . '.sort_order' => SORT_ASC]);
            $deliveries = $query->all();

            foreach ($deliveries as $delivery) {
                if ($delivery->verifyZipCode($this->postal_code) !== false) {
                    $result[$delivery->id] = [
                        'id' => $delivery->id,
                        'name' => $delivery->name,
                    ];
                }
            }

            if (!$result) {
                $this->addError($this->postal_code, Yii::t('common', 'По такому индексу не доставляем.'));
            }
        }

        return $result;
    }
}
