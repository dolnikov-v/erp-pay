<?php
namespace app\modules\api\models;

use Yii;
use yii\base\Model;
use app\models\Country;
use app\models\Notification;

/**
 * Class SystemNotification
 * @property $trigger
 * @property $params
 * @property $countryId
 * @package app\modules\api\models
 */
class SystemNotification extends Model
{
    public
        $trigger,
        $params,
        $countryId = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['trigger', 'params'], 'required'],
            ['trigger', 'string', 'max' => 255],
            [['trigger'], 'exist', 'targetClass' => Notification::className(), 'targetAttribute' => ['trigger' => 'trigger']],
            [['countryId'], 'exist', 'targetClass' => Country::className(), 'targetAttribute' => ['countryId' => 'id']],
        ];
    }

    public function send()
    {
        Yii::$app->notification->send(
            $this->trigger,
            $this->params,
            $this->countryId
        );
    }

    public function fields()
    {
        return [
            "params" => "params",
        ];
    }
}