<?php
namespace app\modules\api\models\search;

use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use yii\data\ActiveDataProvider;

/**
 * Class LeadSearch
 * @package app\modules\api\models\search
 */
class LeadSearch extends Lead
{
    public $ids;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['ids'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
