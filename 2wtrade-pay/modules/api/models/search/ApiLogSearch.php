<?php
namespace app\modules\api\models\search;

use app\modules\api\models\ApiLog;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class LeadLogSearch
 * @package app\modules\api\models\search
 */
class ApiLogSearch extends ApiLog
{
    public $dateFrom;
    public $dateTo;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['country_id', 'integer'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param $params
     * @param string $type
     * @return ActiveDataProvider
     */
    public function search($params, $type = ApiLog::TYPE_LEAD)
    {

        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = ApiLog::find()->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->where(['type' => $type]);
        $query->andFilterWhere(['country_id' => $this->country_id]);
        $query->andFilterWhere(['status' => $this->status]);

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
            $query->andFilterWhere(['>=', 'created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
            $query->andFilterWhere(['<=', 'created_at', $dateTo + 86399]);
        }

        return $dataProvider;
    }
}
