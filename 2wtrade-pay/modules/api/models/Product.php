<?php
namespace app\modules\api\models;

use app\components\ModelTrait;
use app\models\Country;
use app\models\Product as ProductModel;
use app\models\Source;
use app\modules\storage\models\StorageProduct;
use app\modules\storage\models\Storage;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\models\Currency;
use app\models\Partner;
use app\models\PartnerCountry;
use Yii;
use yii\base\Model;

/**
 * Class Product
 * @package app\modules\api\models
 *
 * @property array $productsReport
 * @property array $productsOnDelivery
 * @property array $storageProducts
 * @property array $products
 */
class Product extends Model
{
    use ModelTrait;

    const SCENARIO_STORAGE_PRODUCTS = 'storage-products';
    const SCENARIO_PRODUCTS_ON_DELIVERY = 'products-on-delivery';
    const SCENARIO_PRODUCTS_REPORT = 'products-report';
    const SCENARIO_PRODUCTS_CREATE = 'products-create';
    const REPORT_TYPE_SHIP = 'ship';
    const REPORT_TYPE_SALE = 'sale';

    /**
     * @var string
     */
    public $country;

    /**
     * @var array|integer
     */
    public $id;

    public $name;

    public $sku;

    /**
     * @var Country
     */
    protected $countryModel;

    /**
     * @var integer
     */
    public $delivery;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $date_from;

    /**
     * @var string
     */
    public $date_to;

    /**
     * @var Delivery
     */
    protected $deliveryModel;

    /**
     * @var Partner
     */
    protected $partner;


    /**
     * @var integer
     */
    public $partner_id;

    /**
     * @var string
     */
    public $source;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['country', 'required', 'message' => Yii::t('common', 'Необходимо указать страну.'), 'on' => [
                self::SCENARIO_STORAGE_PRODUCTS,
                self::SCENARIO_PRODUCTS_ON_DELIVERY,
                self::SCENARIO_PRODUCTS_REPORT,
            ],
            ],
            [
                ['name', 'sku'],
                'required',
                //'message' => 'Product name is required',
                'on' => self::SCENARIO_PRODUCTS_CREATE,
            ],
            ['delivery', 'required', 'message' => Yii::t('common', 'Необходимо указать службу доставки.'), 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ]
            ],
            ['type', 'required', 'message' => Yii::t('common', 'Необходимо указать тип.'), 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ]
            ],
            ['date_from', 'required', 'message' => Yii::t('common', 'Необходимо указать дату С.'), 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ]
            ],
            ['country', 'validateCountry', 'on' => [
                self::SCENARIO_PRODUCTS_ON_DELIVERY,
                self::SCENARIO_PRODUCTS_REPORT,
            ],
            ],
            ['delivery', 'validateDelivery', 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ],
            ],
            ['type', 'validateType', 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ],
            ],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('common', 'Неверный формат даты'), 'on' => [
                self::SCENARIO_PRODUCTS_REPORT,
            ],
            ],
            ['source', 'in', 'range' => [Source::SOURCE_ADCOMBO, Source::SOURCE_JEEMPO, Source::SOURCE_2WSTORE], 'message' => Yii::t('common', 'Неверное значение ресурса'), 'on' => [
                self::SCENARIO_STORAGE_PRODUCTS,
            ],
            ],
            ['partner_id', 'validatePartner', 'on' => [
                self::SCENARIO_STORAGE_PRODUCTS,
            ],
            ],
            ['country', 'validateCountryWithPartner', 'on' => [
                self::SCENARIO_STORAGE_PRODUCTS,
            ],
            ],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateCountry($attribute)
    {
        $this->countryModel = Country::find()
            ->bySlug($this->country)
            ->one();

        if (!$this->countryModel) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение страны.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validateDelivery($attribute)
    {
        $this->deliveryModel = Delivery::find()
            ->where(['id' => $this->delivery])
            ->one();

        if (!$this->deliveryModel) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение cлужбы доставки.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validateType($attribute)
    {
        if ($this->type != self::REPORT_TYPE_SHIP && $this->type != self::REPORT_TYPE_SALE ) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение типа.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validatePartner($attribute)
    {
        if ($this->partner_id) {

            $this->partner = Partner::find()
                ->where([
                    'foreign_id' => $this->partner_id,
                    'source_id' => Source::find()->where(['unique_system_name' => $this->source ?: Source::SOURCE_ADCOMBO])->select('id')->scalar(),
                ])
                ->one();

            if (!$this->partner) {
                $this->addError($attribute, Yii::t('common', 'Неверное значение партнера или ресурса.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validateCountryWithPartner($attribute)
    {
        if ($this->partner) {
            $partnerCountry = PartnerCountry::find()
                ->innerJoinWith(['country'])
                ->where([PartnerCountry::tableName() . '.partner_id' => $this->partner->id])
                ->andWhere([Country::tableName() . '.char_code' => $this->country])
                ->one();

            if ($partnerCountry === null) {
                $partnerCountry = PartnerCountry::find()
                    ->innerJoinWith(['partner', 'country'])
                    ->where([Partner::tableName() . '.default' => 1])
                    ->andWhere([Country::tableName() . '.char_code' => $this->country])
                    ->one();
            }

            if ($partnerCountry === null) {
                $this->addError($attribute, Yii::t('common', 'Нет такой страны для этого партнера и для партнера по умолчанию.'));
            } else {
                $this->countryModel = $partnerCountry->country;
            }
        } else {
            $this->countryModel = Country::find()
                ->byCharCode($this->country)
                ->one();

            if (!$this->countryModel) {
                $this->addError($attribute, Yii::t('common', 'Неверное значение страны.'));
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_PRODUCT;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $products = ProductModel::find()
                ->orderBy(['id' => SORT_ASC])
                ->all();

            foreach ($products as $product) {
                $result[$product->id] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'default_value' => $product->getDefaultId()
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getStorageProducts()
    {
        $this->setScenario(self::SCENARIO_STORAGE_PRODUCTS);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_PRODUCT;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {

            if (!$this->partner) {
                $this->partner = Partner::find()->def()->one();
            }

            $products = StorageProduct::find()
                ->joinWith([
                    'product',
                    'storage',
                    'storage.partnerCountry'
                ])
                ->select([
                    'id' => ProductModel::tableName() . '.id',
                    'name' => ProductModel::tableName() . '.name',
                    'amount' => 'SUM(' . StorageProduct::tableName() . '.balance)',
                    'product_id' => ProductModel::tableName() . '.id',
                    'storage_id' => StorageProduct::tableName() . '.storage_id',
                ])
                ->where([
                    Storage::tableName() . '.country_id' => $this->countryModel->id,
                    PartnerCountry::tableName() . '.partner_id' => $this->partner->id,
                ])
                ->groupBy([
                    ProductModel::tableName() . '.id',
                ])
                ->orderBy([ProductModel::tableName() . '.id' => SORT_ASC])
                ->asArray()
                ->all();

            foreach ($products as $product) {
                $result[$product['id']] = [
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'amount' => $product['amount'],
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getProductsOnDelivery()
    {
        $this->setScenario(self::SCENARIO_PRODUCTS_ON_DELIVERY);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_PRODUCT;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $products = OrderProduct::find()
                ->joinWith([
                    'order',
                    'product',
                ])
                ->where([Order::tableName() . '.country_id' => $this->countryModel->id])
                ->andWhere(['IN', Order::tableName() . '.status_id', [
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                    OrderStatus::STATUS_DELIVERY_REFUND,
                    OrderStatus::STATUS_DELIVERY_LOST,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ]])
                ->groupBy([
                    ProductModel::tableName() . '.id',
                ])
                ->orderBy([ProductModel::tableName() . '.id' => SORT_ASC])
                ->all();

            foreach ($products as $product) {
                $result[$product->product->id] = [
                    'id' => $product->product->id,
                    'name' => $product->product->name,
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getProductsReport()
    {
        $this->setScenario(self::SCENARIO_PRODUCTS_REPORT);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_PRODUCT;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {

            if (!$this->date_to) $this->date_to = date("Y-m-d");

            $timeFrom = strtotime($this->date_from);
            $timeTo = strtotime($this->date_to);

            $statusArray = null;
            if ($this->type == self::REPORT_TYPE_SALE) {
                $statusArray = [
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED
                ];
            }

            if ($this->type == self::REPORT_TYPE_SHIP) {
                $statusArray = [
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                    OrderStatus::STATUS_DELIVERY_REFUND,
                    OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                ];
            }

            $products = Order::find()
                ->joinWith([
                    'deliveryRequest',
                    'products',
                    'country',
                    'country.currency'
                ])
                ->select([
                    'id' => ProductModel::tableName() . '.id',
                    'name' => ProductModel::tableName() . '.name',
                    'count' => 'SUM(' . OrderProduct::tableName() . '.quantity)',
                    'sum' => 'SUM(' . OrderProduct::tableName() . '.quantity * ' . OrderProduct::tableName() . '.price)',
                    'country_id' => Order::tableName() . '.country_id',
                    'currency' => Currency::tableName() . '.char_code'
                ])
                ->where([Order::tableName() . '.country_id' => $this->countryModel->id])
                ->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $this->deliveryModel->id])
                ->andWhere(['IN', Order::tableName() . '.status_id', $statusArray])
                ->andFilterWhere(['>=', '(' . DeliveryRequest::tableName() . '.approved_at)', $timeFrom])
                ->andFilterWhere(['<=', '(' . DeliveryRequest::tableName() . '.approved_at)', $timeTo + 86399])
                ->groupBy(ProductModel::tableName() . '.id')
                ->orderBy('name')
                ->asArray()->all();

            foreach ($products as $product) {

                $result[$product['id']] = [
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'count' => $product['count'],
                    'sum' => $product['sum'],
                    'currency' => $product['currency']
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @param $productPost
     * @return bool
     */
    public function create($productPost)
    {
        $this->setScenario(Product::SCENARIO_PRODUCTS_CREATE);
        $this->load(['Product' => $productPost]);
        if (!$this->validate()) {
            return false;
        }
        $productModel = new ProductModel();
        $productModel->name = $this->name;
        $productModel->sku = $this->sku;
        $productModel->use_days = 999;
        if (!$productModel->save()) {
            $this->addErrors($productModel->getErrors());
            return false;
        }
        return $productModel->id;
    }

    /**
     * @param $id
     * @return null|ProductModel
     */
    public static function getProductById($id)
    {
        return ProductModel::findOne(['id' => $id]);
    }
}
