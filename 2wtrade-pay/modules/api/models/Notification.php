<?php
namespace app\modules\api\models;

use app\modules\order\models\OrderNotificationRequest as Model;

class Notification extends Model
{
    /**
     * @return array
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return parent::attributeHints();
    }
}