<?php

namespace app\modules\api\models;

use app\modules\api\controllers\v1\LeadController;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Lead as LeadModel;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class LeadStatus
 * @package app\modules\api\models
 */
class LeadStatus extends Model
{
    const STATUS_HOLD = 'hold';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_TRASH = 'trash';
    const STATUS_UNCALLED = 'uncalled';
    const STATUS_RECALL = 'recall';

    /**
     * @var array Маппинг статуса заказа к стату лида
     */
    public static $mapOrderStatusToLeadStatus = [
        OrderStatus::STATUS_SOURCE_LONG_FORM => LeadModel::STATUS_HOLD,
        OrderStatus::STATUS_SOURCE_SHORT_FORM => LeadModel::STATUS_HOLD,
        OrderStatus::STATUS_CC_POST_CALL => LeadModel::STATUS_HOLD,
        OrderStatus::STATUS_CC_RECALL => LeadModel::STATUS_RECALL,
        OrderStatus::STATUS_CC_FAIL_CALL => LeadModel::STATUS_UNCALLED,
        OrderStatus::STATUS_CC_APPROVED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_CC_REJECTED => LeadModel::STATUS_CANCELLED,
        OrderStatus::STATUS_LOG_ACCEPTED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_GENERATED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_SET => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_PASTED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_ACCEPTED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_BUYOUT => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_DENIAL => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_RETURNED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_REJECTED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_REDELIVERY => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOGISTICS_REJECTED => LeadModel::STATUS_CANCELLED,
        OrderStatus::STATUS_DELIVERY_DELAYED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_CC_TRASH => LeadModel::STATUS_TRASH,
        OrderStatus::STATUS_CC_DOUBLE => LeadModel::STATUS_TRASH,
        OrderStatus::STATUS_DELIVERY_REFUND => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOGISTICS_ACCEPTED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_LOST => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_CC_INPUT_QUEUE => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_DELIVERY_PENDING => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_DEFERRED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_CLIENT_REFUSED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_SENT => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_LOG_RECEIVED => LeadModel::STATUS_CONFIRMED,
        OrderStatus::STATUS_CC_CHECKING => LeadModel::STATUS_CONFIRMED,
    ];

    /**
     * @param array $foreignIds
     * @return array
     * @throws \yii\mongodb\Exception
     * @throws \Exception
     */
    public static function getStatusByForeignIds($foreignIds)
    {
        $result = [];

        if ($foreignIds) {
            $orders = Order::find()
                ->with(['orderLogStatuses', 'callCenterRequest.callCenter', 'deliveryRequest.delivery', 'lead'])
                ->select([
                    Order::tableName() . '.id',
                    'foreign_id',
                    'source_form',
                    'status_id',
                    'comment',
                    'source_confirmed',
                ])
                ->where(['!=', 'foreign_id', 0])
                ->andWhere(['source_id' => Yii::$app->apiUser->source->id])
                ->byForeignIds($foreignIds)
                ->all();
            foreach ($orders as $order) {
                if ($order->lead instanceof LeadModel) {
                    if (!in_array($order->lead->source_status, [LeadModel::STATUS_CONFIRMED, LeadModel::STATUS_TRASH])) {
                        if ($order->status_id == OrderStatus::STATUS_DELIVERY_REJECTED && $order->source_form == Order::TYPE_FORM_LONG) {
                            $logOrderStatuses = ArrayHelper::getColumn($order->orderLogStatuses, 'new');

                            if (in_array(OrderStatus::STATUS_CC_APPROVED, $logOrderStatuses)) {
                                $status = LeadModel::STATUS_CONFIRMED;
                            } else {
                                $status = LeadModel::STATUS_HOLD;
                            }
                        } else {
                            $status = isset(self::$mapOrderStatusToLeadStatus[$order->status_id]) ? self::$mapOrderStatusToLeadStatus[$order->status_id] : LeadModel::STATUS_HOLD;
                        }

                        if ($order->lead->source_status != $status) {
                            $order->lead->source_status = $status;
                            $order->lead->save(false, ['source_status']);
                        }
                    } else {
                        $status = $order->lead->source_status;
                    }

                    if ($status == LeadModel::STATUS_CONFIRMED && $order->source_confirmed == 0) {
                        $order->source_confirmed = 1;
                        $order->save(false, ['source_confirmed']);
                    }
                }

                $sub_status = null;
                //В чате adcombo-дежурка Anastasia Kondratieva сказала, что комментарий им не нужен 16.10.2018
                $comment = null;

                if ($order->callCenterRequest->callCenter->is_amazon_query) {
                    if(!empty($order->callCenterRequest->foreign_substatus)){
                        //todo использовать константу для REJECT статуса КЦ
                        if ($order->callCenterRequest->foreign_status == 5) {
                            //Если не нашли подходящий подстатус то, ставим other reason и добавляем комментарий из КЦ
                            if (!$sub_status = CallCenterRequest::getAdcomboMappingStatus($order->callCenterRequest->foreign_substatus)) {
                                $sub_status = CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON;
                            }
                            if ($sub_status == CallCenterRequest::ADCOMBO_REJECT_REASON_OTHER_REASON) {
                                $comment = $order->callCenterRequest ? $order->callCenterRequest->comment : '';
                            }
                        }

                        //todo использовать константу для TRASH статуса КЦ
                        if ($order->callCenterRequest->foreign_status == 6) {
                            //Если не нашли подходящий подстатус то, ставим other reason и добавляем комментарий из КЦ
                            if (!$sub_status = CallCenterRequest::getAdcomboMappingStatus($order->callCenterRequest->foreign_substatus)) {
                                $sub_status = CallCenterRequest::ADCOMBO_TRASH_REASON_OTHER_REASON;
                            }
                            if ($sub_status == CallCenterRequest::ADCOMBO_TRASH_REASON_OTHER_REASON) {
                                $comment = $order->callCenterRequest ? $order->callCenterRequest->comment : '';
                            }
                        }
                    }
                } else {
                    //для старых КЦ в sub_status отправляем комментарий КЦ
                    $sub_status = $order->callCenterRequest->comment;
                }

                $data = [
                    'order_id' => $order->foreign_id,
                    'status' => $status,
                    'status_id' => $order->status_id,
                    'sub_status' => $sub_status,
                    'comment' => $comment,//($order->callCenterRequest ? $order->callCenterRequest->comment : ''),
                    'delivery' => ($order->deliveryRequest ? $order->deliveryRequest->delivery->name : ''),
                    'delivery_id' => ($order->deliveryRequest ? $order->deliveryRequest->delivery->id : ''),
                    'tracking' => ($order->deliveryRequest && $order->deliveryRequest->tracking ? $order->deliveryRequest->tracking : ''),
                    'attempts' => $order->callCenterRequest ? $order->callCenterRequest->getAttemptDates() : [],
                ];

                /*if($order->callCenterRequest->callCenter->is_amazon_query){
                    $data['foreign_substatus_code'] = $order->callCenterRequest ? $order->callCenterRequest->foreign_substatus : null;
                    $data['foreign_substatus'] = $order->callCenterRequest ? CallCenterRequest::getTextForeignSubStatuses($order->callCenterRequest->foreign_substatus) : null;
                }*/

                $result[] = $data;
            }
        }

        return $result;
    }


    /**
     * @param array $foreignIds
     * @return array
     */
    public static function getBuyoutByForeignIds($foreignIds)
    {

        $result = [];

        if ($foreignIds) {
            $orders = Order::find()
                ->select([
                    Order::tableName() . '.id',
                    'foreign_id',
                    'status_id'
                ])
                ->where(['!=', 'foreign_id', 0])
                ->andWhere(['source_id' => Yii::$app->apiUser->source->id])
                ->byForeignIds($foreignIds)
                ->all();

            foreach ($orders as $order) {

                if (in_array($order->status_id, OrderStatus::getBuyoutList())) {
                    $buyout = true;
                } elseif ( in_array($order->status_id, OrderStatus::getNotBuyoutDeliveryList())) {
                    $buyout = false;
                } else {
                    $buyout = null;
                }

                $result[] = [
                    'order_id' => $order->foreign_id,
                    'buyout' => $buyout
                ];
            }
        }
        return $result;
    }

    /**
     * @param array $foreignIds
     * @return array
     */
    public static function cancelOrdersByForeignIds($foreignIds)
    {
        $result = [];

        if ($foreignIds) {
            $orders = Order::find()
                ->with(['callCenterRequest', 'deliveryRequest', 'country'])
                ->where(['!=', 'foreign_id', 0])
                ->andWhere(['source_id' => Yii::$app->apiUser->source->id])
                ->byForeignIds($foreignIds)
                ->all();

            foreach ($orders as $order) {
                $response = [
                    'status' => 'fail'
                ];
                $returnCode = LeadController::RETURN_CODE_SUCCESS;
                try {
                    if ($order->payment_type == Order::ORDER_PAYMENT_TYPE_PREPAID) {
                        if ($order->canChangeStatusTo(OrderStatus::STATUS_CLIENT_REFUSED)) {
                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                $order->status_id = OrderStatus::STATUS_CLIENT_REFUSED;
                                if ($order->callCenterRequest) {
                                    $order->callCenterRequest->status = CallCenterRequest::STATUS_DONE;
                                    if (!$order->callCenterRequest->save()) {
                                        $returnCode = LeadController::RETURN_CODE_ERROR_CC_REQUEST;
                                        throw new \Exception($order->callCenterRequest->getFirstErrorAsString());
                                    }
                                }
                                if ($order->deliveryRequest) {
                                    $order->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                                    if (!$order->deliveryRequest->save()) {
                                        $returnCode = LeadController::RETURN_CODE_ERROR_D_REQUEST;
                                        throw new \Exception($order->deliveryRequest->getFirstErrorAsString());
                                    }
                                }
                                if (!$order->save()) {
                                    $returnCode = LeadController::RETURN_CODE_ERROR_ORDER_SAVE;
                                    throw new \Exception($order->getFirstErrorAsString());
                                }
                                $transaction->commit();
                                $response['status'] = 'success';
                                $response['result_code'] = $returnCode;
                            } catch (\Throwable $e) {
                                $transaction->rollBack();
                                $returnCode = LeadController::RETURN_CODE_ERROR_UNEXPECTED;
                                throw $e;
                            }
                        } else {
                            $returnCode = LeadController::RETURN_CODE_ERROR_CANCEL_STATUS;
                            throw new \Exception("Заказ, находящийся в статусе \"{$order->status->name}\", нельзя отменить.");
                        }
                    } else {
                        $returnCode = LeadController::RETURN_CODE_ERROR_NOT_PREPAID;
                        throw new \Exception('Нельзя отменить заказ, который не является предоплаченным.');
                    }
                } catch (\Throwable $e) {
                    $response['status'] = 'fail';
                    $response['result_code'] = $returnCode;
                    $response['message'] = $e->getMessage();
                }
                $result[$order->foreign_id] = $response;
            }
        }

        return $result;
    }

    /***
     * @return array
     */
    public static function stateLabels()
    {
        return [
            LeadStatus::STATUS_HOLD => Yii::t('common', 'В процессе'),
            LeadStatus::STATUS_CONFIRMED => Yii::t('common', 'Подтвежден'),
            LeadStatus::STATUS_TRASH => Yii::t('common', 'Мусор'),
            LeadStatus::STATUS_CANCELLED => Yii::t('common', 'Отменен'),
            LeadStatus::STATUS_UNCALLED => Yii::t('common', 'Недозвон'),
            LeadStatus::STATUS_RECALL => Yii::t('common', 'Перезвон'),
        ];
    }
}
