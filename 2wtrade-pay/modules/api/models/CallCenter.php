<?php

namespace app\modules\api\models;

use app\components\ModelTrait;
use app\models\Partner;
use app\models\PartnerCountry;
use app\modules\callcenter\components\api\Lead as CC_Lead;
use app\helpers\PayStatus;
use app\models\Country;
use app\modules\callcenter\models\CallCenter as CallCenterModel;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLog;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Model;

/**
 * Class CallCenter
 * @package app\modules\api\models
 *
 * @property array $canUpdate
 * @property array $statuses
 * @property array $ids
 * @property array $statusesOnOrders
 * @property array $list
 */
class CallCenter extends Model
{
    use ModelTrait;

    const SCENARIO_IDS = 'ids';
    const SCENARIO_STATUSES = 'statuses';
    const SCENARIO_CAN_UPDATE = 'can-update';
    const SCENARIO_UPDATE_ORDER = 'update-order';
    const SCENARIO_NEW_ORDER = 'new-order';
    const SCENARIO_LIST = 'list';
    const SCENARIO_STATUSES_ON_ORDERS = 'statuses-on-orders';

    const CAN_UPDATE = 'update';
    const CAN_DELETE = 'delete';

    const NEW_ORDER_STATUSES = [3, 4, 5, 25];

    /**
     * @var string
     */
    public $country;

    /**
     * @var integer
     */
    public $partner_id;

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @var array|integer
     */
    public $id;

    /**
     * @var integer
     */
    public $active = 1;

    /**
     * @var Country
     */
    protected $countryModel;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'country',
                'required',
                'message' => Yii::t('common', 'Необходимо указать страну.'),
                'on' => [
                    self::SCENARIO_IDS,
                    self::SCENARIO_STATUSES,
                    self::SCENARIO_LIST,
                ],
            ],
            [
                'id',
                'required',
                'message' => Yii::t('common', 'Необходимо указать номера заявок.'),
                'on' => [
                    self::SCENARIO_IDS,
                    self::SCENARIO_STATUSES,
                    self::SCENARIO_STATUSES_ON_ORDERS
                ]
            ],
            [
                'id',
                'required',
                'message' => Yii::t('common', 'Необходимо указать номера заказов.'),
                'on' => [
                    self::SCENARIO_CAN_UPDATE,
                ],
            ],
            [
                'id',
                'each',
                'rule' => ['integer'],
                'on' => [
                    self::SCENARIO_IDS,
                    self::SCENARIO_STATUSES,
                    self::SCENARIO_CAN_UPDATE,
                    self::SCENARIO_STATUSES_ON_ORDERS
                ],
            ],
            [
                'id',
                'integer',
                'on' => [
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ],
            [
                'id',
                'validateNewOrder',
                'on' => [
                    self::SCENARIO_NEW_ORDER,
                ],
            ],
            [
                'country',
                'validateCountry',
                'on' => [
                    self::SCENARIO_IDS,
                    self::SCENARIO_STATUSES,
                    self::SCENARIO_LIST,
                ],
                'when' => function ($model) {
                    return $model->partner_id == null;
                }
            ],
            [
                'partner_id',
                'validatePartner',
                'on' => [
                    self::SCENARIO_IDS,
                    self::SCENARIO_STATUSES,
                    self::SCENARIO_LIST,
                ]
            ],
            [
                'active',
                'integer',
                'on' => [
                    self::SCENARIO_LIST,
                ],
            ],
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCountry($attribute, $params)
    {
        $this->countryModel = Country::find()
            ->bySlug($this->country)
            ->one();

        if (!$this->countryModel) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение страны.'));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatePartner($attribute, $params)
    {
        $partnerCountry = PartnerCountry::find()
            ->innerJoinWith(['partner', 'country'])
            ->where([Partner::tableName() . '.foreign_id' => $this->partner_id])
            ->andWhere([Country::tableName() . '.char_code' => $this->country])
            ->one();

        if ($partnerCountry === null) {
            $partnerCountry = PartnerCountry::find()
                ->innerJoinWith(['partner', 'country'])
                ->where([Partner::tableName() . '.default' => 1])
                ->andWhere([Country::tableName() . '.char_code' => $this->country])
                ->one();
        }

        if ($partnerCountry === null) {
            $this->addError($attribute, Yii::t('common', 'Нет партнера по умолчанию.'));
        } else {
            $this->countryModel = $partnerCountry->country;
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Доступность'),
        ];
    }

    /**
     * @return array
     */
    public function getIds()
    {
        $this->setScenario(self::SCENARIO_IDS);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $apiLog->country_id = $this->countryModel->id;

            $requests = CallCenterRequest::find()
                ->joinWith('callCenter')
                ->where([CallCenterModel::tableName() . '.country_id' => $this->countryModel->id])
                ->andWhere(['in', 'foreign_id', $this->id])
                ->all();

            foreach ($requests as $request) {
                $result[$request->foreign_id] = $request->order_id;

                // Если еще установлен флаг, что ожидаем, когда колл-центр заберет заказ
                if ($request->foreign_waiting) {
                    $request->foreign_waiting = 0;
                    $request->save(
                        false,
                        ['foreign_waiting']
                    ); // Немного трудозатратно сохранять каждую запись, но нам нужны логи изменения
                }
            }

            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
            $apiLog->status = ApiLog::STATUS_SUCCESS;
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        $this->setScenario(self::SCENARIO_STATUSES);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $apiLog->country_id = $this->countryModel->id;

            $requests = CallCenterRequest::find()
                ->joinWith(
                    [
                        'callCenter',
                        'order',
                    ]
                )
                ->where([CallCenterModel::tableName() . '.country_id' => $this->countryModel->id])
                ->andWhere(['in', CallCenterRequest::tableName() . '.foreign_id', $this->id])
                ->all();

            foreach ($requests as $request) {
                $result[$request->foreign_id] = $request->order->status_id;
            }

            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
            $apiLog->status = ApiLog::STATUS_SUCCESS;
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getStatusesOnOrders()
    {
        $this->setScenario(self::SCENARIO_STATUSES_ON_ORDERS);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {

            $requests = Order::find()
                ->where(['in', Order::tableName() . '.id', $this->id])
                ->all();

            foreach ($requests as $request) {
                $result[$request->id] = $request->status_id;
            }

            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
            $apiLog->status = ApiLog::STATUS_SUCCESS;
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function getCanUpdate()
    {
        $this->setScenario(self::SCENARIO_CAN_UPDATE);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $orders = Order::find()
                ->joinWith('deliveryRequest')
                ->where(['in', Order::tableName() . '.id', $this->id])
                ->all();

            foreach ($orders as $order) {
                $apiLog->country_id = $order->country_id;

                $actions = self::getAllowedActions($order);
                $result[$order->id] = in_array(self::CAN_UPDATE, $actions) ? 1 : 0;
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @return array
     */
    public function updateOrder()
    {
        $this->setScenario(self::SCENARIO_UPDATE_ORDER);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $order = Order::find()
                ->joinWith(
                    [
                        'callCenterRequest',
                        'deliveryRequest',
                    ]
                )
                ->where([Order::tableName() . '.id' => $this->id])
                ->one();

            if ($order) {
                $apiLog->country_id = $order->country_id;

                $result['order_id'] = $order->id;

                if ($order->callCenterRequest) {
                    $result['call_center_request_id'] = $order->callCenterRequest->id;
                }

                $actions = self::getAllowedActions($order);

                $transaction = Yii::$app->db->beginTransaction();

                if (in_array(self::CAN_DELETE, $actions)) {
                    // Удаляем заявку из курьерки
                    if (isset($order->deliveryRequest)) {
                        if (!$order->deliveryRequest->delete()) {
                            $this->addError('common', 'Не удалось удалить заявку.');
                        }
                    }

                    // Обновляем заявку в колл-центре и сам заказ
                    $this->resetCallCenterRequestAndOrder($order);
                } elseif (in_array(self::CAN_UPDATE, $actions)) {
                    // Обновляем заявку в колл-центре и сам заказ
                    $this->resetCallCenterRequestAndOrder($order);
                } else {
                    $this->addError('common', 'Этот заказ нельзя изменить.');
                }

                if ($this->hasErrors()) {
                    $result['errors'] = $this->getErrorsAsArray();

                    $transaction->rollBack();
                    $apiLog->status = ApiLog::STATUS_FAIL;
                } else {
                    $transaction->commit();
                    $apiLog->status = ApiLog::STATUS_SUCCESS;
                }
            } else {
                $result['errors'] = ['Заказ не найден.'];
                $apiLog->status = ApiLog::STATUS_FAIL;
            }

            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function Order($data)
    {
        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($data);
        $apiLog->country_id = $this->countryId;
        $apiLog->status = ApiLog::STATUS_FAIL;

        $request = CallCenterRequest::findOne(['order_id' => $this->id]);

        if (($request instanceof CallCenterRequest) && CC_Lead::compareAndSaveRequest($request, $data)) {
            $foreignStatus = CallCenterRequest::find()->select('foreign_status')->where(['id' => $this->id])->scalar();
            if ($data['status'] == $foreignStatus) {
                $apiLog->status = ApiLog::STATUS_SUCCESS;
                $apiLog->save();
                return true;
            }
        }

        $apiError = CallCenterRequest::find()->select('api_error')->where(['id' => $this->id])->scalar();
        $this->addError('id', $apiError ? $apiError : 'Не удалось сохранить заказ');
        $apiLog->save();
        return false;
    }

    /**
     * @return array
     */
    public function getList()
    {
        $this->setScenario(self::SCENARIO_LIST);

        $result = [];

        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_CALL_CENTER;
        $apiLog->input_data = json_encode($this->attributes, JSON_UNESCAPED_UNICODE);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if ($this->validate()) {
            $query = CallCenterModel::find()
                ->byCountryId($this->countryModel->id)
                ->orderBy(['id' => SORT_ASC]);

            if ($this->active) {
                $query->active();
            }

            $callCenters = $query->all();

            foreach ($callCenters as $callCenter) {
                $result[$callCenter->id] = [
                    'id' => $callCenter->id,
                    'name' => $callCenter->name,
                ];
            }

            $apiLog->status = ApiLog::STATUS_SUCCESS;
            $apiLog->output_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            $apiLog->output_data = json_encode($this->getErrorsAsArray(), JSON_UNESCAPED_UNICODE);
        }

        $apiLog->save();

        return $result;
    }

    /**
     * @param Order $order
     * @return array
     */
    protected static function getAllowedActions($order)
    {
        $actions = [];
        //обновляем данные, чтобы иметь конечную инфу по заказу
        $order->refresh();

        //если заказ улетел в адкомбо, запрещаем какие-либо действия с ним
        if ($order->source_confirmed === 1) {
        }// Если создана заявка в курьерку
        elseif (isset($order->deliveryRequest)) {
            // Если заявка в курьерке еще не отправлялась или с ошибкой
            if ($order->deliveryRequest->status == DeliveryRequest::STATUS_PENDING || $order->deliveryRequest->status == DeliveryRequest::STATUS_ERROR) {
                // Если в истории изменения заказа есть информация, что заказ проходил через логистику
                $logisticAccept = OrderLog::find()
                    ->where(['order_id' => $order->id])
                    ->andWhere(['field' => 'status_id'])
                    ->andWhere(
                        [
                            'or',
                            ['new' => OrderStatus::STATUS_LOG_ACCEPTED],
                            ['new' => OrderStatus::STATUS_LOG_GENERATED]
                        ]
                    )
                    ->exists();

                if (!$logisticAccept) {
                    $actions[] = self::CAN_UPDATE;
                    $actions[] = self::CAN_DELETE;
                }
            }
        } else {
            $payStatuses = [
                PayStatus::STATUS_SHIPPED,
                PayStatus::STATUS_BUYOUT,
                PayStatus::STATUS_NOT_BUYOUT,
            ];

            if (in_array($order->status_id, PayStatus::getStatusesByPayStatuses($payStatuses))) {

            } else {
                $actions[] = self::CAN_UPDATE;
            }
        }
        return $actions;
    }


    /**
     * @param Order $order
     */
    protected function resetCallCenterRequestAndOrder(
        $order
    )
    {
        // Меняем статус у заявки в колл-центре
        if (!$this->hasErrors() && $order->callCenterRequest) {
            $order->callCenterRequest->status = CallCenterRequest::STATUS_IN_PROGRESS;
            $order->callCenterRequest->foreign_status = 1;
            $order->callCenterRequest->foreign_substatus = null;

            if (!$order->callCenterRequest->save(false, ['status', 'foreign_status', 'foreign_substatus'])) {
                $this->addError('common', 'Не удалось сменить статус заявке.');
            }
        }

        // Меняем статус заказу
        if (!$this->hasErrors()) {
            $order->status_id = OrderStatus::STATUS_CC_POST_CALL;
            $order->call_center_again = 1;

            if (!$order->save(false, ['status_id', 'call_center_again'])) {
                $this->addError('common', 'Не удалось сменить статус заказу.');
            }
        }
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function validateNewOrder($attribute)
    {
        if (!Order::find()->where(['id' => $this->id])->exists()) {
            $this->addError($attribute, Yii::t('common', 'Заказ не найден.'));
            return false;
        }
        if (!Order::find()->where(['id' => $this->id])->andWhere(['status_id' => self::NEW_ORDER_STATUSES])->exists()) {
            $this->addError($attribute, Yii::t('common', 'Неверный статус заказа.'));
            return false;
        }
        if (!CallCenterRequest::find()->where(['order_id' => $this->id])->exists()) {
            $this->addError($attribute, Yii::t('common', 'Нет заявки в КЦ.'));
            return false;
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    public static function getBuyoutByCallCenterIds($token, $ids)
    {
        $result = [];

        if ($ids) {
            $orders = Order::find()
                ->select([
                    'ccr_foreign_id' => CallCenterRequest::tableName() . '.foreign_id',
                    'cccr_foreign_id' => CallCenterCheckRequest::tableName() . '.foreign_id',
                    Order::tableName() . '.status_id'
                ])
                ->leftJoin(CallCenterRequest::tableName(), Order::tableName() . '.id = ' . CallCenterRequest::tableName() . '.order_id')
                ->leftJoin(CallCenterCheckRequest::tableName(), Order::tableName() . '.id = ' . CallCenterCheckRequest::tableName() . '.order_id')
                ->leftJoin(CallCenterModel::tableName(), CallCenterModel::tableName() . '.id = ' . CallCenterRequest::tableName() . '.call_center_id')
                ->where([CallCenterModel::tableName() . '.token' => $token])
                ->andWhere(['OR', [CallCenterRequest::tableName() . '.foreign_id' => $ids], [CallCenterCheckRequest::tableName() . '.foreign_id' => $ids]])
                ->createCommand()->queryAll();

            foreach ($orders as $order) {
                if (in_array($order['status_id'], OrderStatus::getBuyoutList())) {
                    $buyout = true;
                } elseif (in_array($order['status_id'], OrderStatus::getNotBuyoutDeliveryList())) {
                    $buyout = false;
                } else {
                    $buyout = null;
                }

                $result[] = [
                    'order_id' => $order['ccr_foreign_id'] ?? $order['cccr_foreign_id'],
                    'buyout' => $buyout
                ];
            }
        }
        return $result;
    }

}
