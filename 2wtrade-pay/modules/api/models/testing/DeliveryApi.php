<?php
namespace app\modules\api\models\testing;

use app\models\Country;
use yii\base\InvalidParamException;
use HttpInvalidParamException;
use yii\base\Model;
use Yii;
use yii\httpclient\Client;


class DeliveryApi extends Model
{
    const CHAR_CODE_TEST_COUNTRY = 'XX';

    const ACTION_GET_ORDERS = 'get_orders';
    const ACTION_UPDATE_ORDER_STATUS = 'update_status';
    const ACTION_AUTHORIZATION = 'authorization';

    const RESPONSE_STATUS_SUCCESS = 'success';
    const RESPONSE_STATUS_FAIL = 'fail';

    public $username;

    public $password;

    public $orderId;

    public $status;

    public $page;

    public $token;

    public $perPage;

    public $keyHeader;

    public $valueHeader;

    public $response = [];

    /** @var Country */
    public $country;

    /**
     * @return array
     */
    public static function getActions()
    {
        return [
            self::ACTION_GET_ORDERS => Yii::t('common', 'Получение заказов'),
            self::ACTION_UPDATE_ORDER_STATUS => Yii::t('common', 'Обновление статуса заказа'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $country = Country::find()->byCharCode(self::CHAR_CODE_TEST_COUNTRY)->one();

        if ($country) {
            $this->country = $country;
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не найдена такая страна'));
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [[
                'username',
                'password',
                'orderId',
                'status',
                'page',
                'perPage',
                'token',
                'keyHeader',
                'valueHeader'
            ], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'orderId' => Yii::t('common', 'Номер заказа'),
            'status' => Yii::t('common', 'Статус'),
            'page' => Yii::t('common', 'Страница'),
            'perPage' => Yii::t('common', 'Количество на странице'),
            'token' => Yii::t('common', 'Токен'),
            'keyHeader' => Yii::t('common', 'Ключ заголовка'),
            'valueHeader' => Yii::t('common', 'Значение заголовка'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function authorization()
    {
        $response = $this->send(
            'post',
            'auth/login',
            [
                'username' => $this->username,
                'password' => $this->password,
            ]
        );

        $decodeResponse = json_decode($response, true);
        $answer = [
            'data' => json_encode($decodeResponse, JSON_UNESCAPED_SLASHES),
            'token' => '',
            'tokenBase64' => ''
        ];

        if (isset($decodeResponse['data']['token'])) {
            $answer['token'] = array_shift($decodeResponse['data']['token']);
            $answer['tokenBase64'] = base64_encode($answer['token']);
        } else {
            $answer['token'] = json_encode($decodeResponse['data'], JSON_UNESCAPED_SLASHES);
        }

        return $answer;
    }

    /**
     * @return array|\yii\httpclient\Response
     */
    public function getOrders()
    {
        $header = [$this->keyHeader => $this->valueHeader];
        $data = [
            'country' => $this->country->char_code,
            'page' => $this->page,
            'perPage' => $this->perPage,
        ];

        $response = $this->send('get', 'order/orders', $data, $header);

        return $response;
    }

    /**
     * @return array
     */
    public function updateStatus()
    {
        $header = [$this->keyHeader => $this->valueHeader];
        $data = [
            'country' => 'NG',
            'orderId' => $this->orderId,
            'status' => $this->status,
        ];

        $response = $this->send('get', 'order/update-status', $data, $header);

        return $response;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @param null|array $header
     * @return \yii\httpclient\Response
     * @throws HttpInvalidParamException
     */
    protected function send($method, $url, $data, $header = null)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod($method)
            ->setUrl(Yii::$app->params['domain'] . '/apidelivery/v1/' . $url)
            ->setData($data);

        if ($header) {
            $response->setHeaders($header);
        }

        $answer = $response->send();

        return $answer->content;
    }
}
