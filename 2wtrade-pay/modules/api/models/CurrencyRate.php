<?php
namespace app\modules\api\models;

use yii\base\Model;
use app\models\Currency;
use app\models\CurrencyRateHistory;
use app\models\CurrencyRate as CurrencyRateToday;
use Yii;

/**
 * Class CurrencyRate
 * @package app\modules\api\models
 *
 * @property string $code
 * @property string $date
 */
class CurrencyRate extends Model
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $date;

    /**
     * @var Currency | null
     */
    public $currency;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['code', 'date'], 'required'],
            ['code', 'string', 'max' => 3],
            ['date', 'date', 'format' => 'php:Y-m-d'],
            ['code', 'validateCurrencyCode'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateCurrencyCode($attribute)
    {
        $this->currency = Currency::find()
            ->byCharCode($this->code)
            ->one();

        if (!$this->currency) {
            $this->addError($attribute, Yii::t('common', 'Неверный код валюты.'));
        }
    }

    /**
     * @return array
     */
    public function getRate()
    {
        $this->validate();
        if ($this->currency) {
            if ($this->date == date('Y-m-d')) {
                $data = CurrencyRateToday::find()
                    ->where([CurrencyRateToday::tableName() . '.currency_id' => $this->currency->id])
                    ->orderBy([CurrencyRateToday::tableName() . '.created_at' => 'desc'])
                    ->limit(1)
                    ->one();

                if (isset($data)) {
                    return [
                        'code' => $this->code,
                        'date' => $this->date,
                        'rate' => $data->rate,
                    ];
                }
            }

            $data = CurrencyRateHistory::find()
                ->where([CurrencyRateHistory::tableName() . '.currency_id' => $this->currency->id])
                ->andWhere([CurrencyRateHistory::tableName() . '.date' => $this->date])
                ->orderBy([CurrencyRateHistory::tableName() . '.date' => 'desc'])
                ->limit(1)
                ->one();

            if (isset($data)) {
                return [
                    'code' => $this->code,
                    'date' => $this->date,
                    'rate' => $data->rate,
                ];
            }
        }

        return [
            'code' => $this->code,
            'date' => $this->date,
            'rate' => '0.00'
        ];
    }
}