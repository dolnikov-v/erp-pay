<?php
namespace app\modules\api\models;

use app\modules\report\models\SmsPollHistory as Model;

class SmsPollHistory extends Model
{
    /**
     * @return array
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return parent::attributeHints();
    }
}