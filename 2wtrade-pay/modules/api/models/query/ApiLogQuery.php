<?php
namespace app\modules\api\models\query;

use app\modules\api\models\ApiLog;
use yii\db\ActiveQuery;

/**
 * Class LeadLogQuery
 * @package app\modules\api\models\query
 * @method ApiLog one($db = null)
 * @method ApiLog[] all($db = null)
 */
class ApiLogQuery extends ActiveQuery
{
    /**
     * @param $type
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere(['type' => $type]);
    }
}
