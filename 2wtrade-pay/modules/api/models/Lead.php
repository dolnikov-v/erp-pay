<?php

namespace app\modules\api\models;

use app\models\Country;
use app\models\Landing;
use app\models\Notification;
use app\models\OrderCreationDate;
use app\models\Partner;
use app\models\PartnerCountry;
use app\models\Product;
use app\models\ProductLanding;
use app\models\Source;
use app\models\SourceProduct;
use app\modules\callcenter\jobs\SendCallCenterRequestJob;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\LeadCreateLog;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Model;

/**
 * Class Lead
 */
class Lead extends Model
{

    const MANY_PRODUCTS = 'productsMoreThanOne';

    public $comment;
    public $revenue;
    public $ip;
    public $site;
    public $street;
    public $postal_code;
    public $address_detail;
    public $name_detail;
    public $city;
    public $district;
    public $order_id;
    public $price;
    public $delivery;
    public $phone;
    public $is_validated;
    public $address;
    public $prefecture;
    public $name;
    public $total_price;
    public $goods_id;

    /**
     * foreign_created_at (OrderCreationDate table)
     */
    public $ts_spawn;

    /**
     * Массив продуктов для 2wstore
     */
    public $goods_array = [];

    public $country;
    public $currency_local;
    public $num;
    public $sub_district;
    public $with_address;
    public $package_id;
    public $web_id;
    public $partner_id;
    public $email;

    public $payment_type;
    public $payment_amount;

    public $source_id;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var Country
     */
    public $orderCountry;

    /**
     * @var Product
     */
    public $orderProduct;

    /**
     * @var Landing
     */
    public $orderLanding;

    /**
     * @var ProductLanding
     */
    public $orderProductLanding;

    /**
     * @var bool
     */
    public $isDuplicate;

    /**
     * @var array
     */
    protected static $packages = [
        1 => [
            ['quantity' => 1, 'price' => null],
        ],
        3 => [
            ['quantity' => 2, 'price' => null],
            ['quantity' => 1, 'price' => 0],
        ],
        5 => [
            ['quantity' => 3, 'price' => null],
            ['quantity' => 2, 'price' => 0],
        ],
    ];

    /**
     * @param array $post
     * @return Lead
     * @throws \Exception
     */
    public static function createFromPost($post)
    {
        $model = new Lead();
        if (Yii::$app->apiUser->source->unique_system_name == Source::SOURCE_2WSTORE
            || Yii::$app->apiUser->source->unique_system_name == Source::SOURCE_DIAVITA
            || Yii::$app->apiUser->source->unique_system_name == Source::SOURCE_TOPHOT
        ) {
            $model->setScenario(self::MANY_PRODUCTS);
        }

        if (!$model->load($post, '')) {
            throw new \Exception(__CLASS__ . '::' . __METHOD__);
        }

        return $model;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['order_id', 'country', 'site', 'phone', 'name', 'revenue'],
                'required'
                /*, 'message' => 'Error {attribute} is required!'*/
            ],
            [
                ['goods_id', 'num'],
                'required',
                'except' => self::MANY_PRODUCTS,
            ],
            [
                ['goods_array', 'currency_local'],
                'required',
                'on' => self::MANY_PRODUCTS,
            ],
            [['order_id', 'num', 'ts_spawn'], 'integer'],
            [['price', 'total_price', 'revenue', 'delivery'], 'double'],
            [['country', 'partner_id', 'site', 'name'], 'string'],
            ['email', 'string'],
            [['ip'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 200],

            ['payment_type', 'string', 'max' => 20],
            [['currency_local', 'payment_amount'], 'string'],

            [['address'], 'string', 'max' => 500],

            [
                'country',
                'validateCountry',
                'when' => function ($model) {
                    return $model->partner_id == null;
                },
                'enableClientValidation' => false
            ],
            [
                'partner_id',
                'validatePartner',
            ],
            ['order_id', 'validateOrderId'],
            ['site', 'validateSite'],
            [
                'goods_id',
                'validateGoodsId',
                'except' => self::MANY_PRODUCTS,
            ],
            [
                'goods_array',
                'validateGoodsArray',
                'on' => self::MANY_PRODUCTS,
            ],
            /*[
                'currency_local',
                'validateCurrencyLocal',
                'on' => self::MANY_PRODUCTS,
            ],*/

            [
                [
                    'street',
                    'postal_code',
                    'address_detail',
                    'name_detail',
                    'city',
                    'district',
                    'prefecture',
                    'sub_district',
                    'with_address',
                    'package_id',
                    'web_id'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @param $attribute
     */
    public function validateGoodsArray($attribute)
    {
        $this->source_id = Yii::$app->apiUser->source->id;
        $products = [];
        $priceTotal = 0;
        foreach ($this->goods_array as $product) {
            if (Yii::$app->apiUser->source->unique_system_name == Order::TYPE_CC_ORDER_2WSTORE || Yii::$app->apiUser->source->unique_system_name == Order::TYPE_CC_ORDER_TOPHOT) {
                if ($skuAppend = strstr($product['sku'], '_')) {
                    $product['sku'] = (integer)trim($skuAppend, '_');
                }
                $price = $this->pricePreviousProduct($product['sku']);
                $product['price'] = $price;
                $priceTotal += $price * (int)$product['quantity'];
            }
            $products[] = $product;
            if (!Product::findOne($product['sku'])) {
                $this->addError($attribute, Yii::t('common', 'Неизвестный номер товара "{id}".', [
                    'id' => $product['sku'],
                ]));
            };

            $this->checkSourceProduct($attribute, $product['sku']);
        }
        !$priceTotal ?: ($this->price = $priceTotal);
        !$priceTotal ?: ($this->total_price = $priceTotal);
        $this->goods_array = $products;
    }

    /**
     * Находит цену последнего продукта с ид $sku для страны заказа
     * @param integer $sku
     * @return bool|string
     */
    public function pricePreviousProduct($sku)
    {
        $countryId = Country::find()->byCharCode($this->country)->one()->id;
        return OrderProduct::find()
            ->select([OrderProduct::tableName() . '.price'])
            ->leftJoin(Order::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Country::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.country_id' => $countryId])
            ->andWhere(['=', OrderProduct::tableName() . '.product_id', $sku])
            ->andWhere(['!=', OrderProduct::tableName() . '.price', 0])
            ->orderBy(['order_id' => SORT_DESC])
            ->scalar();
    }


    public function validateCurrencyLocal($attribute)
    {
        if (($this->currency_local == 'USD') && (Yii::$app->apiUser->source->unique_system_name == Order::TYPE_CC_ORDER_2WSTORE || Yii::$app->apiUser->source->unique_system_name == Order::TYPE_CC_ORDER_TOPHOT)) {

            $rate = $this->orderCountry->currencyRate->rate;
            $price = round($rate * $this->price, 1);
            $this->price = $price;
            $this->total_price = $price;
            $products = [];
            foreach ($this->goods_array as $product) {
                $product['price'] = $product['price'] * $rate;
                $products[] = $product;
            }
            $this->goods_array = $products;
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatePartner($attribute, $params)
    {
        if (!$partner = Partner::findOne(['foreign_id' => $this->partner_id,])) {
            $this->addError($attribute, Yii::t('common', 'Такого партнера нет.'));
            return;
        };

        //Проверяем активный ли партнер
        if (!$partner->active) {
            $this->addError($attribute, Yii::t('common', 'Партнер не активен.'));
            return;
        };

        //Проверяем активен ли он в стране
        $partnerCountry = PartnerCountry::find()
            ->innerJoinWith(['country'])
            ->where([PartnerCountry::tableName() . '.partner_id' => $partner->id])
            ->andWhere([Country::tableName() . '.char_code' => $this->country])
            ->one();

        //Если не активен в стране
        //Ошибка Тогда Партнер в этой стране не работает
        if (!$partnerCountry) {
            $this->addError($attribute, Yii::t('common', 'Партнер не работает в стране ') . $this->country);
            return;
        }
        $this->orderCountry = $partnerCountry->country;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCountry($attribute, $params)
    {
        if (!Country::findOne(['char_code' => $this->country])) {
            $this->addError($attribute, Yii::t('common', 'Неизвестный код страны "{code}".', [
                'code' => $this->country,
            ]));
            return;
        }
        $partnerCountry = PartnerCountry::find()
            ->innerJoinWith(['partner', 'country'])
            ->where([Partner::tableName() . '.default' => 1])
            ->andWhere([Partner::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.char_code' => $this->country])
            ->andWhere([Partner::tableName() . '.source_id' => Yii::$app->apiUser->source->id ?? $this->source_id])
            ->one();
        if (!$partnerCountry) {
            $this->addError($attribute, Yii::t('common', 'Нет партнера по умолчанию в стране {code}', ['code' => $this->country]));
            return;
        }
        $this->orderCountry = $partnerCountry->country;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateOrderId($attribute, $params)
    {
        $this->isDuplicate = false;
        $orderFind = Order::find()
            ->bySourceId(Yii::$app->apiUser->source->id ?? $this->source_id)
            ->byForeignId($this->order_id)
            ->one();
        if ($orderFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Заказ уже существует.'));
            $this->isDuplicate = true;
            $this->order = $orderFind;
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateGoodsId($attribute, $params)
    {
        $this->orderProduct = Product::findOne($this->goods_id);

        if (!$this->orderProduct) {
            $this->addError($attribute, Yii::t('common', 'Неизвестный номер товара {id}.', [
                'id' => $this->goods_id,
            ]));
        }
        $this->checkSourceProduct($attribute, $this->goods_id);
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateSite($attribute, $params)
    {
        $this->orderLanding = Landing::find()
            ->byUrl($this->site)
            ->one();

        if (!$this->orderLanding) {
            $this->orderLanding = new Landing();
            $this->orderLanding->url = $this->site;
        }
    }

    /**
     * @param $attribute
     * @param $productId
     */
    private function checkSourceProduct($attribute, $productId)
    {
        $sourceProduct = SourceProduct::find()
            ->joinWith('source')
            ->where([Source::tableName() . '.id' => Yii::$app->apiUser->source->id, 'product_id' => $productId])
            ->exists();

        if (!$sourceProduct) {
            $this->addError($attribute, Yii::t('common', 'Товар {id} не привязан к источнику {source}.', [
                'id' => $productId,
                'source' => Yii::$app->apiUser->source->name,
            ]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country' => Yii::t('common', 'Страна'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'revenue' => Yii::t('common', 'Себестоимость'),
            'price' => Yii::t('common', 'Цена'),
            'total_price' => Yii::t('common', 'Итоговая цена'),
            'site' => Yii::t('common', 'Сайт'),
            'goods_id' => Yii::t('common', 'Номер товара'),
            'num' => Yii::t('common', 'Количество'),
            'phone' => Yii::t('common', 'Телефон заказчика'),
            'name' => Yii::t('common', 'Имя заказчика'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'email' => Yii::t('common', 'e@mail'),
            'payment_type' => Yii::t('common', 'Тип оплаты'),
            'payment_amount' => Yii::t('common', 'Предоплата'),
            'currency_local' => Yii::t('common', 'Валюта'),
            'goods_array' => Yii::t('common', 'Товары'),
            'ts_spawn' => Yii::t('common', 'Дата создания в источнике'),
        ];
    }

    /**
     * @return string
     */
    public function create()
    {
        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_LEAD;
        $apiLog->input_data = json_encode(Yii::$app->request->post(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if (mb_strtolower($this->is_validated) == 'true') {
            $this->is_validated = 1;
        } else {
            $this->is_validated = 0;
        }
        $this->source_id = Yii::$app->apiUser->source->id;
        if (!$isActiveSource = Yii::$app->apiUser->source->active) {
            $this->addError('site', strtr("Source {source} not active", ['{source}' => Yii::$app->apiUser->source->name]));
        }

        if ($this->validate()) {

            $transaction = Yii::$app->db->beginTransaction();

            if ($this->orderLanding->isNewRecord) {
                if (!$this->orderLanding->save()) {
                    $this->addError('site', Yii::t('common', 'Не удалось добавить новый лендинг "{landing}".', [
                        'landing' => $this->site,
                    ]));
                }
            }

            if (!$this->hasErrors() && $this->scenario != self::MANY_PRODUCTS) {
                $this->orderProductLanding = ProductLanding::find()
                    ->where(['product_id' => $this->orderProduct->id])
                    ->andWhere(['landing_id' => $this->orderLanding->id])
                    ->one();

                if (!$this->orderProductLanding) {
                    $this->orderProductLanding = new ProductLanding();
                    $this->orderProductLanding->product_id = $this->orderProduct->id;
                    $this->orderProductLanding->landing_id = $this->orderLanding->id;

                    if (!$this->orderProductLanding->save()) {
                        $this->addError('site', Yii::t('common', 'Не удалось сохранить связку товара с лендингом.'));
                    }
                }
            }

            if (!$this->hasErrors()) {
                $this->order = new Order;

                $this->order->source_id = $this->source_id;
                $this->order->call_center_type = Yii::$app->apiUser->source->default_call_center_type;

                $this->order->foreign_id = $this->order_id;
                $this->order->country_id = $this->orderCountry->id;
                $this->order->landing_id = $this->orderLanding->id;

                if (empty($this->with_address) || mb_strtolower($this->with_address) == 'false') {
                    $this->order->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                    $this->order->source_form = Order::TYPE_FORM_SHORT;
                } else {
                    $this->order->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                    $this->order->source_form = Order::TYPE_FORM_LONG;
                }
                //https://2wtrade-tasks.atlassian.net/browse/NPAY-1234
                if (isset($this->payment_type) && $this->payment_type == Order::ORDER_PAYMENT_TYPE_PREPAID) {
                    $this->order->payment_type = $this->payment_type;
                    $this->order->payment_amount = $this->payment_amount;
                    $this->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                }
                $this->order->customer_ip = $this->ip;
                $this->order->customer_full_name = $this->name;
                $this->order->customer_phone = $this->phone;
                $this->order->customer_city = $this->city;
                $this->order->customer_address = $this->address;
                $this->order->customer_zip = $this->postal_code;

                if (isset($this->email)) {
                    $this->order->customer_email = $this->email;
                }

                $this->order->comment = $this->comment;

                $this->order->price = $this->price ?? 0;
                $this->order->price_total = $this->total_price ?? 0;
                $this->order->income = $this->revenue;
                $this->order->delivery = $this->delivery === null ? 0 : $this->delivery;

                !isset($this->web_id) ?: $this->order->webmaster_identifier = $this->web_id;

                if ($this->order->save()) {

                    /**
                     * Запись даты создания заказа в источнике
                     */
                    if ($this->ts_spawn) {
                        $orderCreationDate = new OrderCreationDate();
                        $orderCreationDate->order_id = $this->order->id;
                        $orderCreationDate->foreign_create_at = $this->ts_spawn;
                        $orderCreationDate->save();
                    }

                    $products = $this->prepareProducts();
                    if ($products) {
                        $apiLog->status = ApiLog::STATUS_SUCCESS;

                        $apiLog->output_data = json_encode([
                            'country' => $this->orderCountry->char_code,
                            'order_id' => $this->order->id,
                            'foreign_id' => $this->order->foreign_id,
                        ], JSON_UNESCAPED_UNICODE);
                    }

                    if ($this->order->payment_type == Order::ORDER_PAYMENT_TYPE_PREPAID) {
                        // для предоплаченных заказов сразу вызовем брокер и назначим КС если возможно
                        try {
                            $brokerResult = $this->order->findDeliveryByBroker($products);
                            if ($brokerResult) {
                                if ($brokerResult['deliveryId']) {
                                    Delivery::createRequest($brokerResult['deliveryId'], $this->order, false);
                                }
                            }
                        } catch (\Exception $e) {
                            // Никуда не передаем ошибку, чтобы нам дубли не слали
                            // $e->getMessage();
                        }
                    }

                } else {
                    $this->addErrors($this->order->getErrors());
                }
            }

            if ($apiLog->status == ApiLog::STATUS_SUCCESS) {
                $transaction->commit();
                Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $this->order->id]));
                Yii::$app->notification->send(Notification::TRIGGER_LEAD_ADDED, null, $this->orderCountry->id);
            } else {
                $transaction->rollBack();
            }
        } else {
            if ($this->isDuplicate) {
                $apiLog->status = ApiLog::STATUS_DUPLICATE;

                $apiLog->output_data = json_encode([
                    'country' => $this->orderCountry->char_code,
                    'order_id' => $this->order->id,
                    'foreign_id' => $this->order->foreign_id,
                ], JSON_UNESCAPED_UNICODE);
            }
        }

        if ($apiLog->status == ApiLog::STATUS_FAIL) {
            $apiLog->output_data = json_encode([
                'errors' => json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE),
                'source' => strtoupper(Yii::$app->apiUser->source->name),
            ], JSON_UNESCAPED_UNICODE);
        }

        $apiLog->country_id = $this->orderCountry ? $this->orderCountry->id : null;
        $apiLog->save();
        if ($this->order instanceof Order && !$this->order->isNewRecord) {
            try {
                $createLog = new LeadCreateLog();
                $createLog->order_id = $this->order->id;
                $createLog->input_data = $apiLog->input_data;
                $createLog->output_data = $apiLog->output_data;
                if (!$createLog->save()) {
                    Yii::getLogger()->log($createLog->getErrors(), \yii\log\Logger::LEVEL_ERROR);
                }
            } catch (\Throwable $e) {
                Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
            }
        }

        return $apiLog->status;
    }

    /**
     * @return array|false
     */
    protected function prepareProducts()
    {
        $products = [];
        //Для 2wstore
        if ($this->scenario == self::MANY_PRODUCTS) {
            foreach ($this->goods_array as $product) {
                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $this->order->id;
                $orderProduct->product_id = $product['sku'];
                $orderProduct->quantity = $product['quantity'];
                $orderProduct->price = $product['price'];

                $products[] = $orderProduct;
                if (!$orderProduct->save()) {
                    $this->addErrors($orderProduct->getErrors());
                    return false;
                }
            }
            return $products;
        }

        if ($this->package_id) {
            if (array_key_exists($this->package_id, self::$packages)) {
                $package = self::$packages[$this->package_id];

                foreach ($package as $info) {
                    $orderProduct = new OrderProduct();
                    $orderProduct->order_id = $this->order->id;
                    $orderProduct->product_id = $this->goods_id;
                    $orderProduct->quantity = $info['quantity'];

                    if (is_null($info['price'])) {
                        $this->order->price = $this->order->price / $info['quantity'];
                        $this->order->save(false, ['price']);

                        $orderProduct->price = $this->order->price;
                    }

                    $products[] = $orderProduct;
                    if (!$orderProduct->save()) {
                        $this->addErrors($orderProduct->getErrors());

                        return false;
                    }
                }

                return $products;
            } else {
                $this->addError('package_id', Yii::t('common', 'Неизвестный номер пакета "{package}".', [
                    'package' => $this->package_id,
                ]));
            }
        } else {
            $orderProduct = new OrderProduct();
            $orderProduct->order_id = $this->order->id;
            $orderProduct->product_id = $this->goods_id;
            $orderProduct->quantity = $this->num;
            $orderProduct->price = $this->order->price;

            $products[] = $orderProduct;
            if ($orderProduct->save()) {
                return $products;
            } else {
                $this->addErrors($orderProduct->getErrors());
            }
        }

        return false;
    }
}
