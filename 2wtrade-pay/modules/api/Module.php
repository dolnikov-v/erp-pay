<?php

namespace app\modules\api;

use Yii;
use app\modules\api\components\filters\auth\HttpBearerAuth;

/**
 * Class Module
 * @package app\modules\api
 */
class Module extends \app\components\base\Module
{
    /**
     * @var array Разрешенные контроллеры для просмотра в админке
     */
    private $clientControllers = [
        'logs/lead',
        'logs/call-center',
        'logs/delivery',
        'testing/delivery-api'
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        if ($this->isClientController()) {
            return parent::behaviors();
        }

        Yii::$app->user->enableSession = false;

        $behaviors = [];

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'realm' => 'api',
        ];

        return $behaviors;
    }

    /**
     * @return boolean
     */
    private function isClientController()
    {
        return in_array(Yii::$app->controller->id, $this->clientControllers);
    }
}
