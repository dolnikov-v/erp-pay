<?php

namespace app\modules\api\components\filters\auth;

use app\components\web\User;
use app\models\api\ApiUserToken;
use yii\filters\auth\AuthMethod;
use yii\web\UnauthorizedHttpException;

/**
 * Class HttpBearerAuth
 * @package app\modules\api\components\filters\auth
 */
class HttpBearerAuth extends AuthMethod
{
    const ACCESS_TYPE_ADCOMBO = 'adcombo';
    const ACCESS_TYPE_JEEMPO = 'jeempo';
    const ACCESS_TYPE_2WSTORE = '2wstore';
    const ACCESS_TYPE_DIAVITA = 'diavita';
    const ACCESS_TYPE_CALLCENTER = 'callcenter';
    const ACCESS_TYPE_SMS_POLL_HISTORY = 'sms_poll_history';
    const ACCESS_TYPE_NOTIFICATION = 'notification';
    const ACCESS_TYPE_ZABBIX = 'zabbix';
    const ACCESS_TYPE_MERCURY = 'mercury';
    const ACCESS_TYPE_2WDISTR = '2wDistr';
    const ACCESS_TYPE_WTRADE_POST_SALE = 'wtrade_post_sale';
    const ACCESS_TYPE_DINA = 'dina';
    
    const ACCESS_TYPE_TEST_1 = 'test_1';
    const ACCESS_TYPE_MY_EPC = 'my_epc';
    const ACCESS_TYPE_TEST_3 = 'test_3';
    const ACCESS_TYPE_TEST_4 = 'test_4';
    const ACCESS_TYPE_TEST_5 = 'test_5';

    const ACCESS_TYPE_TOPHOT = 'tophot';

    /**
     * @var string
     */
    public $realm = 'api';

    /**
     * @deprecated
     * @var array Список разрешенных токенов
     */
    public static $accessTokens = [
        'a77cf421eee4938ef83a89de92f3f65214e99bec' => self::ACCESS_TYPE_ADCOMBO,
        '21afc774aa5e50b232977870305795553f2e0a70' => self::ACCESS_TYPE_JEEMPO,
        '20b70f0af00562e63758b9ee42012ecc96c58590' => self::ACCESS_TYPE_CALLCENTER,
        'c851e902148a4fdcf99c9d7a541285d9135cf500' => self::ACCESS_TYPE_DINA,

        'dgnv20s64l21v9br46he33fdn0n4bd53Gik0fju4' => self::ACCESS_TYPE_2WSTORE,
        'f2e0a7e6f59d6fa0c9d653fdn0n4bd53Gik0fju4' => self::ACCESS_TYPE_DIAVITA,
        '72c46677f59d6fa0c9d6581f632d4746cf28bb36' => self::ACCESS_TYPE_SMS_POLL_HISTORY,
        '310ec4e6213b134f6c2813bed00192195a756ee0' => self::ACCESS_TYPE_NOTIFICATION,
        'e4938ef83a89dem6f59d6fa03fdn0n4bd53Gik0f' => self::ACCESS_TYPE_ZABBIX,
        '4608532b542fa2b0ff78d97c2fee8498a03fdn0n' => self::ACCESS_TYPE_MERCURY,
        '8603fc9f6f5e7ac50f75e7680f72cf17c4162392' => self::ACCESS_TYPE_2WDISTR,
        '653355e7032146e08b85e53de142b355b1317301' => self::ACCESS_TYPE_WTRADE_POST_SALE,

        '5526eca61e078a2e97063fe86d5fd8d3d234f19a' => self::ACCESS_TYPE_TEST_1,
        '655204e9c26c82e11fb2c7bb5a91a74455c3acf1' => self::ACCESS_TYPE_MY_EPC,
        'b3061ed2070c813a97e67c06d8258b060f0a3c21' => self::ACCESS_TYPE_TEST_3,
        '44ca55def9c93b3843359599d27834f47b876a5d' => self::ACCESS_TYPE_TEST_4,
        '3ed2e00b58148a23f21cbbafa62765d621c0ac2b' => self::ACCESS_TYPE_TEST_5,

        'deab196b508fdfee5f8b723d39846a345c20180a' => self::ACCESS_TYPE_TOPHOT,
    ];

    /**
     * @param User $user
     * @param \yii\web\Request $request
     * @param \yii\web\Response $response
     * @return bool|null
     */
    public function authenticate($user, $request, $response)
    {
        $authHeader = $request->getHeaders()->get('Authorization');

        if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
            $accessToken = base64_decode($matches[1]);

            $token = ApiUserToken::find()
                ->joinWith('user')
                ->byToken($accessToken)
                ->andWhere([
                    'OR',
                    ['is', ApiUserToken::tableName() . '.lifetime', null],
                    [ApiUserToken::tableName() . '.lifetime' => 0],
                    ['>', ApiUserToken::tableName() . '.lifetime', time()]
                ])
                ->one();

            if ($token) {
                \Yii::$app->apiUser->token = $token;

                return true;
            }
        }

        return null;
    }

    /**
     * @param \yii\web\Response $response
     * @throws UnauthorizedHttpException
     */
    public function challenge($response)
    {
        $response->getHeaders()->set('WWW-Authenticate', "Basic realm=\"{$this->realm}\"");

        throw new UnauthorizedHttpException('Неверный токен аутентификации.');
    }
}
