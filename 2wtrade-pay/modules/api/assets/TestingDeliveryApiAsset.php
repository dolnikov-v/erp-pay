<?php
namespace app\modules\api\assets;

use yii\web\AssetBundle;


class TestingDeliveryApiAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/api/testing';

    public $js = [
        'delivery-api.js',
    ];

    public $css = [
        'delivery-api.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\assets\vendor\JJsonViewerAsset',
    ];
}
