<?php

namespace app\modules\crocotime\components;

use app\modules\salary\models\PersonLink;
use app\modules\salary\models\WorkingShift;
use Yii;
use yii\base\Component;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use yii\httpclient\Response;

/**
 * Class CrocotimeApi
 * @package app\modules\crocotime\components
 *
 * @property mixed $profileSettings
 * @property null|array $offices
 * @property bool|array $employees
 * @property string $authUrl
 * @property string $session
 * @property mixed $profileSettingsByOffice
 */
class CrocotimeApi extends Component
{

    private
        $url,
        $token,
        $appVersion,
        $login,
        $password,
        $notificationReceivers;

    public function init()
    {
        parent::init();
        $this->token = Yii::$app->params['crocotime']['token'];
        $this->url = Yii::$app->params['crocotime']['url'];
        $this->appVersion = Yii::$app->params['crocotime']['appVersion'];
        $this->login = Yii::$app->params['crocotime']['login'];
        $this->password = Yii::$app->params['crocotime']['password'];
        $this->notificationReceivers = Yii::$app->params['crocotime']['notificationReceivers'];
        $this->client = new Client();
    }

    /**
     * @var Client $client
     */
    private $client;

    public $errors        = [];
    public $notifications = [];

    /**
     * @return string
     * @throws \Exception
     */
    public function getSession()
    {

        $response = $this->client->createRequest()
            ->setMethod('get')
            ->setUrl($this->authUrl)
            ->send();

        if (!$response->isOk
            || (!$result = json_decode($response->content))
            || !isset($result->session)) {
            throw new \Exception('Get session error: ', $response->content . PHP_EOL);
        }

        return $result->session;
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        $query = [
            "user" => [
                "login" => $this->login,
                "password" => md5($this->password),
            ],
        ];

        $query = json_encode($query);

        return $this->url . '/?controller=LogonController&query=' . $query;
    }

    /**
     * Returns list of operators from crocotime api
     * @return array|bool
     * @throws \Exception
     */
    public function getEmployees()
    {
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "api_employees",
        ];

        $this->client->setTransport('yii\httpclient\CurlTransport');
        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))) {
            throw new \Exception('Error response code ' . $response->statusCode);
        }
        if (!isset($result->result->items)) {
            throw new \Exception($result->error->code);
        }

        //Исключаем админов и прочих пользователей
        $operators = array_filter($result->result->items, function ($value) {
            return $value->parent_group_id > 0;
        });

        return ArrayHelper::index($operators, 'employee_id');
    }

    /** Returns difference ids between apiEmployees list and current employees list
     *
     * @param $apiEmployees
     * @param $currentEmployees
     *
     * @return \stdClass[] $newEmployees
     */
    public function findNewEmployees($apiEmployees, $currentEmployees)
    {
        return $newEmployees = array_diff_key($apiEmployees, $currentEmployees);
    }

    /**
     * Bind crocotime employee id to call center person id
     *
     * @param \stdClass[] $newEmployees
     */
    public function bindEmployees($newEmployees)
    {
        $ids = array_keys($newEmployees);
        $boundEmployees = ArrayHelper::getColumn((PersonLink::find()
            ->isCrocoTime()
            ->byForeignId($ids)
            ->all()), 'person_id');
        if ($notBound = array_diff_key($ids, $boundEmployees)) {
            foreach ($notBound as $item) {
                $fullName = trim($newEmployees[$item]->first_name . ' ' . $newEmployees[$item]->second_name);
                $condition = $this->advancedLikeForNames($fullName);
                $person = Person::find()->andFilterWhere($condition)->one();
                if ($person instanceof Person) {
                    $saveResult = $person->savePersonLinkCrocoTime($item);
                    if ($saveResult !== true) {
                        $this->errors[] = $saveResult;
                        continue;
                    }
                } else {
                    $this->notifications[] = 'Unable to bind "' . $newEmployees[$item]->display_name . '" id = ' . $newEmployees[$item]->employee_id . ' from department ' . $newEmployees[$item]->parent_group_id . PHP_EOL;
                }
            }
        }
    }

    /**
     * Set default settings to all employees (timeZone, access, schedule)
     *
     * @param $newEmployees
     *
     * @throws \Exception
     */
    public function setProfileSettings($newEmployees)
    {
        $newEmployeesByOffice = ArrayHelper::map($newEmployees, 'employee_id', 'display_name', 'parent_group_id');
        foreach ($newEmployeesByOffice as $office => $operators) {
            if (!$timezone = Office::getTimezoneByCrocotimeOffice($office)) {
                continue;
            };
            $this->setProfileSettingsByOffice(array_keys($operators), $timezone);
        }
    }

    /**
     * Set default settings to employees for one office (timeZone, access, schedule)
     *
     * @param $ids
     * @param $timeZone
     *
     * @throws \Exception
     */
    public function setProfileSettingsByOffice($ids, $timeZone = null)
    {
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "WorkspaceActionController",
            "action" => "update",
            "query" => [
                "domain" => "employees",
                "filter" => [
                    "employee_id" => $ids,
                ],
                "record" => [
                    "screenshot_period" => 300,
                    "privilege" => 0,
                ],
            ],
        ];

        if ($timeZone) {
            $post["query"]["record"]["time_zone"] = $timeZone;
        }

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))) {
            $this->errors[] = 'Settings not changed to ' . implode(',', $ids);

            return;
        }
        if (!isset($result->result->affected_ids)) {
            $this->errors[] = 'Settings not changed to ' . implode(',', $ids) . PHP_EOL . print_r($result, true);
        }
    }

    /**
     * Return all exist offices from crocotime api
     * @return null | array
     */
    public function getOffices()
    {
        $offices = [];
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "api_departments",
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))) {
            return null;
        }

        if (!isset($result->result->items)) {
            $this->errors[] = 'Get offices error ' . $response->content;

            return null;
        }

        foreach ($result->result->items as $item) {
            if (isset($item->department_id) && $item->is_enabled) {
                unset($item->items);
                $offices[] = $item;
            }
        }

        return ArrayHelper::index($offices, 'department_id');
    }

    /**
     * Bind crocotime_parent_group_id to call center office id
     *
     * @param $apiOffices
     */
    public function bindOffices($apiOffices)
    {
        $apiOfficeIds = array_keys($apiOffices);
        $boundOffices = Office::findBoundOffices($apiOfficeIds);
        if ($notBound = array_diff_key($apiOfficeIds, $boundOffices)) {
            foreach ($notBound as $item) {
                $office = Office::find()->where(['like', 'name', $apiOffices[$item]->display_name])->one();
                if ($office) {
                    $office->crocotime_parent_group_id = $item;
                    if (!$office->save()) {
                        $this->errors[] = $office->getFirstErrorAsString();
                    }
                } else {
                    $this->notifications[] = 'Не удалось привязать ' . $office[$item]->display_name . 'id '
                                             . $apiOffices[$item]->department_id . PHP_EOL;
                }
            }
        }
    }

    /**
     * @return bool
     * @internal param $message
     */
    public function sendEmailNotifications()
    {
        $mailer = Yii::$app->mailer
            ->compose()
            ->setFrom(Yii::$app->params['noReplyEmail'])
            ->setTo($this->notificationReceivers)
            ->setSubject('Crocotime users synchronization error')
            ->setTextBody(print_r($this->notifications, true));

        return $result = $mailer->send();
    }

    /**
     * Returns work time start and end
     *
     * @param array $crocotimeEmployeesIds
     *
     * @return \stdClass[]
     * @throws \Exception
     */
    public function workTime($crocotimeEmployeesIds)
    {
        $lastDay = strtotime('today midnight');
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "api_employee_work_periods",
            "query" => [
                "day" => $lastDay,
                "employees" => $crocotimeEmployeesIds,
            ],
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !isset($result->result->items)
        ) {
            throw new \Exception('Error can not get work time ' . $response->content . PHP_EOL);
        }

        return ArrayHelper::index($result->result->items, 'employee_id');
    }

    /**
     * Returns employees effective time
     *
     * @param $crocotimeEmployeesIds
     *
     * @return \stdClass[]
     * @throws \Exception
     */
    public function activity($crocotimeEmployeesIds)
    {
        $lastDay = strtotime('today midnight');
        $today = strtotime('tomorrow') - 1;
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "api_employee_activity",
            "query" => [
                "interval" => [$lastDay, $today],
                "employees" => $crocotimeEmployeesIds,
            ],
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !isset($result->result->items)
        ) {
            throw new \Exception('Error can not get activity time ' . $response->content . PHP_EOL);
        }

        return ArrayHelper::index($result->result->items, 'employee_id');
    }


    /**
     * @param string $shiftName
     *
     * @return mixed
     */
    public function insertSchedule($shiftName)
    {
        $post = [
            "session" => $this->session,
            "app_version" => $this->appVersion,
            "controller" => "WorkspaceActionController",
            "action" => "insert",
            "query" => [
                "domain" => "schedules",
                "record" => [
                    "display_name" => $shiftName,
                ],
            ],
        ];

        //$headers = ["Cookie: session=$this->session"];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !($scheduleId = $result->result->affected_ids[0] ?? false)) {
            $this->errors[] = 'Insert schedule error: ' . $response->content . PHP_EOL;

            return false;
            //throw new \Exception('Insert schedule error: ' . $response->content . PHP_EOL);
        }

        return $scheduleId;
    }

    /**
     * @param WorkingShift $shift
     *
     * @return bool
     */
    public function updateSchedule($shift)
    {
        $days = $this->convertIntervalsForCrocotime($shift);
        $post = [
            "session" => $this->session,
            "app_version" => $this->appVersion,
            "controller" => "BatchProcessing",
            "query" => [
                "items" => [
                    [
                        "name" => "ScheduleDaysUpdate",
                        "controller" => "schedule_item_setter",
                        "params" => [
                            "entities" => [
                                $shift->crocotime_id,
                            ],
                            "days" => $days,
                        ],

                    ],
                ],
            ],
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !isset($result->result)) {
            $this->errors[] = 'Insert schedule error: ' . $response->content . PHP_EOL;

            return false;
            //throw new \Exception('Insert schedule error: ' . $response->content . PHP_EOL);
        }

        return true;
    }

    public function deleteSchedule($scheduleId)
    {
        $post = [
            "session" => $this->session,
            "app_version" => $this->appVersion,
            "controller" => "WorkspaceActionController",
            "action" => "remove",
            "query" => [
                "domain" => "schedules",
                "filter" => [
                    "schedule_id" => $scheduleId,
                ],
            ],
        ];

        //$headers = ["Cookie: session=$this->session"];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !($scheduleId = $result->result->affected_ids[0] ?? false)) {
            $this->errors[] = 'Insert schedule error: ' . $response->content . PHP_EOL;

            return false;
            //throw new \Exception('Insert schedule error: ' . $response->content . PHP_EOL);
        }

        return $scheduleId;
    }

    /**
     * Set working shift to employee
     *
     * @param int $id
     * @param int $scheduleId
     *
     * @return bool
     */
    public function updateEmployeeSchedule($id, $scheduleId)
    {
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "WorkspaceActionController",
            "action" => "update",
            "query" => [
                "domain" => "employees",
                "filter" => [
                    "employee_id" => $id,
                ],
                "record" => [
                    "schedule_id" => $scheduleId,
                ],
            ],
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))
            || !isset($result->result->affected_ids)) {
            $this->errors[] = 'Settings not changed on CrocoTime ';

            return false;
        }

        return true;
    }

    /**
     * Set working shift to employee
     *
     * @param array $ids
     * @param int $scheduleId
     *
     * @return bool
     */
    public function batchUpdateEmployeeSchedule($scheduleId, $ids)
    {
        $post = [
            "session" => $this->session,
            "app_version" => $this->appVersion,
            "controller" => "employee_schedule_setter",
            //"action"       => "update",
            "query" => [
                "nodes" => [],
                "items" => $ids,
                "value" => $scheduleId,
            ],
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))) {
            $this->errors[] = 'Schedule not set ' . $response->statusCode;

            return false;
        }
        if (!isset($result->result->affected_ids)) {
            $this->errors[] = 'Schedule not set ' . PHP_EOL . print_r($result, true);
            return false;
        }

        return true;
    }

    /**
     * Returns condition for Active Record
     *
     * @param string $fullName
     *
     * @return array $condition
     */
    private function advancedLikeForNames($fullName)
    {
        $condition = [];
        $condition[] = 'or';
        if (trim($fullName) != "") {
            $words = explode(' ', trim($fullName));
            $tmp = [];
            $tmp[] = 'and';
            foreach ($words as $word) {
                $tmp[] = ['like', 'name', $word];
            }
            $condition[] = $tmp;
        }

        return $condition;
    }

    /**
     * @param WorkingShift $shift
     *
     * @return array
     */
    private function convertIntervalsForCrocotime($shift)
    {
        $lunchTimeInterval = $this->convertIntervalToSeconds($shift->lunch_time ?? null);

        return $days = [
            [
                "day_of_week" => 1,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_mon ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 2,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_tue ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 3,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_wed ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 4,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_thu ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 5,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_fri ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 6,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_sat ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],
            [
                "day_of_week" => 0,
                "intervals" => $intervals = $this->convertIntervalToSeconds($shift->working_sun ?? null, $lunchTimeInterval),
                "norm" => $this->calculateNorm($intervals),
            ],

        ];
    }

    /**
     * @param string $interval
     *
     * @param array $lunchTimeInterval
     *
     * @return array
     */
    private function convertIntervalToSeconds($interval, $lunchTimeInterval = null)
    {
        if (!$interval) {
            return [];
        }
        preg_match_all('/\d+:\d+/', $interval, $matches);
        $start = strtotime("1970-01-01 {$matches[0][0]} UTC");
        $end = strtotime("1970-01-01 {$matches[0][1]} UTC");
        if ($lunchTimeInterval) {
            $intervals[] = [$start, $lunchTimeInterval[0][0]];
            $intervals[] = [$lunchTimeInterval[0][1], $end];
        } else {
            $intervals[] = [$start, $end];
        }

        return $intervals;
    }

    /**
     * @param array $intervals
     *
     * @return int
     */
    private function calculateNorm($intervals)
    {
        $seconds = [];
        foreach ($intervals as $interval) {
            $seconds[] = $interval[1] - $interval[0];
        }

        return array_sum($seconds);
    }

    /**
     * Group crocotimeEmployeeId by crocotimeShiftId
     * [
     *      crocotimeShiftId => [
     *      crocotimeEmployeeIds
     * ]]
     * @return array
     */
    public function crocotimeEmployeeIdByShifts()
    {
        $employees = Person::find()
            ->select(['crocotime_employee_id', WorkingShift::tableName() . 'crocotime_id'])
            ->joinWith('workingShift')
            ->where(['not', ['crocotime_employee_id' => null]])
            ->andWhere(['not', [WorkingShift::tableName() . '.crocotime_id' => null]])
            ->asArray()
            ->all();

        $employeesByWorkShift = ArrayHelper::index($employees, 'crocotime_employee_id', 'working_shift_id');
        foreach ($employeesByWorkShift as $crocoTimeShiftId => $crocoTimeEmployeeId) {
            $employeesByWorkShift[$crocoTimeShiftId] = array_keys($crocoTimeEmployeeId);
        }

        return $employeesByWorkShift;
    }

    /**
     * @param int $scheduleId
     * @param array $employeeIds
     */
    public function setScheduleToEmployees($scheduleId, $employeeIds)
    {
        $post = [
            "server_token" => $this->token,
            "app_version" => $this->appVersion,
            "controller" => "WorkspaceActionController",
            "action" => "update",
            "query" => [
                "domain" => "employees",
                "filter" => [
                    "employee_id" => $employeeIds,
                ],
                "record" => [
                    "schedule_id" => $scheduleId,
                ],
            ],
        ];
        /**
         * @var Response $response
         */
        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk || !($result = json_decode($response->content))) {
            $this->errors[] = 'Schedule not set ' . $response->statusCode;

            return;
        }
        if (!isset($result->result->affected_ids)) {
            $this->errors[] = 'Schedule not set ' . PHP_EOL . print_r($result, true);
        }
    }
}

