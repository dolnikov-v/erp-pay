<?php
namespace app\modules\crocotime\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\ModelTrait;

/**
 * Class CrocotimeWorkTime
 * @package app\models
 * @property integer $id
 * @property integer $crocotime_employee_id
 * @property string $date
 * @property string $effective_time
 * @property string $start_at
 * @property string $end_at
 * @property integer $forbidden_time
 * @property integer $unknown_time
 * @property integer $late_time
 * @property integer $early_end_time
 * @property integer $summary_time
 * @property integer $norm
 */
class CrocotimeWorkTime extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crocotime_work_time}}';
    }


    public function rules()
    {
        return [
            [['crocotime_employee_id'], 'required'],
            [['date'], 'safe'],
            [['id', 'crocotime_employee_id', 'effective_time', 'start_at', 'end_at', 'forbidden_time', 'unknown_time', 'late_time', 'early_end_time', 'summary_time', 'norm'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crocotime_employee_id' => Yii::t('common', 'Номер сотрудника'),
            'date' => Yii::t('common', 'Дата'),
            'effective_time' => Yii::t('common', 'Продуктивное время'),
            'start_at' => Yii::t('common', 'Начало работы'),
            'end_at' => Yii::t('common', 'Окончание работы'),
            'forbidden_time' => Yii::t('common', 'Непродуктивное время'),
            'unknown_time' => Yii::t('common', 'Неизвестное время'),
            'late_time' => Yii::t('common', 'Время опозданий'),
            'early_end_time' => Yii::t('common', 'Время ранних уходов'),
            'summary_time' => Yii::t('common', 'Отработанное время'),
            'norm' => Yii::t('common', 'Рабочие часы'),
        ];
    }
}