<?php
namespace app\modules\crocotime;

/**
 * Class Module
 * @package app\modules\crocotime
 */
class Module extends \app\components\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\crocotime\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
