<?php

namespace app\modules\marketplace\modules\mercadolibre;

use app\modules\marketplace\abstracts\extensions\MarketPlaceWithWebHookInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\components\MarketPlaceWithExtendedServices;
use app\modules\marketplace\exceptions\InvalidParamException;
use app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi;
use app\modules\marketplace\modules\mercadolibre\models\Product;

/**
 * Class MercadoLibre
 * @package app\modules\marketplace\modules\mercedolibre
 *
 * @property MercadoLibreApi $apiAdapter
 */
class MercadoLibre extends MarketPlaceWithExtendedServices implements MarketPlaceWithWebHookInterface
{
    /**
     * @param string $foreignId
     * @return OrderInterface|null
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function getOrderByForeignId(string $foreignId): ?OrderInterface
    {
        return $this->apiAdapter->getOrder($foreignId);
    }

    /**
     * Создание заказа
     * @param array $request
     * @return array
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function handleWebHookRequest(array $request): array
    {
        switch ($request['topic'] ?? '') {
            case 'orders':
            case 'created_orders':
                return $this->createOrderByWebHook($request);
            default:
                throw new InvalidParamException('Не удалось определить требуемое действие.');
        }
    }

    /**
     * @param array $request
     * @return array
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    protected function createOrderByWebHook(array $request): array
    {
        if (empty($request['resource'])) {
            throw new \Exception(\Yii::t('common', 'Поле {field} не найдено.', ['field' => 'resource']));
        }
        $resource = parse_url($request['resource'])['path'] ?? '';
        $parts = explode('/', $resource);
        $orderId = (int)array_pop($parts);
        if (empty($orderId)) {
            throw new \Exception('Не удалось определить идентификатор заказа.');
        }
        $orderData = $this->apiAdapter->getOrder($orderId);
        $orderId = $this->dataConverter->createOrUpdateOrderFromData($orderData, $this);
        return [
            'order_id' => $orderId,
            'foreign_id' => $orderData->foreignId,
            'source' => $this->getId()
        ];
    }

    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->params['apiKey'];
    }
}