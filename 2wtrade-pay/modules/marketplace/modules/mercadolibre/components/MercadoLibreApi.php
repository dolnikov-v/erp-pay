<?php

namespace app\modules\marketplace\modules\mercadolibre\components;

use app\modules\marketplace\abstracts\MarketPlaceIntegrationInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithStorageServiceInterface;
use app\modules\marketplace\abstracts\services\StorageServiceInterface;
use app\modules\marketplace\components\MarketPlaceIntegration;
use app\modules\marketplace\models\CustomerAddress;
use app\modules\marketplace\models\CustomerInformation;
use app\modules\marketplace\models\Order;
use app\modules\marketplace\models\OrderProduct;
use app\modules\marketplace\models\Shipment;
use app\modules\marketplace\modules\mercadolibre\models\Product;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

/**
 * Class MercadoLibreApi
 * @package app\modules\marketplace\modules\mercedolibre\components
 */
class MercadoLibreApi extends MarketPlaceIntegration implements MarketPlaceIntegrationInterface, ObjectWithStorageServiceInterface
{
    const CACHE_ACCESS_TOKEN_KEY = 'mercadolibre_auth';
    const STORAGE_REFRESH_TOKEN_KEY = 'refresh_token';
    const STORAGE_AUTH_CODE_KEY = 'auth_code';

    const ORDER_STATUS_CONFIRMED = 'confirmed';
    const ORDER_STATUS_PAYMENT_REQUIRED = 'payment_required';
    const ORDER_STATUS_PAYMENT_IN_PROCESS = 'payment_in_process';
    const ORDER_STATUS_PAID = 'paid';
    const ORDER_STATUS_CANCELLED = 'cancelled';
    const ORDER_STATUS_INVALID = 'invalid';

    /**
     * Базовый URL до АПИ маркета
     * @var string
     */
    public $baseApiUrl;
    /**
     * URL для получения токена авторизации
     * @var string
     */
    public $authorizationUrl;
    /**
     * @var string
     */
    public $clientId;
    /**
     * @var string
     */
    public $secretKey;
    /**
     * @var string
     */
    public $authCode;

    /**
     * @var string
     */
    public $redirectUri;

    /**
     * @var int
     */
    public $sellerId;

    /**
     * @var null
     */
    protected $token = null;

    /**
     * @var StorageServiceInterface
     */
    private $storageService;

    /**
     * Маппинг статусов меркадо в наши
     * @var array
     */
    protected $orderStatusMapping = [
        self::ORDER_STATUS_CONFIRMED => OrderInterface::STATUS_CONFIRMED,
        self::ORDER_STATUS_PAYMENT_REQUIRED => OrderInterface::STATUS_REQUIRE_PAYMENT,
        self::ORDER_STATUS_PAYMENT_IN_PROCESS => OrderInterface::STATUS_REQUIRE_PAYMENT,
        self::ORDER_STATUS_PAID => OrderInterface::STATUS_PAID,
        self::ORDER_STATUS_CANCELLED => OrderInterface::STATUS_CANCELLED,
        self::ORDER_STATUS_INVALID => OrderInterface::STATUS_REQUIRE_PAYMENT
    ];

    /**
     * @var array
     */
    protected $shipmentStatusMapping = [
        'delivered' => ShipmentInterface::STATUS_DELIVERED,
        'shipped' => ShipmentInterface::STATUS_IN_PROGRESS,
        'to_be_agreed' => ShipmentInterface::STATUS_PENDING,
        'pending' => ShipmentInterface::STATUS_PENDING,
        'handling' => ShipmentInterface::STATUS_IN_PROGRESS,
        'ready_to_ship' => ShipmentInterface::STATUS_IN_PROGRESS,
        'not_delivered' => ShipmentInterface::STATUS_NOT_DELIVERED,
        'not_verified' => ShipmentInterface::STATUS_PENDING,
        'cancelled' => ShipmentInterface::STATUS_CANCELLED,
        'closed' => ShipmentInterface::STATUS_CANCELLED,
        'active' => ShipmentInterface::STATUS_IN_PROGRESS,
        'not_specified' => ShipmentInterface::STATUS_PENDING,
        'stale_ready_to_ship' => ShipmentInterface::STATUS_IN_PROGRESS,
        'stale_shipped' => ShipmentInterface::STATUS_IN_PROGRESS,
    ];

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function getProductCategories(): array
    {
        $url = $this->buildUrl('users/me');
        $test = $this->query($url);
        return [];
    }

    /**
     * Обновление информации о продуктах
     * @param ProductInterface[] $products
     * @return void
     */
    public function uploadProductsInformation(array $products)
    {
        // TODO: Implement uploadProductsInformation() method.
    }

    /**
     * @return ProductInterface[]
     */
    public function getProductsInformation(): array
    {
        // TODO: Implement getProductsInformation() method.
    }

    /**
     * Добавление продукта
     * @param ProductInterface $product
     * @return bool
     */
    public function addProduct(ProductInterface $product): bool
    {
        // TODO: Implement addProduct() method.
    }

    /**
     * Удаление продукта
     * @param string $foreignId
     * @return bool
     */
    public function removeProduct(string $foreignId): bool
    {
        // TODO: Implement removeProduct() method.
    }

    /**
     * Обновление информации о продукте
     * @param ProductInterface $product
     * @return bool
     */
    public function updateProduct(ProductInterface $product): bool
    {
        // TODO: Implement updateProduct() method.
    }

    /**
     * Изменение доступности продукта
     * @param ProductInterface $product
     * @param bool $available
     * @return mixed
     */
    public function changeProductAvailability(ProductInterface $product, bool $available): bool
    {
        // TODO: Implement changeProductAvailability() method.
    }

    /**
     * Получение списка новых заказов
     * @return OrderInterface[]
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function getNewOrders(): array
    {
        $offset = 0;
        $limit = 50;
        $orders = [];
        do {
            $url = $this->buildUrl('orders/search', [
                'seller' => $this->sellerId,
                'shipping.status' => 'pending',
                'order.status' => 'paid',
                'offset' => $offset,
                'limit' => $limit
            ]);
            $response = $this->query($url);
            if ($response['status'] != static::STATUS_SUCCESS) {
                throw new \Exception($response['error']);
            }
            $response = $response['response'];
            foreach ($response['results'] as $result) {
                $orders[] = $this->prepareOrder($result);
            }
            $totalCount = $response['paging']['total'];
            $offset += $limit;
        } while ($offset < $totalCount);
        return $orders;
    }

    /**
     * @param array $response
     * @return OrderInterface|null
     */
    protected function prepareOrder(array $response): ?OrderInterface
    {
        $order = new Order([
            'status' => $this->orderStatusMapping[$response['status']] ?? OrderInterface::STATUS_REQUIRE_PAYMENT,
            'foreignId' => $response['id'],
            'foreignStatus' => $response['status'],
            'paidAmount' => $response['paid_amount'],
            'totalAmount' => $response['total_amount'],
            'shippingAmount' => $response['total_amount'] - $response['total_amount_with_shipping'],
            'shipment' => $this->prepareShipment($response['shipping'] ?? []),
            'orderProducts' => [],
            'customerInformation' => $this->prepareCustomerInfo($response),
            'customerAddress' => $this->prepareCustomerAddress($response['shipping']['receiver_address'] ?? []),
            'comment' => $response['shipping']['comment'] ?? null,
            'currencyIsoCode' => $response['currency_id'] ?? null,
            'originalData' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);
        foreach ($response['order_items'] as $item) {
            $order->orderProducts[] = $this->prepareProduct($item);
        }
        return $order;
    }

    /**
     * @param array $response
     * @return CustomerAddressInterface
     */
    protected function prepareCustomerAddress(array $response): CustomerAddressInterface
    {
        return new CustomerAddress([
            'street' => $response['street_name'] ?? '',
            'houseNumber' => $response['street_number'] ?? '',
            'zipCode' => $response['zip_code'] ?? '',
            'city' => $response['neighborhood']['name'] ?? '',
            'country' => $response['country']['id'] ?? '',
            'district' => $response['state']['name'] ?? '',
            'province' => $response['city']['name'] ?? '',
            'longitude' => $response['longitude'] ?? null,
            'latitude' => $response['latitude'] ?? null,
            'fullAddress' => $response['address_line'] ?? null,
            'additionalAddress' => $response['comment'] ?? null
        ]);
    }

    /**
     * @param array $response
     * @return CustomerInformationInterface
     */
    protected function prepareCustomerInfo(array $response): CustomerInformationInterface
    {
        return new CustomerInformation([
            'firstName' => $response['shipping']['receiver_address']['receiver_name'] ?? $response['buyer']['first_name'] ?? '(empty)',
            'lastName' => empty($response['shipping']['receiver_address']['receiver_name']) ? $response['buyer']['last_name'] ?? '' : '',
            'phone' => $response['shipping']['receiver_address']['receiver_phone'] ?? $response['buyer']['phone']['number'] ?? '(empty)',
            'email' => $response['buyer']['email'] ?? '',
        ]);
    }

    /**
     * @param array $data
     * @return OrderProductInterface
     */
    protected function prepareProduct(array $data): OrderProductInterface
    {
        $product = new Product([
            'foreignId' => $data['item']['id'],
            'currencyIsoCode' => $data['currency_id'],
        ]);
        return new OrderProduct([
            'product' => $product,
            'quantity' => $data['quantity'],
            'price' => $data['unit_price'],
            'totalPrice' => $data['full_unit_price'],
        ]);
    }

    /**
     * @param array $data
     * @return ShipmentInterface
     */
    protected function prepareShipment(array $data): ShipmentInterface
    {
        return new Shipment([
            'foreignStatus' => $data['status'] ?? null,
            'status' => $this->shipmentStatusMapping[$data['status']] ?? null,
            'sentAt' => !empty($data['date_first_printed'] ?? $data['date_created'] ?? null) ? strtotime($data['date_first_printed'] ?? $data['date_created']) : null,
        ]);
    }




    /**
     * Создание заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function createShipment(OrderInterface $order)
    {
        // TODO: Implement createShipment() method.
    }

    /**
     * Отмена заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function cancelShipment(OrderInterface $order)
    {
        // TODO: Implement cancelShipment() method.
    }

    /**
     * Обновление информации о доставке
     * @param OrderInterface $order
     * @return mixed
     */
    public function updateShipment(OrderInterface $order)
    {
        // TODO: Implement updateShipment() method.
    }

    /**
     * @return mixed|null|string
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    protected function authorization()
    {
        $accessToken = $this->getAccessTokenFromCache();
        if (!$accessToken) {
            $refreshToken = $this->getRefreshToken();
            $data = [
                'client_id' => $this->clientId,
                'client_secret' => $this->secretKey,
            ];
            if (!is_null($refreshToken)) {
                $additionalData = [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                ];
            } else {
                $additionalData = [
                    'grant_type' => 'authorization_code',
                    'code' => $this->getAuthCode(),
                    'redirect_uri' => $this->redirectUri,
                ];
            }
            $response = $this->query($this->authorizationUrl, array_merge($data, $additionalData));
            if ($response['status'] == static::STATUS_SUCCESS) {
                $response = $response['response'];
                $this->saveAccessTokenToCache($response);
                if (!empty($response['refresh_token'])) {
                    $this->saveRefreshToken($response['refresh_token']);
                }
                if (!empty($response['access_token'])) {
                    $accessToken = $response['access_token'];
                } else {
                    throw new \Exception('Access token not found in response from Mercadolibre API.');
                }
            } else {
                throw new \Exception($response['error']);
            }
        }

        return $accessToken;
    }

    /**
     * @param string $path
     * @param array $params
     * @param bool $auth
     * @return string
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    protected function buildUrl(string $path, array $params = [], bool $auth = true): string
    {
        if (empty($params['access_token']) && $auth) {
            $params['access_token'] = $this->authorization();
        }
        return rtrim($this->baseApiUrl, '/') . '/' . trim($path, '/?') . '?' . http_build_query($params);
    }

    /**
     * @param string $url
     * @param array|null $postData
     * @return array
     * @throws \yii\httpclient\Exception
     */
    protected function query(string $url, array $postData = null): array
    {
        $client = new Client();
        $client->setTransport(CurlTransport::class);
        $request = $client->createRequest();
        if (!empty($postData)) {
            $request->setData($postData);
            $request->setMethod('post');
        }
        $request->setOptions([
            'followLocation' => true
        ]);
        $request->setUrl($url);
        $response = $request->send();
        $result = [
            'status' => static::STATUS_FAIL,
            'error' => null,
            'response' => json_decode($response->content, true)
        ];
        if ($response->isOk) {
            $result['status'] = static::STATUS_SUCCESS;
        } else {
            $result['error'] = $result['response']['message'] ?? $result['response']['error'] ?? $response->getStatusCode();
        }
        return $result;
    }

    /**
     * @return null|string
     */
    protected function getAccessTokenFromCache(): ?string
    {
        if ($this->cacheService && ($authorization = json_decode($this->cacheService->get($this->getAuthorizationCacheKey()), true))) {
            if (!empty($authorization['expires_at']) && $authorization['expires_at'] > time()) {
                return $authorization['access_token'];
            }
        }
        return null;
    }

    /**
     * Сохранение информации о получении токена
     * @param array $response
     */
    protected function saveAccessTokenToCache(array $response)
    {
        if ($this->cacheService && !empty($response['access_token']) && !empty($response['expires_in'])) {
            $data = [
                'access_token' => $response['access_token'],
                'expires_at' => time() + $response['expires_in'],
                'refresh_token' => $response['refresh_token'] ?? null
            ];
            $this->cacheService->save($this->getAuthorizationCacheKey(), json_encode($data, JSON_UNESCAPED_UNICODE), $response['expires_in']);
        }
    }

    /**
     * @return string
     */
    protected function getAuthorizationCacheKey(): string
    {
        return static::CACHE_ACCESS_TOKEN_KEY;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    protected function getAuthCode(): ?string
    {
        if ($this->storageService) {
            $code = $this->storageService->get(static::STORAGE_AUTH_CODE_KEY);
            if (!is_null($code)) {
                $this->authCode = $code;
            }
        }
        if (is_null($this->authCode)) {
            throw new \Exception('Parameter "authentication code" is empty');
        }
        return $this->authCode;
    }

    /**
     * @return null|string
     */
    protected function getRefreshToken(): ?string
    {
        if ($this->storageService) {
            return $this->storageService->get(static::STORAGE_REFRESH_TOKEN_KEY);
        }
        return null;
    }

    /**
     * @param string $token
     */
    protected function saveRefreshToken(string $token)
    {
        if ($this->storageService) {
            $this->storageService->save(static::STORAGE_REFRESH_TOKEN_KEY, $token);
        }
    }

    /**
     * @return StorageServiceInterface|null
     */
    public function getStorageService(): ?StorageServiceInterface
    {
        return $this->storageService;
    }

    /**
     * @param StorageServiceInterface $storage
     * @return ObjectWithStorageServiceInterface
     */
    public function setStorageService(StorageServiceInterface $storage): ObjectWithStorageServiceInterface
    {
        $this->storageService = $storage;
        return $this;
    }

    /**
     * @param string $foreignId
     * @return OrderInterface|null
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function getOrder(string $foreignId): ?OrderInterface
    {
        $url = $this->buildUrl("orders/{$foreignId}");
        $response = $this->query($url);
        if ($response['status'] != static::STATUS_SUCCESS) {
            throw new \Exception($response['error']);
        }
        return $this->prepareOrder($response['response']);
    }
}