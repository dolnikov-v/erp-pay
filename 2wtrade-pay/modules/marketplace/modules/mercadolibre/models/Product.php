<?php

namespace app\modules\marketplace\modules\mercadolibre\models;

/**
 * Class Product
 * @package app\modules\marketplace\modules\mercadolibre\models
 */
class Product extends \app\modules\marketplace\models\Product
{
    public $buyingMode = "buy_it_now";
    public $listingTypeId = "bronze";
    public $condition = "new";
    public $videoId;
    public $warranty;

    /**
     * @return array
     */
    protected  function additionalRules(): array
    {
        return [
            [['buyingMode', 'listingTypeId', 'condition', 'videoId', 'warranty'], 'string'],
            [['buyingMode', 'listingTypeId', 'condition', 'videoId', 'warranty'], 'trim'],
        ];
    }

    /**
     * @return array
     */
    protected  function additionalAttributeLabels(): array
    {
        return [
            'buyingMode' => \Yii::t('common', 'Тип покупки'),
            'listingTypeId' => \Yii::t('common', ''),
            'condition' => \Yii::t('common', 'Состояние'),
            'videoId' => \Yii::t('common', 'Идентификтор видео на youtube'),
            'warranty' => \Yii::t('common', 'Срок действия гарантии'),
        ];
    }

    /**
     * @return array
     */
    public function getOptionalAttributes(): array
    {
        return [
            'buyingMode' => $this->buyingMode,
            'listingTypeId' => $this->listingTypeId,
            'condition' => $this->condition,
            'videoId' => $this->videoId,
            'warranty' => $this->warranty
        ];
    }
}