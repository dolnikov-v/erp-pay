<?php

namespace app\modules\marketplace\modules\lelong;

use app\modules\marketplace\abstracts\extensions\MarketPlaceWithWebHookInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\components\MarketPlaceWithExtendedServices;
use app\modules\marketplace\exceptions\InvalidParamException;
use app\modules\marketplace\modules\lelong\components\LelongApi;

/**
 * Class Lelong
 * @package app\modules\marketplace\modules\lelong
 *
 * @property LelongApi $apiAdapter
 */
class Lelong extends MarketPlaceWithExtendedServices implements MarketPlaceWithWebHookInterface
{
    /**
     * @param string $foreignId
     * @return OrderInterface|null
     * @throws \Exception
     */
    public function getOrderByForeignId(string $foreignId): ?OrderInterface
    {
        return null;
    }

    /**
     * @param array $request
     * @return array
     * @throws \Exception
     * @throws InvalidParamException
     */
    public function handleWebHookRequest(array $request): array
    {
        if (empty($request['orderInformation'])) {
            throw new \Exception(\Yii::t('common', 'Поле {field} не найдено.', ['field' => 'orderInformation']));
        }

        $orderData = json_decode($request['orderInformation'], true);
        $action = $orderData['action'] ?? '';

        unset($orderData);

        switch ($action) {
            case 'UpdateOrder':
                return $this->createOrderByWebHook($request);
            default:
                throw new InvalidParamException('Не удалось определить требуемое действие.');
        }
    }

    /**
     * @param array $request
     * @return array
     * @throws \Exception
     */
    protected function createOrderByWebHook(array $request): array
    {
        $orderData = json_decode($request['orderInformation'], true);

        $foreignId = (int)$orderData['orderid'];

        if (empty($foreignId)) {
            throw new \Exception('Не удалось определить идентификатор заказа.');
        }

        $orderData = $this->apiAdapter->prepareOrder($orderData);
        $orderId = $this->dataConverter->createOrUpdateOrderFromData($orderData, $this);

        return [
            'version' => '1.0',
            'orderId' => $foreignId,
            'merchantorderid' => $orderId,
            'action' => $orderData['action'],
            'status' => 'Success',
            'desc' => 'Successfully updated order.'
        ];
    }

    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->params['apiKey'];
    }
}
