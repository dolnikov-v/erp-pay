<?php

namespace app\modules\marketplace\modules\lelong\components;

use app\modules\marketplace\abstracts\MarketPlaceIntegrationInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\components\MarketPlaceIntegration;
use app\modules\marketplace\models\CustomerAddress;
use app\modules\marketplace\models\CustomerInformation;
use app\modules\marketplace\models\Order;
use app\modules\marketplace\models\OrderProduct;
use app\modules\marketplace\models\Product;
use app\modules\marketplace\models\Shipment;

/**
 * Class LelongApi
 * @package app\modules\marketplace\modules\lelong\components
 */
class LelongApi extends MarketPlaceIntegration implements MarketPlaceIntegrationInterface
{
    /**
     * @var array
     */
    protected $currencyMapping = [
        'RM' => 'MYR'
    ];

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @param array $response
     * @return CustomerAddressInterface
     */
    protected function prepareCustomerAddress(array $response): CustomerAddressInterface
    {
        return new CustomerAddress([
            'zipCode' => $response['shippingpostcode'] ?? '',
            'city' => $response['shippingcity'] ?? '',
            'country' => $response['shippingcountry'] ?? '',
            'fullAddress' => $response['shippingaddress1'] ?? null,
            'additionalAddress' => $response['shippingaddress2'] ?? null
        ]);
    }

    /**
     * @param array $response
     * @return CustomerInformationInterface
     */
    protected function prepareCustomerInfo(array $response): CustomerInformationInterface
    {
        return new CustomerInformation([
            'firstName' => $response['shippingname'] ?? '(empty)',
            'phone' => $response['shippingphone1'] ?? '(empty)',
            'mobile' => $response['shippingphone2'] ?? '(empty)',
            'email' => $response['shippingemail'] ?? null
        ]);
    }

    /**
     * @param array $response
     * @return OrderInterface|null
     */
    public function prepareOrder(array $response): ?OrderInterface
    {
        $order = new Order([
            'status' => OrderInterface::STATUS_PAID,
            'foreignId' => $response['orderid'],
            'paidAmount' => $response['totalorderprice'],
            'totalAmount' => $response['totalorderprice'],
            'shippingAmount' => $response['shippingfees'],
            'shipment' => $this->prepareShipment(),
            'orderProducts' => [],
            'customerInformation' => $this->prepareCustomerInfo($response),
            'customerAddress' => $this->prepareCustomerAddress($response),
            'currencyIsoCode' => $this->currencyMapping[$response['currency']] ?? null,
            'originalData' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        foreach ($response['ordereditem'] as $item) {
            $order->orderProducts[] = $this->prepareProduct($item, $this->currencyMapping[$response['currency']] ?? null);
        }

        return $order;
    }

    /**
     * @param array $data
     * @param string|null $currency
     * @return OrderProductInterface
     */
    protected function prepareProduct(array $data, ?string $currency): OrderProductInterface
    {
        $product = new Product([
            'foreignId' => $data['guid'],
            'currencyIsoCode' => $currency,
        ]);
        return new OrderProduct([
            'product' => $product,
            'quantity' => $data['quantity'],
            'price' => $data['priceperitem'],
            'totalPrice' => $data['totalprice'],
        ]);
    }

    /**
     * @return ShipmentInterface
     */
    protected function prepareShipment(): ShipmentInterface
    {
        return new Shipment([
            'status' => ShipmentInterface::STATUS_PENDING
        ]);
    }

    /**
     * @param string $foreignId
     * @return OrderInterface|null
     * @throws \Exception
     */
    public function getOrder(string $foreignId): ?OrderInterface
    {
        return null;
    }

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategories(): array
    {
        // TODO: Implement getProductCategories() method.
    }

    /**
     * Обновление информации о продуктах
     * @param ProductInterface[] $products
     * @return void
     */
    public function uploadProductsInformation(array $products)
    {
        // TODO: Implement uploadProductsInformation() method.
    }

    /**
     * @return ProductInterface[]
     */
    public function getProductsInformation(): array
    {
        // TODO: Implement getProductsInformation() method.
    }

    /**
     * Добавление продукта
     * @param ProductInterface $product
     * @return bool
     */
    public function addProduct(ProductInterface $product): bool
    {
        // TODO: Implement addProduct() method.
    }

    /**
     * Удаление продукта
     * @param string $foreignId
     * @return bool
     */
    public function removeProduct(string $foreignId): bool
    {
        // TODO: Implement removeProduct() method.
    }

    /**
     * Обновление информации о продукте
     * @param ProductInterface $product
     * @return bool
     */
    public function updateProduct(ProductInterface $product): bool
    {
        // TODO: Implement updateProduct() method.
    }

    /**
     * Изменение доступности продукта
     * @param ProductInterface $product
     * @param bool $available
     * @return mixed
     */
    public function changeProductAvailability(ProductInterface $product, bool $available): bool
    {
        // TODO: Implement changeProductAvailability() method.
    }

    /**
     * Получение списка новых заказов
     * @return OrderInterface[]
     */
    public function getNewOrders(): array
    {
        // TODO: Implement getNewOrders() method.
    }

    /**
     * Создание заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function createShipment(OrderInterface $order)
    {
        // TODO: Implement createShipment() method.
    }

    /**
     * Отмена заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function cancelShipment(OrderInterface $order)
    {
        // TODO: Implement cancelShipment() method.
    }

    /**
     * Обновление информации о доставке
     * @param OrderInterface $order
     * @return mixed
     */
    public function updateShipment(OrderInterface $order)
    {
        // TODO: Implement updateShipment() method.
    }
}
