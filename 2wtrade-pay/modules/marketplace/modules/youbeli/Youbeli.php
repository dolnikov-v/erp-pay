<?php

namespace app\modules\marketplace\modules\Youbeli;

use app\modules\marketplace\abstracts\extensions\MarketPlaceWithGetOrdersByTimeIntervalInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\components\MarketPlaceWithExtendedServices;
use app\modules\marketplace\modules\youbeli\components\YoubeliApi;

/**
 * Class Youbeli
 * @package app\modules\marketplace\modules\youbeli
 *
 * @property YoubeliApi $apiAdapter
 */

class Youbeli extends MarketPlaceWithExtendedServices implements MarketPlaceWithGetOrdersByTimeIntervalInterface
{


    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     */
    public function createOrders(\DateTime $from, \DateTime $to): array
    {
        $result = $this->apiAdapter->getOrders($from, $to);
        $orderIds = [];

        foreach ($result['data'] as $order) {
            $orderIds['data'][] = $this->dataConverter->createOrUpdateOrderFromData($order, $this);
        }

        $orderIds['url'] = $result['url'];
        $orderIds['data_log'] = $result['data_log'];

        return $orderIds;
    }


    /**
     * @param string $foreignId
     * @return OrderInterface|null
     */
    public function getOrderByForeignId(string $foreignId): ?OrderInterface
    {
        return null;
    }


    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->params['apiKey'];
    }
}