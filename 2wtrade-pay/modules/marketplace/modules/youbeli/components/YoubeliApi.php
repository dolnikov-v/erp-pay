<?php

namespace app\modules\marketplace\modules\youbeli\components;

use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\components\MarketPlaceIntegration;
use app\modules\marketplace\models\CustomerAddress;
use app\modules\marketplace\models\CustomerInformation;
use app\modules\marketplace\models\Order;
use app\modules\marketplace\models\OrderProduct;
use app\modules\marketplace\models\Shipment;
use app\modules\marketplace\modules\mercadolibre\models\Product;
use yii\httpclient\Client;

/**
 * Class MercadoLibreApi
 * @package app\modules\marketplace\modules\mercedolibre\components
 */
class YoubeliApi extends MarketPlaceIntegration
{

    /**
     * @var string
     */
    public $currency = 'MYR';

    /**
     * @var string
     */
    public $baseUrl;

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var string
     */
    public $storeId;


    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     */
    public function getOrders(\DateTime $from, \DateTime $to): array
    {
        $offset = 0;
        $limit = 50;
        $ordersYoubeliId = [];

        $dateFrom = $from->getTimestamp();
        $dateTo = $to->getTimestamp();

        $headers = [
            'content-type' => 'application/json',
            'charset' => 'UTF-8',
        ];

        $url = 'get_orders_list.php';

        do {
            $data = [
                'sku' => 'getOrders',
                'timestamp' => time(),
                'offset' => $offset,
                'limit' => $limit,
                'store_id' => $this->storeId,
                'created_after' => $dateFrom,
                'created_before' => $dateTo
            ];
            $data['sign'] = hash_hmac('sha256', json_encode($data), $this->apiKey);

            $client = new Client(['baseUrl' => $this->baseUrl]);
            $response = $client->createRequest()
                ->setMethod('POST')
                ->addHeaders($headers)
                ->setUrl($url)
                ->setFormat(Client::FORMAT_JSON)
                ->setData($data)
                ->send();

            $ordersYoubeliId = array_merge($ordersYoubeliId, $response->data->orders);
            $offset += $limit;

        } while (!empty($response->data->orders));


        $result = [];
        foreach ($ordersYoubeliId as $orderYoubeliId) {
            $result[] = $orderYoubeliId['order_id'];
        }

        $OrdersDetails = $this->getOrdersDetails($result);

        $orders = [
            'error' => false,
            'data' => [],
            'url' => "{$this->baseUrl}/$url",
            'data_log' => json_encode(['headers' => $headers])
        ];

        foreach ($OrdersDetails as $data) {
            $orders['data'][] = $this->prepareOrder($data);
        }

        return $orders;

    }


    /**
     * @param $orders
     * @return array
     */
    public function getOrdersDetails($orders)
    {

        $limit = 50;
        $ordersDetails = [];

        for ($i = 0, $j = 0; $i < count($orders); $i += $limit, $j++) {

            $groupOrders = array_slice($orders, $i, $limit);

            $data = [
                'sku' => 'getOrdersDetails',
                'store_id' => $this->storeId,
                'timestamp' => time(),
                'orders_id' => $groupOrders,
            ];
            $data['sign'] = hash_hmac('sha256', json_encode($data), $this->apiKey);

            $client = new Client(['baseUrl' => $this->baseUrl]);
            $response = $client->createRequest()
                ->setMethod('POST')
                ->addHeaders([
                    'content-type' => 'application/json',
                    'charset' => 'UTF-8',
                ])
                ->setUrl('get_order_details.php')
                ->setFormat(Client::FORMAT_JSON)
                ->setData($data)
                ->send();

            $ordersDetails = array_merge($ordersDetails, $response->data->orders);
        }

        return $ordersDetails;

    }


    /**
     * @param array $response
     * @return CustomerAddressInterface
     */
    protected function prepareCustomerAddress(array $response): CustomerAddressInterface
    {
        return new CustomerAddress([
            'zipCode' => $response['shipping']['postcode'] ?? '',
            'country' => $response['shipping']['country'] ?? '',
            'state' => $response['shipping']['state'] ?? '',
            'city' => $response['shipping']['city'] ?? '',
            'fullAddress' => $response['shipping']['street_address'] ?? null,
        ]);
    }

    /**
     * @param array $response
     * @return CustomerInformationInterface
     */
    protected function prepareCustomerInfo(array $response): CustomerInformationInterface
    {
        //
        return new CustomerInformation([
            'firstName' => $response['customer_name'] ?? '(empty)',
            'phone' => $response['shipping']['telephone'] ? $response['shipping']['handphone'] : '(empty)',
            'mobile' => $response['shipping']['handphone'] ?? '(empty)',
            'email' => $response['customer_email'] ?? null
        ]);
    }

    /**
     * @param array $response
     * @return OrderInterface|null
     */
    public function prepareOrder(array $response): ?OrderInterface
    {
        $order = new Order([
            'status' => OrderInterface::STATUS_PAID,
            'foreignId' => $response['order_id'],
            'foreignStatus' => $response['order_status'],
            'paidAmount' => $response['order_total'],
            'totalAmount' => $response['order_total'],
            'shippingAmount' => $response['shipping_fee'],
            'shipment' => $this->prepareShipment(),
            'orderProducts' => [],
            'customerInformation' => $this->prepareCustomerInfo($response),
            'customerAddress' => $this->prepareCustomerAddress($response),
            'currencyIsoCode' => $this->currency,
            'originalData' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        foreach ($response['products'] as $item) {
            $order->orderProducts[] = $this->prepareProduct($item, $this->currency);
        }

        return $order;
    }

    /**
     * @param array $data
     * @param string|null $currency
     * @return OrderProductInterface
     */
    protected function prepareProduct(array $data, ?string $currency): OrderProductInterface
    {
        $product = new Product([
            'foreignId' => $data['order_product_id'],
            'currencyIsoCode' => $currency,
            'name' => $data['name']
        ]);

        return new OrderProduct([
            'product' => $product,
            'quantity' => $data['quantity'],
            'price' => $data['price'],
            'totalPrice' => $data['total_price'],
        ]);
    }

    /**
     * @return ShipmentInterface
     */
    protected function prepareShipment(): ShipmentInterface
    {
        return new Shipment([
            'status' => ShipmentInterface::STATUS_PENDING
        ]);
    }

    /**
     * @param string $foreignId
     * @return OrderInterface|null
     * @throws \Exception
     */
    public function getOrder(string $foreignId): ?OrderInterface
    {
        return null;
    }

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategories(): array
    {
        // TODO: Implement getProductCategories() method.
    }

    /**
     * Обновление информации о продуктах
     * @param ProductInterface[] $products
     * @return void
     */
    public function uploadProductsInformation(array $products)
    {
        // TODO: Implement uploadProductsInformation() method.
    }

    /**
     * @return ProductInterface[]
     */
    public function getProductsInformation(): array
    {
        // TODO: Implement getProductsInformation() method.
    }

    /**
     * Добавление продукта
     * @param ProductInterface $product
     * @return bool
     */
    public function addProduct(ProductInterface $product): bool
    {
        // TODO: Implement addProduct() method.
    }

    /**
     * Удаление продукта
     * @param string $foreignId
     * @return bool
     */
    public function removeProduct(string $foreignId): bool
    {
        // TODO: Implement removeProduct() method.
    }

    /**
     * Обновление информации о продукте
     * @param ProductInterface $product
     * @return bool
     */
    public function updateProduct(ProductInterface $product): bool
    {
        // TODO: Implement updateProduct() method.
    }

    /**
     * Изменение доступности продукта
     * @param ProductInterface $product
     * @param bool $available
     * @return mixed
     */
    public function changeProductAvailability(ProductInterface $product, bool $available): bool
    {
        // TODO: Implement changeProductAvailability() method.
    }

    /**
     * Получение списка новых заказов
     * @return OrderInterface[]
     */
    public function getNewOrders(): array
    {
        // TODO: Implement getNewOrders() method.
    }

    /**
     * Создание заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function createShipment(OrderInterface $order)
    {
        // TODO: Implement createShipment() method.
    }

    /**
     * Отмена заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function cancelShipment(OrderInterface $order)
    {
        // TODO: Implement cancelShipment() method.
    }

    /**
     * Обновление информации о доставке
     * @param OrderInterface $order
     * @return mixed
     */
    public function updateShipment(OrderInterface $order)
    {
        // TODO: Implement updateShipment() method.
    }
}