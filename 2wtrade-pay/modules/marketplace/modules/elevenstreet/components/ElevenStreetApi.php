<?php

namespace app\modules\marketplace\modules\elevenstreet\components;

use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\components\MarketPlaceIntegration;
use app\modules\marketplace\models\CustomerAddress;
use app\modules\marketplace\models\CustomerInformation;
use app\modules\marketplace\models\Order;
use app\modules\marketplace\models\OrderProduct;
use app\modules\marketplace\models\Product;
use app\modules\marketplace\models\Shipment;
use yii\httpclient\Client;
use yii\httpclient\Exception;

/**
 * Class ElevenStreetApi
 * @package app\modules\marketplace\modules\elevenstreet\components
 */
class ElevenStreetApi extends MarketPlaceIntegration
{
    /**
     * @var string
     */
    public $currency = 'MYR';

    /**
     * @var string
     */
    public $baseApiUrl;

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     * @throws \Exception
     */
    public function getOrders(\DateTime $from, \DateTime $to): array
    {
        $headers = [
            "Content-Type" => "application/xml",
            "Accept-Charset" => "utf-8",
            "openapikey" => $this->apiKey
        ];

        $dateStart = clone $from;
        $dateEnd = clone $from;
        $dateEnd->add(new \DateInterval('P7D'));

        if ($dateEnd > $to) {
            $dateEnd = clone $to;
        }

        $result = [];

        $client = new Client();
        $client->setTransport('yii\httpclient\CurlTransport');

        while ($dateStart < $to) {
            $dateFrom = $dateStart->format('dmYHi');
            $dateTo = $dateEnd->format('dmYHi');

            $url = "{$this->baseApiUrl}$dateFrom/$dateTo";

            try {
                $response = $client->createRequest()
                    ->setUrl($url)
                    ->setMethod('get')
                    ->addHeaders($headers)
                    ->setFormat(Client::FORMAT_XML)
                    ->send();
            } catch (Exception $e) {
                return [
                    'error' => true,
                    'message' => $e->getMessage(),
                    'url' => $url,
                    'data_log' => json_encode(['headers' => $headers])
                ];
            }

            if ($response->isOk) {
                $xml = str_replace(' xmlns:ns2="http://skt.tmall.business.openapi.spring.service.client.domain"', '', $response->getContent());
                try {
                    $content = $this->convertXmlToArray(simplexml_load_string($xml));
                    $result[] = $content;
                    if (isset($content['result_code']) and $content['result_code'] != 0) {
                        return [
                            'error' => true,
                            'message' => $content['result_code'] . ': ' . $content['result_text'],
                            'url' => $url,
                            'data_log' => json_encode(['headers' => $headers])
                        ];
                    }
                } catch (\TypeError $e) {
                    return [
                        'error' => true,
                        'message' => $e->getMessage(),
                        'url' => $url,
                        'data_log' => json_encode(['headers' => $headers])
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Response error',
                    'url' => $url,
                    'data_log' => json_encode(['response' => $response->content])
                ];
            }

            $dateStart = clone $dateEnd;
            $dateStart->add(new \DateInterval('PT1M'));
            $dateEnd = clone $dateStart;
            $dateEnd->add(new \DateInterval('P7D'));

            if ($dateEnd > $to) {
                $dateEnd = clone $to;
            }
        }

        $orders = [
            'error' => false,
            'data' => [],
            'url' => "{$this->baseApiUrl}{$from->format('dmYHi')}/{$to->format('dmYHi')}",
            'data_log' => json_encode(['headers' => $headers])
        ];

        foreach ($result as $data) {
            foreach ($data['ns2:order'] as $order) {
                $orders['data'][] = $this->prepareOrder($order);
            }
        }

        return $orders;
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return array
     */
    protected function convertXmlToArray(\SimpleXMLElement $xml): array
    {
        $arr = array();

        foreach ($xml->children() as $elem) {
            if (!$elem->children()) {
                $arr[$elem->getName()] = strval($elem);
            } else {
                $arr[$elem->getName()][] = $this->convertXmlToArray($elem);
            }
        }

        return $arr;
    }

    /**
     * @param array $response
     * @return CustomerAddressInterface
     */
    protected function prepareCustomerAddress(array $response): CustomerAddressInterface
    {
        return new CustomerAddress([
            'zipCode' => $response['rcvrMailNo'] ?? '',
            'fullAddress' => $response['rcvrBaseAddr'] ?? null,
            'additionalAddress' => $response['rcvrDtlsAddr'] ?? null
        ]);
    }

    /**
     * @param array $response
     * @return CustomerInformationInterface
     */
    protected function prepareCustomerInfo(array $response): CustomerInformationInterface
    {
        return new CustomerInformation([
            'firstName' => $response['rcvrNm'] ?? '(empty)',
            'phone' => $response['rcvrTlphn'] ?? '(empty)',
            'email' => $response['ordEmail'] ?? '',
        ]);
    }

    /**
     * @param array $response
     * @return OrderInterface|null
     */
    protected function prepareOrder(array $response): ?OrderInterface
    {
        $order = new Order([
            'status' => OrderInterface::STATUS_PAID,
            'foreignId' => $response['ordNo'],
            'paidAmount' => $response['ordPayAmt'] + $response['lstTmallDscPrc'],
            'totalAmount' => $response['ordAmt'],
            'shippingAmount' => $response['ordPayAmt'] - $response['ordAmt'],
            'shipment' => $this->prepareShipment(),
            'orderProducts' => [],
            'customerInformation' => $this->prepareCustomerInfo($response),
            'customerAddress' => $this->prepareCustomerAddress($response),
            'currencyIsoCode' => $this->currency,
            'originalData' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        $order->orderProducts[] = $this->prepareProduct($response);

        return $order;
    }

    /**
     * @param array $data
     * @return OrderProductInterface
     */
    protected function prepareProduct(array $data): OrderProductInterface
    {
        $product = new Product([
            'foreignId' => $data['prdNo'],
            'currencyIsoCode' => $this->currency
        ]);

        return new OrderProduct([
            'product' => $product,
            'quantity' => $data['ordQty'],
            'price' => $data['selPrc'],
            'totalPrice' => $data['selPrc'] * $data['ordQty'],
        ]);
    }

    /**
     * @return ShipmentInterface
     */
    protected function prepareShipment(): ShipmentInterface
    {
        return new Shipment([
            'status' => ShipmentInterface::STATUS_PENDING,
        ]);
    }

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategories(): array
    {
        // TODO: Implement getProductCategories() method.
    }

    /**
     * Обновление информации о продуктах
     * @param ProductInterface[] $products
     * @return void
     */
    public function uploadProductsInformation(array $products)
    {
        // TODO: Implement uploadProductsInformation() method.
    }

    /**
     * @return ProductInterface[]
     */
    public function getProductsInformation(): array
    {
        // TODO: Implement getProductsInformation() method.
    }

    /**
     * Добавление продукта
     * @param ProductInterface $product
     * @return bool
     */
    public function addProduct(ProductInterface $product): bool
    {
        // TODO: Implement addProduct() method.
    }

    /**
     * Удаление продукта
     * @param string $foreignId
     * @return bool
     */
    public function removeProduct(string $foreignId): bool
    {
        // TODO: Implement removeProduct() method.
    }

    /**
     * Обновление информации о продукте
     * @param ProductInterface $product
     * @return bool
     */
    public function updateProduct(ProductInterface $product): bool
    {
        // TODO: Implement updateProduct() method.
    }

    /**
     * Изменение доступности продукта
     * @param ProductInterface $product
     * @param bool $available
     * @return mixed
     */
    public function changeProductAvailability(ProductInterface $product, bool $available): bool
    {
        // TODO: Implement changeProductAvailability() method.
    }

    /**
     * Получение списка новых заказов
     * @return OrderInterface[]
     */
    public function getNewOrders(): array
    {
        // TODO: Implement getNewOrders() method.
    }

    /**
     * Создание заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function createShipment(OrderInterface $order)
    {
        // TODO: Implement createShipment() method.
    }

    /**
     * Отмена заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function cancelShipment(OrderInterface $order)
    {
        // TODO: Implement cancelShipment() method.
    }

    /**
     * Обновление информации о доставке
     * @param OrderInterface $order
     * @return mixed
     */
    public function updateShipment(OrderInterface $order)
    {
        // TODO: Implement updateShipment() method.
    }

    /**
     * @param string $foreignId
     * @return OrderInterface|null
     */
    public function getOrder(string $foreignId): ?OrderInterface
    {
        // TODO: Implement getOrder() method.
    }
}
