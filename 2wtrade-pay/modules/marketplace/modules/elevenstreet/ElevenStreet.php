<?php

namespace app\modules\marketplace\modules\elevenstreet;

use app\modules\marketplace\abstracts\extensions\MarketPlaceWithGetOrdersByTimeIntervalInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\components\MarketPlaceWithExtendedServices;
use app\modules\marketplace\modules\elevenstreet\components\ElevenStreetApi;

/**
 * Class ElevenStreet
 * @package app\modules\marketplace\modules\elevenstreet
 *
 * @property ElevenStreetApi $apiAdapter
 */
class ElevenStreet extends MarketPlaceWithExtendedServices implements MarketPlaceWithGetOrdersByTimeIntervalInterface
{
    /**
     * @param string $foreignId
     * @return OrderInterface|null
     */
    public function getOrderByForeignId(string $foreignId): ?OrderInterface
    {
        return null;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return int[]
     */
    public function createOrders(\DateTime $from, \DateTime $to): array
    {
        $result = $this->apiAdapter->getOrders($from, $to);

        if (!empty($result['error'])) {
            return $result;
        }

        $orderIds = [];

        foreach ($result['data'] as $order) {
            $orderIds['data'][] = $this->dataConverter->createOrUpdateOrderFromData($order, $this);
        }

        $orderIds['url'] = $result['url'];
        $orderIds['data_log'] = $result['data_log'];

        return $orderIds;
    }

    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->params['apiKey'];
    }
}
