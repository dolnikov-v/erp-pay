<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.11.2018
 * Time: 19:02
 */

namespace app\modules\marketplace\widgets;


use app\widgets\Widget;

/**
 * Class ProductManagerWidget
 * @package app\modules\marketplace\widgets
 */
class ProductFormWidget extends Widget
{
    public $form;
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('product-form-widget', ['model' => $this->model, 'form' => $this->form]);
    }
}