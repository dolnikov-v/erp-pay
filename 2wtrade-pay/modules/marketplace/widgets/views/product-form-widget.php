<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\models\Product $model
 * @var ActiveForm $form
 */

use app\components\widgets\ActiveForm;

$localForm = false;
if (empty($form)) {
    $form = ActiveForm::begin();
    $localForm = true;
}
?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'id')->select2List(\app\models\Product::find()->collection(), ['length' => 1]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'foreignId')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'quantityMultiplier')->textInput() ?>
        </div>
    </div>

<?php if ($localForm) {
    ActiveForm::end();
} ?>