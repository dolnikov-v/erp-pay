<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\components\DefaultMarketPlaceConfiguration $configuration
 * @var ActiveForm $form
 * @var array $deliveryCollection
 */

use app\components\widgets\ActiveForm;
use app\models\Source;

$localForm = false;
if (empty($form)) {
    $form = ActiveForm::begin();
    $localForm = true;
}
?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($configuration, 'sourceId')
            ->select2List(Source::find()->collection(), ['prompt' => '—', 'length' => 1]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($configuration, 'defaultCountryId')->select2List(\app\models\Country::find()->collection(), ['prompt' => '—', 'length' => 1]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($configuration, 'defaultDeliveryId')->select2List($deliveryCollection, ['prompt' => '—', 'length' => 1]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($configuration, 'deliversByStandardService')->checkboxCustom(['value' => 1, 'checked' => $configuration->deliversByStandardService])?>
    </div>
</div>

<?php if ($localForm) {
    ActiveForm::end();
} ?>
