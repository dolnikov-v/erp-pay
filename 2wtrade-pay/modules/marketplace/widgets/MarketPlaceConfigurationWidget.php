<?php

namespace app\modules\marketplace\widgets;


use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\marketplace\components\DefaultMarketPlaceConfiguration;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class DefaultConfigurationWidget
 * @package app\modules\marketplace\widgets
 */
class MarketPlaceConfigurationWidget extends Widget
{
    /**
     * @var DefaultMarketPlaceConfiguration
     */
    public $configuration;
    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function run()
    {
        return $this->render('default-configuration-widget', [
            'configuration' => $this->configuration,
            'form' => $this->form,
            'deliveryCollection' => $this->getDeliveriesCollection(),
        ]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getDeliveriesCollection()
    {
        $queryDeliveries = Delivery::find()
            ->joinWith(['country'])
            ->select([
                'id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'country_name' => Country::tableName() . '.name',
            ])->orderBy([
                Country::tableName() . '.is_partner' => SORT_ASC,
                Country::tableName() . '.is_distributor' => SORT_ASC,
                Country::tableName() . '.active' => SORT_DESC,
                Delivery::tableName() . '.active' => SORT_DESC
            ]);
        $result = [];
        foreach ($queryDeliveries->createCommand()->queryAll() as $record) {
            $result[$record['id']] = \Yii::t('common', $record['country_name']) . ' ' . $record['delivery_name'];
        }
        return $result;
    }
}