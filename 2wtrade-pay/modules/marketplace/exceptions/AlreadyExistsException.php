<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class AlreadyExistsException
 * @package app\modules\marketplace\exceptions
 */
class AlreadyExistsException extends \Exception
{

}