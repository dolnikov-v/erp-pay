<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class NotFoundException
 * @package app\modules\marketplace\exceptions
 */
class NotFoundException extends \Exception
{

}