<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class InvalidConfigurationException
 * @package app\modules\marketplace\exceptions
 */
class InvalidConfigurationException extends \Exception
{

}