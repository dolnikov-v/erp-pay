<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class MarketplaceNotFoundException
 * @package app\modules\marketplace\exceptions
 */
class MarketplaceNotFoundException extends NotFoundException
{

}