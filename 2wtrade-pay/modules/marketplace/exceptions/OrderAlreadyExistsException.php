<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class OrderAlreadyExistsException
 * @package app\modules\marketplace\exceptions
 */
class OrderAlreadyExistsException extends AlreadyExistsException
{

}