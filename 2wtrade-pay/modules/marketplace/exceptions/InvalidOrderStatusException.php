<?php

namespace app\modules\marketplace\exceptions;

/**
 * Class InvalidOrderStatusException
 * @package app\modules\marketplace\exceptions
 */
class InvalidOrderStatusException extends \Exception
{

}