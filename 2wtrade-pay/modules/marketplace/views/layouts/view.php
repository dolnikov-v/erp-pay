<?php
/**
 * @var \yii\web\View $this
 * @var string $content
 */

$module = Yii::$app->getModule('marketplace');
if ($module instanceof \app\modules\marketplace\MarketPlaceModule) {
    $marketplace = $module->getMarketPlaceById(Yii::$app->request->get('marketplaceId'));
}
$this->title = Yii::t('common', 'Просмотр маркетплейса {name}', ['name' => $marketplace->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Маркетплейсы'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Список маркетплейсов'),
    'url' => \yii\helpers\Url::to(['index/index'])
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$actionId = Yii::$app->controller->action->id;
?>

<?= \app\widgets\Panel::widget([
    'nav' => new \app\widgets\Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Настройки'),
                'url' => \yii\helpers\Url::toRoute(['view/configuration', 'marketplaceId' => Yii::$app->request->get('marketplaceId')]),
                'active' => $actionId == 'configuration',
                'visible' => Yii::$app->user->can('marketplace.view.configuration')
            ],
            [
                'label' => Yii::t('common', 'Товары'),
                'url' => \yii\helpers\Url::toRoute(['view/products', 'marketplaceId' => Yii::$app->request->get('marketplaceId')]),
                'active' => $actionId == 'products' || $actionId == 'edit-product',
                'visible' => Yii::$app->user->can('marketplace.view.products')
            ]
        ]
    ]),
    'content' => $content,
    'withBody' => false
]) ?>