<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 */

$products = \app\models\Product::find()->collection();
?>

<?= \app\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => \yii\grid\SerialColumn::class,
        ],
        [
            'attribute' => 'name',
        ],
        [
            'attribute' => 'id',
            'label' => Yii::t('common', 'Название во внутренней системе'),
            'value' => function ($model) use ($products) {
                return $products[$model->id] ?? $model->id;
            }
        ],
        [
            'attribute' => 'foreignId'
        ],
        'actions' => [
            'class' => \app\components\grid\ActionColumn::class,
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($model) use ($marketplace) {
                        return \yii\helpers\Url::toRoute([
                            'view/edit-product',
                            'marketplaceId' => $marketplace->getId(),
                            'id' => $model->uniqueId
                        ]);
                    },
                    'can' => Yii::$app->user->can('marketplace.view.editproduct')
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function ($model) use ($marketplace) {
                        return \yii\helpers\Url::toRoute([
                            'view/remove-product',
                            'marketplaceId' => $marketplace->getId(),
                            'id' => $model->uniqueId
                        ]);
                    },
                    'can' => Yii::$app->user->can('marketplace.view.removeproduct')
                ]
            ]
        ]
    ]
]) ?>
