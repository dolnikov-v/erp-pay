<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\abstracts\product\ProductInterface $model
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 */

use yii\helpers\Url;

$form = \app\components\widgets\ActiveForm::begin();
?>
<?= \app\widgets\Panel::widget([
    'title' => Yii::t('common', $model && $model->getUniqueId() ? 'Редактирование товара "{name}"' : 'Добавление товара', ['name' => $model && $model->getUniqueId() ? $model->getName() : ""]),
    'content' => $this->render('_edit-product-form', [
        'model' => $model,
        'marketplace' => $marketplace,
        'form' => $form
    ]),
    'footer' => $form->submit(Yii::t('common', 'Сохранить')) . $form->link(Yii::t('common', 'Назад к списку товаров'), Url::toRoute([
            'products',
            'marketplaceId' => $marketplace->getId()
        ]))
]) ?>
<?php \app\components\widgets\ActiveForm::end(); ?>