<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 * @var \app\components\widgets\ActiveForm $form
 */
?>

<div class="marketplace-configuration-form">
    <?= $marketplace->configuration->getForm($form) ?>
</div>