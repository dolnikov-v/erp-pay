<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 */

$form = \app\components\widgets\ActiveForm::begin();
?>
<?= \app\widgets\Panel::widget([
    'content' => $this->render('_configuration-form', ['marketplace' => $marketplace, 'form' => $form]),
    'footer' => $form->submit(Yii::t('common', 'Сохранить настройки'))
]) ?>

<?php \app\components\widgets\ActiveForm::end(); ?>
