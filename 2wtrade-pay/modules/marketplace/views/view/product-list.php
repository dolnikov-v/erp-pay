<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 */

use app\widgets\LinkPager;

?>

<?= \app\widgets\Panel::widget([
    'content' => $this->render('_product-list-content', [
        'dataProvider' => $dataProvider,
        'marketplace' => $marketplace,
    ]),
    'fullScreenBody' => true,
    'footer' => (Yii::$app->user->can('marketplace.view.editproduct') ? \app\widgets\ButtonLink::widget([
        'label' => Yii::t('common', 'Добавить товар'),
        'url' => \yii\helpers\Url::toRoute(['view/edit-product', 'marketplaceId' => $marketplace->getId()]),
        'style' => \app\widgets\ButtonLink::STYLE_SUCCESS,
        'size' => \app\widgets\ButtonLink::SIZE_SMALL
    ]) : "") . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ])
]) ?>
