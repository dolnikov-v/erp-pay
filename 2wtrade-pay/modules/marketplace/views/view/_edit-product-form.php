<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\marketplace\abstracts\product\ProductInterface $model
 * @var \app\modules\marketplace\abstracts\MarketPlaceInterface $marketplace
 * @var \app\components\widgets\ActiveForm $form
 */


?>
<div class="marketplace-edit-product-form">
    <?= $marketplace->productManager->getProductForm($model, $form) ?>
</div>