<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>
<?= \app\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => Yii::t('common', 'Название'),
            'content' => function ($model) {
                return \yii\bootstrap\Html::a($model->getName(), \yii\helpers\Url::toRoute([
                    'view/index',
                    'marketplaceId' => $model->getId()
                ]));
            }
        ],
        'actions' => [
            'class' => \app\components\grid\ActionColumn::class,
            'items' => [
                [
                    'label' => Yii::t('common', 'Активировать'),
                    'can' => function ($model) {
                        /** @var \app\modules\marketplace\abstracts\MarketPlaceInterface $model */
                        return Yii::$app->user->can('marketplace.index.switchavailability') && !$model->configuration->enabled;
                    },
                    'url' => function ($model) {
                        return \yii\helpers\Url::toRoute(['index/switch-availability', 'marketplaceId' => $model->getId()]);
                    }
                ],
                [
                    'label' => Yii::t('common', 'Деактивировать'),
                    'can' => function ($model) {
                        /** @var \app\modules\marketplace\abstracts\MarketPlaceInterface $model */
                        return Yii::$app->user->can('marketplace.index.switchavailability') && $model->configuration->enabled;
                    },
                    'url' => function ($model) {
                        return \yii\helpers\Url::toRoute(['index/switch-availability', 'marketplaceId' => $model->getId()]);
                    }
                ]
            ]
        ]
    ]
]); ?>