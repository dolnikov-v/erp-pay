<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$this->title = Yii::t('common', 'Список маркетплейсов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Маркетплейсы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?=\app\widgets\Panel::widget([
    'title' => Yii::t('common', 'Список маркетплейсов'),
    'withBody' => false,
    'content' => $this->render('_index-content', ['dataProvider' => $dataProvider])
]) ?>
