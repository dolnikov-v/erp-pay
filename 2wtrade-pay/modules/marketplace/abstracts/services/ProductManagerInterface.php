<?php

namespace app\modules\marketplace\abstracts\services;


use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use yii\widgets\ActiveForm;

/**
 * Interface ProductManagerInterface
 * @package app\modules\marketplace\abstracts\services
 */
interface ProductManagerInterface
{
    /**
     * @param array $data
     * @return ProductInterface
     * @throws \Exception
     */
    public function addProduct(array $data): ProductInterface;

    /**
     * @param string $uniqueId
     * @param array $data
     * @return ProductInterface
     * @throws \Exception
     */
    public function updateProduct(string $uniqueId, array $data): ProductInterface;

    /**
     * @param string $uniqueId Уникальный идентификатор связки product_id - foreignId
     * @return bool
     */
    public function removeProduct(string $uniqueId): bool;

    /**
     * @param string $foreignId
     * @return ProductInterface|null
     */
    public function getProductByForeignId(string $foreignId): ?ProductInterface;

    /**
     * Получение продукта по его ид во внутренней системе
     * @param string $uniqueId
     * @return ProductInterface|null
     */
    public function getProductByUniqueId(string $uniqueId): ?ProductInterface;

    /**
     * Получение списка продуктов
     * @param array $condition
     * @return array
     */
    public function getProducts(array $condition = []): array;

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategoryList(): array;

    /**
     * @param ProductInterface|null $model
     * @param ActiveForm|null $form
     * @return string
     */
    public function getProductForm(ProductInterface $model = null, ActiveForm $form = null): string;
}