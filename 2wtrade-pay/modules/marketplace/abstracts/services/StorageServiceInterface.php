<?php

namespace app\modules\marketplace\abstracts\services;
use app\modules\marketplace\abstracts\product\ProductInterface;

/**
 * Interface StorageServiceInterface
 * @package app\modules\marketplace\abstracts
 */
interface StorageServiceInterface
{
    const DATA_TYPE_PARAMS = 'params';
    const DATA_TYPE_PRODUCT_CATEGORY = 'product_category';
    const DATA_TYPE_PRODUCT = 'product';
    const DATA_TYPE_DELIVERY_SERVICE = 'delivery_service';

    /**
     * @param string $key
     * @param string $data
     * @param string $dataType
     * @return bool
     */
    public function save(string $key, string $data, string $dataType = self::DATA_TYPE_PARAMS): bool;

    /**
     * @param string $key
     * @param string $dataType
     * @return string
     */
    public function get(string $key, string $dataType = self::DATA_TYPE_PARAMS): ?string;

    /**
     * @param string $key
     * @param string $dataType
     * @return bool
     */
    public function remove(string $key, string $dataType = self::DATA_TYPE_PARAMS): bool;

    /**
     * @param string $id
     * @return static
     */
    public function setMarketPlaceId(string $id): StorageServiceInterface;
}