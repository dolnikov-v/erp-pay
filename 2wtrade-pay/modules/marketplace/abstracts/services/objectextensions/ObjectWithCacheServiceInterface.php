<?php

namespace app\modules\marketplace\abstracts\services\objectextensions;

use app\modules\marketplace\abstracts\services\CacheServiceInterface;

/**
 * Интерфейс для идентификации наличия cacheService в объекте
 *
 * Interface ObjectWithCacheServiceInterface
 * @package app\modules\marketplace\abstracts\services\objectextensions
 *
 * @property CacheServiceInterface $cacheService
 */
interface ObjectWithCacheServiceInterface
{
    /**
     * @return CacheServiceInterface|null
     */
    public function getCacheService(): ?CacheServiceInterface;

    /**
     * @param CacheServiceInterface $cacheService
     * @return static
     */
    public function setCacheService(CacheServiceInterface $cacheService): ObjectWithCacheServiceInterface;
}