<?php

namespace app\modules\marketplace\abstracts\services\objectextensions;

use app\modules\marketplace\abstracts\services\StorageServiceInterface;

/**
 * Interface ObjectWithStorageServiceInterface
 * @package app\modules\marketplace\abstracts\services\objectextensions
 *
 * @property StorageServiceInterface $storageService
 */
interface ObjectWithStorageServiceInterface
{
    /**
     * @return StorageServiceInterface|null
     */
    public function getStorageService(): ?StorageServiceInterface;

    /**
     * @param StorageServiceInterface $storage
     * @return static
     */
    public function setStorageService(StorageServiceInterface $storage): ObjectWithStorageServiceInterface;
}