<?php

namespace app\modules\marketplace\abstracts\services;

/**
 * Interface CacheServiceInterface
 * @package app\modules\marketplace\abstracts
 */
interface CacheServiceInterface
{
    /**
     * Сохренение данных в кэш
     * @param string $key
     * @param string $data
     * @param int $ttl
     * @return bool
     */
    public function save(string $key, string $data, int $ttl = null): bool;

    /**
     * Взятие данных из кэша
     * @param string $key
     * @return mixed
     */
    public function get(string $key): ?string;

    /**
     * Удаление данных из кэша
     * @param string $key
     * @return mixed
     */
    public function remove(string $key): bool;

    /**
     * @param string $id
     * @return CacheServiceInterface
     */
    public function setMarketPlaceId(string $id): CacheServiceInterface;
}