<?php

namespace app\modules\marketplace\abstracts\product;

/**
 * Interface ProductCategoryInterface
 * @package app\modules\marketplace\models
 *
 * @property int $id
 * @property string $name
 * @property ProductCategoryInterface[] $children
 */
interface ProductCategoryInterface
{
    /**
     * Наименование
     * @return string
     */
    public function getName(): string;

    /**
     * Идентификатор
     * @return int
     */
    public function getId(): int;

    /**
     * Вложенные категории
     * @return static[]
     */
    public function getChildren(): array;

    /**
     * Добавление дочерней категории
     *
     * @param ProductCategoryInterface $child
     * @return ProductCategoryInterface
     */
    public function addChildren(ProductCategoryInterface $child): ProductCategoryInterface;

    /**
     * Установление имени категории
     * @param string $name
     * @return ProductCategoryInterface
     */
    public function setName(string $name): ProductCategoryInterface;

    /**
     * Установление идентификатора категории
     * @param string $id
     * @return ProductCategoryInterface
     */
    public function setId(string $id): ProductCategoryInterface;
}