<?php

namespace app\modules\marketplace\abstracts\product;

/**
 * Interface ProductInterface
 * @package app\modules\marketplace\abstracts
 *
 * @property string $uniqueId
 * @property int $id
 * @property string $foreignId
 * @property string $name
 * @property ProductCategoryInterface[] $categories
 * @property string $description
 * @property string $imageUrl
 * @property float $price
 * @property string $currencyIsoCode
 * @property int $availableQuantity
 * @property int $weight
 * @property int $height
 * @property int $width
 * @property int $length
 * @property int $quantityMultiplier
 */
interface ProductInterface
{
    /**
     * @return string
     */
    public function getUniqueId(): ?string;

    /**
     * Идентификатор продукта внутренней системы
     * @return int
     */
    public function getId(): int;

    /**
     * Идентификатор продукта в системе маркета
     * @return string
     */
    public function getForeignId(): ?string;

    /**
     * Наименование продукта
     * @return string
     */
    public function getName(): string;

    /**
     * Категории, к которым относится продукт
     * @return ProductCategoryInterface[]
     */
    public function getCategories(): array;

    /**
     * Описание продукта
     * @return string
     */
    public function getDescription(): string;

    /**
     * Ссылка на изображение с продуктом
     * @return string
     */
    public function getImageUrl(): string;

    /**
     * Стоимость продукта
     * @return float
     */
    public function getPrice(): float;

    /**
     * Валюта, в которой продается продукт
     * @return string
     */
    public function getCurrencyIsoCode(): string;

    /**
     * Доступное количество товара на складах
     * @return int
     */
    public function getAvailableQuantity(): int;

    /**
     * Вес продукта в граммах
     * @return int
     */
    public function getWeight(): ?int;

    /**
     * Высота коробки с товаром, мм
     * @return int
     */
    public function getHeight(): ?int;

    /**
     * Ширина коробки с товаром, мм
     * @return int
     */
    public function getWidth(): ?int;

    /**
     * Длина коробки с товаром, мм
     * @return int
     */
    public function getLength(): ?int;

    /**
     * Множитель количества продуктов
     * @return int|null
     */
    public function getQuantityMultiplier(): ?int;

    /**
     * Опциональные аттрибуты продукта, ассоциативный массив
     * @return array
     */
    public function getOptionalAttributes(): array;
}