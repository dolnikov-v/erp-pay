<?php

namespace app\modules\marketplace\abstracts\configuration;
use yii\widgets\ActiveForm;

/**
 * Interface MarketPlaceConfigurationInterface
 * @package app\modules\marketplace\abstracts\configuration
 *
 * @property bool $enabled
 * @property int $defaultDeliveryId
 * @property int $defaultCountryId
 * @property string $sourceId
 * @property bool $deliversByStandardService
 */
interface MarketPlaceConfigurationInterface
{
    /**
     * @param bool $status
     * @return static
     */
    public function setEnabled(bool $status): MarketPlaceConfigurationInterface;

    /**
     * @return bool
     */
    public function getEnabled(): bool;

    /**
     * Идентификатор службы доставки по умолчанию
     * @return int|null
     */
    public function getDefaultDeliveryId(): ?int;

    /**
     * Идентификатор страны по умолчанию
     * @return int|null
     */
    public function getDefaultCountryId(): ?int;

    /**
     * Название источника
     * @return null|int
     */
    public function getSourceId(): ?int;

    /**
     * @return bool|null
     */
    public function getDeliversByStandardService(): ?bool;

    /**
     * @param array $attributes
     * @return bool
     */
    public function saveToStorage(array $attributes = []): bool;

    /**
     * @param array $attributes
     * @return bool
     */
    public function loadFromStorage(array $attributes = []): bool;

    /**
     * @param ActiveForm|null $form
     * @return string
     */
    public function getForm(ActiveForm $form = null): string;

    /**
     * @param array $attributes
     * @return bool
     */
    public function loadData(array $attributes): bool;
}