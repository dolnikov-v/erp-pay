<?php

namespace app\modules\marketplace\abstracts;

use app\modules\marketplace\abstracts\orderinformation\OrderInterface;

/**
 * Interface DataTransmitterInterface
 * @package app\modules\marketplace\abstracts
 */
interface DataTransmitterInterface
{
    /**
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return integer ID of new Order
     */
    public function createOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int;

    /**
     * Проверка на существование заказа с указанным номером и идентификатором источника
     * @param string $foreignId
     * @param int|null $sourceId
     * @return bool
     */
    public function checkOrderExists(string $foreignId, ?int $sourceId): bool;

    /**
     * Обновление информации о заказе
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return int
     */
    public function updateOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int;

    /**
     * Создание или обновление информации о заказе
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return int
     */
    public function createOrUpdateOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int;
}