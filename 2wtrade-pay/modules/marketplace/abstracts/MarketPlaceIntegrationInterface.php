<?php

namespace app\modules\marketplace\abstracts;

use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;

/**
 * Interface MarketPlaceIntegrationInterface
 * @package app\modules\marketplace\abstracts
 */
interface MarketPlaceIntegrationInterface
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategories(): array;

    /**
     * Обновление информации о продуктах
     * @param ProductInterface[] $products
     * @return void
     */
    public function uploadProductsInformation(array $products);

    /**
     * @return ProductInterface[]
     */
    public function getProductsInformation(): array;

    /**
     * Добавление продукта
     * @param ProductInterface $product
     * @return bool
     */
    public function addProduct(ProductInterface $product): bool;

    /**
     * Удаление продукта
     * @param string $foreignId
     * @return bool
     */
    public function removeProduct(string $foreignId): bool;

    /**
     * Обновление информации о продукте
     * @param ProductInterface $product
     * @return bool
     */
    public function updateProduct(ProductInterface $product): bool;

    /**
     * Изменение доступности продукта
     * @param ProductInterface $product
     * @param bool $available
     * @return mixed
     */
    public function changeProductAvailability(ProductInterface $product, bool $available): bool;

    /**
     * Получение списка новых заказов
     * @return OrderInterface[]
     */
    public function getNewOrders(): array;

    /**
     * Создание заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function createShipment(OrderInterface $order);

    /**
     * Отмена заявки на доставку в маркете
     * @param OrderInterface $order
     * @return mixed
     */
    public function cancelShipment(OrderInterface $order);

    /**
     * Обновление информации о доставке
     * @param OrderInterface $order
     * @return mixed
     */
    public function updateShipment(OrderInterface $order);

    /**
     * @param string $foreignId
     * @return OrderInterface|null
     */
    public function getOrder(string $foreignId): ?OrderInterface;
}