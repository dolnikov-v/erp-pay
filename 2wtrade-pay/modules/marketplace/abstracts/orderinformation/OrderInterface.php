<?php

namespace app\modules\marketplace\abstracts\orderinformation;

/**
 * Interface OrderInterface
 * @package app\modules\marketplace\models
 *
 * @property integer $id
 * @property string $foreignId
 * @property OrderProductInterface[] $orderProducts
 * @property string $status
 * @property string $foreignStatus
 * @property ShipmentInterface $shipment
 * @property CustomerInformationInterface $customerInformation
 * @property CustomerAddressInterface $customerAddress
 * @property float $paidAmount
 * @property float $totalAmount
 * @property float $shippingAmount
 * @property float $totalAmountWithShipping
 * @property string $comment
 * @property string $currencyIsoCode
 * @property string $originalData
 */
interface OrderInterface
{
    const STATUS_PAID = 'paid';
    const STATUS_REQUIRE_PAYMENT = 'require_payment';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_CONFIRMED = 'confirmed';

    /**
     * Идентификатор заказа в системе
     * @return int
     */
    public function getId(): int;

    /**
     * Идентификатор заказа во внешней системе (маркете)
     * @return string
     */
    public function getForeignId(): string;

    /**
     * Заказанные продукты
     * @return OrderProductInterface[]
     */
    public function getOrderProducts(): array;

    /**
     * Стутус заказа
     * @return string
     */
    public function getStatus(): string;

    /**
     * Статус заказа в маркете
     * @return null|string
     */
    public function getForeignStatus(): ?string;

    /**
     * @return ShipmentInterface
     */
    public function getShipment(): ShipmentInterface;

    /**
     * @return CustomerInformationInterface
     */
    public function getCustomerInformation(): CustomerInformationInterface;

    /**
     * @return CustomerAddressInterface
     */
    public function getCustomerAddress(): CustomerAddressInterface;

    /**
     * Количество уплаченных средств
     * @return float
     */
    public function getPaidAmount(): float;

    /**
     * Общая стоимость заказа без доставки
     * @return float
     */
    public function getTotalAmount(): float;

    /**
     * Стоимость доставки
     * @return float
     */
    public function getShippingAmount(): float;

    /**
     * Общая стоимость заказа с учетом доставки
     * @return float
     */
    public function getTotalAmountWithShipping(): float;

    /**
     * Комментарий к заказу
     * @return null|string
     */
    public function getComment(): ?string;

    /**
     * ISO код валюты заказа
     * @return null|string
     */
    public function getCurrencyIsoCode(): ?string;

    /**
     * Оригинальные данные заказа в формате json
     * @return null|string
     */
    public function getOriginalData(): ?string;
}