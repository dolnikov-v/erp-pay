<?php

namespace app\modules\marketplace\abstracts\orderinformation;

/**
 * Interface CustomerAddressInterface
 * @package app\modules\marketplace\abstracts
 *
 * @property string $fullAddress
 * @property string $additionalAddress
 * @property string $blockOrInterior
 * @property string $houseNumber
 * @property string $street
 * @property string $subDistrict
 * @property string $district
 * @property string $province
 * @property string $state
 * @property string $city
 * @property string $zipCode
 * @property string $countryIsoCode
 * @property float $longitude
 * @property float $latitude
 * @property array $optionalAttributes
 */
interface CustomerAddressInterface
{
    /**
     * Полный адрес доставки
     * @return string
     */
    public function getFullAddress(): ?string;

    /**
     * Строка с уточнением адреса
     * @return string
     */
    public function getAdditionalAddress(): ?string;

    /**
     * Номер блока или квартиры
     * @return string
     */
    public function getBlockOrInterior(): ?string;

    /**
     * Номер дома
     * @return string
     */
    public function getHouseNumber(): ?string;

    /**
     * Улица
     * @return string
     */
    public function getStreet(): ?string;

    /**
     * Квартал
     * @return string
     */
    public function getSubDistrict(): ?string;

    /**
     * Район
     * @return string
     */
    public function getDistrict(): ?string;

    /**
     * Провинция
     * @return string
     */
    public function getProvince(): ?string;

    /**
     * Штат, область
     * @return string
     */
    public function getState(): ?string;

    /**
     * Город
     * @return null|string
     */
    public function getCity(): ?string;

    /**
     * Зип код
     * @return string
     */
    public function getZipCode(): ?string;

    /**
     * Страна
     * @return null|string
     */
    public function getCountryIsoCode(): ?string;

    /**
     * @return float|null
     */
    public function getLongitude(): ?float;

    /**
     * @return float|null
     */
    public function getLatitude(): ?float;

    /**
     * Дополнительные поля
     * @return string[]
     */
    public function getOptionalAttributes(): array;
}