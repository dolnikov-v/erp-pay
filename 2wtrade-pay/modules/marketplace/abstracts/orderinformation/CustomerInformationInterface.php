<?php

namespace app\modules\marketplace\abstracts\orderinformation;

/**
 * Interface CustomerInformationInterface
 * @package app\modules\marketplace\abstracts
 *
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property array $optionalAttributes
 */
interface CustomerInformationInterface
{
    /**
     * @return null|string
     */
    public function getFirstName(): ?string;

    /**
     * @return null|string
     */
    public function getMiddleName(): ?string;

    /**
     * @return null|string
     */
    public function getLastName(): ?string;

    /**
     * @return null|string
     */
    public function getPhone(): ?string;

    /**
     * @return null|string
     */
    public function getMobile(): ?string;

    /**
     * @return null|string
     */
    public function getEmail(): ?string;

    /**
     * @return string[]
     */
    public function getOptionalAttributes(): array;
}