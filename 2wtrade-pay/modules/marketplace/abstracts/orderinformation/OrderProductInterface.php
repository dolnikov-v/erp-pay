<?php

namespace app\modules\marketplace\abstracts\orderinformation;

use app\modules\marketplace\abstracts\product\ProductInterface;

/**
 * Interface OrderProduct
 * @package app\modules\marketplace\models
 *
 * @property ProductInterface $product
 * @property int $quantity
 * @property float $totalPrice
 * @property float $price
 */
interface OrderProductInterface
{
    /**
     * Описание продукта
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface;

    /**
     * Количество продуктов
     * @return int
     */
    public function getQuantity(): int;

    /**
     * Общая стоимость продуктов
     * @return float
     */
    public function getTotalPrice(): float;

    /**
     * Стоимость одного продукта
     * @return float
     */
    public function getPrice(): float;
}