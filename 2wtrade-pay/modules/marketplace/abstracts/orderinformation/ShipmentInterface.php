<?php

namespace app\modules\marketplace\abstracts\orderinformation;

/**
 * Interface ShipmentInterface
 * @package app\modules\marketplace\models
 *
 * @property string $trackingNumber
 * @property string $deliveryName
 * @property int $deliveryId
 * @property string $trackingUrl
 * @property string $status
 * @property string $foreignStatus
 * @property integer $deliveryTimeFrom
 * @property integer $deliveryTimeTo
 * @property integer $sentAt
 * @property integer $deliveredAt
 * @property string $unshippingReason
 */
interface ShipmentInterface
{
    const STATUS_NOT_EXISTS = 'not_exists';
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_NOT_DELIVERED = 'not_delivered';
    const STATUS_CANCELLED = 'cancelled';

    /**
     * Трек-номер отправления в службе доставки
     * @return null|string
     */
    public function getTrackNumber(): ?string;

    /**
     * Название службы доставки, которая будет доставлять заказ
     * @return null|string
     */
    public function getDeliveryName(): ?string;

    /**
     * Идентификатор службы доставки в нашей системе
     * @return int|null
     */
    public function getDeliveryId(): ?int;

    /**
     * Ссылка на отслеживание доставки заказа
     * @return null|string
     */
    public function getTrackingUrl(): ?string;

    /**
     * Статус доставки во внутренней системе
     * @return string
     */
    public function getStatus(): string;

    /**
     * Статус доставки в службе доставки
     * @return string
     */
    public function getForeignStatus(): ?string;

    /**
     * Дата время доставки [с] timestamp
     * @return int|null
     */
    public function getDeliveryTimeFrom(): ?int;

    /**
     * Дата и время доставки [до] timestamp
     * @return int|null
     */
    public function getDeliveryTimeTo(): ?int;

    /**
     * Дата отправки timestamp
     * @return int|null
     */
    public function getSentAt(): ?int;

    /**
     * Дата доставки timestamp
     * @return int|null
     */
    public function getDeliveredAt(): ?int;

    /**
     * Причина недоставки timestamp
     * @return null|string
     */
    public function getUnshippingReason(): ?string;
}