<?php

namespace app\modules\marketplace\abstracts\extensions;

/**
 * Интерфейс для выполнения действий на основе интервала времени
 * Interface MarketPlaceWithGetOrdersByTimeIntervalInterface
 * @package app\modules\marketplace\abstracts\extensions
 */
interface MarketPlaceWithGetOrdersByTimeIntervalInterface
{
    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return int[]
     */
    public function createOrders(\DateTime $from, \DateTime $to): array;

    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string;
}
