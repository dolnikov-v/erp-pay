<?php

namespace app\modules\marketplace\abstracts\extensions;


interface MarketPlaceCustomViewInterface
{
    public function redirectToView();
}