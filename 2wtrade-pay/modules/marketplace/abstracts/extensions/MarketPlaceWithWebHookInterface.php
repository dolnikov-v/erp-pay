<?php

namespace app\modules\marketplace\abstracts\extensions;

/**
 * Интерфейс для выполнения действий на основе вебхуков
 * Interface MarketPlaceWithWebHookInterface
 * @package app\modules\marketplace\abstracts\extensions
 */
interface MarketPlaceWithWebHookInterface
{
    /**
     * Создание заказа
     * @param array $request
     * @return array
     */
    public function handleWebHookRequest(array $request): array;

    /**
     * Уникальный АПИ ключ
     * @return string
     */
    public function getApiKey(): string;
}