<?php

namespace app\modules\marketplace\abstracts;

use app\modules\marketplace\abstracts\configuration\MarketPlaceConfigurationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\abstracts\services\ProductManagerInterface;

/**
 * Interface MarketPlaceInterface
 * @package app\modules\marketplace\abstracts
 *
 * @property string $id
 * @property string $name
 *
 * @property MarketPlaceConfigurationInterface $configuration
 * @property ProductManagerInterface $productManager
 */
interface MarketPlaceInterface
{
    /**
     * @param string $foreignId
     * @return OrderInterface|null
     */
    public function getOrderByForeignId(string $foreignId): ?OrderInterface;

    /**
     * Уникальный идентификатор маркета в системе
     * @return string
     */
    public function getId(): string;

    /**
     * Получение названия маркета
     * @return string
     */
    public function getName(): string;

    /**
     * Настройки маркета
     * @return MarketPlaceConfigurationInterface
     */
    public function getConfiguration(): ?MarketPlaceConfigurationInterface;

    /**
     * @return ProductManagerInterface
     */
    public function getProductManager(): ProductManagerInterface;
}