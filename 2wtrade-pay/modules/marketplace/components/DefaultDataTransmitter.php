<?php

namespace app\modules\marketplace\components;


use app\components\base\Component;
use app\models\Country;
use app\models\Currency;
use app\models\Source;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\marketplace\abstracts\DataTransmitterInterface;
use app\modules\marketplace\abstracts\MarketPlaceInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;
use app\modules\marketplace\exceptions\InvalidConfigurationException;
use app\modules\marketplace\exceptions\InvalidOrderStatusException;
use app\modules\marketplace\exceptions\NotFoundException;
use app\modules\marketplace\exceptions\OrderAlreadyExistsException;
use app\modules\order\models\LeadCreateLog;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use yii\log\Logger;

/**
 * Class DefaultDataConverter
 * @package app\modules\marketplace\components
 */
class DefaultDataTransmitter extends Component implements DataTransmitterInterface
{
    /**
     * Создание или обновление информации о заказе
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return int
     * @throws InvalidConfigurationException
     * @throws NotFoundException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function createOrUpdateOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int
    {
        $lockName = "{$marketplace->getId()}_{$data->getForeignId()}";
        if (!$this->mutex->acquire($lockName, 5)) {
            throw new \Exception("Order {$data->getForeignId()} is locked for update.");
        }
        try {
            if ($this->checkOrderExists($data->foreignId, $marketplace->configuration->sourceId)) {
                $orderId = $this->updateOrderFromData($data, $marketplace);
            } else {
                $orderId = $this->createOrderFromData($data, $marketplace);
            }
        } finally {
            $this->mutex->release($lockName);
        }
        return $orderId;
    }

    /**
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return int
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function createOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int
    {
        if ($this->checkOrderExists($data->foreignId, $marketplace->configuration->sourceId)) {
            throw new OrderAlreadyExistsException(\Yii::t('common', 'Заказ с идентификатором {id} уже существует.', ['id' => $data->getForeignId()]));
        }
        if ($data->status != OrderInterface::STATUS_PAID && $data->status != OrderInterface::STATUS_CONFIRMED) {
            throw new InvalidOrderStatusException(\Yii::t('common', 'Создаваемый заказ должен быть в статусе "Оплачен" или "Подтвержден".'));
        }
        $transaction = Order::getDb()->beginTransaction();
        try {
            $order = $this->prepareOrderFromData($data, $marketplace);
            if (!$order->save()) {
                throw new \Exception($order->getFirstErrorAsString());
            }
            foreach ($this->prepareOrderProducts($order->id, $data, $marketplace) as $orderProduct) {
                if (!$orderProduct->save()) {
                    throw new \Exception($orderProduct->getFirstErrorAsString());
                }
            }
            if (!empty($order->payment_amount)) {
                $orderFinanceFact = $this->prepareOrderFinanceFactForPrepaidOrder($order);
                if (!$orderFinanceFact->save()) {
                    throw new \Exception($orderFinanceFact->getFirstErrorAsString());
                }
            }

            $this->updateDeliveryRequestForOrder($order, $data, $marketplace);
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        $this->createLeadCreateLog($order, $data);
        return $order->id;
    }

    /**
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return int
     * @throws InvalidConfigurationException
     * @throws NotFoundException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function updateOrderFromData(OrderInterface $data, MarketPlaceInterface $marketplace): int
    {
        $order = $this->findOrderByForeignIdAndSourceId($data->getForeignId(), $marketplace->configuration->sourceId);
        $transaction = Order::getDb()->beginTransaction();
        try {
            $this->fillOrderData($order, $data);
            foreach ($order->orderProducts as $orderProduct) {
                if (!$orderProduct->delete()) {
                    throw new \Exception($orderProduct->getFirstErrorAsString());
                }
            }
            foreach ($this->prepareOrderProducts($order->id, $data, $marketplace) as $orderProduct) {
                if (!$orderProduct->save()) {
                    throw new \Exception($orderProduct->getFirstErrorAsString());
                }
            }
            $this->updateDeliveryRequestForOrder($order, $data, $marketplace);
            if (!$order->save()) {
                throw new \Exception($order->getFirstErrorAsString());
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $order->id;
    }

    /**
     * @param string $foreignId
     * @param int|null $sourceId
     * @return bool
     * @throws InvalidConfigurationException
     */
    public function checkOrderExists(string $foreignId, ?int $sourceId): bool
    {
        return Order::find()->byForeignId($foreignId)->bySourceId($sourceId)->exists();
    }

    /**
     * @param Order $order
     * @param OrderInterface $data
     */
    protected function createLeadCreateLog(Order $order, OrderInterface $data)
    {
        try {
            $createLog = new LeadCreateLog([
                'order_id' => $order->id,
                'input_data' => $data->originalData,
            ]);
            if (!$createLog->save()) {
                \Yii::getLogger()->log($createLog->getErrors(), Logger::LEVEL_ERROR);
            }
        } catch (\Throwable $e) {
            \Yii::getLogger()->log($e, Logger::LEVEL_ERROR);
        }
    }

    /**
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketPlace
     * @return Order
     * @throws NotFoundException
     * @throws InvalidConfigurationException
     */
    protected function prepareOrderFromData(OrderInterface $data, MarketPlaceInterface $marketPlace): Order
    {
        if (!((!is_null($data->customerAddress->countryIsoCode) && !empty(($country = Country::find()
                    ->byCharCode($data->customerAddress->countryIsoCode)
                    ->one()))) || ($marketPlace->configuration->defaultCountryId && !empty(($country = Country::find()
                    ->where(['id' => $marketPlace->configuration->defaultCountryId])->one()))))) {
            throw new NotFoundException(\Yii::t('common', 'Страна с ISO кодом "{char_code}" не найдена.', ['char_code' => $data->customerAddress->countryIsoCode]));
        }
        if (!is_null($data->currencyIsoCode)) {
            $currency = Currency::find()->byCharCode($data->currencyIsoCode)->one();
        } else {
            $currency = $country->currency;
        }
        if (empty($currency)) {
            throw new NotFoundException(\Yii::t('common', 'Не удалось определить валюту заказа.'));
        }
        $source = $this->findSourceById($marketPlace->configuration->getSourceId());
        $order = new Order([
            'foreign_id' => (string)$data->foreignId,
            'source_id' => $source->id,
            'country_id' => $country->id,
            'status_id' => $data->status == OrderInterface::STATUS_PAID ? OrderStatus::STATUS_CC_APPROVED : OrderStatus::STATUS_SOURCE_LONG_FORM,
            'source_form' => Order::TYPE_FORM_LONG,
            'call_center_type' => $source->default_call_center_type ?: Order::TYPE_CC_ORDER,
            'pending_delivery_id' => $marketPlace->configuration->defaultDeliveryId ?? null
        ]);

        $this->fillOrderData($order, $data);
        return $order;
    }

    /**
     * Заполнение данных у заказа
     * @param Order $order
     * @param OrderInterface $data
     */
    protected function fillOrderData(Order &$order, OrderInterface $data)
    {
        if (!is_null($data->currencyIsoCode)) {
            $currency = Currency::find()->byCharCode($data->currencyIsoCode)->one();
        } else {
            $currency = $order->country->currency;
        }

        $orderData = [
            'customer_full_name' => implode(" ", [
                $data->customerInformation->firstName,
                $data->customerInformation->middleName,
                $data->customerInformation->lastName
            ]),
            'customer_email' => $data->customerInformation->email,
            'customer_phone' => $data->customerInformation->phone,
            'customer_mobile' => $data->customerInformation->mobile,
            'customer_province' => $data->customerAddress->province,
            'customer_district' => $data->customerAddress->district,
            'customer_subdistrict' => $data->customerAddress->subDistrict,
            'customer_city' => $data->customerAddress->city,
            'customer_street' => $data->customerAddress->street,
            'customer_house_number' => $data->customerAddress->houseNumber,
            'customer_address' => $data->customerAddress->fullAddress,
            'customer_address_additional' => $data->customerAddress->additionalAddress,
            'customer_zip' => $data->customerAddress->zipCode,
            'customer_address_add' => json_encode(array_merge(['customer_state' => $data->customerAddress->state], $data->customerAddress->optionalAttributes, $data->customerInformation->optionalAttributes), JSON_UNESCAPED_UNICODE),
            'longitude' => $data->customerAddress->longitude,
            'latitude' => $data->customerAddress->latitude,
            'comment' => $data->comment,
            'delivery_time_from' => $data->shipment->deliveryTimeFrom,
            'delivery_time_to' => $data->shipment->deliveryTimeTo,
        ];

        $order->load([
            'payment_type' => $data->status == OrderInterface::STATUS_PAID ? Order::ORDER_PAYMENT_TYPE_PREPAID : Order::ORDER_PAYMENT_TYPE_COD,
            'payment_amount' => $data->paidAmount,
            'price' => $data->totalAmount,
            'price_total' => $data->totalAmount,
            'price_currency' => $currency->id,
            'delivery' => $data->shippingAmount,
        ], '');

        if ($order->isNewRecord || $data->shipment->status == ShipmentInterface::STATUS_PENDING) {
            $order->load($orderData, '');
        }
    }

    /**
     * @param int $orderId
     * @param OrderInterface $orderData
     * @param MarketPlaceInterface $marketPlace
     * @return OrderProduct[]
     * @throws NotFoundException
     */
    protected function prepareOrderProducts(int $orderId, OrderInterface $orderData, MarketPlaceInterface $marketPlace): array
    {
        $products = [];
        foreach ($orderData->getOrderProducts() as $orderProductData) {
            $product = $orderProductData->product;
            if (empty($product->id)) {
                $product = $marketPlace->productManager->getProductByForeignId($orderProductData->product->foreignId);
            }
            if (empty($product->id)) {
                throw new NotFoundException(\Yii::t('common', 'Не удалось определить идентификатор продукта в системе для указанного внешнего идентификатора: {foreign_id}.', ['foreign_id' => $orderProductData->product->getForeignId()]));
            }
            $products[] = new OrderProduct([
                'order_id' => $orderId,
                'quantity' => $orderProductData->quantity,
                'price' => $orderProductData->price,
                'product_id' => $product->id
            ]);
            // Дополнительные бесплатные акционные продукты
            $salesProductQuantity = !empty($product->quantityMultiplier) ? ($product->quantityMultiplier * $orderProductData->quantity) - $orderProductData->quantity : 0;
            if (!empty($salesProductQuantity)) {
                $products[] = new OrderProduct([
                    'order_id' => $orderId,
                    'quantity' => $salesProductQuantity,
                    'price' => 0,
                    'product_id' => $product->id
                ]);
            }
        }
        return $products;
    }

    /**
     * @param Order $order
     * @return OrderFinanceFact
     */
    protected function prepareOrderFinanceFactForPrepaidOrder(Order $order): OrderFinanceFact
    {
        return new OrderFinanceFact([
            'order_id' => $order->id,
            'price_cod' => $order->payment_amount,
            'price_cod_currency_id' => $order->price_currency,
            'price_cod_currency_rate' => $order->priceCurrency->currencyRate->rate
        ]);
    }

    /**
     * Обновление заявки на доставку, если доставляетя службой маркета
     * @param Order $order
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketPlace
     * @return DeliveryRequest|null
     * @throws \Exception
     */
    protected function updateDeliveryRequestForOrder(Order &$order, OrderInterface $data, MarketPlaceInterface $marketPlace): ?DeliveryRequest
    {
        /** @var DeliveryRequest $deliveryRequest */
        $deliveryRequest = $this->createDeliveryRequestForOrder($order, $data, $marketPlace);

        // Если заказ доставляется службой доставки маркета, то обновляем deliveryRequest согласно полученной информации
        if ($deliveryRequest && $marketPlace->configuration->deliversByStandardService) {
            $deliveryRequest->status = DeliveryRequest::STATUS_IN_PROGRESS;
            switch ($data->shipment->status) {
                case ShipmentInterface::STATUS_PENDING:
                case ShipmentInterface::STATUS_IN_PROGRESS:
                    $order->status_id = OrderStatus::STATUS_DELIVERY_ACCEPTED;
                    break;
                case ShipmentInterface::STATUS_DELIVERED:
                    $order->status_id = OrderStatus::STATUS_DELIVERY_BUYOUT;
                    break;
                case ShipmentInterface::STATUS_CANCELLED:
                case ShipmentInterface::STATUS_NOT_DELIVERED:
                    $order->status_id = OrderStatus::STATUS_DELIVERY_DENIAL;
                    break;
                default:
                    return null;
            }
            $deliveryRequest->foreign_info = $data->shipment->foreignStatus;
            if ($data->shipment->trackingNumber) {
                $deliveryRequest->tracking = $data->shipment->trackingNumber;
            }
            if ($data->shipment->sentAt && !$deliveryRequest->sent_at) {
                $deliveryRequest->sent_at = $data->shipment->sentAt;
            }
            if ($data->shipment->deliveredAt && !$deliveryRequest->approved_at) {
                $deliveryRequest->approved_at = $data->shipment->deliveredAt;
            }

            if (!$deliveryRequest->save()) {
                throw new \Exception($deliveryRequest->getFirstErrorAsString());
            }

            if (!$order->save()) {
                throw new \Exception($order->getFirstErrorAsString());
            }
        }
        return $deliveryRequest;
    }

    /**
     * Создание заявки на доставку
     * @param Order $order
     * @param OrderInterface $data
     * @param MarketPlaceInterface $marketplace
     * @return DeliveryRequest|null
     * @throws \Exception
     */
    protected function createDeliveryRequestForOrder(Order $order, OrderInterface $data, MarketPlaceInterface $marketplace): ?DeliveryRequest
    {
        $deliveryRequest = $order->getDeliveryRequest()->one();
        if (!$deliveryRequest) {
            $deliveryId = $data->shipment->deliveryId ?? $order->pending_delivery_id;
            // Проверяем, что определили службу доставки
            if (!empty($deliveryId) && !empty($delivery = Delivery::find()->byId($deliveryId)->one())) {
                if (!in_array($data->shipment->status, [
                        ShipmentInterface::STATUS_NOT_EXISTS,
                        ShipmentInterface::STATUS_PENDING
                    ]) || $marketplace->configuration->deliversByStandardService) {
                    $deliveryRequest = new DeliveryRequest([
                        'order_id' => $order->id,
                        'delivery_id' => $deliveryId,
                        'status' => DeliveryRequest::STATUS_IN_PROGRESS
                    ]);
                    if (!$deliveryRequest->save()) {
                        throw new \Exception($deliveryRequest->getFirstErrorAsString());
                    }
                } elseif ($delivery->canDeliveryOrder($order) && $delivery->prepareOrderToSend($order)) {
                    $deliveryRequest = $order->getDeliveryRequest()->one();
                }
            }
        }
        return $deliveryRequest;
    }

    /**
     * @param int|null $id
     * @return Source
     * @throws InvalidConfigurationException
     */
    protected function findSourceById(?int $id): Source
    {
        $source = Source::find()->where(['id' => $id])->one();
        if (!$source) {
            throw new InvalidConfigurationException(\Yii::t('common', 'К маркетплейсу не привязан источник.'));
        }
        return $source;
    }

    /**
     * @param string $foreignId
     * @param int|null $sourceId
     * @return Order
     * @throws NotFoundException
     */
    protected function findOrderByForeignIdAndSourceId(string $foreignId, ?int $sourceId): Order
    {
        $order = Order::find()->byForeignId($foreignId)->bySourceId($sourceId)->one();
        if (!$order) {
            throw new NotFoundException(\Yii::t('common', 'Заказ с идентификатором {id} не найден.', ['id' => $foreignId]));
        }
        return $order;
    }
}