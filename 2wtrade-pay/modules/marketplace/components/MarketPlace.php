<?php

namespace app\modules\marketplace\components;

use app\components\base\Module;
use app\modules\marketplace\abstracts\configuration\MarketPlaceConfigurationInterface;
use app\modules\marketplace\abstracts\DataTransmitterInterface;
use app\modules\marketplace\abstracts\MarketPlaceIntegrationInterface;
use app\modules\marketplace\abstracts\MarketPlaceInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\services\ProductManagerInterface;
use app\modules\marketplace\models\Product;
use app\modules\order\models\Order;

/**
 * Class MarketPlace
 * @package app\modules\marketplace\components
 *
 * @property string $id
 * @property string $name
 * @property MarketPlaceConfigurationInterface $configuration
 * @property MarketPlaceIntegrationInterface $apiAdapter
 * @property DataTransmitterInterface $dataConverter
 * @property ProductManagerInterface $productManager
 */
abstract class MarketPlace extends Module implements MarketPlaceInterface
{
    /**
     * Префикс, добавляемый к имени маркета
     * @var string
     */
    public $namePrefix;

    /**
     * Суффикс, добавляемый к имени маркета
     * @var string
     */
    public $nameSuffix;

    /**
     * Название маркета
     * @var string
     */
    protected $name;

    /**
     * @var null
     */
    public $productManagerClass = null;

    /**
     * @var string
     */
    public $configurationClass = null;

    /**
     * @var null|string
     */
    public $dataConverterClass = null;

    /**
     * Уникальный идентификатор маркета в системе
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Получение названия маркета
     * @return string
     * @throws \ReflectionException
     */
    public function getName(): string
    {
        if (is_null($this->name)) {
            $this->name = (is_string($this->namePrefix) ? $this->namePrefix : '') . (new \ReflectionClass($this))->getShortName() . (is_string($this->nameSuffix) ? $this->nameSuffix : "");
        }
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return $this
     */
    public function setName(?string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Настройки маркета
     * @return MarketPlaceConfigurationInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getConfiguration(): ?MarketPlaceConfigurationInterface
    {
        if (!$this->get('configuration', false) && !empty($this->configurationClass)) {
            $config = [];
            if (is_array($this->configurationClass)) {
                $config = $this->configurationClass;
            } else {
                $config['class'] = $this->configurationClass;
            }
            $config['marketplaceId'] = $this->getId();
            $this->set('configuration', $config);
        }
        return $this->get('configuration', false);
    }

    /**
     * @return MarketPlaceIntegrationInterface|object
     */
    public function getApiAdapter()
    {
        return $this->get('apiAdapter');
    }

    /**
     * @return DataTransmitterInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getDataConverter(): DataTransmitterInterface
    {
        if (!$this->get('dataConverter', false) && !empty($this->dataConverterClass)) {
            $config = [];
            if (is_array($this->dataConverterClass)) {
                $config = $this->dataConverterClass;
            } else {
                $config['class'] = $this->dataConverterClass;
            }
            $this->set('dataConverter', $config);
        }
        return $this->get('dataConverter');
    }

    /**
     * @return ProductManagerInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getProductManager(): ProductManagerInterface
    {
        if (!$this->get('productManager', false) && !empty($this->productManagerClass)) {
            $config = [];
            if (is_array($this->productManagerClass)) {
                $config = $this->productManagerClass;
            } else {
                $config['class'] = $this->productManagerClass;
            }
            $config['marketplaceId'] = $this->getId();
            $this->set('productManager', $config);
        }
        return $this->get('productManager');
    }
}