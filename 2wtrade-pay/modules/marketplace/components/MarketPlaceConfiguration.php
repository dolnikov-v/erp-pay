<?php

namespace app\modules\marketplace\components;

use app\modules\marketplace\models\MarketPlaceParam;
use app\modules\marketplace\widgets\MarketPlaceConfigurationWidget;
use yii\widgets\ActiveForm;

/**
 * Class MarketPlaceConfiguration
 * @package app\modules\marketplace\components
 */
class MarketPlaceConfiguration extends DefaultMarketPlaceConfiguration
{
    /**
     * @param array $attributes
     * @return bool
     * @throws \Exception
     */
    public function saveToStorage(array $attributes = []): bool
    {
        $attributes = $this->getAttributes(empty($attributes) ? null : $attributes, ['marketplaceId']);
        foreach ($attributes as $attribute => $value) {
            if (MarketPlaceParam::find()
                ->where(['market_place_id' => $this->marketplaceId, 'name' => $attribute])
                ->exists()) {
                $model = MarketPlaceParam::find()->where([
                    'market_place_id' => $this->marketplaceId,
                    'name' => $attribute
                ])->one();
            } else {
                $model = new MarketPlaceParam([
                    'market_place_id' => $this->marketplaceId,
                    'name' => $attribute
                ]);
            }
            $model->value = (string)$value;
            if (!$model->save()) {
                throw new \Exception($model->getFirstErrorAsString());
            }
        }
        return true;
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function loadFromStorage(array $attributes = []): bool
    {
        $storageValues = MarketPlaceParam::find()
            ->where(['market_place_id' => $this->marketplaceId])
            ->indexBy('name')
            ->all();
        $attributes = array_keys($this->getAttributes(empty($attributes) ? null : $attributes, ['marketplaceId']));
        $attributeValues = [];
        foreach ($attributes as $attribute) {
            if (isset($storageValues[$attribute])) {
                $attributeValues[$attribute] = $storageValues[$attribute]->value;
            }
        }
        $this->setAttributes($attributeValues);
        return true;
    }

    /**
     * @param ActiveForm|null $form
     * @return string
     * @throws \Exception
     */
    public function getForm(ActiveForm $form = null): string
    {
        return MarketPlaceConfigurationWidget::widget(['configuration' => $this, 'form' => $form]);
    }
}