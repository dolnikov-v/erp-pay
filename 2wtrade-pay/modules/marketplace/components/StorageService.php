<?php

namespace app\modules\marketplace\components;


use app\components\base\Component;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\abstracts\services\StorageServiceInterface;
use app\modules\marketplace\models\MarketPlaceParam;
use app\modules\marketplace\models\MarketPlaceProduct;
use app\modules\marketplace\models\Product;

/**
 * Class StorageService
 * @package app\modules\marketplace\components
 */
class StorageService extends Component implements StorageServiceInterface
{
    /**
     * @var string
     */
    protected $marketPlaceId;

    /**
     * @param string $key
     * @param string $data
     * @param string $dataType
     * @return bool
     * @throws \Exception
     */
    public function save(string $key, string $data, string $dataType = self::DATA_TYPE_PARAMS): bool
    {
        if (MarketPlaceParam::find()->where(['market_place_id' => $this->marketPlaceId, 'name' => $key])->exists()) {
            $model = MarketPlaceParam::find()
                ->where(['market_place_id' => $this->marketPlaceId, 'name' => $key])
                ->one();
        } else {
            $model = new MarketPlaceParam([
                'market_place_id' => $this->marketPlaceId,
                'name' => $key
            ]);
        }
        $model->value = $data;
        if (!$model->save()) {
            throw new \Exception($model->getFirstErrorAsString());
        }
        return true;
    }

    /**
     * @param string $key
     * @param string $dataType
     * @return string
     */
    public function get(string $key, string $dataType = self::DATA_TYPE_PARAMS): ?string
    {
        $model = MarketPlaceParam::find()->where(['market_place_id' => $this->marketPlaceId, 'name' => $key])->one();
        if ($model) {
            return $model->value;
        }
        return null;
    }

    /**
     * @param string $key
     * @param string $dataType
     * @return bool
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function remove(string $key, string $dataType = self::DATA_TYPE_PARAMS): bool
    {
        if (MarketPlaceParam::find()->where(['market_place_id' => $this->marketPlaceId, 'name' => $key])->exists()) {
            $model = MarketPlaceParam::find()
                ->where(['market_place_id' => $this->marketPlaceId, 'name' => $key])
                ->one();
            return $model->delete();
        }
        return true;
    }

    /**
     * @param string $id
     * @return StorageServiceInterface
     */
    public function setMarketPlaceId(string $id): StorageServiceInterface
    {
        $this->marketPlaceId = $id;
        return $this;
    }
}