<?php

namespace app\modules\marketplace\components\base;


use app\components\web\Controller;
use app\modules\marketplace\MarketPlaceModule;
use yii\web\NotFoundHttpException;

/**
 * Class WebController
 * @package app\modules\marketplace\components\base
 *
 * @property MarketPlaceModule $module
 */
abstract class WebController extends Controller
{
    /**
     * @param string $id
     * @return \app\modules\marketplace\abstracts\MarketPlaceInterface|null
     * @throws NotFoundHttpException
     */
    protected function findMarketPlaceById($id)
    {
        $marketPlace = $this->module->getMarketPlaceById($id);
        if (!$marketPlace) {
            throw new NotFoundHttpException(\Yii::t('common', 'Маркетплейс не найден.'));
        }
        return $marketPlace;
    }
}