<?php

namespace app\modules\marketplace\components;

use app\modules\marketplace\abstracts\services\CacheServiceInterface;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithCacheServiceInterface;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithStorageServiceInterface;
use app\modules\marketplace\abstracts\services\StorageServiceInterface;

/**
 * Class MarketPlaceWithExtendedServices
 * @package app\modules\marketplace\components
 *
 * @property StorageServiceInterface $storageService
 * @property CacheServiceInterface $cacheService
 */
abstract class MarketPlaceWithExtendedServices extends MarketPlace implements ObjectWithStorageServiceInterface, ObjectWithCacheServiceInterface
{
    /**
     * @return StorageServiceInterface|null
     */
    public function getStorageService(): ?StorageServiceInterface
    {
        return $this->get('storageService', false);
    }

    /**
     * @param StorageServiceInterface $service
     * @return ObjectWithStorageServiceInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function setStorageService(StorageServiceInterface $service): ObjectWithStorageServiceInterface
    {
        $service->setMarketPlaceId($this->getId());
        $this->set('storageService', $service);
        foreach (array_keys($this->components) as $componentName) {
            $component = $this->get($componentName);
            if (!empty($this->getStorageService()) && ($component instanceof ObjectWithStorageServiceInterface) && empty($component->getStorageService())) {
                $component->setStorageService($this->getStorageService());
            }
        }
        return $this;
    }

    /**
     * @param CacheServiceInterface $cacheService
     * @return ObjectWithCacheServiceInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function setCacheService(CacheServiceInterface $cacheService): ObjectWithCacheServiceInterface
    {
        $cacheService->setMarketPlaceId($this->getId());
        $this->set('cacheService', $cacheService);
        foreach (array_keys($this->components) as $componentName) {
            $component = $this->get($componentName);
            if ($this->getCacheService() && ($component instanceof ObjectWithCacheServiceInterface) && !$component->getCacheService()) {
                $component->setCacheService($this->getCacheService());
            }
        }
        return $this;
    }

    /**
     * @return CacheServiceInterface|null
     */
    public function getCacheService(): ?CacheServiceInterface
    {
        return $this->get('cacheService', false);
    }
}