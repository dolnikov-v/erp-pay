<?php

namespace app\modules\marketplace\components;


use app\components\base\Component;
use app\modules\marketplace\abstracts\services\CacheServiceInterface;

/**
 * Class CacheService
 * @package app\modules\marketplace\components
 */
class CacheService extends Component implements CacheServiceInterface
{
    /**
     * @var null
     */
    protected $marketPlaceId = null;

    /**
     * Сохренение данных в кэш
     * @param string $key
     * @param string $data
     * @param int $ttl
     * @return bool
     */
    public function save(string $key, string $data, int $ttl = null): bool
    {
        return \Yii::$app->cache->set($this->getCacheKey($key), $data, $ttl);
    }

    /**
     * Взятие данных из кэша
     * @param string $key
     * @return mixed
     */
    public function get(string $key): ?string
    {
        return \Yii::$app->cache->get($this->getCacheKey($key));
    }

    /**
     * Удаление данных из кэша
     * @param string $key
     * @return mixed
     */
    public function remove(string $key): bool
    {
        return \Yii::$app->cache->delete($this->getCacheKey($key));
    }

    /**
     * @param string $id
     * @return CacheServiceInterface
     */
    public function setMarketPlaceId(string $id): CacheServiceInterface
    {
        $this->marketPlaceId = $id;
        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getCacheKey(string $key)
    {
        return implode(':', [$this->marketPlaceId, $key]);
    }
}