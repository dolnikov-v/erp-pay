<?php

namespace app\modules\marketplace\components;

use app\components\base\Component;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithCacheServiceInterface;
use app\modules\marketplace\abstracts\MarketPlaceIntegrationInterface;
use app\modules\marketplace\abstracts\services\CacheServiceInterface;

/**
 * Class MarketPlaceIntegration
 * @package app\modules\marketplace\modules\mercedolibre\components
 *
 * @property CacheServiceInterface $cacheService
 */
abstract class MarketPlaceIntegration extends Component implements MarketPlaceIntegrationInterface, ObjectWithCacheServiceInterface
{
    /**
     * @var null|CacheServiceInterface
     */
    private $cacheService = null;

    /**
     * @return CacheServiceInterface|null
     */
    public function getCacheService(): ?CacheServiceInterface
    {
        return $this->cacheService;
    }

    /**
     * @param CacheServiceInterface $cacheService
     * @return ObjectWithCacheServiceInterface
     */
    public function setCacheService(CacheServiceInterface $cacheService): ObjectWithCacheServiceInterface
    {
        $this->cacheService = $cacheService;
        return $this;
    }
}