<?php

namespace app\modules\marketplace\components;


use app\components\base\Model;
use app\modules\marketplace\abstracts\configuration\MarketPlaceConfigurationInterface;
use yii\widgets\ActiveForm;

/**
 * Class DefaultMarketPlaceConfiguration
 * @package app\modules\marketplace\components
 */
class DefaultMarketPlaceConfiguration extends Model implements MarketPlaceConfigurationInterface
{
    public $enabled = true;
    public $defaultCountryId;
    public $defaultDeliveryId;
    public $marketplaceId;
    public $sourceId;
    public $deliversByStandardService = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->loadFromStorage();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge([
            [['enabled', 'deliversByStandardService'], 'boolean'],
            [['defaultDeliveryId', 'sourceId', 'defaultCountryId'], 'integer'],
            [['marketplaceId'], 'string']
        ], $this->rulesExtended());
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge([
            'defaultDeliveryId' => \Yii::t('common', 'Служба доставки по умолчанию'),
            'defaultCountryId' => \Yii::t('common', 'Страна по умолчанию'),
            'enabled' => \Yii::t('common', 'Активность'),
            'sourceId' => \Yii::t('common', 'Привязан к источнику'),
            'deliversByStandardService' => \Yii::t('common', 'Доставка стандартной службой доставки маркета')
        ], $this->attributeLabelExtended());
    }

    /**
     * Дополнение дефолтных рулсов новыми
     * @return array
     */
    protected function rulesExtended(): array
    {
        return [];
    }

    /**
     * Дополнение дефолтных лейблов для атрибутов новыми
     * @return array
     */
    protected function attributeLabelExtended(): array
    {
        return [];
    }

    /**
     * @param bool $status
     * @return static
     */
    public function setEnabled(bool $status): MarketPlaceConfigurationInterface
    {
        $this->enabled = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Идентификатор службы доставки по умолчанию
     * @return int|null
     */
    public function getDefaultDeliveryId(): ?int
    {
        return $this->defaultDeliveryId;
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function saveToStorage(array $attributes = []): bool
    {
        return false;
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function loadFromStorage(array $attributes = []): bool
    {
        return true;
    }

    /**
     * Название источника
     * @return null|int
     */
    public function getSourceId(): ?int
    {
        return $this->sourceId;
    }

    /**
     * @return bool|null
     */
    public function getDeliversByStandardService(): ?bool
    {
        return $this->deliversByStandardService;
    }

    /**
     * @param ActiveForm|null $form
     * @return string
     * @throws \Exception
     */
    public function getForm(ActiveForm $form = null): string
    {
        return '';
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws \ReflectionException
     */
    public function loadData(array $attributes): bool
    {
        return $this->load($attributes[(new \ReflectionClass($this))->getShortName()] ?? $attributes, '');
    }

    /**
     * Идентификатор страны по умолчанию
     * @return int|null
     */
    public function getDefaultCountryId(): ?int
    {
        return $this->defaultCountryId;
    }
}