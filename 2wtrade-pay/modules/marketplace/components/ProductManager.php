<?php

namespace app\modules\marketplace\components;


use app\components\base\Component;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;
use app\modules\marketplace\abstracts\services\ProductManagerInterface;
use app\modules\marketplace\models\MarketPlaceProduct;
use app\modules\marketplace\models\Product;
use app\modules\marketplace\widgets\ProductFormWidget;
use MongoDB\BSON\ObjectId;
use yii\base\Widget;
use yii\widgets\ActiveForm;

/**
 * Class ProductManager
 * @package app\modules\marketplace\components
 */
class ProductManager extends Component implements ProductManagerInterface
{
    /**
     * @var string Should be instance of Product
     */
    public $productClass = Product::class;

    /**
     * @var string
     */
    public $productFormWidgetClass = ProductFormWidget::class;

    public $marketplaceId;

    /**
     * @throws \Exception
     */
    public function init()
    {
        parent::init();
        $this->checkInstanceOfProductClass();
        $this->checkInstanceOfProductFormWidgetClass();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function checkInstanceOfProductClass()
    {
        if (!is_a($this->productClass, Product::class, true)) {
            throw new \Exception("ProductClass should be instance of " . Product::class);
        }
        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function checkInstanceOfProductFormWidgetClass()
    {
        if (!is_a($this->productFormWidgetClass, Widget::class, true)) {
            throw new \Exception("ProductFormWidgetClass should be instance of " . Widget::class);
        }
        return true;
    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function setProductClass($value)
    {
        $this->productClass = $value;
        $this->checkInstanceOfProductClass();
    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function setProductFormWidgetClass($value)
    {
        $this->productFormWidgetClass = $value;
        $this->checkInstanceOfProductFormWidgetClass();
    }

    /**
     * @param array $data
     * @return ProductInterface
     * @throws \Exception
     */
    public function addProduct(array $data): ProductInterface
    {
        return $this->saveProduct($data);
    }

    /**
     * @param string $uniqueId
     * @param array $data
     * @return ProductInterface
     * @throws \Exception
     */
    public function updateProduct(string $uniqueId, array $data): ProductInterface
    {
        $marketPlaceProduct = MarketPlaceProduct::find()->where([
            '_id' => new ObjectId($uniqueId),
            'market_place_id' => $this->marketplaceId
        ])->one();
        if (!$marketPlaceProduct) {
            throw new \Exception("Product with unique ID {$uniqueId} not found.");
        }
        return $this->saveProduct($data, $marketPlaceProduct);
    }

    /**
     * @param string $uniqueId
     * @return bool
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function removeProduct(string $uniqueId): bool
    {
        $marketPlaceProduct = MarketPlaceProduct::find()->where([
            '_id' => new ObjectId($uniqueId),
            'market_place_id' => $this->marketplaceId
        ])->one();

        return $marketPlaceProduct->delete() !== false;
    }

    /**
     * @param string $foreignId
     * @return ProductInterface|null
     * @throws \Exception
     */
    public function getProductByForeignId(string $foreignId): ?ProductInterface
    {
        $marketPlaceProduct = MarketPlaceProduct::find()->where([
            'foreign_product_id' => $foreignId,
            'market_place_id' => $this->marketplaceId
        ])->one();
        return $marketPlaceProduct ? $this->prepareProduct($marketPlaceProduct) : null;
    }

    /**
     * Получение продукта по его ид во внутренней системе
     * @param string $uniqueId
     * @return ProductInterface|Product
     * @throws \Exception
     */
    public function getProductByUniqueId(string $uniqueId): ?ProductInterface
    {
        $marketPlaceProduct = MarketPlaceProduct::find()->where([
            '_id' => new ObjectId($uniqueId),
            'market_place_id' => $this->marketplaceId
        ])->one();
        return $marketPlaceProduct ? $this->prepareProduct($marketPlaceProduct) : null;
    }

    /**
     * Получение списка продуктов
     * @param array $condition
     * @return array
     * @throws \Exception
     */
    public function getProducts(array $condition = []): array
    {
        $productModels = MarketPlaceProduct::find()->where([
            'market_place_id' => $this->marketplaceId
        ])->all();
        $models = [];
        foreach ($productModels as $product) {
            $models[] = $this->prepareProduct($product);
        }
        return $models;
    }

    /**
     * @param MarketPlaceProduct $marketPlaceProduct
     * @return Product
     * @throws \Exception
     */
    protected function prepareProduct(MarketPlaceProduct $marketPlaceProduct = null): Product
    {
        /** @var Product $product */
        $product = new $this->productClass();
        if (!empty($marketPlaceProduct)) {
            $product->load(array_merge(json_decode($marketPlaceProduct->optional_attributes, true) ?? [], [
                'id' => $marketPlaceProduct->product_id,
                'foreignId' => $marketPlaceProduct->foreign_product_id,
                'uniqueId' => (string)$marketPlaceProduct->_id
            ]), '');
        }

        return $product;
    }

    /**
     * Получение списка доступных категорий
     * @return ProductCategoryInterface[]
     */
    public function getProductCategoryList(): array
    {
        // TODO: Implement getProductCategoryList() method.
    }

    /**
     * @param ProductInterface|null $model
     * @param ActiveForm|null $form
     * @return string
     * @throws \Exception
     */
    public function getProductForm(ProductInterface $model = null, ActiveForm $form = null): string
    {
        if (empty($model)) {
            $model = $this->prepareProduct();
        }
        /** @var Widget $widget */
        $widget = new $this->productFormWidgetClass([
            'model' => $model,
            'form' => $form
        ]);
        return $widget->run();
    }

    /**
     * @param array $data
     * @param MarketPlaceProduct $marketPlaceProduct
     * @return Product
     * @throws \Exception
     * @throws \ReflectionException
     */
    protected function saveProduct(array $data, MarketPlaceProduct $marketPlaceProduct = null)
    {
        if (!$marketPlaceProduct) {
            $marketPlaceProduct = new MarketPlaceProduct([
                'market_place_id' => $this->marketplaceId
            ]);
        }
        $model = $this->prepareProduct($marketPlaceProduct);
        if (!$model->load($data[(new \ReflectionClass($this->productClass))->getShortName()] ?? $data, '') || !$model->validate()) {
            throw new \Exception("{$this->productClass}: {$model->getFirstErrorAsString()}");
        }
        $this->prepareMarketPlaceProduct($model, $marketPlaceProduct);
        if (!$marketPlaceProduct->save()) {
            throw new \Exception("Error while save data to storage: {$marketPlaceProduct->getFirstErrorAsString()}");
        }
        if (!$model->uniqueId) {
            $model->uniqueId = (string)$marketPlaceProduct->_id;
        }
        return $model;
    }

    /**
     * @param Product $product
     * @param MarketPlaceProduct|null $marketPlaceProduct
     */
    protected function prepareMarketPlaceProduct(Product $product, MarketPlaceProduct &$marketPlaceProduct)
    {
        $marketPlaceProduct->foreign_product_id = $product->getForeignId();
        $marketPlaceProduct->product_id = $product->getId();
        $optionalAttributes = [];
        foreach ($product->attributes as $key => $value) {
            if (!in_array($key, ['foreignId', 'id', 'uniqueId', 'market_place_id'])) {
                $optionalAttributes[$key] = $value;
            }
        }
        $marketPlaceProduct->optional_attributes = json_encode($optionalAttributes, JSON_UNESCAPED_UNICODE);
    }
}