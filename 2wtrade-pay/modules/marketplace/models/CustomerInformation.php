<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;

/**
 * Class CustomerInformation
 * @package app\modules\marketplace\models
 */
class CustomerInformation extends Model implements CustomerInformationInterface
{
    /**
     * @var string
     */
    public $firstName, $middleName, $lastName;
    /**
     * @var string
     */
    public $phone, $mobile, $email;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['firstName', 'middleName', 'lastName', 'phone', 'mobile', 'email'], 'string']
        ];
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return null|string
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return null|string
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    public function getOptionalAttributes(): array
    {
        return [];
    }
}