<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;

/**
 * Class Shipment
 * @package app\modules\marketplace\models
 */
class Shipment extends Model implements ShipmentInterface
{
    public $trackingNumber;
    public $deliveryName;
    public $trackingUrl;
    public $status;
    public $foreignStatus;
    public $deliveryId;
    public $deliveryTimeFrom;
    public $deliveryTimeTo;
    public $sentAt;
    public $deliveredAt;
    public $unshippingReason;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['trackingUrl', 'trackingNumber', 'foreignStatus', 'status', 'unshippingReason'], 'string'],
            [['deliveryId', 'deliveryTimeFrom', 'deliveryTimeTo', 'sentAt', 'deliveredAt'], 'integer'],
        ];
    }

    /**
     * Трек-номер отправления в службе доставки
     * @return null|string
     */
    public function getTrackNumber(): ?string
    {
        return $this->trackingNumber;
    }

    /**
     * Название службы доставки, которая будет доставлять заказ
     * @return null|string
     */
    public function getDeliveryName(): ?string
    {
        return $this->deliveryName;
    }

    /**
     * Ссылка на отслеживание доставки заказа
     * @return null|string
     */
    public function getTrackingUrl(): ?string
    {
        return $this->trackingUrl;
    }

    /**
     * Статус доставки во внутренней системе
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Статус доставки в службе доставки
     * @return string
     */
    public function getForeignStatus(): ?string
    {
        return $this->foreignStatus;
    }

    /**
     * Идентификатор службы доставки в нашей системе
     * @return int|null
     */
    public function getDeliveryId(): ?int
    {
        return $this->deliveryId;
    }

    /**
     * Дата время доставки [с] timestamp
     * @return int|null
     */
    public function getDeliveryTimeFrom(): ?int
    {
        return $this->deliveryTimeFrom;
    }

    /**
     * Дата и время доставки [до] timestamp
     * @return int|null
     */
    public function getDeliveryTimeTo(): ?int
    {
        return $this->deliveryTimeTo;
    }

    /**
     * Дата отправки timestamp
     * @return int|null
     */
    public function getSentAt(): ?int
    {
        return $this->sentAt;
    }

    /**
     * Дата доставки timestamp
     * @return int|null
     */
    public function getDeliveredAt(): ?int
    {
        return $this->deliveredAt;
    }

    /**
     * Причина недоставки timestamp
     * @return null|string
     */
    public function getUnshippingReason(): ?string
    {
        return $this->unshippingReason;
    }
}