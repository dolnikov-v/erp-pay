<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;

/**
 * Class Product
 * @package app\modules\marketplace\models
 */
class Product extends Model implements ProductInterface
{
    public $uniqueId;

    public $id;

    public $foreignId;

    public $name;

    public $categories = [];

    public $description;

    public $imageUrl;

    public $price;

    public $currencyIsoCode;

    public $availableQuantity;

    public $weight;

    public $height, $width, $length;

    public $quantityMultiplier = 1;

    /**
     * Расширение рулсов
     * @return array
     */
    protected function additionalRules(): array
    {
        return [];
    }

    /**
     * Расширение списка лейблов для атрибутов
     * @return array
     */
    protected function additionalAttributeLabels(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge([
            [['id', 'name'], 'required'],
            [['name', 'foreignId', 'description', 'currencyIsoCode', 'imageUrl', 'uniqueId'], 'string'],
            [['id', 'weight', 'width', 'height', 'length', 'availableQuantity', 'quantityMultiplier'], 'integer'],
            [['price'], 'double'],
            [['name', 'foreignId', 'currencyIsoCode', 'imageUrl', 'uniqueId'], 'trim'],
        ], $this->additionalRules());
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge([
            'uniqueId' => \Yii::t('common', 'Уникальный идентификатор записи'),
            'id' => \Yii::t('common', 'Идентификатор во внутренней системе'),
            'name' => \Yii::t('common', 'Наименование'),
            'foreignId' => \Yii::t('common', 'Идентификатор во внешней системе'),
            'description' => \Yii::t('common', 'Описание'),
            'currencyIsoCode' => \Yii::t('common', 'ISO код валюты'),
            'imageUrl' => \Yii::t('common', 'Ссылка на изображение'),
            'weight' => \Yii::t('common', 'Вес'),
            'width' => \Yii::t('common', 'Ширина упаковки'),
            'height' => \Yii::t('common', 'Высота упаковки'),
            'length' => \Yii::t('common', 'Длина упаковки'),
            'availableQuantity' => \Yii::t('common', 'Доступное количество'),
            'price' => \Yii::t('common', 'Цена'),
            'quantityMultiplier' => \Yii::t('common', 'Кол-во продуктов в одном заказе')
        ], $this->additionalAttributeLabels());
    }

    /**
     * @return null|string
     */
    public function getUniqueId(): ?string
    {
        return $this->uniqueId;
    }

    /**
     * Идентификатор продукта внутренней системы
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Идентификатор продукта в системе маркета
     * @return string
     */
    public function getForeignId(): ?string
    {
        return $this->foreignId;
    }

    /**
     * Наименование продукта
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }

    /**
     * Категории, к которым относится продукт
     * @return ProductCategoryInterface[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * Описание продукта
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Ссылка на изображение с продуктом
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * Стоимость продукта
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Валюта, в которой продается продукт
     * @return string
     */
    public function getCurrencyIsoCode(): string
    {
        return $this->currencyIsoCode;
    }

    /**
     * Доступное количество товара на складах
     * @return int
     */
    public function getAvailableQuantity(): int
    {
        return $this->availableQuantity;
    }

    /**
     * Вес продукта в граммах
     * @return int
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * Высота коробки с товаром, мм
     * @return int
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * Ширина коробки с товаром, мм
     * @return int
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * Длина коробки с товаром, мм
     * @return int
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * Опциональные аттрибуты продукта, ассоциативный массив
     * @return array
     */
    public function getOptionalAttributes(): array
    {
        return [];
    }

    /**
     * Множитель количества продуктов
     * @return int|null
     */
    public function getQuantityMultiplier(): ?int
    {
        return $this->quantityMultiplier;
    }
}