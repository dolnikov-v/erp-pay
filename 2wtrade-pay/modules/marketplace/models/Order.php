<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;
use app\modules\marketplace\abstracts\orderinformation\CustomerInformationInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderInterface;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\orderinformation\ShipmentInterface;

/**
 * Class Order
 * @package app\modules\marketplace\models
 */
class Order extends Model implements OrderInterface
{
    public $id;
    public $foreignId;
    public $orderProducts;
    public $status = self::STATUS_REQUIRE_PAYMENT;
    public $foreignStatus;
    public $shipment;
    public $customerInformation;
    public $customerAddress;
    public $paidAmount, $totalAmount, $shippingAmount;
    public $comment;
    public $currencyIsoCode;
    public $originalData;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['foreignId', 'status', 'foreignStatus', 'comment', 'currencyIsoCode', 'originalData'], 'string'],
            [['paidAmount', 'totalAmount', 'shippingAmount'], 'double']
        ];
    }

    /**
     * Идентификатор заказа в системе
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Идентификатор заказа во внешней системе (маркете)
     * @return string
     */
    public function getForeignId(): string
    {
        return $this->foreignId;
    }

    /**
     * Заказанные продукты
     * @return OrderProductInterface[]
     */
    public function getOrderProducts(): array
    {
        return $this->orderProducts;
    }

    /**
     * Стутус заказа
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Статус заказа в маркете
     * @return null|string
     */
    public function getForeignStatus(): ?string
    {
        return $this->foreignStatus;
    }

    /**
     * @return ShipmentInterface
     */
    public function getShipment(): ShipmentInterface
    {
        return $this->shipment;
    }

    /**
     * @return CustomerInformationInterface
     */
    public function getCustomerInformation(): CustomerInformationInterface
    {
        return $this->customerInformation;
    }

    /**
     * @return CustomerAddressInterface
     */
    public function getCustomerAddress(): CustomerAddressInterface
    {
        return $this->customerAddress;
    }

    /**
     * @return float
     */
    public function getPaidAmount(): float
    {
        return $this->paidAmount;
    }

    /**
     * Общая стоимость заказа без доставки
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * Стоимость доставки
     * @return float
     */
    public function getShippingAmount(): float
    {
        return $this->shippingAmount;
    }

    /**
     * Общая стоимость заказа с учетом доставки
     * @return float
     */
    public function getTotalAmountWithShipping(): float
    {
        return $this->getShippingAmount() + $this->getTotalAmount();
    }

    /**
     * Комментарий к заказу
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * ISO код валюты заказа
     * @return null|string
     */
    public function getCurrencyIsoCode(): ?string
    {
        return $this->currencyIsoCode;
    }

    /**
     * Оригинальные данные заказа в формате json
     * @return null|string
     */
    public function getOriginalData(): ?string
    {
        return $this->originalData;
    }
}