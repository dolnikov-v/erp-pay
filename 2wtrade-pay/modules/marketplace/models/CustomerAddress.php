<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\orderinformation\CustomerAddressInterface;

/**
 * Class CustomerAddress
 * @package app\modules\marketplace\models
 */
class CustomerAddress extends Model implements CustomerAddressInterface
{
    /**
     * @var string
     */
    public $blockOrInterior, $houseNumber, $street, $subDistrict, $district, $province, $state, $city, $zipCode, $country, $additionalAddress, $fullAddress;

    /**
     * @var float
     */
    public $longitude, $latitude;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'blockOrInterior',
                    'houseNumber',
                    'state',
                    'street',
                    'subDistrict',
                    'district',
                    'province',
                    'city',
                    'zipCode',
                    'country',
                    'additionalAddress',
                    'fullAddress'
                ],
                'string'
            ],
            [['longitude', 'latitude'], 'float']
        ];
    }

    /**
     * Полный адрес доставки
     * @return string
     */
    public function getFullAddress(): ?string
    {
        return $this->fullAddress;
    }

    /**
     * Строка с уточнением адреса
     * @return string
     */
    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    /**
     * Номер блока или квартиры
     * @return string
     */
    public function getBlockOrInterior(): ?string
    {
        return $this->blockOrInterior;
    }

    /**
     * Номер дома
     * @return string
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * Улица
     * @return string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * Квартал
     * @return string
     */
    public function getSubDistrict(): ?string
    {
        return $this->subDistrict;
    }

    /**
     * Район
     * @return string
     */
    public function getDistrict(): ?string
    {
        return $this->district;
    }

    /**
     * Провинция
     * @return string
     */
    public function getProvince(): ?string
    {
        return $this->province;
    }

    /**
     * Штат, область
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * Город
     * @return null|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Зип код
     * @return string
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * Страна
     * @return null|string
     */
    public function getCountryIsoCode(): ?string
    {
        return $this->country;
    }

    /**
     * Дополнительные поля
     * @return string[]
     */
    public function getOptionalAttributes(): array
    {
        return [];
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }
}