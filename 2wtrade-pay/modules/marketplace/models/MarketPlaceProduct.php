<?php

namespace app\modules\marketplace\models;


use app\components\mongodb\ActiveRecord;
use MongoDB\BSON\ObjectId;

/**
 * Class MarketPlaceProduct
 * @package app\modules\marketplace\models
 *
 * @property ObjectId $_id
 * @property string $market_place_id
 * @property integer $product_id
 * @property string $foreign_product_id
 * @property string $optional_attributes
 */
class MarketPlaceProduct extends ActiveRecord
{
    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'market_place_product';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'market_place_id', 'product_id', 'foreign_product_id', 'optional_attributes', 'created_at'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['market_place_id', 'foreign_product_id', 'optional_attributes'], 'string'],
            [['foreign_product_id', 'market_place_id'], 'trim'],
            ['product_id', 'integer']
        ];
    }
}