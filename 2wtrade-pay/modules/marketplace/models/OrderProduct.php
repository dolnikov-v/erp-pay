<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\orderinformation\OrderProductInterface;
use app\modules\marketplace\abstracts\product\ProductInterface;

/**
 * Class OrderProduct
 * @package app\modules\marketplace\models
 */
class OrderProduct extends Model implements OrderProductInterface
{
    public $product;
    public $quantity;
    public $totalPrice;
    public $price;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['quantity'], 'integer'],
            [['totalPrice', 'price'], 'double']
        ];
    }

    /**
     * Описание продукта
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    /**
     * Количество продуктов
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Общая стоимость продуктов
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * Стоимость одного продукта
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}