<?php

namespace app\modules\marketplace\models;


use app\components\mongodb\ActiveRecord;

/**
 * Class StorageParamRecord
 * @package app\modules\marketplace\models
 *
 * @property string $market_place_id
 * @property string $name
 * @property string $value
 * @property int $created_at
 */
class MarketPlaceParam extends ActiveRecord
{
    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'market_place_param';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'market_place_id', 'name', 'value', 'created_at'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['market_place_id', 'name'], 'required'],
            [['market_place_id', 'name', 'value'], 'string'],
            ['created_at', 'safe']
        ];
    }
}