<?php

namespace app\modules\marketplace\models;


use app\components\base\Model;
use app\modules\marketplace\abstracts\product\ProductCategoryInterface;

/**
 * Class ProductCategory
 * @package app\modules\marketplace\models
 */
class ProductCategory extends Model implements ProductCategoryInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var static[]
     */
    public $children = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'id'], 'required'],
            ['name', 'string'],
            ['id', 'integer'],
            ['children', 'safe']
        ];
    }

    /**
     * Наименование
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Идентификатор
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Вложенные категории
     * @return static[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * Добавление дочерней категории
     *
     * @param ProductCategoryInterface $child
     * @return ProductCategoryInterface
     */
    public function addChildren(ProductCategoryInterface $child): ProductCategoryInterface
    {
        $this->children[] = $child;
        return $this;
    }

    /**
     * Установление имени категории
     * @param string $name
     * @return ProductCategoryInterface
     */
    public function setName(string $name): ProductCategoryInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Установление идентификатора категории
     * @param string $id
     * @return ProductCategoryInterface
     */
    public function setId(string $id): ProductCategoryInterface
    {
        $this->id = $id;
        return $this;
    }
}