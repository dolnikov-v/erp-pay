<?php

namespace app\modules\marketplace\controllers;


use app\components\Notifier;
use app\modules\marketplace\components\base\WebController;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;

/**
 * Class ViewController
 * @package app\modules\marketplace\controllers
 */
class ViewController extends WebController
{
    /**
     * Используем подшаблон для отображения крошек и названия
     * @var string
     */
    public $subLayout = 'view';

    /**
     * @param $marketplaceId
     * @return \yii\web\Response
     */
    public function actionIndex($marketplaceId)
    {
        return $this->redirect(Url::toRoute(['view/configuration', 'marketplaceId' => $marketplaceId]));
    }

    /**
     * @param $marketplaceId
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionConfiguration($marketplaceId)
    {
        $marketplace = $this->findMarketPlaceById($marketplaceId);
        if (\Yii::$app->request->post() && (!$marketplace->configuration->loadData(\Yii::$app->request->post()) || !$marketplace->configuration->saveToStorage())) {
            $this->notifier->addNotification(\Yii::t('common', 'Не удалось сохранить настройки.'));
        }
        return $this->render('configuration', ['marketplace' => $marketplace]);
    }

    /**
     * @param $marketplaceId
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionProducts($marketplaceId)
    {
        $marketplace = $this->findMarketPlaceById($marketplaceId);
        $products = $marketplace->productManager->getProducts();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $products
        ]);
        return $this->render('product-list', ['dataProvider' => $dataProvider, 'marketplace' => $marketplace]);
    }

    /**
     * @param $marketplaceId
     * @param null $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEditProduct($marketplaceId, $id = null)
    {
        $marketplace = $this->findMarketPlaceById($marketplaceId);
        $model = $id ? $marketplace->productManager->getProductByUniqueId($id) : null;
        if ($data = \Yii::$app->request->post()) {
            try {
                if (empty($model)) {
                    $model = $marketplace->productManager->addProduct($data);
                } else {
                    $model = $marketplace->productManager->updateProduct($id, $data);
                }
                $this->notifier->addNotification(\Yii::t('common', 'Товар успешно сохранен.'), Notifier::TYPE_SUCCESS);
                $this->redirect(Url::toRoute([
                    'view/edit-product',
                    'marketplaceId' => $marketplace->getId(),
                    'id' => $model->uniqueId
                ]));
            } catch (\Throwable $e) {
                $this->notifier->addNotification($e->getMessage());
            }
        }

        return $this->render('edit-product', ['marketplace' => $marketplace, 'model' => $model]);
    }

    /**
     * @param $marketplaceId
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRemoveProduct($marketplaceId, $id)
    {
        $marketplace = $this->findMarketPlaceById($marketplaceId);
        if (!$marketplace->productManager->removeProduct($id)) {
            $this->notifier->addNotification(\Yii::t('common', 'Не удалось удалить продукт.'));
        }
        return $this->redirect(['products', 'marketplaceId' => $marketplaceId]);
    }
}