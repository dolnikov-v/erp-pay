<?php

namespace app\modules\marketplace\controllers;

use app\modules\marketplace\components\base\WebController;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class IndexController
 * @package app\modules\marketplace\controllers
 */
class IndexController extends WebController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $this->module->marketplaces
            ])
        ]);
    }

    /**
     * @param $marketplaceId
     * @throws NotFoundHttpException
     */
    public function actionSwitchAvailability($marketplaceId)
    {
        $marketPlace = $this->findMarketPlaceById($marketplaceId);
        $marketPlace->configuration->enabled = !$marketPlace->configuration->enabled;
        if (!$marketPlace->configuration->saveToStorage(['enabled'])) {
            $this->notifier->addNotification(\Yii::t('common', 'Не удалось сохранить настройки маркетплейса.'));
        }
        $this->redirect('index');
    }
}