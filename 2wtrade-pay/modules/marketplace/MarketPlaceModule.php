<?php

namespace app\modules\marketplace;

use app\components\base\Module;
use app\modules\marketplace\abstracts\configuration\MarketPlaceConfigurationInterface;
use app\modules\marketplace\abstracts\DataTransmitterInterface;
use app\modules\marketplace\abstracts\extensions\MarketPlaceWithWebHookInterface;
use app\modules\marketplace\abstracts\MarketPlaceInterface;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithCacheServiceInterface;
use app\modules\marketplace\abstracts\services\objectextensions\ObjectWithStorageServiceInterface;
use app\modules\marketplace\components\CacheService;
use app\modules\marketplace\components\DefaultMarketPlaceConfiguration;
use app\modules\marketplace\components\StorageService;

/**
 * Class Module
 * @package app\modules\marketplace
 *
 * @property MarketPlaceInterface[] $marketplaces
 * @property StorageService $defaultStorageService Компонент для сохранения данных маркетов в хранилище
 * @property CacheService $defaultCacheService
 * @property DataTransmitterInterface $dataConverter Конвертирование внутренних данных в данные системы
 */
class MarketPlaceModule extends Module
{
    /**
     * @var array
     */
    public $defaultMarketplaceConfiguration = [];

    /**
     * @var MarketPlaceInterface[]
     */
    protected $marketplaces = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->marketplaces = [];
        foreach (array_keys($this->modules) as $moduleId) {
            if (($module = $this->getModule($moduleId)) instanceof MarketPlaceInterface) {
                if (($module instanceof ObjectWithStorageServiceInterface) && !empty($this->defaultStorageService) && empty($module->storageService)) {
                    $module->setStorageService(clone $this->defaultStorageService);
                }
                if (($module instanceof ObjectWithCacheServiceInterface) && !empty($this->defaultCacheService) && empty($module->cacheService)) {
                    $module->setCacheService(clone $this->defaultCacheService);
                }
                if (is_array($this->defaultMarketplaceConfiguration)) {
                    foreach ($this->defaultMarketplaceConfiguration as $key => $value) {
                        if (property_exists($module, $key) && empty($module->{$key})) {
                            $module->{$key} = $value;
                        }
                    }
                }
                $this->marketplaces[$module->id] = $module;
            }
        }
    }

    /**
     * @return MarketPlaceInterface[]
     */
    public function getMarketplaces(): array
    {
        return $this->marketplaces;
    }

    /**
     * @param string $id
     * @return MarketPlaceInterface|null
     */
    public function getMarketPlaceById(string $id): ?MarketPlaceInterface
    {
        return $this->marketplaces[$id] ?? null;
    }

    /**
     * @param string $apiKey
     * @return MarketPlaceInterface|MarketPlaceWithWebHookInterface|null
     */
    public function getMarketPlaceByApiKey(string $apiKey): ?MarketPlaceWithWebHookInterface
    {
        foreach ($this->getMarketplaces() as $marketplace) {
            if (($marketplace instanceof MarketPlaceWithWebHookInterface) && $marketplace->getApiKey() == $apiKey) {
                return $marketplace;
            }
        }
        return null;
    }
}