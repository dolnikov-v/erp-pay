<?php
namespace app\modules\finance\assets;

use yii\web\AssetBundle;

/**
 * Class ReportProfitabilityAnalysisAsset
 * @package app\modules\finance\assets
 */
class ReportProfitabilityAnalysisAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/finance/report-profitability-analysis';

    public $js = [
        'index.js',
    ];

    public $css = [
        'index.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
