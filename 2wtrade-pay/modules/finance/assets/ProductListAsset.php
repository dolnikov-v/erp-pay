<?php

namespace app\modules\finance\assets;

use yii\web\AssetBundle;

/**
 * Class ProductListAsset
 * @package app\modules\finance\assets
 */
class ProductListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/finance/product-list';

    public $js = [
        'product-list.js',
    ];

    public $css = [];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
