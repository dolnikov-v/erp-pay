<?php
namespace app\modules\finance\assets;

use yii\web\AssetBundle;

/**
 * Class ReportExpensesItemEditFormAsset
 * @package app\modules\finance\assets
 */
class ReportExpensesItemEditFormAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/finance/report-expenses-item';

    public $js = [
        'edit-form.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
