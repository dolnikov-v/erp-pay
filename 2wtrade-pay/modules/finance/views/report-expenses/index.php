<?php
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $countries */

$this->title = Yii::t('common', 'Анализ расходов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Анализ расходов'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProvider,
        'countries' => $countries,
    ]),
]) ?>
