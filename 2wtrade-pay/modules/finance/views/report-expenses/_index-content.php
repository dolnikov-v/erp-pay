<?php
use app\modules\finance\extensions\GridViewReportExpenses;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $countries */
?>

<div class="table-responsive">
    <?= GridViewReportExpenses::widget([
        'id' => 'report-grid-expense-analysis',
        'tableOptions' => ['class' => 'table table-striped table-hover table-bordered tl-fixed'],
        'countries' => $countries,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
