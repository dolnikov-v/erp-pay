<?php
use app\modules\finance\models\ReportExpensesCountry;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\GridView;
use app\widgets\Label;
use app\components\grid\IdColumn;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список категорий');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочник'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список категорий'),
    'withBody' => false,
    'content' => GridView::widget([
        'summary' => false,
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'code',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'content' => function($model) {
                    /* @var ReportExpensesCountry $model */
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT
                    ]);
                },
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/finance/report-expenses-category/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('finance.reportexpensescategory.edit');
                        }
                    ],
                    [
                        'can' => function () {
                            return
                                Yii::$app->user->can('finance.reportexpensescategory.edit') &&
                                Yii::$app->user->can('finance.reportexpensescategory.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/finance/report-expenses-category/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('finance.reportexpensescategory.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('finance.reportexpensescategory.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить категорию'),
                'url' => Url::toRoute('/finance/report-expenses-category/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
