<?php
use app\modules\finance\models\ReportExpensesItem;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use yii\helpers\Url;
use app\components\grid\GridView;
use app\widgets\Label;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
?>

<?= GridView::widget([
    'summary' => false,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table-sortable'
    ],
    'dataProvider' => $dataProvider,
    'columns' => [
        'code',
//        [
//            'attribute' => 'category.name',
//            'label' => Yii::t('common', 'Категория'),
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
//        ],
        'name',
        [
            'attribute' => 'type',
            'content' => function($model) {
                /* @var ReportExpensesItem $model */
                return Label::widget([
                    'label' => $model->getTypeLabel(),
                    'style' => ($model->type == ReportExpensesItem::TYPE_REGULAR) ? Label::STYLE_DEFAULT : Label::STYLE_WARNING
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'label' => Yii::t('common', 'Значение/Формула'),
            'content' => function($model) {
                /* @var ReportExpensesItem $model */
                return ($model->type == ReportExpensesItem::TYPE_REGULAR) ? $model->value : \app\helpers\math\Formula::getFormattedFormula($model->formula, $model->getFormulaVariables());
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ],
        [
            'attribute' => 'active',
            'content' => function($model) {
                /* @var ReportExpensesItem $model */
                return Label::widget([
                    'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'is_expense',
            'content' => function($model) {
                /* @var ReportExpensesItem $model */
                return Label::widget([
                    'label' => $model->is_expense ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $model->is_expense ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'detailed',
            'content' => function($model) {
                /* @var ReportExpensesItem $model */
                return Label::widget([
                    'label' => $model->detailed ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $model->detailed ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'updated_at',
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($model) {
                        return Url::toRoute(['/finance/report-expenses-item/edit', 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('finance.reportexpensesitem.edit');
                    }
                ],
                [
                    'can' => function () {
                        return
                            Yii::$app->user->can('finance.reportexpensesitem.edit') &&
                            Yii::$app->user->can('finance.reportexpensesitem.delete');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($model) {
                        return [
                            'data-href' => Url::toRoute(['/finance/report-expenses-item/delete', 'id' => $model->id]),
                        ];
                    },
                    'can' => function () {
                        return Yii::$app->user->can('finance.reportexpensesitem.delete');
                    }
                ],
            ]
        ]
    ],
]) ?>