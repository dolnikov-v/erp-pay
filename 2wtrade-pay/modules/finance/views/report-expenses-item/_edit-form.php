<?php
use app\widgets\CKEditor;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var array $categories */
/** @var \app\modules\finance\models\ReportExpensesItem $model */
?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'code')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'category_id')->select2List($categories, ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'type')->select2List($model->typeList()) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'value')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'formula')->widget(CKEditor::className(), ['tokens' => $model->getTokens(), 'toolbarLocation' => 'bottom']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'is_expense')->checkboxCustom([
            'value' => 1,
            'checked' => $model->is_expense,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'detailed')->checkboxCustom([
            'value' => 1,
            'checked' => $model->detailed,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить статью расходов') : Yii::t('common', 'Сохранить статью расходов')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::to(['index'])) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
