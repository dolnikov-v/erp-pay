<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\finance\assets\ReportExpensesItemEditFormAsset;

/** @var \yii\web\View $this */
/** @var array $categories */
/** @var \app\modules\finance\models\ReportExpensesItem $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление статьи расходов') : Yii::t('common', 'Редактирование статьи расходов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи расходов'), 'url' => Url::to(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportExpensesItemEditFormAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статья расходов'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'categories' => $categories,
    ])
]) ?>