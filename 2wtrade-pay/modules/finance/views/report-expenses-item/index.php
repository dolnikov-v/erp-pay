<?php
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var array $items */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Статьи расходов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочник'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$tabs = [];
foreach ($items as $item) {
    $tabs[] = [
        'label' => Yii::t('common', $item['name']),
        'content' => $this->render('_index-content', [
            'dataProvider' => $item['dataProvider'],
        ]),
    ];
}
?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Статьи расходов'),
//    'withBody' => false,
    'nav' => new Nav(['tabs' => $tabs]),
    'footer' => (Yii::$app->user->can('finance.reportexpensesitem.edit') ?
        ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить статью расходов'),
            'url' => Url::toRoute('/finance/report-expenses-item/edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '')
]) ?>

<?= ModalConfirmDelete::widget() ?>
