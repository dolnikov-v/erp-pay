<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\finance\models\ReportExpensesItem;
use app\modules\finance\widgets\ProductList;
use app\widgets\CKEditor;

/** @var \app\modules\finance\models\ReportExpensesCountryItem $model */
/** @var array $productList */
/** @var array $productCosts */
/** @var integer $categoryId */

?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'action' => Url::toRoute([
        'edit',
        'id' => $model->parent_id,
        'country_id' => $model->country_id,
        'category_id' => $model->category_id,
        'dateFrom' => $model->date_from,
        'dateTo' => $model->date_to,
    ]),
    'enableClientValidation' => true
]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'category')->textInput([
                'disabled' => true,
                'value' => $model->category ? $model->category->name : Yii::t('common', 'Без категории')
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'parent')->textInput(['disabled' => true, 'value' => $model->parent->name]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'country')->textInput(['disabled' => true, 'value' => $model->country->name]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'type')->select2List(ReportExpensesItem::typeList()) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'value')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'formula')->widget(CKEditor::className(), [
                'tokens' => $model->getTokens(),
                'toolbarLocation' => 'bottom'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'date_from')->datePicker([
                'disabled' => true
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'date_to')->datePicker([
                'disabled' => true
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'active')->checkboxCustom([
                'value' => 1,
                'checked' => $model->active,
                'uncheckedValue' => 0,
            ]) ?>
        </div>
    </div>

<?php if ($model->parent->detailed): ?>
    <hr>
    <div class="row margin-top-20">
        <div class="col-lg-12">
            <h4><?= Yii::t('common', 'Продукты') ?></h4>
            <?= ProductList::widget([
                'productList' => $productList,
                'productCosts' => $productCosts,
                'parentCode' => $model->parent->code
            ]) ?>
        </div>
    </div>
    <hr>
<?php endif; ?>

    <div class="row margin-top-20">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить статью расходов') : Yii::t('common', 'Сохранить статью расходов')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::to([
                'index',
                's' => [
                    'country_id' => $model->country_id,
                    'date_from' => $model->date_from,
                    'date_to' => $model->date_to,
                ]
            ])) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>