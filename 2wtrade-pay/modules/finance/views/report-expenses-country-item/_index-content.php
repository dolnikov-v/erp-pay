<?php
use app\modules\finance\models\ReportExpensesItem;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\components\grid\GridView;
use app\widgets\Label;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var integer $categoryId */
/** @var array $countryExpenses */
/** @var \app\modules\finance\models\filter\ReportExpensesCountryItemFilter $filterForm */
?>

<?= GridView::widget([
    'summary' => false,
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'name',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'type',
            'content' => function($model) use ($countryExpenses) {
                /* @var ReportExpensesItem $model */
                if ($item = ArrayHelper::getValue($countryExpenses, $model->id)) {
                    $label = $item->getTypeLabel();
                    $type = $item->type;
                } else {
                    $label = $model->getTypeLabel();
                    $type = $model->type;
                }
                return Label::widget([
                    'label' => $label,
                    'style' => ($type == ReportExpensesItem::TYPE_REGULAR) ? Label::STYLE_DEFAULT : Label::STYLE_WARNING
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Значение/Формула'),
            'content' => function($model) use ($countryExpenses) {
                /* @var ReportExpensesItem $model */
                if ($item = ArrayHelper::getValue($countryExpenses, $model->id)) {
                    $content = ($item->type == ReportExpensesItem::TYPE_REGULAR) ? $item->value : \app\helpers\math\Formula::getFormattedFormula($item->formula, $item->getFormulaVariables());
                } else {
                    $content = ($model->type == ReportExpensesItem::TYPE_REGULAR) ? $model->value : \app\helpers\math\Formula::getFormattedFormula($model->formula, $model->getFormulaVariables());;
                }
                return $content;
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ],
        [
            'attribute' => 'active',
            'content' => function($model) use ($countryExpenses) {
                /* @var ReportExpensesItem $model */
                if ($item = ArrayHelper::getValue($countryExpenses, $model->id)) {
                    $active = $item->active;
                } else {
                    $active = $model->active;
                }
                return Label::widget([
                    'label' => $active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT
                ]);
            },
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Дата создания'),
            'enableSorting' => false,
            'content' => function($model) use ($countryExpenses) {
                /* @var ReportExpensesItem $model */
                if ($item = ArrayHelper::getValue($countryExpenses, $model->id)) {
                    return Yii::$app->formatter->asDatetime($item->created_at);
                } else {
                    return '-';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Дата изменения'),
            'enableSorting' => false,
            'content' => function($model) use ($countryExpenses) {
                /* @var ReportExpensesItem $model */
                if ($item = ArrayHelper::getValue($countryExpenses, $model->id)) {
                    return Yii::$app->formatter->asDatetime($item->updated_at);
                } else {
                    return '-';
                }
            },
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($model) use ($filterForm, $categoryId) {
                        /* @var ReportExpensesItem $model */
                        return Url::to(['/finance/report-expenses-country-item/edit',
                            'id' => $model->id,
                            'country_id' => $filterForm->country_id,
                            'dateFrom' => $filterForm->dateFrom,
                            'dateTo' => $filterForm->dateTo,
                            'category_id' => $categoryId,
                        ]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('finance.reportexpensescountryitem.edit');
                    }
                ],
                [
                    'can' => function ($model) use ($countryExpenses) {
                        /* @var ReportExpensesItem $model */
                        return (
                            Yii::$app->user->can('finance.reportexpensescountryitem.delete') &&
                            ArrayHelper::getValue($countryExpenses, $model->id)
                        );
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($model) use ($filterForm, $categoryId) {
                        return [
                            'data-href' => Url::to([
                                '/finance/report-expenses-country-item/delete',
                                'id' => $model->id,
                                'country_id' => $filterForm->country_id,
                                'dateFrom' => $filterForm->dateFrom,
                                'dateTo' => $filterForm->dateTo,
                                'category_id' => $categoryId,
                            ]),
                        ];
                    },
                    'can' => function ($model) use ($countryExpenses) {
                        /* @var ReportExpensesItem $model */
                        return (
                            Yii::$app->user->can('finance.reportexpensescountryitem.delete') &&
                            ArrayHelper::getValue($countryExpenses, $model->id)
                        );
                    }
                ],
            ]
        ]
    ],
]) ?>
