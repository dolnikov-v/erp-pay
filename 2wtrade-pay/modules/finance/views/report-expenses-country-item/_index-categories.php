<?php
use app\modules\finance\models\ReportExpensesItem;
use app\components\grid\ActionColumn;
use yii\helpers\Url;
use kartik\grid\GridView;

/** @var yii\web\View $this */
/** @var $filterForm */
/** @var yii\data\ActiveDataProvider $dataProvider */
?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table table-striped table-hover table-sortable margin-0'],
    'rowOptions' => ['class' => 'tr-vertical-align-middle'],
    'layout' => '{items}',
    'summary' => false,
    'bordered' => false,
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => Yii::t('common', 'Категория'),
            'attribute' => 'category.name',
            'enableSorting' => false,
            'group' => true,
        ],
        [
            'attribute' => 'name',
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Офис'),
            'attribute' => 'office.name',
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Служба доставки'),
            'attribute' => 'delivery.name',
            'enableSorting' => false,
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($model) use ($filterForm) {
                        /* @var ReportExpensesItem $model */
                        return Url::to(['/finance/report-expenses-country-item/edit-category',
                            'id' => $model->id,
                            'country_id' => $filterForm->country_id,
                        ]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('finance.reportexpensescountryitem.editcategory');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($model) use ($filterForm) {
                        return [
                            'data-href' => Url::to([
                                '/finance/report-expenses-country-item/delete-category',
                                'id' => $model->id,
                                'country_id' => $filterForm->country_id,
                            ]),
                        ];
                    },
                    'can' => function () {
                        return Yii::$app->user->can('finance.reportexpensescountryitem.deletecategory');
                    }
                ],
            ]
        ]
    ],
]) ?>
