<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\finance\models\ReportExpensesCountryItem $model */
/** @var array $categoryCollection */
/** @var array $deliveryCollection */
/** @var array $officeCollection */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление категории') : Yii::t('common', 'Редактирование категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи расходов'), 'url' => Url::to(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Категории расходов'),
    'content' => $this->render('_edit-category-form', [
        'model' => $model,
        'categoryCollection' => $categoryCollection,
        'deliveryCollection' => $deliveryCollection,
        'officeCollection' => $officeCollection,
    ])
]) ?>

