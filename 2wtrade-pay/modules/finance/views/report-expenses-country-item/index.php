<?php
use app\modules\finance\widgets\CategoryDropdown;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use app\modules\finance\models\ReportExpensesCountryItem;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $categoryDataProvider */
/** @var ReportExpensesCountryItem[] $countryExpenses */
/** @var array $countryCollection */
/** @var array $categoryCollection */
/** @var \app\modules\finance\models\filter\ReportExpensesCountryItemFilter $filterForm */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Статьи расходов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочник'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_index-filter', [
        'filterForm' => $filterForm,
        'countryCollection' => $countryCollection,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Категории'),
    'withBody' => false,
    'content' => $this->render('_index-categories', [
        'dataProvider' => $categoryDataProvider,
        'filterForm' => $filterForm,
    ]),
    'collapse' => true,
    'footer' => (Yii::$app->user->can('finance.reportexpensescountryitem.editcategory')
        ? CategoryDropdown::widget([
            'categoryList' => $categoryCollection,
            'countryId' => $filterForm->country_id,
        ]) : '')
]) ?>

<?php
$tabs = [];
foreach ($items as $item) {
    $tabs[] = [
        'label' => Yii::t('common', $item['name']),
        'content' => $this->render('_index-content', [
            'filterForm' => $filterForm,
            'dataProvider' => $item['dataProvider'],
            'countryExpenses' => $item['countryExpenses'],
            'categoryId' => $item['id'],
        ]),
    ];
}
?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Статьи расходов'),
    'withBody' => false,
    'nav' => new \app\widgets\Nav(['tabs' => $tabs]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
