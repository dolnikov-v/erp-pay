<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $countryCollection */
/** @var \app\modules\finance\models\filter\ReportExpensesCountryItemFilter $filterForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($filterForm, 'country_id')->select2List($countryCollection) ?>
    </div>
    <div class="col-lg-8">
        <?= $form->field($filterForm, 'date')->dateRangePicker('dateFrom', 'dateTo', ['ranges' => ['list' => \app\widgets\DateRangePicker::getMonthRanges()]]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
