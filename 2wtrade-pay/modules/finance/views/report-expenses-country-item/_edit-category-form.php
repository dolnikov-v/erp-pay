<?php
use app\components\widgets\ActiveForm;
use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesCountryCategory;
use yii\helpers\Url;

/** @var ReportExpensesCountryCategory $model */
/** @var array $categoryCollection */
/** @var array $deliveryCollection */
/** @var array $officeCollection */
?>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'action' => Url::toRoute(['edit-category',
        'id' => $model->id,
        'country_id' => $model->country_id,
        'category_id' => $model->category_id,
    ]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'category_id')->select2List($categoryCollection, ['prompt' => '—', 'disabled' => true]) ?>
    </div>

<?php if (in_array($model->category->code, [
    ReportExpensesCategory::CATEGORY_CALL_CENTER,
    ReportExpensesCategory::CATEGORY_BRANCH,
    ReportExpensesCategory::CATEGORY_STORY,
])) { ?>
    <div class="col-lg-4">
        <?= $form->field($model, 'office_id')->select2List($officeCollection, ['prompt' => '—']) ?>
    </div>
<?php } ?>

<?php if ($model->category->code == ReportExpensesCategory::CATEGORY_DELIVERY) { ?>
    <div class="col-lg-4">
        <?= $form->field($model, 'delivery_id')->select2List($deliveryCollection, ['prompt' => '—']) ?>
    </div>
<?php } ?>

</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить категорию') : Yii::t('common', 'Сохранить категорию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::to(['index', 's' => ['country_id' => $model->country_id]])) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>