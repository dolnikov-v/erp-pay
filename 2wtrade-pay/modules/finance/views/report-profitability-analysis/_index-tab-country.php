<?php
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider[] $dataProviders */
/** @var array $countryCollection */
/** @var array $groupCollection */
/** @var array $categoryCollection */
/** @var array $categoryItems */
/** @var $filterForm */

$formatter = new IntlDateFormatter(Yii::$app->formatter->locale, IntlDateFormatter::LONG, IntlDateFormatter::LONG);
$formatter->setCalendar(IntlDateFormatter::GREGORIAN);
$formatter->setPattern('LLLL yyyy');
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Финансы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
    //    'alert' => $reportForm->panelAlert,
    //    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => true,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProviders['finance'],
        'categoryCollection' => $categoryCollection,
        'groupCollection' => $groupCollection,
        'categoryItems' => $categoryItems,
        'orientation' => 'normal',
        'tableType' => 'finance',
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Показатели - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
    //    'alert' => $reportForm->panelAlert,
    //    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => true,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProviders['design'],
        'groupCollection' => $groupCollection,
        'orientation' => 'normal',
        'tableType' => 'design',
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Расходы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
    //    'alert' => $reportForm->panelAlert,
    //    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => true,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProviders['estimate'],
        'groupCollection' => $groupCollection,
        'orientation' => 'inversion',
        'tableType' => 'estimate',
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Финансы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01 -1month"))]),
    //    'alert' => $reportForm->panelAlert,
    //    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => true,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProviders['prev_month'],
        'groupCollection' => $groupCollection,
        'orientation' => 'normal',
        'tableType' => 'prev_month',
    ]),
]) ?>

