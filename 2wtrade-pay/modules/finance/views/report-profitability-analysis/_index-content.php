<?php
use app\modules\finance\extensions\GridViewReportProfitabilityAnalysis;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $groupCollection */
/** @var array $categoryCollection */
/** @var array $categoryItems */
/** @var string $orientation */
/** @var string $tableType */
?>

<div class="table-responsive">
    <?= GridViewReportProfitabilityAnalysis::widget([
        'summary' => false,
        'bordered' => false,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable finance-profitability-analysis-table'
        ],
        'categoryCollection' => $categoryCollection,
        'groupCollection' => $groupCollection,
        'categoryItems' => $categoryItems,
        'renderOrientation' => $orientation,
        'dataProvider' => $dataProvider,
        'tableType' => $tableType,
    ]) ?>
</div>
