<?php
use app\modules\finance\models\filter\ReportProfitabilityAnalysisFilter;
use app\widgets\Nav;
use app\widgets\Panel;
use app\modules\finance\assets\ReportProfitabilityAnalysisAsset;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider[] $dataProviders */
/** @var \yii\data\ArrayDataProvider $countriesDataProvider */
/** @var array $countryCollection */
/** @var array $groupCollection */
/** @var array $categoryCollection */
/** @var array $categoryItems */
/** @var ReportProfitabilityAnalysisFilter $filterForm */
/** @var $forecastForm */
/** @var $reportForm */

$this->title = Yii::t('common', 'Операционный баланс (по дате создания Лида)');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportProfitabilityAnalysisAsset::register($this);
?>

<?php if ($filterForm->isForecastPeriod()): ?>

    <?= Panel::widget([
        'nav' => new Nav([
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Фильтр'),
                    'content' => $this->render('_index-filter', [
                        'filterForm' => $filterForm,
                        'countryCollection' => $countryCollection,
                        'groupCollection' => $groupCollection,
                    ]),
                ],
                [
                    'label' => Yii::t('common', 'Расчетные показатели'),
                    'content' => $this->render('_index-forecast-form', [
                        'filterForm' => $filterForm,
                        'forecastForm' => $forecastForm,
                        'countryCollection' => $countryCollection,
                        'groupCollection' => $groupCollection,
                    ]),
                ],
            ]
        ]),
    ]) ?>

<?php else: ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Фильтр'),
        'content' => $this->render('_index-filter', [
            'filterForm' => $filterForm,
            'countryCollection' => $countryCollection,
            'groupCollection' => $groupCollection,
        ]),
        'collapse' => true,
    ]) ?>

<?php endif; ?>

<?= Panel::widget([
    'border' => false,
    'withBody' => false,
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'nav' => new Nav([
        'activeTab' => 5,
        'tabs' => [
            5 => [
                'label' => Yii::t('common', 'Детально по стране'),
                'content' => $filterForm->country_id ? $this->render('_index-tab-country', [
                    'filterForm' => $filterForm,
                    'dataProviders' => $dataProviders,
                    'countryCollection' => $countryCollection,
                    'groupCollection' => $groupCollection,
                    'categoryCollection' => $categoryCollection,
                    'categoryItems' => $categoryItems
                ]) : '',
            ],
            6 => [
                'label' => Yii::t('common', 'По всем странам'),
                'content' => $this->render('_index-tab-all-countries', [
                    'countriesDataProvider' => $countriesDataProvider,
                    'filterForm' => $filterForm,
                ]),
            ],
        ]
    ]),
]) ?>
