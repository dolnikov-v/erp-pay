<?php
use app\components\widgets\ActiveForm;
use app\modules\finance\widgets\ExportDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $groupCollection */
/** @var array $countryCollection */
/** @var \app\modules\finance\models\filter\ReportProfitabilityAnalysisFilter $filterForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($filterForm, 'country_id')
            ->select2List($countryCollection, [
                'prompt' => Yii::t('common', '-'),
                'length' => false
            ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($filterForm, 'date')->datePicker(['type' => 'month']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
        <div class="pull-right">
            <?= ExportDropdown::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
