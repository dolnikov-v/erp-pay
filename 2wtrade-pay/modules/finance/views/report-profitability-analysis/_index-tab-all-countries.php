<?php
use kartik\grid\GridView;
use app\widgets\Button;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var $filterForm */
/** @var \yii\data\ArrayDataProvider $countriesDataProvider */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Информация по странам'),
    'border' => false,
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $countriesDataProvider,
        'showPageSummary' => true,
        'layout' => '{items}',
        'bordered' => false,
        'tableOptions' => [
            'id' => 'current-balance-all-country-table',
        ],
        'columns' => [
            [
                'attribute' => 'country',
                'label' => Yii::t('common', 'Страна'),
                'pageSummary' => Yii::t('common', 'ИТОГО'),
            ],
            [
                'label' => Yii::t('common', 'Стоимость лида'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'lead_avg_cost',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px'], 'data-summary' => 'avg'],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'pageSummaryFunc' => function ($data) {
                    foreach ($data as $k => $v) {
                        if (!$v) {
                            unset($data[$k]);
                            continue;
                        }
                        $data[$k] = (float)str_replace(',', '.', $data[$k]);
                    };
                    return count($data) ? (array_sum($data) / count($data)) : 0;
                },
                'attribute' => 'lead_avg_cost',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['lead_avg_cost'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Ср. расход на оператора'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'operator_avg_salary',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px'], 'data-summary' => 'avg'],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'pageSummaryFunc' => function ($data) {
                    foreach ($data as $k => $v) {
                        if (!$v) {
                            unset($data[$k]);
                            continue;
                        }
                        $data[$k] = (float)str_replace(',', '.', $data[$k]);
                    };
                    return count($data) ? (array_sum($data) / count($data)) : 0;
                },
                'attribute' => 'operator_avg_salary',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['operator_avg_salary'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Стоимость доставки'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'delivery_cost',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'attribute' => 'delivery_cost',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['delivery_cost'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Стоимость склада'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'warehouse_avg_costs',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'attribute' => 'warehouse_avg_costs',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['warehouse_avg_costs'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Доход'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'income',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'attribute' => 'income',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['income'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Расход'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'costs',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'attribute' => 'costs',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['costs'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'label' => Yii::t('common', 'Прибыль'),
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px'],
                    'data-field' => 'profit',
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '115px']],
                'pageSummaryOptions' => ['style' => ['text-align' => 'center']],
                'pageSummary' => true,
                'attribute' => 'profit',
                'format' => ['decimal', 2],
                'content' => function ($model) {
                    $value = $model['profit'];
                    if (!$value) {
                        return '';
                    }
                    return Yii::$app->formatter->asDecimal($value, ((int) $value == $value) ? 0 : 2);
                },
            ],
            [
                'header' => Button::widget([
                    'icon' => 'wb-refresh',
                    'size' => Button::SIZE_SMALL,
                    'style' => Button::STYLE_PRIMARY . ' btn-update-all',
                ]),
                'contentOptions' => ['style' => ['text-align' => 'center', 'width' => '35px']],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '35px']],
                'content' => function ($model) use($filterForm) {
                    return Button::widget([
                        'icon' => 'wb-refresh',
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_PRIMARY . ' btn-update',
                        'attributes' => [
                            'data-url' => Url::to(['refresh-data',
                                'country_id' => $model['country_id'],
                                'month' => $filterForm->month,
                                'year' => $filterForm->year,
                            ]),
                        ]
                    ]);
                },
            ],
        ],
    ])
]) ?>
