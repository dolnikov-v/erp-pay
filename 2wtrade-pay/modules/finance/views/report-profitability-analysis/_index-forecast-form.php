<?php
use app\components\widgets\ActiveForm;
use app\modules\finance\models\filter\ReportProfitabilityAnalysisFilter;
use app\modules\finance\models\filter\ReportProfitabilityAnalysisForecastForm;
use app\modules\finance\widgets\ExportDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $groupCollection */
/** @var array $countryCollection */
/** @var ReportProfitabilityAnalysisFilter $filterForm */
/** @var ReportProfitabilityAnalysisForecastForm $forecastForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index',
        's[country_id]' => $filterForm->country_id,
        's[date]' => $filterForm->date,
    ],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="forecast-variables">

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($forecastForm, 'operator_count')->textInput() ?>
        </div>
    </div>

    <table class="table table-striped table-hover table-sortable">
        <thead>
            <tr>
                <th><?= Yii::t('common', 'Прогноз по товарам') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Лидов в день') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Процент апрува') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Процент выкупа') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Процент отказа') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Процент в процессе') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($groupCollection as $id => $label ) { ?>
            <tr>
                <td><?= $label ?></td>
                <td width="80">
                    <?= Html::textInput(
                        "{$forecastForm->formName()}[lead_per_day][{$id}]",
                        ArrayHelper::getValue($forecastForm->lead_per_day, $id),
                        ['class' => 'form-control text-center']
                    ) ?>
                </td>
                <td width="80">
                    <?= Html::textInput(
                        "{$forecastForm->formName()}[approve_percent][{$id}]",
                        ArrayHelper::getValue($forecastForm->approve_percent, $id),
                        ['class' => 'form-control text-center']
                    ) ?>
                </td>
                <td width="80">
                    <?= Html::textInput(
                        "{$forecastForm->formName()}[buyout_percent][{$id}]",
                        ArrayHelper::getValue($forecastForm->buyout_percent, $id),
                        ['class' => 'form-control text-center']
                    ) ?>
                </td>
                <td width="80">
                    <?= Html::textInput(
                        "{$forecastForm->formName()}[refuse_percent][{$id}]",
                        ArrayHelper::getValue($forecastForm->refuse_percent, $id),
                        ['class' => 'form-control text-center']
                    ) ?>
                </td>
                <td width="80">
                    <?= Html::textInput(
                        "{$forecastForm->formName()}[in_process_percent][{$id}]",
                        ArrayHelper::getValue($forecastForm->in_process_percent, $id),
                        ['class' => 'form-control text-center', 'disabled' => true]
                    ) ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-4">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить параметры'), Url::toRoute(['index',
            's[country_id]' => $filterForm->country_id,
            's[date]' => $filterForm->date
        ])) ?>
        <div class="pull-right">
            <?= ExportDropdown::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
