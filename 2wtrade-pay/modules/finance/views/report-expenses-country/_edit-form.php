<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\finance\models\ReportExpensesCountryItem $model */
/** @var array $countryList */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($countryList, ['prompt' => '—', 'length' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
        ]) ?>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить страну') : Yii::t('common', 'Сохранить страну')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::to(['index'])) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
