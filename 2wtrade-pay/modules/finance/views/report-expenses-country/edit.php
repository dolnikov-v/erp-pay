<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\finance\models\ReportExpensesCountry $model */
/** @var array $countryList */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление страны') : Yii::t('common', 'Редактирование страны');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи расходов'), 'url' => Url::to(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Страна'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'countryList' => $countryList,
    ])
]) ?>