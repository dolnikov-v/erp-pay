<?php

namespace app\modules\finance\components;

use app\modules\finance\models\ReportExpensesCountry;
use yii\base\BaseObject;

class EstimateProvider extends BaseObject
{
    const GROUP_BY_PRODUCT = 'product';

    const PROVIDER_FINANCE = 'finance';
    const PROVIDER_CALL_CENTER = 'call_center';
    const PROVIDER_CATALOG = 'catalog';
    const PROVIDER_DELIVERY = 'delivery';
    const PROVIDER_CALL_CENTER_DIRECTION = 'call_center_direction';

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $groupBy;

    /**
     * @var integer
     */
    public $month;

    /**
     * @var integer
     */
    public $year;

    /**
     * @var ReportExpensesCountry
     */
    public $country;

    /**
     * @var string
     */
    public $dateFrom;

    /**
     * @var string
     */
    public $dateTo;

    /**
     * @var array[]
     */
    protected $items;

    /**
     * @var EstimateFactory
     */
    public $factory;

    /**
     * @var bool
     */
    public $isForecastPeriod;

    /**
     * @var array
     */
    public $forecastParams = [];

    /**
     * @return int
     */
    public function getForecastDays()
    {
        $ts1 = strtotime(date('Y-m-d 00:00:00'));
        $ts2 = strtotime($this->dateTo . ' 00:00:00');

        return round(($ts2 - $ts1) / 86400);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->month && $this->year) {
            $lastDay = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
            $lastDay = str_pad($lastDay, 2, '0', STR_PAD_LEFT);
            $this->dateFrom = $this->year . '-' . $this->month . '-01';
            $this->dateTo = $this->year . '-' . $this->month . '-' . $lastDay;
        }

        parent::init();
    }

    /**
     * @return EstimateItem[]
     */
    public function getItems()
    {
        if (is_null($this->items)) {
            $this->items = [];
            foreach ($this->loadItems() as $item) {
                $estimateItem = new EstimateItem($item);
                $estimateItem->factory = $this->factory;
                $this->items[] = $estimateItem;
            }
        }
        return $this->items;
    }

    /**
     * @return array[]
     */
    public function loadItems()
    {
        return [];
    }

    /**
     * @param $code
     * @return bool|mixed
     */
    public function getItemDetailValues($code)
    {
        foreach ($this->getItems() as $item) {
            if ($item->code == $code) {
                return $item->detailValues ?? false;
            }
        }
        return false;
    }

    /**
     * @param $value
     * @param array $array
     * @param integer $key
     * @return float|int
     */
    public static function getDivideByPart($value, $array, $key)
    {
        if (!$array || !isset($array[$key]) || !$sum = array_sum($array)) {
            return 0;
        }
        return $value * $array[$key] / array_sum($array);
    }
}