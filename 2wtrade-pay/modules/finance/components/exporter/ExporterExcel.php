<?php

namespace app\modules\finance\components\exporter;

use app\modules\finance\components\EstimateItem;
use app\modules\finance\models\ReportExpensesCountry;
use PHPExcel;
use PHPExcel_Style_Alignment;
use PHPExcel_Writer_Excel2007;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ExporterRecordExcel
 * @package app\modules\finance\components\exporter
 */
class ExporterExcel extends Exporter
{
    /**
     * @var string
     */
    protected static $type = self::TYPE_EXCEL;

    /**
     * @var ReportExpensesCountry
     */
    public $country;

    /**
     * @var string
     */
    public $date;

    /**
     * @var array
     */
    public $tables = [];

    /**
     * @var array
     */
    public $groupCollection = [];

    /**
     * @var array
     */
    public $itemsNames = [];

    /**
     * @param array $table
     * @return array
     */
    private function prepareTable($table)
    {
        if ($table['orientation'] == 'normal') {
            $models = $table['dataProvider']->getModels();
            $data = [];
            foreach ($this->groupCollection as $id => $name) {
                foreach ($models as $key => $model) {
                    $value = ArrayHelper::getValue($model['detailValues'], $id);
                    $data[$id][$key] = $value;
                }
            }
            foreach ($models as $key => $model) {
                $this->itemsNames[$key] = $model['name'];
                $data['all'][$key] = $model['total'];
            }
            $table['dataProvider']->setModels($data);
            $table['dataProvider']->setKeys(array_keys($data));
        } elseif ($table['orientation'] == 'inversion') {
            $models = $table['dataProvider']->getModels();
            $prev = false;
            foreach ($models as $key => $model) {
                if ($prev !== false) {
                    $models[$prev]['lastInGroup'] = $models[$prev]['group'] !== $model['group'];
                }
                $prev = $key;
            }
            if ($table['tableType'] == 'estimate') {
                $total = null;
                foreach ($models as $model) {
                    if ($model['code'] == EstimateItem::COSTS) {
                        $total = $model['total'];
                        break;
                    }
                }
                foreach ($models as &$model) {
                    $model['total_percent'] = ($model['total'] && $total) ? round($model['total'] / $total * 100, 2) : null;
                } unset($model);
            }

            $table['dataProvider']->setModels($models);
        }

        return $table;
    }

    /**
     * @param array $table
     * @return array
     */
    private function buildTable($table)
    {
        $headers = [];
        $rows = [];
        if ($table['orientation'] == 'normal') {
            $headers[] = Yii::t('common', 'Товар');
            foreach ($this->itemsNames as $id => $name) {
                $headers[] = $name;
            }

            foreach ($table['dataProvider']->getModels() as $key => $model) {
                $columns = [];
                $columns[] = ($key == 'all') ? Yii::t('common', 'Итого') : $this->groupCollection[$key];
                foreach ($this->itemsNames as $id => $name) {
                    $value = ArrayHelper::getValue($model, $id);
                    if (is_numeric($value)) {
                        $decPoint = ((int) $value == $value) ? 0 : 2;
                        $value = number_format($value, $decPoint, '.', '');
                    }
                    $columns[] = $value;
                }
                $rows[] = $columns;
            }

        } elseif ($table['orientation'] == 'inversion') {

            $headers[] = Yii::t('common', 'Название');
            foreach ($this->groupCollection as $id => $name) {
                $headers[] = $name;
            }
            $headers[] = Yii::t('common', 'Итого');
            $headers[] = Yii::t('common', 'Итого (%)');

            foreach ($table['dataProvider']->getModels() as $key => $model) {
                $columns = [];
                $columns[] = $model['name'];
                foreach ($this->groupCollection as $id => $name) {
                    $value = ArrayHelper::getValue($model['detailValues'], $id);
                    if (is_numeric($value)) {
                        $decPoint = ((int) $value == $value) ? 0 : 2;
                        $value = number_format($value, $decPoint, '.', '');
                    }
                    $columns[] = $value;
                }

                $value = $model['total'];
                if (is_numeric($value)) {
                    $decPoint = ((int) $value == $value) ? 0 : 2;
                    $value = number_format($value, $decPoint, '.', '');
                }
                $columns[] = $value;
                $columns[] = $model['total_percent'];
                $rows[] = $columns;
            }
        }

        return [
            'headers' => $headers,
            'rows' => $rows,
        ];
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function sendFile()
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(Yii::t('common', $this->country->name));
        $sheet->getSheetView()->setZoomScale(90);

        $titleStyle = [
            'font'  => [
                'bold' => false,
                'size'  => 9,
                'name'  => 'Arial'
            ]
        ];

        $headerStyle = [
            'font'  => [
                'bold' => false,
                'size'  => 11,
                'name'  => 'Calibri'
            ]
        ];


        $current = 1;
        foreach ($this->tables as $table) {
            $data = $this->buildTable($this->prepareTable($table));

            $sheet->setCellValue($this->printAlphabet(0) . ($current), $table['title']);
            $sheet->getStyle($this->printAlphabet(0) . ($current))->applyFromArray($titleStyle);
            $current++;

            $iHead = 0;
            foreach ($data['headers'] as $header) {
                $width = (!$iHead) ? 22.14 : 9.43;

                $sheet->getColumnDimension($this->printAlphabet($iHead))->setWidth($width);
                $sheet->setCellValue($this->printAlphabet($iHead) . ($current), $header);
                if ($iHead) {
                    $sheet->getStyle($this->printAlphabet($iHead) . ($current))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $sheet->getStyle($this->printAlphabet($iHead) . ($current))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
                $sheet->getStyle($this->printAlphabet($iHead) . ($current))->getAlignment()->setWrapText(true);
                $sheet->getStyle($this->printAlphabet($iHead) . ($current))->applyFromArray($headerStyle);
                $iHead++;
            }
            $current++;

            foreach ($data['rows'] as $columns) {
                $i = 0;
                foreach ($columns as $column) {
                    $width = (!$i) ? 22.14 : 9.43;
                    $sheet->getColumnDimension($this->printAlphabet($i))->setWidth($width);
                    $sheet->setCellValue($this->printAlphabet($i) . ($current), $column);
                    if ($i) {
                        $sheet->getStyle($this->printAlphabet($i) . ($current))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    }
                    $sheet->getStyle($this->printAlphabet($i) . ($current))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                    $sheet->getStyle($this->printAlphabet($i) . ($current))->getAlignment()->setWrapText(true);
                    $sheet->getStyle($this->printAlphabet($i) . ($current))->applyFromArray($headerStyle);
                    $i++;
                }
                $current++;
            }
            $current += 5;
        }

        // Заполнение данными

        $filename = 'Export_CurrentBalance_' . date('Y-m-d_H-i-s') . '_' . Yii::t('common',
                $this->country->name) . '_' . $this->date . '_2wtrade.xlsx';

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');

        Yii::$app->end();
    }
}
