<?php

namespace app\modules\finance\components\exporter;

use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class ExporterRecord
 * @package app\modules\finance\components\exporter
 */
abstract class Exporter extends BaseObject
{
    const TYPE_EXCEL = 'excel';

    /** @var ActiveDataProvider */
    public $dataProvider;

    /**
     * @var string
     */
    protected static $type;

    abstract public function sendFile();

    /**
     * @return string
     */
    public static function getExporterUrl()
    {
        $queryParams = Yii::$app->request->getQueryParams();
        $queryParams['export'] = static::$type;
        $url = array_merge([''], $queryParams);

        return Url::to($url);
    }

    /**
     * @param $key
     * @return string
     */
    protected function printAlphabet($key)
    {
        $array = [];
        $range = range(65, 90);

        foreach ($range as $letter) {
            $array[] = chr($letter);
        }

        $str = "";
        do {
            $index = $key % 25;
            $str = $array[$index] . $str;
            $key /= 25;
            $key = intval($key) - 1;
        } while ($key >= 0);

        return $str;
    }

    /**
     * @param $dataCell
     * @return mixed|string
     */
    protected function prepareMapping($dataCell)
    {
        if ($dataCell) {
            return $dataCell;
        }
        return "";
    }
}
