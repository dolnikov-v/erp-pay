<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesCountryCategory;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\finance\models\ReportExpensesCountryItemProduct;
use app\modules\finance\models\ReportExpensesItem;
use app\modules\salary\models\Office;
use yii\helpers\ArrayHelper;

class EstimateCatalogProvider extends EstimateProvider
{
    /**
     * @var null | ReportExpensesItem
     */
    protected static $expensesItems = null;
    /**
     * @param string $formula
     * @param Office $office
     * @return string
     */
    public function modifyFormula($formula, $office = null)
    {
        if ($office && $office->type == Office::TYPE_CALL_CENTER &&
            $variables = Formula::getVariables($formula)
        ) {
            $pattern = 'call_center_direction.office#office_id#.direction#direction_id#.#itemcode#';
            $replace = [];
            foreach ($variables as $variable) {
                $map = [
                    EstimateItem::CALL_CENTER_OPERATORS_SALARY => 'operators_salary',
                    EstimateItem::CALL_CENTER_STAFF_SALARY => 'staff_salary',
                ];
                if (isset($map[$variable])) {
                    $tmp = $pattern;
                    $tmp = str_replace('#direction_id#', $office->country_id, $tmp);
                    $tmp = str_replace('#office_id#', $office->id, $tmp);
                    $tmp = str_replace('#itemcode#', $map[$variable], $tmp);
                    $replace[Formula::prepareVariable($variable)] = Formula::prepareVariable($tmp);
                }
            }
            if ($replace) {
                $formula = str_replace(array_keys($replace), $replace, $formula);
            }
        }

        return $formula;
    }

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $expensesItems = static::getExpensesItems();

        $expensesCountryItems = [];
        $expensesCountryCategories = [];
        $categories = [];
        if ($this->country && $this->country->id) {
            $expensesCountryCategories = ReportExpensesCountryCategory::find()
                ->byCountryId($this->country->id)
                ->active()
                ->indexBy('id')
                ->all();
            $allCategory = [null];
            $categories = [null => null];
            foreach ($expensesCountryCategories as $category) {
                $allCategory[] = $category->id;
                $categories[$category->id] = $category->category_id;
            }

            $expensesCountryItems = ReportExpensesCountryItem::find()
                ->joinWith(['category', 'category.category', 'parent'])
                ->byCountryId($this->country->id)
                ->byCategory($allCategory)
                ->byParentId(ArrayHelper::getColumn($expensesItems, 'id'))
                ->byYearMonth($this->year, $this->month)
                ->all();
        }
        $categoryItems = ArrayHelper::index($expensesCountryItems, 'parent_id', ['category_id']);

        $groupingList = [];
        if ($this->groupBy) {
            $list = [];
            foreach ($expensesCountryItems as $expensesCountryItem) {
                if ($expensesCountryItem->active) {
                    $list[] = $expensesCountryItem->id;
                }
            }
            if ($list) {
                $countryItemProducts = ReportExpensesCountryItemProduct::find()
                    ->where(['item_id' => $list])
                    ->asArray()
                    ->all();
                $groupingList = ArrayHelper::index($countryItemProducts, 'id', ['item_id']);
            }
        }

        $items = [];
        foreach ($categories as $sub_category => $category) {
            foreach ($expensesItems as $key => $expensesItem) {
                /* @var ReportExpensesItem $expensesItem */

                if ($expensesItem->code == ReportExpensesItem::ADCOMBO_PENALTY_ITEM_CODE ||
                    $expensesItem->code == ReportExpensesItem::IP_TELEPHONY) {
                    // пропускаем этот пункт, приклеим позже
                    continue;
                }

                if ($expensesItem->category_id != $category) {
                    continue;
                }
                /* @var ReportExpensesCountryItem $childItem */
                $childItem = null;
                /* @var ReportExpensesCountryCategory $subCategory */
                $subCategory = ArrayHelper::getValue($expensesCountryCategories, $sub_category);
                if (isset($categoryItems[$sub_category][$expensesItem->id])) {
                    $childItem = $categoryItems[$sub_category][$expensesItem->id];
                }
                if (!$childItem || $childItem->active) {
                    $categoryCode = ($subCategory)
                        ? ReportExpensesCountryCategory::fullCode($subCategory)
                        : ReportExpensesCategory::fullCode($category);

                    $item = [
                        'name' => \Yii::t('common', $expensesItem->name),
                        'category' => $categoryCode,
                        'categoryType' => $subCategory ? $subCategory->category->code : null,
                        'categoryEntityId' => $subCategory ? $subCategory->office_id : null,
                    ];
                    $parentCode = self::PROVIDER_CATALOG . '.' . $categoryCode . '.' . $expensesItem->code ?: 'item' . $expensesItem->id;
                    if ($childItem) {
                        $item['code'] = self::PROVIDER_CATALOG . '.' . $categoryCode . '.' . 'item' . $childItem->id;
                        $item['type'] = ($childItem->type == ReportExpensesItem::TYPE_VARIABLE) ? 'formula' : 'regular';
                        $item['value'] = $childItem->value;
                        $item['formula'] = $this->modifyFormula($childItem->formula, $subCategory ? $subCategory->office : null);
                        $item['parent'] = $childItem->parent_id;
                        $item['parentCode'] = $parentCode;
                        if ($this->groupBy && $childItem->parent_id) {
                            $values = [];
                            if ($details = ArrayHelper::getValue($groupingList, $childItem->id)) {
                                $values = ArrayHelper::map($details, 'product_id', 'value');
                            }
                            $item['detailOn'] = true;
                            $item['detailValues'] = $values;
                        }
                    } else {
                        $item['code'] = $parentCode;
                        $item['type'] = ($expensesItem->type == ReportExpensesItem::TYPE_VARIABLE) ? 'formula' : 'regular';
                        $item['value'] = $expensesItem->value;
                        $item['formula'] = $this->modifyFormula($expensesItem->formula, $subCategory ? $subCategory->office : null);
                        $item['parent'] = null;
                    }
                    $items[] = $item;
                }
            }
        }

        return $items;
    }

    /**
     * @return ReportExpensesItem|null
     */
    protected static function getExpensesItems()
    {
        if(is_null(static::$expensesItems)) {
            static::$expensesItems = ReportExpensesItem::find()
                ->joinWith(['category'])
                ->active()
                ->orderBy([ReportExpensesCategory::tableName() . '.name' => SORT_ASC])
                ->all();
        }

        return static::$expensesItems;
    }
}