<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesCountryCategory;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use Yii;

class EstimateFactory extends BaseObject
{
    /**
     * @var array
     */
    public $options;

    /**
     * @var EstimateProvider
     */
    public $commonProvider;

    /**
     * @var EstimateCallCenterProvider
     */
    public $callCenterProvider;

    /**
     * @var EstimateCallCenterDirectionProvider
     */
    public $callCenterDirectionProvider;

    /**
     * @var EstimateCatalogProvider
     */
    public $catalogProvider;

    /**
     * @var EstimateFinanceProvider
     */
    public $financeProvider;

    /**
     * @var EstimateDeliveryProvider
     */
    public $deliveryProvider;

    /**
     * @var EstimateOfficeProvider
     */
    public $officeProvider;

    /**
     * @var null|ReportExpensesCategory
     */
    protected static $categories = null;

    /**
     * @var null|ReportExpensesCountryCategory
     */
    protected static $countryCategories = null;

    /**
     * @return EstimateFinanceProvider
     */
    public function getFinanceProvider()
    {
        if (is_null($this->financeProvider)) {
            $this->financeProvider = new EstimateFinanceProvider($this->options);
            $this->financeProvider->factory = $this;
        }

        return $this->financeProvider;
    }

    /**
     * @return EstimateCallCenterProvider
     */
    public function getCallCenterProvider()
    {
        if (is_null($this->callCenterProvider)) {
            $this->callCenterProvider = new EstimateCallCenterProvider($this->options);
            $this->callCenterProvider->factory = $this;
        }

        return $this->callCenterProvider;
    }

    /**
     * @return EstimateCatalogProvider
     */
    public function getCatalogProvider()
    {
        if (is_null($this->catalogProvider)) {
            $this->catalogProvider = new EstimateCatalogProvider($this->options);
            $this->catalogProvider->factory = $this;
        }

        return $this->catalogProvider;
    }

    /**
     * @return EstimateCommonProvider
     */
    public function getCommonProvider()
    {
        if (is_null($this->commonProvider)) {
            $this->commonProvider = new EstimateCommonProvider($this->options);
            $this->commonProvider->factory = $this;
        }

        return $this->commonProvider;
    }

    /**
     * @return EstimateCallCenterDirectionProvider
     */
    public function getCallCenterDirectionProvider()
    {
        if (is_null($this->callCenterDirectionProvider)) {
            $this->callCenterDirectionProvider = new EstimateCallCenterDirectionProvider($this->options);
            $this->callCenterDirectionProvider->factory = $this;
        }

        return $this->callCenterDirectionProvider;
    }

    /**
     * @return EstimateDeliveryProvider
     */
    public function getDeliveryProvider()
    {
        if (is_null($this->deliveryProvider)) {
            $this->deliveryProvider = new EstimateDeliveryProvider($this->options);
            $this->deliveryProvider->factory = $this;
        }

        return $this->deliveryProvider;
    }

    /**
     * @return EstimateOfficeProvider
     */
    public function getOfficeProvider()
    {
        if (is_null($this->officeProvider)) {
            $this->officeProvider = new EstimateOfficeProvider($this->options);
            $this->officeProvider->factory = $this;
        }

        return $this->officeProvider;
    }

    /**
     * @return EstimateItem[]
     */
    public function getItems()
    {
        $items = array_merge(
            $this->getFinanceProvider()->getItems(),
            $this->getCatalogProvider()->getItems(),
            $this->getCallCenterProvider()->getItems(),
//            $this->getCallCenterDirectionProvider()->getItems(),
            $this->getDeliveryProvider()->getItems(),
            $this->getOfficeProvider()->getItems(),
            $this->getCommonProvider()->getItems()
        );
        return ArrayHelper::index($items, 'code');
    }

    /**
     * @param string
     * @return EstimateItem|null
     */
    public function getItem($code)
    {
        $items = $this->getItems();
        return isset($items[$code]) ? $items[$code] : null;
    }

    /**
     * @param string
     * @param string
     * @return EstimateItem|null
     */
    public function findItemByParent($code)
    {
        foreach ($this->getItems() as $item) {
            if ($item->parentCode == $code) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param mixed
     * @return array
     */
    public function getVariablesList()
    {
        $list = [];

        $categoryList = [
            ReportExpensesCategory::CATEGORY_ROOT => Yii::t('common', 'Без категории')
        ];

        $categories = static::getCategories();
        $countryCategories = static::getCountryCategories();
        foreach ($categories as $category) {
            $categoryList[ReportExpensesCategory::fullCode($category)] = $category->name;
        }
        foreach ($countryCategories as $category) {
            $categoryList[ReportExpensesCountryCategory::fullCode($category)] = $category->name;
        }
        $providers = [
            Yii::t('common', 'Финансы') => $this->getFinanceProvider(),
            Yii::t('common', 'Справочник') => $this->getCatalogProvider(),
            Yii::t('common', 'Колл-центр') => $this->getCallCenterProvider(),
            Yii::t('common', 'Доставка') => $this->getDeliveryProvider(),
            Yii::t('common', 'Офисы') => $this->getOfficeProvider(),
            Yii::t('common', 'Общие') => $this->getCommonProvider(),
        ];

        foreach ($providers as $label => $provider) {
            foreach ($provider->getItems() as $item) {
                $name = [];
                if ($label) {
                    $name[] = $label;
                }
                if ($item->category && isset($categoryList[$item->category])) {
                    $name[] = $categoryList[$item->category];
                }
                $name[] = $item->name;

                $list[$item->code] = join(' | ', $name);
            }
        }
        asort($list);

        return $list;
    }

    /**
     * @param string $variable
     * @return array
     */
    public function getIncoherentVariables($variable)
    {
        $list = [];
        $variables = Formula::getIndependentVariables($this->getFormulaList(), $variable);

        if ($variables) {
            $list = $this->getVariablesList();
            foreach ($variables as $v) {
                if (isset($list[$v])) {
                    unset($list[$v]);
                }
            }
            unset($list[$variable]);
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getFormulaList()
    {
        $list = [];
        foreach ($this->getItems() as $item) {
            if ($item->type == 'formula' && $item->formula) {
                $list[$item->code] = $item->formula;
            }
        }
        return $list;
    }

    /**
     * @param array $variables
     * @param int|null
     * @return array
     */
    public function getVariableValues($variables, $detail = null)
    {
        $values = [];
        foreach ($variables as $variable) {
            $value = null;
            if ($item = $this->getItem($variable)) {
                $value = (!is_null($detail)) ? $item->getDetailValue($detail) : $item->getValue();
            }
            $values[$variable] = $value ?: 0;
        }
        return $values;
    }

    /**
     * @return array
     */
    public function getGroupingList()
    {
        $list = [];
        $groupBy = ArrayHelper::getValue($this->options, 'groupBy');
        if ($groupBy == EstimateProvider::GROUP_BY_PRODUCT) {
            $products = [];
            foreach ($this->getFinanceProvider()->getItems() as $item) {
                if ($item->detailValues) {
                    $products = array_merge($products, array_keys($item->detailValues));
                }
            }
            $list = array_unique($products);
        }
        return $list;
    }

    /**
     * @return ReportExpensesCategory|null
     */
    protected static function getCategories()
    {
        if(is_null(static::$categories)) {
            static::$categories = ReportExpensesCategory::find()->all();
        }

        return static::$categories;
    }

    /**
     * @return ReportExpensesCountryCategory|null
     */
    protected static function getCountryCategories()
    {
        if(is_null(static::$countryCategories)) {
            static::$countryCategories =  ReportExpensesCountryCategory::find()->with(['category'])->all();
        }
        return static::$countryCategories;
    }
}