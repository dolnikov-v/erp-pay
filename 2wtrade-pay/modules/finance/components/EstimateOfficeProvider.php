<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\callcenter\models\CallCenter;
use app\modules\report\components\ReportFormSalary;
use app\modules\report\controllers\SalaryController;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\Office;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class EstimateOfficeProvider
 * @package app\modules\finance\components
 */
class EstimateOfficeProvider extends EstimateProvider
{
    const CATEGORY_PARENT_OFFICE = 'office.office#office_id#';
    const CATEGORY_PARENT_OFFICE_DIRECTION = 'office.office#office_id#.direction#direction_id#';

    /**
     * @var Office[]
     */
    private $cacheOffices;

    /**
     * @param int $officeId
     * @param int $directionId
     * @return string
     */
    public function generateCategory($officeId, $directionId = null)
    {
        $pattern = ($directionId) ? self::CATEGORY_PARENT_OFFICE_DIRECTION : self::CATEGORY_PARENT_OFFICE;
        $search = ['#office_id#', '#direction_id#'];
        $replace = [$officeId, $directionId];

        return str_replace($search, $replace, $pattern);
    }

    /**
     * @return Office[]
     */
    public function getOffices()
    {
        if (is_null($this->cacheOffices)) {
            $offices = [];
            if ($this->country && $countryId = $this->country->country_id) {
                $offices = Office::find()
                    ->joinWith(['callCenters'], false)
                    ->andWhere(['or',
                        [
                            CallCenter::tableName() . '.country_id' => $countryId,
                            Office::tableName() . '.type' => Office::TYPE_CALL_CENTER,
                        ],
                        [
                            Office::tableName() . '.country_id' => $countryId,
                            Office::tableName() . '.type' => [Office::TYPE_BRANCH, Office::TYPE_STORAGE]
                        ],
                    ])
                    ->active()
                    ->all();
            }

            $this->cacheOffices = $offices;
        }

        return $this->cacheOffices;
    }

    /**
     * @return array
     */
    public function getForecastData()
    {
        $operatorCount = 0;
        if ($this->forecastParams) {
            $operatorCount = $this->forecastParams['operatorCount'];
        }

        return [
            'operatorCountByDirection' => $operatorCount,
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        $offices = [];
        $callCenters = [];
        $operatorCount = 0;
        $operatorSalary = 0;

        foreach ($this->getOffices() as $office) {

            $monthlyStatement = MonthlyStatement::find()
                ->byOfficeId($office->id)
                ->andWhere(['and',
                    ['date_from' => $this->dateFrom],
                    ['date_to' => $this->dateTo]
                ])
                ->one();

            $reportForm = new ReportFormSalary();
            $params = [
                'from' => $this->dateFrom,
                'to' => $this->dateTo,
                'office' => [$office->id],
            ];
            if ($monthlyStatement) {
                $params['monthlyStatement'] = $monthlyStatement->id;
            }
            $dataProvider = $reportForm->apply(['s' => $params]);

            $totals = SalaryController::generateTotalData($dataProvider, $this->dateFrom, $this->dateTo, $reportForm);
            $total = ArrayHelper::getValue($totals, $office->id);
            if ($total) {

                if ($office->type == Office::TYPE_CALL_CENTER) {

                    foreach ($total['call_centers'] as $id => $callCenter) {
                        $operatorCount += $callCenter['count_opers'];
                        $operatorSalary += $callCenter['operators_salary_tax_usd'];
                        foreach ($callCenter['expenses'] as $expense => $cost) {
                            $callCenters[$office->id][$id][$expense] = $cost;
                        }
                        $callCenters[$office->id][$id]['operators_salary'] = $callCenter['operators_salary_tax_usd'];
                        $callCenters[$office->id][$id]['staff_salary'] = $callCenter['staff_salary_tax_usd'];
                        $callCenters[$office->id][$id]['operator_count'] = $callCenter['count_opers'];
                    }

                } else {
                    foreach ($total['expenses'] as $expense => $cost) {
                        $offices[$office->id][$expense] = $cost;
                    }
                    $offices[$office->id]['staff_salary'] = $total['staff_salary_tax_usd'];
                }

            } else {
                foreach (Office::expensesFields() as $field) {
                    $offices[$office->id][$field] = $office->getExpenseValue($field, $this->dateFrom, $this->dateTo);
                }
                $offices[$office->id]['staff_salary'] = null;
            }
        }

        return [
            'operatorCountByDirection' => $operatorCount,
            'operatorAvgSalaryByDirection' => ($operatorCount) ? $operatorSalary / $operatorCount : 0,
            'callCenters' => $callCenters,
            'offices' => $offices,
        ];
    }

    /**
     * @return array
     */
    public function getFinalData()
    {
        $data = $this->getData();
        $forecastData = ($this->forecastParams) ? $this->getForecastData() : null;
        if ($forecastData) {

            $ts1 = strtotime(date('Y-m-d 00:00:00'));
            $ts2 = strtotime($this->dateTo . ' 00:00:00');
            $forecastDays = ($ts2 - $ts1) / 86400;
            $daysInMonth = date('t', $ts2);

            $defOperatorCount = $forecastData['operatorCountByDirection'] - $data['operatorCountByDirection'];
            $salary = $data['operatorAvgSalaryByDirection'] * $defOperatorCount * ($forecastDays / $daysInMonth);
            $operatorsSalary = 0;
            foreach ($data['callCenters'] as $officeCallCenters) {
                foreach ($officeCallCenters as $callCenter) {
                    if ($data['operatorCountByDirection']) {
                        $callCenter['operators_salary'] += $salary * $callCenter['operator_count'] / $data['operatorCountByDirection'];
                    }
                    if ($callCenter['operators_salary'] < 0) {
                        $callCenter['operators_salary'] = 0;
                    }
                    $operatorsSalary += $callCenter['operators_salary'];
                }
            }
            $operatorAvgSalary = ($forecastData['operatorCountByDirection']) ? $operatorsSalary / $forecastData['operatorCountByDirection'] : 0;

            $data['operatorAvgSalaryByDirection'] = $operatorAvgSalary;
            $data['operatorCountByDirection'] = $forecastData['operatorCountByDirection'];
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $data = $this->getFinalData();

        $ordersApprove = $this->factory->getFinanceProvider()->getItemDetailValues(EstimateItem::FINANCE_ORDERS_APPROVE);

        $items = [];
        $directionCategories = [];

        foreach ($this->getOffices() as $office) {

            if ($office->type == Office::TYPE_CALL_CENTER) {
                if (isset($data['callCenters'][$office->id])) {

                    foreach ($data['callCenters'][$office->id] as $id => $callCenter) {
                        $category = $this->generateCategory($office->id, $id);
                        $directionCategories[] = $category;

                        foreach (Office::expensesFields() as $expense) {
                            $code = $category . '.' . $expense;
                            $items[$code] = [
                                'code' => $code,
                                'type' => 'regular',
                                'name' => $office->getAttributeLabel($expense),
                                'category' => $category,
                                'value' => $callCenter[$expense],
                                'detailOn' => true,
                                'calcValue' => false,
                                'detailValue' => function ($item, $id) use ($callCenter, $expense, $ordersApprove) {
                                    return self::getDivideByPart($callCenter[$expense], $ordersApprove, $id);
                                },
                            ];
                        }

                        $expense = 'operators_salary';
                        $code = $category . '.' . $expense;
                        $items[$code] = [
                            'code' => $code,
                            'type' => 'regular',
                            'name' => Yii::t('common', 'Зарплата операторов'),
                            'category' => $category,
                            'value' => $callCenter['operators_salary'],
                            'detailOn' => true,
                            'calcValue' => false,
                            'detailValue' => function ($item, $id) use ($callCenter, $ordersApprove) {
                                return self::getDivideByPart($callCenter['operators_salary'], $ordersApprove, $id);
                            },
                        ];

                        $expense = 'staff_salary';
                        $code = $category . '.' . $expense;
                        $items[$code] = [
                            'code' => $code,
                            'type' => 'regular',
                            'name' => Yii::t('common', 'Зарплата персонала'),
                            'category' => $category,
                            'value' => $callCenter['staff_salary'],
                            'detailOn' => true,
                            'calcValue' => false,
                            'detailValue' => function ($item, $id) use ($callCenter, $ordersApprove) {
                                return self::getDivideByPart($callCenter['staff_salary'], $ordersApprove, $id);
                            },
                        ];
                    }

                }
            } else {
                $expenses = $data['offices'][$office->id];
                $category = $this->generateCategory($office->id);

                foreach (Office::expensesFields() as $expense) {
                    $code = $category . '.' . $expense;
                    $items[$code] = [
                        'code' =>  $code,
                        'type' => 'regular',
                        'name' => $office->getAttributeLabel($expense),
                        'category' => $category,
                        'value' => $expenses[$expense],
                        'detailOn' => true,
                        'calcValue' => false,
                        'detailValue' => function ($item, $id) use ($expenses, $expense, $ordersApprove) {
                            return self::getDivideByPart($expenses[$expense], $ordersApprove, $id);
                        },
                    ];
                }

                $expense = 'staff_salary';
                $code = $category . '.' . $expense;
                $items[$code] = [
                    'code' =>  $code,
                    'type' => 'regular',
                    'name' => Yii::t('common', 'Зарплата персонала'),
                    'category' => $category,
                    'value' => $expenses['staff_salary'],
                    'detailOn' => true,
                    'calcValue' => false,
                    'detailValue' => function ($item, $id) use ($expenses, $ordersApprove) {
                        return self::getDivideByPart($expenses['staff_salary'], $ordersApprove, $id);
                    },
                ];
            }
        }

        $items[EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER] = [
            'code' => EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER,
            'type' => 'regular',
            'name' => Yii::t('common', 'Кол-во операторов на направлении'),
            'value' => $data['operatorCountByDirection'],
        ];

        $directionVariables = [];
        foreach ($directionCategories as $category) {
            $directionVariables[] = Formula::prepareVariable($category . '.costs');
        }

        $items[EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR] = [
            'code' =>  EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR,
            'type' => 'formula',
            'name' => Yii::t('common', 'Ср. расход на оператора на направление'),
            'formula' => (!$directionVariables) ? '' :
                '(' . implode(' + ', $directionVariables) . ') / ' . Formula::prepareVariable(EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER),
        ];

        $items[EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS] = [
            'code' => EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS,
            'name' => Yii::t('common', 'Стоимость всех КЦ'),
            'type' => 'formula',
            'formula' => (!$directionVariables) ? '' : '(' . implode(' + ', $directionVariables) . ')',
        ];

        return $items;
    }
}