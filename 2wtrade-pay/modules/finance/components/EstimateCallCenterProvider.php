<?php

namespace app\modules\finance\components;

use app\models\VoipExpense;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;

class EstimateCallCenterProvider extends EstimateProvider
{

    /**
     * @var array
     */
    private $offices;

    /**
     * @var integer
     */
    private $designationOperatorId;

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        return [
            [
                'code' => EstimateItem::CALL_CENTER_OPERATORS_SALARY,
                'name' => Yii::t('common', 'Зарплата операторов'),
                'type' => 'regular',
                'value' => $this->getOperatorsSalary(),
            ],
            [
                'code' => EstimateItem::CALL_CENTER_STAFF_SALARY,
                'name' => Yii::t('common', 'Зарплата персонала'),
                'type' => 'regular',
                'value' => $this->getStaffSalary(),
            ],
        ];
    }

    /**
     * @return int|null
     */
    protected function getDesignationOperatorId()
    {
        if (is_null($this->designationOperatorId)) {
            $designation = Designation::find()
                ->where(['calc_work_time' => 1])
                ->one();
            $this->designationOperatorId = $designation ? $designation->id : null;
        }
        return $this->designationOperatorId;
    }

    /**
     * @return mixed
     */
    protected function getOperatorsSalary()
    {
        $salary = null;
        if ($this->country &&
            $this->country->country_id &&
            $this->dateFrom &&
            $this->dateTo &&
            $designation = $this->getDesignationOperatorId()
        ) {
            $offices = $this->getOffices();

            if ($officeIds = array_keys($offices)) {
                $persons = Person::find()
                    ->byOfficeId($officeIds)
                    ->andWhere(['designation_id' => $designation])
                    ->andWhere(['or',
                        ['<=', 'start_date', $this->dateTo],
                        ['is', 'start_date', null],
                        ['=', 'start_date', ''],
                    ])
                    ->andWhere(['or',
                        ['>=', 'dismissal_date', $this->dateFrom],
                        ['is', 'dismissal_date', null],
                        ['=', 'dismissal_date', ''],
                    ])
                    ->active()
                    ->all();

                $salary = 0;
                $rate = $this->country->country->currencyRate->rate;
                foreach ($persons as $person) {
                    $office = $offices[$person['office_id']];
                    $workHourInMonth = $office['working_hours_per_week'] * 4.348; // - количество недель в месяце = 4.348
                    // Вычислить долю отработанного времени
                    $from = ($this->dateFrom > $person->start_date) ? $this->dateFrom : $person->start_date;
                    $to = ($this->dateTo < $person->dismissal_date || !$person->dismissal_date) ? $this->dateTo : $person->dismissal_date;
                    $workTime = strtotime($to) - strtotime($from);
                    $allTime = strtotime($this->dateTo) - strtotime($this->dateFrom);
                    $part = ($workTime / $allTime);

                    $personSalary = 0;
                    if ($office['calc_salary_usd']) {
                        if ($person->salary_usd) {
                            $personSalary = $person->salary_usd;
                        } elseif ($person->salary_hour_usd) {
                            $personSalary = $person->salary_hour_usd * $workHourInMonth;
                        }
                    }
                    if ($office['calc_salary_local'] && $rate) {
                        if ($person->salary_local) {
                            $personSalary = $person->salary_local / $rate;
                        } elseif ($person->salary_hour_local) {
                            $personSalary = $person->salary_hour_local * $workHourInMonth / $rate;
                        }
                    }
                    $salary += $personSalary * $part;
                }
            }
        }

        return $salary;
    }

    /**
     * @return mixed
     */
    protected function getStaffSalary()
    {
        $salary = null;
        if ($this->country &&
            $this->country->country_id &&
            $this->dateFrom &&
            $this->dateTo
        ) {
            $offices = $this->getOffices();

            if ($officeIds = array_keys($offices)) {
                $persons = Person::find()
                    ->byOfficeId($officeIds)
                    ->andWhere(['<>', 'designation_id', $this->getDesignationOperatorId()])
                    ->andWhere(['or',
                        ['<=', 'start_date', $this->dateTo],
                        ['is', 'start_date', null],
                        ['=', 'start_date', ''],
                    ])
                    ->andWhere(['or',
                        ['>=', 'dismissal_date', $this->dateFrom],
                        ['is', 'dismissal_date', null],
                        ['=', 'dismissal_date', ''],
                    ])
                    ->active()
                    ->all();

                $salary = 0;
                $rate = $this->country->country->currencyRate->rate;

                foreach ($persons as $person) {
                    $office = $offices[$person['office_id']];
                    $workHourInMonth = $office['working_hours_per_week'] * 4.348; // - количество недель в месяце = 4.348
                    // Вычислить долю отработанного времени
                    $from = ($this->dateFrom > $person->start_date) ? $this->dateFrom : $person->start_date;
                    $to = ($this->dateTo < $person->dismissal_date || !$person->dismissal_date) ? $this->dateTo : $person->dismissal_date;
                    $workTime = strtotime($to) - strtotime($from);
                    $allTime = strtotime($this->dateTo) - strtotime($this->dateFrom);
                    $part = ($workTime / $allTime);

                    $personSalary = 0;
                    if ($office['calc_salary_usd']) {
                        if ($person->salary_usd) {
                            $personSalary = $person->salary_usd;
                        } elseif ($person->salary_hour_usd) {
                            $personSalary = $person->salary_hour_usd * $workHourInMonth;
                        }
                    }
                    if ($office['calc_salary_local'] && $rate) {
                        if ($person->salary_local) {
                            $personSalary = $person->salary_local / $rate;
                        } elseif ($person->salary_hour_local) {
                            $personSalary = $person->salary_hour_local * $workHourInMonth / $rate;
                        }
                    }
                    $salary += $personSalary * $part;
                }
            }
        }

        return $salary;
    }

    /**
     * @return Office[]|array
     */
    protected function getOffices()
    {
        if (is_null($this->offices)) {
            $this->offices = Office::find()
                ->select([
                    'id',
                    'calc_salary_usd',
                    'calc_salary_local',
                    'working_hours_per_week',
                ])
                ->byCountryId($this->country->country_id)
                ->active()
                ->indexBy('id')
                ->asArray()
                ->all();
        }

        return $this->offices;
    }
}