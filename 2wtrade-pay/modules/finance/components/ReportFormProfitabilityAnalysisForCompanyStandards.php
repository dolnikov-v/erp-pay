<?php
namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\models\ReportExpensesCountryCategory;
use app\modules\salary\models\Office;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class
 * @package app\modules\finance\components
 */
class ReportFormProfitabilityAnalysisForCompanyStandards extends ReportFormProfitabilityAnalysis
{
    /**
     * @param EstimateFactory $factory
     * @param array $data
     * @param array $variables
     * @return array
     */
    protected function prepareDate($factory, $data, $variables)
    {
        $rows = array_filter($data, function ($row) {
            return (!empty($row['code'])) || isset($row['label']);
        });
        $models = [];
        foreach ($rows as $row) {
            if ($item = $factory->getItem($row['code'])) {
                $model = [
                    'code' => $row['code'],
                    'name' => ArrayHelper::getValue($row, 'label'),
                    'total' => $item->getValue(),
                    'isParent' => ArrayHelper::getValue($row, 'isParent'),
                    'group' => ArrayHelper::getValue($row, 'group'),
                    "descriptions" => $item->getDescriptions(),
                    'totalDescription' => $item->getTotalDescription(),
                    'title' => ($item->type == 'formula') ? Formula::getFormattedFormula($item->formula, $variables, false) : null,
                ];
                if ($this->groupBy && $collection = $this->getGroupingCollection()) {
                    $values = $item->getDetailValues();
                    foreach ($collection as $id => $name) {
                        $model['detailValues'][$id] = ArrayHelper::getValue($values, $id);
                    }
                }
                $models[] = $model;
            }
        }

        return $models;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider[]
     */
    public function apply($params)
    {
        $this->factory = new EstimateFactory(['options' => [
            'month' => $params['month'],
            'year' => $params['year'],
            'country' => ReportExpensesCountry::findOne(['id' => $params['country_id']]),
            'groupBy' => $this->groupBy,
            'isForecastPeriod' => $params['isForecastPeriod'],
            'forecastParams' => ($params['isForecastPeriod']) ? $params['forecastParams'] : [],
        ]]);

        $variables = ArrayHelper::map(
            array_filter($this->geRows(), function ($row) {
                return (!empty($row['code'])) || isset($row['label']);
            }),
            'code', 'label');

        $variables = array_merge($variables, [
            EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS => Yii::t('common', 'Затраты на КЦ'),
            EstimateItem::EXPENSES => Yii::t('common', 'Затраты'),
        ]);

        $providers = $this->prepareDate($this->factory, $this->geRows(), $variables);

        return $providers;
    }

    /**
     * @return array
     */
    private function geRows()
    {
        $rows = [];
        $rows[] = [
            'code' => EstimateItem::FINANCE_BUYOUT_INCOME,
            'label' => Yii::t('common', 'Доход'),
        ];
        $rows[] = [
            'code' => EstimateItem::COSTS,
            'label' => Yii::t('common', 'Расходы'),
        ];

        $categories = [];
        $country = $this->factory->options['country'];
        if ($country) {
            $countryCategories = ReportExpensesCountryCategory::find()
                ->joinWith(['office', 'category'])
                ->byCountryId($country->id)
                ->active()
                ->all();

            $tmp = [];
            foreach ($countryCategories as $category) {
                if ($category->office && $category->office->type == Office::TYPE_CALL_CENTER) {
                    continue;
                }
                $code = $category->getFullCode();
                $tmp[$code] = [
                    'code' => 'catalog.category.' . $code . '.costs',
                    'label' => Yii::t('common', $category->name ?: $category->category->name),
                    'items' => [],
                    'detailOn' => true,
                    'calcValue' => false,
                ];
            }
            $categories = $tmp;
        }
        foreach ($this->factory->getCatalogProvider()->getItems() as $item) {
            if ($item->category && isset($categories[$item->category])) {
                $categories[$item->category]['items'][] = [
                    'code' => $item->code,
                    'label' => $item->name,
                    'group' => $item->category,
                ];
            } elseif ($item->category == 'root') {
                $rows[] = [
                    'code' => $item->code,
                    'label' => $item->name,
                    'group' => null,
                ];
            }
        }

        foreach ($categories as $key => &$category) {
            if (in_array('product', explode('.', $key))) {

                $productCostsCode = '';

                foreach ($this->factory->getItems() as $item) {
                    if ($item->getRealCode() == EstimateItem::PRODUCT_COSTS) {
                        $productCostsCode = $item->code;
                    }
                }

                if ($productCostsCode) {
                    $category['items'][] = [
                        'code' => $productCostsCode,
                        'label' => Yii::t('common', 'Стоимость товаров'),
                        'group' => $key,
                    ];
                }
            }
        } unset($category);

        $provider = $this->factory->getOfficeProvider();
        $itemGroups = ArrayHelper::index($provider->getItems(), 'code', ['category']);
        foreach ($provider->getOffices() as $office) {

            if ($office->type == Office::TYPE_CALL_CENTER) {
                foreach ($office->callCenters as $callCenter) {
                    if ($callCenter->country_id != $provider->country->country_id) {
                        continue;
                    }

                    $categoryCode = $provider->generateCategory($office->id, $callCenter->id);
                    if ($subItems = $itemGroups[$categoryCode] ?? null) {
                        $categories[$categoryCode] = [
                            'code' => $categoryCode . '.costs',
                            'label' => Yii::t('common', 'Стоимость {office_name}', [
                                'office_name' => Yii::t('common', $office->name),
                            ]),
                            'items' => [],
                        ];

                        foreach ($subItems as $item) {
                            $categories[$categoryCode]['items'][] = [
                                'code' => $item->code,
                                'label' => $item->name,
                                'group' => $categoryCode,
                            ];
                        }
                    }
                }
            }
        }

        foreach ($categories as $name => $category) {
            if ($category['items']) {
                $rows[] = [
                    'code' => $category['code'],
                    'label' => $category['label'],
                    'group' => $name,
                    'isParent' => true,
                    'detailOn' => true,
                    'calcValue' => false,
                ];
                $rows = array_merge($rows, $category['items']);
            }
        }
        $rows[] = [
            'code' => EstimateItem::LEAD_COSTS,
            'label' => Yii::t('common', 'Стоимость рекламы'),
        ];

        $rows[] = [
            'code' => EstimateItem::INCOME,
            'label' => Yii::t('common', 'Прибыль'),
        ];
        $rows[] = [
            'code' => EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS,
            'label' => Yii::t('common', 'Затраты на КЦ'),
        ];

        return $rows;
    }
}
