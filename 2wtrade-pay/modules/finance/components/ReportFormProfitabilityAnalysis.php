<?php
namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductLinkCategory;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\models\ReportExpensesCountryCategory;
use app\modules\salary\models\Office;
use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class
 * @package app\modules\finance\components
 */
class ReportFormProfitabilityAnalysis extends Component
{
    /**
     * @var array
     */
    private $groupCollection;

    /**
     * @var array
     */
    private $categoryCollection;

    /**
     * @var EstimateFactory
     */
    public $factory;

    /**
     * @var string
     */
    public $groupBy = 'product';

    /**
     * @var string
     */
    public $panelAlert;

    /**
     * @var string
     */
    public $panelAlertStyle;

    /**
     * @return array
     */
    public function getGroupingCollection()
    {
        if (is_null($this->groupCollection)) {
            $collection = [];
            if ($this->groupBy == 'product') {
                $collection = Product::find()
                    ->select(['id' => Product::tableName() . '.id', 'name' => Product::tableName() . '.name'])
                    ->leftJoin(ProductLinkCategory::tableName(), ProductLinkCategory::tableName() . '.product_id = ' . Product::tableName() . '.id')
                    ->leftJoin(ProductCategory::tableName(), ProductCategory::tableName() . '.id = ' . ProductLinkCategory::tableName() . '.category_id')
                    ->where([Product::tableName() . '.id' => $this->factory->getGroupingList()])
                    ->orderBy(['ISNULL(' . ProductCategory::tableName() . '.name' . ')' => '', ProductCategory::tableName() . '.name' => SORT_ASC, 'name' => SORT_ASC])
                    ->collection();
            }
            $this->groupCollection = $collection;
        }

        return $this->groupCollection;
    }

    /**
     * @return array
     */
    public function getCategoryCollection(): array
    {
        if (is_null($this->groupCollection)) {
            $this->groupCollection = $this->getGroupingCollection();
        }

        if (is_null($this->categoryCollection)) {
            $collection = [];
            if ($this->groupBy == 'product') {
                $collection = ProductLinkCategory::find()
                    ->select(['id' => 'product_id', 'name'])
                    ->leftJoin(ProductCategory::tableName(), ProductCategory::tableName() . '.id = ' . ProductLinkCategory::tableName() . '.category_id')
                    ->where(['product_id' => array_keys($this->groupCollection)])
                    ->asArray()
                    ->all();

                $collection = ArrayHelper::map($collection, 'id', 'name');
            }

            $this->categoryCollection = $collection;
        }

        return $this->categoryCollection;
    }

    /**
     * @param array $models
     * @return array
     */
    public function getCategoryItems(array $models): array
    {
        $productCategories = [];
        $categoryItems = [];
        $data = [];

        foreach ($this->groupCollection as $id => $name) {
            foreach ($models as $key => $model) {
                $value = ArrayHelper::getValue($model['detailValues'], $id);
                $data[$id][$model['code']] = $value;
            }
        }

        foreach ($data as $key => $row) {
            $productCategories[$this->categoryCollection[$key]][] = $row;
        }

        foreach ($productCategories as $key => $productCategory) {
            foreach ($productCategory as $row) {
                foreach ($row as $column => $value) {
                    if (!in_array($column, [EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT, EstimateItem::FINANCE_ORDERS_REFUSE_PERCENT, EstimateItem::FINANCE_ORDERS_IN_PROCESS_PERCENT, EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED_PERCENT])) {
                        $categoryItems[$key][$column] = ($categoryItems[$key][$column] ?? 0) + $value;
                    }
                }
            }
            $categoryItems[$key][EstimateItem::FINANCE_AVG_LEAD_PRICE] /= count($productCategory);
            $categoryItems[$key][EstimateItem::FINANCE_AVG_BUYOUT_CHECK] /= count($productCategory);
            $categoryItems[$key][EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT] = $categoryItems[$key][EstimateItem::FINANCE_ORDERS_BUYOUT] / $categoryItems[$key][EstimateItem::FINANCE_ORDERS_APPROVE] * 100;
            $categoryItems[$key][EstimateItem::FINANCE_ORDERS_REFUSE_PERCENT] = $categoryItems[$key][EstimateItem::FINANCE_ORDERS_REFUSE] / $categoryItems[$key][EstimateItem::FINANCE_ORDERS_APPROVE] * 100;
            $categoryItems[$key][EstimateItem::FINANCE_ORDERS_IN_PROCESS_PERCENT] = $categoryItems[$key][EstimateItem::FINANCE_ORDERS_IN_PROCESS] / $categoryItems[$key][EstimateItem::FINANCE_ORDERS_APPROVE] * 100;
            $categoryItems[$key][EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED_PERCENT] = $categoryItems[$key][EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED] / $categoryItems[$key][EstimateItem::FINANCE_ORDERS_BUYOUT] * 100;

            foreach ($categoryItems[$key] as $item => $value) {
                if (is_numeric($value) and !is_nan($value) and !is_infinite($value)) {
                    $decPoint = ((int)$value == $value) ? 0 : 2;
                    $categoryItems[$key][$item] = number_format($value, $decPoint, '.', ' ');
                } elseif (is_nan($value) or is_infinite($value)) {
                    $categoryItems[$key][$item] = "";
                }
            }
        }

        return $categoryItems;
    }

    /**
     * @param EstimateFactory $factory
     * @param array $data
     * @param array $variables
     * @return ArrayDataProvider
     */
    protected function prepareDate($factory, $data, $variables)
    {
        $rows = array_filter($data, function ($row) {
            return (!empty($row['code'])) || isset($row['label']);
        });
        $models = [];
        foreach ($rows as $row) {
            if ($item = $factory->getItem($row['code'])) {
                $model = [
                    'code' => $row['code'],
                    'name' => ArrayHelper::getValue($row, 'label'),
                    'total' => $item->getValue(),
                    'isParent' => ArrayHelper::getValue($row, 'isParent'),
                    'group' => ArrayHelper::getValue($row, 'group'),
                    "descriptions" => $item->getDescriptions(),
                    'totalDescription' => $item->getTotalDescription(),
                    'title' => ($item->type == 'formula') ? Formula::getFormattedFormula($item->formula, $variables, false) : null,
                ];
                if ($this->groupBy && $collection = $this->getGroupingCollection()) {
                    $values = $item->getDetailValues();
                    foreach ($collection as $id => $name) {
                        $model['detailValues'][$id] = ArrayHelper::getValue($values, $id);
                    }
                }
                $models[] = $model;
            }
        }

        return new ArrayDataProvider(['models' => $models]);
    }

    /**
     * @param array $params
     * @return ArrayDataProvider[]
     */
    public function apply($params)
    {
        $this->factory = new EstimateFactory(['options' => [
            'month' => $params['month'],
            'year' => $params['year'],
            'country' => ReportExpensesCountry::findOne(['id' => $params['country_id']]),
            'groupBy' => $this->groupBy,
            'isForecastPeriod' => $params['isForecastPeriod'],
            'forecastParams' => ($params['isForecastPeriod']) ? $params['forecastParams'] : [],
        ]]);
        $variables = [];
        foreach ($this->getTables() as $tableName => $rows) {
            $rows = array_filter($rows, function ($row) {
                return (!empty($row['code'])) || isset($row['label']);
            });
            $variables = array_merge($variables, ArrayHelper::map($rows, 'code', 'label'));

            // добавляется чтобы в формулах подстановка была:
            $variables = array_merge($variables, [
                EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS => Yii::t('common', 'Затраты на КЦ'),
                EstimateItem::EXPENSES => Yii::t('common', 'Затраты'),
            ]);
        }

        $providers = [];
        foreach ($this->getTables() as $tableName => $rows) {
            $providers[$tableName] = $this->prepareDate($this->factory, $rows, $variables);
        }

//        Костылище во избежание повторения запроса. В новой версии ОБ переделается.
//        ------------>
        if (isset($providers['design']) && isset($providers['estimate']) && !empty($providers['finance'])) {
            /* @var ArrayDataProvider[] $providers  */
            $models = $providers['design']->getModels();
            $approves = [];
            foreach ($providers['finance']->getModels() as $finance)
            {
                if ($finance['code'] == EstimateItem::FINANCE_ORDERS_APPROVE) {
                    $approves = $finance['detailValues'];
                    break;
                }
            }
            foreach ($models as &$model) {
                if ($model['code'] == EstimateItem::COST_DELIVERY) {
                    foreach ($providers['estimate']->getModels() as $estimate) {
                        if (preg_match("/delivery\.delivery\w+\.costs/i", $estimate['code'])) {
                            $model['title'] = "({$estimate['title']}) / " . Yii::t('common', 'Кол-во апрувленных лидов');
                            foreach ($estimate['detailValues'] as $productID => $count) {
                                $model['detailValues'][$productID] += !empty($approves[$productID]) ? $count / $approves[$productID] : 0;
                            }
                        }
                    }
                    $total = $count = 0;
                    foreach ($model['detailValues'] as $val) {
                        if (!empty($val)) {
                            $total += $val;
                            $count++;
                        }
                    }
                    $model['total'] = !empty($count) ? $total / $count : 0;
                    break;
                }
            }
            $providers['design']->setModels($models);
        }
//        <------------

        $month = ($params['month'] - 1) ?: 12;
        $year = $params['year'] - intval($month == 12);
        $factory = new EstimateFactory(['options' => [
            'month' => $month,
            'year' => $year,
            'country' => ReportExpensesCountry::findOne(['id' => $params['country_id']]),
            'groupBy' => $this->groupBy,
        ]]);

        $rows = array_filter($this->getPrevMonthColumns(), function ($row) {
            return (!empty($row['code'])) || isset($row['label']);
        });
        $variables = ArrayHelper::map($rows, 'code', 'label');

        // добавляется чтобы в формулах подстановка была:
        $variables = array_merge($variables, [
            EstimateItem::FINANCE_AVG_BUYOUT_CHECK => Yii::t('common', 'Средний выкупной чек'),
            EstimateItem::FINANCE_ORDERS_APPROVE => Yii::t('common', 'Количество апрувнутых заказов'),
        ]);
        $providers['prev_month'] = $this->prepareDate($factory, $rows, $variables);

        return $providers;
    }

    /**
     * @return array
     */
    private function getFinanceColumns()
    {
        $columns = [];
        $columns[] = [
            'code' => EstimateItem::FINANCE_LEAD_COUNT,
            'label' => Yii::t('common', 'Кол-во лидов всего'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_APPROVE,
            'label' => Yii::t('common', 'Кол-во апрувленных лидов'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_DELIVERY,
            'label' => Yii::t('common', 'Кол-во лидов переданных в КС'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_PRODUCT_QUANTITY,
            'label' => Yii::t('common', 'Кол-во товаров'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY,
            'label' => Yii::t('common', 'Кол-во выкупленного товара'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY,
            'label' => Yii::t('common', 'Кол-во отказного товара'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_AVG_LEAD_PRICE,
            'label' => Yii::t('common', 'Цена за лид'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_AVG_BUYOUT_CHECK,
            'label' => Yii::t('common', 'Выкупной чек'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_BUYOUT,
            'label' => Yii::t('common', 'Выкуп'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT,
            'label' => Yii::t('common', '%'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_REFUSE,
            'label' => Yii::t('common', 'Отказ'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_REFUSE_PERCENT,
            'label' => Yii::t('common', '%'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_IN_PROCESS,
            'label' => Yii::t('common', 'В процессе'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_IN_PROCESS_PERCENT,
            'label' => Yii::t('common', '%'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED,
            'label' => Yii::t('common', 'Денег получено'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED_PERCENT,
            'label' => Yii::t('common', '%'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_BUYOUT_INCOME,
            'label' => Yii::t('common', 'Доход'),
        ];
        $columns[] = [
            'code' => EstimateItem::COSTS,
            'label' => Yii::t('common', 'Расходы'),
        ];
//        $columns[] = [
//            'code' => EstimateItem::FINANCE_PROFITABILITY,
//            'label' => Yii::t('common', 'Доходность'),
//        ];
        return $columns;
    }

    /**
     * @return array
     */
    private function getEstimateRows()
    {
        $rows = [];
        $rows[] = [
            'code' => EstimateItem::COSTS,
            'label' => Yii::t('common', 'Расходы'),
        ];
        $categories = [];

        $country = $this->factory->options['country'];
        if ($country) {
            $countryCategories = ReportExpensesCountryCategory::find()
                ->joinWith(['office', 'category'])
                ->byCountryId($country->id)
                ->active()
                ->all();

            $tmp = [];
            foreach ($countryCategories as $category) {
                if ($category->office && $category->office->type == Office::TYPE_CALL_CENTER) {
                    continue;
                }
                $code = $category->getFullCode();
                $tmp[$code] = [
                    'code' => 'catalog.category.' . $code . '.costs',
                    'label' => Yii::t('common', $category->name ?: $category->category->name),
                    'items' => [],
                    'detailOn' => true,
                    'calcValue' => false,
                ];
            }
            $categories = $tmp;
        }
        foreach ($this->factory->getCatalogProvider()->getItems() as $item) {
            if ($item->category && isset($categories[$item->category])) {
                $categories[$item->category]['items'][] = [
                    'code' => $item->code,
                    'label' => $item->name,
                    'group' => $item->category,
                ];
            } elseif ($item->category == 'root') {
                $rows[] = [
                    'code' => $item->code,
                    'label' => $item->name,
                    'group' => null,
                ];
            }
        }
        if ($country && $country->country) {
//            $offices = Office::find()
//                ->joinWith(['callCenters'], false)
//                ->where([
//                    CallCenter::tableName() . '.active' => 1,
//                    CallCenter::tableName() . '.country_id' => $country->country_id,
//                    Office::tableName() . '.type' => Office::TYPE_CALL_CENTER,
//                ])
//                ->all();
//
//            foreach ($offices as $office) {
//                $expensesCountry = ReportExpensesCountry::findOne(['country_id' => $office->country_id]);
//                if ($expensesCountry) {
//                    $pattern = 'call_center_direction.office#office_id#.direction#direction_id#';
//                    $tmp = str_replace('#direction_id#', $office->country_id, $pattern);
//                    $code = str_replace('#office_id#', $office->id, $tmp);
//                    $categories[$code] = [
//                        'code' => $code . '.costs',
//                        'label' => Yii::t('common', 'Стоимость {office_name} - направление {country_name}', [
//                            'office_name' => $office->name,
//                            'country_name' => $expensesCountry->country->name,
//                        ]),
//                        'items' => [],
//                    ];
//
//                    $options = $this->factory->options;
//                    $options['country'] = $expensesCountry;
//                    $factory = new EstimateFactory(['options' => $options]);
//                    foreach ($factory->getCallCenterDirectionProvider()->getItems() as $item) {
//                        if (isset($categories[$item->category]) && $item->category) {
//                            $categories[$item->category]['items'][] = [
//                                'code' => $item->code,
//                                'label' => $item->name,
//                                'group' => $item->category,
//                            ];
//                        }
//                    }
//                }
//            }

            // Курьерки
            if ($deliveries = $this->factory->getDeliveryProvider()->getDeliveryList()) {
                foreach ($deliveries as $id => $name) {
                    $code = EstimateProvider::PROVIDER_DELIVERY . '.delivery' . $id;
                    $categories[$code] = [
                        'code' => $code . '.costs',
                        'label' => Yii::t('common', 'Стоимость курьерской службы {delivery_name}', [
                            'delivery_name' => $name,
                        ]),
                        'items' => [],
                    ];
                }
                foreach ($this->factory->getDeliveryProvider()->getItems() as $item) {
                    if (isset($categories[$item->category]) && $item->category) {
                        $categories[$item->category]['items'][] = [
                            'code' => $item->code,
                            'label' => $item->name,
                            'group' => $item->category,
                        ];
                    }
                }
            }
        }

        foreach ($categories as $key => &$category) {
            if (in_array('product', explode('.', $key))) {

                $productCostsCode = '';
                $productSpoilageCode = '';

                foreach ($this->factory->getItems() as $item) {
                    if ($item->getRealCode() == EstimateItem::PRODUCT_COSTS) {
                        $productCostsCode = $item->code;
                    }
                    if ($item->getRealCode() == EstimateItem::PRODUCT_SPOILAGE) {
                        $productSpoilageCode = $item->code;
                    }
                }

                if ($productCostsCode) {
                    $category['items'][] = [
                        'code' => $productCostsCode,
                        'label' => Yii::t('common', 'Стоимость товаров'),
                        'group' => $key,
                    ];
                }
                if ($productSpoilageCode) {
                    $category['items'][] = [
                        'code' => $productSpoilageCode,
                        'label' => Yii::t('common', 'Порча / бой'),
                        'group' => $key,
                    ];
                }
            }
        } unset($category);

        $provider = $this->factory->getOfficeProvider();
        $itemGroups = ArrayHelper::index($provider->getItems(), 'code', ['category']);
        foreach ($provider->getOffices() as $office) {

            if ($office->type == Office::TYPE_CALL_CENTER) {

                foreach ($office->callCenters as $callCenter) {
                    if ($callCenter->country_id != $provider->country->country_id) {
                        continue;
                    }

                    $categoryCode = $provider->generateCategory($office->id, $callCenter->id);
                    if ($subItems = $itemGroups[$categoryCode] ?? null) {
                        $categories[$categoryCode] = [
                            'code' => $categoryCode . '.costs',
                            'label' => Yii::t('common', 'Стоимость {office_name}', [
                                'office_name' => Yii::t('common', $office->name),
                            ]),
                            'items' => [],
                        ];

                        foreach ($subItems as $item) {
                            $categories[$categoryCode]['items'][] = [
                                'code' => $item->code,
                                'label' => $item->name,
                                'group' => $categoryCode,
                            ];
                        }
                    }
                }

            } else {

                $categoryCode = $provider->generateCategory($office->id);
                if ($subItems = $itemGroups[$categoryCode] ?? null) {
                    $categories[$categoryCode] = [
                        'code' => $categoryCode . '.costs',
                        'label' => Yii::t('common', 'Стоимость {office_name}', [
                            'office_name' => Yii::t('common', $office->name),
                        ]),
                        'items' => [],
                    ];

                    foreach ($subItems as $item) {
                        $categories[$categoryCode]['items'][] = [
                            'code' => $item->code,
                            'label' => $item->name,
                            'group' => $categoryCode,
                        ];
                    }
                }
            }
        }

        foreach ($categories as $name => $category) {
            if ($category['items']) {
                $rows[] = [
                    'code' => $category['code'],
                    'label' => $category['label'],
                    'group' => $name,
                    'isParent' => true,
                    'detailOn' => true,
                    'calcValue' => false,
                ];
                $rows = array_merge($rows, $category['items']);
            }
        }
//        $rows[] = [
//            'code' => EstimateItem::DELIVERY_COSTS,
//            'label' => Yii::t('common', 'Стоимость курьерской службы'),
//        ];
        $rows[] = [
            'code' => EstimateItem::LEAD_COSTS,
            'label' => Yii::t('common', 'Стоимость рекламы'),
        ];
        $rows[] = [
            'code' => EstimateItem::PENALTY_FOR_LEADS,
            'label' => Yii::t('common', 'Штраф за рекламу'),
        ];
        $rows[] = [
            'code' => EstimateItem::CAll_CENTER_IP_TELEPHONY,
            'label' => Yii::t('common', 'Стоимость IP телефонии'),
        ];

        return $rows;
    }

    /**
     * @return array
     */
    private function getDesignColumns()
    {
        $rows = [];
        $rows[] = [
            'code' => EstimateItem::INCOME,
            'label' => Yii::t('common', 'Прибыль'),
        ];
        $rows[] = [
            'code' => EstimateItem::INCOME_BY_LEAD,
            'label' => Yii::t('common', 'Прибыль на лида'),
        ];
        $rows[] = [
            'code' => EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS,
            'label' => Yii::t('common', 'Затраты на КЦ'),
        ];
        $rows[] = [
            'code' => EstimateItem::COST_APPROVE,
            'label' => Yii::t('common', 'Стоимость апрува'),
        ];
        $rows[] = [
            'code' => EstimateItem::EXPENSES_DELIVERY,
            'label' => Yii::t('common', 'Расходы кроме рекламы, офисов КЦ и телефонии'),
        ];
        $rows[] = [
            'code' => EstimateItem::COST_BUYOUT,
            'label' => Yii::t('common', 'Стоимость выкупа'),
        ];
        $rows[] = [
            'code' => EstimateItem::COST_ORDER_SERVICE,
            'label' => Yii::t('common', 'Стоимость обслуживания заказа'),
        ];
        $rows[] = [
            'code' => EstimateItem::COST_DELIVERY,
            'label' => Yii::t('common', 'Стоимость доставки'),
        ];
        return $rows;
    }

    /**
     * @return array
     */
    private function getPrevMonthColumns()
    {
        $columns = [];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_BUYOUT,
            'label' => Yii::t('common', 'Выкуп'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT,
            'label' => Yii::t('common', '%'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_AVG_LEAD_PRICE,
            'label' => Yii::t('common', 'Цена за лид'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_BUYOUT_INCOME,
            'label' => Yii::t('common', 'Доход'),
        ];
        $columns[] = [
            'code' => EstimateItem::FINANCE_PROFITABILITY,
            'label' => Yii::t('common', 'Доходность'),
        ];
        return $columns;
    }

    /**
     * @return array
     */
    private function getTables()
    {
        $tables = [];
        $tables['finance'] = $this->getFinanceColumns();
        $tables['design'] = $this->getDesignColumns();
        $tables['estimate'] = $this->getEstimateRows();
        $tables['prev_month'] = $this->getPrevMonthColumns();
        return $tables;
    }
}
