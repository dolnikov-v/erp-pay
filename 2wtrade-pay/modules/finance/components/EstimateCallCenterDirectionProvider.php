<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\report\components\ReportFormSalary;
use app\modules\salary\models\Designation;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use yii\helpers\ArrayHelper;

class EstimateCallCenterDirectionProvider extends EstimateProvider
{
    /**
     * @var integer
     */
    private $designationOperatorId;

    /**
     * @return int|null
     */
    protected function getDesignationOperatorId()
    {
        if (is_null($this->designationOperatorId)) {
            $designation = Designation::find()
                ->where(['calc_work_time' => 1])
                ->one();
            $this->designationOperatorId = $designation ? $designation->id : null;
        }
        return $this->designationOperatorId;
    }


    /**
     * @param array $range
     * @param string $step
     * @return array
     */
    public static function rangeToPeriods($range, $step = '+1 day')
    {
        $from = $to = array_shift($range);
        $periods = [];
        foreach ($range as $key => $date) {
            if (strtotime($date) != strtotime($step, strtotime($to))) {
                $periods[] = [$from, $to];
                $from = $date;
            }
            $to = $date;
        }
        $periods[] = [$from, $to];

        return $periods;
    }

    /**
     * @param array $range
     * @param string $from
     * @param string $to
     * @return array
     */
    public static function cutPeriodFromRange($range, $from, $to)
    {
        foreach ($range as $key => $date) {
            if ($from <= $date && $date <= $to) {
                unset($range[$key]);
            }
        }

        return $range;
    }

    /**
     * @param string $from
     * @param string $to
     * @return array
     */
    public static function preparePeriod($from, $to)
    {
        $currentDate = date('Y-m-d');
        $definiteDates = [];
        $forecastDates = [];
        foreach (self::dateRange($from, $to) as $date) {
            if ($date >= $currentDate) {
                $forecastDates[] = $date;
            } else {
                $definiteDates[] = $date;
            }
        }

        return [
            'definite' => ($definiteDates) ? [min($definiteDates), max($definiteDates)] : null,
            'forecast' => ($forecastDates) ? [min($forecastDates), max($forecastDates)] : null,
        ];
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $step
     * @param string $format
     * @return array
     */
    public static function dateRange($from, $to, $step = '+1 day', $format = 'Y-m-d')
    {
        $dates = [];
        $current = strtotime($from);
        $last = strtotime($to);

        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    /**
     * @param array $separators
     * @param string $string
     * @return array
     */
    protected static function multiExplode($separators, $string)
    {
        $result = [];
        if ($separators) {
            $separator = array_shift($separators);
            if ($separator) {
                foreach (explode($separator, $string) as $item) {
                    if ($item) {
                        $result[] = self::multiExplode($separators, $item);
                    }
                }
            }
        } else {
            $result = $string;
        }

        return $result;
    }

    /**
     * @param Office $office
     * @param CallCenter[] $callCenters
     * @param integer $countryId
     * @param string $dateFrom
     * @param string $dateTo
     * @param MonthlyStatement $monthlyStatement
     * @return array
     */
    protected function getSalaryFromReport($office, $callCenters, $countryId, $dateFrom, $dateTo, $monthlyStatement = null)
    {
        $params['s'] = [
            'from' => $dateFrom,
            'to' => $dateTo,
            'office' => [$office->id],
        ];
        if ($monthlyStatement) {
            $params['s']['monthlyStatement'] = $monthlyStatement->id;
        }
        $reportForm = new ReportFormSalary();
        $dataProvider = $reportForm->apply($params);

        $ccCountries = ArrayHelper::map($callCenters, 'id', 'country_id');

        $rate = $office->currency
            ? $office->currency->currencyRate->rate
            : $office->country->currencyRate->rate;

        $salary = [
            'operators' => 0,
            'staff' => 0,
        ];
        foreach ($dataProvider->allModels as $model) {
            $personSalary = 0;
            if ($office['calc_salary_usd']) {
                $personSalary = $model['salary_tax_usd'];
            } elseif ($office['calc_salary_local'] && $rate) {
                $personSalary = $model['salary_tax_local'] / $rate;
            }
            if ($model['designation_id'] == $this->getDesignationOperatorId()) {
                $countries = array_unique($ccCountries);
                $countries = array_fill_keys($countries, 0);
                $records = self::multiExplode([',', '_'], $model['call_center_id_oper_id']);
                if ($records && $record = $records[0]) {
                    if ($ccCountryId = ArrayHelper::getValue($ccCountries, $record[0])) {
                        $countries[$ccCountryId] = 1;
                    }
                }
                if ($count = array_sum($countries)) {
                    $percent = $countries[$countryId] / $count;
                    $salary['operators'] += $personSalary * $percent;
                } else {
                    $salary['staff'] += $personSalary;
                }
            } else {
                $salary['staff'] += $personSalary;
            }
        }

        return $salary;
    }

    /**
     * @param Office $office
     * @param int $countryId
     * @return array
     */
    protected function getAllStaffSalary($office, $countryId)
    {
        $monthlyStatements = MonthlyStatement::find()
            ->byOfficeId($office->id)
            ->andWhere(['and',
                ['date_from' => $this->dateFrom],
                ['date_to' => $this->dateTo]
            ])
            ->all();

        $salary = [
            'operators' => 0,
            'staff' => 0,
        ];
        $callCenters = CallCenter::find()->all();

        // Смотрим в зарплатных графиках
        if ($monthlyStatements) {
            $range = $this->dateRange($this->dateFrom, $this->dateTo);
            foreach ($monthlyStatements as $monthlyStatement) {
                $dateFrom = $monthlyStatement->date_from;
                $dateTo = $monthlyStatement->date_to;
                $result = $this->getSalaryFromReport($office, $callCenters, $countryId, $dateFrom, $dateTo, $monthlyStatement);
                $salary['operators'] += $result['operators'];
                $salary['staff'] += $result['staff'];
                $range = $this->cutPeriodFromRange($range, $dateFrom, $dateTo);
            }
            $periods = ($range) ? $this->rangeToPeriods($range) : [];
        } else {
            $periods = [[$this->dateFrom, $this->dateTo]];
        }

        // Если остались даты без графиков
        foreach ($periods as $period) {
            $list = $this->preparePeriod($period[0], $period[1]);
            // if ($list['definite']) {
                list($dateFrom, $dateTo) = $list['definite'];
                $result = $this->getSalaryFromReport($office, $callCenters, $countryId, $dateFrom, $dateTo);
                $salary['operators'] += $result['operators'];
                $salary['staff'] += $result['staff'];
            /*
            }
            if ($list['forecast']) {
                list($dateFrom, $dateTo) = $list['forecast'];
                $salary['operators'] += $this->getOperatorsSalary($office, $countryId, $dateFrom, $dateTo);
                $salary['staff'] += $this->getStaffSalary($office, $dateFrom, $dateTo);
            }
            */
        }

        return $salary;
    }

    /**
     * @param Office $office
     * @return array
     */
    protected function getOfficeDirectionData($office)
    {
        $monthlyStatements = MonthlyStatement::find()
            ->byOfficeId($office->id)
            ->andWhere(['and',
                ['date_from' => $this->dateFrom],
                ['date_to' => $this->dateTo]
            ])
            ->all();

        $ccUsers = [];
        // Смотрим в зарплатных графиках
        if ($monthlyStatements) {
            $range = $this->dateRange($this->dateFrom, $this->dateTo);
            foreach ($monthlyStatements as $monthlyStatement) {
                $range = $this->cutPeriodFromRange($range, $monthlyStatement->date_from, $monthlyStatement->date_to);
                foreach ($monthlyStatement->monthlyStatementDatas as $data) {
                    $records = self::multiExplode([',', '_'], $data->call_center_id_oper_id);
                    if ($records && $record = $records[0]) {
                        $ccId = $record[0];
                        if (!isset($ccUsers[$ccId])) {
                            $ccUsers[$ccId] = 0;
                        }
                        $ccUsers[$ccId]++;
                    }
                }
            }
            $periods = ($range) ? $this->rangeToPeriods($range) : [];
        } else {
            $periods = [[$this->dateFrom, $this->dateTo]];
        }

        // Если остались даты без графиков
        foreach ($periods as $period) {
            list($dateFrom, $dateTo) = $period;

            $data = Person::find()
                ->addSelect([
                    'id' => CallCenter::tableName() . '.id',
                    'count' => 'COUNT(1)',
                ])
                ->joinWith(['callCenterUsers', 'callCenterUsers.callCenter'], false)
                ->byOfficeId($office->id)
                ->andWhere([Person::tableName() . '.designation_id' => $this->getDesignationOperatorId()])
                ->andWhere(['or',
                    ['<=', Person::tableName() . '.start_date', $dateTo],
                    ['is', Person::tableName() . '.start_date', null],
                    ['=', Person::tableName() . '.start_date', ''],
                ])
                ->andWhere(['or',
                    ['>=', Person::tableName() . '.dismissal_date', $dateFrom],
                    ['is', Person::tableName() . '.dismissal_date', null],
                    ['=', Person::tableName() . '.dismissal_date', ''],
                ])
                ->andWhere(['or',
                    ['and',
                        ['!=', Person::tableName() . '.active', 1],
                        ['is not', Person::tableName() . '.dismissal_date', null],
                        ['!=', Person::tableName() . '.dismissal_date', ''],
                    ],
                    ['=', Person::tableName() . '.active', 1],
                ])
                ->andWhere([CallCenter::tableName() . '.active' => 1])
                ->andWhere([CallCenterUser::tableName() . '.active' => 1])
                ->groupBy([CallCenter::tableName() . '.id'])
                ->asArray()
                ->all();

            foreach ($data as $item) {
                $ccId = $item['id'];
                if ($ccId && $item['count']) {
                    if (!isset($ccUsers[$ccId])) {
                        $ccUsers[$ccId] = 0;
                    }
                    $ccUsers[$ccId] += $item['count'];
                }
            }
        }

        $data = [];
        if ($ccs = array_keys($ccUsers)) {
            $callCenters = CallCenter::find()->byId($ccs)->all();
        } else {
            $callCenters = $office->callCenters;
        }
        if ($callCenters) {
            $ccCountries = array_fill_keys(ArrayHelper::getColumn($callCenters, 'country_id') , 0);
            foreach ($callCenters as $callCenter) {
                $ccCountries[$callCenter->country_id] += ArrayHelper::getValue($ccUsers, $callCenter->id);
            }
            if ($count = count($ccCountries)) {
                $total = array_sum($ccCountries);
                foreach ($ccCountries as $country_id => $value) {
                    $data[$country_id] = [
                        'rate' => ($total) ? ($value / $total) : (1 / $count),
                        'operatorCount' => $value,
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @param Office $office
     * @param Person $person
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    protected function calcSalary($office, $person, $dateFrom, $dateTo)
    {
        // Вычислить долю отработанного времени
        $from = ($dateFrom > $person->start_date) ? $dateFrom : $person->start_date;
        $to = ($dateTo < $person->dismissal_date || !$person->dismissal_date) ? $dateTo : $person->dismissal_date;
        $workTime = strtotime($to) - strtotime($from);
        $allTime = strtotime($this->dateTo) - strtotime($this->dateFrom);
        $weekdays = $allTime / (7 * 86400);
        $workHourInMonth = $office['working_hours_per_week'] * $weekdays;
        $part = ($workTime / $allTime);

        $personSalary = 0;
        if ($office['calc_salary_usd']) {
            if ($person->salary_usd) {
                $personSalary = $person->salary_usd;
            } elseif ($person->salary_hour_usd) {
                $personSalary = $person->salary_hour_usd * $workHourInMonth;
            }
        }

        $rate = $office->currency
            ? $office->currency->currencyRate->rate
            : $office->country->currencyRate->rate;

        if ($office['calc_salary_local'] && $rate) {
            if ($person->salary_local) {
                $personSalary = $person->salary_local / $rate;
            } elseif ($person->salary_hour_local) {
                $personSalary = $person->salary_hour_local * $workHourInMonth / $rate;
            }
        }

        return $personSalary * $part;
    }

    /**
     * @param Office $office
     * @param int $countryId
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    protected function getOperatorsSalary($office, $countryId, $dateFrom, $dateTo)
    {
        $salary = null;
        if ($office && $countryId && $dateFrom && $dateTo &&
            $designation = $this->getDesignationOperatorId()
        ) {
            $persons = Person::find()
                ->joinWith(['callCenterUsers', 'callCenterUsers.callCenter'], false)
                ->byOfficeId($office->id)
                ->andWhere([Person::tableName() . '.designation_id' => $designation])
                ->andWhere(['or',
                    ['<=', Person::tableName() . '.start_date', $dateTo],
                    ['is', Person::tableName() . '.start_date', null],
                    ['=', Person::tableName() . '.start_date', ''],
                ])
                ->andWhere(['or',
                    ['>=', Person::tableName() . '.dismissal_date', $dateFrom],
                    ['is', Person::tableName() . '.dismissal_date', null],
                    ['=', Person::tableName() . '.dismissal_date', ''],
                ])
                ->andWhere([CallCenter::tableName() . '.country_id' => $countryId])
                ->andWhere([CallCenter::tableName() . '.active' => 1])
                ->andWhere([CallCenterUser::tableName() . '.active' => 1])
                ->groupBy([Person::tableName() . '.id'])
                ->all();

            $salary = 0;
            foreach ($persons as $person) {
                $users = [];
                foreach ($person->callCenterUsers as $user) {
                     if ($user->active) {
                         if (!isset($users[$user->callCenter->id])) {
                             $users[$user->callCenter->id] = 0;
                         }
                         $users[$user->callCenter->id]++;
                     }
                }
                $count = count($users) ?: 1;
                $salary += $this->calcSalary($office, $person, $dateFrom, $dateTo) / $count;
            }
        }

        return $salary;
    }

    /**
     * @param Office $office
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    protected function getStaffSalary($office, $dateFrom, $dateTo)
    {
        $salary = null;
        if ($office && $dateFrom && $dateTo &&
            $designation = $this->getDesignationOperatorId()
        ) {
            $persons = Person::find()
                ->byOfficeId($office->id)
                ->andWhere(['!=', Person::tableName() . '.designation_id', $designation])
                ->andWhere(['or',
                    ['<=', Person::tableName() . '.start_date', $dateTo],
                    ['is', Person::tableName() . '.start_date', null],
                    ['=', Person::tableName() . '.start_date', ''],
                ])
                ->andWhere(['or',
                    ['>=', Person::tableName() . '.dismissal_date', $dateFrom],
                    ['is', Person::tableName() . '.dismissal_date', null],
                    ['=', Person::tableName() . '.dismissal_date', ''],
                ])
                ->active()
                ->all();

            $salary = 0;
            foreach ($persons as $person) {
                $salary += $this->calcSalary($office, $person, $dateFrom, $dateTo);
            }
        }

        return $salary;
    }

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $items = [];
        $operatorCount = 0;
        $directionCategories = [];
        $pattern = 'call_center_direction.office#office_id#.direction#direction_id#';
        if ($this->country && $countryId = $this->country->country_id) {
            $offices = Office::find()
                ->joinWith(['callCenters'], false)
                ->where([
                    CallCenter::tableName() . '.active' => 1,
                    CallCenter::tableName() . '.country_id' => $countryId,
                    Office::tableName() . '.type' => Office::TYPE_CALL_CENTER,
                ])
                ->active()
                ->all();

            foreach ($offices as $office) {
                $salary = $this->getAllStaffSalary($office, $countryId);
                $data = $this->getOfficeDirectionData($office);
                $rate = 0;
                if ($data = ArrayHelper::getValue($data, $countryId)) {
                    $rate = $data['rate'];
                    $operatorCount = $data['operatorCount'];
                }
                $country = ReportExpensesCountry::find()
                    ->byCountryId($office->country_id)
                    ->one();

                if ($country) {
                    $options = $this->factory->options;
                    $options['country'] = $country;
                    $catalogProvider = new EstimateCatalogProvider($options);
                    foreach ($catalogProvider->getItems() as $item) {
                        if (
                            $item->categoryType == ReportExpensesCategory::CATEGORY_CALL_CENTER &&
                            $item->categoryEntityId == $office->id
                        ) {
                            $tmp = $pattern;
                            $tmp = str_replace('#direction_id#', $office->country_id, $tmp);
                            $category = str_replace('#office_id#', $office->id, $tmp);
                            $code = $category . '.' . $item->getRealCode();

                            if (!isset($items[$code])) {
                                $items[$code] = [
                                    'code' =>  $code,
                                    'type' => $item->type,
                                    'name' => $item->name,
                                    'category' => $category,
                                    'value' => 0,
                                    'formula' => $item->formula,
                                ];
                            }

                            if ($item->getRealCode() == 'operators_salary') {
                                $items[$code]['type'] = 'regular';
                                $items[$code]['value'] = $salary['operators'];
                            } elseif ($item->getRealCode() == 'staff_salary') {
                                $items[$code]['type'] = 'regular';
                                $items[$code]['value'] = $salary['staff'] * $rate;
                            } else {
                                if ($item->type == 'regular') {
                                    $items[$code]['value'] = $item->getValue() * $rate;
                                }
                            }
                            $directionCategories[$category] = $category;
                        }
                    }
                }
            }
        }

        $items[EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER] = [
            'code' =>  EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER,
            'type' => 'regular',
            'name' => Yii::t('common', 'Кол-во операторов'),
            'value' => $operatorCount,
        ];

        $directionVariables = [];
        foreach ($directionCategories as $category) {
            $directionVariables[] = Formula::prepareVariable($category . '.costs');
        }

        $items[EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR] = [
            'code' =>  EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR,
            'type' => 'formula',
            'name' => Yii::t('common', 'Ср. зп. оператора'),
            'formula' => '(' . implode(' + ', $directionVariables) . ') / ' . Formula::prepareVariable(EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER),
        ];

        return $items;
    }
}