<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\models\Source;
use app\models\VoipExpense;
use app\modules\callcenter\models\CallCenter;
use app\modules\finance\models\ReportExpensesCountryCategory;
use app\modules\order\models\Lead;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\Query;
use app\modules\salary\models\Office;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class EstimateCommonProvider
 * @package app\modules\finance\components
 */
class EstimateCommonProvider extends EstimateProvider
{
    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $items = [];

        $ordersApprove = $this->factory->getFinanceProvider()->getItemDetailValues(EstimateItem::FINANCE_ORDERS_APPROVE);

//        $rootCategories = ReportExpensesCategory::find()->all();
        $categories = [
//            ReportExpensesCategory::CATEGORY_ROOT => Yii::t('common', 'Без категории'),
        ];
//        foreach ($rootCategories as $category) {
//            $code = ReportExpensesCategory::fullCode($category);
//            $categories[$code] = $category->name;
//        }
        if ($this->country) {
            $countryCategories = ReportExpensesCountryCategory::find()
                ->joinWith(['office', 'category'])
                ->byCountryId($this->country->id)
                ->active()
                ->all();
            foreach ($countryCategories as $countryCategory) {
                $code = ReportExpensesCountryCategory::fullCode($countryCategory);
                $categories[$code] = Yii::t('common', $countryCategory->name);
            }
        }
        $costVariables = [];
        $deliveryVariables = [];
        $categoryItemCodes = [];
        foreach ($this->factory->getCatalogProvider()->getItems() as $item) {
            if (in_array('product', explode('.', $item->category)) && $item->getRealCode() == 'unit_cost') {
                continue;
            }
            if ($item->category && $item->category != 'root') {
                if (in_array($item->category, array_keys($categories))) {
                    $categoryItemCodes[$item->category][] = Formula::prepareVariable($item->code);
                }
            } else {
                $costVariables[] = Formula::prepareVariable($item->code);
                $deliveryVariables[] = Formula::prepareVariable($item->code);
            }
        }

        // Добавляем расходы на продукты
        foreach ($categories as $category => $name) {
            if (in_array('product', explode('.', $category))) {
                $unitCostCode = '';
                foreach ($this->factory->getCatalogProvider()->getItems() as $item) {
                    if ($item->getRealCode() == 'unit_cost') {
                        $unitCostCode = $item->code;
                        break;
                    }
                }
                if ($unitCostCode) {
                    $items[] = [
                        'code' => $category . '.' . EstimateItem::PRODUCT_COSTS,
                        'name' => Yii::t('common', 'Стоимость продуктов'),
                        'type' => 'formula',
                        'detailOn' => true,
                        'calcValue' => true,
                        'formula' => Formula::prepareVariable($unitCostCode) . ' * ' . Formula::prepareVariable(EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY),
                    ];
                    $items[] = [
                        'code' => $category . '.' . EstimateItem::PRODUCT_SPOILAGE,
                        'name' => Yii::t('common', 'Бой / порча'),
                        'type' => 'formula',
                        'detailOn' => true,
                        'calcValue' => true,
                        'formula' => Formula::prepareVariable($unitCostCode) . ' * ' . Formula::prepareVariable(EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY) . ' / 2',
                    ];

                    $categoryItemCodes[$category][] = Formula::prepareVariable($category . '.' . EstimateItem::PRODUCT_COSTS);
                    $categoryItemCodes[$category][] = Formula::prepareVariable($category . '.' . EstimateItem::PRODUCT_SPOILAGE);
                }
            }
        }

        foreach ($categoryItemCodes as $category => $codes) {
            if (in_array('call_center', explode('.', $category))) {
                continue;
            }
            if ($codes) {
                $code = self::PROVIDER_CATALOG . '.' . 'category.' . $category . '.costs';
                $items[] = [
                    'code' => $code,
                    'type' => 'formula',
                    'name' => $categories[$category],
                    'formula' => implode(' + ', $codes)
                ];
                if ($category != 'call_center') {
                    $costVariables[] = Formula::prepareVariable($code);
                    $deliveryVariables[] = Formula::prepareVariable($code);
                }
            }
        }
//        $costVariables[] = Formula::prepareVariable(EstimateItem::PRODUCT_COSTS);
        $costVariables[] = Formula::prepareVariable(EstimateItem::LEAD_COSTS);

        // добавить в расходы Стоимость IP телефонии
        $costVariables[] = Formula::prepareVariable(EstimateItem::CAll_CENTER_IP_TELEPHONY);

        // исключаем Штраф за рекламу из расходов
//        $costVariables[] = Formula::prepareVariable(EstimateItem::PENALTY_FOR_LEADS);
//        $costVariables[] = Formula::prepareVariable(EstimateItem::DELIVERY_COSTS);

        if ($this->country && $this->country->country_id) {
            // Категории для направлений колл центров
/*
            $offices = Office::find()
                ->joinWith(['callCenters'], false)
                ->where([
                    CallCenter::tableName() . '.active' => 1,
                    CallCenter::tableName() . '.country_id' => $this->country->country_id,
                ])
                ->active()
                ->all();
*/
//            foreach ($offices as $office) {
//                $expensesCountry = ReportExpensesCountry::findOne(['country_id' => $office->country_id]);
//                if ($expensesCountry) {
//                    $pattern = 'call_center_direction.office#office_id#.direction#direction_id#';
//                    $tmp = str_replace('#direction_id#', $office->country_id, $pattern);
//                    $code = str_replace('#office_id#', $office->id, $tmp);
//                    $categories[$code] = $office->name;
//
//                    $categoryCodes = array_keys($categories);
//                    $categoryItemCodes = [];
//
//                    $options = $this->factory->options;
//                    $options['country'] = $expensesCountry;
//                    $factory = new EstimateFactory(['options' => $options]);
//
//                    foreach ($factory->getCallCenterDirectionProvider()->getItems() as $item) {
//                        if (in_array($item->code, [
//                            EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER,
//                            EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR,
//                        ])) {
//                            continue;
//                        }
//                        if ($item->category) {
//                            if (in_array($item->category, $categoryCodes)) {
//                                $categoryItemCodes[$item->category][] = Formula::prepareVariable($item->code);
//                            }
//                        } else {
//                            $costVariables[] = Formula::prepareVariable($item->code);
//                        }
//                    }
//
//                    foreach ($categoryItemCodes as $category => $codes) {
//                        if ($codes) {
//                            $code = $category . '.costs';
//                            $items[] = [
//                                'code' => $code,
//                                'type' => 'formula',
//                                'name' => $categories[$category],
//                                'formula' => implode(' + ', $codes)
//                            ];
//                            $costVariables[] = Formula::prepareVariable($code);
//                        }
//                    }
//                }
//            }

            // курьерки
            if ($deliveries = $this->factory->getDeliveryProvider()->getDeliveryList()) {
                $categories = [];
                foreach ($deliveries as $id => $name) {
                    $code = EstimateProvider::PROVIDER_DELIVERY . '.delivery' . $id;
                    $categories[$code] = $name;
                }
                $categoryCodes = array_keys($categories);
                $categoryItemCodes = [];
                foreach ($this->factory->getDeliveryProvider()->getItems() as $item) {
                    if ($item->code == EstimateItem::DELIVERY_COSTS_ALL_DELIVERY) {
                        continue;
                    }
                    if ($item->category) {
                        if (in_array($item->category, $categoryCodes)) {
                            $categoryItemCodes[$item->category][] = Formula::prepareVariable($item->code);
                        }
                    } else {
                        $costVariables[] = Formula::prepareVariable($item->code);
                        $deliveryVariables[] = Formula::prepareVariable($item->code);
                    }
                }
                foreach ($categoryItemCodes as $category => $codes) {
                    if ($codes) {
                        $code = $category . '.costs';
                        $items[] = [
                            'code' => $code,
                            'type' => 'formula',
                            'name' => $categories[$category],
                            'formula' => implode(' + ', $codes)
                        ];
                        $costVariables[] = Formula::prepareVariable($code);
                        $deliveryVariables[] = Formula::prepareVariable($code);
                    }
                }
            }

            $provider = $this->factory->getOfficeProvider();
            $itemGroups = ArrayHelper::index($provider->getItems(), 'code', ['category']);
            foreach ($provider->getOffices() as $office) {
                if ($office->type == Office::TYPE_CALL_CENTER) {
                    foreach ($office->callCenters as $callCenter) {
                        if ($callCenter->country_id != $provider->country->country_id) {
                            continue;
                        }

                        $categoryCode = $provider->generateCategory($office->id, $callCenter->id);
                        if ($subItems = $itemGroups[$categoryCode] ?? null) {

                            $variables = [];
                            foreach ($subItems as $item) {
                                $variables[] = Formula::prepareVariable($item->code);
                            }
                            if ($variables) {
                                $items[] = [
                                    'code' => $categoryCode . '.costs',
                                    'type' => 'formula',
                                    'name' => Yii::t('common', 'Стоимость {office_name}', [
                                        'office_name' => $office->name,
                                    ]),
                                    'formula' => implode(' + ', $variables)
                                ];
                                $costVariables[] = Formula::prepareVariable($categoryCode . '.costs');
                            }
                        }

                    }
                } else {
                    $categoryCode = $provider->generateCategory($office->id);
                    if ($subItems = $itemGroups[$categoryCode] ?? null) {

                        $categoryCode = $provider->generateCategory($office->id);
                        if ($subItems = $itemGroups[$categoryCode] ?? null) {

                            $variables = [];
                            foreach ($subItems as $item) {
                                $variables[] = Formula::prepareVariable($item->code);
                            }
                            if ($variables) {
                                $items[] = [
                                    'code' => $categoryCode . '.costs',
                                    'type' => 'formula',
                                    'name' => Yii::t('common', 'Стоимость {office_name}', [
                                        'office_name' => $office->name,
                                    ]),
                                    'formula' => implode(' + ', $variables)
                                ];
                                $costVariables[] = Formula::prepareVariable($categoryCode . '.costs');
                                $deliveryVariables[] = Formula::prepareVariable($categoryCode . '.costs');
                            }
                        }
                    }
                }
            }
        }

        $items[] = [
            'code' => EstimateItem::COSTS,
            'type' => 'formula',
            'name' => Yii::t('common', 'Расходы'),
            'formula' => ($costVariables) ? implode(' + ', $costVariables) : null,
        ];

        $expensesVariables = [];
        foreach ($costVariables as $variable) {
            if ($variable == '{lead_costs}') {
                continue;
            }
            $expensesVariables[] = $variable;
        }
        $items[] = [
            'code' => EstimateItem::EXPENSES,
            'type' => 'formula',
            'name' => Yii::t('common', 'Затраты'),
            'formula' => ($expensesVariables) ? implode(' + ', $expensesVariables) : null,
        ];
        $items[] = [
            'code' => EstimateItem::INCOME,
            'name' => Yii::t('common', 'Прибыль'),
            'type' => 'formula',
            'formula' => Formula::prepareVariable(EstimateItem::FINANCE_BUYOUT_INCOME) . ' - ' . Formula::prepareVariable(EstimateItem::COSTS),
        ];
        $items[] = [
            'code' => EstimateItem::INCOME_BY_LEAD,
            'name' => Yii::t('common', 'Прибыль на лид'),
            'type' => 'formula',
            'formula' => Formula::prepareVariable(EstimateItem::INCOME) . ' / ( ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE) . ')',
        ];
        $items[] = [
            'code' => EstimateItem::COST_APPROVE,
            'name' => Yii::t('common', 'Стоимость апрува'),
            'type' => 'formula',
            'formula' => Formula::prepareVariable(EstimateItem::CATALOG_CATEGORY_CALL_CENTER_COSTS) . ' / ( ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE) . ')',
        ];

        $items[] = [
            'code' => EstimateItem::EXPENSES_DELIVERY,
            'type' => 'formula',
            'name' => Yii::t('common', 'Расходы кроме рекламы, офисов КЦ и телефонии'),
            'formula' => ($deliveryVariables) ? implode(' + ', $deliveryVariables) : null,
        ];
        $items[] = [
            'code' => EstimateItem::COST_BUYOUT,
            'name' => Yii::t('common', 'Стоимость выкупа'),
            'type' => 'formula',
            'formula' => Formula::prepareVariable(EstimateItem::EXPENSES_DELIVERY) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_BUYOUT),
        ];
        $items[] = [
            'code' => EstimateItem::COST_ORDER_SERVICE,
            'name' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'type' => 'formula',
            'formula' => Formula::prepareVariable(EstimateItem::EXPENSES) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE),
        ];
        $items[] = [
            'code' => EstimateItem::COST_DELIVERY,
            'name' => Yii::t('common', 'Стоимость доставки'),
            'type' => 'formula',
            'formula' => null,
        ];

        $leadsCosts = $this->getLeadsCosts();
        $items[] = [
            'code' => EstimateItem::LEAD_COSTS,
            'name' => Yii::t('common', 'Стоимость лидов'),
            'type' => 'regular',
            'detailOn' => true,
            'calcValue' => true,
            'detailValue' => function ($item, $id) use ($leadsCosts) {
                return $leadsCosts[$id] ?? 0;
            },
        ];

        $leadsPenalty = $this->getLeadsPenalty();
        $items[] = [
            'code' => EstimateItem::PENALTY_FOR_LEADS,
            'name' => Yii::t('common', 'Штрафы за рекламу'),
            'type' => 'regular',
            'detailOn' => true,
            'calcValue' => true,
            'detailValue' => function ($item, $id) use ($leadsPenalty) {
                return $leadsPenalty[$id] ?? 0;
            },
        ];

        $voipCosts = $this->getVoipCosts();
        $items[] = [
            'code' => EstimateItem::CAll_CENTER_IP_TELEPHONY,
            'name' => Yii::t('common', 'IP телефония'),
            'type' => 'regular',
            'value' => $voipCosts,
            'detailOn' => true,
            'calcValue' => false,
            'detailValue' => function ($item, $id) use ($voipCosts, $ordersApprove) {
                return self::getDivideByPart($voipCosts, $ordersApprove, $id);
            },
        ];

        return $items;
    }

    /**
     * цена рекламы, считать отдельно и по статусам lead.source_status = 'confirm', по дате лида и сумму revenue.
     * @return array
     */
    protected function getLeadsCosts()
    {
        $return = [];
        if ($this->country &&
            $this->country->country_id &&
            $this->dateFrom &&
            $this->dateTo
        ) {

            $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom . ' 00:00:00');
            $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo . ' 23:59:59');

            $query = new Query();
            $query->select(['revenue' => 'SUM(revenue)']);
            $query->from([Lead::tableName()]);
            $query->where(
                [
                    'and',
                    ['=', Lead::tableName() . '.country', $this->country->country->char_code],
                    ['=', Lead::tableName() . '.source_status', Lead::STATUS_CONFIRMED],
                    ['=', Lead::tableName() . '.source_id', Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()],
                    ['between', Lead::tableName() . '.ts_spawn', $timeFrom, $timeTo]
                ]
            );
            if ($this->groupBy == static::GROUP_BY_PRODUCT) {
                $query->addSelect([
                    static::GROUP_BY_PRODUCT => LeadProduct::find()
                        ->select([
                            LeadProduct::tableName() . '.product_id'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Lead::tableName() . '.order_id')])
                        ->limit(1)
                ]);
                $query->groupBy([static::GROUP_BY_PRODUCT]);
            }
            return ArrayHelper::map($query->all(), static::GROUP_BY_PRODUCT, 'revenue');
        }
        return $return;
    }

    /**
     * @return array
     */
    protected function getLeadsPenalty()
    {
        $return = [];
        if ($this->country &&
            $this->country->country_id &&
            $this->dateFrom &&
            $this->dateTo
        ) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom . ' 00:00:00');
            $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo . ' 23:59:59');

            $query = new Query();
            $query->select(['revenue' => 'SUM(revenue)']);
            $query->from([Lead::tableName()]);
            $query->where(
                [
                    'and',
                    ['=', Lead::tableName() . '.country', $this->country->country->char_code],
                    ['=', Lead::tableName() . '.source_status', Lead::STATUS_CONFIRMED],
                    ['=', Lead::tableName() . '.source_id', Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()],
                    ['between', Lead::tableName() . '.ts_spawn', $timeFrom, $timeTo],
                    [Lead::tableName() . '.has_approve_penalty' => true]
                ]
            );
            if ($this->groupBy == static::GROUP_BY_PRODUCT) {
                $query->addSelect([
                    static::GROUP_BY_PRODUCT => LeadProduct::find()
                        ->select([
                            LeadProduct::tableName() . '.product_id'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Lead::tableName() . '.order_id')])
                        ->limit(1)
                ]);
                $query->groupBy([static::GROUP_BY_PRODUCT]);
            }
            $return = ArrayHelper::map($query->all(), static::GROUP_BY_PRODUCT, 'revenue');
        }
        return $return;
    }

    /**
     * @return mixed
     */
    protected function getVoipCosts()
    {
        $sum = 0;
        if ($this->country &&
            $this->country->country_id &&
            $this->dateFrom &&
            $this->dateTo
        ) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom . ' 00:00:00');
            $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo . ' 23:59:59');

            $sum = VoipExpense::find()
                ->where(['country_id' => $this->country->country_id])
                ->andWhere(['>=', VoipExpense::tableName() . '.date_stat', $this->dateFrom])
                ->andWhere(['<=', VoipExpense::tableName() . '.date_stat', $this->dateTo])
                ->sum('amount_usd');

            if ($this->isForecastPeriod) {
                $forecastTimeFrom = strtotime('-1 month', $timeFrom);
                $forecastTimeTo = strtotime('-1 month', $timeTo);
                $forecastDateFrom = date('Y-m-d', $forecastTimeFrom);
                $forecastDateTo = date('Y-m-d', $forecastTimeTo);

                $timeFrom = Yii::$app->formatter->asTimestamp($forecastDateFrom . ' 00:00:00');
                $timeTo = Yii::$app->formatter->asTimestamp($forecastDateTo . ' 23:59:59');

                $ipCost = VoipExpense::find()
                    ->where(['country_id' => $this->country->country_id])
                    ->andWhere(['>=', VoipExpense::tableName() . '.date_stat', $forecastDateFrom])
                    ->andWhere(['<=', VoipExpense::tableName() . '.date_stat', $forecastDateTo])
                    ->sum('amount_usd');

                $leadCount = Order::find()
                    ->andWhere(['>=', Order::tableName() . '.created_at', $timeFrom])
                    ->andWhere(['<=', Order::tableName() . '.created_at', $timeTo])
                    ->andWhere(['status_id' => OrderStatus::getAllList()])
                    ->count();

                $ts1 = strtotime(date('Y-m-d 00:00:00'));
                $ts2 = strtotime($this->dateTo . ' 00:00:00');
                $forecastDays = ($ts2 - $ts1) / 86400;
                $avgIpCosts = ($leadCount) ? ($ipCost / $leadCount) : 0;

                $sum += $avgIpCosts * $forecastDays;
            }
        }

        return $sum;
    }
}