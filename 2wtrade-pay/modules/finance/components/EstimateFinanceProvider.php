<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\models\Source;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\order\models\Lead;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\Query;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class EstimateFinanceProvider extends EstimateProvider
{
    protected $data = [];

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $items = [
            EstimateItem::FINANCE_LEAD_COUNT => [
                'name' => Yii::t('common', 'Кол-во лидов всего'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_APPROVE => [
                'name' => Yii::t('common', 'Кол-во апрувленных лидов'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_DELIVERY => [
                'name' => Yii::t('common', 'Кол-во лидов переданных в КС'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_RECALL => [
                'name' => Yii::t('common', 'Количество переобзвонненых заказов'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_BUYOUT => [
                'name' => Yii::t('common', 'Количество выкупленных заказов'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED => [
                'name' => Yii::t('common', 'Деньги получены'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_IN_PROCESS => [
                'name' => Yii::t('common', 'Количество заказов в процессе'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_REFUSE => [
                'name' => Yii::t('common', 'Количество отказов от заказовов'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_PRODUCT_QUANTITY => [
                'name' => Yii::t('common', 'Количество товара'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY => [
                'name' => Yii::t('common', 'Количество выкупленного товара'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY => [
                'name' => Yii::t('common', 'Количество отказного товара'),
                'type' => 'regular',
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_AVG_LEAD_PRICE => [
                'name' => Yii::t('common', 'Средняя цена за лид'),
                'type' => 'regular',
                'calcValueFunc' => function ($item) {
                    /* @var EstimateItem $item */
                    $sum = null;
                    $approveCount = $item->factory->getItem(EstimateItem::FINANCE_ORDERS_APPROVE);
                    if ($count = $approveCount->getValue()) {
                        $sum = 0;
                        foreach ($item->getDetailValues() as $id => $value) {
                            $detailCount = $approveCount->getDetailValue($id);
                            if (is_numeric($value)) {
                                $sum += $value * $detailCount;
                            } else {
                                $count -= $detailCount;
                            }
                        }
                        $sum = ($count) ? $sum / $count : null;
                    }
                    return $sum;
                },
                'calcValue' => true
            ],
            EstimateItem::FINANCE_AVG_CHECK => [
                'name' => Yii::t('common', 'Средний чек'),
                'type' => 'regular',
                'calcValueFunc' => function ($item) {
                    /* @var EstimateItem $item */
                    $sum = null;
                    $approveCount = $item->factory->getItem(EstimateItem::FINANCE_ORDERS_APPROVE);
                    if ($count = $approveCount->getValue()) {
                        $sum = 0;
                        foreach ($item->getDetailValues() as $id => $value) {
                            $detailCount = $approveCount->getDetailValue($id);
                            if (is_numeric($value)) {
                                $sum += $value * $detailCount;
                            } else {
                                $count -= $detailCount;
                            }
                        }
                        $sum = ($count) ? $sum / $count : null;
                    }
                    return $sum;
                },
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_AVG_BUYOUT_CHECK => [
                'name' => Yii::t('common', 'Средний выкупной чек'),
                'type' => 'regular',
                'calcValueFunc' => function ($item) {
                    /* @var EstimateItem $item */
                    $sum = null;
                    $buyoutCount = $item->factory->getItem(EstimateItem::FINANCE_ORDERS_BUYOUT);
                    if ($count = $buyoutCount->getValue()) {
                        $sum = 0;
                        if ($item->detailOn) {
                            foreach ($item->getDetailValues() as $id => $value) {
                                $detailCount = $buyoutCount->getDetailValue($id);
                                if (is_numeric($value)) {
                                    $sum += $value * $detailCount;
                                } else {
                                    $count -= $detailCount;
                                }
                            }
                        } else {
                            $sum = $item->value;
                        }
                        $sum = ($count) ? $sum / $count : null;
                    }
                    return $sum;
                },
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_BUYOUT_INCOME => [
                'name' => Yii::t('common', 'Доход'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_AVG_BUYOUT_CHECK) . ' * ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_BUYOUT),
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_DELIVERY_COSTS => [
                'name' => Yii::t('common', 'Стоимость доставка'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_AVG_DELIVERY_COSTS) . ' * ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_IN_PROCESS),
                'calcValueFunc' => 'sum',
                'calcValue' => true,
            ],
            EstimateItem::FINANCE_PROFITABILITY => [
                'name' => Yii::t('common', 'Прибыльность'),
                'type' => 'regular',
                'detailValue' => function ($item, $id) {
                    /* @var $item EstimateItem */
                    $leadPrice = $item->factory->getItem(EstimateItem::FINANCE_AVG_LEAD_PRICE);
                    $values = $leadPrice->getDetailValues();
                    $min = min($values);
                    $max = max($values);
                    $value = null;

                    $approve = $item->factory->getItem(EstimateItem::FINANCE_ORDERS_APPROVE)->getDetailValue($id);
                    $buyout = $item->factory->getItem(EstimateItem::FINANCE_ORDERS_BUYOUT)->getDetailValue($id);
                    if ($min != $max && $approve) {
                        $current = $leadPrice->getDetailValue($id);
                        $value = ($max - $current) / ($max - $min);
                        $value *= $buyout;
                        $value /= $approve;
                        $value *= 100;
                    }
                    return $value;
                },
            ],
            EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT => [
                'name' => Yii::t('common', 'Проценты выкупленных заказов'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_BUYOUT) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE) . ' * 100',
                //                'calcValueFunc' => 'sum',
                //                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED_PERCENT => [
                'name' => Yii::t('common', 'Проценты денег получено'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_BUYOUT) . ' * 100',
                //                'calcValueFunc' => 'sum',
                //                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_IN_PROCESS_PERCENT => [
                'name' => Yii::t('common', 'Проценты заказов в процессе'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_IN_PROCESS) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE) . ' * 100',
                //                'calcValueFunc' => 'sum',
                //                'calcValue' => true,
            ],
            EstimateItem::FINANCE_ORDERS_REFUSE_PERCENT => [
                'name' => Yii::t('common', 'Проценты отказов от заказовов'),
                'type' => 'formula',
                'formula' => Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_REFUSE) . ' / ' . Formula::prepareVariable(EstimateItem::FINANCE_ORDERS_APPROVE) . ' * 100',
                //                'calcValueFunc' => 'sum',
                //                'calcValue' => true,
            ],
        ];

        $data = $this->getData();
        $forecastData = null;
        if ($this->isForecastPeriod) {
            $forecastData = $this->getForecastData();
        }
        foreach ($items as $code => &$item) {
            $item['code'] = $code;
            if ($this->groupBy) {
                $item['detailOn'] = true;
                $item['detailValues'] = ArrayHelper::map($data, $this->groupBy, $this->getRef($code));
                if ($forecastData) {
                    $forecastDetailValues = ArrayHelper::map($forecastData, $this->groupBy, $this->getRef($code, false));

                    if ($code == EstimateItem::FINANCE_AVG_LEAD_PRICE) {

                        $forecastLead = ArrayHelper::map($forecastData, $this->groupBy, $this->getRef(EstimateItem::FINANCE_LEAD_COUNT, false));
                        $lead = ArrayHelper::map($data, $this->groupBy, $this->getRef(EstimateItem::FINANCE_LEAD_COUNT));
                        foreach ($item['detailValues'] as $id => &$value) {
                            $forecastValue = ArrayHelper::getValue($forecastDetailValues, $id);
                            $forecastLeadCount = ArrayHelper::getValue($forecastLead, $id);
                            $leadCount = ArrayHelper::getValue($lead, $id);
                            if ($allCount = $forecastLeadCount + $leadCount) {
                                $value = ($forecastLeadCount / $allCount) * $forecastValue + ($leadCount / $allCount) * $value;
                            } else {
                                $value = null;
                            }
                        }
                        unset($value);

                    } elseif ($code == EstimateItem::FINANCE_AVG_CHECK) {

                        $forecastApprove = ArrayHelper::map($forecastData, $this->groupBy, $this->getRef(EstimateItem::FINANCE_ORDERS_APPROVE, false));
                        $approve = ArrayHelper::map($data, $this->groupBy, $this->getRef(EstimateItem::FINANCE_ORDERS_APPROVE));
                        foreach ($item['detailValues'] as $id => &$value) {
                            $forecastValue = ArrayHelper::getValue($forecastDetailValues, $id);
                            $forecastApproveCount = ArrayHelper::getValue($forecastApprove, $id);
                            $approveCount = ArrayHelper::getValue($approve, $id);
                            if ($allCount = $forecastApproveCount + $approveCount) {
                                $value = ($forecastApproveCount / $allCount) * $forecastValue + ($approveCount / $allCount) * $value;
                            } else {
                                $value = null;
                            }

                        }
                        unset($value);

                    } elseif ($code == EstimateItem::FINANCE_AVG_BUYOUT_CHECK) {

                        $forecastBuyout = ArrayHelper::map($forecastData, $this->groupBy, $this->getRef(EstimateItem::FINANCE_ORDERS_BUYOUT, false));
                        $buyout = ArrayHelper::map($data, $this->groupBy, $this->getRef(EstimateItem::FINANCE_ORDERS_BUYOUT));
                        foreach ($item['detailValues'] as $id => &$value) {
                            $forecastValue = ArrayHelper::getValue($forecastDetailValues, $id);
                            $forecastBuyoutCount = ArrayHelper::getValue($forecastBuyout, $id);
                            $buyoutCount = ArrayHelper::getValue($buyout, $id);
                            if ($allCount = $forecastBuyoutCount + $buyoutCount) {
                                $value = ($forecastBuyoutCount / $allCount) * $forecastValue + ($buyoutCount / $allCount) * $value;
                            } else {
                                $value = null;
                            }

                        }
                        unset($value);

                    } elseif (in_array($code, [
                        EstimateItem::FINANCE_ORDERS_APPROVE,
                        EstimateItem::FINANCE_ORDERS_BUYOUT,
                        EstimateItem::FINANCE_ORDERS_IN_PROCESS,
                        EstimateItem::FINANCE_ORDERS_REFUSE,
                        EstimateItem::FINANCE_LEAD_COUNT,
                        EstimateItem::FINANCE_PRODUCT_QUANTITY,
                        EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY,
                        EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY,
                    ])) {
                        foreach ($item['detailValues'] as $id => &$value) {
                            $value += ArrayHelper::getValue($forecastDetailValues, $id);
                        }
                    }
                }
            } else {
                $item['value'] = ArrayHelper::getValue($data, $this->getRef($code));
                if ($forecastData) {
                    $item['forecastValue'] = ArrayHelper::getValue($forecastData, $this->getRef($code));
                }
            }
        }
        unset($item);

        return $items;
    }

    /**
     * @param $name
     * @param $trim
     * @return mixed|null
     */
    public function getRef($name, $trim = true)
    {
        $map = [
            EstimateItem::FINANCE_ORDERS_APPROVE => 'approve_count',
            EstimateItem::FINANCE_ORDERS_DELIVERY => 'delivery_count',
            EstimateItem::FINANCE_ORDERS_RECALL => 'recall_count',
            EstimateItem::FINANCE_ORDERS_BUYOUT => 'buyout_count',
            EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED => 'money_received_count',
            EstimateItem::FINANCE_ORDERS_IN_PROCESS => 'in_process_count',
            EstimateItem::FINANCE_ORDERS_REFUSE => 'refuse_count',
            EstimateItem::FINANCE_LEAD_COUNT => 'lead_count',
            EstimateItem::FINANCE_PRODUCT_QUANTITY => 'product_quantity',
            EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY => 'product_buyout_quantity',
            EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY => 'product_refuse_quantity',
            EstimateItem::FINANCE_AVG_LEAD_PRICE => 'avg_leads',
            EstimateItem::FINANCE_AVG_CHECK => 'avg_check',
            EstimateItem::FINANCE_AVG_BUYOUT_CHECK => 'avg_buyout_check',
        ];

        return (isset($map[$name])) ? ($trim ? str_replace('_', '', $map[$name]) : $map[$name]) : null;
    }

    /**
     * @return array
     */
    private function getForecastLeadData()
    {
        $dataTo = date('Y-m-d 00:00:00');
        $timeTo = Yii::$app->formatter->asTimestamp($dataTo);
        $dataFrom = date('Y-m-d 00:00:00', strtotime('-3day', $timeTo));
        $timeFrom = Yii::$app->formatter->asTimestamp($dataFrom);

        $query = new Query();
        $query->from([Order::tableName()]);
        $query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $query->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
        $query->andWhere(['>=', Order::tableName() . '.created_at', $timeFrom]);
        $query->andWhere(['<', Order::tableName() . '.created_at', $timeTo]);

        $useDollar = true;
        $fromDollar = (!$useDollar) ? '* ' . CurrencyRate::tableName() . '.rate' : '';

        $query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');

        $query->addAvgCondition(Order::tableName() . '.income ' . $fromDollar, 'lead_avg_price2');
        $query->addSelect(['count' => 'count(1)']);

        if (in_array($this->groupBy, [static::GROUP_BY_PRODUCT])) {
            if ($this->groupBy == static::GROUP_BY_PRODUCT) {

                $subQuery = new Query();
                $subQuery->from([Order::tableName()]);
                $subQuery->select([
                    'order_id' => OrderProduct::tableName() . '.order_id',
                    'product_id' => OrderProduct::tableName() . '.product_id',
                    'quantity' => OrderProduct::tableName() . '.quantity',
                ]);
                $subQuery->leftJoin(OrderProduct::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id');
                $subQuery->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
                $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
                $subQuery->andWhere(['>=', Order::tableName() . '.created_at', $timeFrom]);
                $subQuery->andWhere(['<', Order::tableName() . '.created_at', $timeTo]);
                $subQuery->groupBy(['order_id']);

                $aliasSq0 = 'sq0';
                $query->innerJoin([$aliasSq0 => $subQuery], $aliasSq0 . '.order_id=' . Order::tableName() . '.id');

                $subQuery = new Query();
                $subQuery->from([Order::tableName()]);
                $subQuery->select([
                    'product_id' => OrderProduct::tableName() . '.product_id',
                    'quantity' => 'sum(' . OrderProduct::tableName() . '.quantity' . ')',
                ]);
                $subQuery->leftJoin(OrderProduct::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id');
                $subQuery->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
                $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
                $subQuery->andWhere(['>=', Order::tableName() . '.created_at', $timeFrom]);
                $subQuery->andWhere(['<', Order::tableName() . '.created_at', $timeTo]);
                $subQuery->groupBy(['product_id']);

                $aliasSq1 = 'sq1';
                $query->innerJoin([$aliasSq1 => $subQuery], $aliasSq1 . '.product_id=' . $aliasSq0 . '.product_id');
                $query->addSelect([
                    static::GROUP_BY_PRODUCT => $aliasSq0 . '.product_id',
                    'productquantity' => $aliasSq1 . '.quantity',
                ]);
                $query->indexBy(static::GROUP_BY_PRODUCT);
                $query->groupBy(static::GROUP_BY_PRODUCT);

            }
            if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                $data = $query->all();
                $this->data[sha1($query->createCommand()->rawSql)] = $data;
            } else {
                $data = $this->data[sha1($query->createCommand()->rawSql)];
            }
        } else {
            if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                $data = $query->one();
                $this->data[sha1($query->createCommand()->rawSql)] = $data;
            } else {
                $data = $this->data[sha1($query->createCommand()->rawSql)];
            }
        }

        if ($data) {
            foreach ($data as &$item) {
                $item['productquantity'] = $item['productquantity'] / $item['count'];
                $item['count'] = intval($item['count'] / 3);
            }
            unset($item);
        }

        return $data;
    }

    /**
     * @return array
     */
    private function getForecastApproveData()
    {
        $today = date('Y-m-d 00:00:00');
        $time = strtotime('-7day', strtotime($today));
        $date = date('Y-m-d 00:00:00', $time);
        $time = Yii::$app->formatter->asTimestamp($date);

        $mapStatuses = [
            'approve_count' => OrderStatus::getApproveList(),
            'lead_count' => OrderStatus::getAllList(),
        ];
        $statuses = [];
        foreach ($mapStatuses as $mapStatus) {
            $statuses = array_merge($statuses, $mapStatus);
        }

        $query = new Query();
        $query->from([Order::tableName()]);
        $query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        $query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
        $query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $query->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
        $query->andWhere(['<', CallCenterRequest::tableName() . '.sent_at', $time]);
        $query->andWhere(['in', Order::tableName() . '.status_id', OrderStatus::getAllList()]);

        // Количество заказов в статусе
        foreach ($mapStatuses as $name => $statuses) {
            $query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
        }

        $useDollar = true;
        $toDollar = ($useDollar) ? '/ ' . CurrencyRate::tableName() . '.rate' : '';
        $query->addAvgCondition('(' . Order::tableName() . '.price_total ' . $toDollar . ' )', 'avg_check');
        $query->orderBy([CallCenterRequest::tableName() . '.sent_at' => SORT_DESC]);

        $data = [];
        if ($this->groupBy) {
            $groups = ArrayHelper::getColumn($this->getData(), $this->groupBy);
            foreach ($groups as $groupId) {
                $mainQuery = clone $query;
                if ($this->groupBy == static::GROUP_BY_PRODUCT) {
                    $subQuery = new Query();
                    $subQuery->from([Order::tableName()]);
                    $subQuery->select([
                        'order_id' => OrderProduct::tableName() . '.order_id',
                        'product_id' => OrderProduct::tableName() . '.product_id',
                        'quantity' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                    ]);
                    $subQuery->leftJoin(OrderProduct::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id');
                    $subQuery->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                    $subQuery->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
                    $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
                    $subQuery->andWhere(['<', CallCenterRequest::tableName() . '.sent_at', $time]);
                    $subQuery->andWhere(['in', Order::tableName() . '.status_id', OrderStatus::getAllList()]);
                    $subQuery->andWhere(['=', OrderProduct::tableName() . '.product_id', $groupId]);
                    $subQuery->orderBy([CallCenterRequest::tableName() . '.sent_at' => SORT_DESC]);
                    $subQuery->groupBy(['order_id']);
                    $subQuery->limit(1000);

                    $aliasSq0 = 'sq0';
                    $mainQuery->innerJoin([$aliasSq0 => $subQuery], $aliasSq0 . '.order_id=' . Order::tableName() . '.id');
                    $mainQuery->addSelect([
                        static::GROUP_BY_PRODUCT => $aliasSq0 . '.product_id',
                        'productquantity' => 'sum(' . $aliasSq0 . '.quantity)',
                    ]);
                    $mainQuery->indexBy(static::GROUP_BY_PRODUCT);
                    $mainQuery->groupBy(static::GROUP_BY_PRODUCT);
                }
                if (!isset($this->data[sha1($mainQuery->createCommand()->rawSql)])) {
                    $result = $mainQuery->one();
                    $this->data[sha1($mainQuery->createCommand()->rawSql)] = $result;
                } else {
                    $result = $this->data[sha1($mainQuery->createCommand()->rawSql)];
                }
                if ($result) {
                    $data[$groupId] = $result;
                }
            }
        } else {
            if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                $data = $query->one();
                $this->data[sha1($query->createCommand()->rawSql)] = $data;
            } else {
                $data = $this->data[sha1($query->createCommand()->rawSql)];
            }
        }

        foreach ($data as &$item) {
            if ($item['leadcount'] < 100) {
                $item['productquantity'] = 0;
                $item['avgcheck'] = 0;
            } else {
                $item['approvepercent'] = $item['approvecount'] / $item['leadcount'];
                $item['productquantity'] = $item['productquantity'] / $item['leadcount'];
            }
        }
        unset($item);

        return $data;
    }

    /**
     * @return array
     */
    private function getForecastOrderData()
    {
        $today = date('Y-m-d 00:00:00');
        $time = strtotime('-14day', strtotime($today));
        $date = date('Y-m-d 00:00:00', $time);
        $time = Yii::$app->formatter->asTimestamp($date);

        $mapStatuses = [
            'approve_count' => OrderStatus::getApproveList(),
            'in_process_count' => OrderStatus::getProcessDeliveryList(),
            'buyout_count' => OrderStatus::getBuyoutList(),
            'refuse_count' => OrderStatus::getNotBuyoutList(),
        ];
        $statuses = [];
        foreach ($mapStatuses as $mapStatus) {
            $statuses = array_merge($statuses, $mapStatus);
        }

        $query = new Query();
        $query->from([Order::tableName()]);
        // Исключаем дубли заказов, которые не выкуп
        $fromQuery = (new Query())->from(Order::tableName())
            ->leftJoin(['original_order_table' => Order::tableName()], Order::tableName() . ".id = `original_order_table`.duplicate_order_id")
            ->where([Order::tableName() . '.duplicate_order_id' => null, Order::tableName().'.country_id' => $this->country->country_id])
            ->select([
                'status_id' => '(CASE WHEN `original_order_table`.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') THEN `original_order_table`.status_id ELSE ' . Order::tableName() . '.status_id END)',
                'id' => 'COALESCE(`original_order_table`.id, `order`.id)'
            ]);
        $query->innerJoin(['unique_order_table' => $fromQuery], '`unique_order_table`.id = ' . Order::tableName() . '.id');
        $query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
        $query->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
        $query->andWhere(['<', DeliveryRequest::tableName() . '.sent_at', $time]);
        $query->andWhere(['in', 'unique_order_table.status_id', $statuses]);

        $usdCurrency = Currency::getUSD();
        $convertPriceTotalToCurrencyId = $usdCurrency->id;

        // Количество заказов в статусе
        foreach ($mapStatuses as $name => $statuses) {
            $query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
        }

        $case = $query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')',
            'unique_order_table.status_id',
            OrderStatus::getBuyoutList()
        );
        $query->addAvgCondition($case, 'buyout_avg_check');
        $query->orderBy([DeliveryRequest::tableName() . '.sent_at' => SORT_DESC]);

        $data = [];
        if ($this->groupBy) {
            $groups = array_keys($this->getData());
            foreach ($groups as $groupId) {
                $mainQuery = clone $query;
                if ($this->groupBy == static::GROUP_BY_PRODUCT) {
                    $subQuery = new Query();
                    $subQuery->from([Order::tableName()]);
                    $subQuery->select([
                        'order_id' => OrderProduct::tableName() . '.order_id',
                        'product_id' => OrderProduct::tableName() . '.product_id',
                        'quantity' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                    ]);
                    $subQuery->leftJoin(OrderProduct::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id');
                    $subQuery->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                    $subQuery->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
                    $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country->country_id]);
                    $subQuery->andWhere(['<', DeliveryRequest::tableName() . '.sent_at', $time]);
                    $subQuery->andWhere(['in', Order::tableName() . '.status_id', OrderStatus::getApproveList()]);
                    $subQuery->andWhere(['=', OrderProduct::tableName() . '.product_id', $groupId]);
                    $subQuery->orderBy([DeliveryRequest::tableName() . '.sent_at' => SORT_DESC]);
                    $subQuery->groupBy(['order_id']);
                    $subQuery->limit(1000);

                    $aliasSq0 = 'sq0';
                    $mainQuery->innerJoin([$aliasSq0 => $subQuery], $aliasSq0 . '.order_id=' . Order::tableName() . '.id');

                    $case = $query->buildCaseCondition(
                        '(' . $aliasSq0 . '.quantity)',
                        'unique_order_table.status_id',
                        OrderStatus::getBuyoutList()
                    );
                    $mainQuery->addSumCondition($case, 'buyoutproductquantity');

                    $case = $query->buildCaseCondition(
                        '(' . $aliasSq0 . '.quantity)',
                        'unique_order_table.status_id',
                        OrderStatus::getNotBuyoutList()
                    );
                    $mainQuery->addSumCondition($case, 'refuseproductquantity');

                    $mainQuery->addSelect([
                        static::GROUP_BY_PRODUCT => $aliasSq0 . '.product_id',
                    ]);

                    $mainQuery->indexBy(static::GROUP_BY_PRODUCT);
                    $mainQuery->groupBy(static::GROUP_BY_PRODUCT);
                }
                if (!isset($this->data[sha1($mainQuery->createCommand()->rawSql)])) {
                    $result = $mainQuery->one();
                    $this->data[sha1($mainQuery->createCommand()->rawSql)] = $result;
                } else {
                    $result = $this->data[sha1($mainQuery->createCommand()->rawSql)];
                }
                if ($result) {
                    $data[$groupId] = $result;
                }
            }
        } else {
            if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                $data = $query->one();
                $this->data[sha1($query->createCommand()->rawSql)] = $data;
            } else {
                $data = $this->data[sha1($query->createCommand()->rawSql)];
            }
        }

        foreach ($data as &$item) {
            if ($item['buyoutcount'] < 100) {
                $item['refuseproductquantity'] = 0;
                $item['buyoutproductquantity'] = 0;
                $item['refusecount'] = 0;
                $item['buyoutcount'] = 0;
                $item['buyoutavgcheck'] = 0;
                $item['refusepercent'] = 0;
                $item['buyoutpercent'] = 0;
            } else {
                $item['refuseproductquantity'] = ($item['refusecount']) ? $item['refuseproductquantity'] / $item['refusecount'] : 0;
                $item['buyoutproductquantity'] = ($item['buyoutcount']) ? $item['buyoutproductquantity'] / $item['buyoutcount'] : 0;
                $item['refusepercent'] = ($item['refusecount']) / ($item['approvecount']);
                $item['buyoutpercent'] = ($item['buyoutcount']) / ($item['approvecount']);
            }
        }
        unset($item);

        return $data;
    }

    /**
     * @return array
     */
    public function getForecastParams()
    {
        $params = [];
        if ($this->groupBy) {
            $groups = ArrayHelper::getColumn($this->getData(), $this->groupBy);

            $leadData = $this->getForecastLeadData();
            foreach ($groups as $group) {
                $item = ArrayHelper::getValue($leadData, $group);
                $params[$group]['leadAvgPrice'] = ($item) ? $item['leadavgprice'] : 0;
                $params[$group]['leadCountPerDay'] = ($item) ? $item['count'] : 0;
                $params[$group]['leadProductQuantity'] = ($item) ? $item['productquantity'] : 0;
            }

            $approveData = $this->getForecastApproveData();
            foreach ($groups as $group) {
                $item = ArrayHelper::getValue($approveData, $group);
                $params[$group]['approvePercent'] = $item['approvepercent'] ?? 0;
                $params[$group]['approveAvgCheck'] = ($item) ? $item['avgcheck'] : 0;
                $params[$group]['approveProductQuantity'] = ($item) ? $item['productquantity'] : 0;
            }

            $orderData = $this->getForecastOrderData();
            foreach ($groups as $group) {
                $item = ArrayHelper::getValue($orderData, $group);
                $params[$group]['refusePercent'] = ($item) ? $item['refusepercent'] : 0;
                $params[$group]['buyoutPercent'] = ($item) ? $item['buyoutpercent'] : 0;
                $params[$group]['inProcessPercent'] = ($item['refusepercent'] && $item['buyoutpercent']) ? (1 - $item['refusepercent'] - $item['buyoutpercent']) : 0;
                $params[$group]['refuseProductQuantity'] = ($item) ? $item['refuseproductquantity'] : 0;
                $params[$group]['buyoutProductQuantity'] = ($item) ? $item['buyoutproductquantity'] : 0;
                $params[$group]['buyoutAvgCheck'] = ($item) ? $item['buyoutavgcheck'] : 0;
            }

            if ($this->forecastParams) {
                foreach ($groups as $group) {
                    $leadCountPerDay = ArrayHelper::getValue($this->forecastParams['leadCountPerDay'], $group, 0);
                    $approvePercent = ArrayHelper::getValue($this->forecastParams['approvePercent'], $group, 0) / 100;
                    $refusePercent = ArrayHelper::getValue($this->forecastParams['refusePercent'], $group, 0) / 100;
                    $buyoutPercent = ArrayHelper::getValue($this->forecastParams['buyoutPercent'], $group, 0) / 100;

                    $params[$group]['leadCountPerDay'] = $leadCountPerDay;
                    $params[$group]['approvePercent'] = $approvePercent;
                    $params[$group]['refusePercent'] = $refusePercent;
                    $params[$group]['buyoutPercent'] = $buyoutPercent;
                    $params[$group]['inProcessPercent'] = ($refusePercent && $buyoutPercent) ? (1 - $refusePercent - $buyoutPercent) : 0;
                }
            }
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getForecastData()
    {
        $data = [];
        if ($this->country &&
            $this->dateFrom &&
            $this->dateTo &&
            $this->country->country_id
        ) {
            $ts1 = strtotime(date('Y-m-d 00:00:00'));
            $ts2 = strtotime($this->dateTo . ' 00:00:00');
            $forecastDays = ($ts2 - $ts1) / 86400;

            if ($this->groupBy == static::GROUP_BY_PRODUCT) {

                foreach ($this->getForecastParams() as $group => $params) {

                    $leadCount = intval($params['leadCountPerDay'] * $forecastDays);
                    $approveCount = intval($params['approvePercent'] * $leadCount);
                    $refuseCount = intval($params['refusePercent'] * $approveCount);
                    $buyoutCount = intval($params['buyoutPercent'] * $approveCount);
                    $inProcessCount = intval($params['inProcessPercent'] * $approveCount);

                    $data[$group] = [
                        $this->groupBy => $group,
                        'lead_count' => $leadCount,
                        'avg_leads' => $params['leadAvgPrice'],
                        'lead_product_quantity' => intval($params['leadProductQuantity'] * $leadCount),

                        'approve_count' => intval($params['approvePercent'] * $leadCount),
                        'avg_check' => $params['approveAvgCheck'],
                        'product_quantity' => intval($params['approveProductQuantity'] * $approveCount),

                        'refuse_count' => $refuseCount,
                        'buyout_count' => $buyoutCount,
                        'in_process_count' => $inProcessCount,
                        'avg_buyout_check' => $params['buyoutAvgCheck'],
                        'product_refuse_quantity' => intval($params['refuseProductQuantity'] * $refuseCount),
                        'product_buyout_quantity' => intval($params['buyoutProductQuantity'] * $buyoutCount),
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        if ($this->country &&
            $this->dateFrom &&
            $this->dateTo &&
            $this->country->country_id
        ) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom . ' 00:00:00');
            $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo . ' 23:59:59');

            $mapStatuses = [
                'approve_count' => OrderStatus::getApproveCCList(),
                'delivery_count' => OrderStatus::getInDeliveryAndFinanceList(),
                'in_process_count' => OrderStatus::getProcessDeliveryList(),
                'buyout_count' => OrderStatus::getBuyoutList(),
                'money_received_count' => OrderStatus::getOnlyMoneyReceivedList(),
                'refuse_count' => OrderStatus::getNotBuyoutList(),
                'lead_count' => OrderStatus::getAllList()
            ];
            $tmp = [];
            foreach ($mapStatuses as $statuses) {
                $tmp = array_merge($tmp, $statuses);
            }
            $mapStatuses['all'] = array_unique($tmp);

            $query = new Query();
            $query->from([Order::tableName()]);
            // Исключаем дубли заказов, которые не выкуп
            $fromQuery = (new Query())->from(Order::tableName())
                ->leftJoin(['original_order_table' => Order::tableName()], Order::tableName() . ".id = `original_order_table`.duplicate_order_id")
                ->where([Order::tableName() . '.duplicate_order_id' => null, Order::tableName().'.country_id' => $this->country->country_id])
                ->select([
                    'status_id' => '(CASE WHEN `original_order_table`.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') THEN `original_order_table`.status_id ELSE ' . Order::tableName() . '.status_id END)',
                    'id' => 'COALESCE(`original_order_table`.id, `order`.id)'
                ])->andWhere(['=', new Expression('COALESCE(`original_order_table`.source_id, ' . Order::tableName().'.source_id)'), Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
            $query->innerJoin(['unique_order_table' => $fromQuery], '`unique_order_table`.id = ' . Order::tableName() . '.id');

            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;

            // Количество заказов в статусе
            foreach ($mapStatuses as $name => $statuses) {
                $query->addInConditionCount('unique_order_table.status_id', $statuses, $name);
            }

            // Средний чек
            $case = $query->buildCaseCondition(
                'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')',
                'unique_order_table.status_id',
                $mapStatuses['approve_count']
            );
            $query->addAvgCondition($case, 'avg_check');

            // Доход по выкупам
            $case = $query->buildCaseCondition(
                'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')',
                'unique_order_table.status_id',
                $mapStatuses['buyout_count']
            );
            $query->addSumCondition($case, 'buyout_income');

            // Средний выкупной чек
            $case = $query->buildCaseCondition(
                'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')',
                'unique_order_table.status_id',
                $mapStatuses['buyout_count']
            );
            $query->addAvgCondition($case, 'avg_buyout_check');

            // Средняя стоимость доставки
            $field = 'convertToCurrencyByCountry(' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')';
            $case = $query->buildCaseCondition($field, 'unique_order_table.status_id', $mapStatuses['buyout_count']);
            $query->addAvgCondition('IF ((' . $case . ') IS NOT NULL AND ' . $field . ' > 0,' . $field . ', NULL)', 'avg_delivery_price');

            // Средняя стоимость лида
            $case = $query->buildCaseCondition(
                '(' . Order::tableName() . '.income)',
                'unique_order_table.status_id',
                $mapStatuses['approve_count']
            );
            $query->addAvgCondition($case, 'avg_leads');

            $query->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');

            $query->andWhere(['>=', Lead::tableName() . '.ts_spawn', $timeFrom]);
            $query->andWhere(['<=', Lead::tableName() . '.ts_spawn', $timeTo]);
            $query->andWhere(['in', 'unique_order_table.status_id', $mapStatuses['all']]);

            if (in_array($this->groupBy, [static::GROUP_BY_PRODUCT])) {
                if ($this->groupBy == static::GROUP_BY_PRODUCT) {
                    $subQuery = new Query();
                    $subQuery->from([OrderProduct::tableName()]);
                    $subQuery->select([
                        'order_id' => OrderProduct::tableName() . '.order_id',
                        'product_id' => OrderProduct::tableName() . '.product_id',
                        'quantity' => 'SUM(' . OrderProduct::tableName().'.quantity)'
                    ]);
                    $subQuery->leftJoin(Order::tableName(), Order::tableName().'.id = ' . OrderProduct::tableName().'.order_id');
                    $subQuery->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');
                    $subQuery->where([Order::tableName().'.country_id' => $this->country->country_id, Order::tableName().'.source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
                    $subQuery->innerJoin(LeadProduct::tableName(), LeadProduct::tableName().'.order_id = ' . OrderProduct::tableName().'.order_id AND ' . LeadProduct::tableName().'.product_id = ' . OrderProduct::tableName().'.product_id');
                    $subQuery->groupBy(['order_id', 'product_id']);
                    $subQuery->andWhere(['>=', Lead::tableName() . '.ts_spawn', $timeFrom]);
                    $subQuery->andWhere(['<=', Lead::tableName() . '.ts_spawn', $timeTo]);
                    $aliasSq0 = 'sq0';
                    $query->innerJoin([$aliasSq0 => $subQuery], $aliasSq0 . '.order_id=' . Order::tableName() . '.id');

                    $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['approve_count']);
                    $query->addSumCondition($condition, 'productquantity');
                    $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['buyout_count']);
                    $query->addSumCondition($condition, 'productbuyoutquantity');
                    $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['refuse_count']);
                    $query->addSumCondition($condition, 'productrefusequantity');

                    $query->addSelect([
                        static::GROUP_BY_PRODUCT => $aliasSq0 . '.product_id'
                    ]);

                    /***
                     * Колонка "количество переобзвоненых заказов" и считать количество заказов отправленных на переобзвон в кц.
                     * По таблице заявок отправки в очереди в кц. по дате результата в период отчета. группировать по первичному товару заказы (таблица lead_product)
                     */
                    $subQuery = new Query();
                    $subQuery->from([CallCenterCheckRequest::tableName()]);
                    $subQuery->select([
                        'count' => 'count(' . CallCenterCheckRequest::tableName() . '.id)',
                        'product_id' => LeadProduct::tableName() . '.product_id',
                    ]);
                    $subQuery->leftJoin(Order::tableName(), Order::tableName() . '.id=' . CallCenterCheckRequest::tableName() . '.order_id');
                    $subQuery->leftJoin(LeadProduct::tableName(), LeadProduct::tableName() . '.order_id=' . Order::tableName() . '.id');
                    $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country->country_id, Order::tableName().'.source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
                    $subQuery->andWhere(['>=', CallCenterCheckRequest::tableName() . '.done_at', $timeFrom]);
                    $subQuery->andWhere(['<=', CallCenterCheckRequest::tableName() . '.done_at', $timeTo]);
                    $subQuery->groupBy(['product_id']);
                    $aliasSq2 = 'sq2';

                    $query->leftJoin([$aliasSq2 => $subQuery], $aliasSq2 . '.product_id=' . $aliasSq0 . '.product_id');
                    $query->addSelect(['recallcount' => $aliasSq2 . '.count']);

                    $query->indexBy(static::GROUP_BY_PRODUCT);
                    $query->groupBy(static::GROUP_BY_PRODUCT);
                }
                if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                    $data = $query->all();
                    $this->data[sha1($query->createCommand()->rawSql)] = $data;
                } else {
                    $data = $this->data[sha1($query->createCommand()->rawSql)];
                }
                if ($data && $this->year && $this->month) {
                    ReportExpensesCountryItem::checkUnitCosts(array_keys($data), $this->country->id, $this->year, $this->month);
                }
            } else {
                if (!isset($this->data[sha1($query->createCommand()->rawSql)])) {
                    $data = $query->one();
                    $this->data[sha1($query->createCommand()->rawSql)] = $data;
                } else {
                    $data = $this->data[sha1($query->createCommand()->rawSql)];
                }
            }
        }

        return $data;
    }
}