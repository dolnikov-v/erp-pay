<?php

namespace app\modules\finance\components;

use app\helpers\math\Formula;
use app\modules\finance\models\ReportExpensesCountry;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use Closure;

class EstimateItem extends BaseObject
{
    const COSTS = 'costs';
    const EXPENSES = 'expenses';
    const EXPENSES_DELIVERY = 'expenses_delivery';
    const INCOME = 'income';
    const INCOME_BY_LEAD = 'income_by_lead';
    const COST_APPROVE = 'cost_approve';
    const COST_BUYOUT = 'cost_buyout';
    const COST_ORDER_SERVICE = 'COST_ORDER_SERVICE';
    const COST_DELIVERY = 'COST_DELIVERY';

    const PRODUCT_COSTS = 'product_costs';
    const PRODUCT_SPOILAGE = 'product_spoilage';
    const LEAD_COSTS = 'lead_costs';
    const PENALTY_FOR_LEADS = 'penalties_for_advertising';

    const CAll_CENTER_IP_TELEPHONY = 'call_center.ip_telephony_costs';
    const CALL_CENTER_OPERATORS_SALARY = 'call_center.operators_salary';
    const CALL_CENTER_STAFF_SALARY = 'call_center.staff_salary';

    const CALL_CENTERS_AVG_COSTS_PER_OPERATOR = 'call_center_avg_costs_per_operator';
    const CALL_CENTERS_DIRECTION_OPERATOR_NUMBER = 'call_center_direction_operator_number';
    const CALL_CENTERS_DIRECTION_OPERATOR_AVG_SALARY = 'call_center_direction_operator_salary';

    const DELIVERY_COSTS = 'delivery_costs';
    const DELIVERY_COSTS_ALL_DELIVERY = 'delivery.costs_all_delivery';
    const DELIVERY_PRICE_STORAGE = 'delivery.price_storage';
    const DELIVERY_PRICE_FULFILMENT = 'delivery.price_fulfilment';
    const DELIVERY_PRICE_PACKAGE = 'delivery.price_package';
    const DELIVERY_PRICE_PACKING = 'delivery.price_packing';
    const DELIVERY_PRICE_ADDRESS_CORRECTION = 'delivery.price_address_correction';
    const DELIVERY_PRICE_DELIVERY = 'delivery.price_delivery';
    const DELIVERY_PRICE_REDELIVERY = 'delivery.price_redelivery';
    const DELIVERY_PRICE_DELIVERY_BACK = 'delivery.price_delivery_back';
    const DELIVERY_PRICE_DELIVERY_RETURN = 'delivery.price_delivery_return';
    const DELIVERY_PRICE_COD_SERVICE = 'delivery.price_cod_service';
    const DELIVERY_PRICE_VAT = 'delivery.price_vat';
    const DELIVERY_PRICE_MONTHLY = 'delivery.price_monthly';

    const DELIVERY_REPORT_TOTAL_COSTS = 'delivery.total_costs';
    const DELIVERY_REPORT_ADDITIONAL_COSTS = 'delivery.additional_costs';

    const FINANCE_ORDERS_APPROVE = 'finance.orders_approve';
    const FINANCE_ORDERS_DELIVERY = 'finance.orders_delivery';
    const FINANCE_ORDERS_RECALL = 'finance.orders_recall';
    const FINANCE_ORDERS_BUYOUT = 'finance.orders_buyout';
    const FINANCE_ORDERS_MONEY_RECEIVED = 'finance.orders_money_received';
    const FINANCE_ORDERS_IN_PROCESS = 'finance.orders_in_progress';
    const FINANCE_ORDERS_REFUSE = 'finance.orders_refuse';
    const FINANCE_LEAD_COUNT = 'finance.lead_count';
    const FINANCE_ORDERS_BUYOUT_PERCENT = 'finance.orders_buyout_percent';
    const FINANCE_ORDERS_MONEY_RECEIVED_PERCENT = 'finance.orders_money_received_percent';
    const FINANCE_ORDERS_IN_PROCESS_PERCENT = 'finance.orders_in_progress_percent';
    const FINANCE_ORDERS_REFUSE_PERCENT = 'finance.orders_refuse_percent';
    const FINANCE_PRODUCT_QUANTITY = 'finance.product_quantity';
    const FINANCE_PRODUCT_REFUSE_QUANTITY = 'finance.product_refuse_quantity';
    const FINANCE_PRODUCT_BUYOUT_QUANTITY = 'finance.product_buyout_quantity';
    const FINANCE_AVG_LEAD_PRICE = 'finance.avg_lead_price';
    const FINANCE_AVG_CHECK = 'finance.avd_check';
    const FINANCE_AVG_BUYOUT_CHECK = 'finance.avg_buyout_check';
    const FINANCE_AVG_DELIVERY_COSTS = 'finance.avg_delivery_costs';
    const FINANCE_BUYOUT_INCOME = 'finance.buyout_income';
    const FINANCE_DELIVERY_COSTS = 'finance.delivery_costs';
    const FINANCE_PROFITABILITY = 'finance.profitability';

    const CATALOG_CALL_CENTER_OPERATORS_SALARY = 'catalog.call_center.operators_salary';
    const CATALOG_CALL_CENTER_STAFF_SALARY = 'catalog.call_center.staff_salary';
    const CATALOG_PRODUCT_UNIT_COST = 'catalog.product.unit_cost';
    const CATALOG_DELIVERY_COSTS = 'catalog.delivery_costs';

    const CATALOG_CATEGORY_CALL_CENTER_COSTS = 'catalog.category.call_center.costs';
    const CATALOG_CATEGORY_PRODUCTS_COSTS = 'catalog.category.products.costs';
    const CATALOG_CATEGORY_STORY_COSTS = 'catalog.category.story.costs';

    const WAREHOUSES_AVG_COSTS = 'warehouses_avg_costs';

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var integer | Closure
     */
    public $value;

    /**
     * @var integer
     */
    public $formula;

    /**
     * @var integer
     */
    public $type;

    /**
     * @var ReportExpensesCountry
     */
    public $detailOn = true;

    /**
     * @var array
     */
    public $detailValues = [];

    /**
     * @var int
     */
    public $categoryEntityId;

    /**
     * @var string
     */
    public $categoryType;

    /**
     * @var Closure
     */
    public $detailValue;

    /**
     * @var string
     */
    public $category;

    /**
     * @var bool
     */
    public $calcValue;

    /**
     * @var string | Closure
     */
    public $calcValueFunc = 'sum';

    /**
     * @var int
     */
    public $parent;

    /**
     * @var string
     */
    public $parentCode;

    /**
     * @var string
     */
    public $group;

    /**
     * @var EstimateFactory
     */
    public $factory;

    /**
     * @var array
     */
    public $descriptions = [];

    /**
     * @var string
     */
    public $totalDescription;

    /**
     * @return string
     */
    public function getRealCode()
    {
        if ($this->parentCode) {
            $codeParts = explode('.', $this->parentCode);
            $code = end($codeParts);
        } else {
            $parentCodeParts = explode('.', $this->code);
            $code = end($parentCodeParts);
        }

        return $code;
    }

    /**
     * @param mixed|null $identity
     * @return mixed
     */
    public function evalFormula($identity = null)
    {
        $result = null;
        if ($this->formula && $this->factory) {
            $variables = $this->factory->getVariableValues(Formula::getVariables($this->formula), $identity);
            $result = Formula::execute($this->formula, $variables);
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        $value = null;
        if ($this->detailOn && $this->calcValue) {
            if ($values = $this->getDetailValues()) {
               if (is_object($this->calcValueFunc) && ($this->calcValueFunc instanceof Closure)) {
                    $value = call_user_func($this->calcValueFunc, $this);
               } elseif ($this->calcValueFunc == 'sum') {
                    $value = array_sum($values);
               } elseif ($this->calcValueFunc == 'avg') {
                   $value = array_sum($values) / count($values);
               }
            }
        } elseif ($this->type == 'formula') {
            $value = $this->evalFormula();
        } elseif (is_object($this->value) && ($this->value instanceof Closure)) {
            $value = call_user_func($this->detailValues, [$this]);
        } else {
            $value = $this->value;
        }
        return $value;
    }

    /**
     * @return array
     */
    public function getDetailValues()
    {
        $values = [];
        if ($this->detailOn) {
            foreach($this->factory->getGroupingList() as $id) {
                $value = $this->getDetailValue($id);
                $values[$id] = (is_numeric($value)) ? $value : null;
            }
        }
        return $values;
    }

    /**
     * @param int
     * @return mixed
     */
    public function getDetailValue($id)
    {
        $value = null;
        if ($this->detailOn) {
            if ($this->type == 'formula') {
                $value = $this->evalFormula($id);
            } elseif (is_object($this->detailValue) && ($this->detailValue instanceof Closure)) {
                $value = call_user_func($this->detailValue, $this, $id);
            } else {
                $value = ArrayHelper::getValue($this->detailValues, $id);
            }
        }

        return $value;
    }

    public function getDescriptions() {
        if (is_object($this->descriptions) && ($this->descriptions instanceof Closure)) {
            return call_user_func($this->descriptions, $this);
        }
        return $this->descriptions;
    }

    public function getTotalDescription() {
        return $this->totalDescription;
    }
}