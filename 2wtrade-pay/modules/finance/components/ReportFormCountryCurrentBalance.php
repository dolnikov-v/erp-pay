<?php
namespace app\modules\finance\components;

use yii\data\ArrayDataProvider;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class
 * @package app\modules\finance\components
 */
class ReportFormCountryCurrentBalance extends Component
{
    /**
     * @var bool
     */
    public $useCache = true;

    /**
     * @return array
     */
    public static function getExpenses()
    {
        return [
            'lead_avg_cost' => EstimateItem::FINANCE_AVG_LEAD_PRICE,
            'delivery_cost' => EstimateItem::DELIVERY_COSTS_ALL_DELIVERY,
            'operator_avg_salary' => EstimateItem::CALL_CENTERS_AVG_COSTS_PER_OPERATOR,
            'warehouse_avg_costs' => EstimateItem::WAREHOUSES_AVG_COSTS,
            'income' => EstimateItem::FINANCE_BUYOUT_INCOME,
            'avg_buyout_check' => EstimateItem::FINANCE_AVG_BUYOUT_CHECK,
            'buyout' => EstimateItem::FINANCE_ORDERS_BUYOUT,
            'costs' => EstimateItem::COSTS,
            'profit' => EstimateItem::INCOME,
        ];
    }
    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $cache = Yii::$app->cache;

        $models = [];
        foreach ($params['countries'] as $country) {

            $key = 'report_form_country_current_balance' . $country->id .  $params['month'] . $params['year'];

            if ($this->useCache) {
                $model = $cache->get($key);
                if (!$model) {
                    $model = [
                        'country' => $country->name,
                        'country_id' => $country->id,
                    ];
                    foreach (self::getExpenses() as $name => $code) {
                        $model[$name] = null;
                    }
                }
            } else {
                $factory = new EstimateFactory(['options' => [
                    'month' => $params['month'],
                    'year' => $params['year'],
                    'country' => $country,
                    'groupBy' => EstimateProvider::GROUP_BY_PRODUCT,
                ]]);

                $model = [
                    'country' => $country->name,
                    'country_id' => $country->id,
                ];
                foreach (self::getExpenses() as $name => $code) {
                    $item = $factory->getItem($code);
                    $model[$name] = ($item instanceof EstimateItem) ? $item->getValue() : null;
                }

                $cache->set($key, $model, 3600);
            }

            if ($model) {
                $models[] = $model;
            }
        }

        ArrayHelper::multisort($models, 'profit');

        return new ArrayDataProvider(['models' => $models]);
    }

    /**
     * @param array|null $params
     * @return array
     */
    public function getProfit(?array $params): array
    {
        $models = [];
        foreach ($params['countries'] as $country) {
            $factory = new EstimateFactory(['options' => [
                'month' => $params['month'],
                'year' => $params['year'],
                'country' => $country,
                'groupBy' => EstimateProvider::GROUP_BY_PRODUCT,
            ]]);
            $model = [
                'country' => $country->name,
                'country_id' => $country->id,
            ];
            $item = $factory->getItem(EstimateItem::INCOME);
            $model['profit'] = ($item instanceof EstimateItem) ? $item->getValue() : null;
            if ($model) {
                $models[] = $model;
            }
        }
        return $models;
    }
}
