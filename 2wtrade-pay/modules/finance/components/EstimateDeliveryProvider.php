<?php

namespace app\modules\finance\components;

use app\models\Currency;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportCosts;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\InvoiceOrder;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class EstimateDeliveryProvider extends EstimateProvider
{
    /**
     * @var array
     */
    protected $data;


    /**
     * @return array
     */
    private function getStatuses()
    {
        return array_merge(
            OrderStatus::getOnlyBuyoutList(),
            OrderStatus::getNotBuyoutInDeliveryProcessList(),
            OrderStatus::getOnlyMoneyReceivedList(),
            OrderStatus::getOnlyNotBuyoutDoneList()
        );
    }

    /**
     * @inheritdoc
     */
    public function loadItems()
    {
        $deliveryItems = [
            EstimateItem::DELIVERY_PRICE_STORAGE => [
                'name' => Yii::t('common', 'Стоимость storage'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_FULFILMENT => [
                'name' => Yii::t('common', 'Стоимость fulfilment'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_PACKAGE => [
                'name' => Yii::t('common', 'Стоимость package'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_PACKING => [
                'name' => Yii::t('common', 'Стоимость packing'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_ADDRESS_CORRECTION => [
                'name' => Yii::t('common', 'Стоимость address correction'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_DELIVERY => [
                'name' => Yii::t('common', 'Стоимость delivery'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_REDELIVERY => [
                'name' => Yii::t('common', 'Стоимость redelivery'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_DELIVERY_BACK => [
                'name' => Yii::t('common', 'Стоимость delivery back'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_DELIVERY_RETURN => [
                'name' => Yii::t('common', 'Стоимость delivery return'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_COD_SERVICE => [
                'name' => Yii::t('common', 'Стоимость cod service'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_VAT => [
                'name' => Yii::t('common', 'Стоимость vat'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_PRICE_MONTHLY => [
                'name' => Yii::t('common', 'Ежемесячные расходы'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_REPORT_TOTAL_COSTS => [
                'name' => Yii::t('common', 'Комиссия банка'),
                'type' => 'regular',
            ],
            EstimateItem::DELIVERY_REPORT_ADDITIONAL_COSTS => [
                'name' => Yii::t('common', 'Доп. издержки'),
                'type' => 'regular',
            ],
        ];

        $items = [];
        $deliveryCostsAllDelivery = 0;
        if ($data = $this->getData()) {
            foreach ($data as $deliveryCosts) {
                $subItems = [];
                foreach ($deliveryItems as $key => $item) {
                    $keyCode = substr($key, strlen(self::PROVIDER_DELIVERY . '.'));
                    $category = self::PROVIDER_DELIVERY . '.delivery' . $deliveryCosts['delivery_id'];
                    $code = $category . '.' . $keyCode;
                    $item['code'] = $code;
                    $item['category'] = $category;
                    $item['value'] = ArrayHelper::getValue($deliveryCosts, $this->getRef($key));
                    $item['detailValue'] = function ($item, $id) use ($deliveryCosts, $keyCode) {
                        return $deliveryCosts['details'][$id][$keyCode] ?? 0;
                    };
                    $subItems[$code] = $item;
                }
                $items = array_merge($items, $subItems);
                $deliveryCostsAllDelivery += $deliveryCosts['sum'] ?? 0;
            }
        }

        $items[] = [
            'code' => EstimateItem::DELIVERY_COSTS_ALL_DELIVERY,
            'name' => Yii::t('common', 'Стоимость всей доставки'),
            'type' => 'regular',
            'value' => $deliveryCostsAllDelivery,
        ];

        return $items;
    }

    public function getDeliveryList()
    {
        return ArrayHelper::map($this->getData(), 'delivery_id', 'delivery_name');
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getRef($name)
    {
        $map = [
            EstimateItem::DELIVERY_PRICE_STORAGE => 'price_storage',
            EstimateItem::DELIVERY_PRICE_FULFILMENT => 'price_fulfilment',
            EstimateItem::DELIVERY_PRICE_PACKAGE => 'price_package',
            EstimateItem::DELIVERY_PRICE_PACKING => 'price_packing',
            EstimateItem::DELIVERY_PRICE_ADDRESS_CORRECTION => 'price_address_correction',
            EstimateItem::DELIVERY_PRICE_DELIVERY => 'price_delivery',
            EstimateItem::DELIVERY_PRICE_REDELIVERY => 'price_redelivery',
            EstimateItem::DELIVERY_PRICE_DELIVERY_BACK => 'price_delivery_back',
            EstimateItem::DELIVERY_PRICE_DELIVERY_RETURN => 'price_delivery_return',
            EstimateItem::DELIVERY_PRICE_COD_SERVICE => 'price_cod_service',
            EstimateItem::DELIVERY_PRICE_VAT => 'price_vat',
            EstimateItem::DELIVERY_PRICE_MONTHLY => 'price_monthly',
            EstimateItem::DELIVERY_REPORT_TOTAL_COSTS => 'total_costs',
            EstimateItem::DELIVERY_REPORT_ADDITIONAL_COSTS => 'additional_costs',
        ];

        return (isset($map[$name])) ? $map[$name] : null;
    }

    /**
     * @return array
     */
    protected function getData()
    {
        if (is_null($this->data)) {
            $this->data = $this->loadData();
        }

        return $this->data;
    }

    /**
     * @return array
     */
    protected function loadData()
    {
        $return = [];
        if ($this->country &&
            $this->country->country &&
            $this->dateFrom &&
            $this->dateTo
        ) {
            $unixFrom = strtotime($this->dateFrom . ' 00:00:00');
            $unixTo = strtotime($this->dateTo . ' 23:59:59');
            $query = DeliveryRequest::find()->joinWith([
                'order.financePrediction.invoiceOrder',
                'delivery.country.currency.currencyRate',
                'order.lead',
            ], false);
            $query->joinWith([
                'order.financePrediction.priceStorageCurrencyRate',
                'order.financePrediction.priceFulfilmentCurrencyRate',
                'order.financePrediction.pricePackingCurrencyRate',
                'order.financePrediction.pricePackageCurrencyRate',
                'order.financePrediction.priceAddressCorrectionCurrencyRate',
                'order.financePrediction.priceDeliveryCurrencyRate',
                'order.financePrediction.priceRedeliveryCurrencyRate',
                'order.financePrediction.priceDeliveryReturnCurrencyRate',
                'order.financePrediction.priceDeliveryBackCurrencyRate',
                'order.financePrediction.priceCodServiceCurrencyRate',
                'order.financePrediction.priceVatCurrencyRate',
            ], false);

            $currencyFields = OrderFinancePrediction::getExpensesFields();
            $currencyRateRelationMap = OrderFinancePrediction::currencyRateRelationMap();

            $select = [
                'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                'delivery_name' => Delivery::tableName() . '.name',
            ];
            $sumFields = [];
            foreach (array_keys($currencyFields) as $key) {
                $sumFields[] = $key;
                $relationName = $currencyRateRelationMap[$currencyFields[$key]];
                $query->leftJoin("invoice_currency as {$relationName}_temp", "{$relationName}_temp.invoice_id = " . InvoiceOrder::tableName() . ".invoice_id and {$relationName}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]}");
                $query->leftJoin(CurrencyRateHistory::tableName() . " as {$relationName}_history", "{$relationName}_history.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]} AND {$relationName}_history.date = DATE(FROM_UNIXTIME(COALESCE(" . DeliveryRequest::tableName() . ".paid_at," . DeliveryRequest::tableName() . ".done_at)))");
                $select[$key] = "SUM(IFNULL(" . OrderFinancePrediction::tableName() . ".{$key} / COALESCE({$relationName}_temp.rate, {$relationName}_history.rate,{$relationName}.rate, " . CurrencyRate::tableName() . ".rate), 0))";
            }
            $query->select($select);

            $query->addSelect([
                'product_id' => OrderProduct::find()
                    ->select([
                        OrderProduct::tableName() . '.product_id'
                    ])
                    ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                    ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
                    ->limit(1),
                'product_count' => 'count(' . Delivery::tableName() . '.id)'
            ]);


            $query->where([Order::tableName() . '.status_id' => $this->getStatuses()]);
            $query->andWhere(['between', Lead::tableName() . '.ts_spawn' . '', $unixFrom, $unixTo]);
            $query->andWhere([Order::tableName() . '.country_id' => $this->country->country_id, Order::tableName().'.source' => 'adcombo']);

            $query->groupBy([Delivery::tableName() . '.id', 'product_id']);
            $query->asArray();
            $data = $query->all();

            $totalProductsCount = [];
            foreach ($data as $row) {
                if (!isset($totalProductsCount[$row['delivery_id']])) {
                    $totalProductsCount[$row['delivery_id']] = 0;
                }
                $totalProductsCount[$row['delivery_id']] += $row['product_count'] ?? 0;
            }
            $return = [];
            $productsIds = [];
            foreach ($data as $row) {
                if (!isset($return[$row['delivery_id']])) {

                    foreach ($sumFields as $field) {
                        $return[$row['delivery_id']][$field] = 0;
                    }
                    $return[$row['delivery_id']]['sum'] = 0;
                    $return[$row['delivery_id']]['delivery_id'] = $row['delivery_id'];
                    $return[$row['delivery_id']]['delivery_name'] = $row['delivery_name'];
                    $return[$row['delivery_id']]['price_monthly'] = $this->getPriceMonthly($row['delivery_id']);
                    $return[$row['delivery_id']]['details'] = [];
                }
                foreach ($sumFields as $field) {
                    $return[$row['delivery_id']]['sum'] += $row[$field] ?? 0;
                    $return[$row['delivery_id']][$field] += $row[$field] ?? 0;
                    $return[$row['delivery_id']]['details'][$row['product_id']][$field] = $row[$field];
                }
                if (!in_array('price_monthly', $sumFields)) {
                    $return[$row['delivery_id']]['details'][$row['product_id']]['price_monthly'] = !empty($totalProductsCount[$row['delivery_id']]) ? $return[$row['delivery_id']]['price_monthly'] * $row['product_count'] / $totalProductsCount[$row['delivery_id']] : 0;
                }
                $productsIds[$row['product_id']] = $row['product_id'];
            }

            $reports = DeliveryReport::find()->where(['country_id' => $this->country->country_id])
//                Показываем только в первом месяце (берем только по period_from) т.к. репорт может быть за несколько месяцев, а ТБ только за месяц
                ->andWhere(['and', ['>=', 'period_from', $unixFrom],['<', 'period_from', $unixTo]])
                ->andWhere(['type' => DeliveryReport::TYPE_FINANCIAL])
                ->all();


            $usd = Currency::getUSD();
            if ($reports) {
                foreach ($reports as $report) {
                    /** @var $report DeliveryReport */
                    if (!isset($return[$report->delivery_id])) {
                        foreach ($sumFields as $field) {
                            $return[$report->delivery_id][$field] = 0;
                        }
                        $return[$report->delivery_id]['sum'] = 0;
                        $return[$report->delivery_id]['delivery_id'] = $report->delivery_id;
                        $return[$report->delivery_id]['delivery_name'] = $report->delivery->name;
                        $return[$report->delivery_id]['price_monthly'] = $this->getPriceMonthly($report->delivery_id);
                        $return[$report->delivery_id]['details'] = [];
                    }

                    $rate = 0;
                    if (!empty($report->rate_costs)) {
                        $rate = $report->rate_costs;
                    }
                    if (!$rate) {
                        $rate = $report->getRate($report->country->currency_id, $usd->id);
                    }
                    if (!$rate) {
                        $rate = 1;
                    }

                    if (!isset($return[$report->delivery_id]['total_costs'])) {
                        $return[$report->delivery_id]['total_costs'] = 0;
                    }
                    if (!isset($return[$report->delivery_id]['additional_costs'])) {
                        $return[$report->delivery_id]['additional_costs'] = 0;
                    }
// Разобраться с рейтом! Эта херня не только для USD, но и для EUR... без указания для какой именно!!!
                    $return[$report->delivery_id]['total_costs'] += $report->total_costs / $rate;
                }
            }
            $costs = DeliveryReportCosts::find()
                ->joinWith(['deliveryReport'])
                ->where([
                    DeliveryReport::tableName() . '.country_id' => $this->country->country_id,
                    DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL,
                ])
                ->andWhere([
                    'or',
                    [
                        'and',
                        ['>=', 'from', $unixFrom],
                        ['<', 'from', $unixTo],
                    ],
                    [
                        'and',
                        ['>', 'to', $unixFrom],
                        ['<=', 'to', $unixTo],
                    ],
                    [
                        'and',
                        ['<', 'from', $unixFrom],
                        ['>', 'to', $unixTo],
                    ],
                ])->all();
            if ($costs) {
                foreach ($costs as $cost) {
                    $daysCount = (($cost->to - $cost->from) / 86400);
                    $costByDay = ($cost->sum / $daysCount) / $cost->deliveryReport->getRate($cost->deliveryReport->country->currency_id, $usd->id, null, null, true);
                    if ($cost->from >= $unixFrom && $cost->from < $unixTo) {
                        if (($activeDays = ($unixTo - $cost->from) / 86400) && $activeDays < $daysCount) {
                            $daysCount = $activeDays;
                        }
                    } elseif ($cost->to > $unixFrom && $cost->to <= $unixTo) {
                        if (($activeDays = ($cost->to - $unixFrom) / 86400) && $activeDays < $daysCount) {
                            $daysCount = $activeDays;
                        }
                    } else {
                        $daysCount = ($unixTo - $unixFrom) / 86400;
                    }
                    if (!isset($return[$cost->deliveryReport->delivery_id]['additional_costs'])) {
                        $return[$cost->deliveryReport->delivery_id]['additional_costs'] = 0;
                    }
                    $return[$cost->deliveryReport->delivery_id]['additional_costs'] += $costByDay * $daysCount;
                }
            }

            foreach ($return as $deliveryId => $row) {
                foreach ($productsIds as $productsId) {
                    $k = 0;
                    if (isset($row['price_delivery']) && $row['price_delivery'] && isset($row['details'][$productsId]['price_delivery'])) {
                        $k = $row['details'][$productsId]['price_delivery'] / $row['price_delivery'];
                    }
                    $return[$deliveryId]['details'][$productsId]['total_costs'] = ($row['total_costs'] ?? 0) * $k;
                    $return[$deliveryId]['details'][$productsId]['additional_costs'] = ($row['additional_costs'] ?? 0) * $k;
                }
            }
        }

        return $return;
    }


    /**
     * Ежемесячные расходы КС согласно контракту
     *
     * @param $deliveryId
     * @return float
     */
    protected function getPriceMonthly($deliveryId)
    {

        $contract = DeliveryContract::find()
            ->with(['chargesCalculatorModel'])
            ->byDeliveryId($deliveryId)
            ->byDates($this->dateFrom, $this->dateTo)
            ->indexBy('id')
            ->one();

        $return = 0;
        if ($contract instanceof DeliveryContract && $contract->chargesCalculatorModel) {
            $currencyFields = OrderFinancePrediction::getExpensesFields();
            foreach ($currencyFields as $fieldName => $currencyField) {
                $type = $contract->chargesCalculator->getCalculatingTypeForField($fieldName);
                if ($type["type"] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE &&
                    $type['currency_rate'] &&
                    $type['charge']
                ) {
                    $return += $type['charge'] / $type['currency_rate'];
                }
            }
        }
        return $return;
    }
}