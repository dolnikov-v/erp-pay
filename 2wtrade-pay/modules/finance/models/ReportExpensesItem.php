<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\components\validator\FormulaValidator;
use app\helpers\math\Formula;
use app\modules\finance\components\EstimateFactory;
use app\modules\finance\models\query\ReportExpensesItemQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "report_expenses_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property string $value
 * @property string $formula
 * @property string $category_id
 * @property integer $active
 * @property integer $detailed
 * @property integer $is_expense
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ReportExpensesCategory $category
 * @property ReportExpensesCountryItem[] $countryItems
 */
class ReportExpensesItem extends ActiveRecordLogUpdateTime
{
    const TYPE_REGULAR = 'regular';
    const TYPE_VARIABLE = 'variable';

    const ADCOMBO_GTD_APPROVE_PERCENT = 'adcombo_gtd_approve_percent';
    const ADCOMBO_PENALTY_ITEM_CODE = 'adcombo_penalty';
    const UNIT_COST_ITEM_CODE = 'unit_cost';
    const IP_TELEPHONY = 'ip_telephony';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_item}}';
    }

    /**
     * @return ReportExpensesItemQuery
     */
    public static function find()
    {
        return new ReportExpensesItemQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'created_at', 'category_id', 'updated_at', 'detailed', 'is_expense'], 'integer'],
            [['name', 'code', 'type', 'value', 'formula'], 'string', 'max' => 255],
            [['name', 'code', 'type'], 'required'],
            [['formula'], FormulaValidator::className(), 'existVariables' => array_keys($this->getTokens())],
            [['code'], 'unique', 'targetAttribute' => ['code', 'category_id'], 'message' => Yii::t('common', 'Значение «{value}» для «{attribute}» и указанной категории уже занято.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'code' => Yii::t('common', 'Код'),
            'type' => Yii::t('common', 'Тип'),
            'active' => Yii::t('common', 'Активность'),
            'detailed' => Yii::t('common', 'Детально'),
            'is_expense' => Yii::t('common', 'Расход'),
            'value' =>  Yii::t('common', 'Значение'),
            'formula' =>  Yii::t('common', 'Формула'),
            'category' =>  Yii::t('common', 'Категория'),
            'category_id' =>  Yii::t('common', 'Категория'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ReportExpensesCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return string|null
     */
    public function getTypeLabel()
    {
        return ArrayHelper::getValue(self::typeList(), $this->type);
    }

    /**
     * @return array
     */
    public static function typeList()
    {
        return [
            static::TYPE_REGULAR => Yii::t('common', 'Постоянная'),
            static::TYPE_VARIABLE => Yii::t('common', 'Переменная'),
        ];
    }

    /**
     * @return array
     */
    public function getTokens()
    {
        return (new EstimateFactory)->getIncoherentVariables($this->getFullCode());
    }

    /**
     * @return string
     */
    public function getFullCode()
    {
        $categoryName = 'root';
        if ($this->category) {
            $categoryName = $this->category->code ?: 'category' . $this->category->id ?: 'root';
        }
        $itemName = $this->code ?: 'item' . $this->id;
        $code = 'catalog' . '.' . $categoryName . '.' . $itemName;
        return $code;
    }

    /**
     * @return array
     */
    public function getFormulaVariables()
    {
        $variables = [];
        $tokens = $this->getTokens();
        foreach (Formula::getVariables($this->formula) as $variable) {
            $variables[$variable] = (isset($tokens[$variable])) ? $tokens[$variable] : $variable;
        }

        return $variables;
    }
}
