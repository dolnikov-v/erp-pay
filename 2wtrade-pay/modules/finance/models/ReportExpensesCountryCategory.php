<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\Delivery;
use app\modules\finance\models\query\ReportExpensesCountryCategoryQuery;
use app\modules\salary\models\Office;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "report_expenses_country_category".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $country_id
 * @property integer $office_id
 * @property integer $delivery_id
 * @property string $reference
 * @property string $name
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ReportExpensesCountry $country
 * @property ReportExpensesCategory $category
 * @property Office $office
 * @property Delivery $delivery
 */
class ReportExpensesCountryCategory extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country_category}}';
    }

    /**
     * @return ReportExpensesCountryCategoryQuery
     */
    public static function find()
    {
        return new ReportExpensesCountryCategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'category_id', 'country_id', 'office_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
            'country_id' => Yii::t('common', 'Страна'),
            'country' => Yii::t('common', 'Страна'),
            'category_id' => Yii::t('common', 'Категория'),
            'category' => Yii::t('common', 'Категория'),
            'office_id' => Yii::t('common', 'Офис'),
            'office' => Yii::t('common', 'Офис'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'delivery' => Yii::t('common', 'Служба доставки'),
            'reference' => Yii::t('common', 'Привязка'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(ReportExpensesCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ReportExpensesCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return string
     */
    public function getFullCode()
    {
        return self::fullCode($this);
    }

    /**
     * @param self $category
     * @return string
     */
    public static function fullCode($category)
    {
        $code = '';
        if ($category && $category->category) {
            $code = ReportExpensesCategory::fullCode($category->category);
            $code .= '.subcategory' . $category->id;
        }
        return $code;
    }
}
