<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\components\validator\FormulaValidator;
use app\helpers\math\Formula;
use app\modules\finance\components\EstimateFactory;
use app\modules\finance\models\query\ReportExpensesCountryItemQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "report_expenses_country_item".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $parent_id
 * @property string $type
 * @property string $value
 * @property string $formula
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $name
 * @property integer $category_id
 *
 * @property ReportExpensesCountry $country
 * @property ReportExpensesCountryCategory $category
 * @property ReportExpensesItem $parent
 * @property ReportExpensesCountryItemProduct[] $products
 */
class ReportExpensesCountryItem extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country_item}}';
    }

    /**
     * @return ReportExpensesCountryItemQuery
     */
    public static function find()
    {
        return new ReportExpensesCountryItemQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'parent_id', 'type'], 'required'],
            [['country_id', 'parent_id', 'active', 'created_at', 'updated_at', 'category_id'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['type', 'value', 'formula'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 256],
            [
                ['category_id'],
                'exist',
                'skipOnError' => true,
                'skipOnEmpty' => true,
                'targetClass' => ReportExpensesCountryCategory::className(),
                'targetAttribute' => ['category_id' => 'id']
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ReportExpensesCountry::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ReportExpensesItem::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [['formula'], FormulaValidator::className(), 'existVariables' => array_keys($this->getTokens())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'country' => Yii::t('common', 'Страна'),
            'parent_id' => Yii::t('common', 'Статья расходов'),
            'category_id' => Yii::t('common', 'Категория'),
            'category' => Yii::t('common', 'Категория'),
            'parent' => Yii::t('common', 'Статья расходов'),
            'type' => Yii::t('common', 'Тип'),
            'active' => Yii::t('common', 'Активность'),
            'date_from' => Yii::t('common', 'Дата от'),
            'date_to' => Yii::t('common', 'Дата до'),
            'value' => Yii::t('common', 'Значение'),
            'formula' => Yii::t('common', 'Формула'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(ReportExpensesCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ReportExpensesCountryCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ReportExpensesItem::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(ReportExpensesCountryItemProduct::className(), ['item_id' => 'id']);
    }

    /**
     * @return string|null
     */
    public function getTypeLabel()
    {
        return ArrayHelper::getValue(ReportExpensesItem::typeList(), $this->type);
    }

    /**
     * @return array
     */
    public function getTokens()
    {
        $options = [
            'month' => date('m'),
            'year' => date('Y'),
            'country' => $this->country,
        ];

        return (new EstimateFactory(['options' => $options]))->getIncoherentVariables($this->getFullCode());
    }

    /**
     * @return string
     */
    public function getFullCode()
    {
        if ($this->category) {
            $category = ReportExpensesCountryCategory::fullCode($this->category);
        } else {
            $category = ReportExpensesCategory::fullCode($this->parent->category);
        }
        $code = 'catalog.' . $category . $this->parent->code ?: $this->parent->id;
        return $code;
    }

    /**
     * @return array
     */
    public function getFormulaVariables()
    {
        $variables = [];
        $tokens = $this->getTokens();
        foreach (Formula::getVariables($this->formula) as $variable) {
            $variables[$variable] = (isset($tokens[$variable])) ? $tokens[$variable] : $variable;
        }

        return $variables;
    }

    /**
     * Проверка есть ли категории расходов в этом периоде, если нет то генерация на основе прошлого
     *
     * @param integer $countryId
     * @param $dateFrom
     * @param $dateTo
     * @param bool $inDates
     * @return bool
     * @throws \Exception
     */
    public static function checkCloneLast($countryId, $dateFrom, $dateTo, $inDates = false)
    {

        $dateFrom = date('Y-m-d', strtotime($dateFrom));
        $dateTo = date('Y-m-d', strtotime($dateTo));

        $countryExpenses = ReportExpensesCountryItem::find()->byCountryId($countryId);

        if ($inDates) {
            $countryExpenses->byDatesIn($dateFrom, $dateTo);
        } else {
            $countryExpenses->byDates($dateFrom, $dateTo);
        }

        $countryExpenses = $countryExpenses->all();
        if ($countryExpenses) {
            // уже есть данные на этот месяц
            return true;
        }

        $i = 1;
        while (!$countryExpenses) {
            $countryExpenses = ReportExpensesCountryItem::find()
                ->byDates(
                    date('Y-m-01', strtotime($dateFrom . "-" . $i . " month")),
                    date('Y-m-t', strtotime($dateFrom . "-" . $i . " month"))
                )
                ->byCountryId($countryId)
                ->all();

            $i++;
            if ($i > 12) {
                break;
            }
        }

        if ($countryExpenses) {
            $newData = [];
            $newDataProducts = [];

            $transaction = ReportExpensesCountryItem::getDb()->beginTransaction();

            $i = 0;
            foreach ($countryExpenses as $item) {
                $tmp = [
                    'country_id' => $item->country_id,
                    'date_from' => $dateFrom,
                    'date_to' => $dateTo,
                    'parent_id' => $item->parent_id,
                    'type' => $item->type,
                    'value' => $item->value,
                    'formula' => $item->formula,
                    'active' => $item->active,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'name' => $item->name,
                    'category_id' => $item->category_id
                ];
                foreach ($item->getProducts()->all() as $productItem) {
                    $newDataProducts[] = [
                        'item_id' => $i,
                        'product_id' => $productItem->product_id,
                        'value' => $productItem->value,
                        'lead_limit' => $productItem->lead_limit,
                        'date' => $productItem->date,
                        'date_from' => $productItem->date_from,
                        'date_to' => $productItem->date_to,
                    ];
                }
                $newData[$i] = $tmp;
                $i++;
            }
            try {
                $insertCount = Yii::$app->db->createCommand()->batchInsert(
                    ReportExpensesCountryItem::tableName(),
                    [
                        'country_id',
                        'date_from',
                        'date_to',
                        'parent_id',
                        'type',
                        'value',
                        'formula',
                        'active',
                        'created_at',
                        'updated_at',
                        'name',
                        'category_id'
                    ],
                    $newData
                )->execute();

                if ($newDataProducts) {
                    $itemIds = ReportExpensesCountryItem::find()
                        ->select('id')
                        ->limit($insertCount)
                        ->orderBy(['id' => SORT_DESC])
                        ->column();
                    $itemIds = array_reverse($itemIds);

                    foreach ($newDataProducts as &$dataProduct) {
                        $dataProduct['item_id'] = $itemIds[$dataProduct['item_id']];
                    }

                    Yii::$app->db->createCommand()->batchInsert(
                        ReportExpensesCountryItemProduct::tableName(),
                        [
                            'item_id',
                            'product_id',
                            'value',
                            'lead_limit',
                            'date',
                            'date_from',
                            'date_to',
                        ],
                        $newDataProducts
                    )->execute();
                }
                $transaction->commit();
                return true;
            } catch (\Exception $e) {
                $transaction->rollback();
                throw $e;
            }
        }
        return false;
    }

    /**
     * Проверим и при необходимости сохраним стоимость продуктов, если лиды по ним были, а данных нет
     * @param $productIds
     * @param $countryId
     * @param $year
     * @param $month
     */
    public static function checkUnitCosts($productIds, $countryId, $year, $month)
    {
        $reportExpensesItem = ReportExpensesItem::find()->byCode(ReportExpensesItem::UNIT_COST_ITEM_CODE)->one();
        if ($reportExpensesItem) {

            $reportExpensesCountryItem = ReportExpensesCountryItem::find()
                ->byCountryId($countryId)
                ->byParentId($reportExpensesItem->id)
                ->byYearMonth($year, $month)
                ->one();

            if ($reportExpensesCountryItem) {
                $raw = $reportExpensesCountryItem->getProducts()->asArray()->all();
                $savedProducts = [];
                if ($raw) {
                    $savedProducts = ArrayHelper::index($raw, 'product_id');
                }
                
                foreach ($productIds as $productId) {
                    if (!isset($savedProducts[$productId])) {
                        $newProduct = new ReportExpensesCountryItemProduct();
                        $newProduct->item_id = $reportExpensesCountryItem->id;
                        $newProduct->product_id = $productId;
                        $newProduct->value = (string)ReportExpensesCountryItemProduct::DEFAULT_UNIT_COST;
                        $newProduct->save(false);
                    }
                }
            }
        }
    }
}
