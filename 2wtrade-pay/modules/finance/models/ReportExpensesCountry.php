<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\finance\models\query\ReportExpensesCountryQuery;
use Yii;

/**
 * This is the model class for table "report_expenses_country".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $active
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property ReportExpensesCountryItem[] $reportExpensesCountryItems
 */
class ReportExpensesCountry extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country}}';
    }

    /**
     * @return ReportExpensesCountryQuery
     */
    public static function find()
    {
        return new ReportExpensesCountryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['country_id', 'created_at', 'updated_at', 'active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'country_id' =>  Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportExpensesCountryItems()
    {
        return $this->hasMany(ReportExpensesCountryItem::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
