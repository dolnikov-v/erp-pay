<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\finance\models\query\ReportExpensesCategoryQuery;
use Yii;

/**
 * This is the model class for table "report_expenses_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class ReportExpensesCategory extends ActiveRecordLogUpdateTime
{
    const CATEGORY_CALL_CENTER = 'call_center';
    const CATEGORY_DELIVERY = 'delivery';
    const CATEGORY_PRODUCT = 'product';
    const CATEGORY_BRANCH = 'branch';
    const CATEGORY_STORY = 'story';
    const CATEGORY_ROOT = 'root';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_category}}';
    }

    /**
     * @return ReportExpensesCategoryQuery
     */
    public static function find()
    {
        return new ReportExpensesCategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
            [['name', 'code'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return string
     */
    public function getFullCode()
    {
        return self::fullCode($this);
    }

    /**
     * @param self $category
     * @return string
     */
    public static function fullCode($category)
    {
        $code = self::CATEGORY_ROOT;
        if ($category) {
            $code = $category->code ?: 'category' . $category->id ?: self::CATEGORY_ROOT;
        }
        return $code;
    }
}
