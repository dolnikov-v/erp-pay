<?php

namespace app\modules\finance\models\filter;

use app\components\ModelTrait;
use app\modules\finance\models\ReportExpensesCountry;
use Yii;
use yii\base\Model;

/**
 * Class ReportExpensesCountryItem
 * @package app\modules\finance\models\filter
 */
class ReportProfitabilityAnalysisFilter extends Model
{
    use ModelTrait;

    /**
     * @var integer
     */
    public $country_id;

    /**
     * @var string
     */
    public $date;

    /**
     * @var integer
     */
    public $year;

    /**
     * @var integer
     */
    public $month;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'string'],
            [['country_id'], 'integer'],
            [['country_id', 'date'], 'required'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportExpensesCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Страна'),
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    protected function parseDate()
    {
        $this->month = substr($this->date, 0, 2);
        $this->year = substr($this->date, 3);
    }

    /**
     * @param array $params
     * @return bool
     */
    public function filter($params = [])
    {
        $this->load($params);
        if (strlen($this->date) == 6) {
            $this->date = '0' . $this->date;
        }
        $this->parseDate();
        return $this->validate();
    }

    public function isForecastPeriod()
    {
        return false;
        $year = date('Y');
        $month = date('m');

        return ($this->year >= $year && $this->month >= $month);
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }
}
