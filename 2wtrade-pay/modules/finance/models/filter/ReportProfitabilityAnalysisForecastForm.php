<?php

namespace app\modules\finance\models\filter;

use app\components\ModelTrait;
use Yii;
use yii\base\Model;

/**
 * Class ReportProfitabilityAnalysisForecastForm
 * @package app\modules\finance\models\filter
 */
class ReportProfitabilityAnalysisForecastForm extends Model
{
    use ModelTrait;

    /**
     * @var mixed
     */
    public $lead_per_day;

    /**
     * @var mixed
     */
    public $approve_percent;

    /**
     * @var mixed
     */
    public $buyout_percent;

    /**
     * @var mixed
     */
    public $refuse_percent;

    /**
     * @var array
     */
    public $in_process_percent;

    /**
     * @var int
     */
    public $operator_count;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operator_count'], 'integer'],
            [['lead_per_day', 'approve_percent', 'buyout_percent', 'refuse_percent', 'in_process_percent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lead_per_day' => Yii::t('common', 'Кол-во лидов'),
            'approve_percent' => Yii::t('common', 'Процент апрува'),
            'buyout_percent' => Yii::t('common', 'Процент выкупа'),
            'refuse_percent' => Yii::t('common', 'Процент отказа'),
            'operator_count' => Yii::t('common', 'Кол-во операторов'),
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 'sf';
    }
}
