<?php

namespace app\modules\finance\models\filter;

use app\components\ModelTrait;
use app\modules\finance\models\ReportExpensesCountry;
use Yii;
use yii\base\Model;

/**
 * Class ReportExpensesCountryItem
 * @package app\modules\finance\models\filter
 */
class ReportExpensesCountryItemFilter extends Model
{
    use ModelTrait;

    /**
     * @var integer
     */
    public $country_id;

    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['country_id'], 'required'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportExpensesCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['dateFrom', 'dateTo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' =>  Yii::t('common', 'Страна'),
            'date' =>  Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param array $params
     * @return bool
     */
    public function filter($params = [])
    {
        $this->load($params);
        if ($this->validate()) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }
}
