<?php
namespace app\modules\finance\models\query;

use app\components\db\ActiveQuery;
use app\modules\finance\models\ReportExpensesCountryCategory;

/**
 * Class ReportExpensesCountryCatalogQuery
 * @package app\modules\finance\models\query
 *
 * @method ReportExpensesCountryCategory one($db = null)
 * @method ReportExpensesCountryCategory[] all($db = null)
 */
class ReportExpensesCountryCategoryQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([ReportExpensesCountryCategory::tableName() . '.id' => $id]);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function byCountryId($id)
    {
        return $this->andWhere([ReportExpensesCountryCategory::tableName() . '.country_id' => $id]);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function byCategoryId($id)
    {
        return $this->andWhere([ReportExpensesCountryCategory::tableName() . '.category_id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([ReportExpensesCountryCategory::tableName() . '.active' => 1]);
    }
}
