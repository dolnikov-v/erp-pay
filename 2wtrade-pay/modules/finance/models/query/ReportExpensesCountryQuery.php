<?php
namespace app\modules\finance\models\query;

use app\components\db\ActiveQuery;
use app\modules\finance\models\ReportExpensesCountry;

/**
 * Class ReportExpensesCountryQuery
 * @package app\modules\finance\models\query
 *
 * @method ReportExpensesCountry one($db = null)
 * @method ReportExpensesCountry[] all($db = null)
 */
class ReportExpensesCountryQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([ReportExpensesCountry::tableName() . '.id' => $id]);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function byCountryId($id)
    {
        return $this->andWhere([ReportExpensesCountry::tableName() . '.country_id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([ReportExpensesCountry::tableName() . '.active' => 1]);
    }
}
