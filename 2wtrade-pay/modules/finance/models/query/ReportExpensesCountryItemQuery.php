<?php
namespace app\modules\finance\models\query;

use app\components\db\ActiveQuery;
use app\modules\finance\models\ReportExpensesCountryItem;
use yii\db\Query;

/**
 * Class ReportExpensesCountryItemQuery
 * @package app\modules\finance\models\query
 *
 * @method ReportExpensesCountryItem one($db = null)
 * @method ReportExpensesCountryItem[] all($db = null)
 */
class ReportExpensesCountryItemQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return $this
     */
    public function byCountryId($id)
    {
        return $this->andWhere([ReportExpensesCountryItem::tableName() . '.country_id' => $id]);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([ReportExpensesCountryItem::tableName() . '.id' => $id]);
    }

    /**
     * @param int $month
     * @param int $year
     * @return $this
     */
    public function byPeriod($month, $year)
    {
        return $this
            ->andWhere([ReportExpensesCountryItem::tableName() . '.month' => $month])
            ->andWhere([ReportExpensesCountryItem::tableName() . '.year' => $year]);
    }

    /**
     * @param int|array $id
     * @return $this
     */
    public function byParentId($id)
    {
        return $this->andWhere([ReportExpensesCountryItem::tableName() . '.parent_id' => $id]);
    }

    /**
     * @param int|array $id
     * @return $this
     */
    public function byCategory($id)
    {
        $null = false;
        if (is_array($id)) {
            foreach ($id as $key => $value) {
                if (!$value) {
                    $null = $key;
                    break;
                }
            }
            if ($null !== false) {
                unset($id[$null]);
                $null = true;
            }
        } elseif(!$id) {
            $null = true;
        }
        $condition = null;
        if ($id && $null) {
            $condition = ['or',
                [ReportExpensesCountryItem::tableName() . '.category_id' => $id],
                [ReportExpensesCountryItem::tableName() . '.category_id' => false],
                [ReportExpensesCountryItem::tableName() . '.category_id' => null],
            ];
        } elseif ($id) {
            $condition = [ReportExpensesCountryItem::tableName() . '.category_id' => $id];
        } elseif ($null) {
            $condition = ['or',
                [ReportExpensesCountryItem::tableName() . '.category_id' => false],
                [ReportExpensesCountryItem::tableName() . '.category_id' => null],
            ];
        }

        return ($condition) ? $this->andWhere($condition) : $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([ReportExpensesCountryItem::tableName() . '.active' => 1]);
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return $this
     */
    public function byDates($dateFrom, $dateTo)
    {
        return $this->andWhere([ReportExpensesCountryItem::tableName() . '.date_from' => date('Y-m-d', strtotime($dateFrom))])
                    ->andWhere([ReportExpensesCountryItem::tableName() . '.date_to' => date('Y-m-d', strtotime($dateTo))]);
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return $this
     */
    public function byDatesIn($dateFrom, $dateTo)
    {
        return $this
            ->andWhere([
                '<=',
                ReportExpensesCountryItem::tableName() . '.date_from',
                date('Y-m-d', strtotime($dateFrom))
            ])
            ->andWhere([
                '>=',
                ReportExpensesCountryItem::tableName() . '.date_to',
                date('Y-m-d', strtotime($dateTo))
            ]);
    }

    /**
     * @param $year
     * @param $month
     * @return $this
     */
    public function byYearMonth($year, $month)
    {
        $dateFrom = $year . '-' . $month . '-01';
        $dateTo = $year . '-' . $month . '-' . date('t', strtotime($dateFrom));

        $subQuery = ReportExpensesCountryItem::find()
            ->select([
                'diff' => 'MIN(DATEDIFF(date_to, date_from))',
                'country_id',
                'category_id',
                'parent_id'
            ])
            ->byDatesIn($dateFrom, $dateTo)
            ->groupBy([
                'country_id',
                'category_id',
                'parent_id'
            ]);

        $this->innerJoin(['sub' => $subQuery],
            '(sub.country_id=' . ReportExpensesCountryItem::tableName() . '.country_id and 
            sub.category_id=' . ReportExpensesCountryItem::tableName() . '.category_id and
            sub.parent_id=' . ReportExpensesCountryItem::tableName() . '.parent_id and
            sub.diff=DATEDIFF(' . ReportExpensesCountryItem::tableName() . '.date_to, ' . ReportExpensesCountryItem::tableName() . '.date_from))'
        );

        return $this->byDatesIn($dateFrom, $dateTo)
            ->select(ReportExpensesCountryItem::tableName() . '.*')
            ->addSelect(['diff' => 'DATEDIFF(date_to, date_from)']);
    }
}
