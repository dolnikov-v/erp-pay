<?php
namespace app\modules\finance\models\query;

use app\components\db\ActiveQuery;
use app\modules\finance\models\ReportExpensesItem;

/**
 * Class ReportExpensesItemQuery
 * @package app\modules\finance\models\query
 *
 * @method ReportExpensesItem one($db = null)
 * @method ReportExpensesItem[] all($db = null)
 */
class ReportExpensesItemQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([ReportExpensesItem::tableName() . '.id' => $id]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byCategory($id)
    {
        $null = false;
        if (is_array($id)) {
            foreach ($id as $key => $value) {
                if (!$value) {
                    $null = $key;
                    break;
                }
            }
            if ($null !== false) {
                unset($id[$null]);
                $null = true;
            }
        } elseif(!$id) {
            $null = true;
        }

        $condition = null;
        if ($id && $null) {
            $condition = ['or',
                [ReportExpensesItem::tableName() . '.category_id' => $id],
                [ReportExpensesItem::tableName() . '.category_id' => false],
                [ReportExpensesItem::tableName() . '.category_id' => null],
            ];
        } elseif ($id) {
            $condition = [ReportExpensesItem::tableName() . '.category_id' => $id];
        } elseif ($null) {
            $condition = ['or',
                [ReportExpensesItem::tableName() . '.category_id' => false],
                [ReportExpensesItem::tableName() . '.category_id' => null],
            ];
        }

        return ($condition) ? $this->andWhere($condition) : $this;
    }

    /**
     * @param $type
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere([ReportExpensesItem::tableName() . '.type' => $type]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([ReportExpensesItem::tableName() . '.active' => 1]);
    }

    /**
     * @param string $code
     * @return $this
     */
    public function byCode($code)
    {
        return $this->andWhere([ReportExpensesItem::tableName() . '.code' => $code]);
    }
}
