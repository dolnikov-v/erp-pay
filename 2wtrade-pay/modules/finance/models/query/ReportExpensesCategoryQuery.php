<?php
namespace app\modules\finance\models\query;

use app\components\db\ActiveQuery;
use app\modules\finance\models\ReportExpensesCategory;

/**
 * Class ReportExpensesCatalogQuery
 * @package app\modules\finance\models\query
 *
 * @method ReportExpensesCategory one($db = null)
 * @method ReportExpensesCategory[] all($db = null)
 */
class ReportExpensesCategoryQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([ReportExpensesCategory::tableName() . '.id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([ReportExpensesCategory::tableName() . '.active' => 1]);
    }
}
