<?php

namespace app\modules\finance\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Product;
use Yii;

/**
 * This is the model class for table "report_expenses_country_item_product".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $product_id
 * @property string $value
 * @property integer $lead_limit
 * @property string $date
 * @property string $date_from
 * @property string $date_to
 *
 * @property ReportExpensesCountryItem $item
 * @property Product $product
 */
class ReportExpensesCountryItemProduct extends ActiveRecordLogUpdateTime
{

    /**
     * Значение по умолчанию для стоимости продукта
     */
    const DEFAULT_UNIT_COST = 3;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_expenses_country_item_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'product_id', 'lead_limit'], 'integer'],
            [['date', 'date_from', 'date_to'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportExpensesCountryItem::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'item_id' => Yii::t('common', 'Статья расходов'),
            'product_id' => Yii::t('common', 'Товар'),
            'value' =>  Yii::t('common', 'Значение'),
            'lead_limit' =>  Yii::t('common', 'Лимит лидов'),
            'date' => Yii::t('common', 'Дата'),
            'date_from' => Yii::t('common', 'Дата от'),
            'date_to' => Yii::t('common', 'Дата до'),
        ];
    }

    /**
     * @param $parentCode
     * @return array
     */
    public static function getFormTitlesByParentCode($parentCode)
    {
        $titles = [
            'value' => '',
            'lead_limit' => '',
            'date' => '',
            'date_from' => '',
            'date_to' => '',
        ];
        switch ($parentCode) {
            case ReportExpensesItem::UNIT_COST_ITEM_CODE;
                $titles['value'] = Yii::t('common', 'Стоимость');
                $titles['lead_limit'] = Yii::t('common', 'Лимит лидов');
                break;
            case ReportExpensesItem::ADCOMBO_GTD_APPROVE_PERCENT;
                $titles['value'] = Yii::t('common', '% Гаранта апрува');
                break;
            case ReportExpensesItem::ADCOMBO_PENALTY_ITEM_CODE;
                $titles['value'] = Yii::t('common', 'Сумма штрафа');
                $titles['lead_limit'] = Yii::t('common', 'Кол-во лидов');
                $titles['date'] = Yii::t('common', 'Дата штрафа');
                $titles['date_from'] = Yii::t('common', 'Период штрафа от');
                $titles['date_to'] = Yii::t('common', 'Период штрафа до');
                break;
        }
        return $titles;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(ReportExpensesCountryItem::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ReportExpensesCountryItem::className(), ['id' => 'item_id']);
    }
}
