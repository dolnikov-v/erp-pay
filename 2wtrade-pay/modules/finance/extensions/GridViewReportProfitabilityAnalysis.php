<?php
namespace app\modules\finance\extensions;

use app\modules\finance\components\EstimateItem;
use app\widgets\Label;
use kartik\grid\GridView;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class GridViewProfitabilityAnalysis
 * @package app\modules\finance\extensions
 */
class GridViewReportProfitabilityAnalysis extends GridView
{
    /**
     * @var ArrayDataProvider
     */
    public $dataProvider;

    /**
     * @var string
     */
    public $tableType;

    /**
     * @var string = normal|inverted
     */
    public $renderOrientation = 'inversion';

    /**
     * @var array
     */
    public $groupCollection = [];

    /**
     * @var array
     */
    public $categoryCollection = [];

    /**
     * @var array
     */
    public $categoryItems = [];

    /**
     * @var array
     */
    private $itemsNames = [];

    /**
     * @var array
     */
    private $itemsTitles = [];

    public function init()
    {
        $this->dataProvider->pagination = false;

        if ($this->renderOrientation == 'normal') {
            $this->renderNormalView();
        } elseif ($this->renderOrientation == 'inversion') {
            $this->renderInversionView();
        }

        parent::init();
    }

    private function renderNormalView()
    {
        $models = $this->dataProvider->getModels();
        $data = [];
        foreach ($models as $key => $model) {
            $this->itemsNames[$key] = $model['name'];
            $this->itemsTitles[$key] = $model['title'] ?? null;
            $data['all'][$key] = $model['total'];
        }
        foreach ($this->groupCollection as $id => $name) {
            foreach ($models as $key => $model) {
                $value = ArrayHelper::getValue($model['detailValues'], $id);
                $data[$id][$key] = $value;
            }
        }
        $this->dataProvider->setModels($data);
        $this->dataProvider->setKeys(array_keys($data));

        $this->rowOptions = function ($model, $index, $key) {
            $options = [];
            if ($index == 'all') {
                $options['class'] = 'warning';
            }
            return $options;
        };
    }

    private function renderInversionView()
    {
        $models = $this->dataProvider->getModels();
        $prev = false;
        foreach ($models as $key => $model) {
            if ($model['total'] == 0 && (!isset($model['detailValues']) || empty((float)implode($model['detailValues'])))) {
                unset($models[$key]);
            } else {
                if ($prev !== false) {
                    $models[$prev]['lastInGroup'] = $models[$prev]['group'] !== $model['group'];
                }
                $prev = $key;
            }
        }
        if ($this->tableType == 'estimate') {
            $total = null;
            foreach ($models as $model) {
                if ($model['code'] == EstimateItem::COSTS) {
                    $total = $model['total'];
                    break;
                }
            }
            foreach ($models as &$model) {
                $model['total_percent'] = ($model['total'] && $total) ? round($model['total'] / $total * 100, 2) : null;
            } unset($model);
        }

        $this->dataProvider->setModels($models);
        $this->rowOptions = function ($model, $index, $key) {
            $options = [];

            if ($model['group']) {
                $options['data-group'] = $model['group'];
                if (!$model['isParent']) {
                    $options['class'] = 'hidden active';
                    if (isset($model['lastInGroup']) && $model['lastInGroup']) {
                        $options['class'] .= ' close-group last-in-group';
                    }
                } else {
                    $options['class'] = 'root-group';
                }
            } else {
                if (($this->tableType == 'estimate' && $model['code'] == EstimateItem::COSTS)) {
                    $options['class'] = 'warning';
                }
            }
            return $options;
        };
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        if ($this->renderOrientation == 'normal') {
            $this->initColumnsForNormalOrientation();
        } elseif ($this->renderOrientation == 'inversion') {
            $this->initColumnsForInvertedOrientation();
        }

        parent::initColumns();
    }

    private function initColumnsForNormalOrientation() {
        if ($this->tableType === 'finance') {
            $this->columns[] = [
                'label' => Yii::t('common', 'Категория товаров'),
                'content' => function ($model, $key) {
                    return ($key == 'all') ? '—' : $this->categoryCollection[$key] ?? Yii::t('common', 'Нет категории');
                },
                'contentOptions' => [
                    'style' => ['width' => '170px']
                ],
                'headerOptions' => ['style' => ['width' => '170px']],
                'group' => true,

                'groupHeader' => function ($model, $key) {
                    if ($key !== 'all') {
                        return [
                            'mergeColumns' => [[0, 1]],
                            'content' => [
                                0 => $this->categoryCollection[$key] ?? Yii::t('common', 'Нет категории'),
                                2 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_LEAD_COUNT],
                                3 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_APPROVE],
                                4 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_DELIVERY],
                                5 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_PRODUCT_QUANTITY],
                                6 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_PRODUCT_BUYOUT_QUANTITY],
                                7 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_PRODUCT_REFUSE_QUANTITY],
                                8 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_AVG_LEAD_PRICE],
                                9 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_AVG_BUYOUT_CHECK],
                                10 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_BUYOUT],
                                11 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_BUYOUT_PERCENT],
                                12 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_REFUSE],
                                13 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_REFUSE_PERCENT],
                                14 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_IN_PROCESS],
                                15 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_IN_PROCESS_PERCENT],
                                16 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED],
                                17 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_ORDERS_MONEY_RECEIVED_PERCENT],
                                18 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::FINANCE_BUYOUT_INCOME],
                                19 => $this->categoryItems[$this->categoryCollection[$key]][EstimateItem::COSTS],
                            ],
                            'contentOptions' => [
                                2 => ['style' => 'text-align:center'],
                                3 => ['style' => 'text-align:center'],
                                4 => ['style' => 'text-align:center'],
                                5 => ['style' => 'text-align:center'],
                                6 => ['style' => 'text-align:center'],
                                7 => ['style' => 'text-align:center'],
                                8 => ['style' => 'text-align:center'],
                                9 => ['style' => 'text-align:center'],
                                10 => ['style' => 'text-align:center'],
                                11 => ['style' => 'text-align:center'],
                                12 => ['style' => 'text-align:center'],
                                13 => ['style' => 'text-align:center'],
                                14 => ['style' => 'text-align:center'],
                                15 => ['style' => 'text-align:center'],
                                16 => ['style' => 'text-align:center'],
                                17 => ['style' => 'text-align:center'],
                                18 => ['style' => 'text-align:center'],
                                19 => ['style' => 'text-align:center'],
                            ],
                            'options' => ['class' => 'custom-warning']
                        ];
                    }
                }
            ];
        }
        $this->columns[] = [
            'label' => Yii::t('common', 'Товар'),
            'content' => function ($model, $key) {
                return ($key == 'all') ? Yii::t('common', 'Итого') : $this->groupCollection[$key];
            },
            'contentOptions' => [
                'style' => ['width' => '170px']
            ],
            'headerOptions' => ['style' => ['width' => '170px']],
        ];
        foreach ($this->itemsNames as $id => $name) {

            $header = $name;
            if ($title = $this->itemsTitles[$id]) {
                $label = Label::widget([
                    'size' => Label::SIZE_SMALL,
                    'label' => 'fn'
                ]);
                $header .= Html::tag('span', $label, [
                    'title' => $title,
                    'data-toggle' => 'tooltip',
                    'data-container' => 'body',
                    'class' => 'padding-left-3',
                ]);
            }

            $this->columns[] = [
                'header' => $header,
                'content' => function ($model) use ($id) {
                    $value = ArrayHelper::getValue($model, $id);
                    if (is_numeric($value)) {
                        $decPoint = ((int) $value == $value) ? 0 : 2;
                        $value = number_format($value, $decPoint, '.', ' ');
                    }
                    return $value;
                },
                'contentOptions' => [
                    'style' => [
                        'text-align' => 'center', 'width' => '100px',
                    ]
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'format' => 'numeric'
            ];
        }
    }

    private function initColumnsForInvertedOrientation() {
        $this->columns[] = [
            'attribute' => 'name',
            'label' => Yii::t('common', 'Название'),
            'content' => function ($model) {
                $content = $model['name'];
                if ($model['isParent']) {
                    $label = Label::widget([
                        'size' => Label::SIZE_SMALL,
                        'label' => '+'
                    ]);
                    $content .= Html::tag('span', $label, [
                        'class' => 'padding-left-3',
                    ]);
                }
                if ($model['title']) {
                    $label = Label::widget([
                        'size' => Label::SIZE_SMALL,
                        'label' => 'fn'
                    ]);
                    $content .= Html::tag('span', $label, [
                        'title' => $model['title'],
                        'data-toggle' => 'tooltip',
                        'data-container' => 'body',
                        'class' => 'padding-left-3',
                    ]);
                }
                return $content;
            },
            'contentOptions' => ['style' => ['width' => '170px']],
            'headerOptions' => ['style' => ['width' => '170px']],
        ];
        foreach ($this->groupCollection as $id => $name) {
            $this->columns[] = [
                'label' => $name,
                'content' => function ($model) use ($id) {
                    $value = ArrayHelper::getValue($model['detailValues'], $id);
                    if (is_numeric($value)) {
                        $decPoint = ((int) $value == $value) ? 0 : 2;
                        $value = number_format($value, $decPoint, '.', ' ');
                    }
                    return Html::tag("span", $value, [
                        "title" => ArrayHelper::getValue($model['descriptions'], $id)
                    ]);
                },
                'contentOptions' => [
                    'style' => ['text-align' => 'center', 'width' => '100px']
                ],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Итого'),
            'content' => function ($model) {
                $value = $model['total'];
                if (is_numeric($value)) {
                    $decPoint = ((int) $value == $value) ? 0 : 2;
                    $value = number_format($value, $decPoint, '.', ' ');
                }
                return Html::tag("span", $value, [
//                    TODO: разобраться почему пустое totalDescription
                    "title" => (!empty($model['totalDescription'])?$model['totalDescription']:'')
                ]);
            },
            'contentOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
            'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
        ];

        if ($this->tableType == 'estimate') {
            $this->columns[] = [
                'label' => Yii::t('common', 'Итого (%)'),
                'content' => function ($model) {
                    // не считать процент по штрафам за рекламу
                    if ($model['code'] == EstimateItem::PENALTY_FOR_LEADS) {
                        return '';
                    }
                    return $model['total_percent'];
                },
                'contentOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '100px']],
            ];
        }
    }
}