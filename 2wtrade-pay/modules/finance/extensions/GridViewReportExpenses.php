<?php
namespace app\modules\finance\extensions;

use app\components\grid\GridView;
use Yii;

/**
 * Class GridViewExpenseAnalysis
 * @package app\modules\finance\extensions
 */
class GridViewReportExpenses extends GridView
{
    /**
     * @var array
     */
    public $countries;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'attribute' => 'expense_name',
            'label' => Yii::t('common', 'Статьи расходов'),
            'contentOptions' => ['style' => ['width' => '200px']],
            'headerOptions' => ['style' => ['width' => '200px']],
        ];

        foreach ($this->countries as $id => $name) {
            $this->columns[] = [
                'label' => Yii::t('common', $name),
                'attribute' => 'country_' . $id,
                'value' => function($model) use ($id) {
                    return (is_numeric($model['country_' . $id]) ? round($model['country_' . $id], 2) : null);
                },
                'contentOptions' => ['style' => ['text-align' => 'center', 'width' => '140px']],
                'headerOptions' => ['style' => ['text-align' => 'center', 'width' => '140px']],
            ];
        }

        parent::initColumns();
    }
}