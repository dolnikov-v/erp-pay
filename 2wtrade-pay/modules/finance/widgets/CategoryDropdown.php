<?php
namespace app\modules\finance\widgets;

use app\widgets\ButtonLink;
use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/**
 * Class CategoryDropdown
 * @package app\modules\finance\widgets
 */
class CategoryDropdown extends Widget
{
    /**
     * @var array
     */
    public $categoryList = [];

    /**
     * @var int
     */
    public $countryId;

    /**
     * @return string
     */
    public function run()
    {
        $items = [];
        foreach ($this->categoryList as $id => $name) {
            $items[] = [
                'label' => $name,
                'url' => Url::toRoute(['/finance/report-expenses-country-item/edit-category',
                    'category_id' => $id,
                    'country_id' => $this->countryId,
                ])
            ];
        }
        $items[] = [
            'label' => Yii::t('common', 'Новая'),
            'url' => Url::toRoute(['/finance/report-expenses-country-item/edit-category',
                'category_id' => null,
                'country_id' => $this->countryId,
            ])
        ];

        return ButtonDropdown::widget([
            'label' => Yii::t('common', 'Добавить'),
            'options' => ['class' => ButtonLink::STYLE_SUCCESS . ' ' . ButtonLink::SIZE_SMALL],
            'dropdown' => ['items' => $items]
        ]);
    }
}
