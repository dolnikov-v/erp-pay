<?php
namespace app\modules\finance\widgets;

use yii\base\Widget;

/**
 * Class ExportDropdown
 * @package app\modules\finance\widgets
 */
class ExportDropdown extends Widget
{
    /**
     * @var string
     */
    public $name = 'Экспорт';

    /**
     * @var array
     */
    public $extraParams = null;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-dropdown/index', [
            'name' => $this->name,
            'extraParams' => $this->extraParams,
        ]);
    }
}
