<?php

use app\widgets\Select2;
use app\modules\finance\models\ReportExpensesCountryItemProduct;
use app\modules\finance\models\ReportExpensesItem;
use yii\helpers\ArrayHelper;
use app\widgets\DateTimePicker;
use app\widgets\DateRangePicker;
use app\helpers\i18n\DateTimePicker as DateTimePickerHelper;
use app\widgets\Button;


/** @var $this \yii\web\View */
/** @var $productList array[] */
/** @var $productCosts array[] */
/** @var $parentCode string */
/** @var $titles array */

$disableMode = 1;
if ($parentCode == ReportExpensesItem::ADCOMBO_PENALTY_ITEM_CODE) {
    $disableMode = 0;
}

?>

<div class="container-product-list" data-disable="<?= $disableMode ?>">

    <?php foreach ($productCosts as $item) { ?>
        <?php /** @var $item ReportExpensesCountryItemProduct */ ?>
        <div class="product-list-element row" data-id="<?= $item->id ?>">
            <div class="form-group col-lg-2">
                <input name="products[<?= $item->id ?>]" value="<?= $item->product_id ?>" type="hidden">
                <input class="form-control" value="<?= $productList[$item->product_id] ?>" type="text" disabled>
            </div>

            <?php
            if ($titles['date'] != ''): ?>
                <div class="form-group col-lg-2">
                    <?= DateTimePicker::widget([
                        'value' => $item->date,
                        'name' => 'dates[' . $item->id . ']',
                        'attributes' =>
                            [
                                'title' => $titles['date'],
                                'placeholder' => $titles['date']
                            ]
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php
            if ($titles['date_from'] != ''): ?>
                <div class="form-group col-lg-3">
                    <?= DateRangePicker::widget([
                        'value' => $item->date,
                        'nameFrom' => 'dates_from[' . $item->id . ']',
                        'nameTo' => 'dates_to[' . $item->id . ']',
                        'valueFrom' => $item->date_from,
                        'valueTo' => $item->date_to,
                        'titleFrom' => $titles['date_from'],
                        'titleTo' => $titles['date_to'],
                        'placeholderFrom' => $titles['date_from'],
                        'placeholderTo' => $titles['date_to'],
                        'hideRanges' => true
                    ]) ?>
                </div>
            <?php endif; ?>

            <div class="form-group col-lg-2">
                <input class="form-control" name="values[<?= $item->id ?>]" value="<?= $item->value ?>"
                       type="text" placeholder="<?= $titles['value'] ?>" title="<?= $titles['value'] ?>">
            </div>
            <?php if ($titles['lead_limit'] != ''): ?>
                <div class="form-group col-lg-2">
                    <input class="form-control" name="limits[<?= $item->id ?>]" value="<?= $item->lead_limit ?>"
                           type="text" placeholder="<?= $titles['lead_limit'] ?>" title="<?= $titles['lead_limit'] ?>">
                </div>
            <?php endif; ?>

            <button class="btn btn-danger btn-product-list delete-product-element" type="button">
                <i class="icon wb-minus"></i>
            </button>
        </div>

    <?php } ?>

    <div class="product-list-element row">
        <div class="form-group col-lg-2">
            <?= Select2::widget([
                'items' => $productList,
                'disabledItems' => $disableMode ? array_keys(ArrayHelper::index($productCosts, 'product_id')) : []
            ]) ?>
        </div>
        <button class="btn btn-success btn-product-list add-product-element" type="button">
            <i class="icon wb-plus"></i>
        </button>
    </div>

    <div class="product-list-element row hidden">
        <div class="form-group col-lg-2">
            <input class="product_id" type="hidden">
            <input class="form-control product_name" type="text" disabled>
        </div>

        <?php if ($titles['date'] != ''): ?>
            <div class="form-group col-lg-2">
                <div class="input-group">
                    <span class="input-group-btn">
                        <?= Button::widget([
                            'icon' => 'wb-calendar',
                        ]) ?>
                    </span>
                    <input class="form-control margin-left-0 dates"
                           data-format="<?= DateTimePickerHelper::getDateFormat() ?>"
                           data-locale="<?= DateTimePickerHelper::getLocale() ?>"
                           placeholder="<?= Yii::t('common', $titles['date']) ?>"
                           title="<?= Yii::t('common', $titles['date']) ?>">
                </div>
            </div>
        <?php endif; ?>

        <?php
        if ($titles['date_from'] != ''): ?>
            <div class="form-group col-lg-3">

                <div class="container-daterangepicker">
                    <div class="inputs-daterangepicker" style="width: auto;">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="icon wb-calendar" aria-hidden="true"></i>
                            </span>
                            <input class="form-control margin-left-0 dates_from"
                                   data-format="<?= DateTimePickerHelper::getDateFormat() ?>"
                                   data-locale="<?= DateTimePickerHelper::getLocale() ?>"
                                   placeholder="<?= Yii::t('common', $titles['date_from']) ?>"
                                   title="<?= Yii::t('common', $titles['date_from']) ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">—</span>
                            <input class="form-control margin-left-0 dates_to"
                                   data-format="<?= DateTimePickerHelper::getDateFormat() ?>"
                                   data-locale="<?= DateTimePickerHelper::getLocale() ?>"
                                   placeholder="<?= Yii::t('common', $titles['date_to']) ?>"
                                   title="<?= Yii::t('common', $titles['date_to']) ?>">
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group col-lg-2">
            <input class="form-control margin-left-0 values" placeholder="<?= Yii::t('common', $titles['value']) ?>"
                   title="<?= Yii::t('common', $titles['value']) ?>">
        </div>

        <?php if ($titles['lead_limit'] != ''): ?>
            <div class="form-group col-lg-2">
                <input class="form-control margin-left-0 limits" type="text" placeholder="<?= $titles['lead_limit'] ?>"
                       title="<?= $titles['lead_limit'] ?>">
            </div>
        <?php endif; ?>

        <button class="btn btn-danger btn-product-list delete-product-element" type="button">
            <i class="icon wb-minus"></i>
        </button>
    </div>

</div>