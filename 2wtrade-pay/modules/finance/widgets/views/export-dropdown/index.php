<?php
use app\modules\finance\components\exporter\ExporterExcel;

/** @var string $name */
/** @var array $extraParams */
?>

<div id="orders_export_box">
    <div class="btn-group">
        <button id="orders_export_btn" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-share-square-o"></i> <?= Yii::t('common', $name) ?><span class="caret"></span>
        </button>
        <ul id="orders_export_dropdown" class="dropdown-menu dropdown-menu-right">
            <li>
                <a class="export-xls" href="<?= ExporterExcel::getExporterUrl() ?>" tabindex="-1">
                    <i class="text-success fa fa-file-excel-o"></i> Excel
                </a>
            </li>
        </ul>
    </div>
</div>
