<?php

namespace app\modules\finance\widgets;

use app\modules\finance\models\ReportExpensesCountryItemProduct;
use app\widgets\Widget;

/**
 * Class EmailList
 * @package app\modules\finance\widgets
 */
class ProductList extends Widget
{
    /**
     * @var array[]
     */
    public $productList;

    /**
     * @var array[]
     */
    public $productCosts;

    /**
     * @var string
     */
    public $parentCode;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('product-list/index', [
            'productList' => $this->productList,
            'productCosts' => $this->productCosts,
            'parentCode' => $this->parentCode,
            'titles' => $titles = ReportExpensesCountryItemProduct::getFormTitlesByParentCode($this->parentCode),
        ]);
    }
}
