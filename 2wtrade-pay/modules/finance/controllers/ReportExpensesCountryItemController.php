<?php

namespace app\modules\finance\controllers;

use app\components\web\Controller;
use app\models\Country;
use app\models\Product;
use app\modules\delivery\models\Delivery;
use app\modules\finance\models\filter\ReportExpensesCountryItemFilter;
use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\models\ReportExpensesCountryCategory;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\finance\models\ReportExpensesCountryItemProduct;
use app\modules\finance\models\ReportExpensesItem;
use app\modules\salary\models\Office;
use Yii;
use yii\base\Exception;
use yii\base\Response;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ReportExpensesCountryItemController
 * @package app\modules\finance\controllers
 */
class ReportExpensesCountryItemController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $systemCountries = Country::find()->bySystemUserCountries()->column();

        $countryCollection = ReportExpensesCountry::find()
            ->where(['country_id' => $systemCountries])
            ->orderBy(['name' => SORT_ASC])
            ->collection();

        $filterForm = new ReportExpensesCountryItemFilter([
            'country_id' => array_keys($countryCollection)[0],
            'dateFrom' => date('Y-m-01'),
            'dateTo' => date('Y-m-t'),
        ]);
        $filterForm->filter(Yii::$app->request->get());

        ReportExpensesCountryItem::checkCloneLast($filterForm->country_id, $filterForm->dateFrom, $filterForm->dateTo);

        $countryCategories = ReportExpensesCountryCategory::find()
            ->joinWith(['category'], false)
            ->byCountryId($filterForm->country_id)
            ->orderBy([
                ReportExpensesCategory::tableName() . '.name' => SORT_ASC,
                ReportExpensesCountryCategory::tableName() . '.name' => SORT_ASC,
            ])
            ->all();

        $categoryCollection = ReportExpensesCategory::find()
            ->orderBy(['name' => SORT_ASC])
            ->collection();

        $items = [];
        foreach ($countryCategories as $countryCategory) {
            $countryExpenses = ReportExpensesCountryItem::find()
                ->joinWith(['parent'], false)
                ->byCategory($countryCategory->id)
                ->byDates($filterForm->dateFrom, $filterForm->dateTo)
                ->byCountryId($filterForm->country_id)
                ->indexBy('parent_id')
                ->all();

            $dataProvider = new ActiveDataProvider([
                'query' => ReportExpensesItem::find()
                    ->with(['category'])
                    ->byCategory($countryCategory->category_id)
                    ->active(),
                'pagination' => false,
            ]);

            $items[] = [
                'id' => $countryCategory->id,
                'name' => $countryCategory->name,
                'dataProvider' => $dataProvider,
                'countryExpenses' => $countryExpenses,
            ];
        }

        $countryExpenses = ReportExpensesCountryItem::find()
            ->joinWith(['parent'], false)
            ->andWhere([
                'or',
                [ReportExpensesItem::tableName() . '.category_id' => null],
                [ReportExpensesItem::tableName() . '.category_id' => false],
            ])
            ->byDates($filterForm->dateFrom, $filterForm->dateTo)
            ->byCountryId($filterForm->country_id)
            ->indexBy('parent_id')
            ->all();

        $dataProvider = new ActiveDataProvider([
            'query' => ReportExpensesItem::find()
                ->with(['category'])
                ->byCategory(false)
                ->active(),
            'pagination' => false,
        ]);
        $items[] = [
            'id' => null,
            'name' => Yii::t('common', 'Без категории'),
            'dataProvider' => $dataProvider,
            'countryExpenses' => $countryExpenses,
        ];

        $dataProvider = new ArrayDataProvider(['models' => $countryCategories]);

        return $this->render('index', [
            'items' => $items,
            'categoryDataProvider' => $dataProvider,
            'filterForm' => $filterForm,
            'countryCategories' => $countryCategories,
            'countryCollection' => $countryCollection,
            'categoryCollection' => $categoryCollection,
        ]);
    }

    /**
     * @param integer $id
     * @param integer $country_id
     * @param integer $category_id
     * @return string|Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEditCategory($id = null, $country_id, $category_id = null)
    {
        if (!is_null($id)) {
            $model = $this->findCategoryModel($id);
        } else {
            $model = new ReportExpensesCountryCategory([
                'country_id' => $country_id,
                'category_id' => $category_id,
            ]);
        }
        if ($post = Yii::$app->request->post()) {
            $model->load($post);
            if ($model->save()) {
                return $this->redirect(['index', 's' => ['country_id' => $model->country_id]]);
            }
        }
        $categoryCollection = ReportExpensesCategory::find()->collection();
        $officeCollection = [];
        if ($model->country && $model->country->country_id) {
            $officeCollection = Office::find()->byCountryId($model->country->country_id)->collection();
        }

        $deliveryCollection = [];
        if ($model->country && $model->country->country_id) {
            $deliveryCollection = Delivery::find()->byCountryId($model->country->country_id)->collection();
        }

        return $this->render('edit-category', [
            'model' => $model,
            'categoryCollection' => $categoryCollection,
            'deliveryCollection' => $deliveryCollection,
            'officeCollection' => $officeCollection,
        ]);
    }

    /**
     * @param integer $id
     * @param integer $country_id
     * @param string $dateFrom
     * @param string $dateTo
     * @param integer $category_id
     * @return string|Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id, $country_id, $dateFrom, $dateTo, $category_id = null)
    {
        $model = $this->getModelByParent($id, $country_id, $dateFrom, $dateTo, $category_id);
        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $model->formula = strtr($model->formula, [
                PHP_EOL => '',
                '<br />' => PHP_EOL,
                '&nbsp;' => ' ',
            ]);
            $model->formula = strip_tags($model->formula);
            $model->formula = trim($model->formula);

            if ($model->save()) {
                $values = Yii::$app->request->post('values');
                $products = Yii::$app->request->post('products');
                $limits = Yii::$app->request->post('limits');
                $dates = Yii::$app->request->post('dates');
                $datesFrom = Yii::$app->request->post('dates_from');
                $datesTo = Yii::$app->request->post('dates_to');

                if ($products) {
                    $oldItems = ReportExpensesCountryItemProduct::find()
                        ->select('id')
                        ->where(['item_id' => $model->id])
                        ->column();
                    if ($oldItems) {
                        $oldItems = array_combine($oldItems, $oldItems);
                    }

                    foreach ($products as $id => $productId) {

                        $item = null;
                        if ($id) {
                            $item = ReportExpensesCountryItemProduct::findOne($id);
                        }
                        if (!$item) {
                            $item = new ReportExpensesCountryItemProduct();
                        }
                        $item->item_id = $model->id;
                        $item->value = $values[$id] ?? null;
                        $item->value = str_replace(',', '.', $item->value);
                        $item->lead_limit = $limits[$id] ?? null;
                        $item->date = !empty($dates[$id]) ? date('Y-m-d', strtotime($dates[$id])) : null;
                        $item->date_from = !empty($datesFrom[$id]) ? date('Y-m-d', strtotime($datesFrom[$id])) : null;
                        $item->date_to = !empty($datesTo[$id]) ? date('Y-m-d', strtotime($datesTo[$id])) : null;
                        $item->product_id = $productId;
                        $item->save();
                        unset($oldItems[$item->id]);
                    }

                    if ($oldItems) {
                        ReportExpensesCountryItemProduct::deleteAll(['id' => $oldItems]);
                    }
                } else {
                    ReportExpensesCountryItemProduct::deleteAll(
                        ['item_id' => $model->id]
                    );
                }

                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Статья расхода успешно добавлена.') : Yii::t('common', 'Статья расхода успешно сохранена.'), 'success');
                return $this->redirect([
                    'index',
                    's' => [
                        'country_id' => $model->country_id,
                        'dateFrom' => $model->date_from,
                        'dateTo' => $model->date_to,
                    ]
                ]);
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $productCosts = [];
        if ($model->isNewRecord) {
            $model->category_id = $category_id;
        } else {
            $productCosts = ReportExpensesCountryItemProduct::find()
                ->where(['item_id' => $model->id])
                ->all();
        }
        $productList = Product::find()->orderBy(['name' => SORT_ASC])->collection();

        return $this->render('edit', [
            'model' => $model,
            'productList' => $productList,
            'productCosts' => $productCosts,
        ]);
    }

    /**
     * @param $id
     * @param $country_id
     * @param string $dateFrom
     * @param string $dateTo
     * @param $category_id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDelete($id, $country_id, $dateFrom, $dateTo, $category_id = null)
    {
        $model = $this->getModelByParent($id, $country_id, $dateFrom, $dateTo, $category_id);
        if ($model && $model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Статья расхода успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect([
            'index',
            's' => [
                'country_id' => $model->country_id,
                'dateFrom' => $model->date_from,
                'dateTo' => $model->date_to,
            ]
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDeleteCategory($id)
    {
        $model = $this->findCategoryModel($id);
        if ($model && $model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Статья расхода успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['index', 's' => ['country_id' => $model->country_id]]);
    }


    /**
     * @param $id
     * @param $countryId
     * @param $dateFrom
     * @param $dateTo
     * @param $category_id
     * @return ReportExpensesCountryItem
     */
    protected function getModelByParent($id, $countryId, $dateFrom, $dateTo, $category_id)
    {
        $parent = $this->findParentModel($id);

        $params = [
            'parent_id' => $id,
            'country_id' => $countryId,
            'category_id' => $category_id,
            'date_from' => date('Y-m-d', strtotime($dateFrom)),
            'date_to' => date('Y-m-d', strtotime($dateTo)),
        ];
        $model = ReportExpensesCountryItem::findOne($params) ?: new ReportExpensesCountryItem($params);
        if ($model->isNewRecord) {
            $model->type = $parent->type;
            $model->active = $parent->active;
            $model->value = $parent->value;
            $model->formula = $parent->formula;
        }

        return $model;
    }

    /**
     * @param $id
     * @return ReportExpensesItem
     * @throws NotFoundHttpException
     */
    protected function findParentModel($id)
    {
        $model = ReportExpensesItem::find()->byId($id)->one();
        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Статья расходов не найдена.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return ReportExpensesCountryCategory
     * @throws NotFoundHttpException
     */
    protected function findCategoryModel($id)
    {
        $model = ReportExpensesCountryCategory::find()->byId($id)->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Категория не найдена.'));
        }

        return $model;
    }
}
