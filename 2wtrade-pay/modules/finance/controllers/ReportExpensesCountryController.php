<?php

namespace app\modules\finance\controllers;

use app\models\Country;
use app\modules\finance\models\ReportExpensesCountry;
use Yii;
use yii\data\ActiveDataProvider;
use app\components\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ReportExpensesCountryController
 * @package app\modules\finance\controllers
 */
class ReportExpensesCountryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(['query' => ReportExpensesCountry::find()]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id = null)
    {
        $model = ($id) ? $this->findModel($id) : new ReportExpensesCountry();
        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;
            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Страна успешно добавлена.') : Yii::t('common', 'Страна успешно сохранена.'), 'success');
                return $this->redirect(['index']);
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $countryList = Country::find()->orderBy(['name' => SORT_ASC])->collection();

        return $this->render('edit', [
            'model' => $model,
            'countryList' => $countryList,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Страна успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return ReportExpensesCountry
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = ReportExpensesCountry::find()->byId($id)->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Страна не найдена.'));
        }

        return $model;
    }
}
