<?php

namespace app\modules\finance\controllers;

use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\finance\models\ReportExpensesItem;
use app\components\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class ReportExpensesController
 * @package app\modules\finance\controllers
 */
class ReportExpensesController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $expenses = ReportExpensesItem::find()->orderBy('name')->all();
        $countryExpenses = ReportExpensesCountryItem::find()->byYearMonth(date('Y'), date('m'))->all();
        $countryExpenses = ArrayHelper::index($countryExpenses, 'parent_id', ['country_id']);

        $countries = ReportExpensesCountry::find()
            ->active()
            ->orderBy(['name' => SORT_ASC])
            ->collection();

        $data = [];
        foreach ($expenses as $expense) {
            $row['expense_name'] = $expense->name;
            foreach ($countries as $id => $name) {
                $value = null;
                $item = ArrayHelper::getValue($countryExpenses, "{$id}.{$expense->id}");
                if ($item && $item->active) {
                    if ($item->type == ReportExpensesItem::TYPE_REGULAR) {
                        $value = $item->value;
                    } elseif ($item->type == ReportExpensesItem::TYPE_VARIABLE) {

                    }
                } else {
                    if ($expense->type == ReportExpensesItem::TYPE_REGULAR) {
                        $value = $expense->value;
                    } elseif ($expense->type == ReportExpensesItem::TYPE_VARIABLE) {

                    }
                }
                $row['country_' . $id] = $value;
            }
            $data[] = $row;
        }

        $dataProvider = new ArrayDataProvider(['models' => $data]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'countries' => $countries,
        ]);
    }
}
