<?php

namespace app\modules\finance\controllers;

use app\modules\finance\models\ReportExpensesCategory;
use app\modules\finance\models\ReportExpensesItem;
use Yii;
use yii\data\ActiveDataProvider;
use app\components\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ReportExpensesItemController
 * @package app\modules\finance\controllers
 */
class ReportExpensesItemController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = ReportExpensesItem::find()->with(['category']);
        if (!Yii::$app->request->get('sort')) {
            $query->orderBy(['name' => SORT_ASC]);
        }
        $categories = ReportExpensesCategory::find()->orderBy('name')->all();
        $items = [];
        foreach ($categories as $category) {
            $cloneQuery = clone $query;
            $cloneQuery->byCategory($category->id);
            $dataProvider = new ActiveDataProvider([
                'query' => $cloneQuery,
                'pagination' => false,
            ]);
            $items[] = [
                'name' => $category->name,
                'dataProvider' => $dataProvider,
            ];
        }

        $query->byCategory(false);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        $items[] = [
            'name' => 'Без категории',
            'dataProvider' => $dataProvider,
        ];

        return $this->render('index', [
            'items' => $items,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id = null)
    {
        $model = ($id) ? $this->findModel($id) : new ReportExpensesItem();
        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $model->formula = strtr($model->formula, [
                PHP_EOL => '',
                '<br />' => PHP_EOL,
                '&nbsp;' => ' ',
            ]);
            $model->formula = strip_tags($model->formula);
            $model->formula = trim($model->formula);

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Статья расхода успешно добавлена.') : Yii::t('common', 'Статья расхода успешно сохранена.'), 'success');
                return $this->redirect(['index']);
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $categories = ReportExpensesCategory::find()->orderBy('name')->collection();
        return $this->render('edit', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Статья расхода успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return ReportExpensesItem
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = ReportExpensesItem::find()->byId($id)->one();
        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Статья расходов не найдена.'));
        }

        return $model;
    }
}
