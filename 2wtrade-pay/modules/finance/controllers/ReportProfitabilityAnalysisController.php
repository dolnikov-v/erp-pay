<?php
namespace app\modules\finance\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\modules\finance\components\EstimateItem;
use app\modules\finance\components\exporter\ExporterExcel;
use app\modules\finance\components\ReportFormCountryCurrentBalance;
use app\modules\finance\models\filter\ReportProfitabilityAnalysisFilter;
use app\modules\finance\models\filter\ReportProfitabilityAnalysisForecastForm;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\components\ReportFormProfitabilityAnalysis;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\report\models\ReportProfitabilityAnalysis;
use app\widgets\Panel;
use IntlDateFormatter;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ProfitabilityAnalysisController
 * @package app\modules\finance\controllers
 */
class ReportProfitabilityAnalysisController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-param', 'refresh-data'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $systemCountries = Country::find()->bySystemUserCountries()->column();
        $countries = ReportExpensesCountry::find()
            ->where(['country_id' => $systemCountries])
            ->orderBy(['name' => SORT_ASC])
            ->active()
            ->indexBy('id')
            ->all();

        $countryCollection = ArrayHelper::map($countries, 'id', 'name');

        $forecastForm = new ReportProfitabilityAnalysisForecastForm();
        $filterForm = new ReportProfitabilityAnalysisFilter();
        $filterForm->date = date('m-Y');
        $filterForm->filter(Yii::$app->request->get());

        $dateFrom = $filterForm->year . '-' . $filterForm->month . '-01';
        ReportExpensesCountryItem::checkCloneLast(
            $filterForm->country_id,
            $dateFrom,
            $filterForm->year . '-' . $filterForm->month . '-' . date('t', strtotime($dateFrom)), true
        );

        $reportForm = new ReportFormProfitabilityAnalysis();

        $countriesDataProvider = (new ReportFormCountryCurrentBalance())->apply([
            'countries' => $countries,
            'month' => $filterForm->month,
            'year' => $filterForm->year,
        ]);

        $renderParams = [
            'filterForm' => $filterForm,
            'countryCollection' => $countryCollection,
            'reportForm' => $reportForm,
            'countriesDataProvider' => $countriesDataProvider,
            'groupCollection' => []
        ];

        if (!$filterForm->country_id) {
            $reportForm->panelAlert = Yii::t('common', 'Выберите Страну');
            $reportForm->panelAlertStyle = Panel::ALERT_DANGER;
        }

        if ($filterForm->country_id) {
            $params = [
                'country_id' => $filterForm->country_id,
                'month' => $filterForm->month,
                'year' => $filterForm->year,
                'isForecastPeriod' => $filterForm->isForecastPeriod() && false,
                'forecastParams' => null,
            ];

            $useForecastData = Yii::$app->request->get($forecastForm->formName());
            if ($filterForm->isForecastPeriod() && $useForecastData) {
                $forecastForm->load(Yii::$app->request->get());
                $forecastForm->in_process_percent = [];
                if ($forecastForm->buyout_percent && $groups = array_keys($forecastForm->buyout_percent)) {
                    foreach ($groups as $group) {
                        $forecastForm->in_process_percent[$group] = round(100 - $forecastForm->buyout_percent[$group] - $forecastForm->refuse_percent[$group], 1);
                    }
                }
                $params['forecastParams'] = [
                    'leadCountPerDay' => $forecastForm->lead_per_day,
                    'approvePercent' => $forecastForm->approve_percent,
                    'buyoutPercent' => $forecastForm->buyout_percent,
                    'refusePercent' => $forecastForm->refuse_percent,
                    'inProcessPercent' => $forecastForm->in_process_percent,
                    'operatorCount' => $forecastForm->operator_count,
                ];
            }
            $dataProviders = $reportForm->apply($params);

            if ($filterForm->isForecastPeriod() && !$useForecastData) {
                $forecastParams = $reportForm->factory->getFinanceProvider()->getForecastParams();
                if ($reportForm->factory->getFinanceProvider()->groupBy) {
                    foreach ($forecastParams as $group => $params) {
                        $forecastForm->lead_per_day[$group] = $params['leadCountPerDay'];
                        $forecastForm->approve_percent[$group] = $params['approvePercent'] * 100;
                        $forecastForm->buyout_percent[$group] = $params['buyoutPercent'] * 100;
                        $forecastForm->refuse_percent[$group] = $params['refusePercent'] * 100;
                        $forecastForm->in_process_percent[$group] = $params['inProcessPercent'] * 100;
                    }
                }
                $operatorCount = $reportForm->factory->getItem(EstimateItem::CALL_CENTERS_DIRECTION_OPERATOR_NUMBER);
                $forecastForm->operator_count = $operatorCount ? $operatorCount->getValue() : 0;
            }

            if ($export = Yii::$app->request->get('export')) {

                if ($export == 'excel') {
                    $formatter = new IntlDateFormatter(Yii::$app->formatter->locale, IntlDateFormatter::LONG, IntlDateFormatter::LONG);
                    $formatter->setCalendar(IntlDateFormatter::GREGORIAN);
                    $formatter->setPattern('LLLL yyyy');
                    $tables = [
                        [
                            'title' => Yii::t('common', 'Финансы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
                            'dataProvider' => $dataProviders['finance'],
                            'orientation' => 'normal',
                            'tableType' => 'finance',
                        ],
                        [
                            'title' => Yii::t('common', 'Показатели - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
                            'dataProvider' => $dataProviders['design'],
                            'orientation' => 'normal',
                            'tableType' => 'design',
                        ],
                        [
                            'title' => Yii::t('common', 'Расходы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01"))]),
                            'dataProvider' => $dataProviders['estimate'],
                            'orientation' => 'inversion',
                            'tableType' => 'estimate',
                        ],
                        [
                            'title' => Yii::t('common', 'Финансы - {month}', ['month' => $formatter->format(strtotime("{$filterForm->year}-{$filterForm->month}-01 -1month"))]),
                            'dataProvider' => $dataProviders['prev_month'],
                            'orientation' => 'normal',
                            'tableType' => 'prev_month',
                        ],
                    ];
                    (new ExporterExcel([
                        'tables' => $tables,
                        'groupCollection' => $reportForm->getGroupingCollection(),
                        'country' => $countries[$filterForm->country_id],
                        'date' => $filterForm->date,
                    ]))->sendFile();
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Неизвестный формат'), 'danger');
                }

                $params = Yii::$app->request->get();
                unset($params['export']);
                return $this->redirect(['index'] + $params);
            }


            $renderParams['dataProviders'] = $dataProviders;
            $renderParams['groupCollection'] = $reportForm->getGroupingCollection();
            $renderParams['categoryCollection'] = $reportForm->getCategoryCollection();
            $renderParams['categoryItems'] = $reportForm->getCategoryItems($dataProviders['finance']->models);
        }
        $renderParams['forecastForm'] = $forecastForm;

        return $this->render('index', $renderParams);
    }

    /**
     * @param int $country_id
     * @param int $month
     * @param int $year
     * @return array
     */
    public function actionRefreshData($country_id, $month, $year)
    {
        $country = ReportExpensesCountry::find()
            ->byId($country_id)
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->one();

        $response = [];
        if ($country && $month) {
            $reportForm = new ReportFormCountryCurrentBalance();
            $reportForm->useCache = false;
            $dataProvider = $reportForm->apply([
                'countries' => [$country],
                'month' => $month,
                'year' => $year,
            ]);
            $models = $dataProvider->getModels();
            if ($model = reset($models)) {
                $fields = array_keys(ReportFormCountryCurrentBalance::getExpenses());
                foreach ($model as $field => &$value) {
                    if (in_array($field, $fields)) {
                        $value = Yii::$app->formatter->asDecimal($value, 2);
                    }
                }
                unset($value);
                $response = $model;
            }
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionSetParam()
    {
        $error = null;

        if ($identity = Yii::$app->request->post('identity')) {
            $model = ReportProfitabilityAnalysis::findOne($identity) ?: new ReportProfitabilityAnalysis($identity);
            $param = Yii::$app->request->post('param');
            $value = Yii::$app->request->post('value');
            if ($param && !is_null($value) && (empty($value) || is_numeric($value))) {
                $data = $model->getData();
                $data[$param] = $value;
                $model->setData($data);
                if (!$model->save()) {
                    $error = $model->getFirstErrorAsString();
                }
            } else {
                $error = Yii::t('common', 'Не указан параметр или значение');
            }
        } else {
            $error = Yii::t('common', 'Невозможно идентифицировать запись');
        }

        return [
            'status' => intval(!$error),
            'message' => $error,
        ];
    }
}
