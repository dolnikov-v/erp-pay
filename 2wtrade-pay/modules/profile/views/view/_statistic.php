<?php
/**
 * Created by PhpStorm.
 * User: smykov
 * Date: 29.10.16
 * Time: 17:26
 */
/* @var yii\web\View $this */
/* @var \app\models\User $model */
/* @var array $stats */

use yii\helpers\ArrayHelper;


?>
<p>
    <?php
    if (Yii::$app->user->isSuperadmin || !Yii::$app->user->isImplant) {
        echo(Yii::t('common', 'Кошелек: $ {wallet}', ['wallet' => $stats['wallet']]));
    }
    ?>
</p>
<table class="table tables-striped margin-top-20">
    <tr>
        <th colspan="2"><?= Yii::t('common', 'Страна') ?></th>
        <th><?= Yii::t('common', 'Дата') ?></th>
        <th><?= Yii::t('common', 'Средний чек по КЦ') ?></th>
        <th><?= Yii::t('common', 'Апрув КЦ (%)') ?></th>
        <th><?= Yii::t('common', 'Текущий выкуп') ?></th>
        <th><?= Yii::t('common', 'Текущий невыкуп') ?></th>
        <th><?= Yii::t('common', 'Среднее количество дней просрочки отправки заказов') ?></th>
        <th><?= Yii::t('common', 'Количество выкупов от КС') ?></th>
        <th><?= Yii::t('common', 'Количество невыкупов от КС') ?></th>
        <th><?= Yii::t('common', 'Денег получено от КС') ?></th>
        <th><?= Yii::t('common', 'Не отправленных заказов') ?></th>
        <th><?= Yii::t('common', 'Средний чек по выкупам') ?></th>
        <th><?= Yii::t('common', 'Апрув КЦ') ?></th>
        <th><?= Yii::t('common', 'Убыток компании') ?></th>
    </tr>
    <?php if ($model->countries):
        $currentCountry = 0;
        ?>

        <?php foreach ($model->countries as $country):
        if (empty($stats[$country->id])) {
            continue;
        }
        $countStats = count($stats[$country->id]); ?>
        <?php foreach ($stats[$country->id] as $date => $statistic):

        ?>
        <tr<?= ((($statistic->percentBuyout < $statistic->minPercentBuyout) || ($statistic->percentApprovedCC < $statistic->minPercentApprovedCC) || ($statistic->minAvgCheckCC > 0 && $statistic->avgCheckApprovedCC < $statistic->minAvgCheckCC)) ? ' style="background:lightpink"' : '') ?>>
            <?php if ($currentCountry != $country->id):
                $currentCountry = $country->id; ?>
                <td style="background:white" rowspan="<?= $countStats ?>" class="width-50">
                    <span class="flag-icon flag-icon-<?= mb_strtolower($country->char_code) ?>"></span>
                </td>
                <td style="background:white" rowspan="<?= $countStats ?>"><?= $country->name ?></td>
            <?php endif; ?>
            <td nowrap="nowrap"><?= $date ?></td>
            <td>$ <?= $statistic->avgCheckApprovedCC ?></td>
            <td nowrap="nowrap"><?= $statistic->percentApprovedCC ?> %</td>
            <td nowrap="nowrap"><?= $statistic->percentBuyout ?> %</td>
            <td nowrap="nowrap"><?= $statistic->percentNotBuyout ?> %</td>
            <td<?= (($statistic->averageDelaySent >= 3) ? ' style="background:lightpink"' : '') ?>><?= $statistic->averageDelaySent ?></td>
            <td><?= ($statistic->totalBuyout + $statistic->totalMoneyReceived) ?></td>
            <td><?= $statistic->totalNotBuyout ?></td>
            <td nowrap="nowrap">
                $ <?= (Yii::$app->user->can('view_finance_director') ? $statistic->sumMoneyReceived : '*') ?>
                (<?= $statistic->moneyReceivedPrecent ?> %)
            </td>
            <td><?= $statistic->totalNotSent ?></td>
            <td>$ <?= $statistic->avgCheckBuyout ?></td>
            <td><?= $statistic->totalApprovedCC ?></td>
            <td nowrap="nowrap">$ <?= $statistic->lossOfCompany ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endforeach; ?>
        <tr<?= ((($stats['total']->percentBuyout < $stats['total']->minPercentBuyout) || ($stats['total']->percentApprovedCC < $stats['total']->minPercentApprovedCC) || ($stats['total']->minAvgCheckCC > 0 && $stats['total']->avgCheckApprovedCC < $stats['total']->minAvgCheckCC)) ? ' style="background:lightpink"' : '') ?>>
            <td colspan="3"><?= Yii::t('common', 'Итого:') ?></td>
            <td>$ <?= $stats['total']->avgCheckApprovedCC ?></td>
            <td><?= $stats['total']->percentApprovedCC ?> %</td>
            <td><?= $stats['total']->percentBuyout ?> %</td>
            <td><?= $stats['total']->percentNotBuyout ?> %</td>
            <td<?= (($stats['total']->averageDelaySent >= 3) ? ' style="background:lightpink"' : '') ?>><?= $stats['total']->averageDelaySent ?></td>
            <td><?= ($stats['total']->totalBuyout + $stats['total']->totalMoneyReceived) ?></td>
            <td><?= $stats['total']->totalNotBuyout ?></td>
            <td nowrap="nowrap">
                $ <?= (Yii::$app->user->can('view_finance_director') ? $stats['total']->sumMoneyReceived : '*') ?>
                (<?= $stats['total']->moneyReceivedPrecent ?> %)
            </td>
            <td><?= $stats['total']->totalNotSent ?></td>
            <td>$ <?= $stats['total']->avgCheckBuyout ?></td>
            <td><?= $stats['total']->totalApprovedCC ?></td>
            <td nowrap="nowrap">$ <?= $stats['total']->lossOfCompany ?></td>
        </tr>

    <?php endif; ?>
</table>