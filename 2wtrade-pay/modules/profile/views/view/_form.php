<?php

use app\components\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \app\models\User */
/* @var $form ActiveForm */
/* @var $timezones array */

?>


<?php $form = ActiveForm::begin(); ?>

<div class="row margin-top-20">

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'username')->textInput(['disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'fullname')->textInput() ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput() ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'phone')->textInput() ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'telegram_chat_id')->textInput() ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'timezone_id')->select2List(ArrayHelper::merge([null => Yii::t('common', 'Определять по стране')], $timezones), [
                'length' => 1,
            ]) ?>
        </div>
    </div>

    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Сохранить')); ?>
    </div>

</div>

<?php ActiveForm::end(); ?>
