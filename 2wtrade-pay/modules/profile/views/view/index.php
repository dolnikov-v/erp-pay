<?php

use app\modules\profile\assets\ProfileAsset;
use app\modules\profile\widgets\Profile;
use app\widgets\Nav;
use app\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model \app\models\User */
/* @var $stats array */
/* @var $countriesByUser \app\models\UserCountry[] */
/* @var $roles array */
/* @var $timezones array */
/* @var $themes array */
/* @var $skins array */
/* @var \app\models\NotificationUser $triggers */
/* @var yii\data\ActiveDataProvider $dataProviderDownloads */

ProfileAsset::register($this);

$this->title = Yii::t('common', 'Мой профиль');
?>

<div class="row page-profile">
    <div class="col-md-3">
        <?= Profile::widget([
            'model' => $model,
        ]); ?>
    </div>
    <?php

    $tabs = [
        [
            'label' => Yii::t('common', 'Общая информация'),
            'content' => $this->render('_form', [
                'model' => $model,
                'timezones' => $timezones,
            ]),
        ],
        [
            'label' => Yii::t('common', 'Оповещения'),
            'content' => $this->render('_notifications', [
                'model' => $model
            ]),
        ]
    ];

    // не показываем вкладку статистики для пользователей с ролью имплант
    /*if (!Yii::$app->user->isImplant && !Yii::$app->user->isDeliveryManager) {
        $tabs[] = [
            'label' => Yii::t('common', 'Статистика'),
            'content' => $this->render('_statistic', [
                'model' => $model,
                'stats' => $stats
            ]),
        ];
    }*/

    // не показываем вкладку статистики для пользователей с ролью менеджер доставки
    if (!Yii::$app->user->isDeliveryManager) {
        $tabs[] = [
            'label' => Yii::t('common', 'Мои загрузки'),
            'content' => $this->render('_mydownloads', [
                'model' => $model,
                'timezones' => $timezones,
                'dataProviderDownloads' => $dataProviderDownloads
            ]),
        ];
    }

    $tabs[] = [
        'label' => Yii::t('common', 'Мои настройки'),
        'content' => $this->render('_mysettings', [
            'model' => $model,
            'themes' => $themes,
            'skins' => $skins,
        ]),
    ];
    ?>
    <div class="col-md-9">
        <?= Panel::widget([
            'nav' => new Nav([
                'tabs' => $tabs
            ]),
        ]); ?>
    </div>
</div>
