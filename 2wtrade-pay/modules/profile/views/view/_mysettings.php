<?php

use app\components\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\User */
/* @var $form ActiveForm */
/* @var $themes array */

?>

<h3><?= Yii::t('common', "Внешний вид") ?></h3>

<?php $form = ActiveForm::begin(); ?>

<div class="row margin-top-20">

    <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'theme')->select2List($themes, [
                'length' => 1,
            ]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'skin')->select2List($skins, [
                'length' => 1,
            ]) ?>
        </div>
    </div>

    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Сохранить')); ?>
    </div>

</div>

<?php ActiveForm::end(); ?>
