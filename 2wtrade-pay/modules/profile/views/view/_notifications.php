<?php

use app\components\widgets\ActiveForm;
use app\widgets\Panel;
use app\modules\profile\widgets\NotificationUser;
use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var \app\models\User $model */
/* @var ActiveForm $form */
/* @var array $timezones */

?>

<div class="row">
    <div class="col-lg-6">
        <p><?= Yii::t('common', 'Для подписки на отправку оповещений в Telegram необходимо:') ?></p>
        <p><?= Yii::t('common', '1. Прописать в настройках профиля телефон начиная с 7') ?></p>
        <p><?= Yii::t('common', '2. Добавиться в канал') ?> <?= Html::a(Yii::$app->params['telegram']['invite'], Yii::$app->params['telegram']['invite'], ['target' => '_blank']) ?></p>
        <p><?= Yii::t('common', '3. Отправить свой телефон в канал, используя функцию меню') ?></p>
    </div>
    <div class="col-lg-6">
        <p><?= Yii::t('common', '1. Для получения уведомлений в Skype нужно добавить себе бота в список контакта.') ?></p>
        <p><?=Yii::t('common', 'Воспользуйтесь следующей ссылкой для добавления бота:')?> <a target="_blank" href='https://join.skype.com/bot/<?=Yii::$app->getModule('messenger')->getModule('serviceSkype')->transport->clientID?>'><img src='https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png'/></a></p>
        <p><?= Yii::t('common', '2. Напишите боту "login <Ваш email в системе>" для авторизации, либо "help" - для справки') ?></p>
    </div>
</div>


<div class="row margin-top-20">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список оповещений'),
        'withBody' => false,
        'content' => NotificationUser::widget([
            'model' => $model,
        ]),
    ]) ?>
</div>

