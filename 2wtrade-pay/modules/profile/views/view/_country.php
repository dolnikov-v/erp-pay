<?php
/* @var $this yii\web\View */
/* @var $countriesByUser \app\models\UserCountry[] */
?>

<table class="table tables-striped margin-top-20">
    <?php if ($countriesByUser): ?>

        <?php foreach ($countriesByUser as $item): ?>
            <tr>
                <td class="width-50">
                    <span class="flag-icon flag-icon-<?= mb_strtolower($item->country->char_code) ?>"></span>
                </td>
                <td><?= $item->country->name ?></td>
            </tr>
        <?php endforeach; ?>

    <?php endif; ?>
</table>
