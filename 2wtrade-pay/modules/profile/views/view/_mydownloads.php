<?php
/* @var yii\web\View $this */
/* @var \app\models\User $model */
/* @var yii\data\ActiveDataProvider $dataProviderDownloads */

use yii\helpers\ArrayHelper;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\DateColumn;
use app\widgets\ButtonLink;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use yii\helpers\Html;

$dataProviderDownloads->pagination->pageSize = 10;
$dataProviderDownloads->pagination->params = array_merge(yii::$app->request->getQueryParams(), ['#' => 'tab2']);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Файлы'),
    'withBody' => false,

    'content' => GridView::widget([
        'dataProvider' => $dataProviderDownloads,
        'rowOptions' => function ($model) {
            if (empty($model->filename) && !empty($model->generated_at)) {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'queue_num',
                'headerOptions' => ['class' => 'width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->queue_num;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($model) {
                    return Yii::t('common', $model->country->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'type',
                'headerOptions' => ['class' => 'width-200; text-center',],

                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->type;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'lines',
                'headerOptions' => ['class' => 'width-250'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->lines;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'filename',
                'headerOptions' => ['class' => 'width-500'],
                'content' => function ($model) {
                    if (empty($model->filename) && !empty($model->generated_at)) {
                        $content = 'Произошла ошибка';
                    } else {
                        if ($model->lines > 0) {
                            $content = Html::a($model->filename, Url::toRoute('files/download/' . $model->id), ['target' => '__blank']);
                        } else {
                            $content = $model->filename;
                        }
                    }
                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'width-100;text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'class' => DateColumn::className(),
                'enableSorting' => false
            ],
            [
                'attribute' => 'generated_at',
                'headerOptions' => ['class' => 'width-200; text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'generated_time',
                'headerOptions' => ['class' => 'width-100; text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return isset($model->generated_time) ? $model->generated_time : '';
                },
                'enableSorting' => false,
            ]

        ],
    ]),
    'footer' =>
        LinkPager::widget(['pagination' => $dataProviderDownloads->getPagination()]),
]);

?>
