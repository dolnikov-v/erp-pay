<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\models\Notification;
use app\models\UserNotification;
use app\modules\profile\widgets\ModalConfirmReadAll;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\search\UserNotificationSearch $searchModel */
?>

<?php
$this->title = Yii::t('common', 'Список оповещений');
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-form.php', [
        'searchModel' => $searchModel,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Оповещения'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($data) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($data->read == UserNotification::READ ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Название'),
                'content' => function ($data) {
                    $params = json_decode($data->params);
                    if ($params) {
                        foreach ($params as &$param) {
                            $param = Html::encode($param);
                        }
                    }
                    return Yii::t('common', $data->notification->description, $params);
                },
            ],
            [
                'attribute' => 'country_id',
                'label' => Yii::t('common', 'Страна'),
                'content' => function ($data) {
                    return $data->country ? Yii::t('common', $data->country->name) : "—";
                },
            ],
            [
                'attribute' => 'group',
                'label' => Yii::t('common', 'Группа'),
                'headerOptions' => ['class' => 'width-250 text-center'],
                'contentOptions' => ['class' => 'width-250 text-center'],
                'content' => function ($data) {
                    return Yii::t('common', Notification::getGroupCollection()[$data->notification->group]);
                },
            ],
            [
                'attribute' => 'type',
                'label' => Yii::t('common', 'Тип'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    return $data->notification->renderLabel();
                },
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
            ],
        ],
    ]),
    'footer' => Html::a(Yii::t('common', 'Отметить все, как прочитанные'),
            '#',
            [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => "modal",
                'data-target' => "#modal_confirm_read_all"
            ]) .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmReadAll::widget() ?>
