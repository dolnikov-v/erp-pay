<?php
use app\components\widgets\ActiveForm;
use app\models\Notification;
use app\models\search\UserNotificationSearch;
use app\widgets\assets\Select2Asset;
use yii\helpers\Url;
use app\models\User;

/** @var yii\web\View $this */
/** @var \app\models\search\UserNotificationSearch $searchModel */

Select2Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($searchModel, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'country')->select2List(User::getAllowCountries(), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'group')->select2List(Notification::getGroupCollection(), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'type')->select2List(Notification::getTypesCollection(), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'read')->select2List(UserNotificationSearch::getReadCollection(), ['prompt' => 'Показывать все']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
