<?php
namespace app\modules\profile\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationEmailSwitchery
 * @package app\modules\access\widgets
 */
class NotificationEmailSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-email-switchery', [
            'widget' => new Switchery([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-email-switchery',
                ],
            ]),
        ]);
    }
}
