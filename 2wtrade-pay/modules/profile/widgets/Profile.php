<?php
namespace app\modules\profile\widgets;

use yii\base\Widget;

/**
 * Class Button
 * @package app\widgets
 */
class Profile extends Widget
{
    /**
     * @var \app\models\User Пользователь
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('profile/index', [
            'model' => $this->model,
        ]);
    }
}
