<?php
namespace app\modules\profile\widgets;

use app\models\Notification;
use app\models\UserNotificationSetting;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class NotificationUser
 * @package app\modules\access\widgets
 */
class NotificationUser extends Widget
{
    /**
     * \app\models\User $model
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        $userSettings = UserNotificationSetting::find()
            ->currentUser()
            ->asArray()
            ->all();
        $triggers = Notification::find()->asArray()->all();
        $triggersMap = ArrayHelper::map($triggers, 'trigger', '');
        $email_interval = ArrayHelper::map($userSettings, 'trigger', 'email_interval');
        $sms_active = ArrayHelper::map($userSettings, 'trigger', 'sms_active');
        $telegram_active = ArrayHelper::map($userSettings, 'trigger', 'telegram_active');
        $skype_active = ArrayHelper::map($userSettings, 'trigger', 'skype_active');

        return $this->render('notification-user', [
            'model' => $this->model,
            'listInterval' => [
                'email' => array_merge($triggersMap, $email_interval),
                'sms' => array_merge($triggersMap, $sms_active),
                'telegram' => array_merge($triggersMap, $telegram_active),
                'skype' => array_merge($triggersMap, $skype_active),
            ]
        ]);
    }
}
