<?php
namespace app\modules\profile\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationSkypeSwitchery
 * @package app\modules\access\widgets
 */
class NotificationSkypeSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-skype-switchery', [
            'widget' => new Switchery([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-skype-switchery',
                ],
            ]),
        ]);
    }
}
