<?php
namespace app\modules\profile\widgets;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmReadAll
 * @package app\modules\profile\widgets
 */
class ModalConfirmReadAll extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-read-all/modal-confirm-read-all', [
            'id' => 'modal_confirm_read_all',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_PRIMARY,
            'title' => Yii::t('common', 'Подтверждение действия "Прочитать все оповещения"'),
            'close' => true,
            'body' => $this->render('modal-confirm-read-all/_body'),
            'footer' => $this->render('modal-confirm-read-all/_footer'),
        ]);
    }
}
