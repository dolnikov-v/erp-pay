<?php
namespace app\modules\profile\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationSmsSwitchery
 * @package app\modules\access\widgets
 */
class NotificationSmsSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-sms-switchery', [
            'widget' => new Switchery([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-sms-switchery',
                ],
            ]),
        ]);
    }
}
