<?php
namespace app\modules\profile\widgets;

use app\models\UserNotificationSetting;
use app\widgets\Dropdown;

/**
 * Class NotificationEmailInterval
 * @package app\modules\profile\widgets
 */
class NotificationEmailInterval extends Dropdown
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-email-interval', [
            'widget' => new Dropdown([
                'label' => $this->label,
                'style' => 'mail-interval btn-block btn-outline ' . self::STYLE_DEFAULT,
                'links' => UserNotificationSetting::getIntervalsForDropdown(),
                'size' => self::SIZE_SMALL
            ]),
        ]);
    }

}
