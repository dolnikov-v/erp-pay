<?php

use app\modules\profile\assets\NotificationEmailIntervalAsset;

/** @var \app\widgets\Dropdown $widget */

NotificationEmailIntervalAsset::register($this);

echo $widget->run();
