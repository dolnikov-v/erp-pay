<?php
use app\modules\profile\assets\NotificationTelegramSwitcheryAsset;

/** @var \app\widgets\Switchery $widget */

NotificationTelegramSwitcheryAsset::register($this);

echo $widget->run();
