<?php
use app\modules\profile\assets\NotificationUserSwitcheryAsset;

/** @var \app\widgets\Switchery $widget */

NotificationUserSwitcheryAsset::register($this);

echo $widget->run();
