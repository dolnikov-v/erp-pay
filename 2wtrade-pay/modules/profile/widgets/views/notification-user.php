<?php

use app\modules\profile\widgets\NotificationEmailSwitchery;
use app\modules\profile\widgets\NotificationUserSwitchery;
use app\modules\profile\widgets\NotificationSmsSwitchery;
use app\modules\profile\widgets\NotificationTelegramSwitchery;
use app\modules\profile\widgets\NotificationSkypeSwitchery;

/** @var app\models\User $model
/** @var app\models\Notification[] $modelsNotification */
/** @var array $listInterval */
?>
<table id="table_allow_notifications" class="table table-striped table-hover"
       data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
       data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>">
    <?php if (!empty($model->currentNotifications)): ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Описание') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Отправка по Email') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Отправка по SMS') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Отправка в Telegram') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Отправка в Skype') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->currentNotifications as $notification): ?>
            <tr class="tr-vertical-align-middle tr-notification" data-name="<?= $notification->trigger ?>">
                <td><?= Yii::t('common', $notification->description) ?></td>
                <td class="text-center" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationEmailSwitchery::widget([
                        'checked' => ($listInterval['email'][$notification->trigger] == 'always'),
                    ]); ?>
                </td>
                <td class="text-center" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationSmsSwitchery::widget([
                        'checked' => $listInterval['sms'][$notification->trigger],
                    ]) ?>
                </td>
                <td class="text-center" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationTelegramSwitchery::widget([
                        'checked' => $listInterval['telegram'][$notification->trigger],
                    ]) ?>
                </td>
                <td class="text-center" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationSkypeSwitchery::widget([
                        'checked' => $listInterval['skype'][$notification->trigger],
                    ]) ?>
                </td>
                <td class="text-center" data-notification="<?= $notification->trigger ?>">
                    <?= NotificationUserSwitchery::widget([
                        'checked' => in_array($notification->trigger, array_keys($model->triggersSetting)),
                        'disabled' => !Yii::$app->user->can('profile.view.setnotification'),
                    ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Оповещения отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
</table>
