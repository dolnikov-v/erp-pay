<?php
use app\modules\profile\assets\NotificationSmsSwitcheryAsset;

/** @var \app\widgets\Switchery $widget */

NotificationSmsSwitcheryAsset::register($this);

echo $widget->run();
