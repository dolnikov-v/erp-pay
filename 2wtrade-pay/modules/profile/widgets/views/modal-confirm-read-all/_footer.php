<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
use yii\helpers\Url;

?>

<?= Button::widget([
    'label' => Yii::t('common', 'Отмена'),
    'size' => Button::SIZE_SMALL,
    'attributes' => [
        'data-dismiss' => 'modal',
    ],
]) ?>

<?= ButtonLink::widget([
    'id' => 'modal_confirm_read_all',
    'label' => Yii::t('common', 'Подтвердить'),
    'url' => Url::toRoute('/profile/notification/read-all'),
    'style' => ButtonLink::STYLE_SUCCESS,
    'size' => Button::SIZE_SMALL,
]) ?>
