<?php
use app\modules\profile\assets\NotificationSkypeSwitcheryAsset;

/** @var \app\widgets\Switchery $widget */

NotificationSkypeSwitcheryAsset::register($this);

echo $widget->run();
