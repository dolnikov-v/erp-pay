<?php
use app\modules\profile\assets\NotificationEmailSwitcheryAsset;

/** @var \app\widgets\Switchery $widget */

NotificationEmailSwitcheryAsset::register($this);

echo $widget->run();
