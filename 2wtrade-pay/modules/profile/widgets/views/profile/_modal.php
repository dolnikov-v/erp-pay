<?php
use app\models\User;
use app\widgets\Cropper as CropperWidget;

/** @var User $model */
?>

<?= CropperWidget::widget([
    'linkId' => $model->media ? $model->media->link_id : "",
    'image' => $model->getAvatarUrl(),
    'cropData' => $model->media ? $model->media->crop_data : "",
    'circleImage' => true,
    'aspectRatio' => CropperWidget::RATIO_SQUARE
]); ?>



