<?php
use app\modules\media\models\Media;
use app\widgets\Button;
use app\widgets\Modal;
use yii\helpers\Html;

/** @var \app\models\User $model */
?>

<div class="widget widget-shadow text-center">
    <div class="widget-header">
        <div class="widget-header-content">
            <span class="avatar avatar-lg avatar-online">
                <?= Html::img($model->getAvatarUrl(false)) ?>
            </span>
            <h4 class="profile-user"><?= $model->username ?></h4>
            <p class="profile-job"><?= $model->getRolesNames() ?></p>
        </div>
        <div class="action-panel">
            <?= Button::widget([
                'id' => 'btn_edit_photo',
                'style' => 'btn btn-sm btn-default',
                'label' => '<i class="glyphicon glyphicon-camera"></i> ' . Yii::t('common', 'Редактировать фото'),
                'attributes' => [
                    'title' => Yii::t('common', 'Показать/Скрыть столбцы'),
                    'data-toggle' => 'modal',
                    'data-target' => '#modal_edit_profile_avatar',
                ],
            ]) ?>
        </div>
    </div>
</div>


<?= Modal::widget([
    'id' => 'modal_edit_profile_avatar',
    'title' => Yii::t('common', 'Редактирование фото'),
    'body' => $this->render('_modal', [
        'model' => $model,
    ]),
]) ?>
