<?php
namespace app\modules\profile\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationUserChanger
 * @package app\modules\access\widgets
 */
class NotificationUserSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-user-switchery', [
            'widget' => new Switchery([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-user-switchery',
                ],
            ]),
        ]);
    }
}
