<?php
namespace app\modules\profile\widgets;

use app\widgets\Switchery;

/**
 * Class NotificationTelegramSwitchery
 * @package app\modules\access\widgets
 */
class NotificationTelegramSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('notification-telegram-switchery', [
            'widget' => new Switchery([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'notification-telegram-switchery',
                ],
            ]),
        ]);
    }
}
