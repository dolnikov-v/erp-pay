<?php

namespace app\modules\profile\controllers;

use app\components\web\Controller;
use app\modules\order\models\OrderExport;
use Yii;
use yii\web\HttpException;

/**
 * Class FilesController
 *
 * @package app\modules\profile\controllers
 */
class FilesController extends Controller
{
    /**
     * @param $id
     * @return void
     * @throws HttpException
     */
    public function actionDownload($id)
    {
        $file = OrderExport::findOne(['id' => $id, 'user_id' => Yii::$app->user->id]);

        $path = "";
        $error = false;
        if ($file) {
            $path = Yii::getAlias('@export') . DIRECTORY_SEPARATOR . $file->user_id . DIRECTORY_SEPARATOR;
        } else {
            $error = true;
        }

        if (!$error && file_exists($path . $file->filename)) {
            Yii::$app->response->sendFile($path . $file->filename);
        } else {
            $error = true;
        }

        if ($error) {
            throw new HttpException(404, Yii::t('common', 'Файл не найден.'));
        }
    }
}
