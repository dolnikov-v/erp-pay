<?php
namespace app\modules\profile\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\search\UserNotificationSearch;
use app\models\UserNotification;
use app\models\UserNotificationQueue;
use Yii;
use yii\helpers\Url;

/**
 * Class NotificationController
 * @package app\modules\profile\controllers
 */
class NotificationController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['read', 'get-notifications'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserNotificationSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionReadAll()
    {
        UserNotification::updateAll(
            ['read' => UserNotification::READ],
            ['user_id' => Yii::$app->user->id]
        );

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return array
     */
    public function actionRead()
    {
        /** @var UserNotification $notification */
        $notification = UserNotification::find()
            ->where(['id' => Yii::$app->request->post('id'), 'user_id' => Yii::$app->user->id])
            ->one();

        if ($notification) {
            $notification->read = UserNotification::READ;
            $notification->save(false);
        }
        /** @var UserNotificationQueue $notificationQueue */
        $notificationQueue = UserNotificationQueue::find()
            ->joinWith('userNotification')
            ->where(['notification_id' => Yii::$app->request->post('id'), UserNotification::tableName() . '.user_id' => Yii::$app->user->id])
            ->one();

        if ($notificationQueue) {
            $notificationQueue->userNotification->read = UserNotification::READ;
            $notificationQueue->save(false);
        }

        return [
            'countUnread' => Yii::$app->user->getCountUnreadNotifications(),
        ];
    }

    /**
     * @return array
     */
    public function actionGetNotifications()
    {
        /** @var UserNotification[] $noReadNotifications */
        $noReadNotifications = UserNotification::find()
            ->joinWith('notification', 'country')
            ->where(['user_id' => Yii::$app->user->id, 'read' => UserNotification::UNREAD])
            ->limit(UserNotification::NOTIFICATION_LIMIT + 1)
            ->offset((int)Yii::$app->request->get('offset'))
            ->orderBy('id')
            ->all();

        $data = [
            'notifications' => [],
            'countUnread' => Yii::$app->user->getCountUnreadNotifications(),
            'end' => false,
        ];

        if (count($noReadNotifications) > UserNotification::NOTIFICATION_LIMIT) {
            array_pop($noReadNotifications);
        } else {
            $data['end'] = true;
        }

        foreach ($noReadNotifications as $notification) {
            $data['notifications'][] = [
                'id' => $notification->id,
                'description' => Yii::t('common', $notification->notification->description, json_decode($notification->params)),
                'country' => $notification->country ? Yii::t('common', $notification->country->name) : Yii::t('common', 'Не определена'),
                'cssClass' => $notification->getCssClassIcon(),
            ];
        }

        return $data;
    }
}
