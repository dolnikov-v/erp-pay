<?php

namespace app\modules\profile\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\models\Notification;
use app\models\Timezone;
use app\models\User;
use app\models\UserNotificationSetting;
use app\modules\order\models\OrderExport;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
//use app\modules\report\widgets\ConsolidateInfo;

/**
 * Class ViewController
 *
 * @package app\modules\profile\controllers
 */
class ViewController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-notification',
                    'set-notification-sms',
                    'set-notification-telegram',
                    'set-notification-skype',
                    'upload-photo'
                ],
            ]
        ];
    }

    /**
     * Получение списка файлов пользователя
     * @return Query
     */
    public function getDownloads()
    {
        $userId = Yii::$app->user->id;

        $queueQuery = OrderExport::find()
            ->select([
                new Expression("case 
                                when count(" . OrderExport::tableName() . ".id) = 0 
                                then 
                                     case 
                                     when oe.generated_at is " . new Expression('null') . " 
                                     then 1
                                     else ''
                                     end
                                 else count(" . OrderExport::tableName() . ".id) + 1
                                 end")
            ])
            ->where(['is', OrderExport::tableName() . '.generated_at', new Expression('null')])
            ->andWhere(['<', OrderExport::tableName() . '.id', new Expression('oe.id')]);

        $downloads = OrderExport::find()
            ->alias('oe')
            ->select([
                'queue_num' => $queueQuery,
                'generated_time' => new Expression("round((oe.generated_at-oe.created_at)/60,0)"),
                'oe.*',
                Country::tableName() . '.name'
            ])
            ->leftJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression('oe.country_id')])
            ->where(['oe.user_id' => $userId])
            ->orderBy(['oe.id' => SORT_DESC]);

        return $downloads;

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = $this->findModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(Url::toRoute('index'));
        }

        //$stats = new ConsolidateInfo($model->countries, strtotime('2016-09-01 00:00:00 GMT'), $model->id);

        return $this->render('index', [
            'model' => $model,
            //'stats' => $stats->prepareStats(),
            'themes' => Yii::$app->user->identity->getAllThemes(),
            'skins' => Yii::$app->user->identity->getAllSkinsOfTheme(Yii::$app->user->identity->theme),
            'timezones' => Timezone::find()->collection(),
            'dataProviderDownloads' => new  ActiveDataProvider([
                'query' => $this->getDownloads()
            ])
        ]);
    }

    /**
     * Ajax установка оповещения для пользователя
     *
     * @throws \yii\web\HttpException
     */
    public function actionSetNotification()
    {
        $userId = Yii::$app->user->id;

        /** @var Notification $notification */
        $notification = $this->findNotification(Yii::$app->request->post('trigger'));

        /** @var UserNotificationSetting $link */
        $link = UserNotificationSetting::find()
            ->where(['user_id' => $userId, 'trigger' => $notification->trigger])
            ->one();

        if ($link) {
            if (Yii::$app->request->post('value') == 'on') {
                $link->active = 1;
            } else {
                $link->active = 0;
            }
            $link->save();
        } else {
            $record = new UserNotificationSetting();
            $record->user_id = $userId;
            $record->trigger = $notification->trigger;
            if (Yii::$app->request->post('value') == 'on') {
                $record->active = 1;
            } else {
                $record->active = 0;
            }
            $record->save();
        }

        return true;
    }

    /**
     * Ajax установка оповещения для пользователя
     *
     * @throws \yii\web\HttpException
     */
    public function actionSetNotificationSms()
    {
        $userId = Yii::$app->user->id;

        /** @var Notification $notification */
        $notification = $this->findNotification(Yii::$app->request->post('trigger'));

        /** @var UserNotificationSetting $link */
        $link = UserNotificationSetting::find()
            ->where(['user_id' => $userId, 'trigger' => $notification->trigger])
            ->one();

        if ($link) {
            if (Yii::$app->request->post('value') == 'on') {
                $link->sms_active = 1;
            } else {
                $link->sms_active = 0;
            }
            $link->save();
        } else {
            $record = new UserNotificationSetting();
            $record->user_id = $userId;
            $record->trigger = $notification->trigger;
            if (Yii::$app->request->post('value') == 'on') {
                $record->sms_active = 1;
            } else {
                $record->sms_active = 0;
            }
            $record->save();
        }

        return true;
    }

    /**
     * Ajax установка оповещения для пользователя
     *
     * @throws \yii\web\HttpException
     */
    public function actionSetNotificationTelegram()
    {
        $userId = Yii::$app->user->id;

        /** @var Notification $notification */
        $notification = $this->findNotification(Yii::$app->request->post('trigger'));

        /** @var UserNotificationSetting $link */
        $link = UserNotificationSetting::find()
            ->where(['user_id' => $userId, 'trigger' => $notification->trigger])
            ->one();

        if ($link instanceof UserNotificationSetting) {
            if (Yii::$app->request->post('value') == 'on') {
                $link->telegram_active = 1;
            } else {
                $link->telegram_active = 0;
            }
            $link->save();
        } else {
            $record = new UserNotificationSetting();
            $record->user_id = $userId;
            $record->trigger = $notification->trigger;
            if (Yii::$app->request->post('value') == 'on') {
                $record->telegram_active = 1;
            } else {
                $record->telegram_active = 0;
            }
            $record->save();
        }

        return true;
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionSetNotificationSkype()
    {
        $userId = Yii::$app->user->id;

        /** @var Notification $notification */
        $notification = $this->findNotification(Yii::$app->request->post('trigger'));

        /** @var UserNotificationSetting $link */
        $link = UserNotificationSetting::find()
            ->where(['user_id' => $userId, 'trigger' => $notification->trigger])
            ->one();

        if ($link instanceof UserNotificationSetting) {
            if (Yii::$app->request->post('value') == 'on') {
                $link->skype_active = 1;
            } else {
                $link->skype_active = 0;
            }
            $link->save();
        } else {
            $record = new UserNotificationSetting();
            $record->user_id = $userId;
            $record->trigger = $notification->trigger;
            if (Yii::$app->request->post('value') == 'on') {
                $record->skype_active = 1;
            } else {
                $record->skype_active = 0;
            }
            $record->save();
        }

        return true;
    }

    /**
     * @return bool
     * @throws BadRequestHttpException
     */
    public function actionSetMailInterval()
    {
        $userId = Yii::$app->user->id;

        /** @var Notification $notification */
        $notification = $this->findNotification(Yii::$app->request->post('trigger'));

        /** @var UserNotificationSetting $link */
        $link = UserNotificationSetting::find()
            ->where(['user_id' => $userId, 'trigger' => $notification->trigger])
            ->one();

        if ($link) {
            $interval = (string)Yii::$app->request->post('interval');
            $link->email_interval = !empty($interval) ? $interval : null;
            $link->save();
        } else {
            $record = new UserNotificationSetting();
            $record->user_id = $userId;
            $record->trigger = $notification->trigger;
            $record->active = Notification::ACTIVE;
            $record->email_interval = Yii::$app->request->post('interval');
            $record->save();
        }

        return true;
    }


    public function saveAreaPhoto()
    {

    }

    public function actionUploadPhoto()
    {

    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel()
    {
        /** @var User $model */
        $model = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->with(['notifications', 'media'])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Пользователь не найден'));
        }

        return $model;
    }

    /**
     * @param $notification
     *
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findNotification($notification)
    {
        $model = Notification::find()
            ->where(['trigger' => $notification])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Оповещения не существует.'));
        }

        return $model;
    }
}
