<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationEmailIntervalAsset
 * @package app\modules\profile\assets
 */
class NotificationEmailIntervalAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-email-interval';

    public $js = [
        'notification-email-interval.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
