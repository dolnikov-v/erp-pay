<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationSkypeSwitcheryAsset
 * @package app\modules\access\assets
 */
class NotificationSkypeSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-skype-switchery';

    public $js = [
        'notification-skype-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
