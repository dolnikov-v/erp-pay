<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationSwitcheryUserAsset
 * @package app\modules\access\assets
 */
class NotificationUserSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-user-switchery';

    public $js = [
        'notification-user-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
