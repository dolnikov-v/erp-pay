<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationSmsSwitcheryAsset
 * @package app\modules\access\assets
 */
class NotificationEmailSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-email-switchery';

    public $js = [
        'notification-email-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
