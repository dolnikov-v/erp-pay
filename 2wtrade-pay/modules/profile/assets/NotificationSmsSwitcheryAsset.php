<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationSmsSwitcheryAsset
 * @package app\modules\access\assets
 */
class NotificationSmsSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-sms-switchery';

    public $js = [
        'notification-sms-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
