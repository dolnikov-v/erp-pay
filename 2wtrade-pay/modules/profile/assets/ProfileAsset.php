<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class ProfileAsset
 * @package app\modules\profile\assets
 */
class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/profile';

    public $css = [
        'profile.css',
    ];

    public $js = [
        'profile.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
