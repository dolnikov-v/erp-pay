<?php
namespace app\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationTelegramSwitcheryAsset
 * @package app\modules\access\assets
 */
class NotificationTelegramSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/profile/notification-telegram-switchery';

    public $js = [
        'notification-telegram-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
