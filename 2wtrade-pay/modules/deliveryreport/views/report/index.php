<?php

use app\widgets\Panel;
use app\helpers\DataProvider;
use app\widgets\ButtonProgress;
use yii\helpers\Url;
use app\widgets\LinkPager;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\modules\deliveryreport\assets\ReportIndexAsset;
use app\modules\deliveryreport\widgets\ReportActions;
use app\modules\deliveryreport\widgets\WithoutReportOrderExporter;
use app\modules\deliveryreport\assets\WithoutReportOrderExporterAsset;
use app\modules\deliveryreport\widgets\ReportErrorViewer;
use app\modules\deliveryreport\assets\DeliveryReportUploaderAsset;
use app\modules\deliveryreport\widgets\DeliveryReportUploader;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\deliveryreport\models\search\DeliveryReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $withoutReportOrders array */

ModalConfirmDeleteAsset::register($this);
ReportIndexAsset::register($this);
WithoutReportOrderExporterAsset::register($this);
DeliveryReportUploaderAsset::register($this);

$this->title = Yii::t('common', 'Список отчетов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сверка'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="delivery-report-index" data-check-status-url="<?= Url::toRoute("check-report-status") ?>">

    <?= ReportActions::widget([
        'modelSearch' => $searchModel,
        'withoutReportOrders' => $withoutReportOrders
    ]); ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Таблица с отчетами'),
        'actions' => DataProvider::renderSummary($dataProvider),
        'withBody' => false,
        'content' => $this->render('_index-content', [
            'dataProvider' => $dataProvider
        ]),
        'footer' =>
            ButtonProgress::widget([
                'id' => 'btn_delivery_report_uploader',
                'label' => Yii::t('common', 'Загрузить отчет'),
                'style' => ButtonProgress::STYLE_SUCCESS,
                'size' => ButtonProgress::SIZE_SMALL,
            ]) . LinkPager::widget([
                'pagination' => $dataProvider->getPagination(),
                'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
            ]),
    ]); ?>
</div>

<?= ModalConfirmDelete::widget() ?>

<?= WithoutReportOrderExporter::widget([
    'url' => Url::toRoute(['export-without-report-orders'])
]) ?>

<?= ReportErrorViewer::widget() ?>

<?= DeliveryReportUploader::widget([
    'url' => Url::toRoute('create'),
]) ?>
