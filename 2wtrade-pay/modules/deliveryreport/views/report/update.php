<?php

use app\widgets\Panel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\UploadForm */
/* @var $report app\modules\deliveryreport\models\DeliveryReport */

$this->title = Yii::t('common', 'Перезалить отчет: ') . $report->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сверка'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список отчетов'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Отчет {name}', ['name' => $report->name]),
    'url' => Url::toRoute(['view', 'id' => $report->id])
];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="load-report-update">
    <?= Panel::widget([
        'content' => $this->render('_form', [
            'model' => $model,
            'isNewRecord' => false
        ])
    ]) ?>
</div>
