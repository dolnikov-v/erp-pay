<?php

/* @var $this yii\web\View */
use yii\web\View;

/* @var $errors array */
/** @var array $fractionalOrders */
/** @var \app\modules\deliveryreport\models\DeliveryReportRecord $model */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось получить данные.'] = '" . Yii::t('common', 'Не удалось получить данные.') . "';",
    View::POS_HEAD);
?>
<div class="record-info">
    <?php if (!empty($errors)): ?>
        <div class="row">
            <h4><?= Yii::t('common', 'Найденные ошибки') ?></h4>
            <ul class="">
                <?php foreach ($errors as $error): ?>
                    <li class="">
                        <?= $error; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="row">
        <h4><?= Yii::t('common', 'Совпадающие заказы') ?></h4>
        <div class="fractional-orders col-lg-12">
            <?= $this->render('view_fractional_orders', [
                'fractionalOrders' => $fractionalOrders,
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
