<?php

use app\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\UploadForm */

$this->title = Yii::t('common', 'Загрузка отчета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сверка'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список отчетов'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="load-report-create">

    <?= Panel::widget([
        'content' => $this->render('_form', [
            'model' => $model,
            'isNewRecord' => true
        ])
    ]) ?>
</div>
