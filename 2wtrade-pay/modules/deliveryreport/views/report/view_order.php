<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\deliveryreport\assets\ViewOrderAsset;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\DeliveryReportRecord */
/* @var $order app\modules\deliveryreport\models\DeliveryReportRecord */
/* @var $errors array */
/* @var $fields array */
/** @var array $fractionalOrders */

$this->title = Yii::t('common', 'Запись #{id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сверка'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список отчетов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Отчет {name}', ['name' => $model->report->name]),
    'url' => Url::toRoute('report/view/' . $model->report->id)
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ViewOrderAsset::register($this);
?>
<div class="load-report-order-view">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Информация'),
        'withBody' => true,
        'content' => $this->render('view_order_info', [
            'errors' => $errors,
            'fractionalOrders' => $fractionalOrders,
            'model' => $model,
        ])
    ]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= Panel::widget([
                'title' => Yii::t('common', 'Данные в отчете'),
                'withBody' => true,
                'content' => $this->render('_form_report_order', [
                    'model' => $model,
                    'only_read' => false,
                    'fields' => $fields
                ]),
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= Panel::widget([
                'title' => Yii::t('common', 'Данные в базе'),
                'withBody' => true,
                'content' => $this->render('_form_report_order', [
                    'model' => $order,
                    'only_read' => true,
                    'fields' => []
                ]),
            ]); ?>
        </div>
    </div>

</div>
