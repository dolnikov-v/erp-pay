<?php

use app\modules\deliveryreport\models\DeliveryReportRecord;
use yii\helpers\Html;
use app\components\grid\GridView;
use yii\helpers\Url;
use app\modules\deliveryreport\models\DeliveryReport;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;

/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div id="reports_content">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
                'content' => function ($model) {
                    if ($model->status == DeliveryReport::STATUS_DELETE) {
                        return $model->name;
                    }
                    $options = [];
                    if ($model->status == DeliveryReport::STATUS_QUEUE || $model->status == DeliveryReport::STATUS_PARSING || $model->status == DeliveryReport::STATUS_CHECKING || $model->status == DeliveryReport::STATUS_CHECKING_QUEUE) {
                        $options['class'] = 'report-process';
                    }
                    if ($model->status == DeliveryReport::STATUS_ERROR) {
                        $options['class'] = 'report-error-link';
                        $error = json_decode($model->error_message, true);
                        if (!empty($error)) {
                            $statuses = DeliveryReport::getStatusNames();
                            if (isset($error['message'])) {
                                $options['data-error-message'] = $error['message'];
                            }
                            if (isset($statuses[$error['oldStatus']])) {
                                $options['data-error-status'] = $statuses[$error['oldStatus']];
                            } else {
                                $options['data-error-status'] = $error['oldStatus'];
                            }
                        }
                    }
                    $partLink = explode(' ', $model->name, 2);
                    return
                        (isset($partLink[1]) ? Html::a($partLink[0], Url::to(Yii::$app->params['jira']['host'] . '/browse/' . str_replace('UPL-', 'UPLOAD-', $partLink[0])), ['target' => '_blank']) . ' ' : '') .
                        Html::a($partLink[1] ?? $model->name, Url::toRoute([
                            'view',
                            'id' => $model->id,
                            'record_status' => DeliveryReportRecord::STATUS_ACTIVE
                        ]), $options);
                }
            ],
            [
                'attribute' => 'delivery_id',
                'value' => 'delivery.name',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'type',
                'value' => 'typeName',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Период'),
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
                'content' => function ($model) {
                    return $model->period_from && $model->period_to ? Yii::$app->formatter->asDate($model->period_from) . ' — ' . Yii::$app->formatter->asDate($model->period_to) : '';
                }
            ],
            [
                'attribute' => 'recordCount',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'activeRecordCount',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'approvedRecordCount',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'statusUpdatedRecordCount',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'value' => 'statusName',
                'headerOptions' => ['class' => 'text-center '],
                'contentOptions' => ['class' => 'text-center'],
                'filter' => DeliveryReport::getStatusNames(),
                'enableSorting' => false,
                'content' => function ($model) {
                    $content = Html::tag('div', $model->statusName,
                        [
                            'name' => 'report_status',
                            'data-report-id' => $model->id,
                            'data-report-status' => $model->status
                        ]);

                    return $content;
                },
            ],
            [
                'attribute' => 'user.username',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'priority',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'sum_total',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format' => ['decimal', 2],
                'enableSorting' => false,
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Скачать файл'),
                        'url' => function ($model) {
                            return Url::toRoute(['download-file', 'name' => $model->file]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('deliveryreport.report.downloadfile') &&
                                $model->status != DeliveryReport::STATUS_DELETE;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Обработан'),
                        'url' => function ($data) {
                            return Url::toRoute(['set-report-complete', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('deliveryreport.report.setreportcomplete') && $data->status == DeliveryReport::STATUS_IN_WORK;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Закрыть'),
                        'url' => function ($data) {
                            return Url::toRoute(['set-report-closed', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('deliveryreport.report.setreportclosed') && $data->status == DeliveryReport::STATUS_COMPLETE;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Перепроверить'),
                        'url' => function ($data) {
                            return Url::toRoute(['send-to-checking', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('deliveryreport.report.sendtochecking') && ($data->status == DeliveryReport::STATUS_IN_WORK || $data->status == DeliveryReport::STATUS_COMPLETE);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Повысить приоритет'),
                        'url' => function ($data) {
                            return Url::toRoute(['increase-report-priority', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('deliveryreport.report.increasereportpriority') && ($data->status == DeliveryReport::STATUS_QUEUE || $data->status == DeliveryReport::STATUS_CHECKING_QUEUE);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'style' => 'delivery-report-uploader-edit-btn',
                        'url' => function () {
                            return '#';
                        },
                        'attributes' => function ($model) {
                            /**
                             * @var DeliveryReport $model
                             */
                            return [
                                'data-report-id' => $model->id,
                                'data-title' => $model->name,
                                'data-type' => $model->type,
                                'data-delivery' => $model->delivery_id,
                                'data-date-format' => $model->date_format,
                                'data-period-from' => $model->period_from ? Yii::$app->formatter->asDate($model->period_from) : '',
                                'data-period-to' => $model->period_to ? Yii::$app->formatter->asDate($model->period_to) : '',
                            ];
                        },
                        'can' => function ($model) {
                            /**
                             * @var DeliveryReport $model
                             */
                            return Yii::$app->user->can('deliveryreport.report.update') &&
                                $model->status != DeliveryReport::STATUS_PARSING &&
                                $model->status != DeliveryReport::STATUS_CHECKING &&
                                $model->status != DeliveryReport::STATUS_DELETE;
                        },
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('deliveryreport.report.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('deliveryreport.report.delete') &&
                                $model->status != DeliveryReport::STATUS_DELETE;
                        }
                    ],
                ]
            ],
        ]
    ]) ?>
</div>
