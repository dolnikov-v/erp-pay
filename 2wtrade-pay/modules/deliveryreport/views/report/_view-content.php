<?php

use app\modules\deliveryreport\components\grid\GridView;
use app\modules\order\models\OrderFinancePretension;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;
use app\modules\deliveryreport\components\grid\ReportRecordColumn;
use app\components\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use app\modules\deliveryreport\widgets\ShowerColumns;
use app\modules\order\models\OrderFinanceFact;
use app\widgets\Label;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var app\modules\order\widgets\ShowerColumns $showerColumns */

?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($data) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $data->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
                'contentOptions' => [
                    'rowspan' => 3,
                    'class' => 'td-custom-checkbox',
                    'style' => 'vertical-align: middle !important;',
                ]
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => [
                    'style' => 'vertical-align: middle !important;',
                    'rowspan' => 3
                ]
            ],

            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'order_id',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_ORDER_ID),
                'content' => function ($model, $key, $index, $column) {
                    /**
                     * @var \app\modules\deliveryreport\models\DeliveryReportRecord $model
                     */
                    if (!$column->isSecondAttribute) {
                        return $model->order_id;
                    } else {
                        if (!is_null($model->comment) && $model->comment == 'status_was_changed_by_limited_user') {
                            $url = Html::a($model->order_id, Url::to([
                                '/order/index/index',
                                'NumberFilter' => [
                                    'entity' => 'id',
                                    'number' => $model->order_id
                                ],
                                'force_country_slug' => $model->relatedOrder->country->slug,
                            ]), [
                                'style' => 'background-color: pink;',
                            ]);
                        } else {
                            $url = Html::a($model->order_id, Url::to([
                                '/order/index/index',
                                'NumberFilter' => [
                                    'entity' => 'id',
                                    'number' => $model->order_id
                                ],
                                'force_country_slug' => ($model->relatedOrder) ? $model->relatedOrder->country->slug : '-',
                            ]));
                        }
                        if ($model->isFindByDuplicateOrder()) {
                            $url .= ' (' . Html::a($model->relatedOrder->originalOrder->id, Url::to([
                                    '/order/index/index',
                                    'NumberFilter' => [
                                        'entity' => 'id',
                                        'number' => $model->relatedOrder->originalOrder->id
                                    ],
                                    'force_country_slug' => $model->relatedOrder->originalOrder->country->slug,
                                ])) . ')';
                        }
                        return $url;
                    }
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'pretension',
                'label' => Yii::t('common', 'Претензия'),
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRETENSION),
                'content' => function ($model, $key, $index, $column) {
                    /**
                     * @var \app\modules\deliveryreport\models\DeliveryReportRecord $model
                     */
                    $return = [];

                    if($model->relatedOrder) {
                        if ($model->relatedOrder->orderFinancePretensions) {
                            foreach ($model->relatedOrder->orderFinancePretensions as $pretension) {
                                $label = OrderFinancePretension::getTypeCollecion()[$pretension->type] ?? '';
                                //претензия создана на конкретную запись отчета, а не на ордер!
                                if ($label && $model->id == $pretension->delivery_report_record_id) {
                                    $return[] = \app\widgets\Label::widget([
                                        'label' => $label,
                                        'style' => Label::STYLE_PRIMARY,
                                    ]);
                                }
                            }
                            return implode('<br />', $return);
                        }
                    }
                    return '';
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'call_center_id',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_ID)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'report.delivery.name',
                'secondAttribute' => 'relatedOrder.deliveryRequest.delivery.name',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_NAME)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'tracking',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_TRACKING),
                'content' => function ($model, $key, $index, $column) {
                    /**
                     * @var \app\modules\deliveryreport\models\DeliveryReportRecord $model
                     */
                    if (!$column->isSecondAttribute) {
                        return $model->tracking;
                    } else {
                        $url = $model->tracking;
                        if (!empty($model->relatedOrder->deliveryRequest->finance_tracking)) {
                            $url .= " ({$model->relatedOrder->deliveryRequest->finance_tracking})";
                        }
                        return $url;
                    }
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'statusName',
                'secondAttribute' => 'status',
                'content' => function ($model, $key, $index, $column) {
                    $value = ArrayHelper::getValue($model, $column->attribute);
                    if (empty($value)) {
                        return '';
                    }
                    return \app\widgets\Label::widget([
                        'label' => ArrayHelper::getValue($model, $column->attribute),
                        'style' => $model->error_log === true ? Label::STYLE_SUCCESS : Label::STYLE_PRIMARY,

                    ]);
                },
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_STATUS)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_full_name',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_FULL_NAME)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_phone',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_PHONE)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_address',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_ADDRESS)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_zip',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_ZIP)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_city',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_CITY)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_region',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_REGION)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'customer_district',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_DISTRICT)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'products',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRODUCTS)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'amount',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_AMOUNT)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE),
                'value' => function ($model) {
                    if (is_numeric($model['price'])) {
                        return Yii::$app->formatter->asDecimal($model['price'], 2);
                    }
                    return $model['price'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_cod',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_COD),
                'value' => function ($model) {
                    if (is_numeric($model['price_cod'])) {
                        return Yii::$app->formatter->asDecimal($model['price_cod'], 2);
                    }
                    return $model['price_cod'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_delivery',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_DELIVERY),
                'value' => function ($model) {
                    if (is_numeric($model['price_delivery'])) {
                        return Yii::$app->formatter->asDecimal($model['price_delivery'], 2);
                    }
                    return $model['price_delivery'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_redelivery',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_REDELIVERY),
                'value' => function ($model) {
                    if (is_numeric($model['price_redelivery'])) {
                        return Yii::$app->formatter->asDecimal($model['price_redelivery'], 2);
                    }
                    return $model['price_redelivery'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_delivery_back',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_DELIVERY_BACK),
                'value' => function ($model) {
                    if (is_numeric($model['price_delivery_back'])) {
                        return Yii::$app->formatter->asDecimal($model['price_delivery_back'], 2);
                    }
                    return $model['price_delivery_back'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_delivery_return',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_DELIVERY_RETURN),
                'value' => function ($model) {
                    if (is_numeric($model['price_delivery_return'])) {
                        return Yii::$app->formatter->asDecimal($model['price_delivery_return'], 2);
                    }
                    return $model['price_delivery_return'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_address_correction',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_ADDRESS_CORRECTION),
                'value' => function ($model) {
                    if (is_numeric($model['price_address_correction'])) {
                        return Yii::$app->formatter->asDecimal($model['price_address_correction'], 2);
                    }
                    return $model['price_address_correction'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_cod_service',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_COD_SERVICE),
                'value' => function ($model) {
                    if (is_numeric($model['price_cod_service'])) {
                        return Yii::$app->formatter->asDecimal($model['price_cod_service'], 2);
                    }
                    return $model['price_cod_service'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_vat',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_VAT),
                'value' => function ($model) {
                    if (is_numeric($model['price_vat'])) {
                        return Yii::$app->formatter->asDecimal($model['price_vat'], 2);
                    }
                    return $model['price_vat'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_storage',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_STORAGE),
                'value' => function ($model) {
                    if (is_numeric($model['price_storage'])) {
                        return Yii::$app->formatter->asDecimal($model['price_storage'], 2);
                    }
                    return $model['price_storage'];
                },
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_fulfilment',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_FULFILMENT),
                'value' => function ($model) {
                    if (is_numeric($model['price_fulfilment'])) {
                        return Yii::$app->formatter->asDecimal($model['price_fulfilment'], 2);
                    }
                    return $model['price_fulfilment'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_packing',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_PACKING),
                'value' => function ($model) {
                    if (is_numeric($model['price_packing'])) {
                        return Yii::$app->formatter->asDecimal($model['price_packing'], 2);
                    }
                    return $model['price_packing'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'price_package',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_PACKAGE),
                'value' => function ($model) {
                    if (is_numeric($model['price_package'])) {
                        return Yii::$app->formatter->asDecimal($model['price_package'], 2);
                    }
                    return $model['price_package'];
                }
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'date_created',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DATE_CREATED)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'date_approve',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DATE_APPROVE)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'date_return',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DATE_RETURN)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'date_payment',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DATE_PAYMENT)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'delivery_time_from',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_TIME_FROM)
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'payment_reference_number',
                'secondAttribute' => 'relatedOrder.finance.payment',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PAYMENT_ID),
            ],
            [
                'class' => ReportRecordColumn::className(),
                'attribute' => 'comment',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_COMMENT)
            ],
            [
                'attribute' => 'relatedOrder.country.name',
                'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_COUNTRY),
                'contentOptions' => [
                    'rowspan' => 3,
                    'style' => 'vertical-align: middle !important;',
                ],
                'header' => Yii::t('common', 'Страна')
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['view-order', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('deliveryreport.report.view');
                        },
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('deliveryreport.report.view');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete-order', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('deliveryreport.report.delete');
                        }
                    ],
                ],
                'contentOptions' => [
                    'rowspan' => 3,
                    'style' => 'vertical-align: middle !important;',
                ]
            ]
        ],
    ])
    ?>
</div>
