<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $filters array */
/* @var $model app\modules\deliveryreport\models\DeliveryReport */
/* @var $currentStatus string */
/* @var $tabs array */

?>

<div class="filter-report-form">
    <?php
    $options = Yii::$app->request->get();
    if (isset($options['filters'])) {
        unset($options['filters']);
    }
    $form = ActiveForm::begin([
        'method' => 'get',
        'action' => Url::toRoute(array_merge($options,
            ['view', 'id' => $model->id]))
    ]);

    ?>
    <?= Html::checkboxList('filters', Yii::$app->request->get('filters'), $filters, ['class' => 'checkbox']) ?>

    <div>
        <?= Html::submitButton(Yii::t('common', 'Отфильтровать'),
            ['class' => 'btn btn-success']) ?>
    </div>
    <?php $form->end(); ?>

</div>
