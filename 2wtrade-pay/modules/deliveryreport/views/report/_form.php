<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\deliveryreport\models\DeliveryReport;
use yii\helpers\ArrayHelper;
use app\modules\delivery\models\Delivery;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\UploadForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $isNewRecord bool */

?>

<div class="load-report-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php if (!$isNewRecord): ?>
        <?=$form->field($model, 'report_name')->textInput()?>
    <?php endif; ?>

    <div>
        <?= $form->field($model, 'type')->dropDownList(DeliveryReport::getTypeNames()) ?>
    </div>

    <div>
        <?= $form->field($model, 'delivery_id')->dropDownList(ArrayHelper::map(Delivery::find()->where(['country_id' => Yii::$app->user->country->id])->all(), 'id',
            'name')) ?>
    </div>

    <?php
    $field = $form->field($model, 'file');
    ?>

    <label class="file-upload padding-bottom-10">
        <span><?= Yii::t('common', 'Возможные форматы загружаемых файлов - CSV, XLS, XLSX') ?></span>
        <?=\app\widgets\InputGroupFile::widget([
            'right' => false,
            'input' => \app\widgets\InputText::widget([
                'id' => 'uploadform-file',
                'type' => 'file',
                'name' => 'UploadForm[file]'
            ])
        ])?>
    </label>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Сохранить'),
            ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
