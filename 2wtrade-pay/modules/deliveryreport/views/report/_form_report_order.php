<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\modules\deliveryreport\widgets\InputWithPaste;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\DeliveryReportRecord */
/* @var $only_read bool */
/* @var $fields array */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(array_merge(Yii::$app->request->get(), ['edit-order', 'id' => $model->id])),
    'enableClientValidation' => false,
    'id' => ($only_read ? 'order_in_base' : 'report_order')
]);
?>
<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Основная информация') ?></h4>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'order_id')->textInput([
                    (($only_read || $model->record_status != DeliveryReportRecord::STATUS_ACTIVE) ? 'disabled' : '') => ''
                ]),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'tracking')->textInput([
                    ($only_read ? 'disabled' : '') => ''
                ]),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= InputWithPaste::widget([
                'input' => ($only_read ? $form->field($model,
                    'status')->textInput([($only_read ? 'disabled' : '') => ''])
                    : $form->field($model, 'status')->dropDownList($model->report->getStatusesNamed())),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'call_center_id')->textInput([
                    ($only_read ? 'disabled' : '') => ''
                ]),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Информация о покупателе') ?></h4>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-5">
            <?= InputWithPaste::widget([
                'input' => $form->field($model,
                    'customer_full_name')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'customer_phone')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'customer_zip')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-4">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'customer_region')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'customer_city')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'customer_district')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12 record-data-row">
        <?= InputWithPaste::widget([
            'input' => $form->field($model, 'customer_address')->textarea([
                ($only_read ? 'disabled' : '') => '',
                'rows' => 2,
                'style' => 'resize: none;'
            ]),
            'fields' => $fields
        ]) ?>
    </div>

    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Информация о продуктах') ?></h4>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-7">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'products')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'amount')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Даты') ?></h4>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'date_created')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'date_approve')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'date_return')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'date_payment')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'delivery_time_from')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>

    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Цены') ?></h4>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_cod')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_delivery')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_storage')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_packing')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_package')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_redelivery')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12 record-data-row">
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model,
                    'price_delivery_back')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model,
                    'price_delivery_return')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= InputWithPaste::widget([
                'input' => $form->field($model, 'price_cod_service')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model,
                    'price_address_correction')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= InputWithPaste::widget([
                'input' => $form->field($model,
                    'payment_reference_number')->textInput([($only_read ? 'disabled' : '') => '']),
                'fields' => $fields
            ]) ?>
        </div>
    </div>

    <div class="col-lg-12">
        <?= InputWithPaste::widget([
            'input' => $form->field($model, 'comment')->textarea([
                ($only_read ? 'disabled' : '') => '',
                'rows' => 2,
                'style' => 'resize: none;'
            ]),
            'fields' => $fields
        ]) ?>
    </div>

</div>

<?php if (!$only_read) : ?>
    <div class="row">
        <div class="col-lg-12">
            <?= Html::submitButton(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php endif; ?>

<?php $form->end(); ?>
