<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\LinkPager;
use yii\helpers\Url;
use app\helpers\WbIcon;
use app\components\widgets\ActiveForm;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;
use app\modules\deliveryreport\assets\ReportViewReportAsset;
use app\modules\deliveryreport\components\grid\ReportRecordColumn;
use app\components\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use app\modules\deliveryreport\widgets\RecordActions;
use app\modules\deliveryreport\assets\RecordActionsAsset;
use app\modules\deliveryreport\assets\ShowerColumnsAsset;
use app\modules\deliveryreport\widgets\RecordApprover;
use app\modules\deliveryreport\assets\RecordApproverAsset;
use yii\web\View;
use app\modules\deliveryreport\assets\RecordDeApproverAsset;
use app\modules\deliveryreport\widgets\RecordDeApprover;
use app\modules\deliveryreport\assets\RecordRemoverAsset;
use app\modules\deliveryreport\widgets\RecordRemover;
use app\modules\deliveryreport\widgets\OrderStatusUpdater;
use app\modules\deliveryreport\assets\OrderStatusUpdaterAsset;
use app\modules\deliveryreport\assets\OrderTrackingUpdaterAsset;
use app\modules\deliveryreport\widgets\OrderTrackingUpdater;
use app\modules\deliveryreport\assets\OrderDataUpdaterAsset;
use app\modules\deliveryreport\widgets\OrderDataUpdater;
use app\modules\deliveryreport\assets\CreatePretensionAsset;
use app\modules\deliveryreport\widgets\CreatePretension;
use app\modules\deliveryreport\assets\RemovePretensionAsset;
use app\modules\deliveryreport\widgets\RemovePretension;
use app\modules\deliveryreport\widgets\LinkNav;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\assets\OrderPriceUpdaterAsset;
use app\modules\deliveryreport\widgets\OrderPriceUpdater;
use app\modules\deliveryreport\assets\PaymentIdUpdaterAsset;
use app\modules\deliveryreport\widgets\PaymentIdUpdater;
use app\modules\deliveryreport\widgets\PaymentList;
use app\modules\deliveryreport\assets\PaymentListAsset;
use app\modules\deliveryreport\assets\OrderCopyCreatorAsset;
use app\modules\deliveryreport\widgets\OrderCopyCreator;

/* @var $this yii\web\View */
/* @var $model app\modules\deliveryreport\models\DeliveryReport */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelSearch \app\modules\deliveryreport\models\search\DeliveryReportRecordSearch */
/** @var app\modules\order\widgets\ShowerColumns $showerColumns */
/** @var array $statuses */
/** @var array $unBuyoutReasons */
/** @var array $finInfo */
/** @var array $statusCount */
/** @var array $errorInfo */
/** @var array $orderIds */
/** @var \app\modules\deliveryreport\models\DeliveryReportCurrency $currencies */
/** @var array $currencyRate */

ReportViewReportAsset::register($this);
RecordActionsAsset::register($this);
ShowerColumnsAsset::register($this);
RecordApproverAsset::register($this);
RecordDeApproverAsset::register($this);
RecordRemoverAsset::register($this);
OrderStatusUpdaterAsset::register($this);
OrderTrackingUpdaterAsset::register($this);
CreatePretensionAsset::register($this);
RemovePretensionAsset::register($this);
OrderDataUpdaterAsset::register($this);
OrderPriceUpdaterAsset::register($this);
PaymentIdUpdaterAsset::register($this);
PaymentListAsset::register($this);
OrderCopyCreatorAsset::register($this);

$this->title = Yii::t('common', 'Отчет {delivery} — {name}', ['name' => $model->name, 'delivery' => $model->delivery->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сверка'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список отчетов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
$this->params['breadcrumbs_fixed'] = true;
?>
<div id="delivery_report_view" class="delivery-report-view">
    <div class="delivery-report-view-actions">
        <?= RecordActions::widget([
            'modelSearch' => $modelSearch,
            'reportModel' => $model,
            'statuses' => $statuses,
            'unBuyoutReasons' => $unBuyoutReasons,
            'finInfo' => $finInfo,
            'errorInfo' => $errorInfo,
            'orderIds' => $orderIds,
            'currencies' => $currencies ?? [],
            'currencyRate' => $currencyRate ?? []
        ]) ?>
    </div>

    <?= $this->render('view-content',
        [
            'dataProvider' => $dataProvider,
            'showerColumns' => $showerColumns,
            'statusCount' => $statusCount,
            'model' => $model
        ]) ?>
</div>

<?= $showerColumns->renderModal() ?>

<?php if (Yii::$app->user->can('deliveryreport.report.setapprove')): ?>
    <?= RecordApprover::widget([
        'url' => Url::toRoute(array_merge(['set-approve'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('deliveryreport.report.unsetapprove')): ?>
    <?= RecordDeApprover::widget([
        'url' => Url::toRoute(array_merge(['unset-approve'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('deliveryreport.report.deleterecords')): ?>
    <?= RecordRemover::widget([
        'url' => Url::toRoute(array_merge(['delete-records'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('deliveryreport.report.updateorderstatus')): ?>
    <?= OrderStatusUpdater::widget([
        'url' => Url::toRoute(array_merge(['update-order-status'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('deliveryreport.report.updateordertracking')): ?>
    <?= OrderTrackingUpdater::widget([
        'url' => Url::toRoute(array_merge(['update-order-tracking'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('deliveryreport.report.updateorderdata')): ?>
    <?= OrderDataUpdater::widget([
        'url' => Url::toRoute(array_merge(['update-order-data'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('deliveryreport.report.createpretension')): ?>
    <?= CreatePretension::widget([
        'url' => Url::toRoute(array_merge(['create-pretension'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('deliveryreport.report.removepretension')): ?>
    <?= RemovePretension::widget([
        'url' => Url::toRoute(array_merge(['remove-pretension'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('deliveryreport.report.createduplicateorder')): ?>
    <?= OrderCopyCreator::widget([
        'url' => Url::toRoute(array_merge(['create-duplicate-order'], Yii::$app->request->getQueryParams())),
        'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
        'reportModel' => $model
    ]) ?>
<?php endif; ?>

<?php if (!empty($finInfo)): ?>
    <?php if (Yii::$app->user->can('deliveryreport.report.setorderprices')): ?>
        <?= OrderPriceUpdater::widget([
            'url' => Url::toRoute(array_merge(['set-order-prices'], Yii::$app->request->getQueryParams())),
            'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
            'reportModel' => $model
        ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('deliveryreport.report.setpaymentid')): ?>
        <?= PaymentIdUpdater::widget([
            'url' => Url::toRoute(array_merge(['set-payment-id'], Yii::$app->request->getQueryParams())),
            'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
            'reportModel' => $model,
            'paymentId' => $finInfo['payment_id']
        ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('deliveryreport.report.paymentlist')): ?>
        <?= PaymentList::widget([
            'url' => Url::toRoute(['add-payment', 'id' => $model->id]),
            'updateUrl' => Url::toRoute(array_merge(['view'], Yii::$app->request->getQueryParams())),
            'reportModel' => $model,
            'paymentList' => $finInfo['payments'],
            'paymentsResult' => $finInfo['paymentsResult'],
        ]) ?>
    <?php endif; ?>
<?php endif; ?>
