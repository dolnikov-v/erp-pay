<?php

use app\assets\vendor\BootstrapLaddaAsset;
use app\widgets\ButtonProgress;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var array $fractionalOrders */
/** @var \app\modules\deliveryreport\models\DeliveryReportRecord $model */
BootstrapLaddaAsset::register($this);
?>

<?php if (is_null($fractionalOrders)): ?>
    <?= ButtonProgress::widget([
        'id' => 'show_fractional_orders',
        'label' => Yii::t('common', 'Показать совпадающие заказы'),
        'style' => ButtonProgress::STYLE_PRIMARY,
        'attributes' => [
            'data-url' => Url::toRoute(array_merge(Yii::$app->request->queryParams,
                [
                    'view-order',
                    'show_fractional_orders' => true
                ]))
        ]
    ]) ?>
<?php else: ?>
    <?php
    array_unshift($fractionalOrders, $model->relatedOrder->id);
    $values = [];
    foreach ($fractionalOrders as $orderId) {
        if ($model->convertedOrder->order_id == $orderId) {
            $values[] = '#' . $orderId;
        } else {
            $values[] = Html::a('#' . $orderId, Url::toRoute(array_merge(Yii::$app->request->queryParams,
                [
                    'view-order',
                    'order_id' => $orderId,
                    'id' => $model->id,
                    'show_fractional_orders' => true
                ])));
        }
    } ?>
    <?= implode(', ', $values) ?>
<?php endif; ?>
