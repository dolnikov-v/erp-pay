<?php

use app\helpers\DataProvider;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\widgets\LinkNav;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Url;

/* @var yii\web\View $this */
/* @var app\modules\deliveryreport\models\DeliveryReport $model */
/* @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\order\widgets\ShowerColumns $showerColumns */
/** @var array $statusCount */
?>

<div class="delivery-report-view-content" id="records_content">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список записей'),
        'nav' => new LinkNav([
            'getParameter' => 'record_status',
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Все ({count})', ['count' => array_sum($statusCount)]),
                    'url' => Url::toRoute(['', 'id' => $model->id])
                ],
                DeliveryReportRecord::STATUS_ACTIVE => [
                    'label' => Yii::t('common',
                        'Неподтвержденные ({count})',
                        ['count' => isset($statusCount[DeliveryReportRecord::STATUS_ACTIVE]) ? $statusCount[DeliveryReportRecord::STATUS_ACTIVE] : 0]),
                    'url' => Url::toRoute([
                        '',
                        'id' => $model->id,
                        'record_status' => DeliveryReportRecord::STATUS_ACTIVE
                    ])
                ],
                DeliveryReportRecord::STATUS_APPROVE => [
                    'label' => Yii::t('common',
                        'Подтвержденные ({count})',
                        ['count' => isset($statusCount[DeliveryReportRecord::STATUS_APPROVE]) ? $statusCount[DeliveryReportRecord::STATUS_APPROVE] : 0]),
                    'url' => Url::toRoute([
                        '',
                        'id' => $model->id,
                        'record_status' => DeliveryReportRecord::STATUS_APPROVE
                    ])
                ],
                DeliveryReportRecord::STATUS_STATUS_UPDATED => [
                    'label' => Yii::t('common',
                        'Обновлен статус ({count})',
                        ['count' => isset($statusCount[DeliveryReportRecord::STATUS_STATUS_UPDATED]) ? $statusCount[DeliveryReportRecord::STATUS_STATUS_UPDATED] : 0]),
                    'url' => Url::toRoute([
                        '',
                        'id' => $model->id,
                        'record_status' => DeliveryReportRecord::STATUS_STATUS_UPDATED
                    ])
                ],
            ]
        ]),
        'actions' => DataProvider::renderSummary($dataProvider) . $showerColumns->renderIcon(),
        'withBody' => false,
        'content' => $this->render('_view-content', [
            'dataProvider' => $dataProvider,
            'showerColumns' => $showerColumns
        ]),
        'footer' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ])
    ]) ?>
</div>
