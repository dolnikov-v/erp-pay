<?php

namespace app\modules\deliveryreport;

/**
 * loadreport module definition class
 */
class Module extends \app\components\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\deliveryreport\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
