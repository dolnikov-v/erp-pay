<?php

namespace app\modules\deliveryreport\widgets;

use app\modules\catalog\models\UnBuyoutReason;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\search\DeliveryReportRecordSearch;
use app\modules\order\models\OrderStatus;
use app\widgets\Widget;
use yii\base\InvalidParamException;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class RecordFilters
 * @package app\modules\deliveryreport\widgets
 */
class RecordActions extends Widget
{
    /** @var DeliveryReportRecordSearch */
    public $modelSearch;

    /** @var DeliveryReport */
    public $reportModel;

    /** @var array */
    public $statuses;

    /** @var array */
    public $unBuyoutReasons = [];

    /** @var \app\modules\deliveryreport\models\DeliveryReportCurrency|null */
    public $currencies = null;

    /** @var array */
    public $currencyRate = [];

    /**
     * @var array
     */
    public $finInfo;

    /**
     * @var array
     */
    public $errorInfo;

    /**
     * @var array
     */
    public $orderIds;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->modelSearch instanceof DeliveryReportRecordSearch) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать корректную модель поиска.'));
        }

        return $this->render('record-actions/record-actions', [
            'modelSearch' => $this->modelSearch,
            'reportModel' => $this->reportModel,
            'statuses' => $this->statuses,
            'finInfo' => $this->finInfo,
            'errorInfo' => $this->errorInfo,
            'availableStatuses' => $this->getAvailableOrderStatuses(),
            'orderIds' => $this->orderIds,
            'unBuyoutReasons' => $this->unBuyoutReasons,
            'availableUnBuyoutReasons' => $this->getAvailableUnBuyoutReasons(),
            'currencies' => $this->currencies,
            'currencyRate' => $this->currencyRate,
        ]);
    }

    /**
     * @return array
     */
    protected function getAvailableOrderStatuses()
    {
        $query = OrderStatus::find()->where(['group' => [OrderStatus::GROUP_DELIVERY, OrderStatus::GROUP_FINANCE]]);
        $finishedClasses = array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList());

        if ($this->reportModel->type == DeliveryReport::TYPE_FINANCIAL) {
            $query->andWhere(['id' => $finishedClasses]);
        } else {
            $query->andWhere(['not in', 'id', $finishedClasses]);
        }

        return $query->collection();
    }

    /**
     * @return array
     */
    protected function getAvailableUnBuyoutReasons()
    {
        return ArrayHelper::map(UnBuyoutReason::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

}
