<?php
namespace app\modules\deliveryreport\widgets;

use app\widgets\Widget;

/**
 * Class ReportErrorViewer
 * @package app\modules\deliveryreport\widgets
 */
class ReportErrorViewer extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-error-viewer/modal');
    }
}
