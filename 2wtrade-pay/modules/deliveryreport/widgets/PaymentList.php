<?php

namespace app\modules\deliveryreport\widgets;

use app\models\Currency;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentList
 * @package app\modules\deliveryreport\widgets
 */
class PaymentList extends Sender
{
    /**
     * @var ArrayDataProvider
     */
    public $paymentList = [];

    /**
     * @var array
     */
    public $paymentsResult = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('payment-list/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel,
            'paymentList' => $this->paymentList,
            'currencies' => $this->getCurrencyList(),
            'paymentsResult' => $this->paymentsResult,
        ]);
    }

    /**
     * @return string
     */
    public function renderPayments()
    {
        return $this->render('payment-list/_payments-content',
            [
                'reportModel' => $this->reportModel,
                'paymentList' => $this->paymentList,
                'paymentsResult' => $this->paymentsResult,
            ]);
    }

    /**
     * @return \app\models\Currency[]|array
     */
    protected function getCurrencyList()
    {
        $currencies = Currency::find()->select(['id', 'char_code'])->orderBy([
            new Expression('FIELD(char_code, ' . implode(', ',
                    ["'{$this->reportModel->country->currency->char_code}'", "'USD'"]) . ') DESC'),
            'id' => SORT_ASC
        ])->all();

        $currencies = ArrayHelper::map($currencies, 'id', 'char_code');
        return $currencies;
    }
}
