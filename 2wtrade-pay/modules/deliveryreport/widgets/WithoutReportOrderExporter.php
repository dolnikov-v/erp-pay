<?php

namespace app\modules\deliveryreport\widgets;

/**
 * Class WithoutReportOrderExporter
 * @package app\modules\deliveryreport\widgets
 */
class WithoutReportOrderExporter extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('without-report-order-exporter/modal', [
            'url' => $this->url
        ]);
    }
}
