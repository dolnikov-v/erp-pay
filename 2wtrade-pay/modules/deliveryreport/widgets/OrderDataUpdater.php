<?php

namespace app\modules\deliveryreport\widgets;

use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Class OrderDataUpdater
 * @package app\modules\deliveryreport\widgets
 */
class OrderDataUpdater extends Sender
{
    /**
     * @var array
     */
    protected $columns;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/order-data-updater/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel,
            'columns' => $this->getChangeableColumns()
        ]);
    }

    /**
     * @return array
     */
    protected function getChangeableColumns()
    {
        if (is_null($this->columns)) {
            $labels = DeliveryReportRecord::getAttributeLabels();
            $this->columns = [
                DeliveryReportRecord::COLUMN_CUSTOMER_FULL_NAME => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_FULL_NAME],
                DeliveryReportRecord::COLUMN_CUSTOMER_PHONE => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_PHONE],
                DeliveryReportRecord::COLUMN_CUSTOMER_ADDRESS => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_ADDRESS],
                DeliveryReportRecord::COLUMN_CUSTOMER_ZIP => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_ZIP],
                DeliveryReportRecord::COLUMN_CUSTOMER_CITY => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_CITY],
                DeliveryReportRecord::COLUMN_CUSTOMER_REGION => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_REGION],
                DeliveryReportRecord::COLUMN_CUSTOMER_DISTRICT => $labels[DeliveryReportRecord::COLUMN_CUSTOMER_DISTRICT],
                DeliveryReportRecord::COLUMN_PRICE => $labels[DeliveryReportRecord::COLUMN_PRICE],
                DeliveryReportRecord::COLUMN_PRICE_COD => $labels[DeliveryReportRecord::COLUMN_PRICE_COD],
                DeliveryReportRecord::COLUMN_PRICE_ADDRESS_CORRECTION => $labels[DeliveryReportRecord::COLUMN_PRICE_ADDRESS_CORRECTION],
                DeliveryReportRecord::COLUMN_PRICE_DELIVERY => $labels[DeliveryReportRecord::COLUMN_PRICE_DELIVERY],
                DeliveryReportRecord::COLUMN_PRICE_REDELIVERY => $labels[DeliveryReportRecord::COLUMN_PRICE_REDELIVERY],
                DeliveryReportRecord::COLUMN_PRICE_DELIVERY_BACK => $labels[DeliveryReportRecord::COLUMN_PRICE_DELIVERY_BACK],
                DeliveryReportRecord::COLUMN_PRICE_DELIVERY_RETURN => $labels[DeliveryReportRecord::COLUMN_PRICE_DELIVERY_RETURN],
                DeliveryReportRecord::COLUMN_PRICE_STORAGE => $labels[DeliveryReportRecord::COLUMN_PRICE_STORAGE],
                DeliveryReportRecord::COLUMN_PRICE_FULFILMENT => $labels[DeliveryReportRecord::COLUMN_PRICE_FULFILMENT],
                DeliveryReportRecord::COLUMN_PRICE_PACKING => $labels[DeliveryReportRecord::COLUMN_PRICE_PACKING],
                DeliveryReportRecord::COLUMN_PRICE_PACKAGE => $labels[DeliveryReportRecord::COLUMN_PRICE_PACKAGE],
                DeliveryReportRecord::COLUMN_PRICE_COD_SERVICE => $labels[DeliveryReportRecord::COLUMN_PRICE_COD_SERVICE],
                DeliveryReportRecord::COLUMN_PRICE_VAT => $labels[DeliveryReportRecord::COLUMN_PRICE_VAT],
                DeliveryReportRecord::COLUMN_COMMENT => $labels[DeliveryReportRecord::COLUMN_COMMENT],
                DeliveryReportRecord::COLUMN_DATE_CREATED => $labels[DeliveryReportRecord::COLUMN_DATE_CREATED],
                DeliveryReportRecord::COLUMN_DATE_APPROVE => $labels[DeliveryReportRecord::COLUMN_DATE_APPROVE],
                DeliveryReportRecord::COLUMN_DATE_PAYMENT => $labels[DeliveryReportRecord::COLUMN_DATE_PAYMENT],
                DeliveryReportRecord::COLUMN_DATE_RETURN => $labels[DeliveryReportRecord::COLUMN_DATE_RETURN],
                DeliveryReportRecord::COLUMN_PAYMENT_REFERENCE_NUMBER => $labels[DeliveryReportRecord::COLUMN_PAYMENT_REFERENCE_NUMBER],
                DeliveryReportRecord::COLUMN_DELIVERY_TIME_FROM => $labels[DeliveryReportRecord::COLUMN_DELIVERY_TIME_FROM],
                DeliveryReportRecord::COLUMN_CALL_CENTER_PENALTY => \Yii::t('common', 'Штрафы операторам'),
                DeliveryReportRecord::COLUMN_UNBUYOUT_REASONS => \Yii::t('common', 'Причины невыкупа'),
                DeliveryReportRecord::COLUMN_PRODUCTS => $labels[DeliveryReportRecord::COLUMN_PRODUCTS],
            ];
        }

        return $this->columns;
    }
}
