<?php
namespace app\modules\deliveryreport\widgets;

/**
 * Class RecordApprover
 * @package app\modules\deliveryreport\widgets
 */
class RecordApprover extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/record-approver/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
