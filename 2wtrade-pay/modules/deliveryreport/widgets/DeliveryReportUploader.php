<?php

namespace app\modules\deliveryreport\widgets;

use app\modules\delivery\models\Delivery;
use app\modules\deliveryreport\models\DeliveryReport;
use app\widgets\Widget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryReportUploader
 * @package app\modules\deliveryreport\widgets
 */
class DeliveryReportUploader extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return array
     */
    protected function getTypes()
    {
        return DeliveryReport::getTypeNames();
    }

    /**
     * @return array
     */
    protected function getDeliveries()
    {
        return ArrayHelper::map(Delivery::find()->where(['country_id' => Yii::$app->user->country->id])->all(), 'id',
            'name');
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-report-uploader/modal', [
            'types' => $this->getTypes(),
            'deliveries' => $this->getDeliveries(),
            'url' => $this->url,
        ]);
    }
}
