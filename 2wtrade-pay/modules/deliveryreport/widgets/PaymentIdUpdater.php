<?php

namespace app\modules\deliveryreport\widgets;


class PaymentIdUpdater extends Sender
{
    /**
     * @var string
     */
    public $paymentId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/payment-id-updater/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel,
            'paymentId' => $this->paymentId
        ]);
    }
}
