<?php

namespace app\modules\deliveryreport\widgets;


use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\widgets\Widget;
use Yii;
use app\helpers\WbIcon;
use app\widgets\Link;

/**
 * Class ShowerColumns
 * @package app\modules\deliveryreport\widgets
 */
class ShowerColumns extends Widget
{
    const COLUMN_CHECKBOX = 'checkbox';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_ACTIONS = 'actions';

    const COLUMN_ORDER_ID = 'order_id';
    const COLUMN_TRACKING = 'tracking';
    const COLUMN_STATUS = 'status';
    const COLUMN_CUSTOMER_FULL_NAME = 'customer_full_name';
    const COLUMN_CUSTOMER_ADDRESS = 'customer_address';
    const COLUMN_CUSTOMER_PHONE = 'customer_phone';
    const COLUMN_CUSTOMER_ZIP = 'customer_zip';
    const COLUMN_CUSTOMER_CITY = 'customer_city';
    const COLUMN_CUSTOMER_REGION = 'customer_region';
    const COLUMN_CUSTOMER_DISTRICT = 'customer_district';
    const COLUMN_COMMENT = 'comment';
    const COLUMN_PRODUCTS = 'products';
    const COLUMN_AMOUNT = 'amount';
    const COLUMN_PRICE = 'price';
    const COLUMN_PRICE_COD = 'price_cod';
    const COLUMN_DATE_CREATED = 'date_created';
    const COLUMN_DATE_APPROVE = 'date_approve';
    const COLUMN_DATE_PAYMENT = 'date_payment';
    const COLUMN_DATE_RETURN = 'date_return';
    const COLUMN_PRICE_DELIVERY = 'price_delivery';
    const COLUMN_PRICE_REDELIVERY = 'price_redelivery';
    const COLUMN_PRICE_STORAGE = 'price_storage';
    const COLUMN_PRICE_PACKING = 'price_packing';
    const COLUMN_PRICE_PACKAGE = 'price_package';
    const COLUMN_PRICE_DELIVERY_BACK = 'price_delivery_back';
    const COLUMN_PRICE_DELIVERY_RETURN = 'price_delivery_return';
    const COLUMN_PRICE_COD_SERVICE = 'price_cod_service';
    const COLUMN_PRICE_ADDRESS_CORRECTION = 'price_address_correction';
    const COLUMN_RECORD_STATUS = 'record_status';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_PAYMENT_ID = 'relatedOrder.finance.payment';
    const COLUMN_COUNTRY = 'relatedOrder.country.name';
    const COLUMN_DELIVERY_TIME_FROM = 'delivery_time_from';
    const COLUMN_DELIVERY_NAME = 'delivery_name';
    const COLUMN_CALL_CENTER_ID = 'call_center_id';
    const COLUMN_PRICE_FULFILMENT = 'price_fulfilment';
    const COLUMN_PRICE_VAT = 'price_vat';
    const COLUMN_PRETENSION = 'pretension';

    /** @var string */
    public $id = 'modal_shower_columns';

    /** @var array */
    private $requireColumnsCollection;

    /** @var array */
    private $manualColumnsCollection;

    /**
     * @var array
     */
    private $columnsCollection;

    /** @var string */
    private $sessionKey = 'delivery_report_records.columns';

    /**
     * @return string
     */
    public function run()
    {
        return [
            'icon' => $this->renderIcon(),
            'modal' => $this->renderModal(),
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function renderIcon()
    {
        return Link::widget([
            'style' => 'panel-action icon ' . WbIcon::SETTINGS,
            'url' => '#',
            'attributes' => [
                'title' => Yii::t('common', 'Показать/Скрыть столбцы'),
                'data-toggle' => 'modal',
                'data-target' => '#' . $this->id,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function renderModal()
    {
        return $this->render('shower-columns/modal', [
            'id' => $this->id,
            'requireColumns' => $this->getRequireColumnsCollection(),
            'manualColumns' => $this->getManualColumnsCollection(),
            'chosenColumns' => $this->getChosenColumns(),
        ]);
    }

    /**
     * @param string $column
     * @return boolean
     */
    public function isChosenColumn($column)
    {
        $columns = $this->getChosenColumns();

        if (is_null($columns) || !is_array($columns)) {
            $columns = $this->getDefaultColumns();
            Yii::$app->session->set($this->sessionKey, $columns);
        }

        return in_array($column, $columns);
    }

    /**
     * @param $column
     * @param boolean $choose
     */
    public function toggleColumn($column, $choose = true)
    {
        $allowColumns = array_keys($this->getManualColumnsCollection());

        if (in_array($column, $allowColumns)) {
            $chosenColumns = $this->getChosenColumns();

            if ($choose) {
                if (!in_array($column, $chosenColumns)) {
                    $chosenColumns[] = $column;
                }
            } else {
                $index = array_search($column, $chosenColumns);

                if ($index !== false) {
                    unset($chosenColumns[$index]);
                }
            }

            Yii::$app->session->set($this->sessionKey, array_values($chosenColumns));
        }
    }

    /**
     * @return array
     */
    public function getColumnsCollection()
    {
        if (is_null($this->columnsCollection)) {
            $this->columnsCollection = [
                self::COLUMN_ORDER_ID,
                self::COLUMN_TRACKING,
                self::COLUMN_STATUS,
                self::COLUMN_CUSTOMER_FULL_NAME,
                self::COLUMN_CUSTOMER_PHONE,
                self::COLUMN_CUSTOMER_ADDRESS,
                self::COLUMN_CUSTOMER_ZIP,
                self::COLUMN_CUSTOMER_CITY,
                self::COLUMN_CUSTOMER_REGION,
                self::COLUMN_CUSTOMER_DISTRICT,
                self::COLUMN_PRODUCTS,
                self::COLUMN_AMOUNT,
                self::COLUMN_PRICE,
                self::COLUMN_PRICE_COD,
                self::COLUMN_PRICE_ADDRESS_CORRECTION,
                self::COLUMN_PRICE_DELIVERY,
                self::COLUMN_PRICE_REDELIVERY,
                self::COLUMN_PRICE_DELIVERY_BACK,
                self::COLUMN_PRICE_DELIVERY_RETURN,
                self::COLUMN_PRICE_STORAGE,
                self::COLUMN_PRICE_PACKING,
                self::COLUMN_PRICE_PACKAGE,
                self::COLUMN_PRICE_COD_SERVICE,
                self::COLUMN_DATE_CREATED,
                self::COLUMN_DATE_APPROVE,
                self::COLUMN_DATE_PAYMENT,
                self::COLUMN_DATE_RETURN,
                self::COLUMN_COMMENT,
                self::COLUMN_CREATED_AT,
                self::COLUMN_UPDATED_AT,
                self::COLUMN_RECORD_STATUS,
                self::COLUMN_DELIVERY_TIME_FROM,
                self::COLUMN_DELIVERY_NAME,
                self::COLUMN_PRICE_VAT,
                self::COLUMN_PRICE_FULFILMENT,
                self::COLUMN_PRETENSION,
            ];
        }
        return $this->columnsCollection;
    }

    /**
     * @return array
     */
    private function getRequireColumnsCollection()
    {
        if (is_null($this->requireColumnsCollection)) {
            $labels = DeliveryReportRecord::getAttributeLabels();
            $this->requireColumnsCollection = [
                self::COLUMN_CHECKBOX => Yii::t('common', 'Выделение заказа'),
                self::COLUMN_ORDER_ID => $labels[self::COLUMN_ORDER_ID],
                self::COLUMN_TRACKING => $labels[self::COLUMN_TRACKING],
                self::COLUMN_ACTIONS => Yii::t('common', 'Действия над заказом'),
            ];
        }

        return $this->requireColumnsCollection;
    }

    /**
     * @return array
     */
    public function getManualColumnsCollection()
    {
        if (is_null($this->manualColumnsCollection)) {
            $labels = DeliveryReportRecord::getAttributeLabels();
            $this->manualColumnsCollection = [
                self::COLUMN_CALL_CENTER_ID => $labels[self::COLUMN_CALL_CENTER_ID],
                self::COLUMN_STATUS => $labels[self::COLUMN_STATUS],
                self::COLUMN_CUSTOMER_FULL_NAME => $labels[self::COLUMN_CUSTOMER_FULL_NAME],
                self::COLUMN_CUSTOMER_PHONE => $labels[self::COLUMN_CUSTOMER_PHONE],
                self::COLUMN_CUSTOMER_ADDRESS => $labels[self::COLUMN_CUSTOMER_ADDRESS],
                self::COLUMN_CUSTOMER_ZIP => $labels[self::COLUMN_CUSTOMER_ZIP],
                self::COLUMN_CUSTOMER_CITY => $labels[self::COLUMN_CUSTOMER_CITY],
                self::COLUMN_CUSTOMER_REGION => $labels[self::COLUMN_CUSTOMER_REGION],
                self::COLUMN_CUSTOMER_DISTRICT => $labels[self::COLUMN_CUSTOMER_DISTRICT],
                self::COLUMN_PRODUCTS => $labels[self::COLUMN_PRODUCTS],
                self::COLUMN_AMOUNT => $labels[self::COLUMN_AMOUNT],
                self::COLUMN_PRICE => $labels[self::COLUMN_PRICE],
                self::COLUMN_PRICE_COD => $labels[self::COLUMN_PRICE_COD],
                self::COLUMN_PRICE_ADDRESS_CORRECTION => $labels[self::COLUMN_PRICE_ADDRESS_CORRECTION],
                self::COLUMN_PRICE_DELIVERY => $labels[self::COLUMN_PRICE_DELIVERY],
                self::COLUMN_PRICE_REDELIVERY => $labels[self::COLUMN_PRICE_REDELIVERY],
                self::COLUMN_PRICE_DELIVERY_BACK => $labels[self::COLUMN_PRICE_DELIVERY_BACK],
                self::COLUMN_PRICE_DELIVERY_RETURN => $labels[self::COLUMN_PRICE_DELIVERY_RETURN],
                self::COLUMN_PRICE_STORAGE => $labels[self::COLUMN_PRICE_STORAGE],
                self::COLUMN_PRICE_FULFILMENT => $labels[self::COLUMN_PRICE_FULFILMENT],
                self::COLUMN_PRICE_PACKING => $labels[self::COLUMN_PRICE_PACKING],
                self::COLUMN_PRICE_PACKAGE => $labels[self::COLUMN_PRICE_PACKAGE],
                self::COLUMN_PRICE_COD_SERVICE => $labels[self::COLUMN_PRICE_COD_SERVICE],
                self::COLUMN_PRICE_VAT => $labels[self::COLUMN_PRICE_VAT],
                self::COLUMN_DATE_CREATED => $labels[self::COLUMN_DATE_CREATED],
                self::COLUMN_DATE_APPROVE => $labels[self::COLUMN_DATE_APPROVE],
                self::COLUMN_DATE_PAYMENT => $labels[self::COLUMN_DATE_PAYMENT],
                self::COLUMN_DATE_RETURN => $labels[self::COLUMN_DATE_RETURN],
                self::COLUMN_COMMENT => $labels[self::COLUMN_COMMENT],
                self::COLUMN_CREATED_AT => $labels[self::COLUMN_CREATED_AT],
                self::COLUMN_UPDATED_AT => $labels[self::COLUMN_UPDATED_AT],
                self::COLUMN_RECORD_STATUS => $labels[self::COLUMN_RECORD_STATUS],
                self::COLUMN_PAYMENT_ID => $labels[self::COLUMN_PAYMENT_ID],
                self::COLUMN_COUNTRY => Yii::t('common', 'Страна'),
                self::COLUMN_DELIVERY_TIME_FROM => $labels[self::COLUMN_DELIVERY_TIME_FROM],
                self::COLUMN_DELIVERY_NAME => Yii::t('common', 'Служба доставки'),
                self::COLUMN_PRETENSION => Yii::t('common', 'Претензия')
            ];
        }

        return $this->manualColumnsCollection;
    }


    /**
     * @return array
     */
    public function getChosenColumns()
    {
        $columns = Yii::$app->session->get($this->sessionKey);

        if (is_null($columns) || !is_array($columns)) {
            $columns = $this->getDefaultColumns();
            Yii::$app->session->set($this->sessionKey, $columns);
        }

        return $columns;
    }

    /**
     * @return array
     */
    private function getDefaultColumns()
    {
        $requireColumns = array_keys($this->getRequireColumnsCollection());

        $manualColumns = [
            self::COLUMN_ORDER_ID,
            self::COLUMN_TRACKING,
            self::COLUMN_STATUS,
            self::COLUMN_CUSTOMER_FULL_NAME,
            self::COLUMN_CUSTOMER_PHONE,
            self::COLUMN_CUSTOMER_ADDRESS,
            self::COLUMN_PRODUCTS,
            self::COLUMN_AMOUNT,
            self::COLUMN_PRICE,
            self::COLUMN_PRICE_COD,
            self::COLUMN_DATE_CREATED,
            self::COLUMN_DATE_APPROVE,
            self::COLUMN_DATE_PAYMENT,
            self::COLUMN_DATE_RETURN,
        ];

        return array_merge($requireColumns, $manualColumns);
    }
}
