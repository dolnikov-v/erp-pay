<?php

namespace app\modules\deliveryreport\widgets;

use app\modules\order\models\OrderFinancePretension;

/**
 * Class CreatePretension
 * @package app\modules\deliveryreport\widgets
 */
class CreatePretension extends Sender
{
    /**
     * @var array
     */
    protected $columns;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/create-pretension/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel,
            'pretensionTypes' => OrderFinancePretension::getTypeCollecion()
        ]);
    }
}
