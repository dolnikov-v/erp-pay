<?php
namespace app\modules\deliveryreport\widgets;

use app\modules\deliveryreport\models\DeliveryReport;
use Yii;
use yii\base\NotSupportedException;
use yii\base\Widget;

/**
 * Class Sender
 * @package app\modules\deliveryreport\widgets
 */
class Sender extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $updateUrl;

    /**
     * @var DeliveryReport
     */
    public $reportModel;

    /**
     * @throws NotSupportedException
     */
    public function run()
    {
        throw new NotSupportedException(Yii::t('common', 'Необходима реализация метода run().'));
    }
}
