<?php

namespace app\modules\deliveryreport\widgets;

use app\widgets\ChangeableList;

/**
 * Class CostList
 * @package app\modules\deliveryreport\widgets
 */
class CostList extends ChangeableList
{
    /**
     * @return string
     */
    public function run()
    {
        if (is_callable($this->items)) {
            $this->items = call_user_func($this->items);
        }
        if (is_callable($this->template)) {
            $this->template = call_user_func($this->template);
        }
        return $this->render('cost-list/template', [
            'label' => $this->label,
            'template' => $this->template,
            'items' => $this->items,
            'id' => $this->id,
            'class' => $this->cssClass,
            'callbackJsFuncAfterAddRow' => $this->callbackJsFuncAfterAddRow,
        ]);
    }
}