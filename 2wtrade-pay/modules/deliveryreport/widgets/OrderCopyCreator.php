<?php

namespace app\modules\deliveryreport\widgets;

/**
 * Class OrderCopyCreator
 * @package app\modules\deliveryreport\widgets
 */
class OrderCopyCreator extends Sender
{

    public function run()
    {
        return $this->render('operations/order-copy-creator/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
