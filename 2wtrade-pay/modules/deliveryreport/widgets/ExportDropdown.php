<?php
namespace app\modules\deliveryreport\widgets;

use yii\base\Widget;

/**
 * Class ExportDropdown
 * @package app\modules\deliveryreport\widgets
 */
class ExportDropdown extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-dropdown');
    }
}
