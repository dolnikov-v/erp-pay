<?php

namespace app\modules\deliveryreport\widgets;

/**
 * Class OrderPriceUpdater
 * @package app\modules\deliveryreport\widgets
 */
class OrderPriceUpdater extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/order-price-updater/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
