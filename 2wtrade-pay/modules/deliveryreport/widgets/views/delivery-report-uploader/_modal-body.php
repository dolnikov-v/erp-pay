<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\DateRangePicker;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Html;
use yii\web\View;

/** @var string $url */
/** @var array $deliveries */
/** @var array $types */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось загрузить отчет.'] = '" . Yii::t('common', 'Не удалось загрузить отчет.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Отчет успешно загружен и отправлен на сканирование.'] = '" . Yii::t('common',
        'Отчет успешно загружен и отправлен на сканирование.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Отчет успешно отредактирован.'] = '" . Yii::t('common',
        'Отчет успешно отредактирован.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Редактирование отчета'] = '" . Yii::t('common',
        'Редактирование отчета') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Загрузка отчета'] = '" . Yii::t('common',
        'Загрузка отчета') . "';",
    View::POS_HEAD);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выберите тип отчета, службу доставки и укажите файл содержащий отчет.') ?>
    </div>
</div>

<div class="row-with-text-start padding-top-10">
    <div id="delivery_report_uploader_title_row" class="row margin-bottom-10">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label for="title"><?= Yii::t('common', 'Название отчета') ?></label>
            <?= InputText::widget([
                'id' => 'delivery_report_uploader_title',
                'name' => 'title',
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label for="delivery"><?= Yii::t('common', 'Служба доставки') ?></label>
            <?= Select2::widget([
                'id' => 'delivery_report_uploader_delivery_id',
                'name' => 'delivery_id',
                'items' => $deliveries,
                'length' => -1,
                'autoEllipsis' => true,
                'prompt' => Yii::t('common', 'Укажите службу доставки'),
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row margin-top-10">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label for="type"><?= Yii::t('common', 'Тип отчета') ?></label>
            <?= Select2::widget([
                'id' => 'delivery_report_uploader_type',
                'name' => 'type',
                'items' => $types,
                'length' => -1,
                'autoEllipsis' => true,
                'prompt' => Yii::t('common', 'Укажите тип отчета'),
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row margin-top-10">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label for="date_format"><?= Yii::t('common', 'Формат дат') ?></label>
            <?= InputText::widget([
                'id' => 'delivery_report_uploader_date_format',
                'name' => 'date_format',
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row margin-top-10 margin-bottom-30">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 form-group">
            <label for="period_from"><?= Yii::t('common', 'Период') ?></label>
            <?= DateRangePicker::widget([
                'nameFrom' => 'period_from',
                'nameTo' => 'period_to',
                'hideRanges' => true,
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row margin-top-10">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => 'file',
                ]),
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <div class="row-with-text-start help-block text-center">
        <?= Yii::t('common', 'Возможные форматы загружаемых файлов - CSV, XLS, XLSX') ?>
    </div>
</div>

<div class="row-with-text-progress text-center" style="display: none;">
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 text-center">
            <div class="delivery-report-uploader-spinner"></div>
        </div>
        <div class="col-xs-8"></div>
    </div>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Загрузить'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
