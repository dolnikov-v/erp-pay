<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
/** @var array $types */
?>

<?= Modal::widget([
    'id' => 'modal_delivery_report_uploader',
    'title' => Yii::t('common', 'Загрузка отчета'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries,
        'types' => $types,
    ]),
    'color' => Modal::COLOR_SUCCESS
]) ?>
