<?php

use app\widgets\assets\ChangeableListAsset;

/**
 * @var \yii\web\View $this
 * @var string $label
 * @var string $template
 * @var string[] $items
 * @var string $id
 * @var string $class
 * @var string $callbackJsFuncAfterAddRow
 */

ChangeableListAsset::register($this);
?>

<div class="changeable-list <?= $class ?>" id="<?= $id ?>">
    <?php if ($label): ?>
        <div class="changeable-list-header row margin-bottom-10">
            <div class="col-lg-12">
                <?= $label ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($items)):
        $lastItem = array_pop($items);
        foreach ($items as $item): ?>
            <div class="changeable-list-element row">
                <div class="col-lg-12 cost-item">
                    <?= $item ?>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="changeable-list-element changeable-list-template row">
            <div class="col-lg-12 cost-item">
                <?= $lastItem ?>
            </div>
            <div class="col-lg-12">
                <button class="btn btn-success add-changeable-element col-xs-12 <?= \app\widgets\Button::SIZE_TINY ?>"
                        type="button" data-class="<?= $class ?>" data-callback-func="<?= $callbackJsFuncAfterAddRow ?>">
                    <i class="icon wb-plus"></i>
                </button>
            </div>
        </div>
    <?php else: ?>
        <div class="changeable-list-element changeable-list-template row">
            <div class="col-lg-12 cost-item">
                <?= $template ?>
            </div>
            <div class="col-lg-12">
                <button class="btn btn-success add-changeable-element col-md-12 <?= \app\widgets\Button::SIZE_TINY ?>"
                        type="button" data-class="<?= $class ?>" data-callback-func="<?= $callbackJsFuncAfterAddRow ?>">
                    <i class="icon wb-plus"></i>
                </button>
            </div>
        </div>
    <?php endif; ?>
</div>
