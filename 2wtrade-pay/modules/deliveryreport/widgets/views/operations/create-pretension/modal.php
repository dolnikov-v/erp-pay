<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $pretensionTypes */
?>

<?= Modal::widget([
    'id' => 'modal_create_pretension',
    'title' => Yii::t('common', 'Создание претензии'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl,
        'pretensionTypes' => $pretensionTypes
    ]),
]) ?>
