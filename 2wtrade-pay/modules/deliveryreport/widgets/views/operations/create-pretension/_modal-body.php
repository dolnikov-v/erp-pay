<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $pretensionTypes */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные.'] = '" . Yii::t('common', 'Не удалось обновить данные.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Обновление данных завершено.'] = '" . Yii::t('common',
        'Обновление данных завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить страницу.'] = '" . Yii::t('common',
        'Не удалось обновить страницу.') . "';", View::POS_HEAD);

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выберите тип претензии') ?>
    </div>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Select2::widget([
                'id' => 'create_pretension_select',
                'items' => $pretensionTypes,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <span class="help-block text-center action-record-count">
        <?= Yii::t('common', 'Действие выполниться для всех записей в текущей таблице.') ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет обновление данных') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_create_pretension',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_create_pretension',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
