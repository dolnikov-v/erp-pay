<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?= Modal::widget([
    'id' => 'modal_create_order_copy',
    'title' => Yii::t('common', 'Создание копий заказов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl
    ]),
]) ?>
