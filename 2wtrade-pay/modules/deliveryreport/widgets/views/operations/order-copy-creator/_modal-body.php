<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать записи.'] = '" . Yii::t('common', 'Необходимо выбрать записи.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Создание дубликатов завершено.'] = '" . Yii::t('common',
        'Создание дубликатов завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не создать дубликат заказа.'] = '" . Yii::t('common',
        'Не создать дубликат заказа.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные на странице.'] = '" . Yii::t('common',
        'Не удалось обновить данные на странице.') . "';", View::POS_HEAD);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common',
            'Вы действительно хотите подтвердить указанные записи и создать для этих заказов дубликаты для службы доставки {delivery}?',
            ['delivery' => Html::tag('strong', $reportModel->delivery->name)]) ?>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет создание копий заказов и подтверждение записей') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_create_order_copy',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_create_order_copy',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
