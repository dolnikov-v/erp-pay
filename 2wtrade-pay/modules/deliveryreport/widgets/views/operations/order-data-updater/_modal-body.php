<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $columns */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные.'] = '" . Yii::t('common', 'Не удалось обновить данные.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Обновление данных завершено.'] = '" . Yii::t('common',
        'Обновление данных завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить страницу.'] = '" . Yii::t('common',
        'Не удалось обновить страницу.') . "';", View::POS_HEAD);

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите обновить данные у заказов на значения из отчета?') ?>
    </div>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Select2::widget([
                'id' => 'update_order_data_column_select',
                'items' => $columns,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <?php if (Yii::$app->user->can('deliveryreport.report.rewritedata')): ?>
        <div class="help-block text-center">
            <div class="">
                <?= Checkbox::widget([
                    'name' => 'rewrite',
                    'label' => Html::tag('strong', Yii::t('common', 'Перезаписать данные?'))
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <span class="help-block text-center action-record-count">
        <?= Yii::t('common', 'Действие выполниться для всех записей в текущей таблице.') ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет обновление данных') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_update_order_data',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_update_order_data',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
