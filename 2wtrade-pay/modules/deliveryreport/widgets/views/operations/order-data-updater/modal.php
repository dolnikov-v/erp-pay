<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $columns */
?>

<?= Modal::widget([
    'id' => 'modal_update_order_data',
    'title' => Yii::t('common', 'Обновление данных у заказов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl,
        'columns' => $columns
    ]),
]) ?>
