<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?= Modal::widget([
    'id' => 'modal_update_tracking',
    'title' => Yii::t('common', 'Обновление трек номеров у заказов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl
    ]),
]) ?>
