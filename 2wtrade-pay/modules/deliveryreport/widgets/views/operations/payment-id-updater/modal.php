<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var string $paymentId */
?>

<?= Modal::widget([
    'id' => 'modal_update_payment_id',
    'title' => Yii::t('common', 'Обновление номера платежа у заказов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl,
        'paymentId' => $paymentId
    ]),
]) ?>
