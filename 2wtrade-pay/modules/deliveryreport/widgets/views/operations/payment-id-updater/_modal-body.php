<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;
use app\widgets\InputText;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var string $paymentId */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить номер платежа.'] = '" . Yii::t('common', 'Не удалось обновить номер платежа.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Обновление номера платежа завершено.'] = '" . Yii::t('common',
        'Обновление номера платежа завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные на странице.'] = '" . Yii::t('common',
        'Не удалось обновить данные на странице.') . "';", View::POS_HEAD);

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите обновить номер платежа у заказов?') ?>
    </div>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label><?= Yii::t('common', 'Номер платежа') ?></label>
            <?= InputText::widget([
                'id' => 'update_payment_id_input',
                'focus' => true,
                'size' => InputText::SIZE_SMALL,
                'value' => $paymentId
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <label><?= Yii::t('common', 'Сумма платежа') ?></label>
            <?= InputText::widget([
                'id' => 'update_payment_sum_input',
                'size' => InputText::SIZE_SMALL,
                //'value' => $sum
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <?php if (Yii::$app->user->can('deliveryreport.report.rewritedata')): ?>
        <div class="help-block text-center">
            <div class="">
                <?= Checkbox::widget([
                    'name' => 'rewrite',
                    'label' => Html::tag('strong', Yii::t('common', 'Перезаписать данные?'))
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <span class="help-block text-center action-record-count">
        <?= Yii::t('common', 'Действие выполниться для всех записей в текущей таблице.') ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет обновление данных') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_update_payment_id',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_update_payment_id',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
