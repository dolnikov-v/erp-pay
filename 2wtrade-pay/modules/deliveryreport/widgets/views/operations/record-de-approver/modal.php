<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?= Modal::widget([
    'id' => 'modal_unset_approve',
    'title' => Yii::t('common', 'Отмена подтверждения того, что записи соответствуют заказам в базе.'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl
    ]),
]) ?>
