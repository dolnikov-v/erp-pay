<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\widgets\custom\Checkbox;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить статус.'] = '" . Yii::t('common', 'Не удалось обновить статус.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Обновление статусов завершено.'] = '" . Yii::t('common',
        'Обновление статусов завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные на странице.'] = '" . Yii::t('common',
        'Не удалось обновить данные на странице.') . "';", View::POS_HEAD);

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите обновить статусы у заказов согласно данным из отчета?') ?>
    </div>
    <?php if (Yii::$app->user->can('deliveryreport.report.rewritedata') || Yii::$app->user->can('deliveryreport.report.ignorebalance')): ?>
        <div class="help-block text-center">
            <?php if (Yii::$app->user->can('deliveryreport.report.rewritedata')): ?>
                <div class="">
                    <?= Checkbox::widget([
                        'name' => 'rewrite',
                        'label' => Html::tag('strong', Yii::t('common', 'Перезаписать данные?'))
                    ]) ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('deliveryreport.report.ignorebalance')): ?>
                <div class="">
                    <?= Checkbox::widget([
                        'name' => 'ignore-balance',
                        'label' => Html::tag('strong', Yii::t('common', 'Игнорировать баланс'))
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <span class="help-block text-center action-record-count">
        <?= Yii::t('common', 'Действие выполниться для всех записей в текущей таблице.') ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет обновление статусов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_update_status',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_update_status',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
