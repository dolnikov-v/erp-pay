<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var yii\web\View $this */
/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сделать подтверждение записи.'] = '" . Yii::t('common', 'Не удалось сделать подтверждение записи.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Подтверждение записей завершено.'] = '" . Yii::t('common', 'Подтверждение записей завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось обновить данные на странице.'] = '" . Yii::t('common', 'Не удалось обновить данные на странице.') . "';", View::POS_HEAD);

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-updater-url' => $updateUrl
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите подтвердить соответствие для выбранных записей?') ?>
    </div>
    <span class="help-block text-center action-record-count">
        <?= Yii::t('common', 'Действие выполниться для всех записей в текущей таблице.') ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет подтверждение записей') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_set_approve',
            'label' => Yii::t('common', 'Выполнить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_set_approve',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
