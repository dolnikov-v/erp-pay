<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?= Modal::widget([
    'id' => 'modal_set_approve',
    'title' => Yii::t('common', 'Подтверждение соответствия записей'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl
    ]),
]) ?>
