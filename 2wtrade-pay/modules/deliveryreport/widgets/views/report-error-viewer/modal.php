<?php
use app\widgets\Modal;

?>

<?= Modal::widget([
    'id' => 'modal_view_report_error',
    'title' => Yii::t('common', 'Ошибка при обработке отчета'),
    'body' => $this->render('_modal-body'),
    'color' => Modal::COLOR_DANGER
]) ?>
