<?php

use app\widgets\Button;
use yii\web\View;

/** @var yii\web\View $this */

?>
<div class="row-with-text-start">
    <div class="text-center error-message">
        <h5><?= Yii::t('common', 'При обработке отчета возникла следующая ошибка') ?>:</h5>
        <p id="modal_view_report_error_text"></p>
    </div>
    <div class="common-error-message text-center">
        <h5><?= Yii::t('common', 'При обработке отчета возникла ошибка.') ?></h5>
    </div>
    <div class="text-center last-correct-status">
        <h5><?= Yii::t('common', 'Последний корректный статус') ?>:</h5>
        <p id="modal_view_report_error_status"></p>
    </div>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>
