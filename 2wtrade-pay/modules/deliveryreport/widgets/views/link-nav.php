<?php

use yii\helpers\Url;

/** @var string $id */
/** @var string $typeNav */
/** @var string $typeTabs */
/** @var array $tabs */
/** @var int $currentTab */
/** @var string $getParam */
?>

<ul <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?> class="nav link-nav-tabs <?= $typeNav ?> <?= $typeTabs ?>"
    data-plugin="nav-tabs">
    <?php foreach ($tabs as $key => $tab): ?>
        <li class="<?php if ($key === $currentTab): ?>active<?php endif; ?>">
            <a <?php if ($key !== $currentTab): ?>href="<?= isset($tab['url']) ? $tab['url'] : Url::to(['', $getParam => $key]) ?>"<?php endif; ?>>
                <?php if (isset($tab['icon'])): ?>
                    <i class="icon <?= $tab['icon'] ?>"></i>
                <?php endif; ?>
                <?= $tab['label'] ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
