<?php
use app\widgets\Modal;

/** @var \yii\web\View $this */
/** @var string $id */
/** @var array $requireColumns */
/** @var array $manualColumns */
/** @var array $chosenColumns */
?>

<?= Modal::widget([
    'id' => $id,
    'title' => Yii::t('common', 'Показать/Скрыть колонки'),
    'body' => $this->render('_modal-body', [
        'requireColumns' => $requireColumns,
        'manualColumns' => $manualColumns,
        'chosenColumns' => $chosenColumns,
    ]),
    'footer' => $this->render('_modal-footer'),
]) ?>
