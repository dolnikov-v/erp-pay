<?php
use app\modules\deliveryreport\components\exporter\ExporterRecordCsv;
use app\modules\deliveryreport\components\exporter\ExporterRecordExcel;

?>

<div id="orders_export_box">
    <div class="btn-group">
        <button id="orders_export_btn" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-share-square-o"></i> <?= Yii::t('common', 'Экспорт') ?><span class="caret"></span>
        </button>
        <ul id="orders_export_dropdown" class="dropdown-menu dropdown-menu-right">
            <li>
                <a class="export-xls" href="<?= ExporterRecordExcel::getExporterUrl() ?>" tabindex="-1">
                    <i class="text-success fa fa-file-excel-o"></i> Excel
                </a>
            </li>
            <li>
                <a class="export-csv" href="<?= ExporterRecordCsv::getExporterUrl() ?>" tabindex="-1">
                    <i class="text-primary fa fa-file-code-o"></i> CSV
                </a>
            </li>
        </ul>
    </div>
</div>
