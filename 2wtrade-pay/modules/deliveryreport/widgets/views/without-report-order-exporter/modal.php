<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_export_without_report',
    'title' => Yii::t('common', 'Выгрузка заказов, отсутствующих в отчетах'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>
