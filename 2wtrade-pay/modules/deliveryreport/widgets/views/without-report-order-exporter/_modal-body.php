<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\helpers\WbIcon;

/** @var string $url */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Произошла ошибка при генерации файла.'] = '" . Yii::t('common',
        'Произошла ошибка при генерации файла.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Генерация файла завершена.'] = '" . Yii::t('common',
        'Генерация файла завершена.') . "';", View::POS_HEAD);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-download-url' => Url::toRoute(['download-tmp-file'])
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите сгенерировать файл с заказами, которые отсутствуют в отчетах?') ?>
    </div>
</div>

<div class="row-with-text-progress" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет генерация файла') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-text-finish" style="display: none;">
    <div class="margin-bottom-15 text-center">
        <a id="export_without_report_link_download" class="btn btn-dark" href="#">
            <i class="icon <?= WbIcon::DOWNLOAD ?>" aria-hidden="true"></i> <?= Yii::t('common', 'Скачать файл') ?>
        </a>
    </div>
</div>

<div class="padding-top-20 text-right">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_export_without_report',
            'label' => Yii::t('common', 'Сгенерировать'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_export_without_report',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
