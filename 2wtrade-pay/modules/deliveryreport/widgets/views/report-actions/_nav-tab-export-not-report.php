<?php

use app\widgets\DateRangePicker;
use app\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\modules\delivery\models\Delivery;
use app\widgets\ButtonProgress;
use yii\web\View;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var array $withoutReportOrders */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Число отсутствующих заказов согласно параметрам'] = '" . Yii::t('common',
        'Число отсутствующих заказов согласно параметрам') . "';",
    View::POS_HEAD);
?>

<div class="row export-without-report" data-action-url="<?= Url::toRoute(['without-report-order-count']) ?>">
    <div class="col-lg-12">
        <div class="col-lg-4">
            <p><?= Yii::t('common', 'Общее число заказов: {count}.',
                    ['count' => $withoutReportOrders['totalCount']]) ?></p>
            <p><?= Yii::t('common', 'Число заказов, которые отсутствуют в отчете: {count}.',
                    ['count' => $withoutReportOrders['withoutReportOrderCount']]) ?></p>
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4 row">
            <div class="col-lg-12 margin-bottom-10 choosen-period">
                <?= DateRangePicker::widget([
                    'nameFrom' => 'dateFrom',
                    'nameTo' => 'dateTo',
                ]) ?>
            </div>
            <div class="col-lg-12 margin-bottom-10 choosen-delivery">
                <?= Select2::widget([
                    'name' => 'delivery_id',
                    'items' => ArrayHelper::map(Delivery::find()->active()->byCountryId(Yii::$app->user->country->id)->all(),
                        'id', 'name'),
                    'length' => -1,
                    'prompt' => Yii::t('common', 'Курьерская служба не указана.')
                ])
                ?>
            </div>
            <div class="col-lg-12">
                <p class="export-count"><?= Yii::t('common', 'Число отсутствующих заказов согласно параметрам') ?>:
                    <?= $withoutReportOrders['withoutReportOrderCount'] ?></p>
            </div>
            <div class="col-lg-12">
                <?= ButtonProgress::widget([
                    'id' => 'btn_export_without_report',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-full',
                    'label' => Yii::t('common', 'Экспорт'),
                ]) ?>
            </div>
        </div>
    </div>
</div>
