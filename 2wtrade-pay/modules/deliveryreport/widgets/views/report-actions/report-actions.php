<?php

use app\widgets\Panel;
use app\widgets\Nav;
use app\assets\vendor\BootstrapLaddaAsset;

/** @var yii\web\View $this */
/** @var app\modules\deliveryreport\models\search\DeliveryReportSearch $modelSearch */
/** @var array $withoutReportOrders */

BootstrapLaddaAsset::register($this);

$tabs = [
    [
        'label' => Yii::t('common', 'Фильтр поиска'),
        'content' => $this->render('_nav-tab-filters', [
            'modelSearch' => $modelSearch,
        ]),
    ],
    [
        'label' => Yii::t('common', 'Заказы, отсутствующие в отчетах'),
        'content' => $this->render('_nav-tab-export-not-report', [
            'withoutReportOrders' => $withoutReportOrders
        ]),
    ],
];

?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => $tabs
    ]),
]) ?>
