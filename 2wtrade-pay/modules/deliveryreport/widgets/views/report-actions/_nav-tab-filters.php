<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\deliveryreport\models\search\DeliveryReportSearch $modelSearch */
?>

<?php
$form = ActiveForm::begin([
    'action' => [Yii::$app->controller->action->id],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

    <div class="row">
        <div class="col-lg-3">
            <?= $modelSearch->getDeliveryFilter()->restore($form) ?>
        </div>
        <div class="col-lg-3">
            <?= $modelSearch->getTypeFilter()->restore($form) ?>
        </div>
        <div class="col-lg-3">
            <?= $modelSearch->getStatusFilter()->restore($form) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')) ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(Yii::$app->controller->action->id)) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>