<?php

use \app\components\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var \app\modules\deliveryreport\models\DeliveryReportCosts $model
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 */

?>
<?php $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'action' => Url::toRoute(['change-additional-cost']),
    'method' => 'post',
    'options' => [
        'data-delete-url' => Url::toRoute(['delete-additional-cost']),
    ],
]); ?>
    <?= $form->field($model, 'delivery_report_id')->hiddenInput(['value' => $reportModel->id])->label(false); ?>
    <?= $form->field($model, 'id')->hiddenInput([
        'data-default' => '',
    ])->label(false); ?>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-xs-6 form-group">
                <label for="period_from"><?= Yii::t('common', 'Период') ?></label>
                <?= \app\widgets\DateRangePicker::widget([
                    'nameFrom' => $model->formName() . '[from]',
                    'nameTo' => $model->formName() . '[to]',
                    'hideRanges' => true,
                    'valueFrom' => date('Y-m-d', $model->from ?? time()),
                    'valueTo' => date('Y-m-d', $model->to ?? strtotime(" + 1 day")),
                    'attributes' => [
                        'required' => true
                    ],
                ]) ?>
            </div>
            <div class="col-xs-3">
                <?= $form->field($model, 'sum')->textInput([
                    'attributes' => [
                        'data-default' => null,
                        'required' => true,
                        'type' => 'number',
                        'min' => $model->balance ? $model->sum - $model->balance : 0,
                    ],
                ])->label(Yii::t('common', 'Сумма ({currency})', ['currency' => $reportModel->country->currency->char_code])) ?>
            </div>
            <div class="col-xs-3">
                <?= $form->field($model, 'balance')->textInput([
                    'attributes' => [
                        'data-default' => null,
                        'type' => 'number',
                        'disabled' => true,
                    ],
                ])->label(Yii::t('common', 'Баланс ({currency})', ['currency' => $reportModel->country->currency->char_code])) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-xs-6">
                <?= $form->field($model, 'description')->textInput([
                    'attributes' => [
                        'class' => 'list-refresh',
                        'maxlength' => 255,
                        'data-default' => null,
                    ],
                ]) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="row-with-buttons text-right margin-top-30">
                    <?= \app\widgets\Button::widget([
                        'id' => 'save-cost',
                        'label' => Yii::t('common', 'Сохранить'),
                        'style' => \app\widgets\Button::STYLE_SUCCESS,
                        'size' => \app\widgets\Button::SIZE_SMALL,
                        'attributes' => [
                            'disabled' => true,
                        ],
                    ]) ?>
                    <?= \app\widgets\Button::widget([
                        'id' => 'delete-cost',
                        'label' => Yii::t('common', 'Удалить'),
                        'style' => \app\widgets\Button::STYLE_DANGER,
                        'size' => \app\widgets\Button::SIZE_SMALL,
                        'attributes' => [
                            'disabled' => !$model->canBeDelete(),
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 margin-top-25"></div>
<?php ActiveForm::end(); ?>