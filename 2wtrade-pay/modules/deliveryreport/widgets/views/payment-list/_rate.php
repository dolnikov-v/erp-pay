<?php
use yii\helpers\Html;
/**
 * @var \app\modules\deliveryreport\models\PaymentCurrencyRate $model
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 * @var app\components\widgets\ActiveForm $form
 * @var \app\models\Currency $currency
 */
?>

<div class="col-sm-10">
    <div class="row">
        <div class="col-sm-4">
            <?=$form->field($model, 'currency_id_to')->select2List(
                    $currency,
                    ['name' => Html::getInputName($model, 'currency_id_to') . '[]', 'length' => 1, 'value' => $reportModel->country->currency->id]
            )->label(false)?>
        </div>
        <div class="col-sm-4">
            <?=$form->field($model, 'currency_id_from')->select2List(
                    $currency,
                    ['name' => Html::getInputName($model, 'currency_id_from') . '[]', 'length' => 1, 'value' => $reportModel->country->currency->id]
            )->label(false)?>
        </div>
        <div class="col-sm-4">
            <?=$form->field($model, 'rate')->textInput(['name' => Html::getInputName($model, 'rate') . '[]'])->label(false)?>
        </div>
    </div>
</div>
