<?php
/**
 * @var \yii\web\View $this
 * @var string $url
 * @var string $updateUrl
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 * @var \yii\data\ArrayDataProvider $paymentList
 * @var array $currencies
 * @var array $paymentsResult
 */

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ButtonProgress;
use app\widgets\custom\Checkbox;
use app\widgets\DateTimePicker;
use app\widgets\InputText;
use app\widgets\Select2;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;
use \app\modules\deliveryreport\models\PaymentOrder;

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Платеж прикреплен.'] = '" . Yii::t('common', 'Платеж прикреплен.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось прикрепить платеж.'] = '" . Yii::t('common',
        'Не удалось прикрепить платеж.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Платеж удален.'] = '" . Yii::t('common', 'Платеж удален.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить платеж.'] = '" . Yii::t('common', 'Не удалось удалить платеж.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Не удалось изменить значения издержек.'] = '" . Yii::t('common',
        'Не удалось изменить значения издержек.') . "';",
    View::POS_HEAD);
$paymentModel = new PaymentOrder();
?>

<?php if (Yii::$app->user->can('deliveryreport.report.addpayment')): ?>
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
        'method' => 'post',
        'options' => [
            'data-sender-url' => $url,
            'data-updater-url' => $updateUrl,
            'data-change-costs-url' => Url::toRoute(['change-costs', 'id' => $reportModel->id]),
            'data-getrate' => Url::toRoute(['get-rate-for-currency-date', 'id' => $reportModel->id]),
        ]
    ]); ?>

    <h4>
        <?= Yii::t('common', 'Добавить платеж') ?>
    </h4>
    <div class="rows-with-payment-inputs">
        <div class="row">
            <div class="col-lg-8">
                <?= $form->field($paymentModel, 'name')->textInput([
                    'size' => InputText::SIZE_SMALL,
                ]); ?>
            </div>
            <div class="col-lg-4">
                <label for="payment_list_date_input"><?= Yii::t('common', 'Дата платежа') ?></label>
                <?= DateTimePicker::widget([
                    'value' => date("Y-m-d"),
                    'maxDate' => date("Y-m-d"),
                    'attributes' => [
                        'style' => 'text-align: center;',
                        'id' => 'payment_list_date_input',
                        'name' => Html::getInputName($paymentModel, 'paid_at'),
                    ]
                ]) ?>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-lg-4">
                <?= $form->field($paymentModel, 'sum')->textInput([
                    'size' => InputText::SIZE_SMALL,
                ]); ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($paymentModel, 'currency_id')->select2List($currencies, [
                    'value' => $reportModel->country->currency->id,
                    'defaultValue' => $reportModel->country->currency->id,
                    'length' => 1,
                ]); ?>
            </div>
            <div class="col-lg-5">
                <div class="row-with-buttons text-right margin-top-30">
                    <?= ButtonProgress::widget([
                        'id' => 'start_adding_payment',
                        'label' => Yii::t('common', 'Добавить'),
                        'style' => ButtonProgress::STYLE_SUCCESS . ' width-100',
                        'size' => ButtonProgress::SIZE_SMALL
                    ]) ?>

                    <?= Button::widget([
                        'label' => Yii::t('common', 'Закрыть'),
                        'style' => ButtonProgress::STYLE_DEFAULT . ' width-100',
                        'size' => ButtonProgress::SIZE_SMALL,
                        'attributes' => [
                            'data-dismiss' => 'modal',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5>
                    <?= Yii::t('common', 'Актуальные курсы валют') ?>
                </h5>
                <?= \app\widgets\ChangeableList::widget([
                    'label' => '<div class="col-lg-10"><div class="row"><div class="col-sm-4">Из валюты</div><div class="col-sm-4">В валюту</div><div class="col-sm-4">Курс (из = в * курс)</div></div></div>',
                    'template' => function () use ($currencies, $form, $reportModel) {
                        return $this->render('_rate', [
                            'form' => $form,
                            'model' => (new \app\modules\deliveryreport\models\PaymentCurrencyRate()),
                            'currency' => $currencies,
                            'reportModel' => $reportModel,
                        ]);
                    },
                    'items' => [],
                    'callbackJsFuncAfterAddRow' => 'ChangeableList.refreshSelect',
                    'id' => 'payment_rates'
                ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <hr>
<?php endif; ?>
<?php if (Yii::$app->user->can('deliveryreport.report.changecosts')):

    \app\modules\deliveryreport\assets\CostListAsset::register($this);
    ?>
    <div class="payment-costs">
        <h4><?= Yii::t('common', 'Издержки') ?></h4>
        <div class="payments-costs-content row">
            <div class="col-lg-3">
                <label><?= Yii::t('common', 'Комиссия банка ({currency})',
                        ['currency' => $reportModel->country->currency->char_code]) ?></label>
                <?= InputText::widget([
                    'id' => 'total_report_costs',
                    'value' => $reportModel->total_costs
                ]) ?>
            </div>
            <div class="col-lg-3">
                <label><?= Yii::t('common', 'Баланс ({currency})',
                        ['currency' => $reportModel->country->currency->char_code]) ?></label>
                <?= InputText::widget([
                    'id' => 'sum_total',
                    'value' => $reportModel->sum_total,
                    'disabled' => true,
                ]) ?>
            </div>
            <div class="col-lg-2">
                <label><?= Yii::t('common', 'Валюта') ?></label>
                <?= Select2::widget([
                    'id' => 'report_costs_currency',
                    'items' => $currencies,
                    'value' => $reportModel->country->currency->id
                ]) ?>
            </div>
            <div class="col-lg-2">
                <label><?= Yii::t('common', 'Курс') ?></label>
                <?= InputText::widget([
                    'id' => 'total_report_rate',
                    'value' => $reportModel->rate_costs
                ]) ?>
            </div>
            <div class="col-lg-2 row-with-buttons text-right margin-top-30">
                <?= ButtonProgress::widget([
                    'id' => 'change_report_costs',
                    'label' => Yii::t('common', 'Изменить'),
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-100',
                    'size' => ButtonProgress::SIZE_SMALL
                ]) ?>
            </div>
        </div>
    </div>

    <div class="payment-costs">
        <h5><?= Yii::t('common', 'Дополнительные издержки') ?></h5>
        <?= \app\modules\deliveryreport\widgets\CostList::widget([
            'id' => 'modal_cost_list',
            'template' => function () use ($reportModel) {
                return $this->render('_costs', [
                    'model' => (new \app\modules\deliveryreport\models\DeliveryReportCosts()),
                    'reportModel' => $reportModel,
                ]);
            },
            'callbackJsFuncAfterAddRow' => 'ChangeableList.setDefaultValue;ChangeableList.refreshDatepicker;CostList.addNewRow',
            'items' => function () use ($form, $reportModel) {
                $data = [];
                foreach ($reportModel->costs as $cost) {
                    $data[] = $this->render('_costs', [
                        'model' => $cost,
                        'reportModel' => $reportModel,
                    ]);
                }
                return $data;
            },
        ]) ?>
    </div>
    <hr>
<?php endif; ?>
<div class="payment-list">
    <h4><?= Yii::t('common', 'Список платежей') ?></h4>
    <div class="payment-list-content">
        <?= $this->render('_payments-content', ['paymentList' => $paymentList, 'reportModel' => $reportModel, 'paymentsResult' => $paymentsResult]) ?>
    </div>
</div>
