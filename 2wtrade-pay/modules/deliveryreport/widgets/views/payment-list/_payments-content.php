<?php
/**
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 * @var \yii\data\ArrayDataProvider $paymentList
 * @var array $paymentsResult
 */
use app\widgets\ButtonProgress;
use kartik\grid\GridView;
use yii\helpers\Url;

$currencies = \app\models\Currency::find()->customCollection('id', 'char_code');
if (!isset($paymentsResult)) {
    $paymentsResult = [];
}
?>

<?= GridView::widget([
    'dataProvider' => $paymentList,
    'layout' => '{items}',
    'showPageSummary' => true,
    'columns' => [
        [
            'attribute' => 'name',
            'vAlign' => 'middle',
            'label' => Yii::t('common', 'Номер платежа'),
        ],
        [
            'attribute' => 'paid_at',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'label' => Yii::t('common', 'Дата платежа'),
            'value' => function ($model) {
                return Yii::$app->formatter->asDate($model->paid_at);
            }
        ],
        [
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'attribute' => 'sum',
            'label' => Yii::t('common', 'Сумма'),
            'format' => ['decimal', 2],
            'pageSummary' => Yii::$app->formatter->asDecimal(round($paymentsResult['sum'] ?? 0, 4)),
        ],
        [
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'attribute' => 'balance',
            'label' => Yii::t('common', 'Баланс ({char})', ['char' => $currencies[$reportModel->country->currency_id] ?? '-']),
            'value' => function ($model) use ($reportModel) {
                $rate = $reportModel->getRate($model->currency_id, $reportModel->country->currency_id, $model->paid_at, $model);
                return $model->balance / (!empty($rate) ? $rate : 1);
            },
            'format' => ['decimal', 2],
            'pageSummary' => Yii::$app->formatter->asDecimal(round($paymentsResult['balance'] ?? 0, 4)),
        ],
        [
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'value' => function ($model) use ($currencies) {
                return $currencies[$model->currency_id] ?? null;
            },
            'label' => Yii::t('common', 'Валюта'),
            'pageSummary' => $currencies[$reportModel->country->currency_id] ?? null,
        ],
        [
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'label' => Yii::t('common', 'Курс'),
            'value' => function ($model) use ($reportModel) {
                return round($reportModel->getRate($reportModel->country->currency_id, $model->currency_id, $model->paid_at, $model), 4);
            },
        ],
        [
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'content' => function ($model) use ($reportModel) {
                if ($model->active && Yii::$app->user->can('deliveryreport.report.removepayment')) {
                    return ButtonProgress::widget([
                        'attributes' => ['data-url' => Url::toRoute(['remove-payment', 'id' => $model->id, 'report_id' => $reportModel->id])],
                        'label' => Yii::t('common', 'Удалить'),
                        'size' => ButtonProgress::SIZE_SMALL . ' remove-payment',
                        'style' => ButtonProgress::STYLE_DANGER,
                    ]);
                } else {
                    return "—";
                }
            }
        ]
    ]
]) ?>
