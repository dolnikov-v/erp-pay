<?php
/**
 * @var string $url
 * @var string $updateUrl
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 * @var \yii\data\ArrayDataProvider $paymentList
 * @var array $currencies
 * @var array $paymentsResult
 */
use app\widgets\Modal;

?>

<?= Modal::widget([
    'id' => 'modal_payment_list',
    'title' => Yii::t('common', 'Платежи'),
    'size' => Modal::SIZE_LARGE,
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'reportModel' => $reportModel,
        'updateUrl' => $updateUrl,
        'paymentList' => $paymentList,
        'currencies' => $currencies,
        'paymentsResult' => $paymentsResult,
    ]),
]) ?>
