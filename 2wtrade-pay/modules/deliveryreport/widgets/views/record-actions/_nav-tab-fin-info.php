<?php

use app\components\widgets\ActiveForm;
use app\models\Currency;
use app\widgets\DateTimePicker;
use app\widgets\InputText;
use app\widgets\Select2;
use yii\grid\GridView;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\widgets\ButtonProgress;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $finInfo */

$queryParams = Yii::$app->request->getQueryParams();
$currentTab = Yii::$app->request->get('record_status');
$queryParams = Yii::$app->request->queryParams;
$queryParams['tab'] = 4;
unset($queryParams['finance_panel_currency']);
unset($queryParams['finance_panel_date']);
unset($queryParams['finance_panel_rate']);
$currencies = Currency::find()->collection();
?>

<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-4">
            <h4 class="report-balance"><?= Yii::t('common', 'Баланс: {balance} {currency}',
                    ['balance' => Yii::$app->formatter->asDecimal($reportModel->getBalance(), 2), 'currency' => $reportModel->country->currency->char_code]) ?></h4>
        </div>
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">
            <?php if ($currentTab == DeliveryReportRecord::STATUS_APPROVE || $currentTab == DeliveryReportRecord::STATUS_STATUS_UPDATED): ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (Yii::$app->user->can('deliveryreport.report.paymentlist')): ?>
                            <?= ButtonProgress::widget([
                                'label' => Yii::t('common', 'Платежи'),
                                'id' => 'btn_payment_list',
                                'style' => ButtonProgress::STYLE_PRIMARY . ' width-200',
                            ]) ?>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->can('deliveryreport.report.setorderprices')): ?>
                            <?= ButtonProgress::widget([
                                'label' => Yii::t('common', 'Обновить цены'),
                                'id' => 'btn_update_prices',
                                'style' => ButtonProgress::STYLE_PRIMARY . ' width-200',
                            ]) ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row margin-top-20 finance-panel-converter-container">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'method' => 'get',
        'action' => Url::toRoute(ArrayHelper::merge([Yii::$app->controller->action->id], $queryParams)),
    ]); ?>
    <div class="row">
        <div class="col-lg-2">
            <label for="finance_panel_currency"><?= Yii::t('common', 'Валюта отчета') ?></label>
            <?= Select2::widget([
                'id' => 'finance_panel_currency',
                'items' => $currencies,
                'name' => 'finance_panel_currency',
                'value' => $finInfo['currencyReport'],
                'prompt' => Yii::t('common', 'Валюта не указана'),
                'defaultValue' => 0,
            ]) ?>
        </div>
        <div class="col-lg-2 margin-top-30">
            <?= $form->submit(Yii::t('common', 'Применить')) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<div class="row">
    <div class="col-lg-12 margin-top-20">
        <?php if (empty($finInfo['rateErrors'])): ?>
        <?= GridView::widget([
            'layout' => '{items}',
            'rowOptions' => ['class' => 'tr-vertical-align-middle tr-text-align-center text-nowrap'],
            'headerRowOptions' => ['class' => 'tr-text-align-center'],
            'dataProvider' => new ArrayDataProvider(['models' => $finInfo['financeRows']]),
            'columns' => [
                [
                    'attribute' => 'title',
                    'label' => '',
                ],
                [
                    'attribute' => 'price_cod',
                    'label' => Yii::t('common', 'COD'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_storage',
                    'label' => Yii::t('common', 'Плата за хранение'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_fulfilment',
                    'label' => Yii::t('common', 'Плата за обслуживание'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_packing',
                    'label' => Yii::t('common', 'Плата за упаковывание'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_package',
                    'label' => Yii::t('common', 'Плата за упаковку'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_delivery',
                    'label' => Yii::t('common', 'Плата за доставку'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_redelivery',
                    'label' => Yii::t('common', 'Плата за передоставку'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_delivery_return',
                    'label' => Yii::t('common', 'Плата за возврат'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_delivery_back',
                    'label' => Yii::t('common', 'Плата за обратную доставку'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_cod_service',
                    'label' => Yii::t('common', 'Плата за наложенный платеж'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_vat',
                    'label' => Yii::t('common', 'НДС'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'price_address_correction',
                    'label' => Yii::t('common', 'Плата за коррекцию адреса доставки'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'total_charges',
                    'label' => Yii::t('common', 'Общая сумма расходов'),
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'to_pay',
                    'label' => Yii::t('common', 'Итого к переводу'),
                    'format' => ['decimal', 2]
                ]
            ]
        ]) ?>
        <?php else: ?>
            <?php foreach ($finInfo['rateErrors'] as $currID): ?>
                <p class="red-800"><?= Yii::t('common', 'Отсутствует курс для валюты "{char}"', ['char' => $currencies[$currID]]) ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
