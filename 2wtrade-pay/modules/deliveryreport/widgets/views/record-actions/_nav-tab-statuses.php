<?php

use app\components\widgets\ActiveForm;
use app\widgets\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;

/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $statuses */
/** @var array $availableStatuses */

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'method' => 'post',
]); ?>

<div class="status-report-form">
    <?php foreach ($statuses as $label => $status): ?>
        <div class="form-group">
            <label><?= $label ?></label>
            <?= Select2::widget([
                'items' => $availableStatuses,
                'name' => "status[" . base64_encode($label) . "]",
                'value' => $status,
                'prompt' => Yii::t('common', 'Статус не привязан'),
            ])?>
        </div>
    <?php endforeach; ?>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
