<?php

use app\components\widgets\ActiveForm;
use app\widgets\Select2;

/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $unBuyoutReasons */
/** @var array $availableUnBuyoutReasons */

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'method' => 'post',
]); ?>

<div class="status-report-form">
    <?php foreach ($unBuyoutReasons as $label => $reason): ?>
        <div class="form-group">
            <label><?= $label ?></label>
            <?= Select2::widget([
                'items' => $availableUnBuyoutReasons,
                'name' => "unbuyout_reason[" . base64_encode($label) . "]",
                'value' => $reason,
                'prompt' => Yii::t('common', 'Причина не привязана'),
            ])?>
        </div>
    <?php endforeach; ?>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
