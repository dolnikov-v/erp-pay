<?php

use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use yii\helpers\Url;
use app\widgets\Button;
use app\widgets\Select2;
use app\modules\deliveryreport\widgets\filters\ErrorFilter;
use app\modules\deliveryreport\widgets\filters\FieldTextFilter;
use app\modules\deliveryreport\widgets\filters\FieldNumberFilter;
use app\modules\deliveryreport\widgets\filters\StatusFilter;
use app\modules\deliveryreport\widgets\ExportDropdown;
use app\modules\deliveryreport\widgets\filters\OrderStatusFilter;
use app\modules\deliveryreport\widgets\filters\PretensionFilter;

/** @var yii\web\View $this */
/** @var \app\modules\deliveryreport\models\search\DeliveryReportRecordSearch $modelSearch */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */

?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', 'id' => $reportModel->id]),
    'method' => 'get',
]); ?>

<?= $modelSearch->restore($form) ?>

<div id="row_selectable_filters" class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <div class="control-label">
                <label><?= Yii::t('common', 'Параметр поиска') ?></label>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div id="container_selectable_filters" class="container-selectable-filters">
                        <?= Select2::widget([
                            'id' => 'filter_records_selectable_filters',
                            'items' => $modelSearch->selectableFilters,
                            'length' => -1,
                        ]) ?>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?= Button::widget([
                        'id' => 'add_selectable_filters',
                        'style' => Button::STYLE_SUCCESS . ' add-selectable-filters',
                        'icon' => WbIcon::PLUS,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute([
            'view',
            'id' => $reportModel->id,
            'record_status' => Yii::$app->request->get('record_status')
        ])) ?>

        <div class="pull-right">
            <?= ExportDropdown::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div id="container_empty_filters" style="display: none;">
    <?= ErrorFilter::widget(['form' => $form]) ?>
    <?= FieldTextFilter::widget(['form' => $form]) ?>
    <?= FieldNumberFilter::widget(['form' => $form]) ?>
    <?= StatusFilter::widget(['form' => $form, 'report' => $reportModel]) ?>
    <?= OrderStatusFilter::widget(['form' => $form]); ?>
    <?= PretensionFilter::widget(['form' => $form]); ?>
</div>
