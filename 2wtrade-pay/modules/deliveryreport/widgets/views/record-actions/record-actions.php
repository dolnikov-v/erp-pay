<?php

use app\modules\deliveryreport\models\DeliveryReport;
use app\widgets\Nav;
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\deliveryreport\models\search\DeliveryReportRecordSearch $modelSearch */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $statuses */
/** @var array $finInfo */
/** @var array $errorInfo */
/** @var array $availableStatuses */
/** @var array $orderIds */
/** @var \app\modules\deliveryreport\models\DeliveryReportCurrency $currencies */
/** @var array $currencyRate */

/** @var array $unBuyoutReasons */
/** @var array $availableUnBuyoutReasons */

$tabs = [
    [
        'label' => Yii::t('common', 'Фильтр поиска'),
        'content' => $this->render('_nav-tab-filters', [
            'modelSearch' => $modelSearch,
            'reportModel' => $reportModel
        ]),
    ],
    [
        'label' => Yii::t('common', 'Групповые операции'),
        'content' => $this->render('_nav-tab-group-operations', [
            'modelSearch' => $modelSearch,
            'reportModel' => $reportModel,
            'orderIds' => $orderIds,
        ]),
    ],
];

if (!empty($statuses)) {
    $tabs[] = [
        'label' => Yii::t('common', 'Статусы'),
        'content' => $this->render('_nav-tab-statuses', [
            'reportModel' => $reportModel,
            'statuses' => $statuses,
            'availableStatuses' => $availableStatuses,
        ]),
    ];
}

if (!empty($unBuyoutReasons)) {
    $tabs[] = [
        'label' => Yii::t('common', 'Причины недоставки'),
        'content' => $this->render('_nav-tab-unbuyout-reasons', [
            'reportModel' => $reportModel,
            'unBuyoutReasons' => $unBuyoutReasons,
            'availableUnBuyoutReasons' => $availableUnBuyoutReasons,
        ]),
    ];
}

$tabs[] = [
    'label' => Yii::t('common', 'Форматы'),
    'content' => $this->render('_nav-tab-formats', [
        'reportModel' => $reportModel,
    ])
];

if (!empty($finInfo) && $reportModel->type == DeliveryReport::TYPE_FINANCIAL) {
    $tabs[] = [
        'label' => Yii::t('common', 'Финансы'),
        'content' => $this->render('_nav-tab-fin-info', [
            'reportModel' => $reportModel,
            'finInfo' => $finInfo
        ]),
    ];
    $tabs[] = [
        'label' => Yii::t('common', 'Валюты'),
        'content' => $this->render('_nav-tab-currency', [
            'reportModel' => $reportModel,
            'currencies' => $currencies,
            'currencyRate' => $currencyRate,
        ]),
    ];
}
$tabs[] = [
    'label' => Yii::t('common', 'Сводка'),
    'content' => $this->render('_nav-tab-error-info', [
        'errorInfo' => $errorInfo
    ]),
];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => $tabs,
        'activeTab' => \Yii::$app->request->get('tab', 0),
    ]),
]) ?>
