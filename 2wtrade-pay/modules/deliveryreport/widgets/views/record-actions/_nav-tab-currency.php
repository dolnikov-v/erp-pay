<?php

use app\components\widgets\ActiveForm;
use app\models\Currency;
use yii\helpers\Url;
use app\widgets\ChangeableList;

/**
 * @var \app\modules\deliveryreport\models\DeliveryReport $reportModel
 * @var \app\modules\deliveryreport\models\DeliveryReportCurrency $currencies
 * @var \app\modules\deliveryreport\models\DeliveryReportCurrencyRate[] $currencyRate
 */

$queryParams = Yii::$app->request->getQueryParams();
$currentTab = Yii::$app->request->get('record_status');
$queryParams = Yii::$app->request->queryParams;
unset($queryParams['finance_panel_currency']);
unset($queryParams['finance_panel_date']);
unset($queryParams['finance_panel_rate']);
$currenciesArray = Currency::find()->collection();
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'method' => 'post',
    'action' => Url::toRoute(['set-currencies', 'id' => $reportModel->id]),
]); ?>
<?= $form->field($currencies, 'delivery_report_id')->hiddenInput(['value' => $reportModel->id])->label(false); ?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-lg-6 right-border-lg">
            <div class="row">
                <?php foreach ($currencies::getFinancialColumns() as $attribute): ?>
                    <div class="col-lg-6"><?= $form->field($currencies, $attribute)->select2List($currenciesArray); ?></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-lg-6">
            <?= ChangeableList::widget([
                'label' => '<div class="col-lg-12"><div class="row"><div class="col-sm-5">Из валюты (из = в * курс)</div><div class="col-sm-5">В валюту</div><div class="col-sm-2">Курс</div></div></div>',
                'template' => function () use ($currenciesArray, $form) {
                    return $this->render('_rate', [
                        'form' => $form,
                        'model' => (new \app\modules\deliveryreport\models\DeliveryReportCurrencyRate()),
                        'currency' => $currenciesArray
                    ]);
                },
                'items' => function () use ($currencyRate, $currenciesArray, $form) {
                    $data = [];
                    foreach ($currencyRate as $model) {
                        $data[] = $this->render('_rate', [
                            'form' => $form,
                            'model' => $model,
                            'currency' => $currenciesArray
                        ]);
                    }
                    return $data;
                },
                'callbackJsFuncAfterAddRow' => 'ChangeableList.refreshSelect'
            ]) ?>
        </div>
    </div>
</div>
<div class="col-lg-2 margin-top-20">
    <?= $form->submit(Yii::t('common', 'Применить'), ['id' => 'delivery_report_currency_form']) ?>
</div>
<?php ActiveForm::end(); ?>
