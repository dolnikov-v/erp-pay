<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'method' => 'post',
]); ?>

<div class="format-report-form">
    <div class="row">
        <div class="col-lg-3 form-group">
            <label><?= Yii::t('common', 'Формат дат') ?></label>
            <?= Html::textInput('formats[date]', $reportModel->date_format, ['class' => 'form-control']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
