<?php

use app\assets\vendor\BootstrapLaddaAsset;
use app\components\widgets\ActiveForm;
use app\modules\deliveryreport\models\DeliveryReport;
use app\widgets\ButtonLink;
use app\widgets\ButtonProgress;
use app\widgets\Link;
use yii\helpers\Url;
use app\modules\deliveryreport\models\DeliveryReportRecord;

BootstrapLaddaAsset::register($this);

/** @var DeliveryReport $reportModel */
/** @var array $orderIds */

$currentTab = Yii::$app->request->get('record_status');
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute(['view', Yii::$app->request->getQueryParams()]),
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-md-6">
        <?php if (Yii::$app->user->can('deliveryreport.report.setapprove') && ($currentTab == DeliveryReportRecord::STATUS_ACTIVE || ($reportModel->type != DeliveryReport::TYPE_FINANCIAL && $currentTab == DeliveryReportRecord::STATUS_STATUS_UPDATED))): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_approve',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Подтвердить записи'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.unsetapprove') && $currentTab == DeliveryReportRecord::STATUS_APPROVE): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_unset_approve',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Отменить подтверждение'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.deleterecords') && $currentTab != DeliveryReportRecord::STATUS_STATUS_UPDATED): ?>
            <div class="">
                <?= ButtonProgress::widget([
                    'id' => 'btn_remove_records',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Удалить записи'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-3">
        <?php
        if (Yii::$app->user->can('deliveryreport.report.createpretension') && $currentTab == DeliveryReportRecord::STATUS_APPROVE && $reportModel->type == DeliveryReport::TYPE_FINANCIAL): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_create_pretension',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Указать претензию'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.removepretension') && $reportModel->type == DeliveryReport::TYPE_FINANCIAL): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_remove_pretension',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Удалить претензии'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-3">
        <?php if (Yii::$app->user->can('deliveryreport.report.createduplicateorder') && $currentTab == DeliveryReportRecord::STATUS_ACTIVE): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_create_order_copy',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Создать дубликаты заказов'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.updateorderstatus') && $currentTab == DeliveryReportRecord::STATUS_APPROVE): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_update_status',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Обновить статус'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.updateordertracking') && ($currentTab == DeliveryReportRecord::STATUS_APPROVE || $currentTab == DeliveryReportRecord::STATUS_STATUS_UPDATED)): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_update_tracking',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Обновить трек номер'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('deliveryreport.report.updateorderdata') && ($currentTab == DeliveryReportRecord::STATUS_APPROVE || $currentTab == DeliveryReportRecord::STATUS_STATUS_UPDATED)): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_update_order_data',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'label' => Yii::t('common', 'Обновить данные'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('order.index.index')): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonLink::widget([
                    'label' => Yii::t('common', 'Показать заказы'),
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300',
                    'url' => Url::toRoute([
                        '/order/index/index',
                        'NumberFilter' => [
                            'entity' => 'id',
                            'number' => implode(',', $orderIds)
                        ],
                    ])
                ])
                ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
