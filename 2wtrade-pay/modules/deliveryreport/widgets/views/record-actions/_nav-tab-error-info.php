<?php

use app\widgets\Link;

/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
/** @var array $errorInfo */

$counter = 0;

?>

<div class="row">
    <div class="col-lg-3">
        <?php foreach ($errorInfo as $type => $data): ?>
        <?php if ($data['count'] > 0): ?>
        <?php if (($counter % 10) == 0 && $counter > 0): ?>
    </div>
    <div class="col-lg-4">
        <?php endif; ?>
        <p>
            <?= Link::widget([
                'label' => "{$data['label']} - {$data['count']}",
                'url' => '#tab0',
                'style' => '',
                'attributes' => [
                    'name' => 'error-type',
                    'data-error-type' => $type,
                    'data-toggle' => 'tab',
                    'aria-expanded' => 'true'
                ]
            ]) ?>
        </p>
        <?php $counter++; ?>
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
