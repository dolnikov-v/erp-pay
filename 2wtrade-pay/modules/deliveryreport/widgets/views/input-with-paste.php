<?php
use yii\helpers\Html;
use app\modules\deliveryreport\assets\InputWithPasteAsset;
/**
 * @var \yii\widgets\ActiveField $input
 * @var string $name
 * @var bool $find
 */

InputWithPasteAsset::register($this);
?>

<div class="input-group form-group <?= $find ? 'has-error' : '' ?>" style="width: 100%;">
    <div class="form-control-wrap">
        <?= $input ?>
    </div>
    <?php if ($find): ?>
        <span class="input-group-btn" style="padding-top: 30px; padding-left: 5px;">
            <?= Html::a(Html::tag('i', '', ['class' => 'font-size-16 fa fa-paste']), '#',
                [
                    'name' => $name,
                    'class' => 'btn-paste'
                ]) ?>
        </span>
    <?php endif; ?>
</div>
