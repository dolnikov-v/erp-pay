<?php

namespace app\modules\deliveryreport\widgets\filters;

use app\widgets\Widget;
use app\modules\deliveryreport\models\search\filters\FieldNumberFilter as FieldNumberFilterModel;

/**
 * Class FieldNumberFilter
 * @package app\modules\deliveryreport\widgets\filters
 */
class FieldNumberFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var FieldNumberFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new FieldNumberFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('field-number-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
