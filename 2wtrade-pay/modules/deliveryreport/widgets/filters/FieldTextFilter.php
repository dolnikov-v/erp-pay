<?php
namespace app\modules\deliveryreport\widgets\filters;

use app\modules\deliveryreport\models\search\filters\FieldTextFilter as FieldTextFilterModel;
use yii\base\Widget;
use Yii;

/**
 * Class TextFilter
 * @package app\modules\deliveryreport\widgets\filters
 */
class FieldTextFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var FieldTextFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new FieldTextFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('field-text-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
