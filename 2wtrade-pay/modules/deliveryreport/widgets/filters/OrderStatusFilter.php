<?php

namespace app\modules\deliveryreport\widgets\filters;

use app\widgets\Widget;
use app\modules\deliveryreport\models\search\filters\OrderStatusFilter as OrderStatusFilterModel;
use app\modules\deliveryreport\models\DeliveryReport;

class OrderStatusFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var OrderStatusFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /**
     * @var DeliveryReport
     */
    public $report;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new OrderStatusFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('order-status-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
