<?php
namespace app\modules\deliveryreport\widgets\filters;


use app\modules\deliveryreport\models\DeliveryReport;
use app\widgets\Widget;
use app\modules\deliveryreport\models\search\filters\StatusFilter as StatusFilterModel;

/**
 * Class StatusFilter
 * @package app\modules\deliveryreport\widgets\filters
 */
class StatusFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var StatusFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /**
     * @var DeliveryReport
     */
    public $report;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new StatusFilterModel(['report' => $this->report]);
            $this->useSelect2 = false;
        }

        if (!is_null($this->report)) {
            $this->model->statuses = $this->report->getStatusesNamed();
        }

        return $this->render('status-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
