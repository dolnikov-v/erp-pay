<?php

namespace app\modules\deliveryreport\widgets;

use app\modules\order\models\OrderFinancePretension;

/**
 * Class RemovePretension
 * @package app\modules\deliveryreport\widgets
 */
class RemovePretension extends Sender
{
    /**
     * @var array
     */
    protected $columns;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/remove-pretension/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel,
            'pretensionTypes' => OrderFinancePretension::getTypeCollecion()
        ]);
    }
}
