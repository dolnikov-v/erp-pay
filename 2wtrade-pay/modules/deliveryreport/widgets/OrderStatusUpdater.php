<?php

namespace app\modules\deliveryreport\widgets;

/**
 * Class OrderStatusUpdater
 * @package app\modules\deliveryreport\widgets
 */
class OrderStatusUpdater extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/order-status-updater/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
