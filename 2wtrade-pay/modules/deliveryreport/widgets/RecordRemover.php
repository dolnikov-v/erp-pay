<?php
namespace app\modules\deliveryreport\widgets;

/**
 * Class RecordRemover
 * @package app\modules\deliveryreport\widgets
 */
class RecordRemover extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/record-remover/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
