<?php

namespace app\modules\deliveryreport\widgets;


use app\widgets\Widget;
use app\modules\deliveryreport\models\search\DeliveryReportSearch;
use yii\base\InvalidParamException;
use Yii;

/**
 * Class ReportActions
 * @package app\modules\deliveryreport\widgets
 */
class ReportActions extends Widget
{
    /** @var DeliveryReportSearch */
    public $modelSearch;

    /**
     * @var array
     */
    public $withoutReportOrders;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->modelSearch instanceof DeliveryReportSearch) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать корректную модель поиска.'));
        }

        return $this->render('report-actions/report-actions', [
            'modelSearch' => $this->modelSearch,
            'withoutReportOrders' => $this->withoutReportOrders
        ]);
    }
}
