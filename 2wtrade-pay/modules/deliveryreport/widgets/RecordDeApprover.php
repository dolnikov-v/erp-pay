<?php
namespace app\modules\deliveryreport\widgets;

/**
 * Class RecordApprover
 * @package app\modules\deliveryreport\widgets
 */
class RecordDeApprover extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/record-de-approver/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
