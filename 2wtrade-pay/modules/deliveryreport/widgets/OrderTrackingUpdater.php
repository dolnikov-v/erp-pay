<?php

namespace app\modules\deliveryreport\widgets;

/**
 * Class OrderTrackingUpdater
 * @package app\modules\deliveryreport\widgets
 */
class OrderTrackingUpdater extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operations/order-tracking-updater/modal', [
            'url' => $this->url,
            'updateUrl' => $this->updateUrl,
            'reportModel' => $this->reportModel
        ]);
    }
}
