<?php
namespace app\modules\deliveryreport\widgets;

use yii\base\Widget;
use yii\widgets\ActiveField;

/**
 * Class InputWithPaste
 * @package app\modules\loadreport\widgets
 */
class InputWithPaste extends Widget
{
    /**
     * @var ActiveField
     */
    public $input;
    /**
     * @var array
     */
    public $fields = [];

    /**
     * @return string
     */
    public function run()
    {
        $find = array_search($this->input->attribute, $this->fields) !== false;
        return $this->render('input-with-paste', [
            'input' => $this->input,
            'name' => $this->input->attribute,
            'find' => $find,
        ]);
    }
}
