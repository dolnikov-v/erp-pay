<?php

namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class ReportViewReportAsset
 * @package app\modules\deliveryreport\assets
 */
class ReportViewReportAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/delivery-report/report';

    public $css = [
        'view-report.css'
    ];

    public $js = [
        'view-report.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
