<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;
/**
 * Class RecordDeApproverAsset
 * @package app\modules\deliveryreport\assets
 */
class RecordDeApproverAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/record-de-approver';

    public $css = [];

    public $js = [
        'record-de-approver.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
