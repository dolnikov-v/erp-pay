<?php

namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class RecordApproverAsset
 * @package app\modules\deliveryreport\assets
 */
class RecordApproverAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/record-approver';

    public $css = [];

    public $js = [
        'record-approver.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
