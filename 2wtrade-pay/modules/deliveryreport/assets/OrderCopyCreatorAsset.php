<?php

namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class OrderCopyCreatorAsset
 * @package app\modules\deliveryreport\assets
 */
class OrderCopyCreatorAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/order-copy-creator';

    public $css = [];

    public $js = [
        'order-copy-creator.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
