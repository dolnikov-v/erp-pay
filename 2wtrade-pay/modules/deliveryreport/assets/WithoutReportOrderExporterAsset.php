<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;
/**
 * Class WithoutReportOrderExporterAsset
 * @package app\modules\deliveryreport\assets
 */
class WithoutReportOrderExporterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/without-report-order-exporter';

    public $css = [];

    public $js = [
        'without-report-order-exporter.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
