<?php

namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class ShowerColumnsAsset
 * @package app\modules\deliveryreport\assets
 */
class ShowerColumnsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/shower-columns';

    public $css = [
        'shower-columns.css',
    ];

    public $js = [
        'shower-columns.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
