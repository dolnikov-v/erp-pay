<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class OrderStatusUpdaterAsset
 * @package app\modules\deliveryreport\assets
 */
class OrderStatusUpdaterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/order-status-updater';

    public $css = [];

    public $js = [
        'order-status-updater.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
