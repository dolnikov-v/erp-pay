<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;
/**
 * Class RemovePretensionAsset
 * @package app\modules\deliveryreport\assets
 */
class RemovePretensionAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/remove-pretension';

    public $css = [];

    public $js = [
        'remove-pretension.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
