<?php

namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;
/**
 * Class PaymentIdUpdaterAsset
 * @package app\modules\deliveryreport\assets
 */
class PaymentIdUpdaterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/payment-id-updater';

    public $js = [
        'payment-id-updater.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
