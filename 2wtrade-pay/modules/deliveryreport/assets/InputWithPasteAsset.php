<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class InputWithPasteAsset
 * @package app\modules\deliveryreport\assets
 */
class InputWithPasteAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/input-with-paste';

    public $js = [
        'input-with-paste.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
