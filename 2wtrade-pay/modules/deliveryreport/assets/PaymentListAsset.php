<?php

namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class PaymentListAsset
 * @package app\modules\deliveryreport\assets
 */
class PaymentListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/payment-list';

    public $js = [
        'payment-list.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
