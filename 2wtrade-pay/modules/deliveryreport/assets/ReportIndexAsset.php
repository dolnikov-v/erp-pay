<?php

namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;
/**
 * Class ReportIndexAsset
 * @package app\modules\deliveryreport\assets
 */
class ReportIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/delivery-report/report';

    public $css = [
        'index.css'
    ];

    public $js = [
        'index.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
