<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class RecordRemover
 * @package app\modules\deliveryreport\assets
 */
class RecordRemoverAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/record-remover';

    public $css = [];

    public $js = [
        'record-remover.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
