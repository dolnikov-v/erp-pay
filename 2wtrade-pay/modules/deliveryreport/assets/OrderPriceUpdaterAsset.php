<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class OrderPriceUpdaterAsset
 * @package app\modules\deliveryreport\assets
 */
class OrderPriceUpdaterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/order-price-updater';

    public $css = [];

    public $js = [
        'order-price-updater.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
