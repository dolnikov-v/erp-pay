<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class CostListAsset
 * @package app\modules\deliveryreport\assets
 */
class CostListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/cost-list';

    public $js = [
        'cost-list.js',
    ];

    public $css = [
        'cost-list.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];

    /**
     * @param \yii\web\View $view
     * @return AssetBundle
     */
    public static function register($view)
    {
        $view->registerJs("I18n['Издержки успешно сохранены.'] = '" .
            \Yii::t('common', 'Издержки успешно сохранены.')
            . "';", \yii\web\View::POS_HEAD);
        $view->registerJs("I18n['Издержки успешно удалены.'] = '" .
            \Yii::t('common', 'Издержки успешно удалены.')
            . "';", \yii\web\View::POS_HEAD);
        $view->registerJs("I18n['Не удалось сохранить информацию.'] = '" .
            \Yii::t('common', 'Не удалось сохранить информацию.')
            . "';", \yii\web\View::POS_HEAD);
        return parent::register($view);
    }
}
