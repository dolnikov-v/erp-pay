<?php

namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class ViewOrderAsset
 * @package app\modules\deliveryreport\assets
 */
class ViewOrderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/delivery-report/report';

    public $css = [];

    public $js = [
        'view-order.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
