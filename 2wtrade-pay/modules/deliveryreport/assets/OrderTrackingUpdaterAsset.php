<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class OrderTrackingUpdaterAsset
 * @package app\modules\deliveryreport\assets
 */
class OrderTrackingUpdaterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/order-tracking-updater';

    public $css = [];

    public $js = [
        'order-tracking-updater.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
