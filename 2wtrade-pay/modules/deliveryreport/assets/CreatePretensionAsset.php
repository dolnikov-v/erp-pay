<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;
/**
 * Class CreatePretensionAsset
 * @package app\modules\deliveryreport\assets
 */
class CreatePretensionAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/create-pretension';

    public $css = [];

    public $js = [
        'create-pretension.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
