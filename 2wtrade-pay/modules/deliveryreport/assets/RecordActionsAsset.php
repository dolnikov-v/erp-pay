<?php
namespace app\modules\deliveryreport\assets;

use yii\web\AssetBundle;

/**
 * Class RecordActionsAsset
 * @package app\modules\deliveryreport\assets
 */
class RecordActionsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/record-actions';

    public $css = [
        'record-actions.css'
    ];

    public $js = [
        'record-actions.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
