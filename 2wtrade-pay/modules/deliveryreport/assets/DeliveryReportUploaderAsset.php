<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;

/**
 * Class DeliveryReportUploaderAsset
 * @package app\modules\deliveryreport\assets
 */
class DeliveryReportUploaderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/delivery-report-uploader';

    public $css = [
        'delivery-report-uploader.css'
    ];

    public $js = [
        'delivery-report-uploader.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
