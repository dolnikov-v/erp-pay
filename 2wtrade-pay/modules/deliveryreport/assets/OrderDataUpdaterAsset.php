<?php
namespace app\modules\deliveryreport\assets;


use yii\web\AssetBundle;
/**
 * Class OrderDataUpdaterAsset
 * @package app\modules\deliveryreport\assets
 */
class OrderDataUpdaterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery-report/order-data-updater';

    public $css = [];

    public $js = [
        'order-data-updater.js'
    ];

    public $depends = [
        'app\modules\deliveryreport\assets\ReportViewReportAsset',
    ];
}
