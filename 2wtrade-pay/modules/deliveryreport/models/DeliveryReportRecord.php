<?php

namespace app\modules\deliveryreport\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\Product;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\components\scanner\ReportScanner;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Class DeliveryReportRecord
 *
 * @property integer $id
 * @property string $order_id
 * @property string $call_center_id
 * @property string $tracking
 * @property string $status
 * @property integer $status_id
 * @property string $customer_full_name
 * @property string $customer_phone
 * @property string $customer_address
 * @property string $customer_zip
 * @property string $customer_city
 * @property string $customer_region
 * @property string $customer_district
 * @property string $comment
 * @property string $products
 * @property string $amount
 * @property string $price
 * @property string $date_created
 * @property string $date_approve
 * @property string $date_return
 * @property string $date_payment
 * @property string $price_storage
 * @property string $price_fulfilment
 * @property string $price_packing
 * @property string $price_package
 * @property string $price_address_correction
 * @property string $price_delivery
 * @property string $price_redelivery
 * @property string $price_delivery_back
 * @property string $price_delivery_return
 * @property string $price_cod_service
 * @property string $price_vat
 * @property string $price_cod
 * @property integer $delivery_report_id
 * @property string $error_log
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $record_status
 * @property string $payment_reference_number
 * @property string $delivery_time_from
 *
 * @property string[] $errorFields
 * @property string[] $errorMessages
 * @property string $statusName
 *
 * @property DeliveryReport $report
 * @property Order $order
 * @property Order $relatedOrder
 * @property DeliveryReportRecord $convertedOrder
 * @property DeliveryRequest[] $deliveryRequest
 * @property DeliveryRequest $firstDeliveryRequest
 * @property DeliveryRequest $firstDeliveryRequestByFinance
 * @property CallCenterRequest $callCenterRequest
 * @property DeliveryReportRecordError[] $recordErrors
 * @property OrderFinanceFact $financeFact
 */
class DeliveryReportRecord extends ActiveRecordLogUpdateTime
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';
    const STATUS_USED = 'used';
    const STATUS_APPROVE = 'approve';
    const STATUS_STATUS_UPDATED = 'status_updated';
    const STATUS_EXPORT = 'export';

    const COLUMN_ORDER_ID = 'order_id';
    const COLUMN_TRACKING = 'tracking';
    const COLUMN_STATUS = 'status';
    const COLUMN_CUSTOMER_FULL_NAME = 'customer_full_name';
    const COLUMN_CUSTOMER_ADDRESS = 'customer_address';
    const COLUMN_CUSTOMER_PHONE = 'customer_phone';
    const COLUMN_CUSTOMER_ZIP = 'customer_zip';
    const COLUMN_CUSTOMER_CITY = 'customer_city';
    const COLUMN_CUSTOMER_REGION = 'customer_region';
    const COLUMN_CUSTOMER_DISTRICT = 'customer_district';
    const COLUMN_COMMENT = 'comment';
    const COLUMN_PRODUCTS = 'products';
    const COLUMN_AMOUNT = 'amount';
    const COLUMN_PRICE = 'price';
    const COLUMN_PRICE_COD = 'price_cod';
    const COLUMN_DATE_CREATED = 'date_created';
    const COLUMN_DATE_APPROVE = 'date_approve';
    const COLUMN_DATE_PAYMENT = 'date_payment';
    const COLUMN_DATE_RETURN = 'date_return';
    const COLUMN_PRICE_DELIVERY = 'price_delivery';
    const COLUMN_PRICE_REDELIVERY = 'price_redelivery';
    const COLUMN_PRICE_STORAGE = 'price_storage';
    const COLUMN_PRICE_PACKING = 'price_packing';
    const COLUMN_PRICE_PACKAGE = 'price_package';
    const COLUMN_PRICE_DELIVERY_BACK = 'price_delivery_back';
    const COLUMN_PRICE_DELIVERY_RETURN = 'price_delivery_return';
    const COLUMN_PRICE_COD_SERVICE = 'price_cod_service';
    const COLUMN_PRICE_ADDRESS_CORRECTION = 'price_address_correction';
    const COLUMN_PAYMENT_REFERENCE_NUMBER = 'payment_reference_number';
    const COLUMN_DELIVERY_TIME_FROM = 'delivery_time_from';
    const COLUMN_CALL_CENTER_ID = 'call_center_id';
    const COLUMN_PRICE_FULFILMENT = 'price_fulfilment';
    const COLUMN_PRICE_VAT = 'price_vat';

    const COLUMN_CALL_CENTER_PENALTY = 'call_center_penalty';
    const COLUMN_UNBUYOUT_REASONS = 'unbuyout_reasons';

    const ERROR_DUPLICATE_TRACK = 1;
    const ERROR_ORDER_NOT_EXIST = 2;
    const ERROR_INCORRECT_ORDERID = 3;
    const ERROR_INCORRECT_TRACK = 4;
    const ERROR_NOT_EQUAL_TRACK = 5;
    const ERROR_NOT_EQUAL_AMOUNT = 6;
    const ERROR_NOT_EQUAL_PRICE = 7;
    const ERROR_NOT_EQUAL_ORDER_DATA = 8;
    const ERROR_NOT_EQUAL_STATUS = 9;
    const ERROR_INCORRECT_STATUS = 10;
    const ERROR_FIND_ORDERID_BY_TRACK = 11;
    const ERROR_NOT_EQUAL_ORDER_NAME = 12;
    const ERROR_NOT_EQUAL_ORDER_PHONE = 13;
    const ERROR_NOT_EQUAL_ORDER_ADDRESS = 14;
    const ERROR_NOT_FOUND = 15;
    const ERROR_DUPLICATE_ORDERID = 16;
    const ERROR_FROM_ANOTHER_COUNTRY = 17;
    const ERROR_NOT_EQUAL_COD = 18;
    const ERROR_HIGH_COD = 45;
    const ERROR_LESS_COD = 46;
    const ERROR_INCORRECT_DATE_CREATED = 19;
    const ERROR_INCORRECT_DATE_APPROVE = 20;
    const ERROR_INCORRECT_DATE_PAYMENT = 21;
    const ERROR_INCORRECT_DATE_RETURN = 22;
    const ERROR_NOT_EQUAL_PAYMENT_NUMBER = 23;
    const ERROR_ORDER_FIND_BY_NAME_ADDRESS_PHONE = 24;
    const ERROR_INCORRECT_DELIVERY_TIME_FROM = 25;
    const ERROR_ORDER_FOUND_BY_FINANCE_TRACK = 26;
    const ERROR_FROM_ANOTHER_DELIVERY = 27;
    const ERROR_FIND_ORDER_ID_BY_DUPLICATE = 28;
    const ERROR_NOT_EQUAL_COSTS = 29;
    const ERROR_INCORRECT_PRODUCTS_FORMAT = 30;
    const ERROR_PRODUCT_NOT_FOUND = 31;
    const ERROR_ORDER_FIND_IN_ANOTHER_REPORT = 32;
    const ERROR_ORDER_IN_FINAL_STATUS = 33;

    const ERROR_NOT_EQUAL_COSTS_STORAGE = 34;
    const ERROR_NOT_EQUAL_COSTS_FULFILMENT = 35;
    const ERROR_NOT_EQUAL_COSTS_PACKING = 36;
    const ERROR_NOT_EQUAL_COSTS_PACKAGE = 37;
    const ERROR_NOT_EQUAL_COSTS_DELIVERY = 38;
    const ERROR_NOT_EQUAL_COSTS_REDELIVERY = 39;
    const ERROR_NOT_EQUAL_COSTS_DELIVERY_BACK = 40;
    const ERROR_NOT_EQUAL_COSTS_DELIVERY_RETURN = 41;
    const ERROR_NOT_EQUAL_COSTS_ADDRESS_CORRECTION = 42;
    const ERROR_NOT_EQUAL_COSTS_COD_SERVICE = 43;
    const ERROR_NOT_EQUAL_COSTS_VAT = 44;

    /**
     * @var array
     */
    protected $_errorFields;

    /**
     * @var array
     */
    protected $_errorMessages;

    /**
     * @var null | Order
     */
    protected $relatedOrder = null;

    /**
     * @var null | self
     */
    protected $convertedOrder = null;

    /**
     * @var bool
     */
    protected $checkConvertedOrder = false;

    /**
     * @var bool
     */
    protected $findByDuplicateOrder = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report_record}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_id', 'created_at', 'updated_at', 'status_id'], 'integer'],
            [
                [
                    'order_id',
                    'customer_phone',
                    'customer_zip',
                    'customer_city',
                    'customer_region',
                    'customer_district',
                    'amount',
                    'price'
                ],
                'string',
                'max' => 100
            ],
            [['error_log', 'record_status'], 'string'],
            [['tracking'], 'string', 'max' => 30],
            [['status', 'payment_reference_number'], 'string', 'max' => 255],
            [['customer_full_name', 'products'], 'string', 'max' => 200],
            [['comment'], 'string', 'max' => 2000],
            [['customer_address'], 'string', 'max' => 500],
            [
                [
                    'date_created',
                    'date_approve',
                    'date_return',
                    'date_payment',
                    'price_storage',
                    'price_packing',
                    'price_package',
                    'price_address_correction',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_cod_service',
                    'price_cod',
                    'delivery_time_from',
                    'call_center_id',
                    'price_fulfilment',
                    'price_vat',
                ],
                'string',
                'max' => 50
            ],
            [
                ['delivery_report_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryReport::className(),
                'targetAttribute' => ['delivery_report_id' => 'id']
            ],
            ['record_status', 'default', 'value' => self::STATUS_ACTIVE],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return self::getAttributeLabels();
    }

    /**
     * @return array
     */
    public static function getAttributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'tracking' => Yii::t('common', 'Трек номер'),
            'status' => Yii::t('common', 'Статус заказа'),
            'status_id' => Yii::t('common', 'ID статуса заказа'),
            'statusName' => Yii::t('common', 'Статус заказа'),
            'customer_full_name' => Yii::t('common', 'Полное имя заказчика'),
            'customer_phone' => Yii::t('common', 'Телефон заказчика'),
            'customer_address' => Yii::t('common', 'Адрес заказчика'),
            'customer_zip' => Yii::t('common', 'Почтовый индекс'),
            'customer_city' => Yii::t('common', 'Город'),
            'customer_region' => Yii::t('common', 'Регион'),
            'customer_district' => Yii::t('common', 'Район'),
            'comment' => Yii::t('common', 'Комментарий'),
            'products' => Yii::t('common', 'Товары'),
            'amount' => Yii::t('common', 'Количество товаров'),
            'price' => Yii::t('common', 'Цена'),
            'date_created' => Yii::t('common', 'Дата создания заказа'),
            'date_approve' => Yii::t('common', 'Дата подтверждения заказа'),
            'date_return' => Yii::t('common', 'Дата возврата заказа'),
            'date_payment' => Yii::t('common', 'Дата оплаты заказа'),
            'price_storage' => Yii::t('common', 'Цена за хранение'),
            'price_packing' => Yii::t('common', 'Цена за упаковывание'),
            'price_package' => Yii::t('common', 'Цена за упаковку'),
            'price_address_correction' => Yii::t('common', 'Цена за коррекцию адреса доставки'),
            'price_delivery' => Yii::t('common', 'Цена за доставку'),
            'price_redelivery' => Yii::t('common', 'Цена передоставки'),
            'price_delivery_back' => Yii::t('common', 'Цена за возврат'),
            'price_delivery_return' => Yii::t('common', 'Цена на обратную доставку'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_cod' => Yii::t('common', 'COD'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'record_status' => Yii::t('common', 'Статус записи'),
            'relatedOrder.finance.payment' => Yii::t('common', 'Номер платежа'),
            'payment_reference_number' => Yii::t('common', 'Номер платежа'),
            'delivery_time_from' => Yii::t('common', 'Дата доставки'),
            'report.delivery.name' => Yii::t('common', 'Служба доставки'),
            'call_center_id' => Yii::t('common', 'Номер в колл-центре'),
            'price_fulfilment' => Yii::t('common', 'Плата за обслуживание'),
            'price_vat' => Yii::t('common', 'НДС')
        ];
    }

    /**
     * @param string $label
     * @return mixed
     */
    public static function attributeLabel($label)
    {
        $labels = self::getAttributeLabels();
        if (isset($labels[$label])) {
            return $labels[$label];
        }

        return $label;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordErrors()
    {
        return $this->hasMany(DeliveryReportRecordError::className(), ['delivery_report_record_id' => 'id'])
            ->indexBy('error_id')->inverseOf('deliveryReportRecord');
    }

    /**
     * @return null|\yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(),
            ['foreign_id' => 'call_center_id'])
            ->andWhere([CallCenter::tableName() . '.country_id' => $this->report ? $this->report->country_id : null])
            ->joinWith('callCenter', false);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getDeliveryRequest()
    {
        return $this->hasMany(DeliveryRequest::className(), ['tracking' => 'tracking'])->where([
            '!=',
            DeliveryRequest::tableName() . '.tracking',
            ""
        ]);
    }

    /**
     * @return array|null| \yii\db\ActiveQuery
     */
    public function getFirstDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(),
            ['tracking' => 'tracking'])->where([
            '!=',
            DeliveryRequest::tableName() . '.tracking',
            ""
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstDeliveryRequestByFinance()
    {
        return $this->hasOne(DeliveryRequest::className(), ['finance_tracking' => 'tracking'])->andWhere([
            '!=',
            'finance_tracking',
            ""
        ]);
    }

    /**
     * @return int|string
     */
    public function getDeliveryRequestCount()
    {
        if ($this->tracking == '') {
            return 0;
        }
        return $this->hasMany(DeliveryRequest::className(), ['tracking' => 'tracking'])->where([
            '!=',
            'tracking',
            ""
        ])->count();
    }

    /**
     * @return array | bool
     */
    public function getConvertedProducts()
    {
        $products = [];
        if (!empty(trim($this->products))) {
            $explodedStrings = explode(',', trim($this->products));
            foreach ($explodedStrings as $string) {
                if (preg_match('@([0-9]+)[\s]*-[\s]*([[:ascii:]]+)@', $string, $matches)) {

                    $quantity = $matches[1];
                    $productName = $matches[2];
                    if (!isset($products[$productName])) {
                        $product = [
                            'quantity' => $quantity,
                            'name' => $productName,
                            'product_id' => '',
                            'price' => 0,
                        ];
                        if ($productOriginal = Product::find()->where(['name' => $productName])->one()) {
                            $product['product_id'] = $productOriginal->id;
                            $price = $productOriginal->getPriceByCurrency($this->report->delivery->country->currency_id, $this->report->delivery->country_id);
                            if ($price) {
                                $product['price'] = $price->price;
                            }
                        }
                        $products[$productName] = $product;
                    } else {
                        $products[$productName]['quantity'] += $quantity;
                    }
                    continue;
                }
                return false;
            }
        }
        return $products;
    }

    /**
     * @return Order | null
     */
    public function getRelatedOrder()
    {
        if (!empty($this->relatedOrder)) {
            return $this->relatedOrder;
        }
        if ($this->relatedOrder !== null) {
            return null;
        }
        $order = $this->order;

        $checkDuplicateOrder = true;

        if (empty($order) && !empty($this->firstDeliveryRequest)) {
            $order = $this->firstDeliveryRequest->order;
            $checkDuplicateOrder = false;
        }

        if (empty($order) && !empty($this->callCenterRequest)) {
            $order = $this->callCenterRequest->order;
            $checkDuplicateOrder = true;
        }

        if (empty($order) && !empty($this->recordErrors)) {
            $order_id = '';
            $error = $this->recordErrors[self::ERROR_ORDER_NOT_EXIST] ?? $this->recordErrors[self::ERROR_NOT_EQUAL_ORDER_DATA] ?? $this->recordErrors[self::ERROR_ORDER_FIND_BY_NAME_ADDRESS_PHONE] ?? null;
            if (!is_null($error)) {
                if ($error->decodedValues) {
                    $order_id = $error->decodedValues[0];
                    // $order_id = array_shift($error->decodedValues);
                }
            }
            if (!empty($order_id)) {
                $order = Order::findOne($order_id);
            }
        }

        if (!empty($order) && $checkDuplicateOrder && $order->country_id == $this->report->country_id
            && $order->deliveryRequest
            && $order->deliveryRequest->delivery_id != $this->report->delivery_id
            && $order->duplicateOrder
            && (($order->duplicateOrder->deliveryRequest && $order->duplicateOrder->deliveryRequest->delivery_id == $this->report->delivery_id) || !$order->duplicateOrder->deliveryRequest)
        ) {
            $order = $order->duplicateOrder;
            $this->findByDuplicateOrder = true;
        }

        $this->relatedOrder = $order;
        return $order;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function setRelatedOrder($order)
    {
        if ($order instanceof Order) {
            $this->relatedOrder = $order;
        }
        return $this->relatedOrder;
    }

    /**
     * @return self|null
     */
    public function getConvertedOrder()
    {
        $bufferOrder = new self();

        if (!$this->checkConvertedOrder && empty($this->convertedOrder)) {

            $this->checkConvertedOrder = true;

            $order = $this->getRelatedOrder();

            if (!empty($order)) {
                $bufferOrder = $this->setConvertedOrder($order);
                $bufferOrder->findByDuplicateOrder = $this->findByDuplicateOrder;
            }
        } elseif ($this->convertedOrder !== null) {
            $bufferOrder = $this->convertedOrder;
        }
        return $bufferOrder;
    }

    /**
     * @param self|Order $order
     * @return self
     */
    public function setConvertedOrder($order)
    {
        if ($order instanceof static) {
            $bufferOrder = $order;
        } else {

            if (!($order instanceof Order) && !empty($order)) {
                $order = Order::find()->where(['id' => $order])->one();
            }

            $bufferOrder = static::convertOrder($order);
            $bufferOrder->findByDuplicateOrder = $this->findByDuplicateOrder;
        }
        $this->convertedOrder = $bufferOrder;
        return $bufferOrder;
    }

    /**
     * @param $order
     * @return DeliveryReportRecord
     */
    public static function convertOrder($order)
    {
        $bufferOrder = new DeliveryReportRecord();

        if (!empty($order) && $order instanceof Order) {
            $bufferOrder->order_id = $order->id;
            $bufferOrder->customer_full_name = $order->customer_full_name;
            $bufferOrder->customer_phone = $order->customer_phone;
            $bufferOrder->customer_address = $order->customer_address;
            $bufferOrder->customer_city = $order->customer_city;
            $bufferOrder->customer_region = $order->customer_province;
            $bufferOrder->customer_district = $order->customer_street;
            $bufferOrder->customer_zip = $order->customer_zip;
            if (!empty($order->status_id)) {
                $bufferOrder->status = Yii::t('common', $order->status->name);
                $bufferOrder->status_id = $order->status_id;
            }
            $bufferOrder->comment = $order->comment;

            if ($order->callCenterRequest) {
                $bufferOrder->call_center_id = $order->callCenterRequest->foreign_id;
            }

            /**
             * @var DeliveryRequest $shipping
             */
            $shipping = $order->deliveryRequest;

            if (!empty($shipping)) {

                $bufferOrder->tracking = $shipping->tracking;
                if ($shipping->accepted_at > 0) {
                    $bufferOrder->date_created = Yii::$app->formatter->asDate($shipping->accepted_at);
                }
                if ($shipping->approved_at > 0) {
                    $bufferOrder->date_approve = Yii::$app->formatter->asDate($shipping->approved_at);
                }
                if ($shipping->paid_at > 0) {
                    $bufferOrder->date_payment = Yii::$app->formatter->asDate($shipping->paid_at);
                }
                if ($shipping->returned_at > 0) {
                    $bufferOrder->date_return = Yii::$app->formatter->asDate($shipping->returned_at);
                }
            }
            $orderProducts = $order->orderProducts;
            $products = [];

            $amount = 0;

            if (!empty($orderProducts)) {
                foreach ($orderProducts as $product) {
                    $products[] = $product->quantity . " - " . $product->product->name;
                    $amount += $product->quantity;
                }
            }
            $bufferOrder->products = implode(", ", $products);
            $bufferOrder->amount = $amount;
            $bufferOrder->price = $order->price_total;
            $bufferOrder->price_cod = $order->price_total + $order->delivery;

            $fin = $order->financePrediction;

            if (!empty($fin)) {
                if ($fin->price_cod != '') {
                    $bufferOrder->price_cod = $fin->price_cod;
                }

                $bufferOrder->price_delivery = $fin->price_delivery;
                $bufferOrder->price_storage = $fin->price_storage;
                $bufferOrder->price_packing = $fin->price_packing;
                $bufferOrder->price_package = $fin->price_package;
                $bufferOrder->price_redelivery = $fin->price_redelivery;
                $bufferOrder->price_delivery_back = $fin->price_delivery_back;
                $bufferOrder->price_delivery_return = $fin->price_delivery_return;
                $bufferOrder->price_cod_service = $fin->price_cod_service;
                $bufferOrder->price_address_correction = $fin->price_address_correction;
                $bufferOrder->price_fulfilment = $fin->price_fulfilment;
                $bufferOrder->price_vat = $fin->price_vat;
            }
            $bufferOrder->error_log = ($order->report_status_id == $order->status_id);

            $bufferOrder->delivery_time_from = Yii::$app->formatter->asDate($order->delivery_time_from);
            $bufferOrder->relatedOrder = $order;
        }

        return $bufferOrder;
    }

    /**
     * @return array|mixed
     */
    public function getErrorFields()
    {
        if (!is_null($this->_errorFields)) {
            return $this->_errorFields;
        }

        $errors = $this->recordErrors;
        $errFields = [];

        if (is_array($errors)) {
            foreach ($errors as $error) {
                switch ($error->error_id) {
                    case self::ERROR_DUPLICATE_ORDERID: {
                        $errorFields[] = 'order_id';
                        break;
                    }
                    case self::ERROR_DUPLICATE_TRACK: {
                        $errorFields[] = 'tracking';
                        break;
                    }
                    case self::ERROR_ORDER_FIND_IN_ANOTHER_REPORT:
                    case self::ERROR_INCORRECT_ORDERID: {
                        $errFields[] = 'order_id';
                        break;
                    }
                    case self::ERROR_INCORRECT_TRACK: {
                        $errFields[] = 'tracking';
                        break;
                    }
                    case self::ERROR_ORDER_NOT_EXIST: {
                        $errFields[] = 'order_id';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_TRACK: {
                        $errFields[] = 'tracking';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_PRICE: {
                        $errFields[] = 'price';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_AMOUNT: {
                        $errFields[] = 'amount';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_STATUS:
                    case self::ERROR_ORDER_IN_FINAL_STATUS:
                    case self::ERROR_INCORRECT_STATUS: {
                        $errFields[] = 'status';
                        break;
                    }
                    case self::ERROR_FIND_ORDERID_BY_TRACK: {
                        $errFields[] = 'order_id';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_ORDER_NAME: {
                        $errFields[] = 'customer_full_name';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_ORDER_PHONE: {
                        $errFields[] = 'customer_phone';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_ORDER_ADDRESS: {
                        $errFields[] = 'customer_address';
                        break;
                    }
                    case self::ERROR_FROM_ANOTHER_COUNTRY: {
                        $errFields[] = 'relatedOrder.country.name';
                        break;
                    }
                    case self::ERROR_FROM_ANOTHER_DELIVERY: {
                        $errFields[] = 'report.delivery.name';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_COD:
                    case self::ERROR_HIGH_COD:
                    case self::ERROR_LESS_COD: {
                        $errFields[] = 'price_cod';
                        break;
                    }
                    case self::ERROR_INCORRECT_DATE_CREATED: {
                        $errFields[] = 'date_created';
                        break;
                    }
                    case self::ERROR_INCORRECT_DATE_APPROVE: {
                        $errFields[] = 'date_approve';
                        break;
                    }
                    case self::ERROR_INCORRECT_DATE_PAYMENT: {
                        $errFields[] = 'date_payment';
                        break;
                    }
                    case self::ERROR_INCORRECT_DATE_RETURN: {
                        $errFields[] = 'date_return';
                        break;
                    }
                    case self::ERROR_INCORRECT_DELIVERY_TIME_FROM: {
                        $errFields[] = 'delivery_time_from';
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_COSTS: {
                        if ($bufferOrder = $this->getConvertedOrder()) {
                            $checkingColumns = [
                                'price_storage',
                                'price_packing',
                                'price_package',
                                'price_address_correction',
                                'price_delivery',
                                'price_redelivery',
                                'price_delivery_back',
                                'price_delivery_return',
                                'price_cod_service',
                                'price_fulfilment',
                                'price_vat',
                            ];

                            foreach ($checkingColumns as $column) {
                                if (($this->$column != '' && !is_null($this->$column) && round($this->$column, 2) != round($bufferOrder->$column, 2)) || (empty($this->$column) && !empty($bufferOrder->$column)) || (!empty($this->$column) && empty($bufferOrder->$column))) {
                                    $errFields[] = $column;
                                }
                            }
                        }
                        break;
                    }
                    case self::ERROR_PRODUCT_NOT_FOUND:
                    case self::ERROR_INCORRECT_PRODUCTS_FORMAT: {
                        $errFields[] = self::COLUMN_PRODUCTS;
                        break;
                    }
                }
            }
        }
        $this->_errorFields = $errFields;
        return $this->_errorFields;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        if (!is_null($this->_errorMessages)) {
            return $this->_errorMessages;
        }

        $errors = $this->recordErrors;

        $messages = [];

        $errorLabels = self::errorLabels();

        if (is_array($errors)) {
            foreach ($errors as $error) {
                switch ($error->error_id) {
                    case self::ERROR_DUPLICATE_ORDERID: {
                        $values = implode(", ", array_map(function ($a) {
                            return Html::a('#' . $a, Url::toRoute(['view-order', 'id' => $a]));
                        }, $error->decodedValues));
                        $message = Yii::t('common', 'В отчете найдено несколько заказов с данным Order ID: {values}',
                            ['values' => $values]);
                        $messages[] = $message;
                        break;
                    }
                    case self::ERROR_DUPLICATE_TRACK: {
                        $values = implode(", ", array_map(function ($a) {
                            return Html::a('#' . $a, Url::toRoute(['view-order', 'id' => $a]));
                        }, $error->decodedValues));
                        $message = Yii::t('common',
                            'В отчете найдено несколько заказов с данным трек номером: {values}',
                            ['values' => $values]);
                        $messages[] = $message;
                        break;
                    }
                    case self::ERROR_INCORRECT_ORDERID: {
                        $messages[] = Yii::t('common', 'Некорректный Order ID');
                        break;
                    }
                    case self::ERROR_INCORRECT_TRACK: {
                        $messages[] = Yii::t('common', 'Некорректный трек номер');
                        break;
                    }
                    case self::ERROR_ORDER_NOT_EXIST: {
                        $messages[] = Yii::t('common', 'Заказа с такими данными не существует.');
                        if (count($error->decodedValues) > 0) {
                            $values = [];
                            $order = $this->getRelatedOrder();
                            $fractOrders = $error->decodedValues;
                            if (!empty($order)) {
                                if (!Yii::$app->request->get('order_id') || Yii::$app->request->get('order_id') == $order->id) {
                                    $values[] = "#$order->id";
                                } else {
                                    $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                        ['view-order', 'order_id' => $order->id]));
                                    $values[] = Html::a("#$order->id", $url);
                                }

                                if (($index = array_search($order->id, $fractOrders)) !== false) {
                                    unset($fractOrders[$index]);
                                }
                            }
                            foreach ($fractOrders as $val) {
                                $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                    ['view-order', 'order_id' => $val, 'id' => $this->id]));
                                if (Yii::$app->request->get('order_id') == $val) {
                                    $values[] = "#$val";
                                } else {
                                    $values[] = Html::a('#' . $val, $url);
                                }
                            }
                            $values = implode(", ", $values);

                            $messages[] = Yii::t('common', 'В базе частично совпадают следующие заказы: {values}',
                                ['values' => $values]);
                        }
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_TRACK: {
                        $messages[] = Yii::t('common', 'Не совпадает трек номер.');
                        if (isset($error->decodedValues['order_id'])) {
                            $values = [];
                            $order = $this->getRelatedOrder();
                            $fractOrders = $error->decodedValues['order_id'];
                            if (!empty($order)) {
                                if (is_null(Yii::$app->request->get('order_id')) || Yii::$app->request->get('order_id') == $order->id) {
                                    $values[] = "#$order->id";
                                } else {
                                    $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                        ['view-order', 'order_id' => $order->id]));
                                    $values[] = Html::a("#$order->id", $url);
                                }

                                if (($index = array_search($order->id, $fractOrders)) !== false) {
                                    unset($fractOrders[$index]);
                                }
                            }
                            foreach ($fractOrders as $val) {
                                $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                    ['view-order', 'order_id' => $val, 'id' => $this->id]));
                                if (Yii::$app->request->get('order_id') == $val) {
                                    $values[] = "#$val";
                                } else {
                                    $values[] = Html::a('#' . $val, $url);
                                }
                            }
                            $values = implode(", ", $values);
                            $messages[] = Yii::t('common',
                                'Данный трек номер используется в следующих заказах: {values}',
                                ['values' => $values]);
                        }

                        if (isset($error->decodedValues['tracking'])) {
                            $messages[] = Yii::t('common',
                                'Для заказа с данным Order ID используется трек: "{value}"',
                                ['value' => $error->decodedValues['tracking']]);
                        }
                        break;
                    }
                    case self::ERROR_NOT_EQUAL_ORDER_DATA: {
                        $messages[] = $errorLabels[self::ERROR_NOT_EQUAL_ORDER_DATA];
                        if (count($error->decodedValues) > 0) {
                            $values = [];
                            $order = $this->getRelatedOrder();
                            $fractOrders = $error->decodedValues;
                            if (!empty($order)) {
                                if (!Yii::$app->request->get('order_id') || Yii::$app->request->get('order_id') == $order->id) {
                                    $values[] = "#$order->id";
                                } else {
                                    $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                        ['view-order', 'order_id' => $order->id]));
                                    $values[] = Html::a("#$order->id", $url);
                                }

                                if (($index = array_search($order->id, $fractOrders)) !== false) {
                                    unset($fractOrders[$index]);
                                }
                            }
                            foreach ($fractOrders as $val) {
                                $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                    ['view-order', 'order_id' => $val, 'id' => $this->id]));
                                if (Yii::$app->request->get('order_id') == $val) {
                                    $values[] = "#$val";
                                } else {
                                    $values[] = Html::a('#' . $val, $url);
                                }
                            }
                            $values = implode(", ", $values);

                            $messages[] = Yii::t('common', 'В базе частично совпадают следующие заказы: {values}',
                                ['values' => $values]);
                        }
                        break;
                    }
                    case self::ERROR_FIND_ORDERID_BY_TRACK: {
                        if (isset($error->decodedValues['order_id'])) {
                            $messages[] = $errorLabels[self::ERROR_FIND_ORDERID_BY_TRACK];
                            if (empty($order_id)) {
                                $order_id = $error->decodedValues['order_id'];
                            }
                        } else {
                            $values = [];
                            $order = $this->getRelatedOrder();
                            foreach ($error->decodedValues as $val) {
                                $url = Url::toRoute(array_merge(Yii::$app->request->queryParams,
                                    ['view-order', 'order_id' => $val, 'id' => $this->id]));
                                if (Yii::$app->request->get('order_id') == $val || (!Yii::$app->request->get('order_id') || Yii::$app->request->get('order_id') == $order->id)) {
                                    $values[] = "#$val";
                                } else {
                                    $values[] = Html::a('#' . $val, $url);
                                }
                            }
                            $values = implode(", ", $values);

                            $messages[] = Yii::t('common',
                                'Данный трек номер используется для следующих заказов: {values}',
                                ['values' => $values]);
                        }
                        break;
                    }
                    case self::ERROR_ORDER_FIND_IN_ANOTHER_REPORT: {
                        $links = [];
                        foreach ($error->decodedValues as $reportId) {
                            $links[] = Html::a('#' . $reportId, Url::toRoute([
                                'view',
                                'id' => $reportId,
                                'FieldNumberFilter' => [
                                    'entity' => ['order_id'],
                                    'number' => $this->order_id,
                                ],
                                'record_status' => self::STATUS_STATUS_UPDATED,
                            ]));
                        }
                        $messages[] = $links ? Yii::t('common', 'Заказ найден в других отчетах: {values}', ['values' => implode(', ', $links)]) : Yii::t('common', 'Заказ найден в другом отчете');
                        break;
                    }
                    default:
                        if (isset($errorLabels[$error->error_id])) {
                            $messages[] = $errorLabels[$error->error_id];
                        } else {
                            $messages[] = Yii::t('common', 'Неизвестная ошибка');
                        }
                }
            }
        }

        $this->_errorMessages = $messages;
        return $this->_errorMessages;
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        $namedStatuses = $this->report->getStatusesNamed();
        if (isset($namedStatuses[$this->status])) {
            return $namedStatuses[$this->status];
        }
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusPair()
    {
        if (!empty($this->report) && !empty($this->report->statuses) && isset($this->report->statuses[$this->status])) {
            return $this->report->statuses[$this->status];
        }
        return '';
    }

    /**
     * @param $errorTypes
     */
    public function removeError($errorTypes)
    {
        if (!is_array($errorTypes)) {
            $errorTypes = [$errorTypes];
        }

        DeliveryReportRecordError::deleteAll([
            'delivery_report_record_id' => $this->id,
            'error_id' => $errorTypes
        ]);
        unset($this->recordErrors);
    }

    /**
     * @param string $type
     * @param array $values
     */
    public function addItemToErrors($type, $values = [])
    {
        $errors = $this->recordErrors;

        if (isset($errors[$type])) {
            $errors[$type]->decodedValues = $values;
            $errors[$type]->save();
        } else {
            $error = new DeliveryReportRecordError([
                'delivery_report_record_id' => $this->id,
            ]);
            $error->decodedValues = $values;
            $error->save();
            unset($this->recordErrors);
        }
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $errors = $this->recordErrors;
        $records = [];
        if (is_array($errors)) {
            foreach ($errors as $error) {
                if ($error->error_id == self::ERROR_DUPLICATE_ORDERID || $error->error_id == self::ERROR_DUPLICATE_TRACK) {
                    $values = $error->decodedValues;
                    foreach ($values as $val) {
                        $records[] = $val;
                    }
                }
            }
        }

        $this->record_status = DeliveryReportRecord::STATUS_DELETED;
        $this->save(true, ['record_status']);

        if (!empty($records)) {
            $report = new ReportScanner($this->delivery_report_id);
            foreach ($records as $record_id) {
                $record = self::findOne($record_id);
                $report->checkRecord($record);
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isFindByDuplicateOrder()
    {
        return $this->findByDuplicateOrder;
    }


    /**
     * @param $type
     * @return array
     */
    public static function getColumnNames($type)
    {
        switch ($type) {
            case DeliveryReport::TYPE_TRANSITIONAL: {
                return self::getTransitionalColumnNames();
            }
            case DeliveryReport::TYPE_FINANCIAL: {
                return self::getFinancialColumnNames();
            }
            default:
                return [
                    'order id' => self::COLUMN_ORDER_ID,
                    'tracking number' => self::COLUMN_TRACKING,
                    'status' => self::COLUMN_STATUS,
                    'customer full name' => self::COLUMN_CUSTOMER_FULL_NAME,
                    'phone' => self::COLUMN_CUSTOMER_PHONE,
                    'address' => self::COLUMN_CUSTOMER_ADDRESS,
                    'post code' => self::COLUMN_CUSTOMER_ZIP,
                    'city' => self::COLUMN_CUSTOMER_CITY,
                    'region' => self::COLUMN_CUSTOMER_REGION,
                    'district' => self::COLUMN_CUSTOMER_DISTRICT,
                    'msg' => self::COLUMN_COMMENT,
                    'products' => self::COLUMN_PRODUCTS,
                    'amount' => self::COLUMN_AMOUNT,
                    'price' => self::COLUMN_PRICE,
                    'date created' => self::COLUMN_DATE_CREATED,
                    'date approve' => self::COLUMN_DATE_APPROVE,
                    'date return' => self::COLUMN_DATE_RETURN,
                    'date payment' => self::COLUMN_DATE_PAYMENT,
                    'price: storage' => self::COLUMN_PRICE_STORAGE,
                    'price: fulfilment' => self::COLUMN_PRICE_FULFILMENT,
                    'price: packing' => self::COLUMN_PRICE_PACKING,
                    'price: package' => self::COLUMN_PRICE_PACKAGE,
                    'price: address correction' => self::COLUMN_PRICE_ADDRESS_CORRECTION,
                    'price: delivery' => self::COLUMN_PRICE_DELIVERY,
                    'price: redelivery' => self::COLUMN_PRICE_REDELIVERY,
                    'price: delivery back to shipper' => self::COLUMN_PRICE_DELIVERY_BACK,
                    'price: return to warehouse' => self::COLUMN_PRICE_DELIVERY_RETURN,
                    'price: cod service' => self::COLUMN_PRICE_COD_SERVICE,
                    'price: vat' => self::COLUMN_PRICE_VAT,
                    'price: cod' => self::COLUMN_PRICE_COD,
                    'payment reference number' => self::COLUMN_PAYMENT_REFERENCE_NUMBER,
                    'date of delivery' => self::COLUMN_DELIVERY_TIME_FROM,
                    'call center id' => self::COLUMN_CALL_CENTER_ID,
                ];
        }
    }

    /**
     * @param $type
     * @return array
     */
    public static function getNonRequiredColumnNames($type)
    {
        $columns = [
            'products' => self::COLUMN_PRODUCTS,
            'amount' => self::COLUMN_AMOUNT,
            'price' => self::COLUMN_PRICE,
            'date of delivery' => self::COLUMN_DELIVERY_TIME_FROM,
            'post code' => self::COLUMN_CUSTOMER_ZIP,
            'city' => self::COLUMN_CUSTOMER_CITY,
            'region' => self::COLUMN_CUSTOMER_REGION,
            'district' => self::COLUMN_CUSTOMER_DISTRICT,
            'msg' => self::COLUMN_COMMENT,
            'call center id' => self::COLUMN_CALL_CENTER_ID,
        ];
        switch ($type) {
            case DeliveryReport::TYPE_FINANCIAL:
                $columns = array_merge($columns, self::getNonRequiredFinancialColumnNames());
                break;
            case DeliveryReport::TYPE_TRANSITIONAL:
                $columns = array_merge($columns, self::getNonRequiredTransitionalColumnNames());
                break;
        }
        return $columns;
    }

    /**
     * @return array
     */
    public static function getNonRequiredFinancialColumnNames()
    {
        return [
            'payment reference number' => self::COLUMN_PAYMENT_REFERENCE_NUMBER,
            'price: storage' => self::COLUMN_PRICE_STORAGE,
            'price: fulfilment' => self::COLUMN_PRICE_FULFILMENT,
            'price: packing' => self::COLUMN_PRICE_PACKING,
            'price: package' => self::COLUMN_PRICE_PACKAGE,
            'price: address correction' => self::COLUMN_PRICE_ADDRESS_CORRECTION,
            'price: delivery' => self::COLUMN_PRICE_DELIVERY,
            'price: redelivery' => self::COLUMN_PRICE_REDELIVERY,
            'price: delivery back to shipper' => self::COLUMN_PRICE_DELIVERY_BACK,
            'price: return to warehouse' => self::COLUMN_PRICE_DELIVERY_RETURN,
            'price: cod service' => self::COLUMN_PRICE_COD_SERVICE,
            'price: vat' => self::COLUMN_PRICE_VAT,
        ];
    }

    /**
     * @return array
     */
    public static function getNonRequiredTransitionalColumnNames()
    {
        return [

        ];
    }

    /**
     * @return array
     */
    public static function getFinancialColumnNames()
    {
        return array_merge(self::getTransitionalColumnNames(), [
            'price: cod' => self::COLUMN_PRICE_COD
        ]);
    }

    /**
     * @return array
     */
    public static function getTransitionalColumnNames()
    {
        return [
            'order id' => self::COLUMN_ORDER_ID,
            'tracking number' => self::COLUMN_TRACKING,
            'status' => self::COLUMN_STATUS,
            'customer full name' => self::COLUMN_CUSTOMER_FULL_NAME,
            'phone' => self::COLUMN_CUSTOMER_PHONE,
            'address' => self::COLUMN_CUSTOMER_ADDRESS,
            'date created' => self::COLUMN_DATE_CREATED,
            'date approve' => self::COLUMN_DATE_APPROVE,
            'date return' => self::COLUMN_DATE_RETURN,
            'date payment' => self::COLUMN_DATE_PAYMENT,
        ];
    }

    /**
     * @return array
     */
    public static function getFinancialColumns()
    {
        return [
            self::COLUMN_PRICE_STORAGE => 'price_storage',
            self::COLUMN_PRICE_PACKING => 'price_packing',
            self::COLUMN_PRICE_PACKAGE => 'price_package',
            self::COLUMN_PRICE_ADDRESS_CORRECTION => 'price_address_correction',
            self::COLUMN_PRICE_DELIVERY => 'price_delivery',
            self::COLUMN_PRICE_REDELIVERY => 'price_redelivery',
            self::COLUMN_PRICE_DELIVERY_BACK => 'price_delivery_back',
            self::COLUMN_PRICE_DELIVERY_RETURN => 'price_delivery_return',
            self::COLUMN_PRICE_COD_SERVICE => 'price_cod_service',
            self::COLUMN_PRICE_COD => 'price_cod',
            self::COLUMN_PAYMENT_REFERENCE_NUMBER => 'payment',
            self::COLUMN_PRICE_FULFILMENT => 'price_fulfilment',
            self::COLUMN_PRICE_VAT => 'price_vat',
        ];
    }

    /**
     * @return array
     */
    public static function getPriceColumn()
    {
        return [
            self::COLUMN_PRICE_STORAGE => 'price_storage',
            self::COLUMN_PRICE_PACKING => 'price_packing',
            self::COLUMN_PRICE_PACKAGE => 'price_package',
            self::COLUMN_PRICE_ADDRESS_CORRECTION => 'price_address_correction',
            self::COLUMN_PRICE_DELIVERY => 'price_delivery',
            self::COLUMN_PRICE_REDELIVERY => 'price_redelivery',
            self::COLUMN_PRICE_DELIVERY_BACK => 'price_delivery_back',
            self::COLUMN_PRICE_DELIVERY_RETURN => 'price_delivery_return',
            self::COLUMN_PRICE_COD_SERVICE => 'price_cod_service',
            self::COLUMN_PRICE_COD => 'price_cod',
            self::COLUMN_PRICE_FULFILMENT => 'price_fulfilment',
            self::COLUMN_PRICE_VAT => 'price_vat',
        ];
    }

    /**
     * @return array
     */
    public static function getDeliveryRequestColumns()
    {
        return [
            self::COLUMN_TRACKING => 'tracking',
            self::COLUMN_DATE_CREATED => 'accepted_at',
            self::COLUMN_DATE_APPROVE => 'approved_at',
            self::COLUMN_DATE_RETURN => 'returned_at',
            self::COLUMN_DATE_PAYMENT => 'paid_at',
        ];
    }

    /**
     * @return array
     */
    public static function getOrderColumns()
    {
        return [
            self::COLUMN_ORDER_ID => 'id',
            self::COLUMN_STATUS => 'status_id',
            self::COLUMN_CUSTOMER_FULL_NAME => 'customer_full_name',
            self::COLUMN_CUSTOMER_PHONE => 'customer_phone',
            self::COLUMN_CUSTOMER_ADDRESS => 'customer_address',
            self::COLUMN_CUSTOMER_ZIP => 'customer_zip',
            self::COLUMN_CUSTOMER_CITY => 'customer_city',
            self::COLUMN_CUSTOMER_REGION => 'customer_province',
            self::COLUMN_CUSTOMER_DISTRICT => 'customer_street',
            self::COLUMN_COMMENT => 'comment',
            self::COLUMN_PRICE => 'price_total',
            self::COLUMN_PRICE_COD => 'price_total',
            self::COLUMN_DELIVERY_TIME_FROM => 'delivery_time_from',
        ];
    }

    /**
     * @return array
     */
    public static function errorLabels()
    {
        return [
            self::ERROR_NOT_FOUND => Yii::t('common', 'Ошибки отсутствуют'),
            self::ERROR_DUPLICATE_TRACK => Yii::t('common', 'Дублируются треки'),
            self::ERROR_DUPLICATE_ORDERID => Yii::t('common', 'Дублируются Order ID'),
            self::ERROR_INCORRECT_ORDERID => Yii::t('common', 'Некорректный Order ID'),
            self::ERROR_INCORRECT_TRACK => Yii::t('common', 'Некорректный трек'),
            self::ERROR_NOT_EQUAL_TRACK => Yii::t('common', 'Не совпадает трек'),
            self::ERROR_ORDER_NOT_EXIST => Yii::t('common', 'Невозможно найти соответствующий заказ'),
            self::ERROR_NOT_EQUAL_AMOUNT => Yii::t('common', 'Не совпадает количество товара'),
            self::ERROR_NOT_EQUAL_PRICE => Yii::t('common', 'Не совпадает цена'),
            self::ERROR_NOT_EQUAL_ORDER_DATA => Yii::t('common', 'Не совпадают данные в заказе'),
            self::ERROR_NOT_EQUAL_ORDER_NAME => Yii::t('common', 'Не совпадает имя покупателя'),
            self::ERROR_NOT_EQUAL_ORDER_PHONE => Yii::t('common', 'Не совпадает номер телефона'),
            self::ERROR_NOT_EQUAL_ORDER_ADDRESS => Yii::t('common', 'Не совпадает адрес'),
            self::ERROR_NOT_EQUAL_STATUS => Yii::t('common', 'Некорректный статус'),
            self::ERROR_INCORRECT_STATUS => Yii::t('common', 'Не задан статус'),
            self::ERROR_FIND_ORDERID_BY_TRACK => Yii::t('common', 'Заказ найден с помощью трека'),
            self::ERROR_ORDER_FOUND_BY_FINANCE_TRACK => Yii::t('common', 'Заказ найден с помощью доп.трека'),
            self::ERROR_FROM_ANOTHER_COUNTRY => Yii::t('common', 'Заказ найден в другой стране'),
            self::ERROR_FROM_ANOTHER_DELIVERY => Yii::t('common', 'У заказа другая служба доставки'),
            self::ERROR_NOT_EQUAL_COD => Yii::t('common', 'Не совпадает COD'),
            self::ERROR_HIGH_COD => Yii::t('common', 'В отчете COD больше'),
            self::ERROR_LESS_COD => Yii::t('common', 'В отчете COD меньше'),
            self::ERROR_INCORRECT_DATE_CREATED => Yii::t('common', 'Некорректная дата создания заказа'),
            self::ERROR_INCORRECT_DATE_APPROVE => Yii::t('common', 'Некорректная дата подтверждения заказа'),
            self::ERROR_INCORRECT_DATE_PAYMENT => Yii::t('common', 'Некорректная дата оплаты заказа'),
            self::ERROR_INCORRECT_DATE_RETURN => Yii::t('common', 'Некорректная дата возврата заказа'),
            self::ERROR_NOT_EQUAL_PAYMENT_NUMBER => Yii::t('common', 'Не совпадает номер платежа'),
            self::ERROR_ORDER_FIND_BY_NAME_ADDRESS_PHONE => Yii::t('common', 'Заказ найден с помощью имени, телефона или адреса'),
            self::ERROR_INCORRECT_DELIVERY_TIME_FROM => Yii::t('common', 'Некорректная дата доставки'),
            self::ERROR_FIND_ORDER_ID_BY_DUPLICATE => Yii::t('common', 'Используется дубль заказа'),
            self::ERROR_NOT_EQUAL_COSTS => Yii::t('common', 'Не совпадают расходы на доставку'),
            self::ERROR_INCORRECT_PRODUCTS_FORMAT => Yii::t('common', 'Продукты заданы в некорректном формате'),
            self::ERROR_PRODUCT_NOT_FOUND => Yii::t('common', 'Не удалось определить продукт'),
            self::ERROR_ORDER_FIND_IN_ANOTHER_REPORT => Yii::t('common', 'Заказ найден в другом отчете'),
            self::ERROR_ORDER_IN_FINAL_STATUS => Yii::t('common', 'Заказ находится в конечном статусе'),
            self::ERROR_NOT_EQUAL_COSTS_STORAGE => Yii::t('common', 'Не совпадают расходы за хранение'),
            self::ERROR_NOT_EQUAL_COSTS_FULFILMENT => Yii::t('common', 'Не совпадают расходы за обслуживание'),
            self::ERROR_NOT_EQUAL_COSTS_PACKING => Yii::t('common', 'Не совпадают расходы за упаковывание'),
            self::ERROR_NOT_EQUAL_COSTS_PACKAGE => Yii::t('common', 'Не совпадают расходы за упаковку'),
            self::ERROR_NOT_EQUAL_COSTS_DELIVERY => Yii::t('common', 'Не совпадают расходы за доставку'),
            self::ERROR_NOT_EQUAL_COSTS_REDELIVERY => Yii::t('common', 'Не совпадают расходы за повторную доставку'),
            self::ERROR_NOT_EQUAL_COSTS_DELIVERY_BACK => Yii::t('common', 'Не совпадают расходы за обратную доставку'),
            self::ERROR_NOT_EQUAL_COSTS_DELIVERY_RETURN => Yii::t('common', 'Не совпадают расходы за возврат'),
            self::ERROR_NOT_EQUAL_COSTS_ADDRESS_CORRECTION => Yii::t('common', 'Не совпадают расходы за коррекцию адреса'),
            self::ERROR_NOT_EQUAL_COSTS_COD_SERVICE => Yii::t('common', 'Не совпадают расходы за наложенный платеж'),
            self::ERROR_NOT_EQUAL_COSTS_VAT => Yii::t('common', 'Не совпадают НДС'),
        ];
    }

    /**
     * @return array
     */
    public static function costsErrors()
    {
        return [
            DeliveryReportRecord::COLUMN_PRICE_STORAGE => static::ERROR_NOT_EQUAL_COSTS_STORAGE,
            DeliveryReportRecord::COLUMN_PRICE_PACKING => static::ERROR_NOT_EQUAL_COSTS_PACKING,
            DeliveryReportRecord::COLUMN_PRICE_PACKAGE => static::ERROR_NOT_EQUAL_COSTS_PACKAGE,
            DeliveryReportRecord::COLUMN_PRICE_ADDRESS_CORRECTION => static::ERROR_NOT_EQUAL_COSTS_ADDRESS_CORRECTION,
            DeliveryReportRecord::COLUMN_PRICE_DELIVERY => static::ERROR_NOT_EQUAL_COSTS_DELIVERY,
            DeliveryReportRecord::COLUMN_PRICE_REDELIVERY => static::ERROR_NOT_EQUAL_COSTS_REDELIVERY,
            DeliveryReportRecord::COLUMN_PRICE_DELIVERY_BACK => static::ERROR_NOT_EQUAL_COSTS_DELIVERY_BACK,
            DeliveryReportRecord::COLUMN_PRICE_DELIVERY_RETURN => static::ERROR_NOT_EQUAL_COSTS_DELIVERY_RETURN,
            DeliveryReportRecord::COLUMN_PRICE_COD_SERVICE => static::ERROR_NOT_EQUAL_COSTS_COD_SERVICE,
            DeliveryReportRecord::COLUMN_PRICE_FULFILMENT => static::ERROR_NOT_EQUAL_COSTS_FULFILMENT,
            DeliveryReportRecord::COLUMN_PRICE_VAT => static::ERROR_NOT_EQUAL_COSTS_VAT,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceFact()
    {
        return $this->hasOne(OrderFinanceFact::className(), ['delivery_report_record_id' => 'id']);
    }

    /**
     * https://2wtrade-tasks.atlassian.net/browse/ERP-633
     *
     * @param PaymentOrder|null $payment
     * @return OrderFinanceFact|null
     * @throws \Exception
     */
    public function createFinanceFact(?PaymentOrder $payment)
    {
        //если была претензия по ордеру - то в order_finance_fact запись будет по этой записи для этого ордера
        //если записей в order_finance_fact нет по данному ордеру - то и нет по данной записи - необходимо создать запись
        if ($this->report->type == DeliveryReport::TYPE_FINANCIAL && !OrderFinanceFact::find()->where([
                'order_id' => $this->relatedOrder->id,
                'delivery_report_id' => $this->report->id,
                'delivery_report_record_id' => $this->id,
                'pretension' => NULL
            ])->exists()) {
            $recordAttributes = $this->getAttributes();

            $attributes = $recordAttributes;
            $attributes['order_id'] = $this->relatedOrder->id;
            $attributes['created_at'] = time();
            $attributes['updated_at'] = time();
            $attributes['delivery_report_id'] = $this->report->id;
            $attributes['delivery_report_record_id'] = $this->id;

            $paymentDate = null;
            if ($payment) {
                $attributes['payment_id'] = $payment->id;
                $paymentDate = $payment->paid_at;
            }

            if ($this->report->currencies) {
                $reportAttributes = $this->report->currencies->getCurrencyIDs();
            } else {
                $reportAttributes = [];
            }
            foreach (OrderFinanceFact::currencyRateRelationMap() as $currencyColumn => $rateColumn) {
                $attributes[$currencyColumn] = (int)($reportAttributes[$currencyColumn] ?? $this->report->country->currency_id);
                $attributes[$rateColumn] = $this->report->getRate($attributes[$currencyColumn], Currency::getUSD()->id, $paymentDate, $payment ?? null);
            }

            $factModel = new OrderFinanceFact();
            $factAttributes = $factModel->attributes();
            $contract = $this->report->delivery->getActiveContractByDelivery($this->report->delivery->id, $this->report->period_from, $this->report->period_to);

            foreach ($attributes as $attr => $value) {
                if (!in_array($attr, $factAttributes) || ($contract && $contract->mutual_settlement != DeliveryContract::MUTUAL_SETTLEMENT_ON && OrderFinanceFact::getFinanceTypeOfField($attr) == OrderFinanceFact::FINANCE_TYPE_COST)) {
                    unset($attributes[$attr]);
                } elseif (OrderFinanceFact::getFinanceTypeOfField($attr) !== false) {
                    $attributes[$attr] = doubleval($attributes[$attr]);
                }
            }

            if (!($factModel->load($attributes, '') && $factModel->save())) {
                throw new \Exception(Yii::t('common', 'Не удалось создать факт для заказа #{order_id}: {error}', [
                    'order_id' => $factModel->order_id,
                    'error' => $factModel->getFirstErrorAsString()
                ]));
            }
            return $factModel;
        }

        return null;
    }
}
