<?php
namespace app\modules\deliveryreport\models;

use Yii;
use app\components\db\ActiveRecord;
use app\models\Currency;

/**
 * This is the model class for table "payment_currency_rate".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $currency_id_from
 * @property integer $currency_id_to
 * @property string $rate
 * @property integer $created_at
 *
 * @property Currency $currencyFrom
 * @property Currency $currencyTo
 * @property PaymentOrder $paymentOrder
 */
class PaymentCurrencyRate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_currency_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'currency_id_from', 'currency_id_to', 'rate'], 'required'],
            [['payment_id', 'currency_id_from', 'currency_id_to', 'created_at'], 'integer'],
            ['currency_id_from', 'compare', 'compareAttribute' => 'currency_id_to', 'operator' => '!='],
            [['rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => Yii::t('common', 'Ид платежа'),
            'currency_id_from' => Yii::t('common', 'Из валюты'),
            'currency_id_to' => Yii::t('common', 'В валюту'),
            'rate' => Yii::t('common', 'Курс'),
            'created_at' => Yii::t('common', 'Дата добавления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyFrom()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyTo()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_id']);
    }
}