<?php

namespace app\modules\deliveryreport\models;

use Yii;
use app\components\db\ActiveRecord;

/**
 * This is the model class for table "delivery_report_currency".
 *
 * @property integer $id
 * @property integer $delivery_report_id
 * @property integer $price_cod_currency_id
 * @property integer $price_storage_currency_id
 * @property integer $price_fulfilment_currency_id
 * @property integer $price_packing_currency_id
 * @property integer $price_package_currency_id
 * @property integer $price_delivery_currency_id
 * @property integer $price_redelivery_currency_id
 * @property integer $price_delivery_return_currency_id
 * @property integer $price_delivery_back_currency_id
 * @property integer $price_cod_service_currency_id
 * @property integer $price_vat_currency_id
 * @property integer $price_address_correction_currency_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryReport $deliveryReport
 */
class DeliveryReportCurrency extends ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%delivery_report_currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_id'], 'required'],
            [
                ['delivery_report_id', 'price_cod_currency_id', 'price_storage_currency_id', 'price_fulfilment_currency_id', 'price_packing_currency_id', 'price_delivery_currency_id', 'price_redelivery_currency_id', 'price_delivery_return_currency_id', 'price_delivery_back_currency_id', 'price_cod_service_currency_id', 'price_vat_currency_id', 'price_address_correction_currency_id', 'price_package_currency_id', 'created_at', 'updated_at'],
                'integer'
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_report_id' => Yii::t('common', 'Ид отчета'),
            'price_cod_currency_id' => Yii::t('common', 'Валюта COD'),
            'price_storage_currency_id' => Yii::t('common', 'Валюта "за хранение"'),
            'price_fulfilment_currency_id' => Yii::t('common', 'Валюта "за обслуживание"'),
            'price_packing_currency_id' => Yii::t('common', 'Валюта "за упаковывание"'),
            'price_package_currency_id' => Yii::t('common', 'Валюта "за упаковку"'),
            'price_delivery_currency_id' => Yii::t('common', 'Валюта "за доставку"'),
            'price_redelivery_currency_id' => Yii::t('common', 'Валюта "за передоставку"'),
            'price_delivery_return_currency_id' => Yii::t('common', 'Валюта "за возврат"'),
            'price_delivery_back_currency_id' => Yii::t('common', 'Валюта "за обратную доставку"'),
            'price_cod_service_currency_id' => Yii::t('common', 'Валюта "за наложенный платеж"'),
            'price_vat_currency_id' => Yii::t('common', 'Валюта "за НДС"'),
            'price_address_correction_currency_id' => Yii::t('common', 'Валюта "за коррекцию адреса доставки"'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @return array
     */
    public static function getFinancialColumns()
    {
        return [
            'price_cod_currency_id',
            'price_storage_currency_id',
            'price_fulfilment_currency_id',
            'price_packing_currency_id',
            'price_package_currency_id',
            'price_delivery_currency_id',
            'price_redelivery_currency_id',
            'price_delivery_return_currency_id',
            'price_delivery_back_currency_id',
            'price_cod_service_currency_id',
            'price_vat_currency_id',
            'price_address_correction_currency_id'
        ];
    }

    /**
     * @return array
     */
    public function getCurrencyIDs()
    {
        $data = [];
        foreach (self::getFinancialColumns() as $column) {
            $data[$column] = $this->$column ?? null;
        }
        return $data;
    }
}