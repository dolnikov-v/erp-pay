<?php

namespace app\modules\deliveryreport\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "delivery_report_costs".
 *
 * @property integer $id
 * @property integer $delivery_report_id
 * @property double $sum
 * @property double $balance
 * @property integer $from
 * @property integer $to
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryReport $deliveryReport
 */
class DeliveryReportCosts extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report_costs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_id', 'from', 'to', 'sum'], 'required'],
            [['from', 'to'], 'filter', 'filter' => function ($value) {
                return $value ? Yii::$app->formatter->asTimestamp($value) : null;
            }],
            [['delivery_report_id', 'from', 'to', 'created_at', 'updated_at', 'id'], 'integer'],
            [['to'], 'compare', 'compareAttribute' => 'from', 'operator' => '>', 'type' => 'number'],
            [['sum'], 'number'],
            [['sum'], 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
            [['balance'], 'number', 'min' => 0],
            [['description'], 'string', 'max' => 255],
            [['delivery_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryReport::className(), 'targetAttribute' => ['delivery_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ИД'),
            'delivery_report_id' => Yii::t('common', 'Отчет'),
            'sum' => Yii::t('common', 'Сумма издержки'),
            'balance' => Yii::t('common', 'Остаток от суммы для списания'),
            'from' => Yii::t('common', 'Начало периода издержки'),
            'to' => Yii::t('common', 'Конец периода издержки'),
            'description' => Yii::t('common', 'Описание'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->balance = $this->sum;
        } elseif ($this->isAttributeChanged('sum')) {
            $this->balance += $this->getAttribute('sum') - $this->getOldAttribute('sum');
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function canBeDelete(): bool
    {
        return $this->balance === $this->sum;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
//        TODO: переделать на сценарии
        if (!$this->canBeDelete()) {
            $this->addError('balance', Yii::t('common', 'Баланс должен быть равен сумме.'));
            return false;
        }
        return parent::beforeDelete();
    }
}
