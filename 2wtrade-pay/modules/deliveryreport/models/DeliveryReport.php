<?php

namespace app\modules\deliveryreport\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRateHistory;
use app\models\User;
use app\modules\delivery\models\Delivery;

use app\modules\delivery\models\DeliveryContract;
use app\modules\order\models\OrderFinance;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;

use app\modules\order\models\OrderStatus;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryReport
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $delivery_id
 * @property string $payment_id
 * @property double $sum_total
 * @property double $total_costs
 * @property double $rate_costs
 * @property string $file
 * @property string $status
 * @property integer $country_id
 * @property string $error_message
 * @property string $type
 * @property string $status_pair
 * @property string $unbuyout_reason_pair
 * @property integer $priority
 * @property integer $period_from
 * @property integer $period_to
 * @property string $date_format
 * @property integer $currency_id
 * @property string $currency_date
 *
 * @property array $statuses
 * @property array $unBuyoutReasons
 * @property string $statusName
 *
 * @property Country $country
 * @property User $user
 * @property Delivery $delivery
 * @property DeliveryReportRecord[] $records
 * @property string $fileRoute
 * @property array $statusesNamed
 * @property \yii\db\ActiveQuery $approvedRecords
 * @property string|int $approvedRecordCount
 * @property \yii\db\ActiveQuery $statusUpdatedRecords
 * @property string|int $statusUpdatedRecordCount
 * @property array $nameColumns
 * @property string|int $activeRecordCount
 * @property \yii\db\ActiveQuery $exportRecords
 * @property mixed $typeName
 * @property string|int $recordCount
 * @property \yii\db\ActiveQuery $activeRecords
 * @property mixed $errorList
 * @property string|int $exportRecordCount
 * @property PaymentOrder[] $payments
 * @property DeliveryReportRate[] $deliveryReportRates
 * @property DeliveryReportCurrency $currencies
 * @property DeliveryReportCurrencyRate[] $currencyRates
 *
 * @property float $balancePayments
 * @property DeliveryReportCosts[] $costs
 */
class DeliveryReport extends ActiveRecordLogUpdateTime
{
    const STATUS_QUEUE = 'queue';
    const STATUS_PARSING = 'parsing';
    const STATUS_COMPLETE = 'complete';
    const STATUS_ERROR = 'error';
    const STATUS_CHECKING = 'checking';
    const STATUS_IN_WORK = 'in_work';
    const STATUS_CHECKING_QUEUE = 'queue_checking';
    const STATUS_CLOSED = 'closed';
    const STATUS_DELETE = 'delete';

    const TYPE_FINANCIAL = 'financial';
    const TYPE_TRANSITIONAL = 'transitional';

    const PATH_UPLOAD = 'delivery-reports';

    /**
     * @var array
     */
    private $rates = [];

    /**
     * @var null|array
     */
    protected $recordCounts = null;

    /**
     * @var array | null
     */
    protected $statusNamed = null;


    //остаток с платёжки
    public $balance = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'file'], 'required'],
            [['user_id', 'country_id', 'delivery_id', 'priority', 'created_at', 'updated_at', 'period_from', 'period_to'], 'integer'],
            [['sum_total', 'total_costs', 'rate_costs'], 'double'],
            [['file', 'status', 'error_message', 'type', 'status_pair', 'unbuyout_reason_pair'], 'string'],
            [['name', 'payment_id'], 'string', 'max' => 255],
            [['date_format'], 'string', 'max' => 20],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            ['status', 'default', 'value' => self::STATUS_QUEUE],
            ['type', 'default', 'value' => self::TYPE_FINANCIAL],
            ['user_id', 'default', 'value' => (isset(Yii::$app->user) ? Yii::$app->user->id : 1)],
            ['country_id', 'default', 'value' => (isset(Yii::$app->user) ? Yii::$app->user->country->id : 1)],
            ['priority', 'default', 'value' => 1],
            [['created_at', 'updated_at', 'currency_id', 'currency_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название'),
            'user_id' => Yii::t('common', 'Добавил'),
            'country_id' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'file' => Yii::t('common', 'Название файла'),
            'status' => Yii::t('common', 'Статус'),
            'error_message' => Yii::t('common', 'Ошибка'),
            'type' => Yii::t('common', 'Тип'),
            'recordCount' => Yii::t('common', 'Всего записей'),
            'statusUpdatedRecordCount' => Yii::t('common', 'Обновлено статусов'),
            'approvedRecordCount' => Yii::t('common', 'Подтверждено записей'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'activeRecordCount' => Yii::t('common', 'Неподтвержденных записей'),
            'exportRecordCount' => Yii::t('common', 'В очереди на экспорт'),
            'priority' => Yii::t('common', 'Приоритет'),
            'payment_id' => Yii::t('common', 'Номер платежа'),
            'sum_total' => Yii::t('common', 'Баланс'),
            'total_costs' => Yii::t('common', 'Общая сумма издержек'),
            'rate_costs' => Yii::t('common', 'Курс издержек'),
            'period_from' => Yii::t('common', 'Начало периода'),
            'period_to' => Yii::t('common', 'Конец периода')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecords()
    {
        return $this->hasMany(DeliveryReportRecord::className(), ['delivery_report_id' => 'id'])->where([
            '!=',
            'record_status',
            DeliveryReportRecord::STATUS_DELETED
        ]);
    }

    /**
     * @return int|string
     */
    public function getRecordCount()
    {
        $recordCounts = $this->getRecordCounts();
        return is_array($recordCounts) ? array_sum($recordCounts) : 0;
    }

    /**
     * @return array
     */
    public function getRecordCounts()
    {
        if (is_null($this->recordCounts)) {
            $query = DeliveryReportRecord::find()->where(['delivery_report_id' => $this->id]);
            $query->andWhere(['!=', 'record_status', DeliveryReportRecord::STATUS_DELETED]);
            $query->select(['count' => 'COUNT(*)', 'status' => 'record_status']);
            $query->groupBy(['record_status']);
            $this->recordCounts = $query->asArray()->all();
            $this->recordCounts = ArrayHelper::map($this->recordCounts, 'status', 'count');
        }
        return $this->recordCounts;
    }

    /**
     * @return mixed
     */
    public function getErrorList()
    {
        $errors = json_decode($this->error_message, true);
        return $errors;
    }

    /**
     * @param $errors
     */
    public function setErrorList($errors)
    {
        $this->error_message = json_encode($errors);
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        $statuses = json_decode($this->status_pair, true);

        return $statuses;
    }

    /**
     * @return array
     */
    public function getUnBuyoutReasons()
    {
        $statuses = json_decode($this->unbuyout_reason_pair, true);

        return $statuses;
    }

    /**
     * @param $value
     */
    public function setStatuses($value)
    {
        $this->status_pair = json_encode($value);
    }

    /**
     * @param $value
     */
    public function setUnBuyoutReasons($value)
    {
        $this->unbuyout_reason_pair = json_encode($value);
    }

    /**
     * @return array
     */
    public function getStatusesNamed()
    {
        if (!is_null($this->statusNamed)) {
            return $this->statusNamed;
        }

        $statuses = $this->getStatuses();

        $buffer = [];

        if (empty($statuses)) {
            return $buffer;
        }

        $orderStatuses = OrderStatus::find()->where(['id' => $statuses])->all();
        $orderStatuses = ArrayHelper::map($orderStatuses, 'id', 'name');

        foreach ($statuses as $key => $val) {
            if (isset($orderStatuses[$val])) {
                $buffer[$key] = Yii::t('common', $orderStatuses[$val]);
            } else {
                $buffer[$key] = $key;
            }
        }

        $this->statusNamed = $buffer;

        return $buffer;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveRecords()
    {
        return $this->hasMany(DeliveryReportRecord::className(),
            ['delivery_report_id' => 'id'])->andWhere(['record_status' => DeliveryReportRecord::STATUS_ACTIVE]);
    }

    /**
     * @return int|string
     */
    public function getActiveRecordCount()
    {
        $recordCounts = $this->getRecordCounts();
        return isset($recordCounts[DeliveryReportRecord::STATUS_ACTIVE]) ? $recordCounts[DeliveryReportRecord::STATUS_ACTIVE] : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedRecords()
    {
        return $this->hasMany(DeliveryReportRecord::className(),
            ['delivery_report_id' => 'id'])->andWhere(['record_status' => DeliveryReportRecord::STATUS_APPROVE]);
    }

    /**
     * @return int|string
     */
    public function getApprovedRecordCount()
    {
        $recordCounts = $this->getRecordCounts();
        return isset($recordCounts[DeliveryReportRecord::STATUS_APPROVE]) ? $recordCounts[DeliveryReportRecord::STATUS_APPROVE] : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusUpdatedRecords()
    {
        return $this->hasMany(DeliveryReportRecord::className(),
            ['delivery_report_id' => 'id'])->andWhere(['record_status' => DeliveryReportRecord::STATUS_STATUS_UPDATED]);
    }

    /**
     * @return int|string
     */
    public function getStatusUpdatedRecordCount()
    {
        $recordCounts = $this->getRecordCounts();
        return isset($recordCounts[DeliveryReportRecord::STATUS_STATUS_UPDATED]) ? $recordCounts[DeliveryReportRecord::STATUS_STATUS_UPDATED] : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExportRecords()
    {
        return $this->hasMany(DeliveryReportRecord::className(),
            ['delivery_report_id' => 'id'])->andWhere(['record_status' => DeliveryReportRecord::STATUS_EXPORT]);
    }

    /**
     * @return int|string
     */
    public function getExportRecordCount()
    {
        $recordCounts = $this->getRecordCounts();
        return isset($recordCounts[DeliveryReportRecord::STATUS_EXPORT]) ? $recordCounts[DeliveryReportRecord::STATUS_EXPORT] : 0;
    }

    /**
     * @return array
     */
    public function getNameColumns()
    {
        return DeliveryReportRecord::getColumnNames($this->type);
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public function getFileRoute()
    {
        return self::getFileFullRoute($this->file);
    }

    /**
     * @param $filename
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function getFileFullRoute($filename)
    {
        $url = self::getUploadPath() . $filename;

        return $url;
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function getUploadPath()
    {
        $dir = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . self::PATH_UPLOAD . DIRECTORY_SEPARATOR;
        Utils::prepareDir($dir);
        return $dir;
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function getTmpPath()
    {
        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . self::PATH_UPLOAD;
        Utils::prepareDir($dir);
        return $dir;
    }

    /**
     *
     */
    public function deleteRecords()
    {
        DeliveryReportRecord::deleteAll(['delivery_report_id' => $this->id]);
    }

    /**
     * @return array
     */
    public static function getStatusNames()
    {
        return [
            self::STATUS_QUEUE => Yii::t('common', 'В очереди'),
            self::STATUS_PARSING => Yii::t('common', 'Идет чтение файла'),
            self::STATUS_CHECKING_QUEUE => Yii::t('common', 'В очереди на проверку'),
            self::STATUS_CHECKING => Yii::t('common', 'Идет проверка'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
            self::STATUS_IN_WORK => Yii::t('common', 'В работе'),
            self::STATUS_COMPLETE => Yii::t('common', 'Обработан'),
            self::STATUS_CLOSED => Yii::t('common', 'Закрыт'),
            self::STATUS_DELETE => Yii::t('common', 'На удаление'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        return static::getStatusNames()[$this->status];
    }

    /**
     * @return array
     */
    public static function getTypeNames()
    {
        $array = [
            self::TYPE_FINANCIAL => Yii::t('common', 'Финансовый'),
            self::TYPE_TRANSITIONAL => Yii::t('common', 'Промежуточный'),
        ];
        return $array;
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return self::getTypeNames()[$this->type];
    }

    /**
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        $path = self::getFileFullRoute($this->file);
        unlink($path);
        return parent::delete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(PaymentOrder::className(), ['id' => 'payment_id'])
            ->viaTable('payment_delivery_report', ['delivery_report_id' => 'id'])
            ->orderBy([PaymentOrder::tableName() . '.paid_at' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReportRates()
    {
        return $this->hasMany(DeliveryReportRate::className(), ['delivery_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyRates()
    {
        return $this->hasMany(DeliveryReportCurrencyRate::className(), ['delivery_report_id' => 'id'])->orderBy(['currency_id_from' => SORT_ASC, 'currency_id_to' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCosts()
    {
        return $this->hasMany(DeliveryReportCosts::className(), ['delivery_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasOne(DeliveryReportCurrency::className(), ['delivery_report_id' => 'id']);
    }

    /**
     * @param bool $asDataProvider
     * @param bool $onlyActive
     * @return array|ArrayDataProvider|PaymentOrder[]
     */
    public function getPaymentsWithStatus($asDataProvider = false, $onlyActive = false)
    {
        /**
         * @var PaymentOrder[] $payments
         */
        $payments = $this->getPayments()->all();
        $finances = OrderFinanceFact::find()->select(['payment_id'])->distinct(true)->where([
            'payment_id' => ArrayHelper::getColumn($payments, 'id'),
            'delivery_report_id' => $this->id
        ])->column();
        foreach ($payments as $key => $payment) {
            if (!in_array($payments[$key]->id, $finances) &&
                $this->getRate($payment->currency_id, $payment->delivery->country->currency_id, $payment->paid_at, $payment)&&
                round($this->sum_total + $this->balancePayments - $payment->balance / $this->getRate($payment->currency_id, $payment->delivery->country->currency_id, $payment->paid_at, $payment), 2) >= 0) {
                $payments[$key]->active = true;
            } else {
                $payments[$key]->active = false;
            }
        }
        if ($onlyActive) {
            $payments = array_filter($payments, function ($item) {
                return $item->active;
            });
        }
        if ($asDataProvider) {
            return new ArrayDataProvider([
                'allModels' => $payments,
                'pagination' => false
            ]);
        }
        return $payments;
    }


    /**
     * @param OrderFinanceFact $fact
     * @param string $cell
     * @param PaymentOrder $payment
     * @return float
     * @throws \Exception
     */
    public function convertCurrency($fact, $cell, $payment)
    {
        return $fact->{$cell}
            ? ($fact->order->country->currency_id != $fact->{$cell . '_currency_id'})
                ? CurrencyRateHistory::convertValueToCurrency($fact->order->country->currency_id, $payment->paid_at, $fact->{$cell})
                : $fact->{$cell}
            : 0;

    }

    /**
     * @return bool
     */
    public function setFalseStatusOrderPretension()
    {
        $statuses['buyout'] = [
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        ];
        $statuses['return'] = [
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_LOST
        ];

        // заказы в выкупе, с парой в невыкупе на которые претензию
        $ordersDoublesData1 = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id',   // это оригинальный заказ доставлен
                'pretension_order_id' => 'doubles1.id',      // на этот заказ претензию, если в фин отчете
            ])
            ->leftJoin(DeliveryReportRecord::tableName(), DeliveryReportRecord::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->leftJoin(['doubles1' => Order::tableName()], 'doubles1.id = ' . Order::tableName() . '.duplicate_order_id')
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->id])
            ->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED,
                ]
            ])
            ->andWhere(['is not', Order::tableName() . '.duplicate_order_id', null])
            ->andWhere([Order::tableName() . '.status_id' => $statuses['buyout']])
            ->andWhere(['doubles1.status_id' => $statuses['return']])
            ->groupBy(['order_id', 'pretension_order_id'])
            ->asArray()
            ->all();


        // заказы в выкупе, с парой в невыкупе на которые претензию
        $ordersDoublesData2 = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id', // это оригинальный заказ доставлен
                'pretension_order_id' => 'doubles2.id',    // на этот заказ претензию, если в фин отчете
            ])
            ->leftJoin(DeliveryReportRecord::tableName(), DeliveryReportRecord::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->leftJoin(['doubles2' => Order::tableName()], 'doubles2.duplicate_order_id = ' . Order::tableName() . '.id')
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->id])
            ->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED,
                ]
            ])
            ->andWhere([Order::tableName() . '.status_id' => $statuses['buyout']])
            ->andWhere(['doubles2.status_id' => $statuses['return']])
            ->groupBy(['pretension_order_id', 'order_id'])
            ->asArray()
            ->all();

        // пары для претензий
        $ordersBuyoutData = array_merge($ordersDoublesData1, $ordersDoublesData2);


        // заказы в невыкупе с парой выкупе
        $ordersDoublesData3 = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id',   // это оригинальный заказ возврат, на него претензию
                'buyout_order_id' => 'doubles1.id',      // этот заказ в выкупе, если в фин отчете
            ])
            ->leftJoin(DeliveryReportRecord::tableName(), DeliveryReportRecord::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->leftJoin(['doubles1' => Order::tableName()], 'doubles1.id = ' . Order::tableName() . '.duplicate_order_id')
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->id])
            ->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED,
                ]
            ])
            ->andWhere(['is not', Order::tableName() . '.duplicate_order_id', null])
            ->andWhere([Order::tableName() . '.status_id' => $statuses['return']])
            ->andWhere(['doubles1.status_id' => $statuses['buyout']])
            ->groupBy(['order_id', 'buyout_order_id'])
            ->asArray()
            ->all();

        $ordersDoublesData4 = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id', // это оригинальный заказ возврат, на него претензию
                'buyout_order_id' => 'doubles2.id',    // этот заказ в выкупе, если в фин отчете
            ])
            ->leftJoin(DeliveryReportRecord::tableName(), DeliveryReportRecord::tableName() . '.order_id = ' . Order::tableName() . '.id')
            ->leftJoin(['doubles2' => Order::tableName()], 'doubles2.duplicate_order_id = ' . Order::tableName() . '.id')
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->id])
            ->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED,
                ]
            ])
            ->andWhere([Order::tableName() . '.status_id' => $statuses['return']])
            ->andWhere(['doubles2.status_id' => $statuses['buyout']])
            ->groupBy(['buyout_order_id', 'order_id'])
            ->asArray()
            ->all();

        // пары для претензий
        $ordersReturnData = array_merge($ordersDoublesData3, $ordersDoublesData4);

        foreach ($ordersBuyoutData as $row) {
            $records = DeliveryReportRecord::find()
                ->joinWith('report')
                ->where(['order_id' => $row['pretension_order_id']])
                ->andWhere([DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL])
                ->all();
            if ($records) {
                foreach ($records as $record) {
                    /** @var $record DeliveryReportRecord */
                    if (!OrderFinancePretension::find()->where([
                        'type' => OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS,
                        'order_id' => $row['pretension_order_id'],
                        'delivery_report_record_id' => $record->id,
                    ])->exists()
                    ) {
                        $p = new OrderFinancePretension();
                        $p->type = OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS;
                        $p->order_id = $row['pretension_order_id'];
                        $p->delivery_report_id = $record->delivery_report_id;
                        $p->delivery_report_record_id = $record->id;
                        $p->comment = 'В отчете ' . $this->id . ' у заказа ' . $row['order_id'] . ' выкуп';
                        $p->save();
                    }
                }
            }
        }

        foreach ($ordersReturnData as $row) {
            $records = DeliveryReportRecord::find()
                ->where(['order_id' => $row['order_id']])
                ->andWhere(['delivery_report_id' => $this->id])
                ->all();

            $otherRecord = DeliveryReportRecord::find()
                ->joinWith('report')
                ->where(['order_id' => $row['buyout_order_id']])
                ->andWhere([DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL])
                ->limit(1)
                ->one();

            /** @var $otherRecord DeliveryReportRecord */
            if ($records && $otherRecord) {
                foreach ($records as $record) {
                    /** @var $record DeliveryReportRecord */
                    if (!OrderFinancePretension::find()->where([
                        'type' => OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS,
                        'order_id' => $row['order_id'],
                        'delivery_report_record_id' => $record->id,
                    ])->exists()
                    ) {
                        $p = new OrderFinancePretension();
                        $p->type = OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS;
                        $p->order_id = $row['order_id'];
                        $p->delivery_report_id = $this->id;
                        $p->delivery_report_record_id = $record->id;
                        $p->comment = 'В отчете ' . $otherRecord->delivery_report_id . ' у заказа ' . $row['buyout_order_id'] . ' выкуп';
                        $p->save();
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return DeliveryReportCurrency
     * @throws \yii\db\Exception
     */
    public function setDefaultByReport()
    {
        $currencies = new DeliveryReportCurrency(['delivery_report_id' => $this->id]);
        $contract = $this->delivery->getDeliveryContract()
            ->andWhere(new  Expression('(date_from is null and (date_to is null or UNIX_TIMESTAMP(date_to) >= :period_from)) or (UNIX_TIMESTAMP(date_from) <= :period_to and (date_to is null or UNIX_TIMESTAMP(date_to) <= :period_to))',
                    [':period_from' => $this->period_from, ':period_to' => $this->period_to])
            )
            ->orderBy(['date_from' => SORT_ASC, 'id' => SORT_ASC])->one();
        $default = $this->delivery->country->currency_id;
        if ($contract && $currencyChar = $contract->getCurrencyCharCode()) {
            if ($currency = Currency::find()->where(['char_code' => $currencyChar])->one()) {
                $default = $currency->id;
            }
        }
        $currencies->price_cod_currency_id = $this->delivery->country->currency_id;
        foreach (DeliveryReportCurrency::getFinancialColumns() as $column) {
            if ($column != 'price_cod_currency_id') {
                $currencies->setAttribute($column, $default);
            }
        }
        $currencies->save();

        $usd = Currency::getUSD();
        try {
            DeliveryReportCurrencyRate::deleteAll(['delivery_report_id' => $this->id]);
            if ($currencyRate = Currency::find()->convertValueToCurrency(1, $default, $usd->id, $this->period_to)) {
                (new DeliveryReportCurrencyRate(['delivery_report_id' => $this->id, 'currency_id_from' => $default, 'currency_id_to' => $usd->id, 'rate' => 1 / $currencyRate]))->save();
            }
        } catch (\Throwable $e) {
            Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
        }
        return $currencies;
    }

    /**
     * @param int $from
     * @param int|null $to
     * @param null $date
     * @param null|PaymentOrder $payment
     * @param bool $checkRevert
     * @return bool|float
     */
    public function getRate(int $from, ?int $to = null, $date = null, PaymentOrder $payment = null, $checkRevert = false)
    {
        try {
            if (!$to) {
                $to = $this->country->currency_id;
            }
            if ($to == $from) {
                return 1;
            }
            if (!$checkRevert && !empty($payment) && $payment instanceof PaymentOrder && $rate = $payment->getRate($from, $to)) {
                return $rate;
            } else {
                $rate = false;
                if ($currRate = DeliveryReportCurrencyRate::find()->where(['delivery_report_id' => $this->id, 'currency_id_from' => $from, 'currency_id_to' => $to])->one()) {
                    $rate = $currRate->rate;
                } elseif (!$checkRevert && $rateRevert = $this->getRate($to, $from, $date, $payment, true)) {
                    $rate = 1 / $rateRevert;
                } elseif (Currency::getUSD()->id == $to && $rateDB = self::getDb()->createCommand("SELECT get_currency_rate_for_date(:currency_id, :date)",
                        [':currency_id' => $from, ':date' => date('Y-m-d', (!empty($date) ? (is_numeric($date) ? $date : strtotime($date)) : $this->period_to))])->queryScalar()) {
                    $rate = $rateDB;
                } elseif (Currency::getUSD()->id != $to && $rateDB = self::getDb()->createCommand("SELECT convert_value_to_currency_by_date(1, :currency_id_from, :currency_id_to, :date)",
                    [':currency_id_from' => $to, ':currency_id_to' => $from, ':date' => date('Y-m-d', (!empty($date) ? (is_numeric($date) ? $date : strtotime($date)) : $this->period_to))])->queryScalar()) {
                    $rate = $rateDB;
                }
                return $rate;
            }
        } catch (Exception $e) {
            Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Баланс прикрепленных платежек
     * @return float
     */
    public function getBalancePayments(): float
    {
        $balance = 0;
        foreach ($this->payments as $payment) {
            if ($rate = $this->getRate($payment->currency_id, $this->country->currency->id, $payment->paid_at, $payment)) {
                $balance += round($payment->balance / $rate, 2);
            }
        }
        return (float)$balance;
    }

    /**
     * @return float
     */
    public function getBalanceAdditionalCosts(): float
    {
        $balance = 0;
        foreach ($this->costs as $cost) {
            $balance += $cost->balance;
        }
        return (float)$balance;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return (float)($this->getBalancePayments() + $this->getBalanceAdditionalCosts() + $this->sum_total);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && !$this->currencies) {
            $this->setDefaultByReport();
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
