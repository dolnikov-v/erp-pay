<?php

namespace app\modules\deliveryreport\models;

use \app\components\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "payment_delivery_report".
 *
 * @property integer $payment_id
 * @property integer $delivery_report_id
 *
 * @property DeliveryReport $deliveryReport
 * @property PaymentOrder $payment
 */
class PaymentDeliveryReport extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_delivery_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'delivery_report_id'], 'integer'],
            [
                ['delivery_report_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryReport::className(),
                'targetAttribute' => ['delivery_report_id' => 'id']
            ],
            [
                ['payment_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PaymentOrder::className(),
                'targetAttribute' => ['payment_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => Yii::t('common', 'Платежка'),
            'delivery_report_id' => Yii::t('common', 'Отчет'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_id']);
    }
}
