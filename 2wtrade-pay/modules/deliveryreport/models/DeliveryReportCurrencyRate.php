<?php
namespace app\modules\deliveryreport\models;

use Yii;
use app\components\db\ActiveRecord;
use app\models\Currency;

/**
 * This is the model class for table "delivery_report_currency_rate".
 *
 * @property integer $id
 * @property integer $delivery_report_id
 * @property integer $currency_id_from
 * @property integer $currency_id_to
 * @property string $rate
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Currency $currencyFrom
 * @property Currency $currencyTo
 * @property DeliveryReport $deliveryReport
 */
class DeliveryReportCurrencyRate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report_currency_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_id', 'currency_id_from', 'currency_id_to', 'rate'], 'required'],
            [['delivery_report_id', 'currency_id_from', 'currency_id_to', 'created_at', 'updated_at'], 'integer'],
            ['currency_id_from', 'compare', 'compareAttribute' => 'currency_id_to', 'operator' => '!='],
            [['rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_report_id' => Yii::t('common', 'Ид отчета'),
            'currency_id_from' => Yii::t('common', 'Из валюты'),
            'currency_id_to' => Yii::t('common', 'В валюту'),
            'rate' => Yii::t('common', 'Курс'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyFrom()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyTo()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }
}