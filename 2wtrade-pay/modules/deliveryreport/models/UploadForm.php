<?php

namespace app\modules\deliveryreport\models;

use app\components\ModelTrait;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\modules\loadreport\models
 */
class UploadForm extends Model
{
    use ModelTrait;

    const SCENARIO_CREATE = 'create_delivery_report';
    const SCENARIO_UPDATE = 'update_delivery_report';

    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @var string
     */
    public $country;
    /**
     * @inheritdoc
     */
    /**
     * @var
     */
    public $type;

    /**
     * @var integer
     */
    public $delivery_id;

    /**
     * @var string
     */
    public $report_name;

    /**
     * @var string
     */
    public $date_format;

    /**
     * @var integer
     */
    public $report_id;

    public $period_from;

    public $period_to;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_id', 'type', 'period_from', 'period_to'], 'required'],
            [
                'file',
                'required',
                'on' => [
                    self::SCENARIO_CREATE,
                ]
            ],
            [
                'report_name',
                'required',
                'on' => [
                    self::SCENARIO_UPDATE,
                ]
            ],
            [['country', 'type', 'period_from', 'period_to'], 'string'],
            [['delivery_id', 'report_id'], 'integer'],
            [['report_name'], 'string', 'max' => 255],
            [
                'file',
                'validateFile',
                'on' => [
                    self::SCENARIO_CREATE,
                ]
            ],
            ['date_format', 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => Yii::t('common', 'Файл'),
            'country' => Yii::t('common', 'Страна'),
            'type' => Yii::t('common', 'Тип'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'report_name' => Yii::t('common', 'Название'),
            'period_from' => Yii::t('common', 'Период'),
            'period_to' => Yii::t('common', 'Период'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateFile($attribute, $params)
    {
        if ($this->file && $this->file instanceof UploadedFile) {
            $availableExtensions = ['csv', 'xls', 'xlsx'];
            if (!in_array($this->file->extension, $availableExtensions)) {
                $this->addError($attribute, Yii::t('common', 'Файл может быть только в формате {extensions}',
                    ['extensions' => implode(', ', $availableExtensions)]));
            }
        } else {
            $this->addError($attribute, Yii::t('common', 'Необходимо указать файл.'));
        }
    }

    /**
     * @param null $filename
     * @return bool|null|string
     */
    public function upload($filename = null)
    {
        if ($this->file && $this->file instanceof UploadedFile) {
            if (is_null($filename)) {
                $filename = md5(time() . $this->file->name) . '.' . $this->file->extension;
            }
            $filePath = DeliveryReport::getFileFullRoute($filename);

            if ($this->file->saveAs($filePath)) {
                return $filename;
            }
        }
        return false;
    }
}
