<?php

namespace app\modules\deliveryreport\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderFinanceFact;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoicePayment;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "payment_order_delivery".
 *
 * @package app\modules\deliveryreport\models
 * @property integer $id
 * @property string $name
 * @property int $source
 * @property string $receiving_bank
 * @property string $payer
 * @property string $bank_recipient
 * @property double $sum
 * @property double $balance
 * @property integer $currency_id
 * @property integer $paid_at
 * @property integer $delivery_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 *
 * @property DeliveryReport[] $deliveryReports
 * @property Currency $currency
 * @property InvoicePayment[] $invoicePayments
 * @property Invoice[] $invoices
 * @property OrderFinanceFact[] $orderFinanceFacts
 * @property PaymentCurrencyRate[] $paymentCurrenciesRates
 * @property Delivery $delivery
 * @property PaymentDeliveryReport[] $paymentDeliveryReports
 */
class PaymentOrder extends ActiveRecordLogUpdateTime
{
    const SOURCE_SYSTEM = 0;
    const SOURCE_DELIVERY_REPORT = 1;
    const SOURCE_MERCURY = 2;

    /**
     * @var bool
     */
    public $active;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_order_delivery}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'created_by'
                ],
                'value' => function () {
                    return isset(Yii::$app->user) ? Yii::$app->user->id : null;
                }
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sum', 'paid_at'], 'required'],
            [['sum', 'balance'], 'number', 'min' => 0],
            [['currency_id', 'delivery_id', 'paid_at', 'created_at', 'updated_at', 'source', 'created_by'], 'integer'],
            [['name', 'receiving_bank', 'payer', 'bank_recipient'], 'string', 'max' => 255],
            ['source', 'default', 'value' => static::SOURCE_SYSTEM],
            [
                ['currency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['currency_id' => 'id']
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Номер платежа'),
            'receiving_bank' => Yii::t('common', 'Банк-отправитель'),
            'payer' => Yii::t('common', 'Плательщик'),
            'bank_recipient' => Yii::t('common', 'Банк-получатель'),
            'sum' => Yii::t('common', 'Сумма'),
            'balance' => Yii::t('common', 'Баланс'),
            'paid_at' => Yii::t('common', 'Дата оплаты'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'source' => Yii::t('common', 'Источник'),
            'created_by' => Yii::t('common', 'Идентификатор создавшего пользователя'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReports()
    {
        return $this->hasMany(DeliveryReport::className(), ['id' => 'delivery_report_id'])
            ->viaTable('payment_delivery_report', ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDeliveryReports()
    {
        return $this->hasMany(PaymentDeliveryReport::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicePayments()
    {
        return $this->hasMany(InvoicePayment::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])
            ->viaTable('invoice_payment', ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFinanceFacts()
    {
        return $this->hasMany(OrderFinanceFact::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentCurrenciesRates()
    {
        return $this->hasMany(PaymentCurrencyRate::className(), ['payment_id' => 'id']);
    }

    /**
     * @param int $from
     * @param int|null $to
     * @param bool $checkRevert
     * @return bool|float|int
     */
    public function getRate(int $from, int $to = null, $checkRevert = false)
    {
        try {
            if (!$to) {
                $to = $this->delivery->country->currency_id;
            }
            if ($to == $from) {
                return 1;
            }
            if ($rates = $this->paymentCurrenciesRates) {
                foreach ($rates as $item) {
                    if ($item->currency_id_from == $from && $item->currency_id_to == $to && !empty($item->rate)) {
                        return $item->rate;
                    }
                }
            }
            if (!$checkRevert) {
                if ($rate = $this->getRate($to, $from, true)) {
                    return 1 / $rate;
                }
            }
        } catch (\Exception $e) {
            Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
        }
        return false;
    }

    /**
     * Возврат максимально допустимой даты платежа для списания
     * @return int|null
     */
    public function getDeadlinePaid(): ?int
    {
        return $this->paid_at ? strtotime("+6 MONTH", $this->paid_at) : null;
    }
}
