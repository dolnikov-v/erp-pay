<?php

namespace app\modules\deliveryreport\models\search\filters;


use yii\base\Model;
use yii\helpers\Html;
use Yii;

/**
 * Class Filter
 * @package app\modules\deliveryreport\models\filters
 */
abstract class Filter extends Model
{
    /**
     * @var bool
     */
    public $not = false;

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public abstract function apply($query, $params);

    /**
     * @param \app\components\widgets\ActiveForm $form
     */
    public abstract function restore($form);

    /**
     * @param string $message
     * @param integer $count
     * @param string $filter
     * @return string
     */
    public function completeRestore($message, $count, $filter)
    {
        $output = '';

        if ($count > 0) {
            $output = Html::a(Yii::t('common', $message, ['count' => $count]), '#', [
                'class' => 'shower-selectable-filters',
                'data-filter' => $filter,
            ]);

            $output = Html::tag('div', $output, [
                'class' => 'form-group',
            ]);

            $output = Html::tag('div', $output, [
                'class' => 'col-lg-12',
            ]);

            $output = Html::tag('div', $output, [
                'class' => 'row',
            ]);
        }

        return $output;
    }
}
