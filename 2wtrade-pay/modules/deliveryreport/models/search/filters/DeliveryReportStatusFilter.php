<?php

namespace app\modules\deliveryreport\models\search\filters;

use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class DeliveryReportStatusFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class DeliveryReportStatusFilter extends Filter
{
    /**
     * @var string
     */
    public $status;

    /**
     * @var array
     */
    protected $statuses = [];

    public function init()
    {
        parent::init();
        $this->statuses = DeliveryReport::getStatusNames();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => array_keys($this->statuses)],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $query->andFilterWhere([DeliveryReport::tableName() . '.status' => $this->status]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'status')->select2List($this->statuses, [
            'prompt' => '—',
            'id' => false,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'status' => \Yii::t('common', 'Статус'),
        ];
    }
}