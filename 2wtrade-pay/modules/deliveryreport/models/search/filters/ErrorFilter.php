<?php

namespace app\modules\deliveryreport\models\search\filters;

use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\DeliveryReportRecordError;
use app\modules\deliveryreport\widgets\filters\ErrorFilter as ErrorFilterWidget;

/**
 * Class ErrorFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class ErrorFilter extends Filter
{
    /** @var array */
    public $not;

    /** @var array */
    public $record_error;

    /** @var array */
    public $errors = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->errors = DeliveryReportRecord::errorLabels();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['record_error', 'in', 'range' => array_keys($this->errors), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $in = [];
            $notIn = [];

            foreach ($this->record_error as $index => $error) {
                if ($error) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $notIn[] = $error;
                    } else {
                        $in[] = $error;
                    }
                }
            }

            if ($in) {
                foreach ($in as $filter) {
                    switch ($filter) {
                        case DeliveryReportRecord::ERROR_NOT_FOUND:
                            $query->andWhere(['is', DeliveryReportRecordError::tableName() . '.error_id', null]);
                            break;
                        default:
                            $query->andFilterWhere([DeliveryReportRecordError::tableName() . '.error_id' => $filter]);
                    }
                }
            }

            if ($notIn) {
                foreach ($notIn as $filter) {
                    switch ($filter) {
                        case DeliveryReportRecord::ERROR_NOT_FOUND:
                            $subQuery = DeliveryReportRecordError::find()
                                ->where(DeliveryReportRecordError::tableName() . '.delivery_report_record_id = ' . DeliveryReportRecord::tableName() . '.id');
                            $query->andWhere(['exists', $subQuery]);
                            break;
                        default:
                            $subQuery = DeliveryReportRecordError::find()
                                ->where(DeliveryReportRecordError::tableName() . '.delivery_report_record_id = ' . DeliveryReportRecord::tableName() . '.id')
                                ->andWhere([DeliveryReportRecordError::tableName() . '.error_id' => $filter]);
                            $query->andWhere(['not exists', $subQuery]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->record_error as $index => $error) {
            if ($error) {
                $model = new ErrorFilter(['record_error' => $error]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= ErrorFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->record_error) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{тип} few{типа} other{типов}}...',
            count($this->record_error) - 1, 'record_error');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->record_error)) {
            $this->record_error = [$this->record_error];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
