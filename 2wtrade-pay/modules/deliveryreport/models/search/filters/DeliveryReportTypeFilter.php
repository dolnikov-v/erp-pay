<?php

namespace app\modules\deliveryreport\models\search\filters;


use app\modules\deliveryreport\models\DeliveryReport;
use Yii;

/**
 * Class DeliveryReportTypeFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class DeliveryReportTypeFilter extends Filter
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var array
     */
    protected $types = [];

    public function init()
    {
        parent::init();
        $this->types = DeliveryReport::getTypeNames();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types)]
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $query->andFilterWhere([DeliveryReport::tableName() . '.type' => $this->type]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'type')->select2List($this->types, [
            'prompt' => '—',
            'id' => false,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('common', 'Тип'),
        ];
    }
}