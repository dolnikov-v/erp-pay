<?php
namespace app\modules\deliveryreport\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\order\models\Order;
use Yii;
use app\modules\deliveryreport\widgets\filters\FieldTextFilter as FieldTextFilterWidget;
use yii\base\InvalidParamException;

/**
 * Class FieldFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class FieldTextFilter extends Filter
{
    const ENTITY_ORDER_ID = 'order_id';
    const ENTITY_TRACKING = 'tracking';
    const ENTITY_CUSTOMER_FULL_NAME = 'fio';
    const ENTITY_CUSTOMER_PHONE = 'phone';
    const ENTITY_CUSTOMER_ADDRESS = 'address';
    const ENTITY_PRODUCTS = 'products';
    const ENTITY_AMOUNT = 'amount';
    const ENTITY_PRICE = 'price';
    const ENTITY_COD = 'cod';
    const ENTITY_DATE_CREATED = 'date_created';
    const ENTITY_DATE_APPROVE = 'date_approve';
    const ENTITY_DATE_RETURN = 'date_return';
    const ENTITY_DATE_PAYMENT = 'date_payment';
    const ENTITY_COMMENT = 'comment';
    const ENTITY_PRICE_DELIVERY = 'delivery';

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $text;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->entities = [
            self::ENTITY_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя заказчика'),
            self::ENTITY_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
            self::ENTITY_CUSTOMER_ADDRESS => Yii::t('common', 'Адрес заказчика'),
            self::ENTITY_COMMENT => Yii::t('common', 'Комментарий к заказу'),
            self::ENTITY_AMOUNT => Yii::t('common', 'Количество товара'),
            self::ENTITY_PRICE => Yii::t('common', 'Цена'),
            self::ENTITY_COD => Yii::t('common', 'Цена при доставке'),
            self::ENTITY_ORDER_ID => Yii::t('common', 'Номер заказа'),
            self::ENTITY_PRODUCTS => Yii::t('common', 'Товары'),
            self::ENTITY_TRACKING => Yii::t('common', 'Трек номер'),
            self::ENTITY_PRICE_DELIVERY => Yii::t('common', 'Цена доставки'),
            self::ENTITY_DATE_APPROVE => Yii::t('common', 'Дата подтверждения заказа'),
            self::ENTITY_DATE_CREATED => Yii::t('common', 'Дата размещения заказа'),
            self::ENTITY_DATE_PAYMENT => Yii::t('common', 'Дата оплаты'),
            self::ENTITY_DATE_RETURN => Yii::t('common', 'Дата возврата')
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true],
            ['text', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->entity as $index => $entity) {
                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->text)) {
                    $operator = 'like';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = 'not like';
                    }

                    $attribute = $this->getEntityAttribute($entity);

                    $query->andFilterWhere([$operator, $attribute, $this->text[$index]]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->text)) {
                if (empty($this->text[$index])) {
                    continue;
                }

                $model = new FieldTextFilter([
                    'entity' => $this->entity[$index],
                    'text' => $this->text[$index],
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= FieldTextFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по текстовому полю...', count($this->entity) - 1, 'number');

        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_ORDER_ID:
                $attribute = DeliveryReportRecord::tableName() . '.order_id';
                break;
            case self::ENTITY_CUSTOMER_FULL_NAME:
                $attribute = DeliveryReportRecord::tableName() . '.customer_full_name';
                break;
            case self::ENTITY_CUSTOMER_PHONE:
                $attribute = DeliveryReportRecord::tableName() . '.customer_phone';
                break;
            case self::ENTITY_CUSTOMER_ADDRESS:
                $attribute = DeliveryReportRecord::tableName() . '.customer_address';
                break;
            case self::ENTITY_COMMENT:
                $attribute = DeliveryReportRecord::tableName() . '.comment';
                break;
            case self::ENTITY_DATE_PAYMENT:
                $attribute = DeliveryReportRecord::tableName() . '.date_payment';
                break;
            case self::ENTITY_TRACKING:
                $attribute = DeliveryReportRecord::tableName() . '.tracking';
                break;
            case self::ENTITY_DATE_CREATED:
                $attribute = DeliveryReportRecord::tableName() . '.date_created';
                break;
            case self::ENTITY_PRODUCTS:
                $attribute = DeliveryReportRecord::tableName() . '.products';
                break;
            case self::ENTITY_DATE_APPROVE:
                $attribute = DeliveryReportRecord::tableName() . '.date_payment';
                break;
            case self::ENTITY_DATE_RETURN:
                $attribute = DeliveryReportRecord::tableName() . '.date_return';
                break;
            case self::ENTITY_COD:
                $attribute = DeliveryReportRecord::tableName() . '.price_cod';
                break;
            case self::ENTITY_PRICE:
                $attribute = DeliveryReportRecord::tableName() . '.price';
                break;
            case self::ENTITY_AMOUNT:
                $attribute = DeliveryReportRecord::tableName() . '.amount';
                break;
            case self::ENTITY_PRICE_DELIVERY:
                $attribute = DeliveryReportRecord::tableName() . '.price_delivery';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->text)) {
            $this->text = [$this->text];
        }
    }
}
