<?php

namespace app\modules\deliveryreport\models\search\filters;

use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\widgets\InputText;
use Yii;

/**
 * Class RecordStatusFilter
 * @package app\modules\deliveryreport\models\filters
 */
class RecordStatusFilter extends Filter
{
    /** @var array */
    public $record_status;

    /**
     * @var array
     */
    public $statuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->statuses = [
            DeliveryReportRecord::STATUS_ACTIVE => Yii::t('common', 'Неподтвержден'),
            DeliveryReportRecord::STATUS_APPROVE => Yii::t('common', 'Подтвержден'),
            DeliveryReportRecord::STATUS_STATUS_UPDATED => Yii::t('common', 'Обновлен статус'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['record_status'], 'string'],
            ['record_status', 'in', 'range' => array_keys($this->statuses)],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->record_status = isset($params['record_status']) ? $params['record_status'] : null;

        if ($this->validate()) {
            if($this->record_status) {
                $query->andWhere([DeliveryReportRecord::tableName() . '.record_status' => $this->record_status]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        if ($this->record_status) {
            $output .= InputText::widget([
                'name' => 'record_status',
                'value' => $this->record_status,
                'type' => 'hidden',
            ]);
        }

        return $output;
    }
}
