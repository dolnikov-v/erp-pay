<?php
namespace app\modules\deliveryreport\models\search\filters;


use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\deliveryreport\widgets\filters\OrderStatusFilter as OrderStatusFilterWidget;

/**
 * Class OrderStatusFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class OrderStatusFilter extends Filter
{
    /** @var array */
    public $not;

    /** @var array */
    public $order_status;

    /** @var array */
    public $statuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->statuses = OrderStatus::find()->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['order_status', 'in', 'range' => array_keys($this->statuses), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $in = [];
            $notIn = [];

            foreach ($this->order_status as $index => $status) {
                if ($status) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $notIn[] = $status;
                    } else {
                        $in[] = $status;
                    }
                }
            }

            if ($in) {
                $query->andFilterWhere(['in', Order::tableName().'.status_id', $in]);
            }

            if ($notIn) {
                $query->andFilterWhere(['not in', Order::tableName().'.status_id', $notIn]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->order_status as $index => $status) {
            if ($status) {
                $model = new OrderStatusFilter(['order_status' => $status]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= OrderStatusFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->order_status) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{статус} few{статуса} other{статусов}}...', count($this->order_status) - 1, 'order_status');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->order_status)) {
            $this->order_status = [$this->order_status];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
