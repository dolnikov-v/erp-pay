<?php

namespace app\modules\deliveryreport\models\search\filters;

use app\modules\deliveryreport\models\DeliveryReportRecord;
use Yii;
use app\modules\deliveryreport\widgets\filters\FieldNumberFilter as FieldNumberFilterWidget;
use yii\base\InvalidParamException;


/**
 * Class FieldNumberFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class FieldNumberFilter extends Filter
{
    const ENTITY_ORDER_ID = 'order_id';
    const ENTITY_TRACKING = 'tracking';
    const ENTITY_CUSTOMER_PHONE = 'phone';
    const ENTITY_AMOUNT = 'amount';
    const ENTITY_PRICE = 'price';
    const ENTITY_COD = 'cod';
    const ENTITY_PRICE_DELIVERY = 'delivery';

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $number;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->entities = [
            self::ENTITY_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
            self::ENTITY_AMOUNT => Yii::t('common', 'Количество товара'),
            self::ENTITY_PRICE => Yii::t('common', 'Цена'),
            self::ENTITY_COD => Yii::t('common', 'Цена при доставке'),
            self::ENTITY_ORDER_ID => Yii::t('common', 'Номер заказа'),
            self::ENTITY_TRACKING => Yii::t('common', 'Трек номер'),
            self::ENTITY_PRICE_DELIVERY => Yii::t('common', 'Цена доставки'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true],
            ['number', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->entity as $index => $entity) {
                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->number)) {
                    $operator = 'in';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = 'not in';
                    }

                    $attribute = $this->getEntityAttribute($entity);

                    $query->andFilterWhere([$operator, $attribute, $this->number[$index]]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->number)) {
                if (empty($this->number[$index])) {
                    continue;
                }

                $model = new FieldNumberFilter([
                    'entity' => $this->entity[$index],
                    'number' => implode(', ', $this->number[$index]),
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= FieldNumberFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по числовому полю...', count($this->entity) - 1, 'number');

        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_ORDER_ID:
                $attribute = DeliveryReportRecord::tableName() . '.order_id';
                break;
            case self::ENTITY_CUSTOMER_PHONE:
                $attribute = DeliveryReportRecord::tableName() . '.customer_phone';
                break;
            case self::ENTITY_TRACKING:
                $attribute = DeliveryReportRecord::tableName() . '.tracking';
                break;
            case self::ENTITY_COD:
                $attribute = DeliveryReportRecord::tableName() . '.price_cod';
                break;
            case self::ENTITY_PRICE:
                $attribute = DeliveryReportRecord::tableName() . '.price';
                break;
            case self::ENTITY_AMOUNT:
                $attribute = DeliveryReportRecord::tableName() . '.amount';
                break;
            case self::ENTITY_PRICE_DELIVERY:
                $attribute = DeliveryReportRecord::tableName() . '.price_delivery';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->number)) {
            $this->number = [$this->number];
        }

        foreach ($this->number as $key => $number) {
            if (!empty($number) && is_string($number)) {
                $number = explode(',', $number);
                $this->number[$key] = array_map('trim', $number);
            }
        }
    }
}
