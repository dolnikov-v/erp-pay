<?php

namespace app\modules\deliveryreport\models\search\filters;


use app\modules\delivery\models\Delivery;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class DeliveryFilter
 * @package app\modules\deliveryreport\models\search\filters
 */
class DeliveryFilter extends Filter
{
    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @var integer
     */
    public $delivery_id;


    public function init()
    {
        parent::init();
        $this->deliveries = Delivery::find()->byCountryId(\Yii::$app->user->country->id)->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['delivery_id', 'integer'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $query->andFilterWhere([DeliveryReport::tableName() . '.delivery_id' => $this->delivery_id]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'delivery_id')->select2List($this->deliveries, [
            'prompt' => '—',
            'id' => false,
            'length' => false,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'delivery_id' => \Yii::t('common', 'Служба доставки')
        ];
    }
}