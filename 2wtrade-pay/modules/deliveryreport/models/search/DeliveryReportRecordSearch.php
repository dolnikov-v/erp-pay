<?php

namespace app\modules\deliveryreport\models\search;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\search\filters\FieldNumberFilter;
use app\modules\deliveryreport\models\search\filters\FieldTextFilter;
use app\modules\deliveryreport\models\search\filters\OrderStatusFilter;
use app\modules\deliveryreport\models\search\filters\StatusFilter;
use app\modules\deliveryreport\models\search\filters\PretensionFilter;
use app\modules\order\models\Order;
use app\widgets\LinkPager;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\search\filters\ErrorFilter;
use app\modules\deliveryreport\models\search\filters\RecordStatusFilter;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\modules\order\models\OrderFinancePretension;

/**
 * Class DeliveryReportRecordSearch
 * @package app\modules\deliveryreport\models\search
 */
class DeliveryReportRecordSearch extends DeliveryReportRecord
{
    /**
     * @var array
     */
    public $selectableFilters = [];

    /** @var \yii\db\ActiveQuery */
    private $query;

    /**
     * @var ErrorFilter
     */
    private $errorFilter;

    /**
     * @var FieldTextFilter
     */
    private $fieldTextFilter;

    /**
     * @var FieldNumberFilter
     */
    private $fieldNumberFilter;

    /**
     * @var StatusFilter
     */
    private $statusFilter;

    /**
     * @var RecordStatusFilter
     */
    private $recordStatusFilter;

    /**
     * @var OrderStatusFilter
     */
    private $orderStatusFilter;

    /**
     * @var PretensionFilter
     */
    private $pretensionFilter;


    /**
     * @var DeliveryReport
     */
    private $report;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->selectableFilters = [
            'record_error' => Yii::t('common', 'По типу ошибки'),
            'field_text_filter' => Yii::t('common', 'Поиск по текстовому полю'),
            'field_number_filter' => Yii::t('common', 'Поиск по числовому полю'),
            'status' => Yii::t('common', 'Поиск по статусу'),
            'order_status' => Yii::t('common', 'Поиск по статусу заказа'),
            'pretension' => Yii::t('common', 'Поиск по претензии'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'delivery_report_id', 'created_at', 'updated_at'], 'integer'],
            [
                [
                    'order_id',
                    'tracking',
                    'status',
                    'customer_full_name',
                    'customer_phone',
                    'customer_address',
                    'customer_zip',
                    'customer_city',
                    'customer_region',
                    'customer_district',
                    'comment',
                    'products',
                    'amount',
                    'price',
                    'date_created',
                    'date_approve',
                    'date_return',
                    'date_payment',
                    'price_storage',
                    'price_packing',
                    'price_package',
                    'price_address_correction',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_cod_service',
                    'price_cod',
                    'error_log'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @param integer $deliveryReportId
     * @param array|bool $with
     * @return ActiveDataProvider
     */
    public function search($params, $deliveryReportId = null, $with = false)
    {
        $this->buildQuery($params, $deliveryReportId, $with);

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        LinkPager::setPageSize($dataProvider->pagination);

        $dataProvider->setModels(self::findRelatedOrders($dataProvider->models, $this->report));

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param integer $deliveryReportId
     * @param array|bool $with
     * @return \yii\db\ActiveQuery
     */
    public function buildQuery($params, $deliveryReportId = null, $with = null)
    {
        $this->load($params);

        if (!is_null($deliveryReportId)) {
            $this->delivery_report_id = $deliveryReportId;
            $this->report = DeliveryReport::findOne($this->delivery_report_id);
        }

        $this->query = DeliveryReportRecord::find();

        if ($this->validate()) {
            if (!is_array($with)) {
                $with = [
                    'report',
                    'recordErrors',
                    'firstDeliveryRequest',
                    'deliveryRequest',
                    'order.deliveryRequest',
                    'order.finance',
                    'order.financePrediction',
                    'order.status',
                    'order.orderProducts',
                    'order.orderProducts.product',
                    'order.callCenterRequest',
                    'order.duplicateOrder.deliveryRequest',
                    'order.duplicateOrder.finance',
                    'order.duplicateOrder.financePrediction',
                    'order.duplicateOrder.status',
                    'order.duplicateOrder.orderProducts',
                    'order.duplicateOrder.orderProducts.product',
                    'order.duplicateOrder.callCenterRequest',
                    'order.duplicateOrder.originalOrder',
                    'firstDeliveryRequest.order',
                    'firstDeliveryRequest.order.deliveryRequest',
                    'firstDeliveryRequest.order.finance',
                    'firstDeliveryRequest.order.financePrediction',
                    'firstDeliveryRequest.order.status',
                    'firstDeliveryRequest.order.callCenterRequest',
                    'firstDeliveryRequest.order.orderProducts',
                    'firstDeliveryRequest.order.orderProducts.product',
                    'callCenterRequest.order',
                    'callCenterRequest.order.deliveryRequest',
                    'callCenterRequest.order.finance',
                    'callCenterRequest.order.financePrediction',
                    'callCenterRequest.order.status',
                    'callCenterRequest.order.orderProducts',
                    'callCenterRequest.order.orderProducts.product',/*
                    'callCenterRequest.order.callCenterRequest',
                    'callCenterRequest.order.duplicateOrder.deliveryRequest',
                    'callCenterRequest.order.duplicateOrder.finance',
                    'callCenterRequest.order.duplicateOrder.financePrediction',
                    'callCenterRequest.order.duplicateOrder.status',
                    'callCenterRequest.order.duplicateOrder.orderProducts',
                    'callCenterRequest.order.duplicateOrder.orderProducts.product',
                    'callCenterRequest.order.duplicateOrder.callCenterRequest',
                    'callCenterRequest.order.duplicateOrder.originalOrder',*/
                ];
            }

            $this->query->joinWith(['order', 'recordErrors'], false)->with($with);


            $this->query->andFilterWhere([
                DeliveryReportRecord::tableName() . '.id' => $this->id,
                DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->delivery_report_id,
                DeliveryReportRecord::tableName() . '.created_at' => $this->created_at,
                DeliveryReportRecord::tableName() . '.updated_at' => $this->updated_at,
            ]);

            $this->query->andWhere([
                '!=',
                DeliveryReportRecord::tableName() . '.record_status',
                DeliveryReportRecord::STATUS_DELETED
            ]);

            $this->query->groupBy([DeliveryReportRecord::tableName() . '.id']);

            $this->applyFilters($params);
        }

        return $this->query;
    }

    public function bindQueryForPretension($params, $deliveryReportId = null, $with = null)
    {
        $this->load($params);

        if (!is_null($deliveryReportId)) {
            $this->delivery_report_id = $deliveryReportId;
            $this->report = DeliveryReport::findOne($this->delivery_report_id);
        }

        $this->query = DeliveryReportRecord::find();

        if ($this->validate()) {
            if (!is_array($with)) {
                $with = [
                    'report',
                    'recordErrors',
                    'firstDeliveryRequest',
                    'deliveryRequest',
                    'order.deliveryRequest',
                    'order.finance',
                    'order.financePrediction',
                    'order.status',
                    'order.orderProducts',
                    'order.orderProducts.product',
                    'order.callCenterRequest',
                    'order.duplicateOrder.deliveryRequest',
                    'order.duplicateOrder.finance',
                    'order.duplicateOrder.financePrediction',
                    'order.duplicateOrder.status',
                    'order.duplicateOrder.orderProducts',
                    'order.duplicateOrder.orderProducts.product',
                    'order.duplicateOrder.callCenterRequest',
                    'order.duplicateOrder.originalOrder',
                    'firstDeliveryRequest.order',
                    'firstDeliveryRequest.order.deliveryRequest',
                    'firstDeliveryRequest.order.finance',
                    'firstDeliveryRequest.order.financePrediction',
                    'firstDeliveryRequest.order.status',
                    'firstDeliveryRequest.order.callCenterRequest',
                    'firstDeliveryRequest.order.orderProducts',
                    'firstDeliveryRequest.order.orderProducts.product',
                    'callCenterRequest.order',
                    'callCenterRequest.order.deliveryRequest',
                    'callCenterRequest.order.finance',
                    'callCenterRequest.order.financePrediction',
                    'callCenterRequest.order.status',
                    'callCenterRequest.order.orderProducts',
                    'callCenterRequest.order.orderProducts.product',/*
                    'callCenterRequest.order.callCenterRequest',
                    'callCenterRequest.order.duplicateOrder.deliveryRequest',
                    'callCenterRequest.order.duplicateOrder.finance',
                    'callCenterRequest.order.duplicateOrder.financePrediction',
                    'callCenterRequest.order.duplicateOrder.status',
                    'callCenterRequest.order.duplicateOrder.orderProducts',
                    'callCenterRequest.order.duplicateOrder.orderProducts.product',
                    'callCenterRequest.order.duplicateOrder.callCenterRequest',
                    'callCenterRequest.order.duplicateOrder.originalOrder',*/
                ];
            }

            $this->query->joinWith(['order', 'recordErrors'], false)->with($with);
            $this->query->innerJoin(
                OrderFinancePretension::tableName(),
                OrderFinancePretension::tableName() . '.' . OrderFinancePretension::COLUMN_ORDER_ID . '=' . DeliveryReportRecord::tableName() . '.' . DeliveryReportRecord::COLUMN_ORDER_ID
                . ' AND ' . OrderFinancePretension::tableName() . '.' . OrderFinancePretension::COLUMN_DISABLED . '!=1'
            );

            $this->query->andFilterWhere([
                DeliveryReportRecord::tableName() . '.id' => $this->id,
                DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->delivery_report_id,
                DeliveryReportRecord::tableName() . '.created_at' => $this->created_at,
                DeliveryReportRecord::tableName() . '.updated_at' => $this->updated_at,
            ]);

            $this->query->andWhere([
                '!=',
                DeliveryReportRecord::tableName() . '.record_status',
                DeliveryReportRecord::STATUS_DELETED
            ]);

            $this->query->groupBy([DeliveryReportRecord::tableName() . '.id']);

            $this->applyFilters($params);
        }

        return $this->query;
    }

    /**
     * @return ErrorFilter
     */
    public function getErrorFilter()
    {
        if (is_null($this->errorFilter)) {
            $this->errorFilter = new ErrorFilter();
        }
        return $this->errorFilter;
    }

    /**
     * @return FieldTextFilter
     */
    public function getFieldTextFilter()
    {
        if (is_null($this->fieldTextFilter)) {
            $this->fieldTextFilter = new FieldTextFilter();
        }
        return $this->fieldTextFilter;
    }

    /**
     * @return FieldNumberFilter
     */
    public function getFieldNumberFilter()
    {
        if (is_null($this->fieldNumberFilter)) {
            $this->fieldNumberFilter = new FieldNumberFilter();
        }
        return $this->fieldNumberFilter;
    }

    /**
     * @return StatusFilter
     */
    public function getStatusFilter()
    {
        if (is_null($this->statusFilter)) {
            $this->statusFilter = new StatusFilter(['report' => $this->report]);
        }
        return $this->statusFilter;
    }

    /**
     * @return RecordStatusFilter
     */
    public function getRecordStatusFilter()
    {
        if (is_null($this->recordStatusFilter)) {
            $this->recordStatusFilter = new RecordStatusFilter();
        }
        return $this->recordStatusFilter;
    }

    /**
     * @return OrderStatusFilter
     */
    public function getOrderStatusFilter()
    {
        if (is_null($this->orderStatusFilter)) {
            $this->orderStatusFilter = new OrderStatusFilter();
        }
        return $this->orderStatusFilter;
    }

    /**
     * @return PretensionFilter
     */
    public function getPretensionFilter()
    {
        if (is_null($this->pretensionFilter)) {
            $this->pretensionFilter = new PretensionFilter();
        }
        return $this->pretensionFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $output .= $this->errorFilter->restore($form);
        $output .= $this->fieldTextFilter->restore($form);
        $output .= $this->fieldNumberFilter->restore($form);
        $output .= $this->statusFilter->restore($form);
        $output .= $this->recordStatusFilter->restore($form);
        $output .= $this->orderStatusFilter->restore($form);
        $output .= $this->pretensionFilter->restore($form);
        return $output;
    }

    /**
     * @param array $params
     */
    public function applyFilters($params)
    {
        $this->getErrorFilter()->apply($this->query, $params);
        $this->getFieldTextFilter()->apply($this->query, $params);
        $this->getFieldNumberFilter()->apply($this->query, $params);
        $this->getStatusFilter()->apply($this->query, $params);
        $this->getRecordStatusFilter()->apply($this->query, $params);
        $this->getOrderStatusFilter()->apply($this->query, $params);
        $this->getPretensionFilter()->apply($this->query, $params);
    }

    /**
     * @param DeliveryReportRecord[] $models
     * @param DeliveryReport $report
     * @return DeliveryReportRecord[]
     */
    public static function findRelatedOrders($models, $report)
    {
        $notFoundIds = [];
        foreach ($models as $model) {
            if (empty($model->relatedOrder)) {
                $notFoundIds[] = $model->id;
            }
        }

        $offset = 0;
        $totalCount = count($notFoundIds);
        $foundedOrders = [];

        while ($offset < $totalCount) {
            $batch = array_slice($notFoundIds, $offset, 300);
            $offset += 300;

            $secondQuery = DeliveryReportRecord::find()->where([DeliveryReportRecord::tableName() . '.id' => $batch]);
            $secondQuery->innerJoin(DeliveryRequest::tableName(), [
                'like',
                DeliveryRequest::tableName() . '.finance_tracking',
                new Expression('CONCAT("%",' . DeliveryReportRecord::tableName() . '.tracking' . ', "%")')
            ]);
            $secondQuery->select([
                'delivery_report_record_id' => DeliveryReportRecord::tableName() . '.id',
                'order_id' => DeliveryRequest::tableName() . '.order_id',
            ]);
            $secondQuery->andWhere(['!=', DeliveryReportRecord::tableName() . '.tracking', '']);
            $secondQuery->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $report->delivery_id]);
            $secondQuery->asArray();

            $orderIds = $secondQuery->all();

            $orderIds = ArrayHelper::map($orderIds, 'order_id', 'delivery_report_record_id');
            $orders = Order::find()->where(['id' => array_keys($orderIds)])->with([
                'deliveryRequest',
                'finance',
                'orderProducts',
                'status'
            ])->all();
            foreach ($orders as $order) {
                $foundedOrders[$orderIds[$order->id]] = $order;
            }
        }
        if (count($foundedOrders) > 0) {
            foreach ($models as $model) {
                if (empty($model->relatedOrder) && isset($foundedOrders[$model->id])) {
                    $model->relatedOrder = $foundedOrders[$model->id];
                }
            }
        }

        return $models;
    }
}
