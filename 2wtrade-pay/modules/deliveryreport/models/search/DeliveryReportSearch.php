<?php

namespace app\modules\deliveryreport\models\search;

use app\components\db\ActiveQuery;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\search\filters\DeliveryFilter;
use app\modules\deliveryreport\models\search\filters\DeliveryReportStatusFilter;
use app\modules\deliveryreport\models\search\filters\DeliveryReportTypeFilter;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryReportSearch
 * @package app\modules\deliveryreport\models\search
 */
class DeliveryReportSearch extends DeliveryReport
{
    /**
     * @var null| DeliveryFilter
     */
    protected $deliveryFilter = null;
    /**
     * @var null | DeliveryReportTypeFilter
     */
    protected $typeFilter = null;
    /**
     * @var null | DeliveryReportStatusFilter
     */
    protected $statusFilter = null;

    /**
     * @var ActiveQuery
     */
    protected $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'updated_at', 'country_id', 'delivery_id'], 'integer'],
            [['type', 'status', 'payment_id'], 'string'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $with = [
            'delivery',
            'user',
        ];

        $this->query = DeliveryReport::find();

        $this->query->with($with);

        $this->applyFilters($params);

        $this->query->andWhere(['country_id' => Yii::$app->user->country->id]);

        $this->query->orderBy(['id' => SORT_DESC]);

        return new ActiveDataProvider([
            'query' => $this->query,
        ]);
    }

    /**
     * @return DeliveryFilter|null
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter();
        }
        return $this->deliveryFilter;
    }

    /**
     * @return DeliveryReportStatusFilter|null
     */
    public function getStatusFilter()
    {
        if (is_null($this->statusFilter)) {
            $this->statusFilter = new DeliveryReportStatusFilter();
        }
        return $this->statusFilter;
    }

    /**
     * @return DeliveryReportTypeFilter|null
     */
    public function getTypeFilter()
    {
        if (is_null($this->typeFilter)) {
            $this->typeFilter = new DeliveryReportTypeFilter();
        }
        return $this->typeFilter;
    }

    /**
     * @param $params
     */
    protected function applyFilters($params)
    {
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getStatusFilter()->apply($this->query, $params);
        $this->getTypeFilter()->apply($this->query, $params);

    }
}
