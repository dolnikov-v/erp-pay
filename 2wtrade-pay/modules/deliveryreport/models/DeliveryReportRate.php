<?php
namespace app\modules\deliveryreport\models;

use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;

/**
 * This is the model class for table "delivery_report_rate".
 *
 * @property integer $id
 * @property integer $delivery_report_id
 * @property integer $currency_id
 * @property string $rate
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Currency $currency
 * @property DeliveryReport $deliveryReport
 */
class DeliveryReportRate extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_id', 'currency_id'], 'required'],
            [['delivery_report_id', 'currency_id', 'created_at', 'updated_at'], 'integer'],
            [['rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_report_id' => Yii::t('common', 'Ид отчета'),
            'currency_id' => Yii::t('common', 'Ид валюты'),
            'rate' => Yii::t('common', 'Курс валюты'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }
}