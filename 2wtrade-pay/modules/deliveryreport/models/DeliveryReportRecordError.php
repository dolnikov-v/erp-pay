<?php

namespace app\modules\deliveryreport\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%delivery_report_record_error}}".
 *
 * @property integer $delivery_report_record_id
 * @property integer $error_id
 * @property string $values
 *
 * @property DeliveryReportRecord $deliveryReportRecord
 * @property array $decodedValues
 */
class DeliveryReportRecordError extends ActiveRecord
{
    protected $decodedValues = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_report_record_error}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_report_record_id', 'error_id'], 'required'],
            [['delivery_report_record_id', 'error_id'], 'integer'],
            [['values'], 'string'],
            [
                ['delivery_report_record_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryReportRecord::className(),
                'targetAttribute' => ['delivery_report_record_id' => 'id']
            ],
        ];
    }

    /**
     * @return array|mixed
     */
    public function getDecodedValues()
    {
        if (is_null($this->decodedValues)) {
            $this->decodedValues = json_decode($this->values, true) ?? [];
        }
        return $this->decodedValues;
    }

    /**
     * @param $values
     */
    public function setDecodedValues($values)
    {
        $this->decodedValues = $values;
        if ($this->decodedValues) {
            $this->values = json_encode($this->decodedValues, JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReportRecord()
    {
        return $this->hasOne(DeliveryReportRecord::className(), ['id' => 'delivery_report_record_id'])->inverseOf('recordErrors');
    }
}
