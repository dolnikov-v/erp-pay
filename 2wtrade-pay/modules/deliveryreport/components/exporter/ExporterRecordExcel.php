<?php

namespace app\modules\deliveryreport\components\exporter;

use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Yii;

/**
 * Class ExporterRecordExcel
 * @package app\modules\order\components\exporter
 */
class ExporterRecordExcel extends ExporterReportRecord
{
    protected static $type = self::TYPE_EXCEL;

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function sendFile()
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(Yii::t('common', 'Заказы'));

        $iHead = 0;
        $sheet->setCellValueByColumnAndRow($iHead, 1, 'DATA SOURCE');
        $excel->getActiveSheet()->getColumnDimension($this->printAlphabet($iHead))
            ->setAutoSize(true);
        $sheet->getStyleByColumnAndRow($iHead, 1)
            ->getBorders()
            ->getBottom()
            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
        $iHead++;
        foreach ($this->getShowerColumns() as $item) {
            if (isset($item['subquery'])) {
                foreach ($item['subquery'] as $nameColumn) {
                    $excel->getActiveSheet()->getColumnDimension($this->printAlphabet($iHead))
                        ->setAutoSize(true);
                    $sheet->setCellValueByColumnAndRow($iHead, 1, $nameColumn);
                    $sheet->getStyleByColumnAndRow($iHead, 1)
                        ->getBorders()
                        ->getBottom()
                        ->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                    $iHead++;
                }
            } else {
                $excel->getActiveSheet()->getColumnDimension($this->printAlphabet($iHead))
                    ->setAutoSize(true);
                $sheet->setCellValueByColumnAndRow($iHead, 1, $item['name']);
                $sheet->getStyleByColumnAndRow($iHead, 1)
                    ->getBorders()
                    ->getBottom()
                    ->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $iHead++;
            }
        }

        // Заполнение данными
        $offset = 0;
        $limit = 100;

        while ($orders = $this->getRequestData($offset, $limit)) {
            foreach ($orders as $key => $order) {
                $i = 0;

                $row = ($offset + $key) * 2 + 2;
                $sheet->setCellValueByColumnAndRow($i, $row, self::ROW_TYPE_REPORT);
                $i++;

                foreach ($this->getShowerColumns() as $keyColumn => $column) {
                    if (isset($column['subquery'])) {
                        foreach ($column['subquery'] as $columnSub => $item) {
                            if (in_array($keyColumn, $order->errorFields)) {
                                $sheet->getStyleByColumnAndRow($i, $row)
                                    ->getFont()
                                    ->getColor()->setRGB('ff0000');
                            }
                            $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $order[$keyColumn][$columnSub]);
                            $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                            $i++;
                        }
                    } else {
                        if (in_array($keyColumn, $order->errorFields)) {
                            $sheet->getStyleByColumnAndRow($i, $row)->getFont()->getColor()->setRGB('ff0000');
                        }
                        $dataCell = $this->prepareMapping($keyColumn, $order[$keyColumn]);
                        $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                        $i++;
                    }
                }

                $i = 0;
                $row++;
                $sheet->setCellValueByColumnAndRow($i, $row, self::ROW_TYPE_PAY);
                $sheet->getStyleByColumnAndRow($i, $row)
                    ->getBorders()
                    ->getBottom()
                    ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $i++;
                $relatedOrder = $order->convertedOrder;
                if ($relatedOrder) {
                    foreach ($this->getShowerColumns() as $keyColumn => $column) {
                        if (isset($column['subquery'])) {
                            foreach ($column['subquery'] as $columnSub => $item) {
                                $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $relatedOrder[$keyColumn][$columnSub]);
                                $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                                $sheet->getStyleByColumnAndRow($i, $row)
                                    ->getBorders()
                                    ->getBottom()
                                    ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                                $i++;
                            }
                        } else {
                            $dataCell = $this->prepareMapping($keyColumn, $relatedOrder[$keyColumn]);
                            $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                            $sheet->getStyleByColumnAndRow($i, $row)
                                ->getBorders()
                                ->getBottom()
                                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                            $i++;
                        }
                    }
                }
            }

            $offset += $limit;
        }

        $filename = "Export_DeliveryReport_" . date("Y-m-d_H-i-s") . "_" . Yii::t('common',
                Yii::$app->user->country->name) . '_2wtrade.xlsx';

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');

        Yii::$app->end();
    }
}
