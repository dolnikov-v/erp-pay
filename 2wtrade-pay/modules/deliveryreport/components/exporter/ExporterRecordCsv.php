<?php

namespace app\modules\deliveryreport\components\exporter;

use PHPExcel;
use PHPExcel_Writer_CSV;
use Yii;

/**
 * Class ExporterCsv
 * @package app\modules\deliveryreport\components\exporter
 */
class ExporterRecordCsv extends ExporterReportRecord
{
    protected static $type = self::TYPE_CSV;

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function sendFile()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $iHead = 0;

        $sheet->setCellValueByColumnAndRow($iHead++, 1, 'DATA SOURCE');
        // Установка заголовка
        foreach ($this->getShowerColumns() as $item) {
            if (isset($item['subquery'])) {
                foreach ($item['subquery'] as $nameColumn) {
                    $sheet->setCellValueByColumnAndRow($iHead, 1, $nameColumn);
                    $iHead++;
                }
            } else {
                $sheet->setCellValueByColumnAndRow($iHead, 1, $item['name']);
                $iHead++;
            }
        }

        $offset = 0;
        $limit = 100;

        while ($orders = $this->getRequestData($offset, $limit)) {
            foreach ($orders as $key => $order) {
                $i = 0;

                $row = ($offset + $key) * 2 + 2;
                $sheet->setCellValueByColumnAndRow($i++, $row, self::ROW_TYPE_REPORT);
                foreach ($this->getShowerColumns() as $keyColumn => $column) {
                    if (isset($column['subquery'])) {
                        foreach ($column['subquery'] as $columnSub => $item) {
                            $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $order[$keyColumn][$columnSub]);
                            $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                            $i++;
                        }
                    } else {
                        $dataCell = $this->prepareMapping($keyColumn, $order[$keyColumn]);
                        $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                        $i++;
                    }
                }

                $i = 0;
                $row++;
                $sheet->setCellValueByColumnAndRow($i++, $row, self::ROW_TYPE_PAY);
                $relatedOrder = $order->convertedOrder;
                if ($relatedOrder) {
                    foreach ($this->getShowerColumns() as $keyColumn => $column) {
                        if (isset($column['subquery'])) {
                            foreach ($column['subquery'] as $columnSub => $item) {
                                $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $relatedOrder[$keyColumn][$columnSub]);
                                $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                                $i++;
                            }
                        } else {
                            $dataCell = $this->prepareMapping($keyColumn, $relatedOrder[$keyColumn]);
                            $sheet->setCellValueByColumnAndRow($i, $row, $dataCell);
                            $i++;
                        }
                    }
                }
            }

            $offset += $limit;
        }

        $filename = "Export_DeliveryReport_" . date("Y-m-d_H-i-s") . "_" . Yii::t('common', Yii::$app->user->country->name) . '_2wtrade.csv';

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
        $objWriter->setDelimiter(',');
        $objWriter->setUseBOM(true);

        $objWriter->setLineEnding("\r\n");
        $objWriter->save('php://output');
        Yii::$app->end();
    }
}
