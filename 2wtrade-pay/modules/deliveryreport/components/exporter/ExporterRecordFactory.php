<?php
namespace app\modules\deliveryreport\components\exporter;

use yii\base\InvalidParamException;
use Yii;

/**
 * Class ExportRecordFactory
 * @package app\modules\deliveryreport\components\exporter
 */
class ExporterRecordFactory
{
    /**
     * @param string $type
     * @param array $config
     * @return ExporterReportRecord
     */
    public static function build($type, $config = [])
    {
        $path = "\\app\\modules\\deliveryreport\\components\\exporter\\ExporterRecord" . ucfirst(strtolower($type));

        if (class_exists($path)) {
            return new $path($config);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип ExporterRecord.'));
        }
    }
}
