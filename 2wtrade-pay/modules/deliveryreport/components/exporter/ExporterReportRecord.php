<?php

namespace app\modules\deliveryreport\components\exporter;

use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\modules\deliveryreport\widgets\ShowerColumns;

/**
 * Class ExporterReportRecord
 * @package app\modules\deliveryreport\components\exporter
 */
abstract class ExporterReportRecord extends BaseObject
{
    const TYPE_EXCEL = 'excel';
    const TYPE_CSV = 'csv';

    /** @var ActiveDataProvider */
    public $dataProvider;

    /** @var ShowerColumns */
    public $showerColumns;

    /**
     * @var DeliveryReport
     */
    public $report;

    const ROW_TYPE_PAY = 'Pay';
    const ROW_TYPE_REPORT = 'Report';

    /**
     * @var string
     */
    protected static $type;

    abstract public function sendFile();

    /**
     * @return string
     */
    public static function getExporterUrl()
    {
        $queryParams = Yii::$app->request->getQueryParams();
        $queryParams['export'] = static::$type;
        $url = array_merge([''], $queryParams);

        return Url::to($url);
    }

    /**
     * @return array
     */
    public function getShowerColumns()
    {
        $necessaryColumns = [];

        $columnNames = array_merge(DeliveryReportRecord::getColumnNames(DeliveryReport::TYPE_FINANCIAL), DeliveryReportRecord::getNonRequiredColumnNames(DeliveryReport::TYPE_FINANCIAL));

        foreach ($columnNames as $value => $key) {
            $necessaryColumns += [
                $key => [
                    'name' => strtoupper($value),
                ]
            ];
        }

        return $necessaryColumns;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getRequestData($offset, $limit)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = $this->dataProvider->query;

        $query->orderBy(DeliveryReportRecord::tableName() . '.id');
        $query->limit($limit);
        $query->offset($offset);

        return $query->all();
    }

    /**
     * @param $key
     * @return string
     */
    protected function printAlphabet($key)
    {
        $array = [];
        $range = range(65, 90);

        foreach ($range as $letter) {
            $array[] = chr($letter);
        }

        $str = "";
        do {
            $index = $key % 25;
            $str = $array[$index] . $str;
            $key /= 25;
            $key = intval($key) - 1;
        } while ($key >= 0);

        return $str;
    }

    /**
     * @param $field
     * @param $dataCell
     * @return mixed|string
     */
    protected function prepareMapping($field, $dataCell)
    {
        if ($dataCell) {
            return $dataCell;
        }
        return "";
    }
}
