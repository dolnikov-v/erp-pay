<?php
namespace app\modules\deliveryreport\components\exporter;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\query\OrderQuery;
use Yii;

/**
 * Class WithoutReportOrderExporter
 * @package app\modules\order\components\exporter
 */
class WithoutReportOrderExporter
{
    /** @var  OrderQuery */
    public $query;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var int
     */
    public $batchSize = 200;

    const PATH_UPLOAD = 'delivery-report';

    /**
     * @var integer
     */
    protected $dateFrom;

    /**
     * @var integer
     */
    protected $dateTo;

    /**
     * @var integer
     */
    protected $deliveryId;

    /**
     * @var array
     */
    protected $availableStatus = [
        OrderStatus::STATUS_DELIVERY_ACCEPTED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY,
        OrderStatus::STATUS_DELIVERY_BUYOUT,
        OrderStatus::STATUS_DELIVERY_DENIAL,
        OrderStatus::STATUS_DELIVERY_RETURNED,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_REDELIVERY,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_DELAYED,
        OrderStatus::STATUS_DELIVERY_REFUND,
        OrderStatus::STATUS_DELIVERY_LOST,
    ];

    /**
     * @param null | string $filename
     * @param null | integer $delivery_id
     * @param null | integer $dateFrom
     * @param null | integer $dateTo
     */
    public function __construct($filename = null, $delivery_id = null, $dateFrom = null, $dateTo = null)
    {
        if (!is_null($dateFrom)) {
            $dateFrom = Yii::$app->formatter->asTimestamp($dateFrom);
        }
        if (!is_null($dateTo)) {
            $dateTo = Yii::$app->formatter->asTimestamp($dateTo);
        }

        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->deliveryId = $delivery_id;

        if (empty($filename)) {
            $filename = "export_WithoutReportOrders_" . str_replace(' ', '', Yii::$app->user->country->slug);

            if (!empty($delivery_id)) {
                $delivery = Delivery::findOne($delivery_id);
                if ($delivery) {
                    $filename .= "_" . str_replace(' ', '', $delivery->name);
                }
            }

            if (!empty($dateFrom)) {
                $filename .= "_from-" . Yii::$app->formatter->asDate($dateFrom, 'php:d.m.Y');
            }
            if (!empty($dateTo)) {
                $filename .= "_to-" . Yii::$app->formatter->asDate($dateTo, 'php:d.m.Y');
            }

            $filename .= "_" . time() . ".csv";
        }

        $this->filename = $filename;
    }


    /**
     * @param int $offset
     * @throws \Exception
     */
    public function generateFile($offset = 0)
    {
        $this->buildQuery();
        $file = null;
        $path = DeliveryReport::getTmpPath() . DIRECTORY_SEPARATOR . $this->filename;
        $file = fopen($path, 'a');

        if (!$file) {
            throw new \Exception(Yii::t('common', 'Не удалось создать файл.'));
        }

        $columns = DeliveryReportRecord::getFinancialColumnNames();

        if ($offset == 0) {
            $row = [];
            foreach ($columns as $name => $column) {
                $row[] = '"' . str_replace('"', '""', strtoupper($name)) . '"';
            }

            fwrite($file, implode(',', $row) . "\r\n");
        }

        $offset = intval($offset) * $this->batchSize;
        $this->query->offset($offset);
        if (!empty($this->batchSize)) {
            $this->query->limit($this->batchSize);
        }

        $orders = $this->query->all();

        $data = "";
        foreach ($orders as $order) {
            $buffer = DeliveryReportRecord::convertOrder($order);
            $row = [];
            foreach ($columns as $column) {
                $cell = str_replace('"', '""', $buffer->$column);
                $row[] = '"' . $cell . '"';
            }
            $data .= implode(',', $row) . "\r\n";
        }

        fwrite($file, $data);
        fclose($file);
    }

    /**
     * @return array
     */
    public function getOrderCount()
    {
        $query = Order::find()->where([Order::tableName() . '.status_id' => $this->availableStatus,]);
        $query->andWhere(['is', Order::tableName() . '.report_status_id', null]);
        $query->byCountryId(Yii::$app->user->country->id);

        $querySecond = Order::find()->where([
            'status_id' => $this->availableStatus
        ])->byCountryId(Yii::$app->user->country->id);

        if (!empty($this->dateFrom)) {
            $querySecond->andFilterWhere(['>=', Order::tableName() . '.created_at', $this->dateFrom]);
            $query->andFilterWhere(['>=', Order::tableName() . '.created_at', $this->dateFrom]);
        }

        if (!empty($this->dateTo)) {
            $querySecond->andFilterWhere(['<=', Order::tableName() . '.created_at', $this->dateTo]);
            $query->andFilterWhere(['<=', Order::tableName() . '.created_at', $this->dateTo]);
        }

        if (!empty($this->deliveryId)) {
            $query->innerJoinWith('deliveryRequest');
            $querySecond->innerJoinWith('deliveryRequest');

            $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $this->deliveryId]);
            $querySecond->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $this->deliveryId]);
        }

        $totalOrderCount = $querySecond->count();

        $withoutReport = $query->count();

        $query = DeliveryReportRecord::find()->joinWith(['report']);
        $query->where([DeliveryReportRecord::tableName() . '.record_status' => DeliveryReportRecord::STATUS_APPROVE]);
        $query->andWhere([DeliveryReport::tableName() . '.country_id' => Yii::$app->user->country->id]);
        $query->groupBy([DeliveryReportRecord::tableName() . '.order_id']);
        $query->select([DeliveryReportRecord::tableName() . '.order_id']);
        $records = $query->column();
        $totalCount = count($records);
        $offset = 0;

        while ($offset < $totalCount) {
            $batch = array_slice($records, $offset, 500);
            $offset += 500;

            $query = Order::find()->where([Order::tableName() . '.id' => $batch]);
            if (!empty($this->deliveryId)) {
                $query->with(['deliveryRequest']);
            }
            $query->andWhere(['is', Order::tableName() . '.report_status_id', null]);
            $orders = $query->all();
            if (empty($this->dateFrom) && empty($this->dateTo) && empty($this->deliveryId)) {
                $withoutReport -= count($orders);
            } else {
                foreach ($orders as $order) {
                    if ((empty($this->dateFrom) || $order->created_at >= $this->dateFrom) && (empty($this->dateTo) && $order->created_at <= $this->dateFrom) && (empty($this->deliveryId) || ($order->deliveryRequest && $order->deliveryRequest->delivery_id = $this->deliveryId))) {
                        $withoutReport--;
                    }
                }
            }
        }

        return [
            'totalCount' => $totalOrderCount,
            'withoutReportOrderCount' => $withoutReport,
        ];
    }

    /**
     *
     */
    protected function buildQuery()
    {
        $query = Order::find()->where([Order::tableName() . '.status_id' => $this->availableStatus])->byCountryId(Yii::$app->user->country->id);

        $subQuery = DeliveryReportRecord::find()->select('count(' . DeliveryReportRecord::tableName() . '.id)')->innerJoinWith('report');
        $subQuery->where(DeliveryReportRecord::tableName() . '.order_id = ' . Order::tableName() . '.id');
        $subQuery->andWhere([
            DeliveryReport::tableName() . '.country_id' => Yii::$app->user->country->id,
            DeliveryReportRecord::tableName() . '.record_status' => [
                DeliveryReportRecord::STATUS_APPROVE,
                DeliveryReportRecord::STATUS_STATUS_UPDATED
            ]
        ]);

        $subQuery->limit(1);

        $query->andWhere("({$subQuery->createCommand()->rawSql}) = 0");

        $query->orderBy(Order::tableName() . '.id');

        if (!empty($delivery_id)) {
            $query->innerJoinWith('deliveryRequest');
            $query->andWhere([DeliveryRequest::tableName() . 'delivery_id' => $delivery_id]);
        }
        if (!empty($dateFrom)) {
            $query->andFilterWhere(['>=', Order::tableName() . '.created_at', $dateFrom]);
        }

        if (!empty($dateTo)) {
            $query->andFilterWhere(['<=', Order::tableName() . '.created_at', $dateTo + 86399]);
        }

        $query->with(['deliveryRequest', 'orderProducts', 'finance', 'status', 'orderProducts.product']);
        $this->query = $query;
    }
}
