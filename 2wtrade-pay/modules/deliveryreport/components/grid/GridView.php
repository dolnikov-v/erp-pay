<?php
namespace app\modules\deliveryreport\components\grid;

use yii\grid\Column;
use Closure;
use yii\helpers\Html;

/**
 * Class GridView
 * @package app\modules\deliveryreport\components\grid
 */
class GridView extends \app\components\grid\GridView
{
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'table table-striped table-sortable'];

    /**
     * @var array
     */
    public $rowOptions = ['class' => 'tr-vertical-align-bottom tr-text-align-center'];

    /**
     * @var array
     */
    public $headerRowOptions = ['class' => 'tr-text-align-center'];

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    public function renderTableRow($model, $key, $index)
    {
        $cells = [];
        $secondRow = [];
        /* @var $column Column */
        foreach ($this->columns as $column) {
            $cells[] = $column->renderDataCell($model, $key, $index);
            if ($column instanceof ReportRecordColumn) {
                $secondRow[] = $column->renderDataSecondCell($model, $key, $index);
            }
        }

        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }
        $options['data-key'] = is_array($key) ? json_encode($key) : (string)$key;

        $content = Html::tag('tr', implode('', $cells), $options);
        $content .= Html::tag('tr');
        if(!isset($options['class'])) {
            $options['class'] = '';
        }
        $options['class'] .= ' tr-order-data tr-vertical-align-top';
        $content .= Html::tag('tr', implode('', $secondRow), $options);

        return $content;
    }
}
