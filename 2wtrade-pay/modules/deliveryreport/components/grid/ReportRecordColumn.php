<?php

namespace app\modules\deliveryreport\components\grid;

use yii\grid\DataColumn;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use Closure;

/**
 * Class ReportRecordColumn
 * @package app\modules\deliveryreport\components\grid
 */
class ReportRecordColumn extends DataColumn
{
    /**
     * @var string
     */
    public $format = 'raw';

    /**
     * @var string
     */
    public $secondAttribute = '';

    /**
     * @var bool
     */
    public $isSecondAttribute = false;

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    public function renderDataCell($model, $key, $index)
    {
        if ($this->contentOptions instanceof Closure) {
            $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
        } else {
            $options = $this->contentOptions;
        }

        if ($model instanceof DeliveryReportRecord && !$this->isSecondAttribute) {
            if (in_array($this->attribute, $model->errorFields)) {
                if (!isset($options['class'])) {
                    $options['class'] = '';
                }
                $options['class'] .= ' tr-cell-content-has-error ';
            }
        }

        return Html::tag('td', $this->renderDataCellContent($model, $key, $index), $options);
    }

    /**
     * @param $model
     * @param $key
     * @param $index
     * @return string
     */
    public function renderDataSecondCell($model, $key, $index)
    {
        if (empty($this->secondAttribute)) {
            $this->secondAttribute = $this->attribute;
        }
        $buffer = $this->attribute;
        $this->attribute = $this->secondAttribute;
        $this->isSecondAttribute = true;

        $content = $this->renderDataCell($model->convertedOrder, $key, $index);
        $this->isSecondAttribute = false;
        $this->attribute = $buffer;
        return $content;
    }
}
