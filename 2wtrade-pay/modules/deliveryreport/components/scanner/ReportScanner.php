<?php

namespace app\modules\deliveryreport\components\scanner;

use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\DeliveryReportRecordError;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinance;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use PHPExcel;
use PHPExcel_IOFactory;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class ReportScanner
 * @package app\modules\deliveryreport\components\scanner
 */
class ReportScanner
{
    /**
     * @var int
     */
    public $batchSize = 50;

    /**
     * @var PHPExcel
     */
    private $excelFile;

    /**
     * @var integer
     */
    private $reportId = null;

    /**
     * @var null | DeliveryReport
     */
    private $report;

    /**
     * @var string
     */
    private $reportType = null;

    /**
     * ReportScanner constructor.
     * @param null $report_id
     * @param null $reportType
     */
    public function __construct($report_id = null, $reportType = null)
    {
        if (!is_null($report_id)) {
            if ($report_id instanceof DeliveryReport) {
                $this->report = $report_id;
            } else {
                $this->report = DeliveryReport::findOne($report_id);
            }
            $this->reportId = $this->report->id;
            $this->reportType = $this->report->type;
        }

        if (!is_null($reportType)) {
            $this->reportType = $reportType;
        }
    }

    /**
     * @param $filename
     * @return string
     * @throws \Exception
     */
    public function checkStructureError($filename)
    {
        if (empty($this->excelFile)) {
            $this->excelFile = PHPExcel_IOFactory::load($filename);
        }
        $count = 0;

        foreach ($this->excelFile->getWorksheetIterator() as $worksheet) {

            $count++;

            $lastColumn = $worksheet->getHighestDataColumn();

            $data = $worksheet->rangeToArray("A1:{$lastColumn}1");

            if (count($data) == 0) {
                continue;
            }

            $buffer = array_map('mb_strtolower', array_map('trim', $data[0]));
            $buffer = array_map(function ($a) {
                return preg_replace('!\s+!', ' ', $a);
            }, $buffer);

            $columnNames = DeliveryReportRecord::getColumnNames($this->reportType);

            if (count($columnNames) == 0) {
                throw new \Exception(Yii::t('common', 'Не задан шаблон'));
            }

            foreach ($columnNames as $name => $val) {
                $res = array_search($name, $buffer);
                if ($res === false) {
                    throw new \Exception(Yii::t('common',
                        'Некорректная структура листа #{id} - не найден столбец с названием "{name}"',
                        ['id' => $count, 'name' => $name]));
                }
            }
        }

        return "";
    }

    /**
     * @param $filename
     * @return bool
     * @throws \Exception
     */
    public function Parse($filename)
    {
        if (!file_exists($filename)) {
            throw new \Exception(Yii::t('common', 'Файл {filename} не найден', ['filename' => $filename]));
        }

        $objReader = PHPExcel_IOFactory::createReaderForFile($filename);

        $chunkSize = 1000;
        $startRow = 1;

        $chunkFilter = new ChunkReadFilter();
        $objReader->setReadFilter($chunkFilter);
        $chunkFilter->setRows($startRow, 1);

        $this->excelFile = $objReader->load($filename);

        $this->checkStructureError($filename);

        $this->excelFile->setActiveSheetIndex(0);

        $worksheet = $this->excelFile->getActiveSheet();

        $lastColumn = $worksheet->getHighestDataColumn();

        $rows = $worksheet->rangeToArray("A1:{$lastColumn}1");

        $buffer = array_map('mb_strtolower', array_map('trim', array_shift($rows)));
        $buffer = array_map(function ($a) {
            return preg_replace('!\s+!', ' ', $a);
        }, $buffer);

        $columnNames = DeliveryReportRecord::getColumnNames($this->report->type);
        $nonRequiredColumns = DeliveryReportRecord::getNonRequiredColumnNames($this->report->type);
        $columnNames = ArrayHelper::merge($columnNames, $nonRequiredColumns);

        if (count($columnNames) == 0) {
            throw new \Exception(Yii::t('common', 'Не задан шаблон'));
        }

        $indices = [];
        foreach ($columnNames as $name => $val) {
            $index = array_search($name, $buffer);
            if ($index !== false) {
                $indices[$val] = array_search($name, $buffer);
            }
        }
        $startRow++;

        $exit = false;

        $statuses = [];
        $unBuyoutReasons = [];

        while (!$exit) {

            $chunkFilter->setRows($startRow, $chunkSize);

            $this->excelFile = $objReader->load($filename);

            foreach ($this->excelFile->getWorksheetIterator() as $worksheet) {
                $lastColumn = $worksheet->getHighestDataColumn();
                $lastRow = $worksheet->getHighestDataRow();

                if ($lastRow < $startRow + $chunkSize - 1) {
                    $exit = true;
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    for ($i = $startRow; $i <= $lastRow; $i++) {
                        $rows = $worksheet->rangeToArray("A$i:{$lastColumn}{$i}");
                        $row = array_shift($rows);
                        $model = new DeliveryReportRecord();
                        $model->delivery_report_id = $this->reportId;
                        $countEmpty = 0;

                        foreach ($indices as $attribute => $columnId) {
                            $value = trim($row[$columnId]);
                            if ($attribute == 'status') {
                                $value = trim(mb_strtolower($value));
                                if (empty($value)) {
                                    $value = "В отчете не указан статус";
                                }
                                if (!isset($statuses[$value])) {
                                    $statuses[$value] = '';
                                }
                            }
                            // из колонки с комментарием собираем массив причин недоставки
                            if ($attribute == 'comment') {
                                $value = trim($value);
                                if (!empty($value)) {
                                    if (!isset($unBuyoutReasons[$value])) {
                                        $unBuyoutReasons[$value] = '';
                                    }
                                }
                            }
                            $model->setAttribute($attribute, $value);
                            if (empty($value)) {
                                $countEmpty++;
                            }
                        }

                        if ($countEmpty == count($indices)) {
                            continue;
                        }

                        if (!$model->save()) {
                            $dbErrors = $model->errors;
                            $errorMsg = "";
                            foreach ($dbErrors as $error) {
                                $errorMsg .= implode("\n", $error);
                            }
                            throw new \Exception(Yii::t('common', 'Произошла ошибка при записи строк.{values}',
                                ['values' => "\n" . $errorMsg]));
                        }
                    }

                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }

            $startRow += $chunkSize;

            $this->excelFile->disconnectWorksheets();
        }


        if (!empty($statuses)) {
            $this->report->statuses = $statuses;
        }

        if (!empty($unBuyoutReasons)) {

            // причины у нас в справочике в базе
            $unBuyoutReasonMapping = ArrayHelper::map(UnBuyoutReasonMapping::find()
                ->asArray()
                ->all(), 'foreign_reason', 'reason_id');

            foreach ($unBuyoutReasons as $reasonKey => $reasonValue) {
                // если у нас есть такие причины в базе, то замаппим их сразу
                $unBuyoutReasons[$reasonKey] = $unBuyoutReasonMapping[$reasonKey] ?? '';
            }
        }
        $this->report->unBuyoutReasons = $unBuyoutReasons;

        $this->report->save();

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function Check()
    {
        if (is_null($this->report)) {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти запись об отчете.'));
        }

        $recordIds = DeliveryReportRecord::find()->where(['delivery_report_id' => $this->reportId])->select('id')->column();
        foreach (array_chunk($recordIds, 500) as $chunk) {
            DeliveryReportRecordError::deleteAll([
                'delivery_report_record_id' => $chunk
            ]);
        }

        if (empty($this->report->statuses)) {
            $statusList = $this->getStatusList();
            $statusPair = [];
            foreach ($statusList as $status) {
                $statusPair[$status] = '';
            }

            if (!empty($statusPair)) {
                $this->report->statuses = $statusPair;
                $this->report->save();
            }
        }

        $this->findDuplicateID();
        $this->findDuplicateTrack();
        $this->checkCorrectIDs();
        $this->checkCorrectnessTracks();
        $this->checkAnotherReports();
        $this->checkOrders();

        return true;
    }

    /**
     * @return mixed
     */
    protected function findDuplicateID()
    {
        $errors = [];
        $order_ids = DeliveryReportRecord::find()
            ->select(['order_id'])
            ->where(['delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                'record_status',
                DeliveryReportRecord::STATUS_DELETED,
            ])
            ->andWhere([
                '!=',
                'order_id',
                ""
            ])
            ->groupBy('order_id')
            ->having('count(id) > 1')
            ->column();

        /**
         * @var DeliveryReportRecord[] $orders
         */
        $orders = DeliveryReportRecord::find()->where([
            'order_id' => $order_ids,
            'delivery_report_id' => $this->reportId
        ])->andWhere([
            '!=',
            DeliveryReportRecord::tableName() . '.record_status',
            DeliveryReportRecord::STATUS_DELETED,
        ])->orderBy('order_id')->all();

        $buffer = [];
        $lastOrderID = '';
        $count = 0;
        $i = 0;
        do {
            $order = null;
            if ($i < count($orders)) {
                $order = $orders[$i];
            }
            $i++;
            if (empty($order) || $order->order_id != $lastOrderID) {
                $ids = array_map(function ($a) {
                    return $a->id;
                }, $buffer);
                foreach ($buffer as $record) {
                    $b = $ids;
                    unset($b[array_search($record->id, $b)]);
                    $error = [
                        'delivery_report_record_id' => $record->id,
                        'values' => json_encode(array_merge($b), JSON_UNESCAPED_UNICODE),
                        'error_id' => DeliveryReportRecord::ERROR_DUPLICATE_ORDERID,
                    ];
                    $errors[] = $error;
                    $count++;
                }
                if (!empty($order)) {
                    $buffer = [$order];
                    $lastOrderID = $order->order_id;
                }
            } else {
                $buffer[] = $order;
            }
        } while ($i <= count($orders));

        if ($errors) {
            Yii::$app->db->createCommand()
                ->batchInsert(DeliveryReportRecordError::tableName(), [
                    'delivery_report_record_id',
                    'values',
                    'error_id',
                ], $errors)->execute();
        }
        return $count;
    }

    /**
     * @return mixed
     */
    protected function findDuplicateTrack()
    {
        $errors = [];
        $order_ids = DeliveryReportRecord::find()
            ->select(['tracking'])
            ->where(['delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                'record_status',
                DeliveryReportRecord::STATUS_DELETED,
            ])
            ->andWhere([
                '!=',
                'tracking',
                ""
            ])
            ->groupBy('tracking')
            ->having('count(id) > 1')
            ->column();

        /**
         * @var DeliveryReportRecord[] $orders
         */
        $orders = DeliveryReportRecord::find()->where([
            'tracking' => $order_ids,
            'delivery_report_id' => $this->reportId
        ])->andWhere([
            '!=',
            DeliveryReportRecord::tableName() . '.record_status',
            DeliveryReportRecord::STATUS_DELETED,
        ])->orderBy('tracking')->all();
        $buffer = [];
        $lastOrderID = '';
        $count = 0;
        $i = 0;
        do {
            $order = null;
            if ($i < count($orders)) {
                $order = $orders[$i];
            }
            $i++;
            if (empty($order) || $order->tracking != $lastOrderID) {
                $ids = array_map(function ($a) {
                    return $a->id;
                }, $buffer);
                foreach ($buffer as $record) {
                    $b = $ids;
                    unset($b[array_search($record->id, $b)]);
                    $error = [
                        'delivery_report_record_id' => $record->id,
                        'error_id' => DeliveryReportRecord::ERROR_DUPLICATE_TRACK,
                        'values' => json_encode(array_merge($b), JSON_UNESCAPED_UNICODE),
                    ];
                    $errors[] = $error;
                    $count++;
                }
                if (!empty($order)) {
                    $buffer = [$order];
                    $lastOrderID = $order->tracking;
                }
            } else {
                $buffer[] = $order;
            }
        } while ($i <= count($orders));

        if ($errors) {
            Yii::$app->db->createCommand()
                ->batchInsert(DeliveryReportRecordError::tableName(), [
                    'delivery_report_record_id',
                    'error_id',
                    'values',
                ], $errors)->execute();
        }
        return $errors;
    }

    /**
     * @return mixed
     */
    protected function checkCorrectIDs()
    {
        $errors = [];
        $query = DeliveryReportRecord::find()
            ->select('id')
            ->where(['delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                'record_status',
                DeliveryReportRecord::STATUS_DELETED,
            ])->andWhere('order_id NOT REGEXP \'^-?[0-9]+$\'');


        foreach ($query->batch(1000) as $records) {
            foreach ($records as $record) {
                $errors[] = [
                    'error_id' => DeliveryReportRecord::ERROR_INCORRECT_ORDERID,
                    'delivery_report_record_id' => $record->id,
                ];
            }
        }
        if ($errors) {
            Yii::$app->db->createCommand()
                ->batchInsert(DeliveryReportRecordError::tableName(), array_keys($errors[0]), $errors)->execute();
        }
        return true;
    }

    /**
     * @return array
     */
    protected function getStatusList()
    {
        $statuses = DeliveryReportRecord::find()
            ->select('status')
            ->where(['delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                'status',
                ""
            ])->groupBy(['status'])
            ->column();

        return $statuses;
    }

    /**
     * @return mixed
     */
    protected function checkCorrectnessTracks()
    {
        $errors = [];
        $query = DeliveryReportRecord::find()
            ->select('id')
            ->where(['delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                'record_status',
                DeliveryReportRecord::STATUS_DELETED,
            ])
            ->andWhere('CHAR_LENGTH(`tracking`) < 4');

        foreach ($query->batch(1000) as $records) {
            foreach ($records as $record) {
                $errors[] = [
                    'error_id' => DeliveryReportRecord::ERROR_INCORRECT_TRACK,
                    'delivery_report_record_id' => $record->id
                ];
            }
        }

        if ($errors) {
            Yii::$app->db->createCommand()
                ->batchInsert(DeliveryReportRecordError::tableName(), array_keys($errors[0]), $errors)->execute();
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkAnotherReports()
    {
        $errors = [];
        $query = DeliveryReportRecord::find()->select([
            'record_id' => DeliveryReportRecord::tableName() . '.id',
            'report_id' => 'GROUP_CONCAT(dr2.delivery_report_id)'
        ])
            ->innerJoin(DeliveryReportRecord::tableName() . ' dr2', DeliveryReportRecord::tableName() . '.delivery_report_id != dr2.delivery_report_id and dr2.order_id = ' . DeliveryReportRecord::tableName() . '.order_id and dr2.record_status = \'' . DeliveryReportRecord::STATUS_STATUS_UPDATED . '\'')
            ->innerJoin(DeliveryReport::tableName(), 'dr2.delivery_report_id = ' . DeliveryReport::tableName() . '.id and ' . DeliveryReport::tableName() . '.type = \'' . DeliveryReport::TYPE_FINANCIAL . '\'')
            ->where(['IS NOT', DeliveryReportRecord::tableName() . '.order_id', null])->andWhere([
                '<>',
                DeliveryReportRecord::tableName() . '.order_id',
                ''
            ])
            ->andWhere([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->reportId])
            ->groupBy(DeliveryReportRecord::tableName() . '.id');
        $recordIds = $query->asArray()->all();
        foreach ($recordIds as $values) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_ORDER_FIND_IN_ANOTHER_REPORT,
                'values' => json_encode(explode(',', $values['report_id']), JSON_UNESCAPED_UNICODE),
                'delivery_report_record_id' => $values['record_id']
            ];
        }
        if ($errors) {
            Yii::$app->db->createCommand()
                ->batchInsert(DeliveryReportRecordError::tableName(), array_keys($errors[0]), $errors)->execute();
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkOrders()
    {
        $query = DeliveryReportRecord::find()
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $this->reportId])
            ->andWhere([
                '!=',
                DeliveryReportRecord::tableName() . '.record_status',
                DeliveryReportRecord::STATUS_DELETED,
            ]);
        $query->with([
            'report',
            'order',
            'firstDeliveryRequest',
            'deliveryRequest',
            'firstDeliveryRequest.order',
            'order.orderProducts',
            'firstDeliveryRequest.order.orderProducts',
            'callCenterRequest.order.orderProducts',
            'order.deliveryRequest',
            'order.financePrediction',
            'callCenterRequest.order.deliveryRequest',
            'callCenterRequest.order.financePrediction',
            'firstDeliveryRequest.order.deliveryRequest',
            'firstDeliveryRequest.order.financePrediction',
            'order.duplicateOrder.orderProducts',
            'order.duplicateOrder.deliveryRequest',
            'order.duplicateOrder.financePrediction',
            'callCenterRequest.order.duplicateOrder.orderProducts',
            'callCenterRequest.order.duplicateOrder.deliveryRequest',
            'callCenterRequest.order.duplicateOrder.financePrediction',
        ]);

        foreach ($query->batch(500) as $records) {
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = ArrayHelper::index($records, 'id');
            $notFoundedIds = [];
            $errors = [];
            foreach ($records as $record_id => $record) {
                if (empty($record->order) && empty($record->firstDeliveryRequest) && empty($record->callCenterRequest)) {
                    $notFoundedIds[] = $record_id;
                    continue;
                }
                $order = $record->order;
                $checkDuplicateOrder = true;
                if (empty($order)) {
                    if (!empty($record->firstDeliveryRequest)) {
                        $order = $record->firstDeliveryRequest->order;
                        $error = [
                            'error_id' => DeliveryReportRecord::ERROR_FIND_ORDERID_BY_TRACK,
                            'values' => [],
                            'delivery_report_record_id' => $record->id
                        ];
                        if (count($record->deliveryRequest) == 1) {
                            $error['values']['order_id'] = $record->order_id;
                        } else {
                            foreach ($record->deliveryRequest as $ship) {
                                $error['values'][] = $ship->order_id;
                            }
                        }
                        $errors[] = $error;
                        $checkDuplicateOrder = false;
                    } elseif (!empty($record->callCenterRequest)) {
                        $order = $record->callCenterRequest->order;
                    }
                }
                if (!empty($order) && $checkDuplicateOrder && $order->country_id == $record->report->country_id
                    && $order->deliveryRequest
                    && $order->deliveryRequest->delivery_id != $record->report->delivery_id
                    && $order->duplicateOrder
                    && (($order->duplicateOrder->deliveryRequest && $order->duplicateOrder->deliveryRequest->delivery_id == $record->report->delivery_id) || !$order->duplicateOrder->deliveryRequest)
                ) {
                    $order = $order->duplicateOrder;
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_FIND_ORDER_ID_BY_DUPLICATE,
                        'delivery_report_record_id' => $record->id,
                    ];
                }
                $this->compareRecord($record, $order, $errors);
            }

            if (!empty($notFoundedIds)) {
                $secondQuery = DeliveryReportRecord::find()
                    ->where([DeliveryReportRecord::tableName() . '.id' => $notFoundedIds]);
                $secondQuery->innerJoin(DeliveryRequest::tableName(), [
                    'like',
                    DeliveryRequest::tableName() . '.finance_tracking',
                    new Expression('CONCAT("%",' . DeliveryReportRecord::tableName() . '.tracking' . ', "%")')
                ]);
                $secondQuery->innerJoin(Order::tableName(),
                    [Order::tableName() . '.id' => new Expression(DeliveryRequest::tableName() . '.order_id')]);
                $secondQuery->select([
                    Order::tableName() . '.*',
                    'delivery_report_record_id' => DeliveryReportRecord::tableName() . '.id',
                    'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                    'delivery_request_created' => DeliveryRequest::tableName() . '.created_at',
                ]);
                $secondQuery->andWhere(['!=', DeliveryReportRecord::tableName() . '.tracking', '']);
                $secondQuery->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $this->report->delivery_id]);
                $secondQuery->asArray();

                $orders = $secondQuery->all();

                $orderIds = ArrayHelper::getColumn($orders, 'id');
                $secondQuery = OrderProduct::find()->where(['order_id' => $orderIds]);
                $secondQuery->select(['order_id', 'SUM(quantity) as quantity']);
                $secondQuery->groupBy(['order_id']);
                $secondQuery->asArray();
                $products = $secondQuery->all();
                $productAmount = ArrayHelper::map($products, 'order_id', 'quantity');

                $secondQuery = OrderFinance::find()->where(['order_id' => $orderIds]);
                $finances = $secondQuery->all();
                $finances = ArrayHelper::index($finances, 'order_id');

                $buffer = [];
                foreach ($orders as $order) {
                    if (!isset($buffer[$order['delivery_report_record_id']])) {
                        $buffer[$order['delivery_report_record_id']] = [];
                    }
                    $buffer[$order['delivery_report_record_id']][] = $order;
                }
                $orders = $buffer;

                $buffer = [];
                foreach ($notFoundedIds as $record_id) {
                    if (!isset($orders[$record_id])) {
                        $buffer[] = $record_id;
                        continue;
                    }
                    $order = $orders[$record_id][0];
                    $record = $records[$record_id];
                    $error = [
                        'error_id' => DeliveryReportRecord::ERROR_ORDER_FOUND_BY_FINANCE_TRACK,
                        'values' => [],
                        'delivery_report_record_id' => $record->id
                    ];
                    if (count($orders[$record_id]) == 1) {
                        $error['values']['order_id'] = $record->order_id;
                    } else {
                        foreach ($orders[$record_id] as $ship) {
                            $error['values'][] = $ship->id;
                        }
                    }
                    $errors[] = $error;
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_TRACK,
                        'delivery_report_record_id' => $record->id
                    ];

                    $this->compareRecord($record, $order, $errors);

                    if ($order['delivery_id'] != $this->report->delivery_id) {
                        $errors[] = [
                            'error_id' => DeliveryReportRecord::ERROR_FROM_ANOTHER_DELIVERY,
                            'delivery_report_record_id' => $record->id
                        ];
                    }

                    if (isset($productAmount[$order['id']]) && !empty($record->amount)) {
                        if ($productAmount[$order['id']] != $record->amount) {
                            $errors[] = [
                                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_AMOUNT,
                                'delivery_report_record_id' => $record->id
                            ];
                        }
                    }

                    if (isset($finances[$order['id']]) && !empty($record->payment_reference_number)) {
                        if ($finances[$order['id']]->payment != $record->payment_reference_number) {
                            $errors[] = [
                                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_PAYMENT_NUMBER,
                                'delivery_report_record_id' => $record->id
                            ];
                        }
                    }
                }
                $notFoundedIds = $buffer;
            }
            foreach ($notFoundedIds as $record_id) {
                $record = $records[$record_id];
                if ($orders = self::getFractionalOrderIDs($record, false)) {
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_ORDER_FIND_BY_NAME_ADDRESS_PHONE,
                        'values' => [$orders[0]->id],
                        'delivery_report_record_id' => $record->id
                    ];;
                    $this->compareRecord($record, $orders[0], $errors);
                } else {
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_ORDER_NOT_EXIST,
                        'delivery_report_record_id' => $record->id
                    ];
                }
            }

            if ($errors) {
                $errors = array_map(function ($item) {
                    $item = [
                        'error_id' => $item['error_id'],
                        'delivery_report_record_id' => $item['delivery_report_record_id'],
                        'values' => isset($item['values']) ? json_encode($item['values'], JSON_UNESCAPED_UNICODE) : null,
                    ];
                    return $item;
                }, $errors);

                foreach (array_chunk($errors, 500) as $batch) {
                    Yii::$app->db->createCommand()
                        ->batchInsert(DeliveryReportRecordError::tableName(), [
                            'error_id',
                            'delivery_report_record_id',
                            'values'
                        ], $batch)->execute();
                }
            }
        }

        return true;
    }

    /**
     * @param DeliveryReportRecord $record
     * @param Order $order
     * @param array $errors
     */
    protected function compareRecord($record, $order, &$errors)
    {
        $flag = 0;
        if (!empty($record->customer_full_name) && str_replace(' ', '',
                mb_strtolower($record->customer_full_name)) != str_replace(' ', '',
                mb_strtolower($order['customer_full_name']))
        ) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_NAME,
                'delivery_report_record_id' => $record->id,
            ];
            $flag++;
        }
        if (!empty($record->customer_phone) && str_replace(' ', '',
                mb_strtolower($record->customer_phone)) != str_replace(' ', '',
                mb_strtolower($order['customer_phone']))
        ) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_PHONE,
                'delivery_report_record_id' => $record->id,
            ];
            $flag++;
        }
        if (!empty($record->customer_address) && str_replace(' ', '',
                mb_strtolower($record->customer_address)) != str_replace(' ', '',
                mb_strtolower($order['customer_address']))
        ) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_ADDRESS,
                'delivery_report_record_id' => $record->id,
            ];
            $flag++;
        }
        if ($flag > 0) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_DATA,
                'delivery_report_record_id' => $record->id,
            ];
        }

        if ($order['price_total'] != $record->price && !empty($record->price)) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_PRICE,
                'delivery_report_record_id' => $record->id,
            ];
        }

        if ($record->price_cod > ($order['price_total'] + $order['delivery'])) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_HIGH_COD,
                'delivery_report_record_id' => $record->id,
            ];
        }

        if ($record->price_cod < ($order['price_total'] + $order['delivery'])) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_LESS_COD,
                'delivery_report_record_id' => $record->id,
            ];
        }

        $products = $record->getConvertedProducts();

        if ($products === false) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_INCORRECT_PRODUCTS_FORMAT,
                'delivery_report_record_id' => $record->id,
            ];
        } else {
            foreach ($products as $product) {
                if (empty($product['product_id'])) {
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_PRODUCT_NOT_FOUND,
                        'delivery_report_record_id' => $record->id,
                    ];
                }
            }
        }

        $checkingDates = [
            'date_created' => DeliveryReportRecord::ERROR_INCORRECT_DATE_CREATED,
            'date_approve' => DeliveryReportRecord::ERROR_INCORRECT_DATE_APPROVE,
            'date_payment' => DeliveryReportRecord::ERROR_INCORRECT_DATE_PAYMENT,
            'date_return' => DeliveryReportRecord::ERROR_INCORRECT_DATE_RETURN,
            'delivery_time_from' => DeliveryReportRecord::ERROR_INCORRECT_DELIVERY_TIME_FROM,
        ];
        $timezone = new \DateTimeZone($this->report->country->timezone->timezone_id);
        foreach ($checkingDates as $attribute => $errorType) {
            $dateFormat = empty(trim($this->report->date_format)) ? $this->report->delivery->date_format : trim($this->report->date_format);
            $trimmedDate = preg_replace("/(^\s+)|(\s+$)/us", "", $record[$attribute]);
            if (empty($trimmedDate)) {
                continue;
            }
            $date = \DateTime::createFromFormat($dateFormat, $trimmedDate, $timezone);
            if ($date && $date->getLastErrors()['warning_count'] == 0 && $date->getLastErrors()['error_count'] == 0) {
                $beginTime = new \DateTime();
                $beginTime->setTimestamp($date->getTimestamp());
                $beginTime = new \DateTime($beginTime->format('H:i:s'));
                $now = new \DateTime('now');

                if (abs($now->getTimestamp() - $beginTime->getTimestamp()) <= 1) {
                    $beginTime->setTimestamp($date->getTimestamp());
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s',
                        $beginTime->format('Y-m-d 11:00:00')); // Ставим 14:00 по МСК
                }
            }
            if (!$date || $date->getLastErrors()['warning_count'] > 0 || $date->getLastErrors()['error_count'] > 0 || $date->getTimestamp() > strtotime('+1day') || (isset($order['delivery_request_created_at']) && $date->getTimestamp() < $order['delivery_request_created_at']) || ($order instanceof Order && !empty($order->deliveryRequest) && $date->getTimestamp() < strtotime("-1 day", $order->deliveryRequest->created_at))) {
                $errors[] = [
                    'error_id' => $errorType,
                    'delivery_report_record_id' => $record->id,
                ];
            }
        }

        if ($order['country_id'] != $this->report->country_id) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_FROM_ANOTHER_COUNTRY,
                'delivery_report_record_id' => $record->id,
            ];
        }

        if ($this->report->type == DeliveryReport::TYPE_FINANCIAL && OrderStatus::isFinalDeliveryStatus($order['status_id'])) {
            $errors[] = [
                'error_id' => DeliveryReportRecord::ERROR_ORDER_IN_FINAL_STATUS,
                'delivery_report_record_id' => $record->id,
            ];
        }

        if ($statusId = $record->getStatusPair()) {
            if ((in_array($statusId, OrderStatus::getNotBuyoutList()) && in_array($order->status_id, OrderStatus::getBuyoutList())) || (in_array($statusId, OrderStatus::getBuyoutList()) && in_array($order->status_id, OrderStatus::getNotBuyoutList()))) {
                $errors[] = [
                    'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_STATUS,
                    'delivery_report_record_id' => $record->id,
                ];
            }
        }

        if ($order instanceof Order) {
            if ($order->deliveryRequest) {
                $financeTracks = explode(',',
                    mb_strtolower(str_replace(' ', '', $order->deliveryRequest->finance_tracking)));

                if (str_replace(' ', '', mb_strtolower($record->tracking)) != str_replace(' ', '',
                        mb_strtolower($order->deliveryRequest->tracking)) && !empty($record->tracking)
                ) {
                    $error = [
                        'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_TRACK,
                        'delivery_report_record_id' => $record->id,
                        'values' => [],
                    ];

                    if (!in_array($record->tracking, $financeTracks)) {
                        $condition = [];

                        if (!empty($record->order_id)) {
                            $condition[] = ['order_id' => $record->order_id];
                        }

                        $condition[] = ['LOWER(tracking)' => mb_strtolower($record->tracking)];
                        if (count($condition) > 1) {
                            array_unshift($condition, 'or');
                        } else {
                            $condition = $condition[0];
                        }

                        $tracks = DeliveryRequest::find()->select([
                            'order_id',
                            'tracking'
                        ])->where($condition)->all();

                        foreach ($tracks as $track) {
                            if ($track->order_id == $record->order_id) {
                                if (!empty($track->tracking)) {
                                    $error['values']['tracking'] = $track->tracking;
                                }
                            } else {
                                $error['values']['order_id'][] = $track->order_id;
                            }
                        }
                    }
                    $errors[] = $error;
                }

                if ($order->deliveryRequest->delivery_id != $this->report->delivery_id) {
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_FROM_ANOTHER_DELIVERY,
                        'delivery_report_record_id' => $record->id,
                    ];
                }
            }
            if (!empty($record->amount)) {
                $amount = 0;

                if (!empty($order->orderProducts)) {
                    foreach ($order->orderProducts as $product) {
                        $amount += $product->quantity;
                    }
                }
                if ($amount != $record->amount) {
                    $error = [
                        'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_AMOUNT,
                        'delivery_report_record_id' => $record->id,
                    ];
                    $errors[] = $error;
                }
            }
            if ($order->finance && !empty($record->payment_reference_number)) {
                if ($order->finance->payment != $record->payment_reference_number) {
                    $errors[] = [
                        'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_PAYMENT_NUMBER,
                        'delivery_report_record_id' => $record->id,
                    ];
                }
            }
            if ($order->financePrediction) {
                $checkingColumns = [
                    DeliveryReportRecord::COLUMN_PRICE_STORAGE => OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                    DeliveryReportRecord::COLUMN_PRICE_PACKING => OrderFinancePrediction::COLUMN_PRICE_PACKING,
                    DeliveryReportRecord::COLUMN_PRICE_PACKAGE => OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                    DeliveryReportRecord::COLUMN_PRICE_ADDRESS_CORRECTION => OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    DeliveryReportRecord::COLUMN_PRICE_DELIVERY => OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    DeliveryReportRecord::COLUMN_PRICE_REDELIVERY => OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                    DeliveryReportRecord::COLUMN_PRICE_DELIVERY_BACK => OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                    DeliveryReportRecord::COLUMN_PRICE_DELIVERY_RETURN => OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                    DeliveryReportRecord::COLUMN_PRICE_COD_SERVICE => OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                    DeliveryReportRecord::COLUMN_PRICE_FULFILMENT => OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    DeliveryReportRecord::COLUMN_PRICE_VAT => OrderFinancePrediction::COLUMN_PRICE_VAT,
                ];

                $costErrors = DeliveryReportRecord::costsErrors();
                $addCommonError = false;
                foreach ($checkingColumns as $reportColumn => $orderColumn) {
                    if (($record->$reportColumn != '' && !is_null($record->$reportColumn) && round($record->$reportColumn, 2) != round($order->financePrediction->$orderColumn, 2)) || (empty($record->$reportColumn) && !empty($order->financePrediction->$orderColumn)) || (!empty($record->$reportColumn) && empty($order->financePrediction->$orderColumn))) {

                        if (!$addCommonError) {
                            $errors[] = [
                                'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_COSTS,
                                'delivery_report_record_id' => $record->id,
                            ];
                        }

                        if (isset($costErrors[$reportColumn])) {
                            $errors[] = [
                                'error_id' => $costErrors[$reportColumn],
                                'delivery_report_record_id' => $record->id,
                            ];
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param DeliveryReportRecord $record
     * @param bool $onlyIds
     * @return Order[] | array
     */
    public static function getFractionalOrderIDs($record, $onlyIds = true)
    {
        $values = [];
        if (trim($record->customer_phone) != '' || trim($record->customer_address) != '' || trim($record->customer_full_name) != '') {

            $condition = [];

            $flag = false;

            if (trim($record->customer_full_name) != '') {
                $words = preg_split('/[\s,.\/()\[\]]/', mb_strtolower(trim($record->customer_full_name)));
                $bufferCond = [];
                foreach ($words as $word) {
                    if (strlen($word) > 1) {
                        $bufferCond[] = ['like', 'LOWER(' . Order::tableName() . '.customer_full_name)', "$word"];
                    }
                }
                if (count($bufferCond) > 1) {
                    array_unshift($bufferCond, 'or');
                    $condition[] = $bufferCond;
                    $flag = true;
                } elseif (count($bufferCond) > 0) {
                    $condition[] = array_shift($bufferCond);
                    $flag = true;
                }

            }
            if (trim($record->customer_address) != '') {
                $words = preg_split('/[\s,.\/()\[\]]/', mb_strtolower(trim($record->customer_address)));
                $bufferCond = [];
                foreach ($words as $word) {
                    if (strlen($word) > 1) {
                        $bufferCond[] = ['like', 'LOWER(' . Order::tableName() . '.customer_address)', "$word"];
                    }
                }
                if (count($bufferCond) > 1) {
                    array_unshift($bufferCond, 'or');
                    $condition[] = $bufferCond;
                } elseif (count($bufferCond) > 0) {
                    $condition[] = array_shift($bufferCond);
                }
            }
            if (trim($record->customer_phone) != '') {
                $words = preg_split('/[\s,.\/()\[\]]/', mb_strtolower(trim($record->customer_phone)));
                $bufferCond = [];
                foreach ($words as $word) {
                    if (strlen($word) > 1) {
                        $bufferCond[] = ['like', Order::tableName() . '.customer_phone', "$word"];
                    }
                }
                $bufferCond[] = [
                    'and',
                    ['>=', 'LENGTH(' . Order::tableName() . '.customer_phone)', 5],
                    "'{$record->customer_phone}' like CONCAT('%'," . Order::tableName() . ".customer_phone, '%')",
                ];
                if (count($bufferCond) > 1) {
                    array_unshift($bufferCond, 'or');
                    $condition[] = $bufferCond;
                    $flag = true;
                } elseif (count($bufferCond) > 0) {
                    $condition[] = array_shift($bufferCond);
                    $flag = true;
                }
            }

            if (count($condition) > 0 && !empty($record->relatedOrder)) {
                $condition[] = ['!=', Order::tableName() . '.id', $record->relatedOrder->id];
            }

            if (count($condition) > 1) {
                array_unshift($condition, 'and');
            } elseif (count($condition) == 1) {
                $condition = array_shift($condition);
            }

            if (count($condition) > 0 && $flag) {
                $query = Order::find()->where($condition)->limit(50);
                // Сортируем сначала по ид курьерки, затем по ид страны, затем уже по ид заказов, в таком случае наиболее подходящий заказ будет всегда сверху
                $query->orderBy([
                    "FIELD(" . DeliveryRequest::tableName() . ".delivery_id, {$record->report->delivery_id})" => SORT_DESC,
                    "FIELD(" . Order::tableName() . ".country_id, {$record->report->country_id})" => SORT_DESC,
                    "FIELD(" . Order::tableName() . ".customer_phone, '" . trim($record->customer_phone) . "')" => SORT_DESC,
                    Order::tableName() . '.id' => SORT_DESC
                ]);
                $query->joinWith(['deliveryRequest']);
                if ($onlyIds) {
                    $query->select([Order::tableName() . '.id']);
                } else {
                    $query->with(['deliveryRequest', 'finance', 'orderProducts']);
                }
                if ($onlyIds) {
                    $values = $query->column();
                } else {
                    $values = $query->all();
                }
            }

        }

        return $values;
    }

    /**
     * @param DeliveryReportRecord $record
     * @return boolean
     */
    public function checkRecord($record)
    {
        $errors = [];
        /**
         * @var Order $order
         */
        $order = $record->order;
        if (empty($order) && !empty(trim($record->tracking))) {
            if ($record->firstDeliveryRequest && $record->firstDeliveryRequest->order) {
                $order = $record->firstDeliveryRequest->order;
                $error = ['type' => DeliveryReportRecord::ERROR_FIND_ORDERID_BY_TRACK, 'values' => []];
                if (count($record->deliveryRequest) == 1) {
                    $error['values']['order_id'] = $record->order_id;
                } else {
                    foreach ($record->deliveryRequest as $ship) {
                        $error['values'][] = $ship->order_id;
                    }
                }
                $errors[] = $error;
            } else {
                $tracking = trim($record->tracking);
                $query = DeliveryRequest::find()->where(['like', 'finance_tracking', "{$tracking}"]);
                $query->select(['order_id']);
                $orders = $query->column();
                if (!empty($orders)) {
                    $order = Order::findOne($orders[0]);
                    $error = ['type' => DeliveryReportRecord::ERROR_ORDER_FOUND_BY_FINANCE_TRACK, 'values' => []];
                    if (count($orders) == 1) {
                        $error['values']['order_id'] = $record->order_id;
                    } else {
                        foreach ($orders as $ship) {
                            $error['values'][] = $ship->id;
                        }
                    }
                    $errors[] = $error;
                } elseif ($record->callCenterRequest && $record->callCenterRequest->order) {
                    $order = $record->callCenterRequest->order;
                } else {
                    $errors[] = ['type' => DeliveryReportRecord::ERROR_ORDER_NOT_EXIST, 'values' => []];
                }
            }
        }

        if (trim($record->order_id) != '') {
            $query = DeliveryReportRecord::find()->select(['id'])
                ->where([
                    'delivery_report_id' => $this->reportId,
                    'order_id' => trim($record->order_id)
                ])
                ->andWhere(['!=', "id", $record->id])
                ->andWhere([
                    '!=',
                    DeliveryReportRecord::tableName() . '.record_status',
                    DeliveryReportRecord::STATUS_DELETED,
                ]);

            if ($query->count() > 0) {
                $error = ['type' => DeliveryReportRecord::ERROR_DUPLICATE_ORDERID, 'values' => []];
                foreach ($query->batch(1000) as $records) {
                    foreach ($records as $item) {
                        $error['values'][] = $item->id;
                    }
                }
                $errors[] = $error;
            }
        }

        if (!preg_match("/^-?[0-9]+$/", $record->order_id)) {
            $errors[] = ['type' => DeliveryReportRecord::ERROR_INCORRECT_ORDERID, 'values' => []];
        }

        if (strlen(trim($record->tracking)) > 3) {
            $query = DeliveryReportRecord::find()->select(['id'])
                ->where([
                    'delivery_report_id' => $record->delivery_report_id,
                    'tracking' => trim($record->tracking),
                ])
                ->andWhere(['!=', 'id', $record->id])
                ->andWhere([
                    '!=',
                    DeliveryReportRecord::tableName() . '.record_status',
                    DeliveryReportRecord::STATUS_DELETED,
                ]);
            if ($query->count() > 0) {
                $error = ['type' => DeliveryReportRecord::ERROR_DUPLICATE_TRACK, 'values' => []];
                foreach ($query->batch(1000) as $records) {
                    foreach ($records as $item) {
                        $error['values'][] = $item->id;
                    }
                }
                $errors[] = $error;
            }
        } else {
            $errors[] = ['type' => DeliveryReportRecord::ERROR_INCORRECT_TRACK, 'values' => []];
        }

        if (!empty($order)) {

            $flag = 0;
            if (!empty($record->customer_full_name) && str_replace(' ', '',
                    mb_strtolower($record->customer_full_name)) != str_replace(' ', '',
                    mb_strtolower($order['customer_full_name']))
            ) {
                $errors[] = [
                    'type' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_NAME,
                    'values' => []
                ];
                $flag++;
            }
            if (!empty($record->customer_phone) && str_replace(' ', '',
                    mb_strtolower($record->customer_phone)) != str_replace(' ', '',
                    mb_strtolower($order['customer_phone']))
            ) {
                $errors[] = [
                    'type' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_PHONE,
                    'values' => []
                ];
                $flag++;
            }
            if (!empty($record->customer_address) && str_replace(' ', '',
                    mb_strtolower($record->customer_address)) != str_replace(' ', '',
                    mb_strtolower($order['customer_address']))
            ) {
                $errors[] = [
                    'type' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_ADDRESS,
                    'values' => []
                ];
                $flag++;
            }
            if ($flag > 0) {
                $error = ['type' => DeliveryReportRecord::ERROR_NOT_EQUAL_ORDER_DATA, 'values' => []];
                $errors[] = $error;
            }

            if ($order['price_total'] != $record->price && !empty($record->price)) {
                $error = ['type' => DeliveryReportRecord::ERROR_NOT_EQUAL_PRICE, 'values' => []];
                $errors[] = $error;
            }

            if ((!empty($record->price_cod) && ($order['price_total'] + $order['delivery']) != $record->price_cod) || $record->price_cod == 0) {
                $error = ['type' => DeliveryReportRecord::ERROR_NOT_EQUAL_COD, 'values' => []];
                $errors[] = $error;
            }

            if ($record->price_cod > ($order['price_total'] + $order['delivery'])) {
                $errors[] = [
                    'error_id' => DeliveryReportRecord::ERROR_HIGH_COD,
                    'delivery_report_record_id' => $record->id,
                ];
            }

            if ($record->price_cod < ($order['price_total'] + $order['delivery'])) {
                $errors[] = [
                    'error_id' => DeliveryReportRecord::ERROR_LESS_COD,
                    'delivery_report_record_id' => $record->id,
                ];
            }

            $checkingDates = [
                'date_created' => DeliveryReportRecord::ERROR_INCORRECT_DATE_CREATED,
                'date_approve' => DeliveryReportRecord::ERROR_INCORRECT_DATE_APPROVE,
                'date_payment' => DeliveryReportRecord::ERROR_INCORRECT_DATE_PAYMENT,
                'date_return' => DeliveryReportRecord::ERROR_INCORRECT_DATE_RETURN,
                'delivery_time_from' => DeliveryReportRecord::ERROR_INCORRECT_DELIVERY_TIME_FROM,
            ];
            $timezone = new \DateTimeZone($this->report->country->timezone->timezone_id);
            foreach ($checkingDates as $attribute => $errorType) {
                $dateFormat = empty(trim($this->report->date_format)) ? $this->report->delivery->date_format : trim($this->report->date_format);
                $trimmedDate = preg_replace("/(^\s+)|(\s+$)/us", "", $record[$attribute]);
                if (empty($trimmedDate)) {
                    continue;
                }
                $date = \DateTime::createFromFormat($dateFormat, $trimmedDate, $timezone);
                if (!$date || $date->getLastErrors()['warning_count'] > 0 || $date->getLastErrors()['error_count'] > 0 || $date->getTimestamp() > strtotime('+1day') || (!empty($order->deliveryRequest) && $date->getTimestamp() < $order->deliveryRequest->created_at)) {
                    $errors[] = ['type' => $errorType, 'values' => []];
                }
            }

            if ($order->deliveryRequest) {
                $financeTracks = explode(',',
                    mb_strtolower(str_replace(' ', '', $order->deliveryRequest->finance_tracking)));

                if (str_replace(' ', '', mb_strtolower($record->tracking)) != str_replace(' ', '',
                        mb_strtolower($order->deliveryRequest->tracking))
                ) {
                    $error = [
                        'type' => DeliveryReportRecord::ERROR_NOT_EQUAL_TRACK,
                        'values' => []
                    ];

                    if (!in_array($record->tracking, $financeTracks)) {
                        $condition = [];

                        if (!empty($record->order_id)) {
                            $condition[] = ['order_id' => $record->order_id];
                        }
                        if (empty($record->tracking)) {
                            $condition[] = ['LOWER(tracking)' => mb_strtolower($record->tracking)];
                        }
                        if (count($condition) > 1) {
                            array_unshift($condition, 'or');
                        } else {
                            $condition = $condition[0];
                        }

                        $tracks = DeliveryRequest::find()->select([
                            'order_id',
                            'tracking'
                        ])->where($condition)->all();

                        foreach ($tracks as $track) {
                            if ($track->order_id == $record->order_id) {
                                if (!empty($track->tracking)) {
                                    $error['values']['tracking'] = $track->tracking;
                                }
                            } else {
                                $error['values']['order_id'][] = $track->order_id;
                            }
                        }
                    }
                    $errors[] = $error;
                }

                if ($order->deliveryRequest->delivery_id != $this->report->delivery_id) {
                    $errors[] = [
                        'type' => DeliveryReportRecord::ERROR_FROM_ANOTHER_DELIVERY,
                        'values' => []
                    ];
                }
            }

            if (!empty($record->amount)) {
                $amount = 0;

                if (!empty($order->orderProducts)) {
                    foreach ($order->orderProducts as $product) {
                        $amount += $product->quantity;
                    }
                }
                if ($amount != $record->amount) {
                    $error = ['type' => DeliveryReportRecord::ERROR_NOT_EQUAL_AMOUNT, 'values' => []];
                    $errors[] = $error;
                }
            }
            if ($order->finance && !empty($record->payment_reference_number)) {
                if ($order->finance->payment != $record->payment_reference_number) {
                    $errors[] = [
                        'type' => DeliveryReportRecord::ERROR_NOT_EQUAL_PAYMENT_NUMBER,
                        'values' => ''
                    ];
                }
            }

            if ($order->country_id != $this->report->country_id) {
                $errors[] = [
                    'type' => DeliveryReportRecord::ERROR_FROM_ANOTHER_COUNTRY,
                    'values' => []
                ];
            }
        }

        $errors = array_map(function ($item) use ($record) {
            return [
                'delivery_report_record_id' => $record->id,
                'error_id' => $item['type'],
                'values' => json_encode($item['values'], JSON_UNESCAPED_UNICODE),
            ];
        }, $errors);

        DeliveryReportRecordError::deleteAll(['delivery_report_record_id' => $record->id]);
        Yii::$app->db->createCommand()
            ->batchInsert(DeliveryReportRecordError::tableName(), [
                'delivery_report_record_id',
                'error_id',
                'values'
            ], $errors)->execute();

        return true;
    }
}
