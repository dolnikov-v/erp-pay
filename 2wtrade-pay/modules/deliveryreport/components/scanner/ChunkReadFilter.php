<?php

namespace app\modules\deliveryreport\components\scanner;

use PHPExcel_Reader_IReadFilter;

/**
 * Class ChunkReadFilter
 * @package app\modules\deliveryreport\components\scanner
 */
class ChunkReadFilter implements PHPExcel_Reader_IReadFilter
{
    /**
     * @var int
     */
    private $_startRow = 0;
    /**
     * @var int
     */
    private $_endRow = 0;

    /**
     * @param $startRow
     * @param $chunkSize
     */
    public function setRows($startRow, $chunkSize)
    {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }

    /**
     * @param String $column
     * @param \Row $row
     * @param string $worksheetName
     * @return bool
     */
    public function readCell($column, $row, $worksheetName = '')
    {
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}
