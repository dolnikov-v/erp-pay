<?php

namespace app\modules\deliveryreport\controllers;

use app\abstracts\rest\AnswerInterface;
use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\jobs\DeliveryReportDelete;
use app\models\Currency;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\delivery\components\control\DeliveryControl;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\deliveryreport\models\DeliveryReportCosts;
use app\modules\deliveryreport\models\DeliveryReportCurrency;
use app\modules\deliveryreport\models\DeliveryReportCurrencyRate;
use app\modules\deliveryreport\models\DeliveryReportRecordError;
use app\modules\deliveryreport\models\PaymentCurrencyRate;
use app\modules\deliveryreport\models\PaymentDeliveryReport;
use app\models\rest\Answer;
use app\modules\order\models\Lead;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderProduct;
use app\modules\salary\models\Penalty as CallCenterPenalty;
use app\modules\salary\models\PenaltyType as CallCenterPenaltyType;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\salary\models\search\PersonSearch as CallCenterPersonSearch;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\components\exporter\ExporterRecordFactory;
use app\modules\deliveryreport\components\exporter\WithoutReportOrderExporter;
use app\modules\deliveryreport\components\scanner\ReportScanner;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRate;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\deliveryreport\models\search\DeliveryReportRecordSearch;
use app\modules\deliveryreport\models\search\DeliveryReportSearch;
use app\modules\deliveryreport\models\UploadForm;
use app\modules\deliveryreport\widgets\PaymentList;
use app\modules\deliveryreport\widgets\ShowerColumns;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinance;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class ReportController
 * @package app\modules\deliveryreport\controllers
 */
class ReportController extends Controller
{
    //остаток с платёжки
    public $balance = 0;

    const WIDGET_PRECISSION = 2;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'set-approve',
                    'unset-approve',
                    'delete-records',
                    'update-order-tracking',
                    'update-order-data',
                    'update-order-status',
                    'set-payment-id',
                    'set-order-prices',
                    'check-report-status',
                    'without-report-order-count',
                    'export-without-report-orders',
                    'create',
                    'add-payment',
                    'remove-payment',
                    'change-costs',
                    'create-duplicate-order',
                    'get-rate-for-currency-date',
                    'create-pretension',
                    'remove-pretension',
                    'set-currencies',
                    'change-additional-cost',
                    'delete-additional-cost',
                ],
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveryReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $reports = Yii::$app->request->post('data');
            $response = [];
            $shouldUpdate = false;
            foreach ($reports as $report_id => $status) {
                /**
                 * @var DeliveryReport $report
                 */
                $report = DeliveryReport::findOne($report_id);
                if (!empty($report) && $report->status != $status) {
                    $shouldUpdate = true;
                    break;
                }
            }
            if ($shouldUpdate) {
                $response['content'] = $this->renderPartial('_index-content', $data);
            }
            $response['status'] = 'success';
            return $response;
        }

        $withoutReportExporter = new WithoutReportOrderExporter();
        $withoutReportOrders = $withoutReportExporter->getOrderCount();

        $data['withoutReportOrders'] = $withoutReportOrders;

        return $this->render('index', $data);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionView($id)
    {
        ini_set('memory_limit', '1024M');
        $report = $this->findModel($id);
        $searchModel = new DeliveryReportRecordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        if ($postStatus = Yii::$app->request->post('status')) {
            $statusLabels = $report->statuses;
            foreach ($postStatus as $key => $val) {
                $statusLabels[base64_decode($key)] = $val;
            }
            $json = json_encode($statusLabels);
            if ($report->status_pair != $json) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $report->statuses = $statusLabels;
                    foreach ($statusLabels as $statusForeignName => $statusId) {
                        DeliveryReportRecord::updateAll([
                            'status_id' => $statusId
                        ], [
                            'status' => $statusForeignName,
                            'delivery_report_id' => $report->id
                        ]);
                    }
                    if ($report->save(true, ['status_pair'])) {
                        $transaction->commit();
                    } else {
                        throw new \Exception(Yii::t('common', 'Произошла ошибка при сохранении статусов: {error}.', ['error' => $report->getFirstErrorAsString()]));
                    }
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($e->getMessage());
                }
            }
            return $this->redirect(array_merge(Yii::$app->request->queryParams, ['view', 'id' => $id]));
        }

        // причины у нас в справочике в базе
        $unBuyoutReasonMapping = ArrayHelper::map(UnBuyoutReasonMapping::find()
            ->asArray()
            ->all(), 'foreign_reason', 'reason_id');

        if ($postUnBuyoutReason = Yii::$app->request->post('unbuyout_reason')) {

            $unBuyoutReasonLabels = $report->unBuyoutReasons;
            foreach ($postUnBuyoutReason as $key => $val) {
                $unBuyoutReasonLabels[base64_decode($key)] = $val;
            }
            $json = json_encode($unBuyoutReasonLabels);

            if ($report->unbuyout_reason_pair != $json) {
                $report->unBuyoutReasons = $unBuyoutReasonLabels;

                // при сохранении мапиинга, проверим есть ли у нас такая связь, если нет запишем в справочник
                foreach ($unBuyoutReasonLabels as $key => $val) {
                    if (!empty($val)) {
                        // не было такой причины раньше, добавим в справочник базы
                        if (!isset($unBuyoutReasonMapping[$key])) {
                            $new = new UnBuyoutReasonMapping();
                            $new->foreign_reason = (string)$key;
                            $new->reason_id = $val;
                            if (!$new->save()) {
                                Yii::$app->notifier->addNotificationsByModel($new);
                            }
                        }
                    }
                }

                if (!$report->save(true, ['unbuyout_reason_pair'])) {
                    Yii::$app->notifier->addNotificationsByModel($report);
                }

            }
            return $this->redirect(array_merge(Yii::$app->request->queryParams, ['view', 'id' => $id]));
        }

        if ($postFormats = Yii::$app->request->post('formats')) {
            if (isset($postFormats['date'])) {
                $report->date_format = $postFormats['date'];
                $report->save(true, ['date_format']);
                return $this->redirect(array_merge(Yii::$app->request->queryParams, ['view', 'id' => $id]));
            }
        }

        $showerColumns = new ShowerColumns();

        $data = [
            'model' => $report,
            'dataProvider' => $dataProvider,
            'modelSearch' => $searchModel,
            'showerColumns' => $showerColumns,
            'statuses' => $report->statuses ? $report->statuses : [],
            'unBuyoutReasons' => $report->unBuyoutReasons ?? []
        ];

        if ($exporterType = Yii::$app->request->get('export')) {
            $exporterConfig = [
                'dataProvider' => $dataProvider,
                'showerColumns' => $showerColumns,
                'report' => $report
            ];

            $exporter = ExporterRecordFactory::build($exporterType, $exporterConfig);
            $exporter->sendFile();
        }


        // если не считались причины в момент чтения из файла, прочтем сейчас из строк базы, потом можно будет это удалить
        if (is_null($report->unBuyoutReasons)) {
            $query = DeliveryReportRecord::find()->where(['delivery_report_id' => $report->id]);
            $query->andWhere(['is not', 'comment', null]);
            $query->andWhere(['<>', 'comment', '']);
            $unBuyoutReasons = [];
            foreach ($query->all() as $record) {
                /** @var $record DeliveryReportRecord */
                if (!empty(trim($record->comment))) {
                    if (!isset($unBuyoutReasons[$record->comment])) {
                        $unBuyoutReasons[$record->comment] = '';
                    }
                }
            }

            foreach ($unBuyoutReasons as $reasonKey => $reasonValue) {
                // если у нас есть такие причины в базе, то замаппим их сразу
                $unBuyoutReasons[$reasonKey] = $unBuyoutReasonMapping[$reasonKey] ?? '';
            }
            $data['unBuyoutReasons'] = $unBuyoutReasons;
        }

        $query = DeliveryReportRecord::find()->where(['delivery_report_id' => $report->id]);
        $query->andWhere(['not in', 'record_status', [DeliveryReportRecord::STATUS_DELETED]]);
        $query->groupBy([DeliveryReportRecord::tableName() . '.record_status']);
        $query->select(['count_n' => 'COUNT(1)', 'status' => DeliveryReportRecord::tableName() . '.record_status']);
        $statuses = $query->asArray()->all();
        $statuses = ArrayHelper::map($statuses, 'status', 'count_n');
        $data['statusCount'] = $statuses;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $column = Yii::$app->request->post('column');
            $choose = Yii::$app->request->post('choose');

            $showerColumns->toggleColumn($column, $choose);

            return [
                'content' => $this->renderPartial('view-content', $data),
            ];
        }

        $finInfo = [];
        if ($report->type == DeliveryReport::TYPE_FINANCIAL) {

            $finStatus = [];
            if ($report->statuses) {
                foreach ($report->statuses as $status => $statusId) {
                    if (in_array($statusId, OrderStatus::getBuyoutList())) {
                        $finStatus[] = $status;
                    }
                }
            }

            $selectingColumns = [
                'price_cod' => [
                    'report' => new Expression(empty($finStatus) ? 0 : 'SUM(CASE WHEN ' . DeliveryReportRecord::tableName() . '.status in ("' . implode('","', $finStatus) . '") THEN ' . DeliveryReportRecord::tableName() . '.price_cod ELSE 0 END)'),
                    'db' => new Expression('SUM(CASE WHEN ' . Order::tableName() . '.status_id in ("' . implode('","', OrderStatus::getBuyoutList()) . '") THEN COALESCE(' . OrderFinancePrediction::tableName() . '.price_cod, ' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery) ELSE 0 END)'),
                ],
                'price_storage' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_storage, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_storage, 2))'),
                ],
                'price_fulfilment' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_fulfilment, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_fulfilment, 2))'),
                ],
                'price_packing' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_packing, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_packing, 2))'),
                ],
                'price_package' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_package, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_package, 2))'),
                ],
                'price_address_correction' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_address_correction, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_address_correction, 2))'),
                ],
                'price_delivery' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_delivery, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_delivery, 2))'),
                ],
                'price_redelivery' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_redelivery, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_redelivery, 2))'),
                ],
                'price_delivery_back' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_delivery_back, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_delivery_back, 2))'),
                ],
                'price_delivery_return' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_delivery_return, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_delivery_return, 2))'),
                ],
                'price_cod_service' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_cod_service, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_cod_service, 2))'),
                ],
                'price_vat' => [
                    'report' => new Expression('SUM(ROUND(' . DeliveryReportRecord::tableName() . '.price_vat, 2))'),
                    'db' => new Expression('SUM(ROUND(' . OrderFinancePrediction::tableName() . '.price_vat, 2))'),
                ],
                'price_cod_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_cod_currency_id'),
                ],
                'price_storage_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_storage_currency_id'),
                ],
                'price_fulfilment_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_fulfilment_currency_id'),
                ],
                'price_packing_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_packing_currency_id'),
                ],
                'price_package_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_package_currency_id'),
                ],
                'price_address_correction_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_address_correction_currency_id'),
                ],
                'price_delivery_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_delivery_currency_id'),
                ],
                'price_redelivery_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_redelivery_currency_id'),
                ],
                'price_delivery_back_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_delivery_back_currency_id'),
                ],
                'price_delivery_return_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_delivery_return_currency_id'),
                ],
                'price_cod_service_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_cod_service_currency_id'),
                ],
                'price_vat_currency_id' => [
                    'db' => new Expression(OrderFinancePrediction::tableName() . '.price_vat_currency_id'),
                ],
            ];
            $selectingColumnsFact = [
                'price_cod' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_cod, 2))'),
                ],
                'price_storage' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_storage, 2))'),
                ],
                'price_fulfilment' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_fulfilment, 2))'),
                ],
                'price_packing' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_packing, 2))'),
                ],
                'price_package' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_package, 2))'),
                ],
                'price_address_correction' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_address_correction, 2))'),
                ],
                'price_delivery' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_delivery, 2))'),
                ],
                'price_redelivery' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_redelivery, 2))'),
                ],
                'price_delivery_back' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_delivery_back, 2))'),
                ],
                'price_delivery_return' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_delivery_return, 2))'),
                ],
                'price_cod_service' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_cod_service, 2))'),
                ],
                'price_vat' => [
                    'fact' => new Expression('SUM(ROUND(' . OrderFinanceFact::tableName() . '.price_vat, 2))'),
                ],
                'price_cod_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_cod_currency_id)'),
                ],
                'price_storage_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_storage_currency_id)'),
                ],
                'price_fulfilment_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_fulfilment_currency_id)'),
                ],
                'price_packing_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_packing_currency_id)'),
                ],
                'price_package_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_package_currency_id)'),
                ],
                'price_address_correction_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_address_correction_currency_id)'),
                ],
                'price_delivery_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_delivery_currency_id)'),
                ],
                'price_redelivery_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_redelivery_currency_id)'),
                ],
                'price_delivery_back_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_delivery_back_currency_id)'),
                ],
                'price_delivery_return_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_delivery_return_currency_id)'),
                ],
                'price_cod_service_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_cod_service_currency_id)'),
                ],
                'price_vat_currency_id' => [
                    'fact' => new Expression('max(' . OrderFinanceFact::tableName() . '.price_vat_currency_id)'),
                ],
                'price_cod_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_cod_currency_rate'),
                ],
                'price_storage_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_storage_currency_rate'),
                ],
                'price_fulfilment_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_fulfilment_currency_rate'),
                ],
                'price_packing_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_packing_currency_rate'),
                ],
                'price_package_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_package_currency_rate'),
                ],
                'price_address_correction_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_address_correction_currency_rate'),
                ],
                'price_delivery_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_delivery_currency_rate'),
                ],
                'price_redelivery_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_redelivery_currency_rate'),
                ],
                'price_delivery_back_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_delivery_back_currency_rate'),
                ],
                'price_delivery_return_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_delivery_return_currency_rate'),
                ],
                'price_cod_service_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_cod_service_currency_rate'),
                ],
                'price_vat_currency_rate' => [
                    'fact' => new Expression(OrderFinanceFact::tableName() . '.price_vat_currency_rate'),
                ],
            ];

            $select = $selectFact = [];

            foreach ($selectingColumns as $column => $values) {
                foreach ($values as $val => $attribute) {
                    $select[$column . '_' . $val] = $attribute;
                }
            }
            foreach ($selectingColumnsFact as $column => $values) {
                foreach ($values as $val => $attribute) {
                    $selectFact[$column . '_' . $val] = $attribute;
                }
            }

            $query = DeliveryReportRecord::find()->joinWith('order.financePrediction')->joinWith('financeFact')
                ->where([
                    DeliveryReportRecord::tableName() . '.delivery_report_id' => $report->id,
                    DeliveryReportRecord::tableName() . '.record_status' => [
                        DeliveryReportRecord::STATUS_APPROVE,
                        DeliveryReportRecord::STATUS_STATUS_UPDATED
                    ],
                ]);

            $queryFact = DeliveryReportRecord::find()
                ->innerJoin(
                    DeliveryReportRecord::tableName() . ' t2',
                    't2.order_id = ' . DeliveryReportRecord::tableName() . '.order_id and t2.delivery_report_id = :report_id', [':report_id' => $report->id])
                ->joinWith('financeFact')
                ->where([
                    't2.record_status' => [
                        DeliveryReportRecord::STATUS_APPROVE,
                        DeliveryReportRecord::STATUS_STATUS_UPDATED
                    ],
                    DeliveryReportRecord::tableName() . '.record_status' => [
                        DeliveryReportRecord::STATUS_STATUS_UPDATED
                    ],
                ]);


            $query->select($select);
            $queryFact->select($selectFact);
            $charges = $query->createCommand()->queryOne();
            $chargesFact = $queryFact->createCommand()->queryAll();

            $data['currencies'] = $report->currencies;
            if (empty($data['currencies'])) {
                $data['currencies'] = $report->setDefaultByReport();
            }
            $data['currencyRate'] = $report->currencyRates;

            /* валюты, используемые в тарифах данной КС */
            $currencyRates = [];
            $currencyRatesFact = [];
            $rateErrors = [];
            $usd = Currency::getUSD();
            foreach ($selectingColumns as $column => $values) {
                if (mb_strpos($column, 'currency_id') !== false) {
                    if ($charges[$column . '_db'] != null && $charges[$column . '_db'] != 0) {
                        $currencyRates[$charges[$column . '_db']] = $report->getRate($charges[$column . '_db'], Yii::$app->request->get('finance_panel_currency'));
                        if (!$currencyRates[$charges[$column . '_db']]) {
                            $rateErrors[$charges[$column . '_db']] = $charges[$column . '_db'];
                        }
                    }
                    if (!empty($charges[$column . '_fact']) && $charges[$column . '_fact'] != 0) {
                        if (Yii::$app->request->get('finance_panel_currency') == $charges[$column . '_fact']) {
                            $currencyRatesFact[$charges[$column . '_fact']] = 1;
                        } elseif ($tmpRate = $report->getRate($charges[$column . '_fact'], Yii::$app->request->get('finance_panel_currency'))) {
                            $currencyRatesFact[$charges[$column . '_fact']] = $tmpRate;
                        }
                        if (empty($currencyRatesFact[$charges[$column . '_fact']])) {
                            $rateErrors[$charges[$column . '_db']] = $charges[$column . '_db'];
                        }
                    }
                }
            }
            foreach (DeliveryReportCurrency::getFinancialColumns() as $column) {
                if (empty($currencyRates[$data['currencies']->getAttribute($column)])) {
                    $currencyRates[$data['currencies']->getAttribute($column)] = $report->getRate($data['currencies']->getAttribute($column), Yii::$app->request->get('finance_panel_currency'));
                    if (empty($currencyRates[$data['currencies']->getAttribute($column)])) {
                        $rateErrors[$data['currencies']->getAttribute($column)] = $data['currencies']->getAttribute($column);
                    }
                }
            }

            $financeRows = [
                'db' => ['title' => Yii::t('common', 'В базе данных'), 'total_charges' => 0],
                'report' => ['title' => Yii::t('common', 'В отчете'), 'total_charges' => 0],
                'difference' => ['title' => Yii::t('common', 'Разность'), 'total_charges' => 0],
                'fact' => ['title' => Yii::t('common', 'Учтено'), 'total_charges' => 0],
            ];

            foreach ($selectingColumns as $column => $values) {
                if (mb_strpos($column, 'currency_id') === false) {
                    $financeRows['db'][$column] = ($charges[$column . '_db'] ?? 0) / (!empty($currencyRates[$charges[$column . '_currency_id_db']]) ? $currencyRates[$charges[$column . '_currency_id_db']] : 1);
                    $financeRows['report'][$column] = ($charges[$column . '_report'] ?? 0) / (!empty($currencyRates[$data['currencies']->getAttribute($column . '_currency_id')]) ? $currencyRates[$data['currencies']->getAttribute($column . '_currency_id')] : 1);

                    // в строке "Разность" переделать формулу на "в отчете" - "в базе"
                    $financeRows['difference'][$column] = $financeRows['report'][$column] - $financeRows['db'][$column];
                    if ($column != 'price_cod') {
                        $financeRows['db']['total_charges'] += $financeRows['db'][$column];
                        $financeRows['report']['total_charges'] += $financeRows['report'][$column];
                        $financeRows['difference']['total_charges'] += $financeRows['difference'][$column];
                    }
                }
            }

            foreach ($selectingColumnsFact as $column => $values) {
                if (mb_strpos($column, '_currency_') === false) {
                    foreach ($chargesFact as $record) {
                        if (!isset($financeRows['fact'][$column])) {
                            $financeRows['fact'][$column] = 0;
                        }
                        $rate = $val = 0;
                        if ($record[$column . '_currency_id_fact'] != null && $record[$column . '_currency_id_fact'] != 0) {
                            if (Yii::$app->request->get('finance_panel_currency') == $record[$column . '_currency_id_fact']) {
                                $rate = 1;
                            } elseif (!empty($record[$column . '_currency_id_rate'])) {
                                $rate = $record[$column . '_currency_id_rate'];
                            } elseif ($tmpRate = $report->getRate($record[$column . '_currency_id_fact'], Yii::$app->request->get('finance_panel_currency'))) {
                                $rate = $tmpRate;
                            }
                        }
                        $financeRows['fact'][$column] += ($record[$column . '_fact'] ?? 0) / (!empty($rate) ? $rate : 1);

                        if ($column != 'price_cod') {
                            $financeRows['fact']['total_charges'] += ($record[$column . '_fact'] ?? 0) / (!empty($rate) ? $rate : 1);
                        }
                    }
                }
            }

            $financeRows['db']['to_pay'] = $financeRows['db']['price_cod'] - $financeRows['db']['total_charges'];
            $financeRows['report']['to_pay'] = $financeRows['report']['price_cod'] - $financeRows['report']['total_charges'];
            $financeRows['difference']['to_pay'] = $financeRows['report']['to_pay'] - $financeRows['db']['to_pay'];
            $financeRows['fact']['to_pay'] = $financeRows['fact']['price_cod'] - $financeRows['fact']['total_charges'];

            $finInfo['financeRows'] = $financeRows;
            $finInfo['financeCurrencies'] = $currencyRates;
            $finInfo['payment_id'] = $report->payment_id;
            $finInfo['payments'] = $report->getPaymentsWithStatus(true);
            $finInfo['paymentsResult']['sum'] = $finInfo['paymentsResult']['balance'] = 0;
            foreach ($finInfo['payments']->getModels() as $model) {
                $rate = $report->getRate($model->currency_id, $report->country->currency_id, null, $model);
                $finInfo['paymentsResult']['sum'] += $model->sum / (!empty($rate) ? $rate : 1);
                $finInfo['paymentsResult']['balance'] += $model->balance / (!empty($rate) ? $rate : 1);
            }
            $finInfo['currencyReport'] = Yii::$app->request->get('finance_panel_currency', $report->currency_id);
            $finInfo['currencyReportDate'] = $report->currency_date ? Yii::$app->formatter->asDate($report->currency_date, 'php:Y-m-d') : null;
            $finInfo['rateErrors'] = $rateErrors;
        }

        $data['finInfo'] = $finInfo;

        $data['errorInfo'] = [];

        $errorTypes = DeliveryReportRecord::errorLabels();

        $query = clone $dataProvider->query;
        $query->joinWith(['recordErrors'], false);
        $query->select([
            'count' => 'COUNT(' . DeliveryReportRecord::tableName() . '.id)',
            'error_id' => DeliveryReportRecordError::tableName() . '.error_id'
        ]);
        $query->groupBy(DeliveryReportRecordError::tableName() . '.error_id');

        $errorsCount = ArrayHelper::map($query->createCommand()->queryAll(), 'error_id', 'count');

        foreach ($errorTypes as $errorType => $label) {

            $count = $errorsCount[$errorType] ?? 0;
            if ($errorType == DeliveryReportRecord::ERROR_NOT_FOUND) {
                $count = $errorsCount[null] ?? 0;
            }

            $data['errorInfo'][$errorType]['label'] = $label;
            $data['errorInfo'][$errorType]['count'] = $count;
        }

        $data['orderIds'] = ArrayHelper::getColumn(ArrayHelper::getColumn($dataProvider->models, 'relatedOrder'), 'id');

        return $this->render('view', $data);
    }

    /**
     * @param $id
     *
     * @return string|array
     * @throws NotFoundHttpException
     */
    public function actionViewOrder($id)
    {
        $model = $this->findRecord($id);
        $model = DeliveryReportRecordSearch::findRelatedOrders([$model], $model->report)[0];

        $order_id = Yii::$app->request->get('order_id');
        $fractionalOrders = null;

        if (Yii::$app->request->get('show_fractional_orders')) {
            $fractionalOrders = ReportScanner::getFractionalOrderIDs($model);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $response = [
                    'status' => 'success',
                    'content' => $this->renderPartial('view_fractional_orders', [
                        'model' => $model,
                        'fractionalOrders' => $fractionalOrders
                    ]),
                ];
                return $response;
            }
        }

        $errFields = $model->errorFields;
        $errors = $model->errorMessages;

        if (!empty($model->relatedOrder) && !empty($order_id) && $model->relatedOrder->id != $order_id) {
            $errFields[] = 'order_id';
        }

        if (!empty($order_id)) {
            $model->convertedOrder = $order_id;
        }

        return $this->render('view_order', [
            'model' => $model,
            'order' => $model->convertedOrder,
            'errors' => $errors,
            'fields' => $errFields,
            'fractionalOrders' => $fractionalOrders,
        ]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];
        $model = new UploadForm();
        $model->load(Yii::$app->request->post(), '');
        $model->file = UploadedFile::getInstanceByName('file');
        if (empty($model->report_id)) {
            $model->setScenario(UploadForm::SCENARIO_CREATE);
        } else {
            $model->setScenario(UploadForm::SCENARIO_UPDATE);
        }

        if ($model->validate()) {

            $report = empty($model->report_id) ? new DeliveryReport() : DeliveryReport::findOne($model->report_id);

            if (!empty($model->file)) {
                $scanner = new ReportScanner(null, $model->type);

                try {
                    $scanner->checkStructureError($model->file->tempName);
                } catch (\Throwable $e) {
                    $response['message'] = $e->getMessage();
                    return $response;
                }

                $filename = md5(time() . $model->file->name) . '.' . $model->file->extension;

                $report->file = $filename;
                $report->name = $model->file->name;
                $report->status = DeliveryReport::STATUS_QUEUE;
            }
            if (!empty($model->report_name)) {
                $report->name = $model->report_name;
            }
            $delivery = Delivery::find()->byId($model->delivery_id)->one();
            $report->type = $model->type;
            $report->delivery_id = $model->delivery_id;
            $report->date_format = $model->date_format;
            $report->period_from = $model->period_from ? Yii::$app->formatter->asTimestamp($model->period_from, $delivery->country->timezone->timezone_id) : null;
            $report->period_to = $model->period_to ? Yii::$app->formatter->asTimestamp($model->period_to, $delivery->country->timezone->timezone_id) : 0;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($report->save() && (!isset($filename) || $model->upload($filename))) {
                    $transaction->commit();
                    $response['status'] = 'success';
                    $searchModel = new DeliveryReportSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    $response['content'] = $this->renderPartial('_index-content', ['dataProvider' => $dataProvider]);
                } else {
                    $transaction->rollBack();
                    $response['message'] = Yii::t('common', 'Произошла ошибка при загрузке файла. Попробуйте еще раз.');
                }
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
            }
        } else {
            $response['message'] = $model->getFirstErrorAsString();
        }
        return $response;
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $report = $this->findModel($id);

        $model = new UploadForm();
        $model->setScenario(UploadForm::SCENARIO_UPDATE);

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if (!empty($file->name)) {
                $filename = md5(time() . $file->name) . '.' . $file->extension;
                $filePath = DeliveryReport::getFileFullRoute($filename);

                unlink(DeliveryReport::getFileFullRoute($report->file));

                $report->file = $filename;
                $report->name = $file->name;
                $report->status = DeliveryReport::STATUS_QUEUE;
                $report->deleteRecords();
                $report->error_message = '';
                if (!$file->saveAs($filePath)) {
                    Yii::$app->notifier->addNotification(Yii::t('common',
                        'Произошла ошибка при загрузке файла. Попробуйте еще раз.'), Notifier::TYPE_DANGER);

                    $model = new UploadForm();
                    unlink($filePath);

                    return $this->render('update', [
                        'model' => $model,
                        'report' => $report
                    ]);
                }
            }

            $report->type = $model->type;
            $report->delivery_id = $model->delivery_id;
            $report->name = $model->report_name;

            if (!$report->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common',
                    'Произошла ошибка при загрузке файла. Попробуйте еще раз.'), Notifier::TYPE_DANGER);

                return $this->render('update', [
                    'model' => $model,
                    'report' => $report
                ]);
            }

            return $this->redirect(Url::toRoute(['index']));

        } else {

            $model->delivery_id = $report->delivery_id;
            $model->type = $report->type;
            $model->report_name = $report->name;

            return $this->render('update', [
                'model' => $model,
                'report' => $report
            ]);
        }
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionEditOrder($id)
    {
        $model = $this->findRecord($id);
        $model = DeliveryReportRecordSearch::findRelatedOrders([$model], $model->report)[0];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $report = new ReportScanner($model->delivery_report_id);
            $report->checkRecord($model);
            return $this->redirect(array_merge(Yii::$app->request->get(), ['view-order', 'id' => $model->id]));
        }

        return $this->redirect(array_merge(Yii::$app->request->get(), ['view-order', 'id' => $model->id]));
    }

    /**
     * @param $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetPaymentId($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $batchSize = 100;
        $paymentId = Yii::$app->request->post('payment_id');
        if (Yii::$app->request->post('set_to_report')) {
            $report = $this->findModel($id);
            $report->payment_id = $paymentId;
            $report->save();
            $response['status'] = 'success';
            return $response;
        }
        /** @var \yii\db\ActiveQuery $query */
        $query = DeliveryReportRecord::find()
            ->where([DeliveryReportRecord::tableName() . '.delivery_report_id' => $id]);
        $query->andWhere([
            DeliveryReportRecord::tableName() . '.record_status' => [
                DeliveryReportRecord::STATUS_APPROVE,
                DeliveryReportRecord::STATUS_STATUS_UPDATED
            ]
        ]);
        $query->with(['order.finance', 'order.deliveryRequest']);
        if (Yii::$app->request->get('getOffsetCount')) {
            $rewrite = Yii::$app->request->get('rewrite');
            $paymentId = Yii::$app->request->get('payment_id');
            $totalCount = $query->count();
            if ($totalCount == 0 || (!$rewrite && empty($paymentId))) {
                $response['message'] = Yii::t('common',
                    'Записи, которым возможно проставить номер платежа, отсутствуют.');
                return $response;
            }
            $offsetCount = (integer)($totalCount / $batchSize) + 1;

            $response['status'] = 'success';
            $response['offsetCount'] = $offsetCount;
            return $response;
        }
        $rewrite = Yii::$app->request->post('rewrite');
        $offset = intval(Yii::$app->request->post('offset'));
        if ($offset) {
            $offset *= $batchSize;
            $query->offset($offset);
        }
        $query->limit($batchSize);
        /**
         * @var DeliveryReportRecord[] $records
         */
        $records = $query->all();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($records as $record) {
                $order = $record->relatedOrder;
                if ($order) {
                    $finance = $order->finance;
                    if (!$finance) {
                        $finance = new OrderFinance();
                        $finance->order_id = $order->id;
                    }
                    if ($rewrite || (empty($finance->payment) && !empty($paymentId))) {
                        $finance->payment = $paymentId;

                        $checkingColumns = [
                            'price_cod',
                            'price_storage',
                            'price_packing',
                            'price_package',
                            'price_address_correction',
                            'price_delivery',
                            'price_redelivery',
                            'price_delivery_back',
                            'price_delivery_return',
                            'price_cod_service',
                        ];
                        foreach ($checkingColumns as $key) {
                            if (empty($finance->$key) && !empty($record->$key)) {
                                $finance->$key = $record->$key;
                            }
                        }
                        $finance->route = $this->route;
                        $finance->save();
                    }
                }
            }
            $transaction->commit();
            $response['status'] = 'success';
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    /**
     * @param $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetOrderPrices($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        $rewrite = Yii::$app->request->post('rewrite');

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            /** @var \yii\db\ActiveQuery $query */
            $query = $searchModel->search(Yii::$app->request->queryParams, $id)->query;
            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED
                ]
            ]);
            $query->with(['order.finance', 'order.deliveryRequest']);
            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, которым возможно изменить цены, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $offset = intval(Yii::$app->request->post('offset'));
            if ($offset) {
                $offset *= $batchSize;
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = $query->all();
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($records as $record) {
                $order = $record->relatedOrder;
                if ($order) {
                    $finance = $order->finance;
                    if (!$finance) {
                        $finance = new OrderFinance();
                        $finance->order_id = $order->id;
                    }
                    $skip = true;
                    $columns = [];
                    $checkingColumns = [
                        'price_cod',
                        'price_storage',
                        'price_packing',
                        'price_package',
                        'price_address_correction',
                        'price_delivery',
                        'price_redelivery',
                        'price_delivery_back',
                        'price_delivery_return',
                        'price_cod_service',
                    ];

                    if ($rewrite) {
                        $skip = false;
                        $columns = $checkingColumns;
                    } else {
                        foreach ($checkingColumns as $key) {
                            if ($finance->$key == '' && $record->$key != '') {
                                if ($skip) {
                                    $skip = false;
                                }
                                $columns[] = $key;
                            }
                        }
                    }

                    if (!$skip) {
                        foreach ($columns as $column) {
                            $finance->$column = $record->$column;
                        }
                        $finance->route = $this->route;
                        $finance->save();
                    }
                }
            }
            $transaction->commit();
            $response['status'] = 'success';
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionSetApprove($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $report = $this->findModel($id);
        $records = [];
        $batchSize = 100;
        $neededWith = [
            'order.deliveryRequest',
            'order.duplicateOrder.deliveryRequest',
            'firstDeliveryRequest.order.duplicateOrder.deliveryRequest',
            /*'callCenterRequest.order.deliveryRequest',
            'callCenterRequest.order.duplicateOrder.deliveryRequest',*/
            'deliveryRequest',
        ];

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);

            $query->andWhere([
                'not in',
                DeliveryReportRecord::tableName() . '.record_status',
                [DeliveryReportRecord::STATUS_APPROVE, DeliveryReportRecord::STATUS_DELETED]
            ]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common', 'Записи для подтверждения отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->with($neededWith);
            $offset = intval(Yii::$app->request->post('offset'));
            if ($offset) {
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = $query->all();
        }
        $records = DeliveryReportRecordSearch::findRelatedOrders($records, $report);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($records as $record) {
                if (!empty($record->relatedOrder) && $record->relatedOrder->id != $record->order_id) {
                    $record->order_id = (string)$record->relatedOrder->id;
                    $record->removeError([
                        DeliveryReportRecord::ERROR_INCORRECT_ORDERID,
                        DeliveryReportRecord::ERROR_ORDER_NOT_EXIST,
                        DeliveryReportRecord::ERROR_FIND_ORDERID_BY_TRACK
                    ]);
                }

                if (empty($record->order_id) || empty($record->relatedOrder) || $record->relatedOrder->country_id != $record->report->country_id || (!empty($record->relatedOrder->deliveryRequest) && $record->relatedOrder->deliveryRequest->delivery_id != $record->report->delivery_id)) {
                    if (isset($offset)) {
                        $offset++;
                    }
                    continue;
                }

                $record->record_status = DeliveryReportRecord::STATUS_APPROVE;
                if (!$record->save(true, ['order_id', 'record_status', 'error_log', 'updated_at'])) {
                    throw new \Exception(Yii::t('common', 'Не удалось подтвердить запись. Причина: {reason}',
                        ['reason' => $record->getFirstErrorAsString()]));
                }
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
            $response['status'] = 'fail';
            return $response;
        }
        if (isset($offset)) {
            $response['offset'] = $offset;
        }
        $response['status'] = 'success';
        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionUnsetApprove($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;
        $neededWith = [];

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);

            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => DeliveryReportRecord::STATUS_APPROVE
            ]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common', 'Подтвержденные записи отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->with($neededWith);
            $query->limit($batchSize);
            $records = $query->all();
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($records as $record) {
                $record->record_status = DeliveryReportRecord::STATUS_ACTIVE;
                if (!$record->save(true, ['record_status'])) {
                    throw new \Exception(Yii::t('common', 'Не удалось отменить подтверждение записи. Причина: {reason}',
                        ['reason' => $record->getFirstErrorAsString()]));
                }
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
            $response['status'] = 'fail';
            return $response;
        }
        $response['status'] = 'success';
        return $response;
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->getRecords()
            ->andWhere([DeliveryReportRecord::tableName() . '.record_status' => DeliveryReportRecord::STATUS_STATUS_UPDATED])
            ->exists()) {
            $this->notifier->addNotification(Yii::t('common', 'Нельзя удалить отчет, в котором присутствуют обновленные записи.'));
        } else {
            $model->status = DeliveryReport::STATUS_DELETE;
            if ($model->save(true, ['status'])) {
                Yii::$app->queue->push(new DeliveryReportDelete(['deliveryReportId' => $model->id]));
            } else {
                $this->notifier->addNotification($model->getFirstErrorAsString());
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDeleteRecord($id)
    {
        $record = $this->findRecord($id);
        $report_id = $record->delivery_report_id;

        $record->delete();

        $options = Yii::$app->request->get();
        $options['id'] = $report_id;

        return $this->redirect(Url::toRoute(array_merge($options, ['view'])));
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionDeleteRecords($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);

            $query->andWhere([
                '!=',
                DeliveryReportRecord::tableName() . '.record_status',
                DeliveryReportRecord::STATUS_DELETED
            ]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common', 'Активные записи отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->limit($batchSize);
            $records = $query->all();
        }

        foreach ($records as $record) {
            $record->delete();
        }
        $response['status'] = 'success';
        return $response;
    }


    /**
     * @param $id
     * @return array
     */
    public function actionUpdateOrderTracking($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        $rewrite = Yii::$app->request->post('rewrite');

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);
            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED
                ]
            ]);
            $query->andWhere(['>', 'LENGTH(' . DeliveryReportRecord::tableName() . '.tracking)', 3]);
            $query->joinWith('order.deliveryRequest');
            if (Yii::$app->request->get('getOffsetCount')) {
                $rewrite = Yii::$app->request->get('rewrite');
                if (!$rewrite) {
                    $query->andWhere([
                        'or',
                        [DeliveryRequest::tableName() . '.tracking' => ''],
                        ['is', DeliveryRequest::tableName() . '.id', null]
                    ]);
                }
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, которым возможно изменить трек номер, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->with(['order.status']);
            if (!$rewrite) {
                $query->andWhere([
                    'or',
                    [DeliveryRequest::tableName() . '.tracking' => ''],
                    ['is', DeliveryRequest::tableName() . '.id', null]
                ]);
            }
            $offset = intval(Yii::$app->request->post('offset'));
            if ($offset) {
                $offset *= $batchSize;
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = $query->all();
        }
        $responseErrors = [];
        foreach ($records as $record) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $order = $record->relatedOrder;
                if (empty($order)) {
                    if ($record_id) {
                        throw new \Exception(Yii::t('common', 'Невозможно найти заказ.'));
                    }
                }

                if (!in_array($order->status->group, [OrderStatus::GROUP_DELIVERY, OrderStatus::GROUP_FINANCE, OrderStatus::GROUP_LOGISTIC]) || !$order->deliveryRequest) {
                    $message = $order->deliveryRequest ? Yii::t('common',
                        'Заказ должен находиться в службе доставке или быть доставленным.') : Yii::t('common',
                        'Отсутствует заявка в службе доставки.');
                    if (!$record_id) {
                        $responseErrors[] = $message;
                        $transaction->rollBack();
                        continue;
                    } else {
                        throw new \Exception($message);
                    }
                }
                $shipping = $order->deliveryRequest;

                if (!empty($shipping) && strlen($record->tracking) > 3 && ($shipping->tracking == '' || $rewrite)) {
                    $shipping->tracking = $record->tracking;
                    $shipping->route = $this->route;
                    if (!$shipping->save(true, ['tracking'])) {
                        throw new \Exception(Yii::t('common', 'Не удалось изменить данные. Причина: {reason}',
                            [$shipping->getFirstErrorAsString()]));
                    }

                    if ($rewrite) {
                        DeliveryReportRecordError::deleteAll([
                            'delivery_report_record_id' => $record->id,
                            'error_id' => DeliveryReportRecord::ERROR_NOT_EQUAL_TRACK
                        ]);
                    }
                }

                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
                $response['status'] = 'fail';
                return $response;
            }
        }
        $response['status'] = 'success';

        $response['errors'] = $responseErrors;
        return $response;
    }

    /**
     * Метод проверки наличие претензии на строчку отчета при обновлении статусов - если есть претензии - fact не пишем
     * @param $facts
     * @return bool
     */
    public function withPretension($facts)
    {
        if (!$facts) {
            $facts = [];
        }

        $is_pretension = false;

        foreach ($facts as $fact) {
            /** @var OrderFinanceFact $fact */
            if (is_numeric($fact->pretension)) {
                $is_pretension = true;
            }
        }

        return $is_pretension;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionUpdateOrderData($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        $report = $this->findModel($id);

        $rewrite = Yii::$app->request->post('rewrite');
        $column = Yii::$app->request->post('column');

        $shippingColumns = DeliveryReportRecord::getDeliveryRequestColumns();
        $financeColumns = DeliveryReportRecord::getFinancialColumns();
        $orderColumns = DeliveryReportRecord::getOrderColumns();
        $orderRefusedReasonColumns = [
            DeliveryReportRecord::COLUMN_CALL_CENTER_PENALTY,
            DeliveryReportRecord::COLUMN_UNBUYOUT_REASONS,
        ];

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);
            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                    DeliveryReportRecord::STATUS_STATUS_UPDATED
                ]
            ]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $recordColumn = Yii::$app->request->get('column');
                if (in_array($recordColumn, $orderRefusedReasonColumns)) {
                    $recordColumn = DeliveryReportRecord::COLUMN_COMMENT;
                }
                $query->andWhere(['!=', DeliveryReportRecord::tableName() . ".{$recordColumn}", '']);
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, для которых возможно изменить данные, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $recordColumn = $column;
            if (in_array($recordColumn, $orderRefusedReasonColumns)) {
                $recordColumn = DeliveryReportRecord::COLUMN_COMMENT;
            }
            $query->with(['order.deliveryRequest', 'order.finance', 'order.status']);
            $query->andWhere(['!=', DeliveryReportRecord::tableName() . ".{$recordColumn}", '']);
            $offset = intval(Yii::$app->request->post('offset'));
            if ($offset) {
                $offset *= $batchSize;
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = $query->all();
        }

        $responseErrors = [];
        foreach ($records as $record) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $order = $record->relatedOrder;
                $order->route = $this->route;

                if (key_exists($column, $shippingColumns)) {
                    $shipCol = $shippingColumns[$column];
                    $shipping = $order->deliveryRequest;
                    $shipping->route = $this->route;
                    if (empty($shipping)) {
                        $responseErrors[] = Yii::t('common', 'Отсутствует заявка в службе доставки.');
                        $transaction->rollBack();
                        continue;
                    }
                    if ($column == DeliveryReportRecord::COLUMN_DATE_CREATED || $column == DeliveryReportRecord::COLUMN_DATE_APPROVE || $column == DeliveryReportRecord::COLUMN_DATE_RETURN || $column == DeliveryReportRecord::COLUMN_DATE_PAYMENT) {
                        $trimmedDate = preg_replace("/(^\s+)|(\s+$)/us", "", $record->$column);
                        if (empty($trimmedDate)) {
                            $transaction->rollBack();
                            continue;
                        }
                        $dateFormat = empty(trim($report->date_format)) ? $report->delivery->date_format : trim($report->date_format);

                        //Закомментировать при локальном тесте
                        if (empty($dateFormat)) {
                            throw new \Exception(Yii::t('common', "Не указан формат дат"));
                        }
                        $timezone = new \DateTimeZone($report->country->timezone->timezone_id);
                        $dateTime = \DateTime::createFromFormat($dateFormat, $trimmedDate, $timezone);
                        if ($dateTime && $dateTime->getLastErrors()['warning_count'] == 0 && $dateTime->getLastErrors()['error_count'] == 0) {
                            $beginTime = new \DateTime();
                            $beginTime->setTimestamp($dateTime->getTimestamp());
                            $beginTime = new \DateTime($beginTime->format('H:i:s'));
                            $now = new \DateTime('now');

                            if (abs($now->getTimestamp() - $beginTime->getTimestamp()) <= 1) {
                                $beginTime->setTimestamp($dateTime->getTimestamp());
                                $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s',
                                    $beginTime->format('Y-m-d 11:00:00')); // Ставим 14:00 по МСК
                            }
                        }

                        //Закомментировать при локальном тесте вместе с исключением
                        if (!$dateTime || $dateTime->getLastErrors()['warning_count'] > 0 || $dateTime->getLastErrors()['error_count'] > 0 || $dateTime->getTimestamp() > strtotime('+1day') || (!empty($shipping->created_at) && $dateTime->getTimestamp() < strtotime("-1 day",
                                    $shipping->created_at))
                        ) {
                            throw new \Exception(Yii::t('common', "Указана некорректная дата для заказа #{id} dateTime={dateTime} dateFormat={dateFormat} trimmedDate={trimmedDate} timezone={timezone}",
                                [
                                    'id' => $order->id,
                                    'dateTime' => $dateTime ? 'true' : 'false',
                                    'dateFormat' => $dateFormat,
                                    'trimmedDate' => $trimmedDate,
                                    'timezone' => print_r($timezone, true)
                                ]));
                        }
                        if ($rewrite || empty($shipping->$shipCol)) {
                            $shipping->$shipCol = $dateTime->getTimestamp();
                            if ($shipping->$shipCol < $shipping->created_at) {
                                $sentAt = $shipping->sent_at;
                                if (empty($sentAt) || $sentAt < $shipping->created_at) {
                                    $sentAt = $shipping->created_at;
                                }
                                $shipping->$shipCol = $sentAt;
                            }
                            if (!$shipping->save(true, [$shipCol])) {
                                throw new \Exception(Yii::t('common',
                                    "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                                    ['id' => $order->id, 'errors' => $shipping->getFirstErrorAsString()]));
                            }
                        }
                    } else {
                        if (($rewrite || $shipping->$shipCol == '') && $record->$column != '') {
                            $shipping->$shipCol = $record->$column;
                            if (!$shipping->save(true, [$shipCol])) {
                                throw new \Exception(Yii::t('common',
                                    "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                                    ['id' => $order->id, 'errors' => $shipping->getFirstErrorAsString()]));
                            }
                        }
                    }
                }

                if (key_exists($column, $financeColumns)) {
                    $finCol = $financeColumns[$column];
                    $finance = $order->finance;
                    if (empty($finance)) {
                        $finance = new OrderFinance();
                        $finance->order_id = $order->id;
                    }
                    $finance->route = $this->route;
                    if (($rewrite || $finance->$finCol == '') && $record->$column != '') {
                        $finance->$finCol = $record->$column;
                        if (!$finance->save()) {
                            throw new \Exception(Yii::t('common',
                                "Возникли ошибки при сохранении заказа #{id}:\n{errors}",
                                ['id' => $order->id, 'errors' => implode("\n", $finance->firstErrors)]));
                        }
                    }
                }

                if (key_exists($column, $orderColumns)) {
                    $orderCol = $orderColumns[$column];
                    if ($column == DeliveryReportRecord::COLUMN_DELIVERY_TIME_FROM) {
                        $trimmedDate = preg_replace("/(^\s+)|(\s+$)/us", "", $record->$column);
                        if (empty($trimmedDate)) {
                            $transaction->rollBack();
                            continue;
                        }
                        $dateFormat = empty(trim($report->date_format)) ? $report->delivery->date_format : trim($report->date_format);

                        //Закомментировать при локальном тесте
                        if (empty($dateFormat)) {
                            throw new \Exception(Yii::t('common', "Не указан формат дат"));
                        }
                        $timezone = new \DateTimeZone($report->country->timezone->timezone_id);
                        $dateTime = \DateTime::createFromFormat($dateFormat, $trimmedDate, $timezone);

                        //Закомментировать при локально тесте
                        if (!$dateTime || $dateTime->getLastErrors()['warning_count'] > 0 || $dateTime->getLastErrors()['error_count'] > 0 || $dateTime->getTimestamp() > strtotime('+1day') || (!empty($order->created_at) && $dateTime->getTimestamp() < $order->created_at)) {
                            throw new \Exception(Yii::t('common', "Указана некорректная дата для заказа #{id}",
                                ['id' => $order->id]));
                        }
                        if (($rewrite || empty($order->$orderCol)) && $record->$column != '') {
                            $timezone = new \DateTimeZone($report->country->timezone->timezone_id);
                            $beginTime = new \DateTime($dateTime->format('H:i:s'), $timezone);
                            $now = new \DateTime('now', $timezone);

                            $order->$orderCol = $dateTime->getTimestamp();
                            if (abs($now->getTimestamp() - $beginTime->getTimestamp()) <= 1) {
                                $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s',
                                    $dateTime->format('Y-m-d 01:00:00'), $timezone);
                                $order->$orderCol = $dateTime->getTimestamp();
                            }
                            $order->delivery_time_to = $order->$orderCol + 169199; // Добавляем 2е суток
                            if (!$order->save(true, [$orderCol, 'delivery_time_to'])) {
                                throw new \Exception(Yii::t('common',
                                    "Возникли ошибки при сохранении заказа #{id}:\n{errors}",
                                    ['id' => $order->id, 'errors' => implode("\n", $order->firstErrors)]));
                            }
                        }
                    } elseif (($rewrite || $order->$orderCol == '') && $record->$column != '') {
                        $orderSaveColumns = [$orderCol];
                        // Если из отчета проставляем COD, то учитываем, что он состоит из суммы price_total + delivery
                        if ($column == DeliveryReportRecord::COLUMN_PRICE_COD) {
                            // шайтанская логика, хз будет ли норм работать
                            $differenceOrderReport = ($order->price_total + $order->delivery) - $record->$column;
                            $differenceReportOrder = $record->$column - ($order->price_total + $order->delivery);
                            if ($differenceOrderReport - $order->delivery >= 0 && $differenceOrderReport - $order->delivery <= $order->delivery) {
                                $order->delivery -= $differenceOrderReport;
                            } elseif ($differenceOrderReport > 0) {
                                if ($record->price > 0 && $record->price == ($record->price_cod - $order->delivery)) {
                                    $order->price_total = $record->price;
                                } else {
                                    $order->price_total -= $differenceOrderReport;
                                }
                            } else {
                                if ($record->price > 0 && $record->price == ($record->price_cod - $order->delivery)) {
                                    $order->price_total = $record->price;
                                } else {
                                    $order->price_total += $differenceReportOrder;
                                }
                            }
                            $orderSaveColumns[] = 'delivery';

                            $order->sendToCalculateDeliveryCharges();
                        } else {
                            $order->$orderCol = $record->$column;
                        }
                        if (!$order->save(true, $orderSaveColumns)) {
                            throw new \Exception(Yii::t('common',
                                "Возникли ошибки при сохранении заказа #{id}:\n{errors}",
                                ['id' => $order->id, 'errors' => implode("\n", $order->firstErrors)]));
                        }
                    }
                }

                if ($column == DeliveryReportRecord::COLUMN_UNBUYOUT_REASONS) {
                    $unBuyoutReasons = $report->unBuyoutReasons;
                    if (!empty($record->comment) && isset($unBuyoutReasons[$record->comment]) && !empty($unBuyoutReasons[$record->comment])) {
                        $order->deliveryRequest->saveUnBuyoutReason($record->comment);
                    }
                }

                if ($column == DeliveryReportRecord::COLUMN_CALL_CENTER_PENALTY) {
                    if (!empty($record->comment)
                        && ($trigger = CallCenterPenaltyType::getTriggerByString($record->comment))
                        && ($penaltyType = CallCenterPenaltyType::find()->byTrigger($trigger)->one())
                        && $penaltyType->active
                        && ($person = CallCenterPersonSearch::findPersonByOrderId($record->order_id))
                        && !CallCenterPenalty::find()->where([
                            'person_id' => $person->id,
                            'order_id' => $record->order_id,
                            'penalty_type_id' => $penaltyType->id
                        ])->exists()
                    ) {
                        $penalty = new CallCenterPenalty([
                            'order_id' => $record->order_id,
                            'person_id' => $person->id,
                            'penalty_type_id' => $penaltyType->id,
                            'penalty_sum_usd' => $penaltyType->sum,
                            'penalty_percent' => $penaltyType->percent,
                        ]);
                        if (!$penalty->save()) {
                            throw new \Exception(Yii::t('common',
                                "Возникли ошибки при сохранении заказа #{id}:\n{errors}",
                                ['id' => $order->id, 'errors' => $penalty->getFirstErrorAsString()]));
                        }
                    }
                }

                if ($column == DeliveryReportRecord::COLUMN_PRODUCTS) {
                    if (!empty($record->products) && ($products = $record->getConvertedProducts()) !== false) {
                        $subTransaction = Yii::$app->db->beginTransaction();
                        try {
                            $orderProducts = $order->orderProducts;
                            foreach ($order->orderProducts as $orderProduct) {
                                $orderProduct->delete();
                            }
                            foreach (array_keys($products) as $productId) {
                                $product = $products[$productId];
                                if (empty($product['product_id'])) {
                                    throw new \Exception(Yii::t('common', 'Не удается найти продукт с именем {name}', ['name' => $product['name']]));
                                }
                                if (empty($product['quantity'])) {
                                    continue;
                                }

                                $price = 0;
                                foreach ($orderProducts as $orderProduct) {
                                    if ($orderProduct->product_id == $product['product_id']) {
                                        if ($price < $orderProduct->price) {
                                            $price = $orderProduct->price;
                                        }
                                    }
                                }
                                if ($price > 0) {
                                    $products[$productId]['price'] = $price;
                                } elseif (empty($product['price'])) {
                                    throw new \Exception(Yii::t('common', 'Не удается определить цену продукта {name}', ['name' => $product['name']]));
                                }
                            }
                            $splitProducts = Lead::splitProductsToPackages($products);
                            $priceTotal = 0;
                            foreach ($splitProducts as $product) {
                                $orderProduct = new OrderProduct([
                                    'order_id' => $order->id,
                                    'product_id' => $product['product_id'],
                                    'quantity' => $product['quantity'],
                                    'price' => $product['price']
                                ]);
                                if (!$orderProduct->save()) {
                                    throw new \Exception($orderProduct->getFirstErrorAsString());
                                }
                                $priceTotal += $product['price'];
                            }
                            $order->price_total = $priceTotal;
                            $order->sendToCalculateDeliveryCharges();
                            $subTransaction->commit();
                        } catch (\Throwable $e) {
                            $subTransaction->rollBack();
                            $responseErrors[] = $e->getMessage();
                        }
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
                $response['status'] = 'fail';
                return $response;
            }
        }
        if ($record_id && !empty($responseErrors)) {
            $response['message'] = array_shift($responseErrors);
            $response['status'] = 'fail';
        } else {
            $response['status'] = 'success';
            $response['errors'] = $responseErrors;
        }
        return $response;
    }

    /**
     * @param integer $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdateOrderStatus($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $report = $this->findModel($id);

        $contract = $report->delivery->getActiveContractByDelivery($report->delivery->id, $report->period_from, $report->period_to);

        $records = [];
        $batchSize = 100;

        $offset = intval(Yii::$app->request->post('offset'));
        $neededRelations = [
            'order.deliveryRequest',
            'order.callCenterRequest',
            'order.finance',
        ];

        if ($record_id = Yii::$app->request->post('id')) {
            /** @var DeliveryReportRecord $record */
            $record = DeliveryReportRecord::find()->where(['id' => $record_id])->with($neededRelations)->one();
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            /** @var \yii\db\ActiveQuery $query */
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);
            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE
                ]
            ]);
            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, которым возможно изменить статус, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->with($neededRelations);
            $query->joinWith(['order.deliveryRequest']);
            if ($report->type == DeliveryReport::TYPE_FINANCIAL) {
                $finStatus = [];
                foreach ($report->statuses as $status => $statusId) {
                    if ($statusId == OrderStatus::STATUS_FINANCE_MONEY_RECEIVED) {
                        $finStatus[] = $status;
                    }
                }
                if (!empty($finStatus)) {
                    $query->addOrderBy([
                        new Expression('FIELD(' . DeliveryReportRecord::tableName() . ".status, '" . implode("', '",
                                $finStatus) . "') ASC"),
                    ]);
                }
            }
            $query->addOrderBy([
                DeliveryRequest::tableName() . '.created_at' => SORT_ASC,
                Order::tableName() . '.id' => SORT_ASC
            ]);
            if ($offset) {
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */
            $records = $query->all();
        }
        $rewrite = Yii::$app->request->post('rewrite');
        $ignoreBalance = Yii::$app->request->post('ignore_balance');
        $responseErrors = [];

        /**
         * @var OrderStatus[] $statusMap
         */
        $statusMap = OrderStatus::find()->where(['id' => $report->statuses])->all();
        $statusMap = ArrayHelper::index($statusMap, 'id');

        $currentPayment = null;
        $payments = [];
        if ($report->type == DeliveryReport::TYPE_FINANCIAL) {
            $payments = $report->payments;
            $currentPayment = array_pop($payments);
        }

        foreach ($records as $record) {
            if (!$record->relatedOrder) {
                $response['status'] = 'fail';
                $response['message'] = yii::t('common', 'Заказ не найден.');
                $response['report_balance'] = $this->getBalanceForWidget(
                    $report->getBalance(),
                    $report->country->currency->char_code
                );

                return $response;
            }

            $transaction = Yii::$app->db->beginTransaction();
            if ($currentPayment) {
                PaymentOrder::getDb()->createCommand('SELECT 1 FROM ' . PaymentOrder::tableName() . ' WHERE id = :id LOCK IN SHARE MODE', [':id' => (int)$currentPayment->id])->execute();
            }
            try {
                if (!isset($report->statuses[$record->status])) {
                    throw new \Exception(Yii::t('common',
                        'Для статуса {status} не указан соответствующий статус системы.',
                        ['status' => $record->status]));
                }
                $order = $record->relatedOrder;
                $order->route = $this->route;
                $statusFromReport = intval($report->statuses[$record->status]);

                if (!in_array($order->status->group, [OrderStatus::GROUP_DELIVERY, OrderStatus::GROUP_FINANCE, OrderStatus::GROUP_LOGISTIC])) {
                    if (!is_null($offset)) {
                        $offset++;
                    }
                    $responseErrors[] = Yii::t('common',
                        'У заказа статус {status}, который не состоит в группе статусов "Служба доставки".',
                        ['status' => $order->status->name]);
                    $transaction->rollBack();
                    continue;
                }

                if ($statusMap[$statusFromReport]->group != OrderStatus::GROUP_DELIVERY && $statusMap[$statusFromReport]->group != OrderStatus::GROUP_FINANCE) {
                    if (!is_null($offset)) {
                        $offset++;
                    }
                    $responseErrors[] = Yii::t('common',
                        'К статусу {status} привязан статус, который не состоит в группе статусов "Служба доставки".',
                        ['status' => $record->status]);
                    $transaction->rollBack();
                    continue;
                }

                if (($order->callCenterRequest && in_array($order->callCenterRequest->status, [
                            CallCenterRequest::STATUS_IN_PROGRESS,
                            CallCenterRequest::STATUS_PENDING
                        ]))
                    || ($order->callCenterCheckRequests && in_array($order->status_id, [OrderStatus::STATUS_CC_CHECKING]) && $statusFromReport == OrderStatus::STATUS_DELIVERY_REJECTED)
                ) {
                    if (!is_null($offset)) {
                        $offset++;
                    }
                    $responseErrors[] = Yii::t('common',
                        'Невозможно обновить статус заказа, т.к. открыта заявка в КЦ.');
                    $transaction->rollBack();
                    continue;
                }

                $deliveryRequest = $order->deliveryRequest;
                if (empty($deliveryRequest)) {
                    $deliveryRequest = new DeliveryRequest([
                        'delivery_id' => $report->delivery_id,
                        'order_id' => $order->id,
                        'status' => ($statusMap[$statusFromReport]->group == OrderStatus::GROUP_DELIVERY && $statusMap[$statusFromReport]->type == OrderStatus::TYPE_STARTING) ? DeliveryRequest::STATUS_PENDING : DeliveryRequest::STATUS_IN_PROGRESS
                    ]);
                }
                $deliveryRequest->route = $this->route;

                $changedOrderColumns = [];
                $changedDeliveryRequestColumns = [];
                if ($rewrite || $order->canUpdateStatusTo($statusFromReport)) {
                    $order->status_id = $statusFromReport;
                    $order->report_status_id = $statusFromReport;
                    $changedOrderColumns[] = 'status_id';
                    $changedOrderColumns[] = 'report_status_id';

                    if (OrderStatus::isFinalDeliveryStatus($statusMap[$statusFromReport])) {
                        $deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                    } elseif ($statusFromReport == OrderStatus::STATUS_DELIVERY_REJECTED) {
                        $deliveryRequest->status = DeliveryRequest::STATUS_ERROR;
                    } elseif ($statusMap[$statusFromReport]->type == OrderStatus::TYPE_MIDDLE) {
                        $deliveryRequest->status = DeliveryRequest::STATUS_IN_PROGRESS;
                    } else {
                        $deliveryRequest->status = DeliveryRequest::STATUS_PENDING;
                    }
                    $changedDeliveryRequestColumns[] = 'status';
                } elseif ($statusFromReport == $order->status_id) {
                    if ($order->report_status_id != $statusFromReport) {
                        $order->report_status_id = $statusFromReport;
                        $changedOrderColumns[] = 'report_status_id';
                    }
                } elseif ($order->couldUpdatedStatusFrom($statusFromReport)) {
                    if (empty($order->report_status_id) || $order->canUpdateStatusTo($statusFromReport,
                            $order->report_status_id)
                    ) {
                        $order->report_status_id = $statusFromReport;
                        $changedOrderColumns[] = 'report_status_id';
                    }
                } else {
                    $transaction->rollBack();
                    $record->addItemToErrors(DeliveryReportRecord::ERROR_INCORRECT_STATUS);
                    if (!$record->save(true, ['error_log', 'updated_at'])) {
                        throw new \Exception(Yii::t('common',
                            "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                            ['id' => $order->id, 'errors' => $record->getFirstErrorAsString()]));
                    }
                    if (!is_null($offset)) {
                        $offset++;
                    }
                    continue;
                }

                if ($report->type == DeliveryReport::TYPE_FINANCIAL) {
//                    TODO: вырезать, это старый функионал
                    $finance = $order->finance;
                    if (!$finance) {
                        $finance = new OrderFinance([
                            'order_id' => $order->id,
                        ]);
                    }
                    $finance->route = $this->route;

                    $costs = 0;
                    $financeColumns = DeliveryReportRecord::getPriceColumn();
                    if (!$record->report->currencies) {
                        throw new \Exception(Yii::t('common', 'Не заданы валюты отчета. Перейдите в раздел "Финансы".'));
                    }

                    $changedFinanceColumns = [];
                    $rates = [];

                    foreach ($financeColumns as $column => $attribute) {
                        if ($currID = $record->report->currencies->getAttribute($column . '_currency_id')) {
                            $rates[$currID] = $report->getRate($currID, $report->country->currency_id, null, $currentPayment ?? null);
                            if (empty($rates[$currID])) {
                                throw new \Exception(Yii::t('common', 'Отсутствует рейт для перевода из {from} в {to}',
                                    ['from' => Currency::findOne(['id' => $currID])->char_code, 'to' => Currency::findOne(['id' => $report->currency_id])->char_code]));
                            }
                        } else {
                            throw new \Exception(Yii::t('common', 'В финансовых колонках отчета отсутствует поле {field}', ['field' => $column . '_currency_id']));
                        }

                        $val = doubleval((!empty($record->$column) ? $record->$column : 0) / (!empty($rates[$currID]) ? $rates[$currID] : 1));
                        if ($column != DeliveryReportRecord::COLUMN_PRICE_COD) {
                            $costs += $val;
                        }
                        $finance->$attribute = $val;
                        $changedFinanceColumns[] = $attribute;
                    }


                    if ($statusFromReport == OrderStatus::STATUS_FINANCE_MONEY_RECEIVED) {
                        //https://2wtrade-tasks.atlassian.net/browse/ERP-675
                        if ($rate = $report->getRate($report->currencies->getAttribute('price_cod_currency_id', $report->country->currency_id), $report->country->currency_id, null, $currentPayment ?? null)) {
                            $priceCod = (float)$record->price_cod / $rate;
                        } else {
                            throw new \Exception(Yii::t('common', 'Отсутствует рейт для перевода из {from} в {to}',
                                ['from' => Currency::findOne(['id' => $report->currencies->getAttribute('price_cod_currency_id', $report->currency_id)])->char_code, 'to' => Currency::findOne(['id' => $report->currency_id])->char_code]));
                        }
                        if (!$contract || $contract->mutual_settlement == DeliveryContract::MUTUAL_SETTLEMENT_ON) {
                            $priceCod -= $costs;
                        }

                        if (!$ignoreBalance) {
                            if (!$currentPayment) {
                                throw new \Exception(Yii::t('common',
                                    'Для перевода заказов в статус "{status}" необходимо прикрепить хотя бы один платеж.',
                                    ['status' => Yii::t('common', 'Финансы (деньги получены)')]));
                            }
                            if ($rate = $report->getRate($currentPayment->currency_id, $report->country->currency_id, null, $currentPayment)) {
                                if ($priceCod > 0 && $report->sum_total) {
                                    if ($report->sum_total <= $priceCod) {
                                        $priceCod -= $report->sum_total;
                                        $report->sum_total = 0;
                                    } else {
                                        $report->sum_total -= $priceCod;
                                        $priceCod = 0;
                                    }
                                    if (!$report->save(true, ['sum_total'])) {
                                        throw new \Exception(Yii::t('common', 'Не удалось сохранить данные по причине: {reason}',
                                            ['reason' => $report->getFirstErrorAsString()]));
                                    }
                                }
                                if($priceCod > 0 && $report->getBalanceAdditionalCosts() > 0) {
                                    foreach ($report->costs as $cost) {
                                        if ($priceCod == 0) {
                                            break;
                                        }
                                        if ($cost->balance == 0) {
                                            continue;
                                        }
                                        if ($cost->balance <= $priceCod) {
                                            $priceCod -= $cost->balance;
                                            $cost->balance = 0;
                                        } else {
                                            $cost->balance -= $priceCod;
                                            $priceCod = 0;
                                        }
                                        if (!$cost->save(true, ['balance'])) {
                                            throw new \Exception(Yii::t('common', 'Не удалось сохранить данные по причине: {reason}',
                                                ['reason' => $report->getFirstErrorAsString()]));
                                        }
                                    }
                                }
                                while (($currentPayment->balance / $rate) < $priceCod) {
                                    if (count($payments) > 0) {
                                        $priceCod -= $currentPayment->balance / $rate;
                                        $currentPayment->balance = 0;
                                        if (!$currentPayment->save(true, ['balance'])) {
                                            throw new \Exception(Yii::t('common',
                                                'Неудалось сохранить изменение баланса в платежке "{name}".',
                                                ['name' => $currentPayment->name]));
                                        }

                                        $currentPayment = array_pop($payments);
                                        PaymentOrder::getDb()->createCommand('SELECT 1 FROM ' . PaymentOrder::tableName() . ' WHERE id = :id LOCK IN SHARE MODE', [':id' => (int)$currentPayment->id])->execute();
                                        $currentPayment->refresh();
                                        $rate = $report->getRate($currentPayment->currency_id, $report->country->currency_id, null, $currentPayment);
                                    } else {
                                        if (!$currentPayment) {
                                            $message = Yii::t('common',
                                                'Для перевода заказов в статус "{status}" необходимо прикрепить хотя бы один платеж.',
                                                ['status' => Yii::t('common', 'Финансы (деньги получены)')]);
                                        } else {
                                            $message = Yii::t('common',
                                                'Для перевода заказа в статус "{status}" недостаточно суммы на балансе отчета (по {price} : {cost}).',
                                                [
                                                    'status' => Yii::t('common', 'Финансы (деньги получены)'),
                                                    'price' => $contract && $contract->mutual_settlement == DeliveryContract::MUTUAL_SETTLEMENT_OFF ? 'price_cod' : 'delivery service',
                                                    'cost' => $priceCod
                                                ]);
                                        }

                                        if ($record_id) {
                                            throw new \Exception($message);
                                        } else {
                                            $responseErrors['balance_payment'] = $message;
                                            break;
                                        }
                                    }
                                }
                                if (($currentPayment->balance / $rate) < $priceCod) {
                                    $transaction->rollBack();
                                    if (!is_null($offset)) {
                                        $offset++;
                                    }
                                    continue;
                                }
                            } else {
                                throw new \Exception(Yii::t('common',
                                    'Не удалось определить курс валюты для указанной даты платежа.'));
                            }

                            $rate = null;
                            if ($rate = $report->getRate($currentPayment->currency_id, $report->country->currency_id, null, $currentPayment)) {
                                $balance = round($currentPayment->balance / $rate, 4);
                            } else {
                                throw new \Exception(Yii::t('common',
                                    'Не удалось определить курс валюты для указанной даты платежа.'));
                            }
                            $currentPayment->balance = ($balance - $priceCod) * $rate;
                            if (!$currentPayment->save(true, ['balance'])) {
                                throw new \Exception(Yii::t('common',
                                    'Неудалось сохранить изменение баланса в платежке "{name}".',
                                    ['name' => $currentPayment->name]));
                            }
                        } else {
                            if (!$currentPayment) {
                                throw new \Exception(Yii::t('common',
                                    'Для перевода заказов в статус "{status}" необходимо прикрепить хотя бы один платеж.',
                                    ['status' => Yii::t('common', 'Финансы (деньги получены)')]));
                            }
                        }
                        $tempPayments = $payments;
                        $paymentConnect = $currentPayment;
                        while ($deliveryRequest->created_at > $paymentConnect->getDeadlinePaid()) {
                            $paymentConnect = array_pop($tempPayments);
                            if (count($tempPayments) == 0) {
                                if (!$paymentConnect) {
                                    $message = Yii::t('common',
                                        'Для перевода заказов в статус "{status}" необходимо прикрепить хотя бы один платеж.',
                                        ['status' => Yii::t('common', 'Финансы (деньги получены)')]);
                                } else {
                                    $message = Yii::t('common',
                                        'Для перевода заказа в статус "{status}" необходимо, чтобы присутствовал платеж, оплаченный, позднее даты отправки заказа в службу доставки.',
                                        ['status' => Yii::t('common', 'Финансы (деньги получены)')]);
                                }

                                if ($record_id) {
                                    throw new \Exception($message);
                                } else {
                                    $responseErrors['null_payment'] = $message;
                                    break;
                                }
                            }
                        }

                        if ($deliveryRequest->created_at > $paymentConnect->getDeadlinePaid()) {
                            $transaction->rollBack();
                            if (!is_null($offset)) {
                                $offset++;
                            }
                            continue;
                        }

                        $deliveryRequest->paid_at = $paymentConnect->paid_at;
                        $changedDeliveryRequestColumns[] = 'paid_at';
                        $changedFinanceColumns[] = 'payment';
                    } elseif (!$contract || $contract->mutual_settlement == DeliveryContract::MUTUAL_SETTLEMENT_ON) {
                        $tempPayments = $payments;
                        $paymentConnect = $currentPayment;
                        while ($deliveryRequest->created_at > $paymentConnect->getDeadlinePaid()) {
                            $paymentConnect = array_pop($tempPayments);
                            if (count($tempPayments) == 0) {
                                if (!$paymentConnect) {
                                    $message = Yii::t('common',
                                        'Для перевода заказов в статус "{status}" необходимо прикрепить хотя бы один платеж.',
                                        ['status' => Yii::t('common', 'Финансы (деньги получены)')]);
                                } else {
                                    $message = Yii::t('common',
                                        'Для перевода заказа в статус "{status}" необходимо, чтобы присутствовал платеж, оплаченный, позднее даты отправки заказа в службу доставки.',
                                        ['status' => Yii::t('common', 'Финансы (деньги получены)')]);
                                }

                                if ($record_id) {
                                    throw new \Exception($message);
                                } else {
                                    $responseErrors['null_payment'] = $message;
                                    break;
                                }
                            }
                        }
                        if (!$paymentConnect || $deliveryRequest->created_at > $paymentConnect->getDeadlinePaid()) {
                            $transaction->rollBack();
                            if (!is_null($offset)) {
                                $offset++;
                            }
                            continue;
                        }
                        $rate = null;
                        if ($rate = $report->getRate($paymentConnect->currency_id, $report->country->currency_id, null, $paymentConnect)) {
                            $paymentConnect->balance = $paymentConnect->balance + $costs * $rate;
                            if (!$paymentConnect->save(true, ['balance'])) {
                                throw new \Exception(Yii::t('common',
                                    'Неудалось сохранить изменение баланса в платежке "{name}".',
                                    ['name' => $paymentConnect->name]));
                            }
                        } else {
                            throw new \Exception(Yii::t('common',
                                'Не удалось определить курс валюты для указанной даты платежа.'));
                        }

                        $deliveryRequest->paid_at = $paymentConnect->paid_at;
                        $changedDeliveryRequestColumns[] = 'paid_at';
                    }

                    $finance->payment = !empty($currentPayment) ? $currentPayment->name : null;

                    if (!$finance->save()) {
                        throw new \Exception(Yii::t('common',
                            "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                            ['id' => $order->id, 'errors' => $finance->getFirstErrorAsString()]));
                    }
                    $record->createFinanceFact($currentPayment);
                }

                $record->record_status = DeliveryReportRecord::STATUS_STATUS_UPDATED;
                $record->removeError(DeliveryReportRecord::ERROR_INCORRECT_STATUS);
                if (!$record->save(true, ['record_status', 'error_log', 'updated_at'])) {
                    throw new \Exception(Yii::t('common',
                        "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                        ['id' => $order->id, 'errors' => $record->getFirstErrorAsString()]));
                }
                if (!empty($changedOrderColumns)) {
                    $changedOrderColumns[] = 'updated_at';
                    if (!$order->save(false, $changedOrderColumns)) {
                        throw new \Exception(Yii::t('common',
                            "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                            ['id' => $order->id, 'errors' => $order->getFirstErrorAsString()]));
                    }
                }

                $unBuyoutReasons = $report->unBuyoutReasons;
                if (!empty($record->comment) && isset($unBuyoutReasons[$record->comment]) && !empty($unBuyoutReasons[$record->comment])) {
                    /**
                     * @todo Временная затычка, когда дойдем до переписывания этого метода, надо переписать. Если в отчете дублируются заказы, то при выставлении причин недоставки у первого все норм, а дубль уже не видит созданную ранее причину для этого же заказа и пытается создать еще одну
                     */
                    unset($order->deliveryRequest->deliveryRequestUnBuyoutReason);
                    $order->deliveryRequest->saveUnBuyoutReason($record->comment);
                }

                $dateColumns = [
                    DeliveryReportRecord::COLUMN_DATE_CREATED,
                    DeliveryReportRecord::COLUMN_DATE_APPROVE,
                    DeliveryReportRecord::COLUMN_DATE_RETURN,
                    DeliveryReportRecord::COLUMN_DATE_PAYMENT,
                ];

                $columnMap = DeliveryReportRecord::getDeliveryRequestColumns();

                $timezone = new \DateTimeZone($report->country->timezone->timezone_id);
                foreach ($dateColumns as $column) {
                    $shipCol = $columnMap[$column];
                    $trimmedDate = preg_replace("/(^\s+)|(\s+$)/us", "", $record->$column);
                    if (empty($trimmedDate) && $column == DeliveryReportRecord::COLUMN_DATE_APPROVE && empty($deliveryRequest->approved_at) && (OrderStatus::isFinalDeliveryStatus($order->status_id) || in_array($order->status_id,
                                [OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_DELIVERY_DENIAL]))
                    ) {
                        $deliveryRequest->$shipCol = $report->created_at;
                        $changedDeliveryRequestColumns[] = $shipCol;
                        continue;
                    } elseif (empty($trimmedDate)) {
                        continue;
                    }
                    $dateFormat = empty(trim($report->date_format)) ? $report->delivery->date_format : trim($report->date_format);
                    //закомментировать условие для локального теста
                    if (empty($dateFormat)) {
                        throw new \Exception(Yii::t('common', "Не указан формат дат"));
                    }
                    $dateTime = \DateTime::createFromFormat($dateFormat, $trimmedDate, $timezone);

                    //Раскомментировать при локальном тесте заглушку
                    /*
                    if (!$dateTime) {
                        $dateTime = new \DateTime();
                    }
                    */
                    if ($dateTime && $dateTime->getLastErrors()['warning_count'] == 0 && $dateTime->getLastErrors()['error_count'] == 0) {
                        $beginTime = new \DateTime();
                        $beginTime->setTimestamp($dateTime->getTimestamp());
                        $beginTime = new \DateTime($beginTime->format('H:i:s'));
                        $now = new \DateTime('now');

                        if (abs($now->getTimestamp() - $beginTime->getTimestamp()) <= 1) {
                            $beginTime->setTimestamp($dateTime->getTimestamp());
                            $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s',
                                $beginTime->format('Y-m-d 11:00:00')); // Ставим 14:00 по МСК
                        }
                    }
                    //закомментировать условие IF  - ELSEIF => IF для локального теста
                    if (!$dateTime || $dateTime->getLastErrors()['warning_count'] > 0 || $dateTime->getLastErrors()['error_count'] > 0 || $dateTime->getTimestamp() > strtotime('+1day') || (!empty($deliveryRequest->created_at) && $dateTime->getTimestamp() < strtotime("-1 day",
                                $deliveryRequest->created_at)) // буфер в -1 день от даты создания
                    ) {
                        $responseErrors[] = Yii::t('common', "Указана некорректная дата для заказа #{id}",
                            ['id' => $order->id]);
                    } elseif (empty($deliveryRequest->$shipCol)) {
                        $deliveryRequest->$shipCol = $dateTime->getTimestamp();
                        // Если из-за таймзоны вдруг оказалось, что КС получила заказ раньше, ставим дату отправки или дату создания
                        if ($deliveryRequest->$shipCol < $deliveryRequest->created_at) {
                            $sentAt = $deliveryRequest->sent_at;
                            if (empty($sentAt) || $sentAt < $deliveryRequest->created_at) {
                                $sentAt = $deliveryRequest->created_at;
                            }
                            $deliveryRequest->$shipCol = $sentAt;
                        }
                        $changedDeliveryRequestColumns[] = $shipCol;
                    }
                }

                if (!empty($changedDeliveryRequestColumns) || $deliveryRequest->isNewRecord) {
                    $changedDeliveryRequestColumns[] = 'updated_at';
                    if (!($deliveryRequest->isNewRecord ? $deliveryRequest->save() : $deliveryRequest->save(true,
                        $changedDeliveryRequestColumns))
                    ) {
                        throw new \Exception(Yii::t('common',
                            "Возникла ошибка при сохранении заказа #{id}:\n{errors}",
                            ['id' => $order->id, 'errors' => $deliveryRequest->getFirstErrorAsString()]));
                    }
                }

                $order->sendToCalculateDeliveryCharges();

                if (!empty($record->comment)
                    && ($trigger = CallCenterPenaltyType::getTriggerByString($record->comment))
                    && ($penaltyType = CallCenterPenaltyType::find()->byTrigger($trigger)->one())
                    && $penaltyType->active
                    && ($person = CallCenterPersonSearch::findPersonByOrderId($record->order_id))
                    && !CallCenterPenalty::find()->where([
                        'person_id' => $person->id,
                        'order_id' => $record->order_id,
                        'penalty_type_id' => $penaltyType->id
                    ])->exists()
                ) {
                    $penalty = new CallCenterPenalty([
                        'order_id' => $record->order_id,
                        'person_id' => $person->id,
                        'penalty_type_id' => $penaltyType->id,
                        'penalty_sum_usd' => $penaltyType->sum,
                        'penalty_percent' => $penaltyType->percent,
                    ]);
                    if (!$penalty->save()) {
                        throw new \Exception($penalty->getFirstErrorAsString());
                    }
                }

                if ($statusFromReport == OrderStatus::STATUS_DELIVERY_REJECTED) {
                    $deliveryControl = new DeliveryControl();
                    if ($otherDeliveryId = $deliveryControl->shouldSendOrderToOtherDelivery($order)) {
                        $deliveryControl->sendOrderToOtherDelivery($order, $otherDeliveryId);
                    }
                }

                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['status'] = 'fail';
                $response['message'] = $e->getMessage();

                $report->refresh();
                $response['report_balance'] = $this->getBalanceForWidget(
                    $report->getBalance(),
                    $report->country->currency->char_code
                );
                return $response;
            }
        }

        if (!is_null($offset)) {
            $response['offset'] = $offset;
        }

        $response['status'] = 'success';
        $response['errors'] = array_values($responseErrors);
        $response['report_balance'] = $this->getBalanceForWidget(
            $report->getBalance(),
            $report->country->currency->char_code
        );
        return $response;
    }

    /**
     * @param int $id
     * @return AnswerInterface
     */
    public function actionAddPayment(int $id): AnswerInterface
    {
        $response = new Answer();

        try {
            $report = $this->findModel($id);

            $ratesPost = Yii::$app->request->post('PaymentCurrencyRate', ['currency_id_from' => null, 'currency_id_to' => null, 'rate' => null]);

            /**
             * @var PaymentCurrencyRate[] $rates
             */
            $rates = [];
            foreach ($ratesPost['rate'] as $key => $val) {
                if (!empty($val)) {
                    $rates[$key] = new PaymentCurrencyRate(['currency_id_from' => $ratesPost['currency_id_from'][$key], 'currency_id_to' => $ratesPost['currency_id_to'][$key], 'rate' => strtr($val, [',' => '.'])]);
                    if (!$rates[$key]->validate(['currency_id_from', 'currency_id_to', 'rate'])) {
                        throw new \Exception($rates[$key]->getFirstErrorAsString());
                    }
                }
            }

            $payment = new PaymentOrder(['delivery_id' => $report->delivery_id]);
            $payment->load(Yii::$app->request->post());
            $payment->paid_at = Yii::$app->formatter->asTimestamp($payment->paid_at);
            $payment->balance = $payment->sum = doubleval($payment->sum);
            $payment->source = PaymentOrder::SOURCE_DELIVERY_REPORT;
            if (!$payment->validate()) {
                $response->setMessage($payment->getFirstErrorAsString());
            } else {
                if (PaymentOrder::find()
                    ->joinWith('paymentDeliveryReports', false)
                    ->where([
                        'name' => $payment->name,
                        'delivery_report_id' => $report->id
                    ])
                    ->exists()
                ) {
                    throw new \Exception(Yii::t('common', 'Платеж с указанным номером был добавлен ранее.'));
                }

                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $payment->paid_at = strtotime("+11hours", $payment->paid_at); // 14:00 по МСК

                    if ($paymentTemp = PaymentOrder::find()->where(['name' => $payment->name, 'delivery_id' => $report->delivery_id])->one()) {
                        $payment = $paymentTemp;
                    }
                    if ($payment->save()) {
                        $paymentDeliveryReports = new PaymentDeliveryReport();
                        $paymentDeliveryReports->payment_id = $payment->id;
                        $paymentDeliveryReports->delivery_report_id = $report->id;
                        $paymentDeliveryReports->save();

                        if (!empty($rates) and is_array($rates)) {
                            foreach ($rates as $rate) {
                                $rate->payment_id = $payment->id;
                                if (!$rate->save()) {
                                    throw new \Exception(Yii::t('common', 'Не удалось сохранить данные по причине: {reason}',
                                        ['reason' => $report->getFirstErrorAsString()]));
                                }
                            }
                        }

                        if ($payment->currency_id != $report->country->currency_id) {
                            if (!($rate = $report->getRate($payment->currency_id, $report->country->currency_id, null, $payment))) {
                                throw new \Exception(Yii::t('common',
                                    'Не удалось определить курс валюты для указанной даты платежа.'));
                            }
                        }
                    } else {
                        throw new \Exception(Yii::t('common', 'Не удалось сохранить данные по причине: {reason}',
                            ['reason' => $payment->getFirstErrorAsString()]));
                    }
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
                $paymentsResult['sum'] = $paymentsResult['balance'] = 0;
                $paymentsDP = $report->getPaymentsWithStatus(true);
                foreach ($paymentsDP->getModels() as $model) {
                    $rate = $report->getRate($model->currency_id, $report->country->currency_id, null, $model);
                    $paymentsResult['sum'] += $model->sum / (!empty($rate) ? $rate : 1);
                    $paymentsResult['balance'] += $model->balance / (!empty($rate) ? $rate : 1);
                }

                $paymentWidget = new PaymentList([
                    'reportModel' => $report,
                    'paymentList' => $paymentsDP,
                    'paymentsResult' => $paymentsResult,
                ]);
                $response->setStatus(AnswerInterface::STATUS_SUCCESS)
                    ->setData([
                        'report_balance' => $this->getBalanceForWidget(
                            $report->getBalance(),
                            $report->country->currency->char_code
                        ),
                        'content' => $paymentWidget->renderPayments()
                    ]);
            }
        } catch (\Throwable $e) {
            $response->setMessage($e->getMessage());
        }
        return $response;
    }

    /**
     * @param int $id
     * @return AnswerInterface
     */
    public function actionRemovePayment(int $id): AnswerInterface
    {
        $response = new Answer();
        try {
            $reportId = trim(Yii::$app->request->get('report_id'));
            $report = $this->findModel($reportId);

            if (!PaymentDeliveryReport::find()
                ->where([
                    'payment_id' => $id,
                    'delivery_report_id' => $report->id
                ])
                ->exists()
            ) {
                $response->setMessage(Yii::t('common', 'Не удалось найти данный платеж.'));
                return $response;
            }

            /**
             * @var PaymentOrder $payment
             */
            $payment = PaymentOrder::findOne($id);


            if (!$payment) {
                throw new \Exception(Yii::t('common', 'Не удалось найти данный платеж.'));
            }

            $activePayments = $report->getPaymentsWithStatus(false, true);
            if (!in_array($payment->id, ArrayHelper::getColumn($activePayments, 'id'))) {
                throw new \Exception(Yii::t('common', 'Невозможно удалить платеж, т.к. он уже используется.'));
            }

            try {
                $transaction = Yii::$app->db->beginTransaction();
                PaymentDeliveryReport::deleteAll([
                    'payment_id' => $id,
                    'delivery_report_id' => $report->id
                ]);
                if (!$payment->getPaymentDeliveryReports()
                        ->exists() && $payment->source == PaymentOrder::SOURCE_DELIVERY_REPORT) {
                    if (!$payment->delete()) {
                        throw new \Exception(Yii::t('common', 'Не удалось удалить платеж. Причина: {reason}',
                            ['reason' => $payment->getFirstErrorAsString()]));
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            $report->refresh();
            $paymentsResult['sum'] = $paymentsResult['balance'] = 0;
            $paymentsDP = $report->getPaymentsWithStatus(true);
            foreach ($paymentsDP->getModels() as $model) {
                $rate = $report->getRate($model->currency_id, $report->country->currency_id, null, $model);
                $paymentsResult['sum'] += $model->sum / (!empty($rate) ? $rate : 1);
                $paymentsResult['balance'] += $model->balance / (!empty($rate) ? $rate : 1);
            }
            $paymentWidget = new PaymentList([
                'reportModel' => $report,
                'paymentList' => $paymentsDP,
                'paymentsResult' => $paymentsResult,
            ]);
            $response->setStatus(AnswerInterface::STATUS_SUCCESS)
                ->setData([
                    'report_balance' => $this->getBalanceForWidget(
                        $report->getBalance(),
                        $report->country->currency->char_code
                    ),
                    'content' => $paymentWidget->renderPayments(),
                ]);
        } catch (\Throwable $e) {
            $response->setMessage($e->getMessage());
        }
        return $response;
    }

    /**
     * @param int $id
     * @return AnswerInterface
     */
    public function actionChangeCosts(int $id): AnswerInterface
    {
        $response = new Answer();

        try {
            $report = $this->findModel($id);

            $totalCosts = doubleval(Yii::$app->request->post('total_costs'));
            $currencyId = Yii::$app->request->post('currency');
            $rate = Yii::$app->request->post('rate');

            if ($currencyId != $report->country->currency_id) {
                if (!count($report->payments)) {
                    $response['message'] = Yii::t('common',
                        'Для конвертации издержек в местную валюту необходимо прикрепить хотя бы один платеж.');
                    return $response;
                }

                $payments = $report->payments;
                $payment = array_pop($payments);
                $rate = strtr($rate, [',' => '.']);
//                TODO: переписать на новую конвертацию
                if (is_numeric($rate) && $rate != 0) {
                    $totalCosts = $totalCosts * $rate;
                } else {
                    $totalCosts = $this->convertValueToCurrency($currencyId, $payment->paid_at, $totalCosts, true);
                    $totalCosts = $this->convertValueToCurrency($report->country->currency_id, $payment->paid_at, $totalCosts);
                }
            }
            $report->sum_total -= $report->total_costs - $totalCosts;
            if ($report->sum_total >= 0) {
                $report->total_costs = $totalCosts;
                $report->rate_costs = $rate;

                if (!$report->save()) {
                    $response->setMessage(Yii::t('common', 'Не удалось сохранить изменения. Причина: {reason}',
                        ['reason' => $report->getFirstErrorAsString()]));
                } else {
                    $response->setMessage(Yii::t('common', 'Суммы издержек изменены.'))
                        ->setStatus(AnswerInterface::STATUS_SUCCESS)
                        ->setData([
                            'report_balance' => $this->getBalanceForWidget(
                                $report->getBalance(),
                                $report->country->currency->char_code),
                            'sum_total' => $report->sum_total,
                        ]);
                }
            } else {
                $response->setMessage(Yii::t('common',
                    'При изменении издержек на указанные значения, баланс отчета становится отрицательным.')
                );
            }
        } catch (\Throwable $e) {
            $response->setMessage($e->getMessage());
        }
        return $response;
    }

    /**
     * @return AnswerInterface
     */
    public function actionChangeAdditionalCost(): AnswerInterface
    {
        $answer = new Answer();
        $request = Yii::$app->request->post();
        $model = new DeliveryReportCosts();
        if (!empty($id = $request[$model->formName()]['id'])) {
            if (empty($model = DeliveryReportCosts::findOne($id))) {
                return $answer->setMessage(Yii::t('common', 'Не удалось найти указанные издержки.'));
            }
        }
        try {
            if ($model->load($request)) {
                $propertyList = null;
                if (!$model->getIsNewRecord()) {
                    $propertyList = ['from', 'to', 'sum', 'description', 'balance'];
                }
                if ($model->save(true, $propertyList)) {
                    $answer->setStatus(AnswerInterface::STATUS_SUCCESS)
                        ->setData([
                            'balance' => $this->getBalanceForWidget(
                                $model->deliveryReport->getBalance(),
                                $model->deliveryReport->country->currency->char_code
                            ),
                            'cost' => $model->getAttributes()
                        ]);
                } else {
                    $answer->setMessage($model->getFirstErrorAsString());
                }
            } else {
                $answer->setMessage(Yii::t('common', 'Не удалось загрузить модель из полученных данных'));
            }
        } catch (\Throwable $e) {
            $answer->setMessage($e->getMessage());
        }
        return $answer;
    }

    /**
     * @return AnswerInterface
     */
    public function actionDeleteAdditionalCost(): AnswerInterface
    {
        $answer = new Answer();
        $id = Yii::$app->request->post('id');
        if (empty($id) || empty($model = DeliveryReportCosts::findOne($id))) {
            return $answer->setMessage(Yii::t('common', 'Не удалось найти указанные издержки.'));
        } else {
            try {
                if ($model->delete()) {
                    $answer->setStatus(AnswerInterface::STATUS_SUCCESS)
                        ->setData([
                            'balance' => $this->getBalanceForWidget(
                                $model->deliveryReport->getBalance(),
                                $model->deliveryReport->country->currency->char_code
                            ),
                        ]);
                } else {
                    $answer->setMessage($model->getFirstErrorAsString());
                }
            } catch (\Throwable $e) {
                $answer->setMessage($e->getMessage());
            }
        }
        return $answer;
    }

    /**
     * @param integer $id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionCreateDuplicateOrder($id)
    {
        $report = $this->findModel($id);

        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $batchSize = 100;

        /**
         * @var DeliveryReportRecord[] $records
         */
        $records = [];
        $neededRelations = [
            'order.deliveryRequest',
            'firstDeliveryRequest.order.deliveryRequest',
            'deliveryRequest',
            'order.duplicateOrder',
            'firstDeliveryRequest.order.duplicateOrder',
        ];

        $offset = intval(Yii::$app->request->post('offset'));
        if (is_null($offset)) {
            $offset = 0;
        }

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::find()->where([
                'id' => $record_id,
                'record_status' => [DeliveryReportRecord::STATUS_ACTIVE]
            ])->with($neededRelations)->one();
            if (empty($record)) {
                $response['message'] = Yii::t('common', 'Не удалось найти указанную запись.');
                return $response;
            }
            $records[] = $record;
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);
            $query->andWhere([DeliveryReportRecord::tableName() . '.record_status' => [DeliveryReportRecord::STATUS_ACTIVE]]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, для которых возможно создать дубли, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }

            $query->with($neededRelations);

            if ($offset) {
                $query->offset($offset);
            }
            $query->limit($batchSize);
            $records = $query->all();
        }
        $records = DeliveryReportRecordSearch::findRelatedOrders($records, $report);

        try {
            foreach ($records as $record) {
                $order = $record->relatedOrder;

                if (empty($order)) {
                    if ($record_id) {
                        throw new \Exception(Yii::t('common', 'Невозможно найти заказ.'));
                    } else {
                        $offset++;
                        continue;
                    }
                }

                if ($order->country_id != $report->country_id) {
                    if ($record_id) {
                        throw new \Exception(Yii::t('common',
                            'Невозможно создать дубль заказа, т.к. он находиться в другой стране.'));
                    } else {
                        $offset++;
                        continue;
                    }
                }

                if (!$order->deliveryRequest || $order->deliveryRequest->delivery_id == $report->delivery_id) {
                    if ($record_id) {
                        throw new \Exception(Yii::t('common', 'Заказ уже находится в курьерке {name}.',
                            ['name' => $report->delivery->name]));
                    } else {
                        $offset++;
                        continue;
                    }
                }

                if ($order->duplicateOrder) {
                    if ($record_id) {
                        throw new \Exception(Yii::t('common', 'У заказа уже имеется дубль.'));
                    } else {
                        $offset++;
                        continue;
                    }
                }
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $orderCopy = $order->duplicateIntoAnotherDelivery($report->delivery, false);
                    if (!$orderCopy) {
                        throw new \Exception(Yii::t('common', 'Не удалось создать дубль.'));
                    }
                    $deliveryRequest = new DeliveryRequest();
                    $deliveryRequest->route = Yii::$app->controller->route;
                    $deliveryRequest->delivery_id = $report->delivery_id;
                    $deliveryRequest->order_id = $orderCopy->id;
                    $deliveryRequest->status = DeliveryRequest::STATUS_IN_PROGRESS;
                    $deliveryRequest->created_at = $order->deliveryRequest->created_at;
                    $deliveryRequest->updated_at = time();
                    $orderCopy->created_at = $order->created_at;
                    $deliveryRequest->detachBehavior('timestamp');

                    $orderCopy->status_id = OrderStatus::STATUS_DELIVERY_ACCEPTED;
                    $orderCopy->route = Yii::$app->controller->route;

                    if (!$orderCopy->save(false, ['status_id', 'created_at'])) {
                        throw new \Exception(Yii::t('common', 'Не удалось создать дубль. Причина: {reason}',
                            ['reason' => $orderCopy->getFirstErrorAsString()]));
                    }
                    if (!$deliveryRequest->save()) {
                        throw new \Exception(Yii::t('common', 'Не удалось создать дубль. Причина: {reason}',
                            ['reason' => $deliveryRequest->getFirstErrorAsString()]));
                    }
                    $record->order_id = "{$orderCopy->id}";
                    $record->removeError([
                        DeliveryReportRecord::ERROR_INCORRECT_ORDERID,
                        DeliveryReportRecord::ERROR_ORDER_NOT_EXIST,
                        DeliveryReportRecord::ERROR_FIND_ORDERID_BY_TRACK,
                        DeliveryReportRecord::ERROR_FROM_ANOTHER_DELIVERY,
                    ]);
                    $record->record_status = DeliveryReportRecord::STATUS_APPROVE;
                    if (!$record->save(true, ['order_id', 'record_status', 'error_log', 'updated_at'])) {
                        throw new \Exception(Yii::t('common', 'Не удалось создать дубль. Причина: {reason}',
                            ['reason' => $record->getFirstErrorAsString()]));
                    }

                    $transaction->commit();
                    $response['status'] = 'success';
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionExportWithoutReportOrders()
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $delivery_id = Yii::$app->request->post('delivery') ? Yii::$app->request->post('delivery') : Yii::$app->request->get('delivery');
        $dateFrom = Yii::$app->request->post('dateFrom') ? Yii::$app->request->post('dateFrom') : Yii::$app->request->get('dateFrom');
        $dateTo = Yii::$app->request->post('dateTo') ? Yii::$app->request->post('dateTo') : Yii::$app->request->get('dateTo');
        $filename = Yii::$app->request->post('filename');
        $offset = Yii::$app->request->post('offset');

        $exporter = new WithoutReportOrderExporter($filename, $delivery_id, $dateFrom, $dateTo);

        if (Yii::$app->request->get('getOffsetCount')) {
            $counts = $exporter->getOrderCount();
            $totalCount = $counts['withoutReportOrderCount'];
            if ($totalCount == 0) {
                $response['message'] = Yii::t('common',
                    'Записи для экспорта отсутствуют.');
                return $response;
            }
            $offsetCount = (integer)($totalCount / $exporter->batchSize) + 1;

            $response['status'] = 'success';
            $response['filename'] = $exporter->filename;
            $response['offsetCount'] = $offsetCount;
            return $response;
        }

        try {
            $exporter->generateFile($offset);
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            return $response;
        }

        $response['status'] = 'success';
        return $response;
    }

    /**
     * @return array
     */
    public function actionWithoutReportOrderCount()
    {
        $dateFrom = Yii::$app->request->get('date_from');
        $dateTo = Yii::$app->request->get('date_to');
        $delivery = Yii::$app->request->get('delivery');

        $exporter = new WithoutReportOrderExporter(null, $delivery, $dateFrom, $dateTo);
        $counts = $exporter->getOrderCount();

        return [
            'count' => $counts['withoutReportOrderCount'],
            'totalCount' => $counts['totalCount'],
        ];
    }

    /**
     * @param $name
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($name)
    {
        $path = DeliveryReport::getFileFullRoute($name);
        if (file_exists($path)) {
            Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Файл не найден.'));
        }
    }

    /**
     * @param $name
     * @throws NotFoundHttpException
     */
    public function actionDownloadTmpFile($name)
    {
        $path = DeliveryReport::getTmpPath() . DIRECTORY_SEPARATOR . $name;

        if (file_exists($path)) {
            Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Файл не найден.'));
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSendToChecking($id)
    {
        $model = $this->findModel($id);

        $model->status = DeliveryReport::STATUS_CHECKING_QUEUE;
        $model->save();

        $options = Yii::$app->request->queryParams;

        if (isset($options['id'])) {
            unset($options['id']);
        }

        return $this->redirect(array_merge($options, ['index']));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionSetReportComplete($id)
    {
        $model = $this->findModel($id);

        if ($model) {
            $model->status = DeliveryReport::STATUS_COMPLETE;
            $model->save();
        }

        $options = Yii::$app->request->queryParams;

        if (isset($options['id'])) {
            unset($options['id']);
        }

        return $this->redirect(array_merge($options, ['index']));
    }

    /**
     * @param $id
     * @return bool|Response
     * @throws NotFoundHttpException
     */
    public function actionSetReportClosed($id)
    {
        /** @var DeliveryReport $model */
        $model = $this->findModel($id);

        if ($model) {

            $model->status = DeliveryReport::STATUS_CLOSED;

            if ($model->save()) {

                if ($model->type == DeliveryReport::TYPE_FINANCIAL) {
                    $model->setFalseStatusOrderPretension();
                }

                $options = Yii::$app->request->queryParams;

                if (isset($options['id'])) {
                    unset($options['id']);
                }

                return $this->redirect(array_merge($options, ['index']));
            } else {
                yii::$app->notifier->addNotification(Yii::t('common', 'Произошла ошибка при закрытии отчета'));
            }
        } else {
            yii::$app->notifier->addNotification(yii::t('common', 'Отчет #{id} не найден', ['id' => $id]));
        }

        return true;
    }


    /**
     * @param OrderFinanceFact $fact
     * @param string $cell
     * @param PaymentOrder $payment
     * @return float
     */
    public function convertCurrency($fact, $cell, $payment)
    {
        return $fact->{$cell}
            ? ($fact->order->country->currency_id != $fact->{$cell . '_currency_id'})
                ? $this->convertValueToCurrency($fact->order->country->currency_id, $payment->paid_at, $fact->{$cell})
                : $fact->{$cell}
            : 0;

    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionIncreaseReportPriority($id)
    {
        $report = $this->findModel($id);
        if ($report) {
            $report->priority++;
            $report->save();
        }

        $options = Yii::$app->request->queryParams;

        if (isset($options['id'])) {
            unset($options['id']);
        }

        return $this->redirect(array_merge($options, ['index']));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionParse($id)
    {
        $model = $this->findModel($id);
        $model->deleteRecords();

        $model->status = DeliveryReport::STATUS_PARSING;
        $model->save();
        $report = new ReportScanner($model->id);

        $result = $report->Parse(DeliveryReport::getFileFullRoute($model->file));
        if (!empty($result)) {
            $model->status = DeliveryReport::STATUS_ERROR;
            $model->error_message = $result;
        } else {
            $model->error_message = "";
            $model->status = DeliveryReport::STATUS_CHECKING;
        }
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionCheck($id)
    {
        $model = $this->findModel($id);
        $model->status = DeliveryReport::STATUS_CHECKING;
        $model->save();

        $report = new ReportScanner($model->id);
        $result = $report->Check();

        if ($result) {
            $model->status = DeliveryReport::STATUS_COMPLETE;
        } else {
            $model->status = DeliveryReport::STATUS_IN_WORK;
        }
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionGetRateForCurrencyDate($id)
    {
        $response = [
            'status' => 'fail',
        ];

        $ratesPost = Yii::$app->request->post('PaymentCurrencyRate', ['currency_id_from' => null, 'currency_id_to' => null]);
        $date = Yii::$app->request->post('PaymentOrder')['paid_at'] ?? null;
        if ($ratesPost && $date) {
            $answer = [];
            foreach ($ratesPost['currency_id_from'] as $key => $from) {
                $from = intval($from);
                $to = intval($ratesPost['currency_id_to'][$key]);
                if ($from && $to && $from != $to && $rate = Currency::find()->convertValueToCurrency(1, $from, $to, $date)) {
                    $answer[] = ['from' => $from, 'to' => $to, 'rate' => round(1 / $rate, 4)];
                }
            }
            if (!empty($answer)) {
                $response['answer'] = $answer;
                $response['status'] = 'success';
                return $response;
            }
        }

        $response['status'] = 'fail';
        $response['message'] = Yii::t('common', 'Не удается определить курс валют для выбранной даты');
        return $response;
    }

    /**
     * @param int $id
     * @return array
     */
    public function actionSetCurrencies(int $id)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        $currency = DeliveryReportCurrency::findOne($id);
        if (!$currency) {
            $currency = new DeliveryReportCurrency(['delivery_report_id' => $id]);
        }
        $currency->load(Yii::$app->request->post(), 'DeliveryReportCurrency');
        if (!$currency->save()) {
            $response['message'] = $currency->getFirstErrorAsString();
            return $response;
        }
        DeliveryReportCurrencyRate::deleteAll(['delivery_report_id' => $id]);
        if ($rate = Yii::$app->request->post('DeliveryReportCurrencyRate')) {
            foreach ($rate['rate'] as $key => $val) {
                if ($val) {
                    $currencyRate = new DeliveryReportCurrencyRate([
                        'delivery_report_id' => $id,
                        'currency_id_from' => $rate['currency_id_from'][$key],
                        'currency_id_to' => $rate['currency_id_to'][$key],
                        'rate' => $val
                    ]);
                    if (!$currencyRate->save()) {
                        $response['message'] = $currencyRate->getFirstErrorAsString();
                        return $response;
                    }
                }
            }
        }
        $response['status'] = 'success';
        return $response;
    }

    /**
     * @param integer $id
     * @return DeliveryReport
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = DeliveryReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Отчет не найден.'));
        }
    }

    /**
     * @param $id
     * @return DeliveryReportRecord
     * @throws NotFoundHttpException
     */
    protected function findRecord($id)
    {
        if (($model = DeliveryReportRecord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }

    /**
     * @param $currency_id
     * @param $date
     * @param $sum
     * @param bool $toUsd
     * @return float|int
     * @throws \Exception
     */
    protected function convertValueToCurrency($currency_id, $date, $sum, $toUsd = false)
    {
        /**
         * @var CurrencyRateHistory[] $rates
         */
        $rates = CurrencyRateHistory::find()->where([CurrencyRateHistory::tableName() . '.currency_id' => $currency_id])
            ->andWhere(['<=', CurrencyRateHistory::tableName() . '.date', date('Y-m-d', strtotime("+5days", $date))])
            ->andWhere([
                '>=',
                CurrencyRateHistory::tableName() . '.date',
                date('Y-m-d', strtotime("-5days", $date))
            ])->all();
        if (empty($rates)) {
            throw new \Exception(Yii::t('common',
                'Не удалось определить курс валюты для указанной даты платежа.'));
        }
        $currentRate = array_pop($rates);
        foreach ($rates as $rate) {
            if ($rate->created_at >= $date) {
                $currentRate = $rate;
                break;
            }
        }
        if ($currentRate->rate == 0) {
            throw new \Exception(Yii::t('common',
                'Для данной даты платежа указан некорректный курс валюты.'));
        }

        return $toUsd ? $sum / $currentRate->rate : $sum * $currentRate->rate;
    }


    /**
     * @param $id
     * @return array
     */
    public function actionCreatePretension($id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        $report = $this->findModel($id);

        if ($report->type != DeliveryReport::TYPE_FINANCIAL) {
            $response['message'] = Yii::t('common',
                'Отчет не финансовый.');
            return $response;
        }

        $pretension_type = Yii::$app->request->post('pretension');

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->buildQuery(Yii::$app->request->queryParams, $id, []);
            $query->andWhere([
                DeliveryReportRecord::tableName() . '.record_status' => [
                    DeliveryReportRecord::STATUS_APPROVE,
                ]
            ]);

            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common',
                        'Записи, для которых возможно изменить данные, отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }

            $query->with(['order.deliveryRequest', 'order.finance', 'order.status']);
            $offset = intval(Yii::$app->request->post('offset'));
            if ($offset) {
                $offset *= $batchSize;
                $query->offset($offset);
            }
            $query->limit($batchSize);
            /**
             * @var DeliveryReportRecord[] $records
             */

            $records = $query->all();
        }

        $responseErrors = [];
        foreach ($records as $record) {
            try {
                $order = $record->relatedOrder;

                if (!($order instanceof Order)) {
                    $response['message'] = yii::t('common', 'Заказ не найден');
                    return $response;
                }
                /*
                $orderFinanceFact = new OrderFinanceFact([
                    'order_id' => $order->id
                ]);

                foreach (OrderFinancePrediction::getCurrencyFields() as $key => $val) {
                    $orderFinanceFact->$key = null;
                    $orderFinanceFact->$val = $order->deliveryRequest->delivery->country->currency->id;
                }
                */
                $statusPair = Json::decode($record->report->status_pair);
                //$status = $statusPair[$record->status] ?? false;

                $pretension = new OrderFinancePretension([
                    'status' => OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS,
                    'comment' => null,
                    'order_id' => $record->order_id,
                    'delivery_report_record_id' => $record->id,
                    'delivery_report_id' => $record->delivery_report_id,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);

                switch ($pretension_type) {
                    case OrderFinancePretension::PRETENSION_TYPE_PRICE:
                        // записывать в цод полученный по отчету
                        $pretension->type = OrderFinancePretension::PRETENSION_TYPE_PRICE;
                        /*
                        $orderFinanceFact->price_cod = $record->price_cod;

                        if (in_array($status, [OrderStatus::STATUS_FINANCE_MONEY_RECEIVED, OrderStatus::STATUS_DELIVERY_BUYOUT])) {
                            $orderFinanceFact->price_delivery = $record->price_delivery;
                            $orderFinanceFact->price_redelivery = $record->price_redelivery;
                            $orderFinanceFact->price_delivery_back = $record->price_delivery_back;
                            $orderFinanceFact->price_delivery_return = $record->price_delivery_return;
                            $orderFinanceFact->price_storage = $record->price_storage;
                            $orderFinanceFact->price_fulfilment = $record->price_fulfilment;
                            $orderFinanceFact->price_package = $record->price_package;
                            $orderFinanceFact->price_packing = $record->price_packing;
                            $orderFinanceFact->price_address_correction = $record->price_address_correction;
                            $orderFinanceFact->price_cod_service = $record->price_cod_service;
                        }
                        */
                        break;
                    case OrderFinancePretension::PRETENSION_TYPE_STATUS:
                        // записывать расход по тарифу цена возврата
                        $pretension->type = OrderFinancePretension::PRETENSION_TYPE_STATUS;

                        /*
                        $orderFinanceFact->price_delivery = $record->price_delivery;
                        $orderFinanceFact->price_redelivery = $record->price_redelivery;
                        $orderFinanceFact->price_delivery_back = $record->price_delivery_back;
                        $orderFinanceFact->price_delivery_return = $record->price_delivery_return;
                        $orderFinanceFact->price_storage = $record->price_storage;
                        $orderFinanceFact->price_fulfilment = $record->price_fulfilment;
                        $orderFinanceFact->price_package = $record->price_package;
                        $orderFinanceFact->price_packing = $record->price_packing;
                        $orderFinanceFact->price_address_correction = $record->price_address_correction;
                        $orderFinanceFact->price_cod_service = $record->price_cod_service;
                        */
                        break;
                    case OrderFinancePretension::PRETENSION_TYPE_DOUBLE:
                        // на каждый дубль создавать запись в таблице с расходами по тарифам по статусу дубля в отчете
                        $pretension->type = OrderFinancePretension::PRETENSION_TYPE_DOUBLE;
                        /*
                        $orderFinanceFact->price_storage = $record->price_storage;
                        $orderFinanceFact->price_fulfilment = $record->price_fulfilment;
                        $orderFinanceFact->price_packing = $record->price_packing;
                        $orderFinanceFact->price_package = $record->price_package;
                        $orderFinanceFact->price_address_correction = $record->price_address_correction;
                        $orderFinanceFact->price_delivery = $record->price_delivery;
                        $orderFinanceFact->price_redelivery = $record->price_redelivery;
                        $orderFinanceFact->price_delivery_back = $record->price_delivery_back;
                        $orderFinanceFact->price_delivery_return = $record->price_delivery_return;
                        $orderFinanceFact->price_cod_service = $record->price_cod_service;
                        $orderFinanceFact->price_vat = $record->price_vat;
                        if (in_array($status, [OrderStatus::STATUS_FINANCE_MONEY_RECEIVED])) {
                            $orderFinanceFact->price_cod = $record->price_cod;
                        }
                        */
                        break;
                    //https://2wtrade-tasks.atlassian.net/browse/ERP-672
                    case OrderFinancePretension::PRETENSION_TYPE_DIFF_TARIFF:
                        $pretension->type = OrderFinancePretension::PRETENSION_TYPE_DIFF_TARIFF;

                        /*
                        if (in_array($status, [OrderStatus::STATUS_FINANCE_MONEY_RECEIVED, OrderStatus::STATUS_DELIVERY_BUYOUT])) {
                            $orderFinanceFact->price_cod = $record->price_cod;
                        }

                        $orderFinanceFact->price_delivery = $record->price_delivery;
                        $orderFinanceFact->price_redelivery = $record->price_redelivery;
                        $orderFinanceFact->price_delivery_back = $record->price_delivery_back;
                        $orderFinanceFact->price_delivery_return = $record->price_delivery_return;
                        $orderFinanceFact->price_storage = $record->price_storage;
                        $orderFinanceFact->price_fulfilment = $record->price_fulfilment;
                        $orderFinanceFact->price_package = $record->price_package;
                        $orderFinanceFact->price_packing = $record->price_packing;
                        $orderFinanceFact->price_address_correction = $record->price_address_correction;
                        $orderFinanceFact->price_cod_service = $record->price_cod_service;
                        */
                        break;
                }

                /** @var OrderFinancePretension $pretension */
                if ($pretension->type) {
                    if (!$pretension->save()) {
                        $responseErrors[] = $pretension->getFirstErrorAsString();
                    }
                }

                /*
                $orderFinanceFact->delivery_report_id = $record->report->id;
                $orderFinanceFact->delivery_report_record_id = $record->id;

                if (!$orderFinanceFact->save()) {
                    $responseErrors[] = $orderFinanceFact->getFirstErrorAsString();
                }
                */


            } catch (\Throwable $e) {
                $response['message'] = $e->getMessage();
                $response['status'] = 'fail';
                return $response;
            }
        }
        if ($record_id && !empty($responseErrors)) {
            $response['message'] = array_shift($responseErrors);
            $response['status'] = 'fail';
        } else {
            $response['status'] = 'success';
            $response['errors'] = $responseErrors;
        }
        return $response;
    }

    /**
     * Помечает претензии для одного отчета как "удаленные"
     *
     * ERP-706
     *
     * @param integer $id ReportID
     * @return array array [status: ..., message: ..., offsetCount: ...]
     * @throws NotFoundHttpException
     */
    public function actionRemovePretension(int $id)
    {
        $response = [
            'status' => 'fail',
            'message' => ''
        ];

        $records = [];
        $batchSize = 100;

        if ($record_id = Yii::$app->request->post('id')) {
            $record = DeliveryReportRecord::findOne($record_id);
            if ($record) {
                $records[] = $record;
            } else {
                $response['message'] = Yii::t('common', 'Запись не найдена.');
                return $response;
            }
        } else {
            $searchModel = new DeliveryReportRecordSearch();
            $query = $searchModel->bindQueryForPretension(Yii::$app->request->queryParams, $id, []);
            if (Yii::$app->request->get('getOffsetCount')) {
                $totalCount = $query->count();
                if ($totalCount == 0) {
                    $response['message'] = Yii::t('common', 'Активные записи отсутствуют.');
                    return $response;
                }
                $offsetCount = (integer)($totalCount / $batchSize) + 1;

                $response['status'] = 'success';
                $response['offsetCount'] = $offsetCount;
                return $response;
            }
            $query->limit($batchSize);
            $records = $query->all();
        }

        $report = $this->findModel($id);

        if ($report->type != DeliveryReport::TYPE_FINANCIAL) {
            $response['message'] = Yii::t('common',
                'Отчет не финансовый.');
            return $response;
        }

        $responseErrors = [];
        foreach ($records as $record) {
            try {
                $order = $record->relatedOrder;
                if (!($order instanceof Order)) {
                    $response['message'] = yii::t('common', 'Заказ не найден');
                    return $response;
                }

                if (!OrderFinancePretension::disableByOrder($order->id)) {
                    $responseErrors[] = 'Не удалось удалить претензии в заказе №' . $order->id;
                }
            } catch (\Throwable $e) {
                $response['message'] = $e->getMessage();
                $response['status'] = 'fail';
                return $response;
            }
        }
        if ($record_id && !empty($responseErrors)) {
            $response['message'] = array_shift($responseErrors);
            $response['status'] = 'fail';
        } else {
            $response['status'] = 'success';
            $response['errors'] = $responseErrors;
        }
        return $response;
    }

    /**
     * @param float $balance
     * @param string $currencyCharCode
     * @return string
     */
    private function getBalanceForWidget(float $balance, string $currencyCharCode): string
    {
        return Yii::t('common', 'Баланс: {balance} {currency}',
            [
                'balance' => Yii::$app->formatter->asDecimal($balance, self::WIDGET_PRECISSION),
                'currency' => $currencyCharCode
            ]);
    }
}
