<?php
namespace app\modules\webhook;

use Yii;
use app\modules\auth\components\filters\AccessControl;
/**
 * Class Module
 * @package app\modules\webhook
 */
class Module extends \yii\base\Module
{
    public function behaviors()
    {
        Yii::$app->user->enableSession = false;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        // If this property is not set or empty, it means this rule applies to all roles.
                        //'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
}
