<?php
namespace app\modules\webhook\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class VpnUser
 * @package app\modules\webhook\models
 * @property string $user_id
 * @property string $user_pass
 * @property string $user_mail
 * @property string $user_fio
 * @property string $user_skype
 * @property integer $user_online
 * @property integer $user_enable
 * @property string $user_start_date
 * @property string $user_end_date
 * @property string $chief
 * @property mixed $dbConnection
 * @property integer $admin
 */
class VpnUser extends ActiveRecord
{

    /**
     * @return null|object
     */
    public static function getDb() {
        return Yii::$app->get('vpnDb');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','user_pass', 'user_fio', 'user_skype', 'chief', 'user_mail','user_start_date', 'user_end_date'], 'string'],
            [['user_online', 'user_enable','admin'], 'integer'],
        ];
    }
}