<?php

namespace app\modules\webhook\controllers;

use app\components\rest\Controller;
use app\models\Country;
use app\models\Notification;
use app\modules\api\models\ApiLog;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class BoxmeController
 * @package app\modules\webhook\controllers
 */
class BoxmeController extends Controller
{
    const BOXME_CHAR_CODE = 'BME'; // Чар код службы доставки BoxMe во внутренней системе, если заказ назначен на КС с другим чар кодом, обновления не будет!!!
    protected $countryChar;
    protected $log = '';

    const SUCCESS = true;

    /**
     * @param $countryChar
     * @return array
     */
    public function actionIndex($countryChar = 'ID')
    {
        $this->countryChar = $countryChar;
        $resultApiJson = file_get_contents('php://input');
        $country = Country::find()->byCharCode($countryChar)->one();
        $apiLog = new ApiLog([
            'type' => ApiLog::TYPE_DELIVERY,
            'input_data' => $resultApiJson,
            'country_id' => $country ? $country->id : null,
            'status' => ApiLog::STATUS_SUCCESS
        ]);

        $this->appendToLog($resultApiJson);
        try {
            if ($result = json_decode($resultApiJson)) {
                $orderId = (int)$result->OrderCode;
                $trackingCode = (string)$result->TrackingCode;
                $request = DeliveryRequest::find()->where(['order_id' => $orderId, 'tracking' => $trackingCode])->one();
                if (!$request) {
                    throw new \Exception("Can't find delivery request with order_id=\"{$orderId}\" and tracking=\"{$trackingCode}\".");
                }
                if ($request->delivery->char_code != static::BOXME_CHAR_CODE) {
                    throw new \Exception('Incorrect delivery char_code.');
                }

                $mutexName = "Boxme_{$orderId}";
                $this->mutex->acquire($mutexName, 10);
                try {
                    $order = $request->order;
                    $apiLog->country_id = $order->country_id;

                    switch ($result->StatusCode) {
                        case 100:
                            $status = OrderStatus::STATUS_DELIVERY_PENDING;
                            break;
                        case 200:
                        case 201:
                        case 202:
                        case 203:
                        case 210:
                        case 211:
                        case 212:
                        case 220:
                        case 230:
                        case 231:
                        case 232:
                        case 233:
                        case 235:
                        case 234:
                        case 236:
                        case 237:
                        case 300:
                        case 304:
                        case 310:
                        case 400:
                        case 410:
                        case 420:
                        case 430:
                        case 500:
                        case 510:
                        case 511:
                        case 520:
                        case 530:
                            $status = OrderStatus::STATUS_DELIVERY_ACCEPTED;
                            if (empty($request->accepted_at)) {
                                $request->accepted_at = $result->TimeStamp;
                            }
                            break;
                        case 610:
                        case 600:
                        case 700:
                        case 701:
                            $status = OrderStatus::STATUS_DELIVERY_DENIAL;
                            $request->approved_at = $result->TimeStamp;
                            break;
                        case 702:
                        case 703:
                            $status = OrderStatus::STATUS_DELIVERY_REJECTED;
                            break;
                        case 800:
                            $status = OrderStatus::STATUS_DELIVERY_BUYOUT;
                            $request->approved_at = $result->TimeStamp;
                            break;
                        default:
                            throw new \Exception("Unknown foreign status \"{$result->StatusCode}\".");
                    }

                    if ($order->status_id != $status) {
                        if (!$order->canChangeStatusTo($status)) {
                            throw new \Exception("Can't change status of order from \"{$order->status_id}\" to \"{$status}\".");
                        }

                        $transaction = Order::getDb()->beginTransaction();
                        try {
                            $order->status_id = $status;
                            if (!$order->save(false, ['status_id'])) {
                                throw new \Exception("Can't save order: {$order->getFirstErrorAsString()}.");
                            }
                            if (OrderStatus::isFinalDeliveryStatus($status)) {
                                $request->status = DeliveryRequest::STATUS_DONE;
                                $request->done_at = time();
                            } elseif ($order->status_id == OrderStatus::STATUS_DELIVERY_REJECTED) {
                                $request->status = DeliveryRequest::STATUS_ERROR;
                                if (!empty($result->Note)) {
                                    $request->api_error = $result->Note;
                                }
                            }
                            if (!$request->save()) {
                                throw new \Exception("Can't save delivery request: {$request->getFirstErrorAsString()}.");
                            }
                            $transaction->commit();
                            // Сохранение последнего ответа от сервиса
                            if ($lastUpdateResponse = $request->addUpdateResponse($resultApiJson)) {
                                $lastUpdateResponse->save();
                            }
                            $msg = "status_id of order #{$orderId} was changed to \"{$status}\".";
                            $this->addMessage($msg);
                            $this->appendToLog($msg);
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            throw $e;
                        }
                    } else {
                        $msg = "Status of order #{$orderId} already set to \"{$status}\".";
                        $this->addMessage($msg);
                        $this->appendToLog($msg);
                    }
                } catch (\Throwable $e) {
                    Yii::$app->notification->send(Notification::TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER,
                        [
                            'order_id' => $order->id,
                            'country' => $request->order->country->name,
                            'delivery' => $request->delivery->char_code,
                            'error' => $e->getMessage(),
                        ]
                    );
                    throw $e;
                } finally {
                    $this->mutex->release($mutexName);
                }
            } else {
                throw new \Exception("Can't decode input data.");
            }
        } catch (\Throwable $e) {
            $this->addMessage($e->getMessage());
            $this->appendToLog($e->getMessage());
            $apiLog->status = ApiLog::STATUS_FAIL;
        }
        $apiLog->output_data = json_encode($this->responseData['data'] ?? [], JSON_UNESCAPED_UNICODE);
        $apiLog->save();
        if ($apiLog->status == ApiLog::STATUS_SUCCESS) {
            $this->saveLog(true);
            return $this->success();
        }
        $this->saveLog();
        return $this->fail();
    }

    /**
     * @param bool $success
     */
    public function saveLog($success = false)
    {
        $append = '';
        if (!$success) {
            $append = 'error' . DIRECTORY_SEPARATOR;
        }

        $path = Yii::getAlias('@logs') . "/webhook/" . str_replace('Controller', '', $this->id) . "/$this->countryChar/$append";

        $fileName = $path . date('Y-m-d') . '.log';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($this->log)) {
            $msg = print_r($this->log, true);
        } else {
            $msg = (string)$this->log;
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        file_put_contents($fileName, $msg, FILE_APPEND);
    }

    /**
     * @param string $text
     * @param bool $eol
     */
    public function appendToLog($text, $eol = true)
    {
        $this->log .= $text . ($eol ? PHP_EOL : '');
    }
}