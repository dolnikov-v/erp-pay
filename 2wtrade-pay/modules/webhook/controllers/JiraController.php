<?php
namespace app\modules\webhook\controllers;

use app\components\rest\Controller;
use app\models\User;
use app\modules\webhook\models\VpnUser;
use Yii;
use yii\httpclient\Client;

/**
 * Class JiraController
 * @package app\modules\webhook\controllers
 */
class JiraController extends Controller
{

    private
        $message,
        $log,
        $errors = [];

    /**
     * @return bool
     */
    public function actionBlockUser()
    {
        $response = file_get_contents('php://input');

        /**
         * @var $blockedUser \stdClass
         */
        $result = json_decode($response);

        if (!$result) {
            $this->addMessage('Url доступен, данных не поступало!');
            return $this->success();
        }

        $blockedUser = $result->user;
        $message = '';
        $this->log = $response . PHP_EOL;

        try {
            $event = $result->webhookEvent;
            if ($event == 'user_deleted' || $this->isUserBlocked($blockedUser->self)) {
                if (!$this->tryFindPayUser($blockedUser)) {
                    $message .= "Пользователь " . $blockedUser->name . " не найден в пее." . PHP_EOL;
                } else $message .= "Пользователь " . $blockedUser->name . " заблокирован в пее." . PHP_EOL;

                if (!$this->tryFindVpnUser($blockedUser)) {
                    $message .= "Пользователь " . $blockedUser->name . " не найден в ВПН." . PHP_EOL;
                } else $message .= "VPN пользователя " . $blockedUser->name . " заблокирован." . PHP_EOL;

                if (!$this->deactivateConfluenceUser($blockedUser)) {
                    $message .= "Пользователь " . $blockedUser->name . " не найден в Confluence." . PHP_EOL;
                } else $message .= "Пользователь " . $blockedUser->name . " заблокирован в Confluence." . PHP_EOL;

                $this->saveLog($this->log . $message);
                $this->wrapMessage($blockedUser, $message);
                $this->mailThatUserBlocked($blockedUser,$this->message);
            }
        }
        catch(\Exception $exception) {
            $this->log .= print_r($this->errors, true) . PHP_EOL;
            $this->log .= print_r($exception->getMessage(), true) . PHP_EOL . $exception->getTraceAsString() . PHP_EOL;
            $this->saveLog($this->log);
            return $this->success();
        }

        $this->saveLog($this->log);
        //$this->addMessage($this->message);
        return $this->success();
    }

    /**
     * @param $url
     *
     * @param int $attempt
     * @return bool
     */
    private function isUserBlocked($url, $attempt = 1)
    {
        $userName = Yii::$app->params['jira']['credentials']['username'];
        $password = Yii::$app->params['jira']['credentials']['password'];

        if ($attempt > 10){
            return false;
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_USERPWD, $userName . ':' . $password);
        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($code>201) {
            $this->log .= print_r($response, true) . PHP_EOL;
            echo 'Jira api return code ' . $code . " ---   $attempt" . PHP_EOL;
            $this->errors[] = 'Jira api return code ' . $code;
            sleep(30);
            $attempt++;
            return $this->isUserBlocked($url,$attempt);
        }

        $result = json_decode($response);
        $this->log .= print_r($response, true) . PHP_EOL;
        return !$result->active;
    }

    /**
     * @param \stdClass $blockedUser
     * @return bool
     */
    public function tryFindPayUser($blockedUser)
    {
        $user =  User::find()->where(['username' => $blockedUser->name])->one();
        if (!$user && isset($user->emailAddress)) {
            $user =  User::find()->where(['email' => $blockedUser->emailAddress])->one();
        }
        if ($user) {
            $user->status = 1;
            return $user->save();
        }
        return false;
    }

    /**
     * @param \stdClass $blockedUser
     * @return bool
     */
    public function tryFindVpnUser($blockedUser)
    {
        /**
         * @var $vpnUser VpnUser
         */
        $vpnUser =  VpnUser::find()->where(['user_id' => $blockedUser->name])->one();
        if (!$vpnUser && isset($blockedUser->emailAddress)) {
            $vpnUser =  VpnUser::find()->where(['user_email' => $blockedUser->emailAddress])->one();
        }
        if ($vpnUser) {
            $vpnUser->user_enable = 0;
            $vpnUser->user_pass = substr(uniqid(),1,8);
            $vpnUser->user_fio = $vpnUser->user_fio . ' (заблокирован)';
            return $vpnUser->save();
        }
        return false;
    }

    /**
     * @param \stdClass $blockedUser
     * @return bool
     */
    public function deactivateConfluenceUser($blockedUser)
    {
        if (!$name = $this->extractNameFromEmail($blockedUser->emailAddress)) {
            return false;
        };

        $post = [
            "jsonrpc" => "2.0",
            "method" => "deactivateUser",
            "params" => [$name],
            "id" => 12345,
                ];

        $userName = Yii::$app->params['jira']['credentials']['username'];
        $password = Yii::$app->params['jira']['credentials']['password'];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(Yii::$app->params['confluence']['jsonRpcUrl'])
            ->setFormat('json')
            ->addHeaders(['Authorization' => 'Basic '.base64_encode("$userName:$password")])
            ->setData($post)
            ->send();

        if(!$response->isOk && ($result = json_decode($response->content)) && isset($result->error) && !$result->result){
            return false;
        }

        return true;
    }

    /**
     * @param string $email
     * @return string | bool
     */
    private function extractNameFromEmail($email)
    {
        return substr($email, 0, strrpos($email, '@'));
    }

    /**
     * @param \stdClass $blockedUser
     * @param string $message
     */
    private function wrapMessage($blockedUser, $message)
    {
        $this->message = "Пользователь $blockedUser->name был заблокирован в Jira" . PHP_EOL;
        $this->message .= "Поэтому была предпринята попытка автоматической блокировки пользователя в пее и впн." . PHP_EOL;
        $this->message .= "Данные пользователя: " . PHP_EOL;
        $this->message .= "    E-mail: " . $blockedUser->emailAddress?:$blockedUser->emailAddress . PHP_EOL;
        $this->message .= PHP_EOL;
        $this->message .= "    Имя в Jira: " . $blockedUser->displayName?:$blockedUser->displayName . PHP_EOL;
        $this->message .= PHP_EOL;
        $this->message .= '---------------------------------' . PHP_EOL . PHP_EOL;
        $this->message .= $message;
        $this->message .= '---------------------------------' . PHP_EOL . PHP_EOL;
        $this->message .= "Служба безопасности \"2Wtrade Rus\"" . PHP_EOL. PHP_EOL;
        $this->message .= "Спасибо, что Вы с нами!" . PHP_EOL;
    }

    /**
     * @param \stdClass $blockedUser
     * @param $message
     * @return bool
     */
    private function mailThatUserBlocked($blockedUser, $message)
    {
        $mailer = Yii::$app->mailer
            ->compose()
            ->setFrom(Yii::$app->params['noReplyEmail'])
            ->setTo(Yii::$app->params['jira']['notifiers'])
            ->setSubject("User " . $blockedUser->name . " blocked in Jira.")
            ->setTextBody($message);
        return $sent = $mailer->send();
    }

    /**
     * @param $msg
     * @param bool $err
     * @return bool
     */
    private function saveLog($msg, $err = false)
    {
        $path = Yii::getAlias('@logs') . DIRECTORY_SEPARATOR . 'webhook' . DIRECTORY_SEPARATOR . 'jira' . DIRECTORY_SEPARATOR;

        $fileName = $path . date('Y-m-d') . ($err ? '.err' : '.log');

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($msg)) {
            $msg = print_r($msg, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        if (file_put_contents($fileName, $msg, FILE_APPEND)) {
            return true;
        }
        return false;
    }
}