<?php

namespace app\modules\webhook\controllers;

use app\components\rest\Controller;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use yii\helpers\ArrayHelper;
use yii\httpclient\Exception;

/**
 * Class ExternalSourceRoleController
 * @package app\modules\webhook\controllers
 */
class ExternalSourceRoleController extends Controller
{
    const RESPONSE_STATUS_SUCCESS = 'success';

    /** @var array */
    public $rolesBySource = [];

    /**
     * example request
     * method POST
     * type: json
     *    url: http://2wtrade-pay.com/webhook/external-source-role/update
     *    json: {"source":"2wcall","roles":{"superadmin":"Суперадминистратор","curator":"Куратор",}}
     */
    public function actionUpdate()
    {
        $response = file_get_contents('php://input');
        $identificate = $this->identificateSource($response);

        if ($identificate['status'] == self::RESPONSE_STATUS_SUCCESS) {
            $external_source_id = $identificate['data']['external_source_id'][0];
            $roles = $identificate['data']['roles'][0];

            //proccess update
            try {
                //по договорённости определили - т.к. id нет у присылаемых ролей - будем определять их по имени

                //1. соберём информацию о роля удалённого источника на пее
                $this->getRolesBySource($external_source_id);
                //2. возможно какие либо роли были удалены, удалим и на пее
                $this->checkNotLiveRoles($external_source_id, $roles);
                //3. обновим описание ролей согласно стороннего источника
                $this->addRolesToSource($external_source_id, $roles);
                //4. обновим описание ролей на пее согласно описанием на удалённом источнике
                if ($this->updateRoleDescription($roles)) {
                    return $this->success();
                }
            } catch (Exception $e) {
                $this->addMessage($e->getMessage());
                return $this->fail();
            }
        }

        return $this->success();
    }

    /**
     * возможно какие либо роли были удалены, удалим и на пее
     * @param $external_source_id
     * @param $source_roles
     */
    public function checkNotLiveRoles($external_source_id, $source_roles)
    {
        $deadRoles = array_diff($this->rolesBySource, array_keys($source_roles));

        if (!empty($deadRoles)) {
            //удалим все роли, отсутствующие роли в источнике
            ExternalSourceRole::deleteAll([
                'external_source_id' => $external_source_id,
                'name' => $deadRoles
            ]);
        }
    }

    /**
     * Метод добавить отсутствующие роли пею
     * @param $external_source_id
     * @param $source_roles
     * @return array|bool
     */
    public function addRolesToSource($external_source_id, $source_roles)
    {
        $newNamesRoles = array_diff(array_keys($source_roles), $this->rolesBySource);

        foreach ($newNamesRoles as $k => $role) {
            $model = new ExternalSourceRole();
            $model->external_source_id = $external_source_id;
            $model->name = $role;
            $model->description = $source_roles[$role];

            if (!$model->save()) {
                $this->addMessage($model->getErrors());
                return $this->fail();
            }
        }

        return true;
    }

    /**
     * @param $source_roles
     * @return array|bool
     */
    public function updateRoleDescription($source_roles)
    {
        foreach ($this->rolesBySource as $id => $name) {
            $modelQuery = ExternalSourceRole::find()->byId($id);

            if ($modelQuery->exists()) {
                $model = $modelQuery->one();
                echo $id . ' - ' . $source_roles[$name] . PHP_EOL;
                $model->description = $source_roles[$name];

                if (!$model->save(false)) {
                    $this->addMessage($model->getErrors());
                    return $this->fail();
                }
            }
        }

        return true;
    }

    /**
     * @param $external_source_id
     */
    public function getRolesBySource($external_source_id)
    {
        $externalRolesCollectionBySource = ExternalSourceRole::find()->bySource($external_source_id)->all();
        $externalRoleBySource = ($externalRolesCollectionBySource) ? ArrayHelper::map($externalRolesCollectionBySource, 'id', 'name') : [];

        $this->rolesBySource = $externalRoleBySource;
    }

    /**
     * @param $response
     * @return bool|mixed
     */
    public function identificateSource($response)
    {
        try {
            $data = json_decode($response, 1);

            $externalSourceCollectionData = ExternalSource::getCollectionExternalSources();
            $externalSource = ($externalSourceCollectionData) ? ArrayHelper::map($externalSourceCollectionData, 'id', 'name') : [];

            if (in_array($data['source'], $externalSource)) {
                $flip = array_flip($externalSource);
                $this->addDataValue('external_source_id', $flip[$data['source']]);
                $this->addDataValue('roles', $data['roles']);

                return $this->success();
            } else {
                $this->addMessage('Error identificate source');

                return $this->fail();
            }
        } catch (Exception $e) {
            $this->addMessage($e->getMessage());
            return $this->fail();
        }
    }


}