<?php
namespace app\modules\webhook\controllers;

use app\components\rest\Controller;
use app\modules\webhook\models\SkypeUser;
use Yii;

/**
 * Class SkypeController
 * @package app\modules\webhook\controllers
 */
class SkypeController extends Controller
{
    const GRANT_TYPE = 'client_credentials';
    const COMMAND_SUBSCRIBE = '[subscribe]';
    const COMMAND_UNSUBSCRIBE = '[unsubscribe]';

    public static $token;

    public function actionGetData()
    {
        $content = file_get_contents("php://input");
        $data = json_decode($content, true);
        if (isset($data['recipient']['id']) && $data['recipient']['id'] === Yii::$app->params['skype']['bot_id']) {
            if (isset($data['text'])) {
                switch ($data['text']) {
                    /* подписаться на бота */
                    case self::COMMAND_SUBSCRIBE:
                        $skype_id = (isset($data['from']['id']) ? $data['from']['id'] : null);
                        if ($skype_id !== null) {
                            $user = SkypeUser::find()->where(['skype_id' => $skype_id])->limit(1)->one();
                            if ($user === null) {
                                $user = new SkypeUser();
                            }
                            $user->skype_id = $skype_id;
                            $user->skype_name = (isset($data['from']['name']) ? $data['from']['name'] : null);
                            $user->conversation_id = (isset($data['conversation']['id']) ? $data['conversation']['id'] : null);
                            $user->bot_id = (isset($data['recipient']['id']) ? $data['recipient']['id'] : null);
                            $user->bot_name = (isset($data['recipient']['name']) ? $data['recipient']['name'] : null);
                            $user->service_url = (isset($data['serviceUrl']) ? $data['serviceUrl'] : null);
                            $user->answer = $content;
                            if ($user->save()) {
                                try {
                                    self::sendMessage($user->conversation_id, $user->skype_name, $user->skype_id, $user->service_url, "You are subscribed");
                                } catch (\Exception $e) {
                                    // @todo: дописать логирование общения
                                }
                            }
                        }
                        break;
                    /* отписаться от бота */
                    case self::COMMAND_UNSUBSCRIBE:
                        $skype_id = (isset($data['from']['id']) ? $data['from']['id'] : null);
                        if ($skype_id !== null) {
                            $user = SkypeUser::find()->where(['skype_id' => $data['from']['id']])->limit(1)->one();
                            if ($user !== null) {
                                try {
                                    self::sendMessage($user->conversation_id, $user->skype_name, $user->skype_id, $user->service_url, "You are unsubscribed");
                                    $user->delete();
                                } catch (\Exception $e) {
                                    // @todo: дописать логирование общения
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    /*
     * @param string $message
     */
    public static function sendAllUsers($message)
    {
        $users = SkypeUser::find()->all();
        if (is_array($users)) {
            foreach ($users as $user) {
                self::sendMessage($user->conversation_id, $user->skype_name, $user->skype_id, $user->service_url, $message);
            }
        }
    }

    /**
     * @param int $id
     * @param string $message
     */
    public static function sendUserById(int $id, string $message): void
    {
        $user = SkypeUser::findOne($id);
        self::sendMessage($user->conversation_id, $user->skype_name, $user->skype_id, $user->service_url, $message);
    }

    /*
     * @param string $conversationId
     * @param string $skypeName
     * @param string $skypeId
     * @param string $serviceUrl
     * @param string $text
     */
    private static function sendMessage($conversationId, $skypeName, $skypeId, $serviceUrl, $text)
    {
        // @todo: дописать логирование общения
        self::$token = Yii::$app->cache->get('skype_token');
        if (self::$token === false) {
            $result = file_get_contents(Yii::$app->params['skype']['token_url'], false, stream_context_create([
                'http' => [
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query([
                        'client_id' => Yii::$app->params['skype']['client_id'],
                        'client_secret' => Yii::$app->params['skype']['client_secret'],
                        'grant_type' => self::GRANT_TYPE,
                        'scope' => Yii::$app->params['skype']['scope'],
                    ])
                ]
            ]));
            $tokenAnswer = json_decode($result, TRUE);
            self::$token = (isset($tokenAnswer['access_token']) ? $tokenAnswer['access_token'] : null);
            try {
                Yii::$app->cache->set('skype_token', self::$token, $tokenAnswer['expires_in']);
            } catch (\Exception $e) {

            }
        }

        $dataString = '
                        {
                          "type": "message",
                          "from": {
                            "id": "' . Yii::$app->params['skype']['bot_id'] . '",
                            "name": "' . Yii::$app->params['skype']['bot_name'] . '"
                          },
                          "conversation": {
                            "id": "' . $conversationId . '"
                          },
                          "recipient": {
                            "id": "' . $conversationId . '",
                            "name": "' . $skypeName . '"
                          },
                          "text": "' . $text . '",
                          "replyToId": "' . $skypeId . '"
                        }
                        ';
        $url = $serviceUrl . 'v3/conversations/' . $conversationId . '/activities/';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Bearer ' . self::$token . '',
                'Content-Length: ' . strlen($dataString)
            ]
        );

        try {
            $answer = curl_exec($ch);
            $str = json_encode($answer);
            SkypeUser::updateAll(['answer' => $str], ['skype_id' => $skypeId]);
        } catch (\Exception $e) {
            SkypeUser::updateAll(['answer' => $e->getMessage()], ['skype_id' => $skypeId]);
        }
    }
}