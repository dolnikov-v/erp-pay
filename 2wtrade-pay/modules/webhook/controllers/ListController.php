<?php
namespace app\modules\webhook\controllers;

use app\components\web\Controller;
use app\modules\order\models\OrderLogisticListRequest;
use Yii;

/**
 * Class ListController
 * @package app\modules\webhook\controllers
 */

class ListController extends Controller{

    const SUCCESS = true;

    /**
     * @param $hash string
     */
    public function actionIndex($hash)
    {
        try {
            $orderLogisticListRequest = OrderLogisticListRequest::find()->byHash($hash)->one();
            if ($orderLogisticListRequest) {

                if ($orderLogisticListRequest->status == OrderLogisticListRequest::STATUS_SENT) {
                    $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_RECEIVED;
                    if ($orderLogisticListRequest->save(true, ['status'])) {

                        $orderLogisticListRequest->list->onReceivedSaveStatus();

                        // большое спасибо что нажали ссылку в письме
                        echo "Thank you. Confirmation is accepted.";
                    }
                }
                else if ($orderLogisticListRequest->status == OrderLogisticListRequest::STATUS_RECEIVED) {
                    // у нас уже есть информация
                    echo "Thank you. Confirmation is accepted.";
                }
            }
            else {
                // ссылка неправильная
                die();
            }
        } catch (\Exception $e) {
            $this->saveLog($e->getMessage());
        }
    }

    /**
     * @param $msg
     * @param bool $success
     */
    public function saveLog($msg, $success = false)
    {
        $append = '';
        if (!$success) {
            $append = 'error' . DIRECTORY_SEPARATOR;
        }

        $path = Yii::getAlias('@logs')  . "/webhook/"  . str_replace('Controller','',$this->id) . "/$append";

        $fileName = $path . date('Y-m-d') . '.log';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($msg)) {
            $msg = print_r($msg, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        file_put_contents($fileName, $msg, FILE_APPEND);
    }
}