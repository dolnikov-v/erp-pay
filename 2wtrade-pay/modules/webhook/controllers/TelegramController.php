<?php
namespace app\modules\webhook\controllers;

use wtrade\TelegramApi\response\Error;
use app\components\rest\Controller;
use app\modules\telegram\components\Telegram;
use Yii;

/**
 * Class TelegramController
 * @package app\modules\webhook\controllers
 */
class TelegramController extends Controller
{
    /**
     * @var string
     */
    private $token = null;
    /**
     * @var string
     */
    private $inputToken = null;

    /**
     * init token by config
     */
    public function init()
    {
        $this->token = Yii::$app->params['telegramBot']['token'];
    }

    /**
     * Проверка авторизации
     * @param int|null $token
     * @return bool
     */
    private function checkToken($token = null):bool
    {
        if ($token)
        {
            $this->inputToken = $token;
        }
        if ($this->inputToken === $this->token)
        {
            return true;
        }
        return false;
    }

    /**
     * @param $token string
     */
    public function actionIndex($token)
    {
        try {
            if ($this->checkToken($token)) {
                $module = new Telegram($this->token);
                $module->runCommand();

                Yii::$app->response->statusCode = 200;
                return true;
            } else {
                // TODO: реализовать функцию перечисляющую активных ботов
            }
        } catch (\Exception $e) {
           echo $e->getMessage();
        }
        return false;
    }

    /**
     * @return Error|array|true
     */
    public function actionSetWebhook()
    {
        $module = new Telegram($this->token);
        return $module->setWebhook(10);
    }
}