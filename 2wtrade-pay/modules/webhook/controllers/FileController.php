<?php

namespace app\modules\webhook\controllers;


use app\components\web\Controller;
use app\helpers\Utils;
use yii\web\NotFoundHttpException;

/**
 * Class FileController
 * @package app\modules\webhook\controllers
 */
class FileController extends Controller
{
    /**
     * Список разрешенных для чтения директорий
     * @return array
     */
    protected function allowDirs(): array
    {
        return [
            \Yii::getAlias('@files'),
            \Yii::getAlias('@runtime'),
        ];
    }

    /**
     * @param string $hash
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public function actionIndex($hash)
    {
        $data = json_decode(gzdecode(base64_decode($hash)), true);
        if (empty($data['path']) || !file_exists($data['path']) || !$this->canDownloadFile($data['path'])) {
            throw new NotFoundHttpException(\Yii::t('common', 'Файл не найден.'));
        }
        \Yii::$app->response->sendFile($data['path'], $data['fileName'] ?? null);
        \Yii::$app->end();
    }

    /**
     * @param string $path
     * @return bool
     */
    protected function canDownloadFile(string $path): bool
    {
        foreach ($this->allowDirs() as $dir) {
            if (Utils::checkSubDirOfDir($path, $dir)) {
                return true;
            }
        }
        return false;
    }
}