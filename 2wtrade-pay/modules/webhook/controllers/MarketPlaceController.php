<?php

namespace app\modules\webhook\controllers;


use app\components\rest\Controller;
use app\modules\api\models\ApiLog;
use app\modules\marketplace\MarketPlaceModule;
use app\modules\order\models\Order;
use yii\log\Logger;
use yii\web\ForbiddenHttpException;

/**
 * Class MarketPlaceController
 * @package app\modules\webhook\controllers
 */
class MarketPlaceController extends Controller
{
    /**
     * Создание заказа из маркетплейса посредством вебхука
     * @throws ForbiddenHttpException
     */
    public function actionWebhook()
    {
        $marketplace = $this->findMarketPlaceByApiKey(\Yii::$app->request->get('access_token'));
        $postData = array_merge(\Yii::$app->request->post(), \Yii::$app->request->get());
        if (YII_ENV_DEV) {
            \Yii::$app->log->getLogger()->log($postData, Logger::LEVEL_INFO);
        }
        $apiLog = new ApiLog([
            'type' => ApiLog::TYPE_LEAD,
            'input_data' => json_encode($postData, JSON_UNESCAPED_UNICODE)
        ]);
        try {
            $this->responseData = $marketplace->handleWebHookRequest($postData);
            $apiLog->output_data = json_encode($this->responseData['data'], JSON_UNESCAPED_UNICODE);
            $apiLog->status = ApiLog::STATUS_SUCCESS;
        } catch (\Throwable $e) {
            $this->addMessage($e->getMessage());
            $apiLog->output_data = json_encode([
                'error' => \Yii::t('common', $e->getMessage()),
                'marketplace' => $marketplace->getId()
            ], JSON_UNESCAPED_UNICODE);
            $apiLog->status = ApiLog::STATUS_FAIL;
        }
        $apiLog->save();
        return $apiLog->status == ApiLog::STATUS_SUCCESS ? $this->success() : $this->fail();
    }

    /**
     * @param string $apiKey
     * @return \app\modules\marketplace\abstracts\extensions\MarketPlaceWithWebHookInterface|\app\modules\marketplace\abstracts\MarketPlaceInterface|null
     * @throws ForbiddenHttpException
     */
    public function findMarketPlaceByApiKey(string $apiKey)
    {
        $marketplace = $this->getMarketPlaceModule()->getMarketPlaceByApiKey($apiKey);
        if (!$marketplace) {
            throw new ForbiddenHttpException(\Yii::t('common', 'Доступ запрещен.'));
        }
        return $marketplace;
    }

    /**
     * @return MarketPlaceModule|null|\yii\base\Module
     */
    protected function getMarketPlaceModule()
    {
        return \Yii::$app->getModule('marketplace');
    }
}