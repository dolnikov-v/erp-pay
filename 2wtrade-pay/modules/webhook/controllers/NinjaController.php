<?php
namespace app\modules\webhook\controllers;

use app\components\rest\Controller;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class NinjaController
 * @package app\modules\webhook\controllers
 */

class NinjaController extends Controller{

    protected $secret;

    protected $countryChar;

    const SUCCESS = true;

    /**
     * @param $countryChar
     * @return array
     */
    public function actionIndex($countryChar)
    {
        $this->countryChar = $countryChar;
        $this->secret = Yii::$app->controller->module->params['ninja'][$this->countryChar]['secret'];

        $hmac_header = $_SERVER['HTTP_X_NINJAVAN_HMAC_SHA256'];
        $resultApiJson = file_get_contents('php://input');
        $msg = $resultApiJson . PHP_EOL;
        $msg .= "secret: $this->secret" . PHP_EOL;
        $msg .= "HMAC: $hmac_header". PHP_EOL;

        try {
            $verified = $this->verifyWebHook($resultApiJson, $hmac_header);
            if ($verified && ($result = json_decode($resultApiJson))) {
                $orderId = (int)$result->shipper_order_ref_no;
                $request = DeliveryRequest::findOne(['order_id' => $orderId]);
                $order = $request->order;
                $eventTime = strtotime("$result->timestamp GMT");

                switch ($result->status) {
                    case 'Completed':
                        $status = 15;
                        $request->approved_at= $eventTime;
                        break;
                    case 'Returned to Sender':
                        $status = 17;
                        $request->returned_at = $eventTime;
                        break;
                    case 'Cancelled':
                        $status = 16;
                        break;
                    case 'Pending Pickup':
                        $request->accepted_at = $eventTime;
                        $status = 12;
                        break;
                    default:
                        $msg .= "Error: wrong status --- $result->status" . PHP_EOL;
                        throw new \Exception($msg);
                }

                if ($order->status_id == $status) {
                    $msg .= "Статус идентичен -- $result->status" . PHP_EOL;
                    $this->saveLog($msg);
                    return $this->success();
                }

                $transaction = Yii::$app->db->beginTransaction();

                if (!$order->canChangeStatusTo($status)) {
                    $msg .= "Неразрешенное значение статуса old: $order->status_id" . PHP_EOL;
                    $this->saveLog($msg);
                    return $this->success();
                }

                $order->status_id = $status;
                $order->route = 'webhook';
                $order->save(false, ['status_id']);
                $msg .= "Order status_id is changed to $status" . PHP_EOL;

                if (OrderStatus::isFinalDeliveryStatus($status)) {
                    $request->status = DeliveryRequest::STATUS_DONE;
                    $request->done_at = time();
                }

                $request->route = Yii::$app->controller->route;
                $request->save();
                $this->saveLog($msg,self::SUCCESS);
                $transaction->commit();
                return $this->success();
            }
            else {
                $msg .= 'Error: verification failed --- ' . PHP_EOL;
                $this->saveLog($msg);
                return $this->fail();
            }
        } catch (\Exception $e) {
            $this->saveLog($e->getMessage());
            return $this->fail();
        }
    }

    /**
     * Верификация пришедших данных
     * @param $data
     * @param $hmac_header
     * @return bool
     */
    protected function verifyWebHook($data, $hmac_header){
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $this->secret, true));
        return ($hmac_header == $calculated_hmac);
    }


    /**
     * @param $msg
     * @param bool $success
     */
    public function saveLog($msg, $success = false)
    {
        $append = '';
        if (!$success) {
            $append = 'error' . DIRECTORY_SEPARATOR;
        }

        $path = Yii::getAlias('@logs')  . "/webhook/"  . str_replace('Controller','',$this->id) . "/$this->countryChar/$append";

        $fileName = $path . date('Y-m-d') . '.log';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($msg)) {
            $msg = print_r($msg, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        file_put_contents($fileName, $msg, FILE_APPEND);
    }
}