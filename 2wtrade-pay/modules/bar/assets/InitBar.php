<?php
namespace app\modules\bar\assets;

use yii\web\AssetBundle;

/**
 * Class InitBar
 * @package app\modules\deliveryreport\assets
 */
class InitBar extends AssetBundle
{
    public $sourcePath = '@app/web/modules/bar/';

    public $js = [
        'bar.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}