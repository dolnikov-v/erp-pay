<?php
namespace app\modules\bar\assets;

use yii\web\AssetBundle;

/**
 * Class PackBar
 * @package app\modules\deliveryreport\assets
 */
class PackBar extends AssetBundle
{
    public $sourcePath = '@app/web/modules/bar/';

    public $js = [
        'bar_pack.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}