<?php
namespace app\modules\bar;

use yii\filters\AccessControl;
/**
 * Class Module
 * @package app\modules\bar
 */
class Module extends \app\components\base\Module
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
