<?php

use app\widgets\Panel;
use app\modules\bar\assets\ReturnBar;

/** @var yii\web\View $this */
/** @var app\modules\order\models\Order $orderModel */
ReturnBar::register($this);


$this->title = Yii::t('common', 'Возврат');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Штрих код'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'content' => $this->render('_index-form', [
        'orderModel' => $orderModel,
    ]),
    'collapse' => true,
]) ?>