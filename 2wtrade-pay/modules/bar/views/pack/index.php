<?php

use app\widgets\Panel;
use app\modules\bar\assets\PackBar;

/** @var app\modules\bar\models\Bar $barModel */
/** @var app\modules\order\models\Order $orderModel */

PackBar::register($this);

$this->title = Yii::t('common', 'Упаковка');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Ид заказа'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'content' => $this->render('_index-form', [
        'orderModel' => $orderModel,
        'barModel' => $barModel,
    ]),
    'collapse' => true,
]) ?>