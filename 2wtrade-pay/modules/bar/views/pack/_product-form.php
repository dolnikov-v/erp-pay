<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\bar\models\Bar $barModel */
/** @var  $orderId */
/** @var  array $products */
/** @var yii\web\View $this */
$n=0;
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['pack-bar']),
    'method' => 'post',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'name' => 'product-form',
    ]
]); ?>

<?php foreach ($products as  $product): ?>
    <?= $this->render('_product-form-input', [
        'form' => $form,
        'orderId' => $orderId,
        'model' => $barModel,
        'productName' => $product['name'],
        'productId' => $product['id'],
        'orderProductId' => $product['order_product_id'],
        'inputName' => ++$n,
    ]) ?>
<?php endforeach; ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Введите штрихкод товаров')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>