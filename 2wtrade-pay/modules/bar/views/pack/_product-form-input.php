<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\bar\models\Bar $model */
/** @var string $productName */
/** @var string $productId */
/** @var string $orderProductId */
/** @var  $inputName */
/** @var  $orderId */
?>

<div class="row" >
    <div class="col-lg-4">
        <?= $form->field($model, 'bar_code')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9{1,9}',
            'options' => ['placeholder' => 'Product barcode',
                'class' => 'product-input',
                'required' => true,
                'maxlength' => '15',
                'name' => $inputName,
                'data-order_id' => $orderId,
                'data-product_id' => $productId,
                'data-order_product_id' => $orderProductId,
            ]
        ])->label($productName) ?>
    </div>
</div>