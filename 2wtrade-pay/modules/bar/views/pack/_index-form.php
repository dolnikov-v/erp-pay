<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\order\models\Order $orderModel */
?>
<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['find']),
    'method' => 'post',
    'enableClientValidation' => false,
    'options' => [
        'name' => 'order-form',
    ]
]); ?>
<div class="row" >
    <div class="col-lg-4">
        <?= $form->field($orderModel, 'id')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9{1,16}',
            'options' => [
                'placeholder' => 'Order Id',
                'autofocus' => 'autofocus',
                'class' => 'order-input'
            ]
        ]) ?>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
                <?= $form->submit(Yii::t('common', 'Введите id заказа')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<div class="row">
    <div class="col-lg-4">
        <div id="products">
        </div>
    </div>
</div>