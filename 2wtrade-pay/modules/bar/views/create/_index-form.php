<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\bar\models\BarList $barListModel */
/** @var array $productsList */
/** @var array $storagesList */
?>
<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['generate-bar']),
    'method' => 'post',
    'enableClientValidation' => false,
//    'enableAjaxValidation' => true
]); ?>
<div class="row">
    <div class="col-lg-3">
        <br><br>
        <?= $form->field($barListModel, 'quantity')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9{1,3}',
            'options' => [
                'placeholder' => Yii::t('common', 'Количество штрих кодов'),
                'autofocus' => 'autofocus',
                'required' => 'required',
            ],
            'clientOptions' => [
                'removeMaskOnSubmit' => true,
        ]
        ]) ?>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($barListModel, 'storage_id', [
                'labelOptions' => ['label' => false]
            ])->storageProduct('storage_id', 'product_id', $barListModel->storage_id, $barListModel->product_id, true, 'shelf_life');
            ?>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
                <?= $form->submit(Yii::t('common', 'Сгенерировать штрих коды')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>