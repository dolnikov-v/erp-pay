<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\helpers\FontAwesome;
use yii\helpers\Html;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\bar\models\BarList $barListModel */
/** @var array $productsList */
/** @var array $storagesList*/

$this->title = Yii::t('common', 'Генерация штрих кодов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Штрих код'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'alert' => Yii::t('common', 'Склад - только физический и использующий сканер штрих-кодов'),
    'title' => Yii::t('common', 'Распечатка штрих кодов'),
    'content' => $this->render('_index-form', [
        'barListModel' => $barListModel,
        'productsList' => $productsList,
        'storagesList' => $storagesList,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица штрих кодов'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'formatType' => 'Date',
            ],
            [
                'headerOptions' => ['class' => 'text-center width-50'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'attribute' => 'file',
                'content' => function ($model) {
                    $content = '';
                /**@var $model  app\modules\bar\models\BarList */
                    if ($model->file) {
                        $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::BAR_CODE]);
                        $content = Html::a($icon, Url::toRoute(['download-file', 'name' => $model->file]), [
                            'class' => 'text-primary',
                            'data-plugin' => 'magnificPopup',
                        ]);
                    }
                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Наименование товара'),
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    /**@var $model  app\modules\bar\models\BarList */
                    return $model->product->name;
                }
            ],
            [
                'attribute' => 'quantity',
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
//                'class' => DateColumn::className(),
//                'formatType' => 'Date',
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
