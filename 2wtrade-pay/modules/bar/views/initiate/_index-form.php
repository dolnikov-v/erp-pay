<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\bar\models\Bar $barModel */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['initiate-barcode']),
    'method' => 'post',
    'enableClientValidation' => false,
    'options' => [
        'name' => 'bar-field'
    ]
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($barModel, 'bar_code')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '*{15}',
            'clientOptions' => [
                'removeMaskOnSubmit' => true,
            ],
            'options' => [
                'placeholder' => 'Scan or enter barcode',
                'autofocus' => 'autofocus',
                'required' => 'required',
                'class' => 'bar-input'
            ]
        ]) ?>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
                <?= $form->submit(Yii::t('common', 'Инициировать штрих код')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>