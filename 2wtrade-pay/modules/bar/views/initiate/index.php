<?php

use app\widgets\Panel;
use app\modules\bar\assets\InitBar;

/** @var yii\web\View $this */
/** @var app\modules\bar\models\Bar $barModel */


InitBar::register($this);

$this->title = Yii::t('common', 'Инициация штрих кодов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Штрих код'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'content' => $this->render('_index-form', [
        'barModel' => $barModel,
    ]),
    'collapse' => true,
]) ?>