<?php
namespace app\modules\bar\controllers;

use Yii;
use app\components\web\Controller;
use app\modules\bar\models\Bar;
use \yii\web\Response;

/**
 * Class InitiateController
 * @package app\modules\bar\controllers
 */
class InitiateController extends Controller{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $barModel = new Bar();
        return $this->render('index', [
            'barModel' => $barModel,
        ]);
    }

    public function actionInitiateBarcode(){
        $response = [
            'status' => '',
            'message' => '',
        ];
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        $barModel = new Bar();
        $barModel->load(Yii::$app->request->post());
        $barModel = Bar::findOne(['bar_code' => $barModel]);

        if (!$barModel) {
            $message = Yii::t('common', 'Товар не найден.');
            if(Yii::$app->request->isAjax) {
                $response['status'] = 'fail';
                $response['message'] = $message;
                return $response;
            }
        }
        $message = Yii::t('common', 'Товар уже отсканирован.');
        if ($barModel->status) {
            if(Yii::$app->request->isAjax) {
                $response['status'] = 'fail';
                $response['message'] = $message;
                return $response;
            }
        }
        $barModel->status = Bar::INITIATED;
        if ($barModel->save()) {
            $message = Yii::t('common', 'Сканирование прошло успешно.');
            if(Yii::$app->request->isAjax) {
                $response['status'] = 'success';
                $response['message'] = $message;
                return $response;
            }
        } else {
            $message = Yii::t('common', 'Ошибка сканирования.');
            if(Yii::$app->request->isAjax) {
                $response['status'] = 'success';
                $response['message'] = $message;
                return $response;
            }
        }
        return $this->redirect('index');
    }
}