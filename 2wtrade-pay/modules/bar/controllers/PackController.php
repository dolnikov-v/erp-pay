<?php
namespace app\modules\bar\controllers;

use app\modules\order\models\Order;
use Yii;
use app\components\web\Controller;
use app\modules\bar\models\Bar;
use \yii\web\Response;

/**
 * Class PackController
 * @package app\modules\bar\controllers
 */
class PackController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $barModel = new Bar();
        $orderModel = new Order();
        return $this->render('index', [
            'barModel' => $barModel,
            'orderModel' => $orderModel,
        ]);
    }

    public function actionFind()
    {
        $response = [
            'status' => '',
            'message' => '',
            'products' => [],
        ];
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $orderId = '';
        if (isset(Yii::$app->request->post()['order_id'])) {
            $orderId = Yii::$app->request->post()['order_id'];
        }

        $orderModel = Order::findOne($orderId);
        if (
            !$orderModel
            || ($orderModel->country_id != Yii::$app->user->country->id)
            && Yii::$app->request->isAjax
        ) {
            $message = Yii::t('common', 'Заказ не найден.');
            $response['status'] = 'fail';
            $response['message'] = $message;
            return $response;
        }
        $barModel = new Bar();
        $orderProducts = $orderModel->orderProducts;
        $products = [];

        $n=0;
        $data = [
            'orderId' => $orderId,
            'barModel' => $barModel
        ];
        $response['content'] = '';
        $message = Yii::t('common', 'Сканирование прошло успешно.');
        $response['status'] = 'success';
        $response['message'] = $message;
        if (Yii::$app->request->isAjax) {
            foreach ($orderProducts as $orderProduct) {
                $productId = $orderProduct->product_id;
                $productName = $orderProduct->product->name;
                for ($i = 1; $i <= $orderProduct->quantity; $i++) {
                    $products[$n] = [
                        'id' => $productId,
                        'name' => $productName,
                    ];
                    $n++;
                }
            }
        }
        $data['products'] = $products;
        $response['content'] = $this->renderPartial('_product-form', $data);
        return $response;
    }

    public function actionPackBar()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        $message = false;
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

        }
        $barData = Yii::$app->request->post();

        if ($this->haveSameElem($barData) && Yii::$app->request->isAjax) {
            $response['message'] = Yii::t('common', 'Введены одинаковые штрих коды.');
            return $response;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
        $orderId = $barData[0]['order_id'];
        foreach ($barData as $bar) {
            /**
             * @var Bar $barModel
             */
            if (!$barModel = Bar::findOne(['bar_code' => $bar['bar_code']])) {
                $message = Yii::t('common', "Товар не найден ") . $bar['bar_code'];
            }
            $productId = $bar["product_id"];
            if ($barModel->product_id != $productId) {
                $message = Yii::t('common', 'Штрих код не соответствует продукту ') . $bar['bar_code'] . " - " . $barModel->product_id;
            }
            if ($barModel->isPacked()) {
                $message = Yii::t('common', 'Товар уже отсканирован. ') . $bar['bar_code'];
            }

            $barModel->status = Bar::PACKED;
            $barModel->order_id = $bar['order_id'];
            if ($message || !$barModel->save()) {
                $transaction->rollBack();
                $response['message'] = $message;
                return $response;
            }
        }
        $order = Order::findOne(['id' => $orderId]);
        $order->status_id = 11;
        $order->route = "Bar code scanning";
        $order->shouldValidateStatusId = false;

        if (!$order->save()) {

            $message = Yii::t('common', 'Не удалось сохранить заказ');
            $response['message'] = $message;
            return $response;
        }

        $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage() . PHP_EOL;
            $message .= Yii::t('common', 'Не удалось сохранить заказ');
            $response['message'] = $message;
            return $response;
        }
        $message = Yii::t('common', 'Продукт отсканирован');
        if (Yii::$app->request->isAjax) {
            $response['status'] = 'success';
            $response['message'] = $message;
            return $response;
        }

        return $this->redirect('index');
    }

    /**
     * @param $barArray
     * @return bool
     */
    private function haveSameElem($barArray){
        if (($countElem = count($barArray))>1) {
            for ($i=0;$i<$countElem-1;$i++){
                if (in_array($barArray[$i]['bar_code'],$barArray)) {
                }
                if ($barArray[$i]['bar_code']==$barArray[$i+1]['bar_code']) {
                    return true;
                }
            }
        };
        return false;
    }
}