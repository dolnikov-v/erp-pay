<?php
namespace app\modules\bar\controllers;

use app\modules\order\models\Order;
use Yii;
use app\components\web\Controller;
use app\modules\bar\models\Bar;
use \yii\web\Response;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class PackController
 * @package app\modules\bar\controllers
 */
class ReturnController extends Controller{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $orderModel = new Order();
        return $this->render('index', [
            'orderModel' => $orderModel,
        ]);
    }

    public function actionReturn(){
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $orderId = Yii::$app->request->post('id');

        $order = Order::findOne(['id' => $orderId]);

        if (
            !$order
            || ($order->country_id != Yii::$app->user->country->id)
            && Yii::$app->request->isAjax
        ) {
            $message = Yii::t('common', 'Заказ не найден.');
            $response['message'] = $message;
            return $response;
        }

        $productsBar = Bar::findAll(['order_id' => $orderId]);
        if (!$productsBar && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response['message'] = Yii::t('common', 'Штрих коды продуктов не найдены.');
            return $response;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($productsBar as $bar) {
                $bar->status = Bar::RETURNED;
                $bar->order_id = null;
                if (!$bar->save()) {
                    throw new \Exception(Yii::t('common', 'Ошибка записи штрих кода'));
                }
            }
            $order->status_id = 17;
            $order->route = "Bar code scanning";
            $order->shouldValidateStatusId = false;

            if (!$request = $order->deliveryRequest) {
                throw new \Exception(Yii::t('common', 'Нет заявки в службу доставки'));
            };

            $request->status = DeliveryRequest::STATUS_DONE;
            $request->done_at = time();
            $request->route = Yii::$app->controller->route;
            if ($order->save() && $request->save()) {
                $transaction->commit();
                $response['status'] = 'success';
                $response['message'] = Yii::t('common', 'Заказ сохранен');
                return $response;
            };
        } catch (\Exception $e) {
            $message = $e->getMessage() . PHP_EOL;
            $message .= Yii::t('common', 'Не удалось сохранить заказ');
            $response['message'] = $message;
            $transaction->rollBack();
            return $response;
        }
        return $this->redirect('index');
    }
}