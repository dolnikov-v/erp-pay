<?php
namespace app\modules\bar\controllers;

use app\components\web\Controller;
use app\models\Product;
use app\modules\bar\models\BarList;
use app\modules\delivery\models\Delivery;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct;
use app\modules\bar\models\Bar;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;


/**
 * Class CreateController
 * @package app\modules\bar\controllers
 */
class CreateController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = BarList::find()->orderBy('created_at DESC');
        if (!Yii::$app->user->isSuperadmin) {
            $query->andWhere(['user_id' => Yii::$app->user->id]);
        }
        $productsList = Product::find()->collection();
        $deliveries = Delivery::find()->byCountryId(Yii::$app->user->country->id)->collection();
        $storagesList = Storage::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->isRealStorage()
            ->useBar()
            ->collection();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $barListModel = new BarList();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'barListModel' => $barListModel,
            'productsList' => $productsList,
            'deliveries' => $deliveries,
            'storagesList' => $storagesList,
        ]);
    }

    /**
     * @param $name
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($name)
    {
        $path = BarList::getDownloadPath($name);
        if (file_exists($path)) {
            Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Файл не найден.'));
        }
    }


    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionGenerateBar()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $barList = new BarList();
            $barList->setScenario(BarList::SCENARIO_CREATE);
            $barList->getFileName();
            $barList->load(Yii::$app->request->post());
            $uniqueIds = $barList->barUniqIdsGenerate();
            $barList->barPdfGenerateAndBarSave($uniqueIds);
            Yii::$app->response->sendFile($barList->filePath);
            if ($barList->save()) {
                $storageDocumentId = StorageDocument::createNewDocument(
                    StorageDocument::TYPE_RECEIPT,
                    $barList->product_id,
                    $barList->quantity,
                    null,
                    $barList->storage_id,
                    $barList->id,
                    null,
                    null,
                    null,
                    StorageDocument::SCENARIO_RECEIPT
                );
                Bar::updateAll(['bar_list_id' => $barList->id, 'storage_document_id' => $storageDocumentId, 'storage_id' => $barList->storage_id], ['bar_code' => $uniqueIds]);
                StorageProduct::addProductOnStorage($barList->product_id, $barList->storage_id, $barList->quantity);
                $transaction->commit();
                Yii::$app->notifier->addNotification(Yii::t('common', 'Лист успешно сгенерирован.'), 'success');
            } else {
                $transaction->rollBack();
                Yii::$app->notifier->addNotificationsByModel($barList);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось сгенерировать лист: {message}', ['message' => $e->getMessage()]));
        }
        return $this->redirect('index');
    }
}