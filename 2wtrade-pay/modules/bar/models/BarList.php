<?php
namespace app\modules\bar\models;

use TCPDF;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\User;
use Yii;
use app\models\Product;

/**
 * Class BarList
 * @package app\modules\bar\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $quantity
 * @property integer $product_id
 * @property integer $delivery_id
 * @property string $file
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 */
class BarList extends ActiveRecordLogUpdateTime
{
    const SCENARIO_CREATE = 'create';

    public $filePath;
    public $storage_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bar_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'quantity'], 'required'],
            [['created_at', 'updated_at', 'storage_id'], 'safe'],
            [['created_at', 'updated_at', 'user_id', 'quantity', 'product_id', 'delivery_id', 'storage_id'], 'integer'],
            [['file'], 'string', 'max' => 500],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['file', 'quantity'], 'required'],
            ['storage_id', 'required', 'on' =>
                [
                    self::SCENARIO_CREATE,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'quantity' => Yii::t('common', 'Количество штрих кодов в листе'),
            'product_id' => Yii::t('common', 'Товар'),
            'storage_id' => Yii::t('common', 'Склад'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'file' => Yii::t('common', 'Скачать файл'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $this->user_id = Yii::$app->user->id;
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return string
     */

    public function getFileName()
    {
        $todayStart = Yii::$app->formatter->asTimestamp(date('Y-m-d 00:00:00'));
        $todayEnd = $todayStart + 86399;
        $todayCount = $this->find()
            ->where(['>=', 'created_at', $todayStart])
            ->andWhere(['<=', 'created_at', $todayEnd])
            ->count();
        return $this->file = "bar" . date('ymd', time()) . str_pad($todayCount + 1, 5, '0', STR_PAD_LEFT);
    }

    /**
     * @param $name
     * @return string
     */
    public static function getDownloadPath($name)
    {
        $subDir = date('ym', time());
        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'bar' . DIRECTORY_SEPARATOR . $subDir . DIRECTORY_SEPARATOR;
        Utils::prepareDir($dir);
        return $dir . $name . '.pdf';
    }

    /**
     * @param array $uniqueIds
     * @param int $width
     * @param int $height
     * @param int $startX
     * @param int $startY
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function barPdfGenerateAndBarSave($uniqueIds, $width = 38, $height = 20, $startX = 10, $startY = 15)
    {
        $this->filePath = $this->getDownloadPath($this->file);
        $pdf = new TCPDF();
        $productList = Product::find()->collection();
        $pdf->setHeaderData('', 0, $productList[$this->product_id], '');
        $pdf->setHeaderMargin(3);
        $pdf->SetMargins(5, 10, 5);
        $pdf->SetAutoPageBreak(TRUE);
        $pdf->AddPage();
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $i = 1;
        foreach ($uniqueIds as $uniqueId) {
            //Запись в таблицу
            $Bar = new Bar();
            $Bar->bar_code = $uniqueId;
            $Bar->product_id = $this->product_id;
            $Bar->status = Bar::CREATED;
            $Bar->save();

            if (!($i % 5)) {
                $pdf->write1DBarcode($uniqueId, 'C128', $startX, $startY, $width, $height, 0.4, $style, 'N');

                $startY += $height;
                $startX = 10;
                if (!($i % 65)) {
                    $pdf->AddPage();
                    $startY = 15;
                }
            } else {
                $pdf->write1DBarcode($uniqueId, 'C128', $startX, $startY, $width, $height, 0.4, $style, 'N');
                $startX += $width;
            }
            $i++;
        }
        $pdf->Output($this->filePath, 'F');
    }

    /**
     * @return array
     */
    public function barUniqIdsGenerate()
    {
        $resultArray = null;
        for ($i = 0; $i <= $this->quantity; $i++) {
            //Генерация уникального кода
            $resultArray[] = uniqid('WT');
        }
        return $resultArray;
    }

}