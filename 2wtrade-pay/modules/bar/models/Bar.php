<?php
namespace app\modules\bar\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use app\models\Product;
use app\models\User;
use Yii;

/**
 * This is the model class for table "bar_codes".
 *
 * @property integer $id
 * @property string $bar_code
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property integer $product_id
 * @property integer $delivery_id
 * @property integer $order_id
 * @property integer $status
 * @property integer $storage_id
 * @property integer $storage_document_id
 *
 * @property Order $order
 * @property Product $product
 * @property User $user
 */

class Bar extends ActiveRecordLogUpdateTime{
    
    const CREATED = 'created';
    const INITIATED = 'initiated';
    const PACKED = 'packed';
    const BOUGHT = 'bought';
    const RETURNED = 'returned';
    const DEFECT = 'defect';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%bar_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bar_code'], 'required', 'skipOnEmpty' => false, 'message' => Yii::t('common', 'Заполните поле')],
            [['bar_code'], 'unique'],
            [['created_at', 'updated_at', 'user_id', 'product_id', 'delivery_id', 'order_id', 'storage_id', 'storage_document_id'], 'integer'],
            [['bar_code'], 'string', 'max' => 64],
            //['status', 'default', 'value' => self::CREATED],
            //['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'bar_code' => Yii::t('common', 'Штрих код'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'delivery_id' => Yii::t('common', 'Курьерская служба'),
            'product_id' => Yii::t('common', 'Продукт'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'status' => Yii::t('common', 'Статус товара'),
            'storage_id' => Yii::t('common', 'Склад'),
            'storage_document_id' => Yii::t('common', 'Документ'),
        ];
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $this->user_id = Yii::$app->user->id;
        if ($this->order_id) {
            try {
                    $this->delivery_id = Order::findOne(['order_id' => $this->order_id])->deliveryRequest->id;
                } catch (\Exception $e) {
                }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function isPacked (){
        return $this->status==self::PACKED;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * When order is bought all products bar codes status also  set to bought
     * @param $orderId
     */
    static function orderBuyOut($orderId)
    {
        $barCodes = self::findAll(['order_id' => $orderId]);
        foreach ($barCodes as $barCode) {
            $barCode->status = self::BOUGHT;
            $barCode->save(false, ['status']);
        }
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::CREATED => Yii::t('common', 'Создан'),
            self::INITIATED => Yii::t('common', 'Инициациирован'),
            self::PACKED => Yii::t('common', 'Упакован'),
            self::BOUGHT => Yii::t('common', 'Выкуплен'),
            self::RETURNED => Yii::t('common', 'Возващен'),
            self::DEFECT => Yii::t('common', 'Поврежден')
        ];
    }
}