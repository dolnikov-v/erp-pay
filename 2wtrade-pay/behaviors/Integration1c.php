<?php

namespace app\behaviors;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\abstracts\DirtyDataInterface;
use app\components\log\LoggerInterface;
use yii\base\Event;
use yii\db\Connection;
use yii\di\Instance;

/**
 * Class Integration1c
 *
 * @property string $queueName
 * @property \yii\redis\Connection $redis
 */
class Integration1c extends \yii\base\Behavior
{
    /**
     * @var string
     */
    public $queueName = 'integration1c';

    /**
     * @var \yii\redis\Connection
     */
    public $redis = 'redis';

    /**
     * @var string|array|LoggerInterface
     */
    public $logger;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, \yii\redis\Connection::class);
        if ($this->logger) {
            $this->logger = Instance::ensure($this->logger, LoggerInterface::class);
            $this->logger->setTags([
                'route' => __FILE__,
                'process_id' => getmypid(),
            ]);
        }
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Connection::EVENT_COMMIT_TRANSACTION => 'commitTransaction',
            ConnectionInterface::EVENT_COMMIT_WITHOUT_TRANSACTION => 'commitTransaction',
            ConnectionInterface::EVENT_ERROR_SAVE_CHANGES => 'errorSaveChanges',
        ];
    }

    /**
     * @param Event $event
     */
    public function commitTransaction(Event $event): void
    {
        /**
         * @var Connection $sender
         */
        $sender = $event->sender;
        if ($sender instanceof ConnectionInterface) {
            $uid = null;
            if (count($sender->getBufferChanges()) > 1) {
                $uid = \app\helpers\Utils::uid();
            }
            /**
             * @var DirtyDataInterface $change
             */
            foreach ($sender->getBufferChanges() as $change) {
                $data = $change->getAttributes();
                if ($uid) {
                    $data += ['group_id' => $uid];
                }
                $this->redis->lpush($this->queueName, json_encode($data, JSON_UNESCAPED_UNICODE));
            }
            $sender->clearBufferChanges();
        }
    }

    /**
     * @param Event $event
     */
    public function errorSaveChanges(Event $event): void
    {
        try {
            if ($event->sender instanceof ConnectionInterface) {
                if ($this->logger) {
                    $this->logger->log('Ошибка постановки в очередь на отправку в SQS', [
                        'messageBody' => json_encode($event->sender->getBufferChanges(), JSON_UNESCAPED_UNICODE)
                    ]);
                }
            }
        } catch (\Throwable $e) {
//            такого происходить не должно, но если вдруг - лучше проигнорить
        }
    }
}