<?php

namespace app\components\db;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\abstracts\DirtyDataInterface;
use app\components\db\models\DirtyData;
use Yii;

/**
 * Class Connection
 * @package app\components\db
 */
class Connection extends \yii\db\Connection implements ConnectionInterface
{
    /**
     * @var DirtyData[]
     */
    private $bufferChanges = [];

    /**
     * @inheritdoc
     */
    public function getBufferChanges(): array
    {
        return $this->bufferChanges ?? [];
    }

    /**
     * @inheritdoc
     */
    public function clearBufferChanges(): void
    {
        $this->bufferChanges = [];
    }

    /**
     * @inheritdoc
     */
    public function setBufferChanges(array $data): void
    {
        foreach ($data as $key => $record) {
            if (!($record instanceof DirtyData)) {
                throw new \Exception(Yii::t('common', 'Некорректный формат одного из значений аттрибута ({key}).', ['key' => "bufferChanges[{$key}]"]));
            }
        }
        $this->bufferChanges = $data;
    }

    /**
     * @inheritdoc
     */
    public function addBufferChange(DirtyDataInterface $dirtyData): void
    {
        try {
            if ($dirtyData->getData()) {
                if (!isset($this->bufferChanges[$dirtyData->getID()])) {
                    $this->bufferChanges[$dirtyData->getID()] = new DirtyData();
                    $this->bufferChanges[$dirtyData->getID()]->load($dirtyData->getAttributes(), '');
                } else {
//                    Приоритет: delete->insert->update
                    if (($this->bufferChanges[$dirtyData->getID()]->getAction() === DirtyDataInterface::ACTION_UPDATE)
                        || ($dirtyData->getAction() === DirtyDataInterface::ACTION_DELETE)
                        || ($dirtyData->getAction() === DirtyDataInterface::ACTION_INSERT
                            && $this->bufferChanges[$dirtyData->getID()]->getAction() !== DirtyDataInterface::ACTION_DELETE)) {
                        $this->bufferChanges[$dirtyData->getID()]->setAction($dirtyData->getAction());
                    }
                    $this->bufferChanges[$dirtyData->getID()]->load($dirtyData->getData());
                }
                if (!$this->getTransaction()) {
                    $this->trigger(static::EVENT_COMMIT_WITHOUT_TRANSACTION);
                }
            }
        } catch (\Throwable $e) {
            $this->trigger(static::EVENT_ERROR_SAVE_CHANGES);
        }
    }
}