<?php

namespace app\components\db;

use app\jobs\SqlErrorNotification;
use app\models\SqlErrorLog;
use Yii;

class Command extends \yii\db\Command
{
    /**
     * @inheritdoc
     */
    public function execute()
    {
        try {
            return parent::execute();
        } catch (\Exception $e) {
            $this->saveLog($e);
            throw $this->db->getSchema()->convertException($e, $this->getRawSql());
        }
    }

    /**
     * @inheritdoc
     */
    protected function queryInternal($method, $fetchMode = null)
    {
        try {
            return parent::queryInternal($method, $fetchMode);
        } catch (\Exception $e) {
            $this->saveLog($e);
            throw $this->db->getSchema()->convertException($e, $this->getRawSql());
        }
    }

    /**
     * @param \Exception $e
     */
    private function saveLog($e)
    {
        $log = new SqlErrorLog();
        $log->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
        $log->sql = $this->getRawSql();
        // записываем для идентификации подобных запросов без учета значений параметров
        $log->sql_hash = md5($this->sql);
        $log->message = $e->getMessage();
        $log->trace = $e->getTraceAsString();
        $log->notified_at = 0;
        if ($log->save()) {
            Yii::$app->queue->push(new SqlErrorNotification(['sqlErrorLog' => $log]));
        }
    }
}