<?php

namespace app\components\db\abstracts;

/**
 * Interface DirtyDataInterface
 * @package app\components\db\abstracts
 */
interface DirtyDataInterface
{
    const ACTION_INSERT = 'insert';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @return string
     */
    public function getID(): string;

    /**
     * @return string static::ACTION_INSERT|static::ACTION_UPDATE|static::ACTION_DELETE
     */
    public function getAction(): string;

    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @param null $names
     * @param array $except
     * @return array attribute values (name => value).
     */
    public function getAttributes($names = null, $except = []);
}