<?php

namespace app\components\db\abstracts;

/**
 * Interface ConnectionInterface
 * @package app\components\db\abstracts
 */
interface ConnectionInterface
{
    const EVENT_COMMIT_WITHOUT_TRANSACTION = 'commitWithoutTransaction';
    const EVENT_ERROR_SAVE_CHANGES = 'errorSaveChanges';

    /**
     * Запрос изменений из буфера
     * @return array
     */
    public function getBufferChanges(): array;

    /**
     * Задать буфер изменений
     * @param array $data
     * @throws \Exception
     */
    public function setBufferChanges(array $data): void;

    /**
     * Добавить в буфер изменений новые записи
     * @param DirtyDataInterface $dirtyData
     */
    public function addBufferChange(DirtyDataInterface $dirtyData): void;

    /**
     * Очистить буфер изменений
     */
    public function clearBufferChanges(): void;
}