<?php
namespace app\components\db\mysql;

use yii\db\mysql\Schema as MysqlSchema;

/**
 * Class Schema
 * @package app\components\db\mysql
 */
abstract class Schema extends MysqlSchema
{
    const TYPE_ENUM = 'enum';

}
