<?php
namespace app\components\db\mysql;

use yii\db\mysql\ColumnSchemaBuilder as MysqlColumnSchemaBuilder;

/**
 * Class ColumnSchemaBuilder
 * @package app\components\db\mysql
 */
class ColumnSchemaBuilder extends MysqlColumnSchemaBuilder
{
    const CATEGORY_ENUM = 'enum';

    /**
     * @var array
     */
    protected $enum;

    /**
     * ColumnSchemaBuilder constructor.
     * @param string $type
     * @param null $length
     * @param null $db
     * @param array $config
     */
    public function __construct($type, $length = null, $db = null, $config = [])
    {
        $this->categoryMap[Schema::TYPE_ENUM] = self::CATEGORY_ENUM;

        parent::__construct($type, $length, $db, $config);
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        if ($this->getTypeCategory() == self::CATEGORY_ENUM) {
            $format = '{type}{enum}{notnull}{unique}{default}{check}{comment}{pos}';

            $placeholderValues = [
                '{type}' => $this->type,
                '{enum}' => $this->buildEnumString(),
                '{notnull}' => $this->buildNotNullString(),
                '{unique}' => $this->buildUniqueString(),
                '{default}' => $this->buildDefaultString(),
                '{check}' => $this->buildCheckString(),
                '{comment}' => $this->buildCommentString(),
                '{pos}' => ($this->isFirst) ?
                    $this->buildFirstString() :
                    $this->buildAfterString(),
            ];

            return strtr($format, $placeholderValues);
        } else {
            return parent::__toString();
        }
    }

    /**
     * @param array $values
     * @return $this
     */
    public function enum($values)
    {
        $this->enum = $values;

        return $this;
    }

    /**
     * Формирует enum строку
     * @return string
     */
    protected function buildEnumString()
    {
        if ($this->enum === null || $this->enum === []) {
            return '';
        }

        return " ('" . implode("', '", $this->enum) . "')";
    }
}
