<?php

namespace app\components\db\models;

use app\components\db\abstracts\DirtyDataInterface;
use yii\base\Model;

/**
 * Class DirtyData
 * @package app\components\db\models
 */
class DirtyData extends Model implements DirtyDataInterface
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $action;
    /**
     * @var array
     */
    private $data;

    /**
     * @return string
     */
    public function getID(): string
    {
        return $this->id ?? 'undefined';
    }

    /**
     * @return string static::ACTION_INSERT|static::ACTION_UPDATE|static::ACTION_DELETE
     */
    public function getAction(): string
    {
        return $this->action ?? static::ACTION_UPDATE;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data ?? [];
    }

    /**
     * @param string $id
     * @return DirtyData
     */
    public function setId(string $id): DirtyData
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $action
     * @return DirtyData
     */
    public function setAction(string $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @param array $data
     * @return DirtyData
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function attributes()
    {
        return ['id', 'data', 'action'];
    }

    public function safeAttributes()
    {
        return ['id', 'data', 'action'];
    }
}