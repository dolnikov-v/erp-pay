<?php
namespace app\components\db;

/**
 * Class TimeLogActiveRecord
 * @package app\components\db
 */
class ActiveRecordLogUpdateTime extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    protected function getExcludedAttribute()
    {
        return ['created_at', 'updated_at'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    static::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
}
