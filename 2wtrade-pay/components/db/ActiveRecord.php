<?php

namespace app\components\db;

use app\components\ModelTrait;
use app\models\logs\DeleteData;
use app\models\logs\InsertData;
use app\models\logs\TableLog;
use app\models\logs\UpdateData;
use Codeception\Lib\Connector\Yii2\Logger;
use Yii;

/**
 * Class ActiveRecord
 * @package app\components\db
 * @property null $parentID
 * @property null $logID
 * @property null $linkData
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use ModelTrait;

    /**
     * Логировать создание новых записей в БД?
     * @var bool
     */
    protected $insertLog = false;

    /**
     * Логировать изменения записи?
     * @var bool
     */
    protected $updateLog = false;

    /**
     * Логировать удаление записи?
     * @var bool
     */
    protected $deleteLog = false;

    /**
     * @var array
     */
    private $_oldAttributes;

    /**
     * Исключаемые из логирования аттрибуты
     * @return array
     */
    protected function getExcludedAttribute()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->deleteLog) {
            $this->_oldAttributes = $this->attributes;
        }
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        try {
            if ($this->deleteLog) {
                $log = new TableLog();
                $log->table = self::clearTableName() ?? null;
                $log->route = $this->route ?? Yii::$app->controller->getRoute() ?? null;
                $log->id = $this->logID ?? $this->id ?? null;
                if (isset($this->parentID)) {
                    $log->parent_id = $this->parentID;
                }
                if ($this->linkData) {
                    $log->links = $this->linkData;
                }
                if (is_array($this->_oldAttributes)) {
                    $data = [];
                    foreach ($this->_oldAttributes as $key => $value) {
                        $data[] = new DeleteData(['field' => $key, 'value' => $value]);
                    }
                    $log->data = $data;
                }
                $log->type = TableLog::TYPE_DELETE;
                $log->save();
            }
        } catch (\Throwable $e) {
            Yii::getLogger()->log($e, Logger::LEVEL_ERROR);
        }

        parent::afterDelete();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        try {
            if ($this->insertLog || $this->updateLog) {
                $log = new TableLog();
                $data = [];
                if ($insert && $this->insertLog) {
                    $log->type = TableLog::TYPE_INSERT;
                    foreach ($this->prepareAttributesInsert($changedAttributes) as $item) {
                        $data[] = new InsertData($item);
                    }
                } elseif ($this->updateLog) {
                    $log->type = TableLog::TYPE_UPDATE;
                    foreach ($this->prepareAttributesUpdate($changedAttributes) as $item) {
                        $data[] = new UpdateData($item);
                    }
                }
                if (!empty($data)) {
                    $log->table = self::clearTableName() ?? null;
                    $log->route = $this->route ?? Yii::$app->controller->getRoute() ?? null;
                    $log->id = $this->logID ?? $this->id ?? null;
                    if (isset($this->parentID)) {
                        $log->parent_id = $this->parentID;
                    }
                    $log->data = $data;
                    if ($this->linkData) {
                        $log->links = $this->linkData;
                    }
                    $log->save();
                }
            }
        } catch (\Throwable $e) {
            Yii::getLogger()->log($e, Logger::LEVEL_ERROR);
        } finally {
            parent::afterSave($insert, $changedAttributes);
        }
    }

    /**
     * Переопределяемый ID для сущностей
     * @return null
     */
    protected function getLogID()
    {
        return null;
    }

    /**
     * Подготовка данных в формате [[field=>..., new=>...], [...], ...]
     * @param array|null $changedAttributes
     * @return array|null
     */
    protected function prepareAttributesInsert(?array $changedAttributes): ?array
    {
        $data = $this->prepareAttributesUpdate($changedAttributes);
        foreach ($data as &$item) {
            unset($item['old']);
        }
        return $data;
    }

    /**
     * Подготовка данных в формате [[field=>..., old=>..., new=>...], [...], ...]
     * @param array|null $changedAttributes
     * @param bool $compare
     * @return array|null
     */
    protected function prepareAttributesUpdate(?array $changedAttributes, bool $compare = true): ?array
    {
        $data = [];
        foreach ($changedAttributes as $key => $item) {
            if (!in_array($key, $this->excludedAttribute) && (!$compare || $this->getAttribute($key) != $item)) {
                $data[] = ['field' => (string)$key, 'old' => $item, 'new' => $this->getAttribute($key)];
            }
        }
        return $data;
    }

    /**
     * Массив дополнительных переменных по которым будет поиск, либо информационные.
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return null;
    }

    /**
     * @return null|string
     */
    public static function clearTableName()
    {
        return trim(static::tableName(), "\{\}\%") ?? null;
    }
}