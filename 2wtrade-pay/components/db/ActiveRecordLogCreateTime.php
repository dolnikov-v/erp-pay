<?php

namespace app\components\db;

/**
 * Class CreateTimeLogActiveRecord
 * @package app\components\db
 */
class ActiveRecordLogCreateTime extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    protected function getExcludedAttribute()
    {
        return ['created_at'];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}