<?php
namespace app\components\db;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ActiveQuery
 * @package app\components\db
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        $collection = ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);

        foreach ($collection as &$item) {
            $item = Yii::t('common', $item, [], $language);
        }

        return $collection;
    }

    /**
     * @param string $key
     * @param string $value
     * @return array
     */
    public function customCollection($key, $value)
    {
        $this->collectionKey = $key ?: $this->collectionKey;
        $this->collectionValue = $value ?: $this->collectionValue;
        $collection = ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);

        foreach ($collection as &$item) {
            $item = Yii::t('common', $item);
        }

        return $collection;
    }
}
