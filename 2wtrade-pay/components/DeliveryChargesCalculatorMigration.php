<?php

namespace app\components;

/**
 * Class DeliveryChargesCalculatorMigration
 * @package app\components
 */
abstract class DeliveryChargesCalculatorMigration extends CustomMigration
{
    /**
     * @return array
     *
     * [
     *      'Tunisia Express' => 'app\modules\delivery\components\charges\tunisiaexpress\TunisiaExpress',
     *      'Express Business Peru' => 'app\modules\delivery\components\charges\expressbusinessperu\ExpressBusinessPeru',
     * ]
     */
    protected abstract static function getCalculators();

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (static::getCalculators() as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (static::getCalculators() as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}