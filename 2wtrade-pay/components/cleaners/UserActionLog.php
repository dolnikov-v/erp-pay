<?php
namespace app\components\cleaners;

use app\models\UserActionLog as UserActionLogModel;

/**
 * Class UserActionLog
 * @package app\components\cleaners
 */
class UserActionLog
{
    /**
     * @return int
     */
    public static function clean()
    {
        /** @var UserActionLogModel[] $userLogs */
        $userLogs = UserActionLogModel::find()
            ->select('user_id')
            ->distinct()
            ->all();

        $totalCount = 0;

        foreach ($userLogs as $userLog) {
            /** @var UserActionLogModel $log */
            $log = UserActionLogModel::find()
                ->select('id')
                ->where(['user_id' => $userLog->user_id])
                ->orderBy(['id' => SORT_DESC])
                ->offset(100)
                ->limit(1)
                ->one();

            if ($log) {
                $time = strtotime('-14 day');

                $count = UserActionLogModel::deleteAll('user_id = :user_id AND created_at < :time AND id < :id', [
                    ':user_id' => $userLog->user_id,
                    ':time' => $time,
                    ':id' => $log->id,
                ]);

                $totalCount += $count;
            }
        }

        return $totalCount;
    }
}
