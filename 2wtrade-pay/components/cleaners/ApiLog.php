<?php
namespace app\components\cleaners;

use app\modules\api\models\ApiLog as ApiLogModel;

/**
 * Class ApiLog
 * @package app\components\cleaners
 */
class ApiLog
{
    /**
     * Очистка логов по запросам API
     * @param int $days За какое количество дней хранить логи
     * @param int $batchSize
     * @return int
     */
    public static function clean($days = 30, $batchSize = 500)
    {
        $time = strtotime("- {$days}day");

        $totalCount = 0;
        while ($ids = ApiLogModel::find()->where('created_at < :time', [
            ':time' => $time
        ])->limit($batchSize)->select('id')->column()) {
            $totalCount += ApiLogModel::deleteAll(['id' => $ids]);
        }

        return $totalCount;
    }
}
