<?php
namespace app\components\cleaners;

use app\models\UserNotification;
use yii\helpers\Console;

/**
 * Class Notification
 * @package app\components\cleaners
 */
class Notification
{
    /**
     * @param bool $delete
     */
    public static function clean($delete = false)
    {
        $time7 = strtotime('-7 day');
        $time14 = strtotime('-14 day');

        $readNotifications = UserNotification::find()
            ->where(['<', 'created_at', $time7])
            ->andWhere(['read' => UserNotification::READ])
            ->count();

        $allNotifications = UserNotification::find()
            ->where(['<', 'created_at', $time14])
            ->count();

        Console::output('Прочитанные оповещения на удаление: ' . $readNotifications);
        Console::output('Оповещений на удаление: ' . $allNotifications);

        if ($delete) {
            $removedReadNotifications = UserNotification::deleteAll(
                ['and', ['<', 'created_at', $time7], ['read' => UserNotification::READ]]
            );
            $removedAllNotifications = UserNotification::deleteAll(
                ['and', ['<', 'created_at', $time14],]
            );
            Console::output('Удалено прочитанных оповещений: ' . $removedReadNotifications);
            Console::output('Удалено старых оповещений: ' . $removedAllNotifications);
        }
    }
}
