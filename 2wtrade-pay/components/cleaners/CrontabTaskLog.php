<?php
namespace app\components\cleaners;

use app\modules\administration\components\crontab\Logger;
use app\modules\administration\models\CrontabTaskLog as CrontabTaskLogModel;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\report\extensions\Query;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class CrontabTaskLog
 * @package app\components\cleaners
 */
class CrontabTaskLog
{
    /**
     * @var array
     */
    protected static $files = [];

    /**
     * @var int
     */
    protected static $checkCount = 25000;

    /**
     * Очистка логов Crontab
     */
    public static function clean()
    {
        $time = strtotime('-7 day');

        $limit = 100;

        $query = CrontabTaskLogModel::find()
            ->joinWith(['answers'])
            ->where(['<', CrontabTaskLogModel::tableName() . '.created_at', $time])
            ->orderBy([CrontabTaskLogModel::tableName() . '.id' => SORT_ASC])
            ->limit($limit);

        while ($logs = $query->offset(0)->all()) {
            foreach ($logs as $log) {
                if ($log->answers) {
                    foreach ($log->answers as $answer) {
                        Logger::delete($answer->file_answer);
                    }
                }
            }

            $min = $logs[0]->id;
            $max = $logs[count($logs) - 1]->id;

            CrontabTaskLogModel::deleteAll('created_at < :time AND id >= :min AND id <= :max', [
                ':time' => $time,
                ':min' => $min,
                ':max' => $max,
            ]);
        }
    }

    /**
     * @param boolean $delete
     */
    public static function check($delete = false)
    {
        $packFiles = CrontabTaskLogAnswer::find()
            ->select('file_answer')
            ->where(['<>', 'file_answer', ""])
            ->limit(self::$checkCount)
            ->asArray();

        $interval = 0;
        $offsetPack = 0;
        $pathDir = Yii::getAlias('@logs') . DIRECTORY_SEPARATOR . 'crontab';

        Console::output('Сканирование директории...');
        self::scanFiles($pathDir);
        Console::output('-----------');

        while ($files = $packFiles->offset($offsetPack)->all()) {
            Console::output('Пакет ' . ($interval + 1) . '-' . ($interval + self::$checkCount));
            $files = ArrayHelper::getColumn($files, 'file_answer');
            $noExists = [];

            foreach ($files as $file) {
                if (array_key_exists($file, self::$files)) {
                    self::$files[$file] = 1;
                } else {
                    if (!file_exists(Logger::getFilePath($file))) {
                        $noExists[] = $file;
                    }
                }
            }

            if ($noExists) {
                Console::output('    Логов с несуществующими файлами: ' . count($noExists));
            }

            $interval = $interval + self::$checkCount;

            if ($delete) {
                $offset = 0;
                $limit = 50;

                while ($keys = array_slice($noExists, $offset, $limit)) {
                    CrontabTaskLogAnswer::deleteAll(['in', 'file_answer', $keys]);
                    $offset = $offset + $limit;
                }
            }

            $offsetPack = $offsetPack + self::$checkCount;
        }

        $unlinkedFiles = 0;

        foreach (self::$files as $file => $status) {
            if ($status == 0) {
                $unlinkedFiles++;
                if ($delete) {
                    Logger::delete($file);
                }
            }
        }
        Console::output('-----------');
        Console::output('Всего файлов: ' . count(self::$files));
        Console::output('Непривязанных файлов: ' . $unlinkedFiles);
    }

    /**
     * @param string $dir
     */
    protected static function scanFiles($dir)
    {
        $folders = scandir($dir);

        foreach ($folders as $folder) {
            if (in_array($folder, ['.', '..', '.gitkeep'])) {
                continue;
            }

            $fullPath = $dir . DIRECTORY_SEPARATOR . $folder;

            if (is_file($fullPath)) {
                self::$files[$folder] = 0;
            } else {
                Console::output($folder);
                self::scanFiles($fullPath);
            }
        }
    }
}
