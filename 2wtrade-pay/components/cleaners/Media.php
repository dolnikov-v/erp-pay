<?php
namespace app\components\cleaners;

use app\modules\media\components\Image;
use app\modules\media\models\Media as MediaModel;
use app\modules\media\models\Uploader;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class Media
 * @package app\components\cleaners
 */
class Media
{
    /**
     * @var array
     */
    protected static $files = [];

    /**
     * @param bool $delete
     */
    public static function clean($delete = false)
    {
        $pathToThumbnail = Image::getPathThumbnail();

        $mediaLinked = MediaModel::find()
            ->where('link_id != ""')
            ->all();

        $mediaUnlinked = MediaModel::find()
            ->where(['link_id' => null])
            ->all();

        self::scanFiles($pathToThumbnail);

        $originalFiles = ArrayHelper::getColumn($mediaUnlinked, 'filename');
        $thumbFiles = ArrayHelper::getColumn($mediaLinked, 'crop_file');
        $unlinkedThumb = array_diff(self::$files, $thumbFiles);

        foreach ($originalFiles as $originalFile) {
            $file = Uploader::getPathFile($originalFile);
            if (file_exists($file)) {
                if ($delete) {
                    unlink($file);
                }
            }
        }

        foreach ($unlinkedThumb as $thumbFile) {
            $file = $pathToThumbnail . DIRECTORY_SEPARATOR . substr($thumbFile, 0, 2) . DIRECTORY_SEPARATOR . $thumbFile;
            if (file_exists($file)) {
                if ($delete) {
                    unlink($file);
                }
            }
        }

        Console::output('Всего непривязанных записей: ' . count($mediaUnlinked));
        Console::output('Всего непривязанных оригинальных фото: ' . count($originalFiles));
        Console::output('Всего непривязанных обрезанных фото: ' . count($unlinkedThumb));

        if ($delete) {
            MediaModel::deleteAll(['link_id' => null]);
        }
    }

    /**
     * @param string $dir
     */
    protected static function scanFiles($dir)
    {
        $folders = scandir($dir);

        foreach ($folders as $folder) {
            if (in_array($folder, ['.', '..', '.gitkeep'])) {
                continue;
            }

            $fullPath = $dir . DIRECTORY_SEPARATOR . $folder;

            if (is_file($fullPath)) {
                self::$files[] = $folder;
            } else {
                self::scanFiles($fullPath);
            }
        }
    }
}

