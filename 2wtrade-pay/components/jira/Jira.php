<?php
namespace app\components\jira;

use JiraRestApi\Issue\IssueSearchResult;
use Yii;
use JiraRestApi\Issue\Comment;
use JiraRestApi\User\UserService;
use JiraRestApi\Issue\IssueType;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueService;
use yii\web\BadRequestHttpException;
use JiraRestApi\Configuration\ArrayConfiguration;

class Jira
{

    private
        $issueService,
        $config;

    public $parentIssueKey;

    /**
     * Пользователь которому назначется родительская задача
     * @var string
     */
    public $supervisor;

    /**
     * @var string | IssueType
     */
    public $issueType = 'Task';

    /**
     * @var string | IssueType
     */
    public $issueTypeSubTask = 'Sub-task';

    public
        $project = 'HDE',
        $priority = 'Major';

    public function __construct()
    {
        $this->config = new ArrayConfiguration(
            [
                'jiraHost' => Yii::$app->params['jira']['host'],
                'jiraUser' => Yii::$app->params['jira']['credentials']['username'],
                'jiraPassword' => Yii::$app->params['jira']['credentials']['password'],
            ]
        );
        $this->issueService = new IssueService($this->config);
        $this->supervisor = Yii::$app->params['jira']['supervisor'];
    }

    /**
     * @param IssueField[] $issueFields
     * @return array
     */
    public function createIssues($issueFields)
    {
        return $this->issueService->createMultiple($issueFields);
    }

    /**
     * Создает подзадачу
     * @param string $country
     * @param array $frozenOrdersByStatuses
     * @param string $countryUser
     * @return IssueField
     */
    public function buildIssueField($country, $frozenOrdersByStatuses, $countryUser)
    {
        $issueField = new IssueField();
        $date = date("F d", time());
        $issueField->setProjectKey($this->project)
            ->setIssueType($this->issueType)
            ->setSummary("Orders statuses delay $country " . $date)
            ->setPriorityName($this->priority)
            ->setDescription($this->descriptionText($frozenOrdersByStatuses));
        $issueField->setParentKeyOrId($this->parentIssueKey);
        $issueField
            //->addLabel('status_monitoring')
            ->addLabel($country)
            ->setAssigneeName($countryUser);
        //$issueField->labels = ['Monitoring', $country];
        return $issueField;
    }

    /**
     * Формирует описание задачи
     * @param array $frozenOrdersByStatuses
     * @return string
     */
    public function descriptionText($frozenOrdersByStatuses)
    {
        $text = '';
        foreach ($frozenOrdersByStatuses as $status => $data) {
            $text .= "{$data['count']} Orders in status $status";
            $text .= $data['norm'] == '(- 1 sec)' . PHP_EOL ? '' : " more than {$data['norm']}" . PHP_EOL;
            $text .= 'http://2wtrade-pay.com/' . strtolower($data['charCode']) . '/order/index/index?StatusFilter%5Bstatus%5D%5B%5D=' . $status;
            $text .= PHP_EOL;
        }
        return $text;
    }

    /**
     * Создает родительскую задачу
     * @param array $frozenOrdersGroupByStatuses
     * @return \JiraRestApi\Issue\Issue $issue
     */
    public function createMonitoringIssue($frozenOrdersGroupByStatuses)
    {
        $date = date("F d", time());
        $summary = 'Monitoring order statuses ' . $date;
        $summaryDescription = $this->buildSummaryDescription($frozenOrdersGroupByStatuses);
        $issueField = new IssueField();
        $issueField->setProjectKey($this->project)
            ->setPriorityName($this->priority)
            ->setSummary($summary)
            ->setAssigneeName($this->supervisor)
            ->setDescription($summaryDescription);
        $issueField
            ->setIssueType($this->issueType)
            ->addLabel('status_monitoring');
        $issueField->labels = ['monitoring'];

        /**
         * @var \JiraRestApi\Issue\Issue $issue
         */
        $issue = $this->issueService->create($issueField);
        $this->parentIssueKey = $issue->key;
        $this->saveMainTaskKey($issue->key);
        return $issue;
    }

    /**
     * Создает родительскую задачу
     * @param string $title
     * @param string $description
     * @param array $receivers
     * Возвращает ключи созданных задач
     * @return array | null
     */
    public function createTask($title, $description, $receivers)
    {
        $keys = null;
        if (is_array($receivers)) {
            foreach ($receivers as $receiver) {
                $issueField = new IssueField();
                $issueField->setProjectKey($this->project);
                $issueField->setPriorityName($this->priority)
                    ->setSummary($title);
                $issueField->setAssigneeName($receiver)
                    ->setDescription($description)
                    ->setIssueType($this->issueType);
                $keys[$receiver] = $this->issueService->create($issueField);
            }
        }
        return $keys;
    }

    /**
     * Создает подзадачу
     * @param string $parentKey
     * @param string $title
     * @param string $description
     * @param array $receivers
     * Возвращает array ответ создания подзадач
     * @return array
     */
    public function createSubTask($parentKey, $title, $description, $receivers)
    {
        $result = null;
        $issueFields = null;
        if (is_array($receivers)) {
            foreach ($receivers as $receiver) {
                $issueField = new IssueField();
                $issueField->setProjectKey($this->project);
                $issueField->setIssueType($this->issueTypeSubTask)
                    ->setPriorityName($this->priority)
                    ->setSummary($title);
                $issueField->setAssigneeName($receiver)
                    ->setDescription($description)
                    ->setParentKeyOrId($parentKey);
                $issueFields[] = $issueField;
            }
            if (is_array($issueFields)) {
                $result = $this->createIssues($issueFields);
            }
        }
        return $result;
    }

    /**
     * Формирует описание подзадачи
     * @param array $frozenOrdersGroupByStatuses
     * @return string
     */
    public function buildSummaryDescription($frozenOrdersGroupByStatuses)
    {
        $text = '';
        foreach ($frozenOrdersGroupByStatuses as $status => $frozenOrdersByCountry) {
            $summaryByStatuses[$status] = 0;
            $count = 0;
            $norm = '';
            foreach ($frozenOrdersByCountry as $countryData) {
                $norm = $countryData['norm'];
                $count += $countryData['count'];
            }
            $text .= "$count Orders in status $status";
            $text .= $norm == '(- 1 sec)' ? '' : " more than $norm";
            $text .= PHP_EOL;
        }
        return $text;
    }

    /**
     * @param $email
     * @return string|bool
     * @throws BadRequestHttpException
     */
    public function getJiraUserNameByEmail($email)
    {
        $userService = new UserService($this->config);
        $user = $userService->findUsers(['username' => $email]);
        return $user[0]->name??false;
    }

    /**
     * Сохраняет ключ оснвной задачи в файл для повторного мониторинга
     * @param $key
     */
    private function saveMainTaskKey($key)
    {
        $path = \Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'jira' . DIRECTORY_SEPARATOR;
        $fileName = $path . 'monitoring_task_key.txt';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        if (!file_put_contents($fileName, $key)) {
            echo "Error file with key no created" . PHP_EOL;
        };
    }

    /**
     * Считвыает ключ основной задачи из файла
     * @return string
     */
    private function getMainTaskKey()
    {
        $path = \Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'jira' . DIRECTORY_SEPARATOR;
        $fileName = $path . 'monitoring_task_key.txt';
        return file_get_contents($fileName);
    }

    /**
     *
     */
    public function getSubTasks()
    {
        if (!$this->parentIssueKey = $this->getMainTaskKey()) {
            echo "Error! Key file not found" . PHP_EOL;
            return false;
        };

        $issue = $this->issueService->get($this->parentIssueKey, [
            'fields' =>
                ['subtasks', 'labels']
        ]);

        $subTasks = [];

        foreach ($issue->fields->subtasks as $subTask) {
            $subTasks[$this->getSubTaskCountry($subTask->key)] = $subTask->key;
        }

        return $subTasks;
    }

    /**
     * @param $issueKey
     * @param $body
     */
    public function addComment($issueKey, $body)
    {
        $comment = new Comment();
        $comment->setBody($body);
        $this->issueService->addComment($issueKey, $body);
    }

    /**
     * @param string $issueKey
     * @return mixed
     */
    public function getSubTaskCountry($issueKey)
    {
        $issue = $this->issueService->get($issueKey, [
            'fields' =>
                ['labels']
        ]);
        return $issue->fields->labels[0];
    }

    /**
     * @param string $project
     * @param string $label
     * @param string $todo
     * @param string $process
     * @return IssueSearchResult
     */
    public function getTasks($label = '013b6796', $project = 'ORDEREDIT', $todo = '10300', $process = '10400')
    {
        return $this->issueService->search('project=' . $project . ' and (status=' . $todo . ' or status=' . $process . ') and labels=collector-' . $label);
    }
}