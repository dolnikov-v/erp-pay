<?php

namespace app\components;

use yii\db\Query;

/**
 * Class PermissionMigration
 * @package app\components
 */
class PermissionMigration extends CustomMigration
{
    /**
     * @var array
     * Example
     * ['catalog.product.edit.source']
     */
    protected $permissions = [];

    /**
     * @var array
     * Example
     * [
     *      'project.manager' => [
     *             'catalog.product.edit.source'
     *      ]
     * ]
     */
    protected $roles = [];

    /**
     * @var array
     */
    protected $roleList = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        foreach ($this->roleList as $role) {
            $this->roles[$role] = isset($this->roles[$role]) ? array_unique(array_merge($this->permissions, $this->roles[$role])) : $this->permissions;
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->addPermission($permission);
        }

        foreach ($this->roles as $role => $permissions) {
            $this->addRole($role);
            foreach ($permissions as $permission) {
                $this->addPermission($permission);

                $query = new Query();
                $exists = $query->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $permission
                ])->exists();

                if (!$exists) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $permission
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->roles as $role => $permissions) {
            foreach ($permissions as $permission) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $permission
                ]);
            }
        }
        foreach ($this->permissions as $permission) {
            $this->removeAuthItem($permission);
        }
    }

    /**
     * @param $name
     */
    protected function addPermission($name)
    {
        $query = new Query();
        $exists = $query->from($this->authManager->itemTable)
            ->where(['name' => $name, 'type' => 2])
            ->exists();
        if (!$exists) {
            $this->insert('{{%auth_item}}', [
                'name' => $name,
                'type' => '2',
                'description' => $name,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
    }

    /**
     * @param $name
     */
    protected function removeAuthItem($name)
    {
        $this->delete('{{%auth_item}}', ['name' => $name]);
    }

    /**
     * @param $name
     */
    protected function addRole($name)
    {
        $query = new Query();
        $exists = $query->from($this->authManager->itemTable)
            ->where(['name' => $name, 'type' => 1])
            ->exists();

        if (!$exists) {
            $this->insert($this->authManager->itemTable, [
                'name' => $name,
                'type' => 1,
                'description' => $name,
                'created_at' => time(),
                'updated_at' => time()]);
        }
    }
}