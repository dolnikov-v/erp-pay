<?php

namespace app\components\adcombo;

use app\models\Notification;
use app\modules\order\models\Lead;
use Yii;
use yii\helpers\Console;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Response;

class AdComboParser extends \yii\base\Component
{
    const MEMORY_DEBUG = false;
    const ERROR_AUTH = 'autorization failed';
    const END_PERIOD = 15;
    const MODE_MANUAL = 'manual';
    const MODE_STANDART = 'standart';
    const COUNT_ON_PAGE = 1000;

    protected $countApprove = 0;
    protected $countTrash = 0;
    protected $countCancelled = 0;

    /** @var  array */
    public $params;

    /** @var Client yii\httpclient\Client */
    public $client;
    /** @var  array */
    public $cookies;
    /** @var  array */
    public $period;
    /** @var array */
    public $adcombo_data = [];
    /** @var  string */
    public $error;
    /** @var  integer */
    public $start_time;
    public $end_time;
    /** @var  integer */
    public $baseMemory;

    public $mode = self::MODE_STANDART;


    public $countAllApprove = 0;
    public $countAllTrash = 0;
    public $countAllCancelled = 0;


    /**
     * AdComboParser constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->baseMemory = memory_get_usage();

        $this->client = new Client();
        $this->client->setTransport('yii\httpclient\CurlTransport');
        $this->setPeriod();

        parent::__construct($config);
    }

    protected function clearCountsPage()
    {
        $this->countApprove = 0;
        $this->countTrash = 0;
        $this->countCancelled = 0;
    }

    /**
     * in seconds
     * @return float
     */
    public function getTimeExecution()
    {
        return $this->end_time - $this->start_time;
    }

    /**
     * Финальные статусы АдКомобо
     * @return array
     */
    public function getFinalStatusesAdCombo()
    {
        return [
            Lead::STATUS_CONFIRMED,
            Lead::STATUS_CANCELLED,
            Lead::STATUS_TRASH
        ];
    }

    /**
     * Авторизация и сохранение кукисов
     * @return array
     */
    public function Auth()
    {
        $this->start_time = time();

        Console::stdout('Authorization on https://partner.adcombo.com' . PHP_EOL);

        /** @var Response $response */
        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->params['url_login'])
            ->setFormat(Client::FORMAT_JSON)
            ->setOptions([
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true
            ])
            ->setData([
                'email' => $this->params['email'],
                'password' => $this->params['password']
            ])
            ->send();

        $this->cookies = $response->getCookies();

        if ($response->getStatusCode() == 200) {
            Console::stdout('Authorization complete' . PHP_EOL);

            $result = [
                'success' => true,
                'message' => $response->content
            ];
        } else {
            Console::stdout('Authorization fail' . PHP_EOL);

            $result = [
                'success' => false,
                'message' => $response->content
            ];
        }

        return $result;
    }

    /**
     * Пакетная отправка запросов
     * @param $pages
     * @throws \yii\console\Exception
     */
    public function getPagesData($pages)
    {

        $requests = [];

        foreach ($pages as $num => $page) {
            $requests[$num] = $this->client->get($page)->setCookies($this->cookies)->addOptions([
                CURLOPT_CONNECTTIMEOUT => 0,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_VERBOSE => false,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYSTATUS => false,
                CURLOPT_RETURNTRANSFER => true,
            ])->setFormat(Client::FORMAT_JSON);

            gc_collect_cycles();
        }

        Console::stdout(PHP_EOL);

        if (self::MEMORY_DEBUG) {
            Console::stdout('Memory usage before send requests: ' . ceil((memory_get_usage(true) - $this->baseMemory) / 1024 / 1024) . ' mb' . PHP_EOL);
        }

        $responses = $this->client->batchSend($requests);

        $requests = null;

        if (self::MEMORY_DEBUG) {
            Console::stdout('Memory usage after send requests: ' . ceil((memory_get_usage(true) - $this->baseMemory) / 1024 / 1024) . ' mb' . PHP_EOL);
        }

        $count = count($responses);

        foreach ($responses as $num => $response) {
            //Иногда не возвращает statusCode
            try {
                $statusCode = $responses[$num]->getStatusCode();

                //первая попытка
                $attempt = 'The first attempt start. ';

                if ($statusCode != 200) {
                    Console::stdout("The first attempt failed. Sleep 10 seconds." . PHP_EOL);
                    sleep(10);
                    //вторая попытка

                    $attempt = 'The second attempt start. ';

                    $statusCode = $responses[$num]->getStatusCode();
                }

                if ($statusCode == 200) {
                    $this->parsePageData($num, json_decode($response->content, 1));
                    $response = null;
                    gc_collect_cycles();
                } else {
                    $textError = "Response #{$num} failed (statusCode = {$statusCode})";

                    Console::stdout($textError . PHP_EOL);

                    $this->error = $textError;

                    throw new \yii\console\Exception($textError);
                }
                $page = $num + 1;
                $info = $attempt . "Last request : page #{$page} of {$count}  stats: APPROVE: {$this->countApprove} ({$this->countAllApprove}), "
                    . "TRASH: {$this->countTrash} ({$this->countAllTrash}), "
                    . "CANCELLED: {$this->countCancelled} ({$this->countAllCancelled})\r";

                Console::stdout("{$info}\r");
                $this->clearCountsPage();

            } catch (Exception $e) {
                $textError = "Request #{$num} Exception: {$e->getMessage()}" . PHP_EOL;

                Console::stdout($textError . PHP_EOL);

                $this->error = $textError;

                throw new \yii\console\Exception($textError);
            }
        }

        if (self::MEMORY_DEBUG) {
            Console::stdout('Memory usage after processing requsts: ' . ceil((memory_get_usage(true) - $this->baseMemory) / 1024 / 1024) . ' mb' . PHP_EOL);
            Console::stdout(PHP_EOL);
        }

        $responses = null;

        if (self::MEMORY_DEBUG) {
            Console::stdout('Memory usage after clean requsts: ' . ceil((memory_get_usage(true) - $this->baseMemory) / 1024 / 1024) . ' mb' . PHP_EOL);
        }

        Console::stdout("Result  stats: APPROVE: {$this->countAllApprove}, TRASH: {$this->countAllTrash}, CANCELLED: {$this->countAllCancelled}" . PHP_EOL);
        Console::stdout(PHP_EOL);

    }

    /**
     * @param $num
     * @param $data
     * @throws \yii\console\Exception
     */
    public function parsePageData($num, $data)
    {
        if (!isset($data['objects'])) {
            $textError = "Request #{$num} processing fail";

            Console::stdout($textError . PHP_EOL);
            Console::stdout("Request text: " . json_encode($data, JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->error = json_encode(['error' => $textError, 'response' => $data], 256);

            throw new \yii\console\Exception($textError);
        } else {
            $leads = [];

            foreach ($data['objects'] as $order) {
                /**
                 * $order['state'] == 'success' не предоставляет полную информацию, видимо это дополнительные данные после финального статуса
                 */
                if (in_array($order['state'], $this->getFinalStatusesAdCombo())) {
                    $this->adcombo_data[$order['state']][] = $order;
                    $this->sumStatusOrders($order['state']);
                }

                gc_collect_cycles();
            }

            $leads = [];
        }
    }

    /**
     * @param $status
     */
    public function sumStatusOrders($status)
    {
        switch ($status) {
            case Lead::STATUS_TRASH :
                $this->countTrash++;
                $this->countAllTrash++;
                break;
            case Lead::STATUS_CONFIRMED :
                $this->countApprove++;
                $this->countAllApprove++;
                break;
            case Lead::STATUS_CANCELLED :
                $this->countCancelled++;
                $this->countAllCancelled++;
                break;
        }
    }

    /**
     * Получение статистики
     * @return array
     * @throws \yii\console\Exception
     */
    public function getStatsPage()
    {
        if (empty($this->cookies)) {
            $auth = $this->Auth();
        } else {
            $auth['success'] = true;
        }

        if (!$auth['success']) {
            $this->error = json_encode(['error' => self::ERROR_AUTH, 'params' => $this->params], 256);

            throw new \yii\console\Exception(self::ERROR_AUTH);
        } else {
            $url = strtr($this->params['url_filter_statistic'], [
                '{start}' => strtotime($this->period['start']),
                '{end}' => strtotime($this->period['end']),
                '{number}' => 1,
                '{count}' => self::COUNT_ON_PAGE
            ]);

            Console::stdout("Get statistic data from: {$url}" . PHP_EOL);

            $response = $this->client->createRequest()
                ->setMethod('get')
                ->setUrl($url)
                ->setOptions([
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_RETURNTRANSFER => true,
                ])
                ->setCookies($this->cookies)
                ->send();

            $result = [
                'success' => true,
                'message' => $response->content
            ];
        }

        return $result;
    }

    /**
     * Установка периода
     * Пока не наступило 15 число - нас интересует весь предыдущий месяц
     * Как только насупило 16 число - нас интересует только текущий месяц
     */
    public function setPeriod()
    {
        if (date('d') <= self::END_PERIOD) {
            //за предыдущий месяц
            $this->period = [
                'start' => date("Y-m-d", strtotime("-1 month", strtotime(date('Y-m-01')))),
                'end' => date("Y-m-d", strtotime("-1 month", strtotime(date('Y-m-t'))))
            ];
        } else {
            //за текущий месяц по сегодня
            $this->period = [
                'start' => date('Y-m-01'),
                'end' => date('Y-m-d')
            ];
        }
    }

    /**
     * Генерация линков страниц с заказми по пагинации
     * @param $total
     */
    public function generateListPagesOnPaginate($total)
    {

        $pages = ceil($total / self::COUNT_ON_PAGE);
        $listPages = [];

        Console::stdout("Found {$total} orders on {$pages} pages" . PHP_EOL);

        Console::stdout("Generate pages url ");

        $num = 1;
        while ($pages >= 1) {
            $pages--;

            $listPages[] = strtr($this->params['url_filter_statistic'], [
                '{start}' => strtotime($this->period['start']),
                '{end}' => strtotime($this->period['end']),
                '{number}' => $num++,
                '{count}' => self::COUNT_ON_PAGE
            ]);
        }

        $listPages = array_reverse($listPages);

        Console::stdout(" is complete" . PHP_EOL);

        Console::stdout(PHP_EOL . "Start parsing AdCombo data");

        $this->getPagesData($listPages);

        $listPages = null;
    }

    /**
     * @param null $start
     * @param null $end
     */
    public function Parse($start = null, $end = null)
    {
        $ad = 'If you want to use a custom period - run the command "php yii crontab/adcombo/parse 2018-01-21 2018-02-30"  format: YYYY-mm-dd' . PHP_EOL;
        $line = str_pad('', strlen($ad), '-') . PHP_EOL;

        Console::stdout($line);
        Console::stdout($ad);
        Console::stdout($line);

        if (!is_null($start) && !is_null($end)) {
            if (!strtotime($start) || !strtotime($end) && !in_array(null, [$start, $end])
                && (preg_match("#^\d{4}\-\d{2}\-\d{2}$#", $start) || preg_match("#^\d{4}\-\d{2}\-\d{2}$#", $end))) {

                Console::stdout(PHP_EOL . "Not a valid period format \"{$start}\", \"{$end}\"");
                exit;
            } else {
                $this->period = [
                    'start' => $start,
                    'end' => $end
                ];
            }
        }

        Console::stdout("Used {$this->mode} period {$this->period['start']} : {$this->period['end']}" . PHP_EOL);

        $this->ParseByWeeks();

        $this->end_time = time();
    }


    /**
     * Разбить интервал на недели - для меньших нагрузок
     * Переписан на интервал в 2 дня. т.к. адкомбо может не выдержать.
     * @param $interval
     * @return array
     */
    public function getWeeks($interval)
    {
        $start = strtotime($interval['start']);
        $end = strtotime($interval['end']);

        $weeks = [];

        if ($start != $end) {
            $i = 0;
            while ($start < $end) {
                $i++;
                $time = ($i > 0 && $i % 2 != 0) ? '00:00:00' : '23:59:59';
                $day = strtotime("+1 day", $start);
                $weeks[] = date("Y-m-d " . $time, $start);
                $start = $day;
            }

            $chunked_weeks = array_chunk($weeks, 2);

            $last = end($chunked_weeks);

            if (!isset($last[1])) {
                $last[1] = str_replace('00:00:00', '23:59:59', $last[0]);

                array_pop($chunked_weeks);
                $chunked_weeks[] = $last;
            }
        } else {
            $chunked_weeks[] = [
                strtotime($interval['start'] . ' 00:00:00'),
                strtotime($interval['end'] . ' 23:59:59')
            ];
        }

        return $chunked_weeks;
    }

    public function ParseByWeeks()
    {
        $weeks = $this->getWeeks($this->period);


        foreach ($weeks as $week) {
            $this->period = [
                'start' => reset($week),
                'end' => end($week),
            ];

            $start = is_numeric($this->period['start']) ? date('Y-m-d H:i:s', $this->period['start']) : $this->period['start'];
            $end = is_numeric($this->period['end']) ? date('Y-m-d H:i:s', $this->period['end']) : $this->period['end'];

            Console::stdout(PHP_EOL . "Chunk period to {$start} from {$end}" . PHP_EOL);

            $content = $this->getStatsPage();

            if (!$content['success']) {
                return $content;
            } else {
                $data = json_decode($content['message'], 1);

                if (!isset($data['total'])) {
                    Console::stdout('Error getting page count, missing "total" element' . PHP_EOL);
                    $this->error = 'Error getting page count, missing "total" element';
                    exit;
                }

                if (!isset($data['objects'])) {
                    Console::stdout('Error retrieving order list from element "object"' . PHP_EOL);
                    $this->error = 'Error retrieving order list from element "object"';
                    exit;
                }

                $this->generateListPagesOnPaginate($data['total']);
            }

            Console::stdout("It is necessary to give rest to AdCombo, e.g. he looks like a small and puny girl" . PHP_EOL);
            Console::stdout("Sleep 5 sec." . PHP_EOL);
            sleep(5);

            gc_collect_cycles();
        }

        $weeks = null;
        $content = null;

        return $this->adcombo_data;
    }

    /**
     * @param callable $callback
     * @param null $start
     * @param null $end
     * @throws \Exception
     */
    public function parseApprovePenalties(callable $callback, $start = null, $end = null)
    {
        if (!is_null($start) && !is_null($end)) {
            $this->period = [
                'start' => $start,
                'end' => $end
            ];
        }
        $weeks = $this->getWeeks($this->period);

        foreach ($weeks as $week) {
            $this->period = [
                'start' => reset($week),
                'end' => end($week),
            ];

            $start = is_numeric($this->period['start']) ? date('Y-m-d H:i:s', $this->period['start']) : $this->period['start'];
            $end = is_numeric($this->period['end']) ? date('Y-m-d H:i:s', $this->period['end']) : $this->period['end'];

            $result = $this->getApprovePenaltiesContent();
            if (!$result['success']) {
                $content = json_decode($result['content'], true);
                Yii::$app->notification->send(
                    Notification::TRIGGER_ADCOMBO_PARSER_ERROR,
                    [
                        'error' => $result['message'] . (isset($content['message']) ? '. ' . $content['message'] : ''),
                    ]
                );
                throw new \Exception($result['message']);
            }
            $data = json_decode($result['content'], true);
            call_user_func($callback, $data, $start, $end);
        }
    }

    protected function getApprovePenaltiesContent()
    {
        $url = strtr($this->params['approve_penalties_url'], [
            '{start}' => strtotime($this->period['start']),
            '{end}' => strtotime($this->period['end']),
            '{token}' => $this->params['token']
        ]);

        $response = $this->client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->setOptions([
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
            ])
            ->send();

        if (!$response->isOk) {
            $result = [
                'success' => false,
                'message' => "Service return code: {$response->statusCode}",
                'content' => $response->content,
            ];
        } else {
            $result = [
                'success' => true,
                'content' => $response->content,
            ];
        }

        return $result;
    }
}