<?php
namespace app\components\notification;

use app\components\notification\transports\TelegramTransport;

/**
 * Class TelegramQueue
 * @package app\components\notification
 */
class TelegramQueue extends Queue
{

    /** @var string */
    public static $typeNotification = self::TYPE_TELEGRAM;
    /** @var string */
    public static $typeInterval = self::FIELD_TELEGRAM_ACTIVE;

    /**
     * @return TelegramTransport
     */
    public static function getTransport()
    {
        if (is_null(self::$transport)) {
            self::$transport = new TelegramTransport();
        }
        return self::$transport;
    }
}
