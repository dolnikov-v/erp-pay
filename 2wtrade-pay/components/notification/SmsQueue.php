<?php
namespace app\components\notification;

use app\components\notification\transports\SmsTransport;

/**
 * Class SmsQueue
 * @package app\components\notification
 */
class SmsQueue extends Queue
{

    /** @var string */
    public static $typeNotification = self::TYPE_PHONE;
    /** @var string */
    public static $typeInterval = self::FIELD_SMS_ACTIVE;

    /**
     * @return SmsTransport
     */
    public static function getTransport()
    {
        if (is_null(self::$transport)) {
            self::$transport = new SmsTransport();
        }
        return self::$transport;
    }
}
