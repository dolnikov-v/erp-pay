<?php
namespace app\components\notification\transports;

use app\models\UserNotificationQueue;
use Yii;

/**
 * Class EmailMessage
 * @package app\components\notification\transports
 *
 */
class EmailMessage extends Message
{
    public $subject;

    public $count = 0;

    public $messages = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->subject = Yii::t('common', 'У вас есть непрочитанные оповещения');
    }

    /**
     * @param UserNotificationQueue[] $queues
     * @return EmailMessage[]
     */
    public static function buildMessages($queues)
    {
        $messages = [];

        foreach ($queues as $queue) {
            if (!empty($queue->userNotification->user->email)) {
                if (!array_key_exists($queue->userNotification->user_id, $messages)) {
                    $messages[$queue->userNotification->user_id] = new EmailMessage();
                    $messages[$queue->userNotification->user_id]->to = $queue->userNotification->user->email;
                }

                $messages[$queue->userNotification->user_id]->messages[] = $queue;
                $messages[$queue->userNotification->user_id]->count++;
            }
        }

        return $messages;
    }
}
