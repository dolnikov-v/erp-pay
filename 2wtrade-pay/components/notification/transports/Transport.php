<?php
namespace app\components\notification\transports;

use app\components\Notification;
use Yii;
use yii\base\Component;

/**
 * Class Transport
 * @package app\components\notification\transports
 */
abstract class Transport extends Component
{
    protected $message;

    /**
     * @return Message
     */
    public abstract function getMessage();

    /**
     * @param Message $message
     * @return bool
     */
    public abstract function send($message);
}
