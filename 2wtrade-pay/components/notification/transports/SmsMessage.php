<?php
namespace app\components\notification\transports;

use app\models\UserNotificationQueue;
use Yii;

/**
 * Class SmsMessage
 * @package app\components\notification\transports
 *
 */
class SmsMessage extends Message
{
    public $count = 0;

    public $messages = [];

    /**
     * @param UserNotificationQueue[] $queues
     * @return SmsMessage[]
     */
    public static function buildMessages($queues)
    {
        $messages = [];

        foreach ($queues as $queue) {
            if (!empty($queue->userNotification->user->phone)) {
                if (!array_key_exists($queue->userNotification->user_id, $messages)) {
                    $messages[$queue->userNotification->user_id] = new SmsMessage();
                    $messages[$queue->userNotification->user_id]->to = $queue->userNotification->user->phone;
                }

                $messages[$queue->userNotification->user_id]->messages[] = $queue;
                $messages[$queue->userNotification->user_id]->count++;
            }
        }

        return $messages;
    }
}
