<?php
namespace app\components\notification\transports;

use app\components\Notification;
use Yii;

/**
 * Class EmailTransport
 * @package app\components\notification\transports
 */
class EmailTransport extends Transport
{
    /**
     * @return EmailMessage
     */
    public function getMessage()
    {
        if (is_null($this->message)) {
            $this->message = new EmailMessage();
        }
        return $this->message;
    }

    /**
     * @param EmailMessage $message
     * @return bool
     */
    public function send($message)
    {
        Yii::$app->mailer->compose('@app/mail/user-notification-queue/mail', [
            'count' => $message->count,
            'messages' => $message->messages,
        ])
            ->setFrom(Yii::$app->params['noReplyEmail'])
            ->setTo($message->to)
            ->setSubject($message->subject)
            ->send();

        return true;
    }
}
