<?php
namespace app\components\notification\transports;

use yii\base\Component;
use yii\base\InvalidParamException;
use Yii;

/**
 * Class Message
 * @package app\components\notification\transports
 */
class Message extends Component
{
    public $subject;

    public $to;

    /**
     * @param $queues
     * @return Message[]
     */
    public static function buildMessages($queues)
    {
        throw new InvalidParamException(Yii::t('common', 'Требуется реализация buildMessages.'));
    }
}
