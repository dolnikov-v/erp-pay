<?php

namespace app\components\notification\transports\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class TransportLog
 * @package app\components\notification\transports\models
 */

class TransportLog extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to', 'from'], 'string'],
            [['request', 'response'], 'string'],
            [['notification_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'to' => Yii::t('common', 'Кому'),
            'from' => Yii::t('common', 'От кого'),
            'request' => Yii::t('common', 'Запрос'),
            'response' => Yii::t('common', 'Ответ'),
            'notification_id' => Yii::t('common', 'Уведомление'),
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

}
