<?php
namespace app\components\notification\transports;

use app\models\UserNotificationQueue;
use app\models\UserNotification;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class TelegramMessage
 * @package app\components\notification\transports
 *
 */
class TelegramMessage extends Message
{
    public $count = 0;

    public $messages = [];

    /**
     * @param UserNotificationQueue[] $queues
     * @return TelegramMessage[]
     */
    public static function buildMessages($queues)
    {
        $messages = [];
        foreach ($queues as $queue) {
            if (!empty($queue->userNotification->user->telegram_chat_id)) {
                if (!array_key_exists($queue->userNotification->user_id, $messages)) {
                    $messages[$queue->userNotification->user_id] = new TelegramMessage();
                    $messages[$queue->userNotification->user_id]->to = $queue->userNotification->user->telegram_chat_id;
                }

                $messages[$queue->userNotification->user_id]->messages[] = $queue;
                $messages[$queue->userNotification->user_id]->count++;
            }
        }
        return $messages;
    }
}
