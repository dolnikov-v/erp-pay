<?php
namespace app\components\notification\transports;

use app\modules\telegram\components\TelegramService;
use Yii;
use app\components\notification\transports\models\TransportLog;

/**
 * Class TelegramTransport
 * @package app\components\notification\transports
 */
class TelegramTransport extends Transport
{
    /**
     * @return TelegramMessage
     */
    public function getMessage()
    {
        if (is_null($this->message)) {
            $this->message = new TelegramMessage();
        }
        return $this->message;
    }

    /**
     * @param TelegramMessage $message
     * @return bool
     */
    public function send($message)
    {
        $to = $message->to;
        foreach ($message->messages as $message) {
            $country = $message->userNotification->country;
            $language = $message->userNotification->user->language;
            $text = Yii::t('common', $message->userNotification->notification->description, json_decode($message->userNotification->params, true), $language) . ($country ? '. ' . Yii::t('common', $country->name, [], $language) : '');

            $result = TelegramService::sendNotification($to, $text);

            $model = new TransportLog();
            $model->notification_id = $message->userNotification->id;
            $model->to = $to;
            $model->from = 'telegram';
            $model->response = json_encode($result['data'], JSON_UNESCAPED_UNICODE);
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();

        }
        return true;
    }
}
