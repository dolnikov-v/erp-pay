<?php

namespace app\components\notification\transports;

use app\models\UserNotificationQueue;

/**
 * Class SkypeMessage
 * @package app\components\notification\transports
 *
 */
class SkypeMessage extends Message
{
    public $count = 0;

    public $messages = [];

    /**
     * @param UserNotificationQueue[] $queues
     * @return SkypeMessage[]
     */
    public static function buildMessages($queues)
    {
        $messages = [];
        foreach ($queues as $queue) {
            if (!array_key_exists($queue->userNotification->user_id, $messages)) {
                $messages[$queue->userNotification->user_id] = new SkypeMessage();
                $messages[$queue->userNotification->user_id]->to = $queue->userNotification->user_id;
            }

            $messages[$queue->userNotification->user_id]->messages[] = $queue;
            $messages[$queue->userNotification->user_id]->count++;
        }
        return $messages;
    }
}
