<?php
namespace app\components\notification\transports;

use Yii;
use app\components\notification\transports\models\TransportLog;
use \app\modules\messenger\models\Message as SkypeModuleMessage;

/**
 * Class SkypeTransport
 * @package app\components\notification\transports
 */
class SkypeTransport extends Transport
{
    /**
     * @return SkypeMessage
     */
    public function getMessage()
    {
        if (is_null($this->message)) {
            $this->message = new SkypeMessage();
        }
        return $this->message;
    }

    /**
     * @param SkypeMessage $message
     * @return bool
     */
    public function send($message)
    {
        $to = $message->to;
        foreach ($message->messages as $message) {
            $country = $message->userNotification->country;
            $language = $message->userNotification->user->language;
            $text = Yii::t('common', $message->userNotification->notification->description, json_decode($message->userNotification->params, true), $language) . ($country ? '. ' . Yii::t('common', $country->name, [], $language) : '');

            try {
                $skypeMessage = new SkypeModuleMessage([
                    'serviceType' => SkypeModuleMessage::SERVICE_TYPE_SKYPE,
                    'to' => ['userID' => $to],
                    'text' => $text,
                ]);
                $result = \Yii::$app->getModule('messenger')->send($skypeMessage);
            } catch (\Throwable $e) {
                $result = $e;
            }
            $model = new TransportLog();
            $model->notification_id = $message->userNotification->id;
            $model->to = $to;
            $model->from = 'skype';
            $model->response = json_encode($result, JSON_UNESCAPED_UNICODE);
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }
        return true;
    }
}
