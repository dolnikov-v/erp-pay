<?php

namespace app\components\notification\transports;

use app\components\Notification;
use app\components\notification\transports\models\TransportLog;
use app\modules\smsnotification\components\SmsService;
use app\components\notification\transports\models\SmsSentErrorLog;
use Yii;

/**
 * Class SmsTransport
 * @package app\components\notification\transports
 */
class SmsTransport extends Transport
{
    /**
     * @return SmsMessage
     */
    public function getMessage()
    {
        if (is_null($this->message)) {
            $this->message = new SmsMessage();
        }
        return $this->message;
    }

    /**
     * @param SmsMessage $message
     * @return []
     */
    public function send($message)
    {
        $module = Yii::$app->getModule('smsnotification');
        $customSmsSender = $module->params['smsSender'];
        $smsService = yii::$app->smsService;

        $phoneTo = $message->to;
        $key = 0;
        $text = [];
        foreach ($message->messages as $message) {
            $country = $message->userNotification->country;
            $language = $message->userNotification->user->language;
            $text[] = $key + 1 . ". " . Yii::t('common', $message->userNotification->notification->description, json_decode($message->userNotification->params, true), $language) . ($country ? '. ' . Yii::t('common', $country->name, [], $language) : '');
            $key++;
        }


        if ($country) {
            //$sentResult - всегда массив массивов
            $sentResults = $smsService->sendSmsNotification($phoneTo,
                null,
                implode(" ", $text),
                $country->sms_notifier_phone_from,
                [
                    'char_code' => $country->char_code,
                    'local' => true
                ]
            );
        } else {
            $sentResults = $smsService->sendSmsNotification($phoneTo,
                $customSmsSender,
                implode(" ", $text),
                $customSmsSender,
                ['local' => true]
            );

        }
        $not_sented_sms = [];

        foreach ($sentResults as $sentResult) {

            $model = new TransportLog();

            $model->notification_id = $message->userNotification->id;
            $model->to = $sentResult['to'];

            $model->from = $sentResult['from'];
            $model->request = $sentResult['request'];
            $model->response = json_encode($sentResult['response'], JSON_UNESCAPED_UNICODE);

            $model->created_at = time();
            $model->updated_at = time();
            $model->save();

        }

    }


}
