<?php
namespace app\components\notification;

use app\components\notification\transports\SkypeTransport;

/**
 * Class SkypeQueue
 * @package app\components\notification
 */
class SkypeQueue extends Queue
{

    /** @var string */
    public static $typeNotification = self::TYPE_SKYPE;
    /** @var string */
    public static $typeInterval = self::FIELD_SKYPE_ACTIVE;

    /**
     * @return SkypeTransport
     */
    public static function getTransport()
    {
        if (is_null(self::$transport)) {
            self::$transport = new SkypeTransport();
        }
        return self::$transport;
    }
}
