<?php
namespace app\components\notification;

use app\components\notification\transports\Transport;
use app\models\Notification as NotificationModel;
use app\models\UserNotification;
use app\models\UserNotificationQueue;
use app\models\UserNotificationSetting;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class Queue
 * @package app\components\notification
 */
abstract class Queue
{
    const TYPE_EMAIL = 'email';
    const TYPE_PHONE = 'phone';
    const TYPE_TELEGRAM = 'telegram_chat_id';
    const TYPE_SKYPE = 'skype';

    const FIELD_INTERVAL_EMAIL = 'email_interval';
    const FIELD_SMS_ACTIVE = 'sms_active';
    const FIELD_TELEGRAM_ACTIVE = 'telegram_active';
    const FIELD_SKYPE_ACTIVE = 'skype_active';

    /** @var  Transport */
    public static $transport;

    public static $typeNotification;

    public static $typeInterval;

    /**
     * @param $userNotificationId
     * @throws \Exception
     */
    public static function add($userNotificationId)
    {
        $typeSending = static::$typeNotification;
        if ($typeSending == 'phone') {
            $typeSending = 'sms';
        }
        if ($typeSending == 'telegram_chat_id') {
            $typeSending = 'telegram';
        }

        $notification = UserNotification::find()->where(['id' => $userNotificationId])->one();
        if (!$notification || $notification->read != UserNotification::UNREAD || $notification->getNotificationQueue()->andWhere(['type_sending' => $typeSending])->exists() || (empty($notification->user->{static::$typeNotification}) && static::$typeNotification != static::TYPE_SKYPE)) {
            return;
        }

        $query = UserNotificationSetting::find()
            ->select(static::$typeInterval)
            ->where(['user_id' => $notification->user_id])
            ->andWhere(['active' => NotificationModel::ACTIVE])
            ->andWhere(['trigger' => $notification->trigger]);

        if (static::$typeInterval == self::FIELD_INTERVAL_EMAIL) {
            $query->andWhere(['<>', static::$typeInterval, 'NULL']);
            $query->andWhere(['<>', static::$typeInterval, '']);
        }

        if (static::$typeInterval == self::FIELD_SMS_ACTIVE) {
            $query->andWhere([static::$typeInterval => '1']);
        }

        if (static::$typeInterval == self::FIELD_TELEGRAM_ACTIVE) {
            $query->andWhere([static::$typeInterval => '1']);
        }

        if (static::$typeInterval == self::FIELD_SKYPE_ACTIVE) {
            $query->andWhere([static::$typeInterval => '1']);
        }

        $interval = $query->scalar();

        if (!$interval) {
            return;
        }

        $queueModel = new UserNotificationQueue([
            'notification_id' => $notification->id,
            'type_sending' => $typeSending,
            'status_send' => UserNotificationQueue::NO_SENT,
        ]);

        if (in_array($interval, array_keys(UserNotificationSetting::getIntervalCollection()))) {
            $queueModel->interval = $interval;
        }

        if (!$queueModel->save()) {
            throw new \Exception("Can't add notification #{$notification->id} to queue with type {$typeSending}: {$queueModel->getFirstErrorAsString()}.");
        }
    }

    /**
     * @param $interval
     */
    public static function send($interval)
    {
        $typeSending = static::$typeNotification;
        if ($typeSending == 'phone') {
            $typeSending = 'sms';
        }
        if ($typeSending == 'telegram_chat_id') {
            $typeSending = 'telegram';
        }
        $queues = UserNotificationQueue::find()
            ->where([UserNotification::tableName() . '.read' => UserNotification::UNREAD])
            ->andWhere(['status_send' => UserNotificationQueue::NO_SENT])
            ->andWhere(['type_sending' => $typeSending])
            ->andWhere(['interval' => $interval])
            ->joinWith('userNotification')
            ->all();

        $message = static::getTransport()->getMessage();
        $messages = $message::buildMessages($queues);

        foreach ($messages as $message) {
            static::getTransport()->send($message);
        }

        self::burnQueues($queues);
    }

    /**
     * @return Transport
     */
    public static function getTransport()
    {
        if (is_null(self::$transport)) {
            throw new InvalidParamException(Yii::t('common', 'Необходима реализация Transport.'));
        }
    }

    /**
     * @param UserNotificationQueue[] $queues
     */
    protected static function burnQueues($queues)
    {
        $ids = ArrayHelper::getColumn($queues, 'id');

        UserNotificationQueue::updateAll(
            ['status_send' => UserNotificationQueue::SENT],
            ['IN', 'id', $ids]
        );
    }
}
