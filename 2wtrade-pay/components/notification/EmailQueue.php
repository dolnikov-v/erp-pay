<?php
namespace app\components\notification;

use app\components\notification\transports\EmailTransport;

/**
 * Class QueueEmail
 * @package app\components\notification
 */
class EmailQueue extends Queue
{
    /** @var string */
    public static $typeNotification = self::TYPE_EMAIL;
    /** @var string */
    public static $typeInterval = self::FIELD_INTERVAL_EMAIL;

    /**
     * @return EmailTransport
     */
    public static function getTransport()
    {
        if (is_null(self::$transport)) {
            self::$transport = new EmailTransport();
        }
        return self::$transport;
    }
}
