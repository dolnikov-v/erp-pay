<?php

namespace app\components\widgets;

use app\helpers\Utils;
use app\models\logs\TableLog;
use app\widgets\Label;
use app\widgets\PanelGroup;
use app\widgets\PanelTab;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\BaseActiveRecord;
use app\components\grid\GridView;

/**
 * Class PanelGroupDeliveryLogs
 * @package app\modules\delivery\widgets
 */
class PanelList extends PanelGroup
{
    /**
     * @var array
     */
    public $data;
    /**
     * @var array
     */
    public $content;

    /**
     * @var string
     */
    public $groupBy = '';

    /**
     * @var array
     */
    public $titleTemplates = [];

    /**
     * @var string
     */
    public $titleContent = '';

    /**
     * @var string
     */
    public $id = 'panel_tab_';

    const TEMPLATES_HEAD = [
        'num' => '<span class="text-gray">#{num}</span> ',
        'revertNum' => 'Изменение #{revertNum}',
        'type' => '{type}',
    ];
    const TEMPLATES_FOOTER = [
        'created_at' => '<span class="align-right-sm nowrap">{created_at}</span>',
        'updated_at' => '<span class="align-right-sm nowrap">{updated_at}</span>',
    ];

    /**
     * @return string
     */
    public function run()
    {
        $this->preparePanels();

        return parent::run();
    }

    /**
     * Подготовка табов
     */
    protected function preparePanels()
    {
        $groups = [];

        if ($this->groupBy) {
            foreach ((array)$this->data as $item) {
                $groups[(string)$item->getAttribute($this->groupBy)][] = $item;
            }
            $number = 0;

            foreach ($groups as $key => $items) {
                $dataProvider = new ArrayDataProvider([
                    'allModels' => $items
                ]);
                $title = '';
                if (!($title = $this->getTitle($dataProvider->getModels()[0], $number, count($groups) - $number))) {
                    $title = Yii::t('common', 'Изменение #{number}', ['number' => count($groups) - $number]);
                }
                $this->panels[] = $this->getPanel(
                    $dataProvider,
                    $title,
                    $this->id . Utils::uid()
                );
                $number++;
            }
        } else {
            $dataProvider = new ArrayDataProvider([
                'allModels' => (array)$this->data
            ]);

            $this->panels[] = $this->getPanel(
                $dataProvider,
                ($this->getTitle($dataProvider->getModels()[0]) ?? ''),
                $this->id . Utils::uid()
            );
        }
    }

    /**
     * @param BaseActiveRecord $model
     * @param null|integer $num
     * @param null|integer $revertNum
     * @return null|string
     * @throws \yii\base\InvalidConfigException
     */
    protected function getTitle(BaseActiveRecord $model, int $num = null, int $revertNum = null): ?string
    {
        $answer = $this->titleContent;
        if (preg_match_all("/\{([^\{\}]+)\}/i", $this->titleContent,$matches)) {
            foreach ($matches[1] as $match) {
                $val = $model;
                foreach (explode('.', $match) as $attr) {
                    if (empty($val->$attr)) {
                        break;
                    }
                    $val = $val->$attr;
                    if (in_array($attr, ['created_at', 'updated_at'])) {
                        $val = Yii::$app->formatter->asDatetime($val);
                    }
                }
                if (is_object($val)) {
                    $val = '—';
                }
                $answer = str_replace('{' . $match . '}', $val, $answer);
            }
        }

        $header = [];
        $footer = [];
        if ($this->titleTemplates) {
            foreach (self::TEMPLATES_HEAD as $key => $item) {
                if (in_array($key, $this->titleTemplates)) {
                    $val = $$key ?? $model->$key ?? '';
                    if (in_array($key, ['created_at', 'updated_at']) && $val) {
                        $val = Yii::$app->formatter->asDatetime($val);
                    }
                    if ($model instanceof TableLog && $key == 'type' && in_array($val, [TableLog::TYPE_INSERT, TableLog::TYPE_UPDATE, TableLog::TYPE_DELETE])) {
                        $val = Label::widget(['label' => $val, 'style' => (
                            $val == TableLog::TYPE_INSERT ? Label::STYLE_SUCCESS :
                                ($val == TableLog::TYPE_UPDATE ? Label::STYLE_INFO :
                                    ($val == TableLog::TYPE_DELETE ? Label::STYLE_DANGER : ''))
                        )]);
                    }
                    $header[] = Yii::t('common', $item, [$key => $val]);
                }
            }
            foreach (self::TEMPLATES_FOOTER as $key => $item) {
                if (in_array($key, $this->titleTemplates)) {
                    $val = $$key ?? $model->$key ?? '';
                    if (in_array($key, ['created_at', 'updated_at']) && $val) {
                        $val = Yii::$app->formatter->asDatetime($val);
                    }
                    $footer[] = Yii::t('common', $item, [$key => $val]);
                }
            }
        }

        return ($header ? implode(' ', $header) . ' ' : '') . ($answer ?? '') . ($footer ? ' ' . implode(' ', $footer) : '');
    }

    /**
     * @param ArrayDataProvider $dataProvider
     * @param string $title
     * @param string $id
     * @return PanelTab
     * @throws \Exception
     */
    protected function getPanel(ArrayDataProvider $dataProvider, string $title = '', string $id = 'panel_tab'): PanelTab
    {
        return new PanelTab([
            'id' => $id,
            'title' => $title,
            'content' => GridView::widget([
                'tableOptions' => [
                    'style' => 'padding-top: 0;',
                ],
                'rowOptions' => ['class' => 'tr-vertical-align-top', 'style' => 'word-break: break-word;'],
                'dataProvider' => $dataProvider,
                'columns' => $this->content
            ]),
        ]);
    }
}
