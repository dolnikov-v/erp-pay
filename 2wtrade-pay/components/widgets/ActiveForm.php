<?php
namespace app\components\widgets;

use app\widgets\Button;
use app\widgets\ButtonLink;
use app\widgets\Submit;
use yii\helpers\ArrayHelper;

class ActiveForm extends \yii\widgets\ActiveForm
{
    public $fieldClass = 'app\components\widgets\ActiveField';

    /**
     * @param array $config
     * @return static
     */
    public static function begin($config = [])
    {
        $form = parent::begin($config);

        if ($form->enableClientValidation) {
            $form->getView()->registerJs("jQuery('#" . $form->id . "').on('afterValidateAttribute', function (event, attribute, message) { $.each(message, function(index, value) { $.notify({message: value}, {type: 'danger dark', animate: {exit: 'hide'}, z_index: 2031 }); }); });");
        }

        return $form;
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param array $options
     * @return \app\components\widgets\ActiveField
     */
    public function field($model, $attribute, $options = [])
    {
        return parent::field($model, $attribute, $options);
    }

    /**
     * @param string $name
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public function submit($name, $options = [])
    {
        $config = ['label' => $name];

        $config['size'] = isset($config['size']) ? $config['size'] : Submit::SIZE_SMALL;
        $config['style'] = isset($config['style']) ? $config['style'] : Submit::STYLE_SUCCESS;

        $config = ArrayHelper::merge($config, $options);

        return Submit::widget($config);
    }

    /**
     * @param $name
     * @return string
     * @throws \Exception
     */
    public function button($name)
    {
        return Button::widget([
            'label' => $name,
            'size' => ButtonLink::SIZE_SMALL
        ]);
    }

    /**
     * @param $name
     * @param $url
     * @return string
     * @throws \Exception
     */
    public function link($name, $url)
    {
        return ButtonLink::widget([
            'label' => $name,
            'url' => $url,
            'size' => ButtonLink::SIZE_SMALL
        ]);
    }
}
