<?php

namespace app\components\widgets;

use app\modules\access\widgets\PasswordGenerate;
use app\widgets\DateRangePicker;
use app\widgets\DateTimePicker;
use app\widgets\InputText;
use app\widgets\Select2;
use app\widgets\custom\Checkbox;
use app\widgets\CountryDelivery;
use app\widgets\CountryStorage;
use app\widgets\StorageProduct;
use app\widgets\StorageProductBarCodes;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ActiveField extends \yii\widgets\ActiveField
{
    /**
     * @var null
     */
    public $options = null;
    /**
     * @var string
     */
    public $template = '<div class="form-group"><div class="control-label">{label}</div>{input}</div>';

    /**
     * @var string
     */
    public $templateTextInputIcon = '<div class="control-label">{label}</div>{input}<span class="{icon} form-control-feedback"></span>';

    /**
     * @var string
     */
    public $templateDateTimePicker = '<div class="row form-group"><div class="col-lg-2 control-label">{label}</div><div class="col-lg-3"><div class="input-group b-date-time-picker">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div></div>';

    /**
     * @var string
     */
    public $templateDateRangePicker = '{label}<div class="input-group">{input_start}<span class="input-group-addon custom">-</span>{input_finish}</div>';

    /**
     * @var string
     */
    public $templatePasswordInput = '{label}<div class="input-group box-password-showing">{input}<span class="input-group-btn"><button class="btn btn-default" type="button" data-widget="password-showing"><i class="fa fa-eye"></i></button></span></div>';

    /**
     * @var string
     */
    public $templateGeneratePasswordInput = '{label}<div class="input-group box-password-showing"><span class="input-group-btn"><button class="btn btn-default" type="button" data-widget="password-generator" data-password-generator-length="8"><i class="fa fa-random"></i></button></span>{input}<span class="input-group-btn"><button class="btn btn-default" type="button" data-widget="password-showing"><i class="fa fa-eye"></i></button></span></div>';

    /**
     * @var string
     */
    public $templateCheckInput = '<div class="form-group check-input-holder"><div class="control-label">{label}</div>{checkbox}{input}</div>';


    /**
     * @var array
     */
    public $labelOptions = [];

    /**
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function textInput($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel($this->attribute),
            'value' => $this->model->{$this->attribute},
        ];

        $options = ArrayHelper::merge($custom, $options);

        $this->parts['{input}'] = InputText::widget($options);

        return $this;
    }

    /**
     * @param $icon
     * @param array $options
     * @return ActiveField
     */
    public function textInputIcon($icon, $options = [])
    {
        $this->parts['{icon}'] = $icon;
        $this->template = $this->templateTextInputIcon;

        return self::textInput($options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function passwordInput($options = [])
    {
        $this->template = $this->templatePasswordInput;

        $placeholder = $this->model->getAttributeLabel($this->attribute);

        $custom = [
            'placeholder' => $placeholder,
        ];

        $options = ArrayHelper::merge($custom, $options);

        return parent::passwordInput($options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function textarea($options = [])
    {
        $placeholder = $this->model->getAttributeLabel($this->attribute);

        $custom = [
            'placeholder' => $placeholder,
        ];

        $options = ArrayHelper::merge($custom, $options);

        return parent::textarea($options);
    }

    /**
     * Виджет Селект2
     * @param $items
     * @param array $options
     * @return $this
     */
    public function select2List($items, $options = [])
    {
        $config = [
            'items' => $items,
        ];

        $options['id'] = isset($options['id']) ? $options['id'] : Html::getInputId($this->model, $this->attribute);
        $options['name'] = isset($options['name']) ? $options['name'] . (isset($options['multiple']) ? '[]' : '') : Html::getInputName($this->model, $this->attribute) . (isset($options['multiple']) ? '[]' : '');
        $options['value'] = isset($options['value']) ? $options['value'] : $this->model{$this->attribute};
        $options['length'] = isset($options['length']) ? $options['length'] : -1;

        $config = ArrayHelper::merge($config, $options);

        $this->parts['{input}'] = Select2::widget($config);

        return $this;
    }

    /**
     * Виджет Селект2 - множественный выбор
     *
     * @param $items
     * @param array $options
     * @return ActiveField
     */
    public function select2ListMultiple($items, $options = [])
    {
        $config = [
            'items' => $items,
        ];

        $options['id'] = isset($options['id']) ? $options['id'] : Html::getInputId($this->model, $this->attribute);
        $options['name'] = isset($options['name']) ? $options['name'] . '[]' : Html::getInputName($this->model, $this->attribute) . '[]';
        $options['value'] = isset($options['value']) ? $options['value'] : $this->model{$this->attribute};
        $options['multiple'] = 'multiple';
        $options['length'] = isset($options['length']) ? $options['length'] : -1;
        $config = ArrayHelper::merge($config, $options);

        $this->parts['{input}'] = Select2::widget($config);

        return $this;
    }

    /**
     * Виджет DateTimePicker
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function datePicker($options = [])
    {
        $this->parts['{input}'] = DateTimePicker::widget(ArrayHelper::merge([
            'label' => $this->model->getAttributeLabel($this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'value' => $this->model->{$this->attribute},
        ], $options));

        return $this;
    }

    /**
     * @param string $attributeFrom
     * @param string $attributeTo
     * @param array $options
     * @return $this
     */
    public function dateRangePicker($attributeFrom, $attributeTo, $options = [])
    {
        $config = [
            'locale' => isset($options['locale']) ? $options['locale'] : null,
            'format' => isset($options['format']) ? $options['format'] : null,
            'formatFunc' => isset($options['formatFunc']) ? $options['formatFunc'] : null,

            'nameFrom' => Html::getInputName($this->model, $attributeFrom),
            'valueFrom' => $this->model->$attributeFrom,
            'disabledFrom' => isset($options['from']['disabled']) ? $options['from']['disabled'] : false,

            'nameTo' => Html::getInputName($this->model, $attributeTo),
            'valueTo' => $this->model->$attributeTo,
            'disabledTo' => isset($options['to']['disabled']) ? $options['to']['disabled'] : false,

            'rangesDisabled' => isset($options['ranges']['disabled']) ? $options['ranges']['disabled'] : false,
            'ranges' => isset($options['ranges']['list']) ? $options['ranges']['list'] : null,
            'hideRanges' => isset($options['ranges']['hide']) ? $options['ranges']['hide'] : false,

            'minDate' => isset($options['minDate']) ? $options['minDate'] : null,
            'maxDate' => isset($options['maxDate']) ? $options['maxDate'] : null,
        ];

        $this->parts['{input}'] = DateRangePicker::widget($config);

        return $this;
    }

    /**
     * @param array $options
     * @param bool $enclosedByLabel
     * @return $this
     */
    public function switchCheckbox($options = [], $enclosedByLabel = false)
    {
        $custom = [
            'class' => 'bootstrap-switch-simple-init',
            'data-text-on' => Yii::t('common', 'Да'),
            'data-text-off' => Yii::t('common', 'Нет'),
        ];

        $options = ArrayHelper::merge($custom, $options);

        return parent::checkbox($options, $enclosedByLabel);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function textGeneratePasswordInput($options = [])
    {
        $this->parts['{input}'] = PasswordGenerate::widget([
            'id' => isset($options['id']) ? $options['id'] : Html::getInputId($this->model, $this->attribute),
            'label' => isset($options['label']) ? $options['label'] : $this->model->getAttributeLabel($this->attribute),
            'name' => isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute),
        ]);

        return $this;
    }

    /**
     * @param array $config
     * @return string
     */
    public function checkboxCustom($config = [])
    {
        $options['id'] = isset($config['id']) ? $config['id'] : Html::getInputId($this->model, $this->attribute);
        $options['name'] = isset($config['name']) ? $config['name'] : Html::getInputName($this->model, $this->attribute);
        $options['value'] = isset($config['value']) ? $config['value'] : $this->model{$this->attribute};
        $options['label'] = isset($config['label']) ? $config['label'] : $this->model->getAttributeLabel($this->attribute);

        $config = ArrayHelper::merge($config, $options);

        return Checkbox::widget($config);
    }

    /**
     * @param string $attributeCountry
     * @param string $attributeDelivery
     * @param integer $currentCountry
     * @param integer $currentDelivery
     * @return $this
     */
    public function countryDelivery($attributeCountry, $attributeDelivery, $currentCountry, $currentDelivery)
    {
        $config = [
            'nameCountry' => Html::getInputName($this->model, $attributeCountry),
            'nameDelivery' => Html::getInputName($this->model, $attributeDelivery),
            'country' => $currentCountry,
            'delivery' => $currentDelivery,
        ];

        $this->parts['{input}'] = CountryDelivery::widget($config);

        return $this;
    }

    /**
     * @param string $attributeCountry
     * @param string $attributeStorage
     * @param integer $currentCountry
     * @param integer $currentStorage
     * @return $this
     */
    public function countryStorage($attributeCountry, $attributeStorage, $currentCountry, $currentStorage)
    {
        $config = [
            'nameCountry' => Html::getInputName($this->model, $attributeCountry),
            'nameStorage' => Html::getInputName($this->model, $attributeStorage),
            'country' => $currentCountry,
            'storage' => $currentStorage,
        ];

        $this->parts['{input}'] = CountryStorage::widget($config);

        return $this;
    }

    public function checkTextInput($options = [])
    {

        $value = $this->model->{$this->attribute};

        $this->template = $this->templateCheckInput;

        $checkboxConfig = [
            'id' => Html::getInputId($this->model, 'ch' . $this->attribute),
            'name' => Html::getInputName($this->model, 'ch' . $this->attribute),
            'label' => ' ',
            'value' => 1,
            'checked' => ((string)$value != '') ? true : false,
            'style' => 'checkbox-inline checkbox-lg',
        ];

        $this->parts['{checkbox}'] = Checkbox::widget($checkboxConfig);

        return self::textInput($options);
    }

    /**
     * @param string $attributeStorage
     * @param string $attributeProduct
     * @param integer $currentStorage
     * @param integer $currentProduct
     * @param bool $noUseBar
     * @return $this
     */
    public function storageProduct($attributeStorage, $attributeProduct, $currentStorage, $currentProduct, $noUseBar = false, $attributeShelfLife)
    {
        $config = [
            'nameStorage' => Html::getInputName($this->model, $attributeStorage),
            'nameProduct' => Html::getInputName($this->model, $attributeProduct),
            'nameShelfLife' => Html::getInputName($this->model, $attributeShelfLife),
            'storage' => $currentStorage,
            'product' => $currentProduct,
            'noUseBar' => $noUseBar,
        ];

        $this->parts['{input}'] = StorageProduct::widget($config);

        return $this;
    }

    /**
     * @param string $attributeStorage
     * @param string $attributeProduct
     * @param string $attributeQuantity
     * @param string $attributeBarList
     * @param integer $currentStorage
     * @param integer $currentProduct
     * @return $this
     */
    public function storageProductBarCodes($attributeStorage, $attributeProduct, $attributeQuantity, $attributeBarList, $currentStorage, $currentProduct, $attributeShelfLife)
    {
        $config = [
            'nameStorage' => Html::getInputName($this->model, $attributeStorage),
            'nameProduct' => Html::getInputName($this->model, $attributeProduct),
            'nameShelfLife' => Html::getInputName($this->model, $attributeShelfLife),
            'nameQuantity' => Html::getInputName($this->model, $attributeQuantity),
            'nameBarList' => Html::getInputName($this->model, $attributeBarList),
            'storage' => $currentStorage,
            'product' => $currentProduct,
        ];

        $this->parts['{input}'] = StorageProductBarCodes::widget($config);

        return $this;
    }

}
