<?php
namespace app\components\gii\fixture;

use elisdn\gii\fixture\Generator;


/**
 * Надстройка над генератором фикстур для работы с большими таблицами чтобы не брать все данные, а только часть
 *
 * Class LimitedGenerator
 */
class LimitedGenerator extends Generator {

    const LIMIT = 100;

    /**
     * @return array
     */
    protected function getFixtureData()
    {
        if (!$this->grabData) {
            return parent::getFixtureData();
        }

        /** @var \yii\db\ActiveRecord $modelClass */
        $modelClass = $this->modelClass;
        $items = [];
        $orderBy = array_combine($modelClass::primaryKey(), array_fill(0, count($modelClass::primaryKey()), SORT_DESC));
        foreach ($modelClass::find()->orderBy($orderBy)->limit(self::LIMIT)->asArray()->each() as $row) {
            $item = [];
            foreach ($row as $name => $value) {
                if (is_null($value)) {
                    $encValue = 'null';
                } elseif (preg_match('/^(0|[1-9-]\d*)$/s', $value)) {
                    $encValue = $value;
                } else {
                    $encValue = var_export($value, true);;
                }
                $item[$name] = $encValue;
            }
            $items[] = $item;
        }
        return $items;
    }
}