<?php
namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Class Notifier
 * @package app\components
 */
class Notifier extends Component
{
    const TYPE_DANGER = 'danger';
    const TYPE_SUCCESS = 'success';

    /**
     * @return array
     */
    public function getNotifications()
    {
        $notifications = Yii::$app->session->getFlash('notifier.notifications');

        return empty($notifications) ? [] : $notifications;
    }

    /**
     * @param string $message
     * @param string $type
     */
    public function addNotification($message, $type = Notifier::TYPE_DANGER)
    {
        $notification = [
            'message' => $message,
            'type' => $type,
        ];

        $notifications = Yii::$app->session->getFlash('notifier.notifications');

        if (empty($notifications)) {
            $notifications = [];
        }

        $notifications[] = $notification;

        Yii::$app->session->setFlash('notifier.notifications', $notifications);
    }

    /**
     * @param \yii\base\Model $model
     * @param bool $first
     */
    public function addNotificationsByModel($model, $first = false)
    {
        $errorsAttributes = $model->getErrors();

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $this->addNotification($error);

                if ($first) {
                    return;
                }
            }
        }
    }
}
