<?php
namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Metric;
use yii\helpers\ArrayHelper;

/**
 * Class MetricTrendDetector
 * @package app\components
 */
class MetricTrendDetector extends Component
{
    /**
     * @param $metric Metric
     * @param $count float
     * @return mixed bool
     */
    public function updateMetric($metric, $count)
    {
        //Сравнение предыдущей метрики и наступившей
        $check = $count > $metric->count;

        $metric->count = floatval($count);

        if (!$metric->save()) {
            $logger = Yii::$app->get("processingLogger");
            $cronLog = $logger->getLogger([
                'metric_name' => $metric->name,
                'component' => 'MetricTrendDetector'
            ]);

            $log = $cronLog($metric->name." not_updated: ".json_encode($metric->getErrors(), JSON_UNESCAPED_UNICODE));
        }

        return $check;
    }

    /**
     * @param $name string
     * @param $count float
     * @return mixed bool
     */
    public function createMetric($name, $count)
    {
        $metric = new Metric();
        $metric->name = $name;
        $metric->count = floatval($count);

        if (!$metric->save()) {
            $logger = Yii::$app->get("processingLogger");
            $cronLog = $logger->getLogger([
                'metric_name' => $name,
                'component' => 'MetricTrendDetector'
            ]);

            $log = $cronLog($metric->name." not_created: ".json_encode($metric->getErrors(), JSON_UNESCAPED_UNICODE));
        }

        return true;
    }

    /**
     * @param $name string
     * @param $count float
     * @return bool
     */
    public function checkMetric($name, $count)
    {
        $metric = Metric::findOne([Metric::tableName() . '.name' => $name]);

        return is_null($metric) ? $this->createMetric($name, $count) : $this->updateMetric($metric, $count);
    }
}