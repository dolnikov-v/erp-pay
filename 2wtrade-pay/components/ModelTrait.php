<?php

namespace app\components;

/**
 * Class ModelTrait
 * @package app\components\db
 */
trait ModelTrait
{
    use ComponentTrait;

    /**
     * @param null $attribute
     * @return array
     */
    public function getErrorsAsArray($attribute = null)
    {
        $result = [];

        $errorsAttributes = $this->getErrors($attribute);

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $result[] = $error;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getFirstErrorAsString()
    {
        $errorsAsArray = $this->getErrorsAsArray();
        $errors = array_shift($errorsAsArray);
        if (is_array($errors) && count($errors)) {
            $key = [array_keys($errors)[0] ?? 'Undefined'];
            do {
                $errors = array_shift($errors);
                $key[] = array_keys($errors)[0] ?? 'Undefined';
            } while (is_array($errors) && count($errors));
            $errors = '[' . implode(' => ', $key) . ']' . (string)$errors;
        }

        return (string)$errors;
    }
}
