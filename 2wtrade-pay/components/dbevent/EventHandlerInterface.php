<?php

namespace app\components\dbevent;

/**
 * Interface EventHandlerInterface
 * @package app\components\dbevent
 */
interface EventHandlerInterface
{
    /**
     * @param EventObject $eventObject
     * @param EventWatcher $watcher
     */
    public function handle(EventObject $eventObject, EventWatcher $watcher);
}