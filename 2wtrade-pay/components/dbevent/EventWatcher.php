<?php

namespace app\components\dbevent;


use app\components\dbevent\events\ErrorEvent;
use app\components\dbevent\events\ExecEvent;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\console\Application as ConsoleApp;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class EventWatcher
 * @package app\components\dbevent
 */
abstract class EventWatcher extends Component implements BootstrapInterface
{
    const EVENT_BEFORE_EXEC = 'beforeExec';
    const EVENT_AFTER_EXEC = 'afterExec';

    const EVENT_BEFORE_EXEC_HANDLER = 'beforeExecHandler';
    const EVENT_AFTER_EXEC_HANDLER = 'afterExecHandler';

    const EVENT_AFTER_ERROR = 'afterError';

    /**
     * @var string command class name
     */
    public $commandClass = Command::class;
    /**
     * @var array of additional options of command
     */
    public $commandOptions = [];

    /**
     * @var callable|null
     */
    public $customHandler = null;

    /**
     * @var array of actions executable when event is triggered
     * [
     *      'event_id' => callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *      'event_id' => [
     *         callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *         callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *         'event_type' => callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *         'event_type' => [
     *              callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *              callback(EventObject, EventWatcher)|EventHandlerInterface::class,
     *              ...
     *          ],
     *      ]
     * ]
     */
    public $eventHandlers = [];


    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function bootstrap($app)
    {
        if ($app instanceof ConsoleApp) {
            $app->controllerMap[$this->getCommandId()] = [
                    'class' => $this->commandClass,
                    'watcher' => $this,
                ] + $this->commandOptions;
        }
    }

    /**
     * @return string command id
     * @throws
     */
    protected function getCommandId()
    {
        foreach (Yii::$app->getComponents(false) as $id => $component) {
            if ($component === $this) {
                return Inflector::camel2id($id);
            }
        }
        throw new InvalidConfigException('Queue must be an application component.');
    }

    /**
     * @param bool $repeat
     * @param int $timeout
     */
    public function run($repeat, $timeout = 0)
    {
        while (true) {
            if (($event = $this->getEvent()) != null) {
                if ($this->handleEvent($event)) {
                    $this->deleteEvent($event->id);
                }
            } elseif (!$repeat) {
                break;
            } else {
                sleep($timeout);
            }
        }
    }

    /**
     * @param EventObject $eventObject
     * @return bool
     */
    public function handleEvent(EventObject $eventObject)
    {
        if (is_callable($this->customHandler)) {
            return call_user_func($this->customHandler, $eventObject);
        }

        $handlers = $this->findHandlersForEvent($eventObject->eventId, $eventObject->type);
        $event = new ExecEvent([
            'eventObject' => $eventObject
        ]);
        $this->trigger(static::EVENT_BEFORE_EXEC, $event);
        if ($event->handled) {
            return true;
        }
        foreach ($handlers as $handler) {
            try {
                $event = new ExecEvent([
                    'handler' => $handler,
                    'eventObject' => $eventObject
                ]);
                $this->trigger(static::EVENT_BEFORE_EXEC_HANDLER, $event);
                if ($event->handled) {
                    continue;
                }
                if (is_callable($handler)) {
                    call_user_func($handler, $eventObject, $this);
                } elseif (is_a($handler, EventHandlerInterface::class, true)) {
                    if (!is_object($handler)) {
                        $handler = new $handler();
                    }
                    /** @var EventHandlerInterface $handler */
                    $handler->handle($eventObject, $this);
                }
                $event = new ExecEvent([
                    'eventObject' => $eventObject,
                    'handler' => $handler
                ]);
                $this->trigger(static::EVENT_AFTER_EXEC_HANDLER, $event);
            } catch (\Throwable $e) {
                $this->handleError($eventObject, $handler, $e);
            }
        }
        $event = new ExecEvent([
            'eventObject' => $eventObject
        ]);
        $this->trigger(static::EVENT_AFTER_EXEC, $event);
        return true;
    }

    /**
     * @param EventObject $eventObject
     * @param $handler
     * @param $error
     */
    public function handleError(EventObject $eventObject, $handler, $error)
    {
        $event = new ErrorEvent([
            'handler' => $handler,
            'eventObject' => $eventObject,
            'error' => $error
        ]);
        $this->trigger(static::EVENT_AFTER_ERROR, $event);
    }

    /**
     * @param string $eventId
     * @param string $eventType
     * @return array
     */
    protected function findHandlersForEvent(string $eventId, string $eventType): array
    {
        $eventHandlers = ArrayHelper::getValue($this->eventHandlers, $eventId, []);
        if (!is_array($eventHandlers)) {
            $eventHandlers = [$eventHandlers];
        }
        $response = [];
        foreach ($eventHandlers as $key => $eventHandler) {
            if (is_integer($key) || $key == $eventType) {
                if (!is_array($eventHandler)) {
                    $eventHandler = [$eventHandler];
                }
                $response = array_merge($response, $eventHandler);
            }
        }
        return $response;
    }

    /**
     * Действия до создания форка процесса (Закрытие соединений и etc.)
     */
    public function flushConnections()
    {
    }

    /**
     * Получение очередного события
     * @param string|null $id
     * @return EventObject|null
     */
    public abstract function getEvent(string $id = null): ?EventObject;

    /**
     * Удаление события
     * @param string $id
     * @return bool
     */
    public abstract function deleteEvent(string $id): bool;

    /**
     * Очистка очереди событий
     * @return bool
     */
    public abstract function clearEvents(): bool;

    /**
     * Текущее состоянии событий
     * @return array
     */
    public abstract function eventsInfo(): array;
}