<?php

namespace app\components\dbevent\events;


use app\components\dbevent\EventObject;
use yii\base\Event;

/**
 * Class ExecEvent
 * @package app\components\dbevent\events
 */
class ExecEvent extends Event
{
    /**
     * @var EventObject
     */
    public $eventObject;

    /**
     * @var null
     */
    public $handler = null;

    /**
     * Event was handled in this ExecEvent and not require to handle by other handlers
     * @var bool
     */
    public $handled = false;
}