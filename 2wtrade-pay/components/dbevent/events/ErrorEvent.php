<?php

namespace app\components\dbevent\events;

/**
 * Class ErrorEvent
 * @package app\components\dbevent\events
 */
class ErrorEvent extends ExecEvent
{
    /**
     * @var \Throwable
     */
    public $error;

    /**
     * Event should retry to handle
     * @var bool
     */
    public $retry = false;
}