<?php

namespace app\components\dbevent;


use yii\base\Model;

/**
 * Class EventObject
 * @package app\components\dbevent
 */
class EventObject extends Model
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $eventId;

    /**
     * @var string|array
     */
    public $primaryKey;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string|array
     */
    public $values;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'eventId', 'type'], 'string'],
            [['values', 'primaryKey'], 'validateJsonValues']
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateJsonValues($attribute, $params)
    {
        if (is_string($this->{$attribute})) {
            $this->{$attribute} = json_decode($this->{$attribute}, true);
        }
    }
}