<?php

namespace app\components\dbevent;


use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\Process;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class Command
 * @package app\components\dbevent
 */
class Command extends Controller
{
    /**
     * @var EventWatcher
     */
    public $watcher;

    /**
     * @var bool
     */
    public $isolate = true;

    /**
     * @var string
     */
    public $defaultAction = 'info';

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        if ($this->canIsolate($actionID)) {
            $options[] = 'isolate';
        }

        return $options;
    }

    /**
     * @inheritdoc
     */
    protected function isWorkerAction($actionID)
    {
        return in_array($actionID, ['run', 'listen'], true);
    }

    /**
     * @param string $actionID
     * @return bool
     */
    protected function canIsolate($actionID)
    {
        return $this->isWorkerAction($actionID);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($this->canIsolate($action->id) && $this->isolate) {
            $this->watcher->customHandler = [$this, 'handleEvent'];
        } else {
            $this->watcher->customHandler = null;
        }

        return parent::beforeAction($action);
    }

    /**
     * Handle all events
     */
    public function actionRun()
    {
        return $this->watcher->run(false);
    }

    /**
     * Information about events
     */
    public function actionInfo()
    {
        $info = $this->watcher->eventsInfo();
        $this->stdout("Events:" . PHP_EOL);
        foreach ($info as $key => $value) {
            $this->stdout("- {$key}: {$value}" . PHP_EOL);
        }
    }

    /**
     * Handle all events and always waiting new
     * @param int $timeout number of second to wait a new event
     */
    public function actionListen($timeout = 3)
    {
        return $this->watcher->run(true, $timeout);
    }

    /**
     * Clear event queue
     */
    public function actionClear()
    {
        if ($this->confirm('Are you sure that want clear events queue?')) {
            $this->watcher->clearEvents();
            $this->stdout('All events was removed from queue.');
        }
    }

    /**
     * Remove event from queue
     * @param int $id
     * @return int exit code
     */
    public function actionRemove($id)
    {
        if ($this->watcher->deleteEvent($id)) {
            $this->stdout("The job has been removed.\n");
            return ExitCode::OK;
        }

        $this->stdout("The job was not found.\n");
        return ExitCode::DATAERR;
    }

    /**
     * @param string $id
     * @return int
     */
    public function actionExec($id)
    {
        if (($eventObject = $this->watcher->getEvent($id)) && $this->watcher->handleEvent($eventObject)) {
            return ExitCode::OK;
        }

        return ExitCode::UNSPECIFIED_ERROR;
    }

    /**
     * @param EventObject $eventObject
     * @return bool
     * @throws \Exception
     */
    public function handleEvent($eventObject)
    {
        // Если у нас есть расширение pcntl, то работаем через fork
        if (extension_loaded('pcntl')) {
            // Закрываем соединения
            $this->watcher->flushConnections();
            $pid = pcntl_fork();
            if ($pid == -1) {
                throw new \Exception("Unable to create fork process.");
            } else if ($pid) {
                // Родительский процесс
                pcntl_waitpid($pid, $status);
                return $status == 0;
            } else {
                $this->watcher->customHandler = null;
                exit($this->watcher->handleEvent($eventObject) ? 0 : 1);
            }
        } else {
            // Executes child process
            $cmd = strtr('php yii queue/exec "id"', [
                'php' => PHP_BINARY,
                'yii' => $_SERVER['SCRIPT_FILENAME'],
                'queue' => $this->uniqueId,
                'id' => $eventObject->id
            ]);
            foreach ($this->getPassedOptions() as $name) {
                if (in_array($name, $this->options('exec'), true)) {
                    $cmd .= ' --' . $name . '=' . $this->$name;
                }
            }
            if (!in_array('color', $this->getPassedOptions(), true)) {
                $cmd .= ' --color=' . $this->isColorEnabled();
            }

            $process = new Process($cmd, null, null, null, null);
            try {
                $process->run(function ($type, $buffer) {
                    if ($type === Process::ERR) {
                        $this->stderr($buffer);
                    } else {
                        $this->stdout($buffer);
                    }
                });
            } catch (ProcessTimedOutException $error) {
                $this->watcher->handleError($eventObject, null, $error);
                return false;
            }

            return $process->isSuccessful();
        }
    }
}