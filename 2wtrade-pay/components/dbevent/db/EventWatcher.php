<?php

namespace app\components\dbevent\db;

use app\components\dbevent\EventObject;
use yii\caching\Cache;
use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;
use yii\mutex\Mutex;

/**
 * Class EventWatcher
 * @package app\components\dbevent\db
 */
class EventWatcher extends \app\components\dbevent\EventWatcher
{
    /**
     * @var Connection|string|array
     */
    public $db = 'db';

    /**
     * @var string
     */
    public $table = '{{%db_events}}';

    /**
     * @var string|array|Mutex
     */
    public $mutex = 'mutex';

    /**
     * @var Cache|string|array
     */
    public $cache = 'cache';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::class);
        $this->mutex = Instance::ensure($this->mutex, Mutex::class);
        $this->cache = Instance::ensure($this->cache, Cache::class);
    }

    /**
     * Получение очередного события
     * @param string|null $id
     * @return EventObject|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getEvent(string $id = null): ?EventObject
    {
        $object = null;
        $query = (new Query())->from($this->table)->orderBy('created_at');
        if ($id) {
            $query->andWhere(['id' => $id]);
            $record = $query->limit(1)->one();
            if ($record) {
                $object = $this->convertRecordToObject($record);
            }
        } else {
            $prefix = $this->getCachePrefix();
            if ($this->mutex->acquire($prefix . '.moving_lock', 5)) {
                $reserved = [];
                if ($reservedData = json_decode($this->cache->get("{$prefix}.reserved"), true)) {
                    foreach ($reservedData as $data) {
                        if ($this->mutex->acquire($data['lock_name'])) {
                            $this->mutex->release($data['lock_name']);
                        } else {
                            $reserved[] = $data;
                        }
                    }
                }
                if (!empty($reserved)) {
                    $conditions = [];
                    foreach ($reserved as $reservedData) {
                        $conditions[] = [
                            'event_id' => $reservedData['event_id'],
                            'primary_key' => $reservedData['primary_key']
                        ];
                    }
                    if (($length = count($conditions)) > 1) {
                        array_unshift($conditions, 'OR');
                    } elseif ($length == 1) {
                        $conditions = $conditions[0];
                    }

                    if (!empty($conditions)) {
                        $query->andWhere(['NOT', $conditions]);
                    }
                }
                $record = $query->limit(1)->one();
                if ($record) {
                    $object = $this->convertRecordToObject($record);
                    $lockName = $this->getLockName($object);
                    $reserved[] = [
                        'event_id' => $object->eventId,
                        'primary_key' => json_encode($object->primaryKey),
                        'lock_name' => $lockName
                    ];
                    if (!$this->mutex->acquire($lockName)) {
                        $object = null;
                    }
                }
                $this->cache->set("{$prefix}.reserved", json_encode($reserved));
                $this->mutex->release($prefix . '.moving_lock');
            }
        }

        return $object;
    }

    /**
     * Удаление события
     * @param string $id
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function deleteEvent(string $id): bool
    {
        if ($object = $this->getEvent($id)) {
            $this->db->createCommand()->delete($this->table, ['id' => $id])->execute();
            $this->mutex->release($this->getLockName($object));
        }
        return true;
    }

    /**
     * Очистка очереди событий
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function clearEvents(): bool
    {
        $prefix = $this->getCachePrefix();
        if ($this->mutex->acquire($prefix . '.moving_lock', 30)) {
            $this->db->createCommand()->delete($this->table)->execute();
            $reserved = $this->cache->get("{$prefix}.reserved");
            if ($reserved = json_decode($reserved, true)) {
                foreach ($reserved as $data) {
                    $this->mutex->release($data['lock_name']);
                }
            }
            $this->cache->delete("{$prefix}.reserved");
            $this->mutex->release($prefix . '.moving_lock');
        } else {
            return false;
        }
        return true;
    }

    /**
     * Текущее состоянии событий
     * @return array
     */
    public function eventsInfo(): array
    {
        return [
            'waiting' => (new Query())->from($this->table)->count(),
        ];
    }

    /**
     * @param $record
     * @return EventObject
     */
    protected function convertRecordToObject($record): ?EventObject
    {
        $object = new EventObject([
            'id' => $record['id'],
            'primaryKey' => $record['primary_key'],
            'eventId' => $record['event_id'],
            'values' => $record['values'],
            'type' => $record['type']
        ]);
        $object->validate();
        return $object;
    }

    /**
     * @param EventObject $eventObject
     * @return string
     */
    protected function getLockName(EventObject $eventObject)
    {
        return __CLASS__ . $eventObject->eventId . json_encode($eventObject->primaryKey);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    protected function getCachePrefix()
    {
        return $this->getCommandId() . '.' . (explode("=", $this->db->dsn)[2] ?? '') . '.' . $this->table;
    }

    /**
     * @inheritdoc
     */
    public function flushConnections()
    {
        $this->db->close();
    }
}