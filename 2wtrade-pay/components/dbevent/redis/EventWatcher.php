<?php

namespace app\components\dbevent\redis;


use app\components\dbevent\EventObject;
use yii\di\Instance;
use yii\redis\Connection;

/**
 * Class EventWatcher
 * @package app\components\dbevent\redis
 */
class EventWatcher extends \app\components\dbevent\EventWatcher
{
    /**
     * @var Connection|string|array
     */
    public $redis = 'redis';

    /**
     * @var string
     */
    public $channel = 'db_events';

    /**
     * Кол-во секунд, на которое блокируется event на выполнение
     * @var int
     */
    public $eventLockTime = 60;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, Connection::class);
    }


    /**
     * Получение очередного события
     * @param string|null $id
     * @return EventObject|null
     */
    public function getEvent(string $id = null): ?EventObject
    {
        // Moves delayed and reserved jobs into waiting list with lock for one second
        if ($this->redis->set("$this->channel.moving_lock", true, 'NX', 'EX', 1)) {
            $this->moveExpired("$this->channel.reserved");
        }

        if (empty($id)) {
            $id = $this->redis->rpop("$this->channel.waiting");
        }
        if (empty($id)) {
            return null;
        }

        if (($message = $this->redis->hget("$this->channel.messages", $id)) && ($object = $this->convertRecordToObject($id, $message))) {
            $this->redis->zadd("$this->channel.reserved", time() + $this->eventLockTime, $id);
            return $object;
        }
        $this->redis->hdel("$this->channel.messages", $id);
        return null;
    }

    /**
     * @param string $id
     * @param string $message
     * @return EventObject|null
     */
    protected function convertRecordToObject($id, $message): ?EventObject
    {
        $data = json_decode($message, true);
        if (is_array($data)) {
            return new EventObject([
                'id' => $id,
                'type' => $data['type'] ?? null,
                'eventId' => $data['event_id'] ?? null,
                'primaryKey' => $data['primary_key'] ?? null,
                'values' => $data['values'] ?? null
            ]);
        }
        return null;
    }

    /**
     * @param string $from
     */
    protected function moveExpired($from)
    {
        $now = time();
        if ($expired = $this->redis->zrevrangebyscore($from, $now, '-inf')) {
            $this->redis->zremrangebyscore($from, '-inf', $now);
            foreach ($expired as $id) {
                $this->redis->rpush("$this->channel.waiting", $id);
            }
        }
    }

    /**
     * Удаление события
     * @param string $id
     * @return bool
     */
    public function deleteEvent(string $id): bool
    {
        $result = false;
        if ($this->redis->hdel("$this->channel.messages", $id)) {
            $this->redis->zrem("$this->channel.reserved", $id);
            $this->redis->lrem("$this->channel.waiting", 0, $id);
            // Если удаляем, значит обработали event
            $this->redis->incr("$this->channel.done_count");
            $result = true;
        }
        return $result;
    }

    /**
     * Очистка очереди событий
     * @return bool
     * @throws \yii\db\Exception
     */
    public function clearEvents(): bool
    {
        while (!$this->redis->set("$this->channel.moving_lock", true, 'NX', 'EX', $this->eventLockTime)) {
            usleep(10000);
        }
        $this->redis->executeCommand('DEL', $this->redis->keys("$this->channel.*"));
        return true;
    }

    /**
     * Текущее состоянии событий
     * @return array
     */
    public function eventsInfo(): array
    {
        $prefix = $this->channel;
        $waiting = $this->redis->llen("$prefix.waiting");
        $reserved = $this->redis->zcount("$prefix.reserved", '-inf', '+inf');
        $done = $this->redis->get("$prefix.done_count") ?? 0;

        return [
            'waiting' => $waiting,
            'reserved' => $reserved,
            'done' => $done
        ];
    }

    /**
     * @inheritdoc
     */
    public function flushConnections()
    {
        $this->redis->close();
    }
}