<?php

namespace app\components\console;

use app\components\ComponentTrait;

/**
 * Class Controller
 * @package app\components\console
 */
class Controller extends \yii\console\Controller
{
    use ComponentTrait;
}