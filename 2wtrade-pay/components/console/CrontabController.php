<?php
namespace app\components\console;

use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLog;
use yii\helpers\Console;

/**
 * Class Controller
 * @package app\components\console
 */
class CrontabController extends Controller
{
    /**
     * @var array
     */
    protected $mapTasks = [];

    /**
     * @var CrontabTask
     */
    protected $task;

    /**
     * @var CrontabTaskLog
     */
    protected $log;

    /**
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (!$this->isActiveTask($action)) {
            $this->log('The task is not found or inactive.');

            return false;
        }

        $this->log = new CrontabTaskLog([
            'task_id' => $this->task->id,
            'status' => CrontabTaskLog::STATUS_IN_PROGRESS,
        ]);

        $this->log->save();

        return parent::beforeAction($action);
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if ($this->log->status != CrontabTaskLog::STATUS_ERROR) {
            $this->log->status = CrontabTaskLog::STATUS_DONE;
        }
        $this->log->save();

        return parent::afterAction($action, $result);
    }

    /**
     * @param string $message
     * @param boolean $eol
     */
    protected function log($message, $eol = true)
    {
        if (YII_ENV_DEV) {
            $message .= $eol ? PHP_EOL : '';

            Console::stdout($message);
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    private function isActiveTask($action)
    {
        $actionKey = str_replace('-', '', strtolower($action->id));

        if (array_key_exists($actionKey, $this->mapTasks)) {
            /** @var CrontabTask $task */
            $this->task = CrontabTask::find()
                ->byName($this->mapTasks[$actionKey])
                ->one();

            if ($this->task && $this->task->active) {
                return true;
            }
        }

        return false;
    }

    /**
     * Установить CrontabTaskLog в состояние Ошибка
     */
    public function setInError()
    {
        $this->log->status = CrontabTaskLog::STATUS_ERROR;
        $this->log->save();
    }
}
