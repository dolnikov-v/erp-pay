<?php

namespace app\components\console;

use yii\helpers\Console;

/**
 * Class WorkerController
 * @package app\components\console
 */
class WorkerController extends Controller
{
    /**
     * @param string $message
     * @param boolean $eol
     */
    protected function log($message, $eol = true)
    {
        if (YII_ENV_DEV) {
            $message .= $eol ? PHP_EOL : '';

            Console::stdout($message);
        }
    }
}