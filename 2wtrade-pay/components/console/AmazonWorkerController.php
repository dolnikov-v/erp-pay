<?php

namespace app\components\console;

use yii\helpers\ArrayHelper;

/**
 * Class AmazonWorkerController
 * @package app\components\console
 */
class AmazonWorkerController extends WorkerController
{
    /**
     * @param string $queueName
     * @param callable $callback
     * @param null|string $errorQueueName
     * @param int|null $waitTimeout
     */
    protected function getMessagesFromQueue(string $queueName, callable $callback, ?string $errorQueueName = null, ?int $waitTimeout = 60)
    {
        $success = [];
        $errors = [];

        $logger = \Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $client = $this->getSqsClient();
            $response = $client->receiveMessage([
                'MaxNumberOfMessages' => 10,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $client->getSqsQueueUrl($queueName),
            ]);

            $messages = $response->get('Messages');

            if (count($messages) == 0) {
                if ($waitTimeout) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    $this->log('Empty queue / Timeout ' . $waitTimeout . ' seconds');
                    sleep($waitTimeout);
                }
            } else {
                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

                if (is_array($messageBodies)) {
                    foreach ($messageBodies as $messageId => $messageBody) {

                        $message = json_decode($messageBody, 1);

                        // Вызов внешней callback функции
                        $data = call_user_func($callback, $message);

                        if ($data['status'] == true) {
                            $success[$messageId] = $handlers[$messageId] ?? '';
                        } else {
                            $errors[$messageId] = $message;
                            $errors[$messageId]['errorMessage'] = $data['message'];
                        }
                    }
                }

                if (!empty($errors)) {
                    if ($errorQueueName) {
                        $errorMessages = [];

                        foreach ($errors as $key => $values) {
                            $errorMessages[] = [
                                'Id' => $key,
                                'MessageBody' => json_encode($values, JSON_UNESCAPED_UNICODE),
                                'MessageGroupId' => md5(time()),
                                'MessageDeduplicationId' => time(),
                            ];
                        }
                        $result = $client->sendMessageBatch([
                            'QueueUrl' => $client->getSqsQueueUrl($errorQueueName),
                            'Entries' => $errorMessages,
                        ]);

                        $failedRequests = $result->get('Failed');
                        if (is_array($failedRequests)) {
                            foreach ($failedRequests as $request) {
                                $cronLog("Error while sending a message to error queue: {$request['Message']}", [
                                    'body' => json_encode($errors[$request['Id']], JSON_UNESCAPED_UNICODE),
                                    'error' => $request['Message']
                                ]);
                                unset($handlers[$request['Id']]);
                            }
                        }
                    } else {
                        foreach ($errors as $key => $values) {
                            $cronLog("Error when handling message from {$queueName}.", [
                                'messageId' => $key,
                                'errorBody' => json_encode($values, JSON_UNESCAPED_UNICODE)
                            ]);
                        }
                    }
                }

                //delete messages from queue
                if (!empty($handlers)) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $client->getSqsQueueUrl($queueName),
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }
            }
        } catch (\Throwable $e) {
            $cronLog("Error: " . $e->getMessage());
            $this->log($e->getMessage());
        }

        $this->log('Receiving done');
        $this->log('Count messages: ' . sizeof($success));
        if ($errors) {
            $this->log('Errors: ' . sizeof($errors));
            foreach ($errors as $error) {
                $this->log($error['errorMessage']);
            }
        }
    }
}