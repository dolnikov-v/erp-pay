<?php
namespace app\components;

use app\components\curl\CurlFactory;
use app\models\Currency as CurrencyModel;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use yii\base\Component;

/**
 * Обновление курса валют с использванием Yahoo
 *
 * Class Currency
 * @package app\components
 */
class Currency extends Component
{
    const DEFAULT_CURRENCY = 'USD';

    private static $url = 'https://query.yahooapis.com/v1/public/yql';
    private static $format = 'json';
    private static $env = 'store://datatables.org/alltableswithkeys';

    /**
     * @param CrontabTaskLog|null $log
     */
    public static function updateRates($log = null)
    {
        $pairs = [];

        /** @var CurrencyModel[] $currencies */
        $currencies = CurrencyModel::find()->all();

        if ($currencies) {
            foreach ($currencies as $currency) {
                $pairs[] = self::DEFAULT_CURRENCY . $currency->char_code;
            }

            $url = self::generateQuery($pairs);

            $logAnswer = new CrontabTaskLogAnswer();

            if ($log) {
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $logAnswer->url = $url;
            }

            $logAnswer->answer = self::sendRequest($url);

            if ($logAnswer->answer && $content = json_decode($logAnswer->answer, true)) {
                if ($log) {
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                }

                self::saveRates($content);
            }

            if ($log) {
                $logAnswer->save();
            }
        }
    }

    /**
     * @param array $pairs
     * @return string
     */
    protected static function generateQuery($pairs)
    {
        $params = [
            'q' => 'select * from yahoo.finance.xchange where pair in ("' . implode('", "', $pairs) . '")',
            'format' => self::$format,
            'env' => self::$env,
        ];

        return self::$url . '?' . http_build_query($params);
    }

    /**
     * @param string $content
     * @return boolean
     */
    protected static function saveRates($content)
    {
        $result = false;

        if (isset($content['query']['results']['rate'])) {
            $rates = $content['query']['results']['rate'];

            foreach ($rates as $rate) {
                if (isset($rate['Name']) && isset($rate['Rate']) && isset($rate['Ask']) && isset($rate['Bid'])) {
                    $charCode = explode('/', $rate['Name'])[1];

                    $currency = CurrencyModel::findByCharCode($charCode);

                    if ($currency) {
                        $history = new CurrencyRateHistory();

                        $history->currency_id = $currency->id;
                        $history->rate = (float)$rate['Rate'];
                        $history->ask = (float)$rate['Ask'];
                        $history->bid = (float)$rate['Bid'];

                        $history->save();

                        $currencyRate = CurrencyRate::find()
                            ->where(['currency_id' => $currency->id])
                            ->one();

                        if (!$currencyRate) {
                            $currencyRate = new CurrencyRate();
                            $currencyRate->currency_id = $currency->id;
                        }

                        $currencyRate->rate = (float)$rate['Rate'];
                        $currencyRate->ask = (float)$rate['Ask'];
                        $currencyRate->bid = (float)$rate['Bid'];

                        $currencyRate->save();
                    }
                }
            }

            $result = true;
        }

        return $result;
    }

    /**
     * @param $url
     * @param int $attempt
     * @return mixed
     */
    protected static function sendRequest($url, $attempt = 1)
    {
        if ($attempt > 10) {
            return false;
        }

        $curl = CurlFactory::build(CurlFactory::METHOD_GET, $url);

        $curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOption(CURLOPT_FOLLOWLOCATION, true);
        $curl->setOption(CURLOPT_CONNECTTIMEOUT, 120);
        $curl->setOption(CURLOPT_TIMEOUT, 120);
        $response = $curl->query();
        if (!$response && !json_decode($response)) {
            $attempt++;
            sleep(60);
            return self::sendRequest($url, $attempt);
        }
        return $response;
    }
}
