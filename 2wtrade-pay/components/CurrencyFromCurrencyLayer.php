<?php

namespace app\components;


use app\helpers\CurrencyLayerAPI;
use app\models\Currency as CurrencyModel;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class CurrencyFromCurrencyLayer
 * @package app\components
 */
class CurrencyFromCurrencyLayer extends Component
{
    const DEFAULT_CURRENCY = 'USD';

    /**
     * @param CrontabTaskLog|null $log
     */
    public static function updateRates($log = null)
    {
        /** @var CurrencyModel[] $currencies */
        $currencies = CurrencyModel::find()->with('currencyRate')->all();

        if ($currencies) {

            $logAnswer = new CrontabTaskLogAnswer();

            if ($log) {
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            }

            $result = CurrencyLayerAPI::live(ArrayHelper::getColumn($currencies, 'char_code'), self::DEFAULT_CURRENCY);
            $logAnswer->url = $result['url'];
            $logAnswer->answer = $result['response'];
            if ($result['success']) {
                if ($log) {
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                }

                self::saveRates($result['result'], $currencies);
            }

            if ($log) {
                $logAnswer->save();
            }
        }
    }

    /**
     * @param array $rates
     * @param CurrencyModel[] $currencies
     */
    protected static function saveRates($rates, $currencies)
    {
        foreach ($currencies as $currency) {
            if (isset($rates[self::DEFAULT_CURRENCY . $currency->char_code])) {
                $rate = $rates[self::DEFAULT_CURRENCY . $currency->char_code];
                $history = CurrencyRateHistory::findOne([
                    'currency_id' => $currency->id,
                    'date' => date('Y-m-d')
                ]);

                if (!$history) {
                    $history = new CurrencyRateHistory();
                }

                $history->currency_id = $currency->id;
                $history->rate = (float)$rate;
                $history->ask = (float)$rate;
                $history->bid = (float)$rate;

                $history->save();

                $currencyRate = $currency->currencyRate;

                if (!$currencyRate) {
                    $currencyRate = new CurrencyRate();
                    $currencyRate->currency_id = $currency->id;
                }

                $currencyRate->rate = (float)$rate;
                $currencyRate->ask = (float)$rate;
                $currencyRate->bid = (float)$rate;

                $currencyRate->save();
            }
        }
    }
}