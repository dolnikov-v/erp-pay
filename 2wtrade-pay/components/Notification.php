<?php
namespace app\components;

use app\components\base\Component;
use app\jobs\CreateNotificationJob;
use app\models\AuthAssignment;
use app\models\NotificationRole;
use app\models\NotificationUser;
use app\models\User;
use app\models\UserCountry;
use app\models\UserNotificationSetting;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Notification
 * @package app\components
 */
class Notification extends Component
{
    /**
     * @param string $trigger
     * @param array $params
     * @param integer $countryId
     * @param integer|array $userId нотификация только для этого пользователя или массива пользователей
     *
     * @return bool | null
     */
    public function send($trigger, $params = null, $countryId = null, $userId = null)
    {
        $this->queue->push(new CreateNotificationJob(['trigger' => $trigger, 'params' => $params, 'countryId' => $countryId, 'userId' => $userId]));
        return true;
    }

    /**
     * @param string $trigger
     * @param integer $countryId
     * @return array
     */
    public function getListUsers($trigger, $countryId = null)
    {
        $roleByRole = NotificationRole::find()->select(['role'])->where(['trigger' => $trigger]);
        $usersByTriggerRole = AuthAssignment::find()
            ->select([AuthAssignment::tableName() . '.user_id'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . AuthAssignment::tableName() . '.user_id')
            ->where(['IN', 'item_name', $roleByRole])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        $usersActive = ArrayHelper::map($usersByTriggerRole, 'user_id', 'user_id');

        $notificationUser = NotificationUser::find()
            ->select([NotificationUser::tableName() . '.user_id', 'active'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . NotificationUser::tableName() . '.user_id')
            ->where(['trigger' => $trigger])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        foreach ($notificationUser as $u) {
            if ($u['active']) {
                if (!isset($usersActive[$u['user_id']])) {
                    $usersActive[$u['user_id']] = $u['user_id'];
                }
            } else {
                if (isset($usersActive[$u['user_id']])) {
                    unset($usersActive[$u['user_id']]);
                }
            }
        }

        $notificationUserSetting = UserNotificationSetting::find()
            ->select([UserNotificationSetting::tableName() . '.user_id', 'active'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . UserNotificationSetting::tableName() . '.user_id')
            ->where(['trigger' => $trigger])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        foreach ($notificationUserSetting as $u) {
            if ($u['active']) {
                if (!isset($usersActive[$u['user_id']])) {
                    $usersActive[$u['user_id']] = $u['user_id'];
                }
            } else {
                if (isset($usersActive[$u['user_id']])) {
                    unset($usersActive[$u['user_id']]);
                }
            }
        }

        $successUsers = [];

        if (!is_null($countryId)) {

            $usersByCountry = UserCountry::find()
                ->select('user_id')
                ->where(['IN', 'user_id', $usersActive])
                ->andWhere(['country_id' => $countryId])
                ->asArray()
                ->all();

            if ($usersByCountry) {
                $usersByCountry = ArrayHelper::getColumn($usersByCountry, 'user_id');
                $successUsers += $usersByCountry;
            }
            $superAdmins = Yii::$app->getAuthManager()->getUserIdsByRole(User::ROLE_SUPERADMIN);
            foreach ($superAdmins as $admin) {
                if (in_array($admin, $usersActive)) {
                    if (!in_array($admin, $successUsers)) {
                        $successUsers[] = $admin;
                    }
                }
            }
        } else {
            $successUsers = $usersActive;
        }

        return $successUsers;
    }
}

