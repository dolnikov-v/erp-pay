<?php
namespace app\components\mongodb;

use app\components\ModelTrait;
use yii\mongodb\ActiveRecord as BaseActiveRecord;

/**
 * Class ActiveRecord
 * @package app\components\db
 */
class ActiveRecord extends BaseActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}
