<?php

namespace app\components\mongodb;


use MongoDB\BSON\Javascript;
use yii\mongodb\ActiveQuery;
use yii\mongodb\Connection;

/**
 * Class ResponseActiveQuery
 * @package app\components\mongodb
 */
class ResponseActiveQuery extends ActiveQuery
{
    const GROUP_FIELD = null;

    /**
     * @param string|Javascript $finalizeFunction
     * @param Connection $db
     * @return array
     * @throws \yii\mongodb\Exception
     * @throws \Exception
     */
    public function lastResponses($finalizeFunction = null, $db = null)
    {
        $options = [
            'condition' => $this->where
        ];
        if ($finalizeFunction) {
            $options['finalize'] = $finalizeFunction;
        }
        if (!static::GROUP_FIELD) {
            throw new \Exception('Group field is not set.');
        }
        $records = $this->getCollection($db)->group([static::GROUP_FIELD => 1], [
            'created_at' => null,
            'response' => null
        ], $this->getGroupForLastResponseFunction(), $options);
        if ($this->asArray) {
            return $records;
        }
        $result = [];
        foreach ($records as $record) {
            $result[] = new $this->modelClass($record);
        }
        return $result;
    }

    /**
     * @return Javascript
     */
    protected function getGroupForLastResponseFunction(): Javascript
    {
        $script = <<< SCRIPT
function (curr, result) { if (result.created_at < curr.created_at) { result.created_at = curr.created_at; result.response = curr.response; } }
SCRIPT;
        return new Javascript($script);
    }
}