<?php

namespace app\components;

use app\components\amazon\sqs\SqsClient;
use app\components\queue\BaseQueue;
use app\modules\smsnotification\abstracts\SmsServiceInterface;
use yii\caching\Cache;
use yii\mutex\Mutex;
use yii\swiftmailer\Mailer;

/**
 * Trait ComponentTrait
 * @package app\components
 *
 * @property SqsClient $sqsClient
 * @property Mailer $mailer
 * @property Notification $notification
 * @property Notifier $notifier
 * @property SmsServiceInterface $smsService
 * @property BaseQueue $queueTransport
 * @property BaseQueue $queueOrders
 * @property BaseQueue $queue
 * @property Mutex $mutex
 * @property Cache $cache
 */
trait ComponentTrait
{
    /**
     * @return \app\components\amazon\sqs\SqsClient|null
     */
    public function getSqsClient()
    {
        return \Yii::$app->sqsClient;
    }

    /**
     * @return Mailer
     */
    public function getMailer()
    {
        return \Yii::$app->mailer;
    }

    public function getSessionKey()
    {
        return \Yii::$app->sessionKey;
    }

    /**
     * @return Notification
     */
    public function getNotification()
    {
        return \Yii::$app->notification;
    }

    /**
     * @return Notifier
     */
    public function getNotifier()
    {
        return \Yii::$app->notifier;
    }

    /**
     * @return SmsServiceInterface|object
     */
    public function getSmsService()
    {
        return \Yii::$app->get('smsService');
    }

    /**
     * @return object|BaseQueue
     */
    public function getQueueTransport()
    {
        return \Yii::$app->get('queueTransport');
    }

    /**
     * @return object|BaseQueue
     */
    public function getQueue()
    {
        return \Yii::$app->get('queue');
    }

    /**
     * @return object|BaseQueue
     */
    public function getQueueOrders()
    {
        return \Yii::$app->get('queueOrders');
    }

    /**
     * @return null|object|Mutex
     */
    public function getMutex()
    {
        return \Yii::$app->get('mutex');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getCache()
    {
        return \Yii::$app->get('cache');
    }
}