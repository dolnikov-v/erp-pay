<?php
namespace app\components\filters;

use app\abstracts\rest\AnswerInterface;
use Yii;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Class AjaxFilter
 * @package app\components\filters
 */
class AjaxFilter extends ActionFilter
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return parent::beforeAction($action);
        }

        throw new BadRequestHttpException(Yii::t('common', 'Разрешен только Ajax запрос.'));
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($result instanceof AnswerInterface) {
            $result = [
                'status' => $result->getStatus(),
                'message' => $result->getMessage(),
                'data' => $result->getData(),
            ];
        }
        return parent::afterAction($action, $result);
    }
}
