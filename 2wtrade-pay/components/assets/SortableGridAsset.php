<?php

namespace app\components\assets;

use yii\web\AssetBundle;

class SortableGridAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/widget/sortable/';

    public $css = [
        'sortable.css',
    ];

    public $js = [
        'sortable.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
        'yii\jui\JuiAsset',
    ];
}
