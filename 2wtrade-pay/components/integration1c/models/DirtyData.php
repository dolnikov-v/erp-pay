<?php

namespace app\components\integration1c\models;

/**
 * Class DirtyData
 * @package app\components\integration1c\models
 */
class DirtyData extends \app\components\db\models\DirtyData
{
    /**
     * @var string
     */
    public $group_id;

    public function attributes()
    {
        return ['id', 'data', 'action', 'group_id'];
    }

    public function safeAttributes()
    {
        return ['id', 'data', 'action', 'group_id'];
    }

    /**
     * @return null|string
     */
    public function getGroupID(): ?string
    {
        return $this->group_id ?? null;
    }

    /**
     * @throws \Exception
     */
    public function prepareData(): void
    {
        $className = 'app\\components\\integration1c\\models\\' . str_replace('_', '', ucwords($this->getID(), '_'));
        if (class_exists($className)) {
            /**
             * @var BaseChangeModel $obj
             */
            $obj = new $className($this->getData());
            if ($obj->validate()) {
                $this->setData($obj->getDirtyAttributes() ?? []);
            } else {
                throw new \Exception($obj->getFirstErrorAsString());
            }
        } else {
            throw new \Exception("Неизвестный класс {$className}");
        }
    }

    /**
     * @param $data
     * @return \app\components\db\models\DirtyData
     */
    public function setData($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        return parent::setData($data);
    }
}