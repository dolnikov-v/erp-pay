<?php

namespace app\components\integration1c\models;

/**
 * Class Storage
 * @package app\models\integration1c
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property int $created_at
 */
class Storage extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id'], 'required'],
            [['name'], 'string'],
            [['id', 'country_id'], 'filter', 'filter' => 'intval'],
            [['created_at'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'name', 'country_id', 'created_at'];
    }
}