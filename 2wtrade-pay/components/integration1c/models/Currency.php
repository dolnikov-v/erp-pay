<?php

namespace app\components\integration1c\models;

/**
 * Class Currency
 * @package app\models\integration1c
 *
 * @property int $id
 * @property string $name
 * @property string $char_code
 * @property int $num_code
 * @property int $created_at
 */
class Currency extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'filter', 'filter' => 'intval'],
            [['num_code', 'created_at'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['name', 'char_code'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'name', 'num_code', 'char_code', 'created_at'];
    }
}