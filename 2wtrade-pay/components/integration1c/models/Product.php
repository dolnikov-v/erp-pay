<?php

namespace app\components\integration1c\models;


/**
 * Class Product
 * @package app\models\integration1c
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 */
class Product extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'filter', 'filter' => 'intval'],
            [['created_at'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'created_at', 'name'];
    }
}