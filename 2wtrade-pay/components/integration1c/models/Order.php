<?php

namespace app\components\integration1c\models;


/**
 * Class Order
 * @package app\models\integration1c
 *
 * @property int $id
 * @property int $country_id
 * @property int $source_id
 * @property int $status_id
 * @property int $created_at
 * @property double $price
 * @property double $price_total
 * @property double $price_currency
 */
class Order extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id'], 'required'],
            [['id', 'country_id', 'source_id'], 'filter', 'filter' => 'intval'],
            [['created_at', 'status_id'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['price', 'price_total', 'price_currency'], 'filter', 'filter' => 'floatval', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'country_id', 'source_id', 'status_id', 'price', 'price_total', 'price_currency', 'created_at'];
    }
}