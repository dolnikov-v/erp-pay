<?php

namespace app\components\integration1c\models;


/**
 * Class OrderProduct
 * @package app\models\integration1c
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $storage_id
 * @property int $quantity
 * @property int $created_at
 * @property double $price
 */
class OrderProduct extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id'], 'required'],
            [['id', 'order_id'], 'filter', 'filter' => 'intval'],
            [['created_at', 'product_id', 'quantity', 'storage_id'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['price',], 'filter', 'filter' => 'floatval', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'order_id', 'product_id', 'storage_id', 'quantity', 'created_at', 'price'];
    }
}