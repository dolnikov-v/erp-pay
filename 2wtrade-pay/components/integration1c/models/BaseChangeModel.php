<?php

namespace app\components\integration1c\models;

use app\components\base\Model;
use yii\base\InvalidParamException;

/**
 * Class BaseChangeModel
 * @package app\models\integration1c
 */
class BaseChangeModel extends Model
{
    /**
     * @var array attribute values indexed by attribute names
     */
    private $_attributes = [];

    /**
     * @var array
     */
    public $_dirtyAttributes = [];

    /**
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        $attributes = $this->attributes();
        foreach ($config as $key => $val) {
            if (!in_array($key, $attributes, true)) {
                unset($config[$key]);
            }
        }
        parent::__construct($config);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @throws \yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if ($this->hasAttribute($name)) {
            $setter = 'set' . $name;
            if (method_exists($this, $setter)) {
                $this->$setter($value);
                $this->_attributes[$name] = $this->_dirtyAttributes[$name] = $this->$name;
            } else {
                $this->_attributes[$name] = $this->_dirtyAttributes[$name] = $value;
            }
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param string $name
     * @return mixed|null
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if (isset($this->_attributes[$name]) || array_key_exists($name, $this->_attributes)) {
            $getter = 'get' . $name;
            if (method_exists($this, $getter)) {
                return $this->$getter();
            } else {
                return $this->_attributes[$name];
            }
        } elseif ($this->hasAttribute($name)) {
            return null;
        }

        $value = parent::__get($name);

        return $value;
    }

    /**
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            try {
                $values[$name] = $this->__get($name);
            } catch (\Throwable $e) {
                $values[$name] = null;
            }
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        try {
            return $this->__get($name) !== null;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        return isset($this->_attributes[$name]) || in_array($name, $this->attributes(), true);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getAttribute($name)
    {
        return isset($this->_attributes[$name]) ? $this->_attributes[$name] : null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setAttribute($name, $value)
    {
        if ($this->hasAttribute($name)) {
            $this->_attributes[$name] = $this->_dirtyAttributes[$name] = $value;
        } else {
            throw new InvalidParamException(get_class($this) . ' has no attribute named "' . $name . '".');
        }
    }

    /**
     * @param null $names
     * @return array
     */
    public function getDirtyAttributes($names = null): array
    {
        if ($names === null) {
            $names = $this->attributes();
        }
        $names = array_flip($names);
        $attributes = [];
        if (!empty($this->_dirtyAttributes)) {
            foreach ($this->_dirtyAttributes as $name => $value) {
                if (isset($names[$name])) {
                    try {
                        $attributes[$name] = $this->__get($name);
                    } catch (\Throwable $e) {
                        $attributes[$name] = null;
                    }
                }
            }
        }
        return $attributes;
    }
}