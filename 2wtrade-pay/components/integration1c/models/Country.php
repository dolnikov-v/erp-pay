<?php

namespace app\components\integration1c\models;

/**
 * Class Country
 * @package app\models\integration1c
 *
 * @property int $id
 * @property string $name
 * @property string $char_code
 * @property int $currency_id
 * @property int $created_at
 */
class Country extends BaseChangeModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'filter', 'filter' => 'intval'],
            [['currency_id', 'created_at'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['name', 'char_code'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'name', 'currency_id', 'char_code', 'created_at'];
    }
}