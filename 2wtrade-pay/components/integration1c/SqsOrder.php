<?php

namespace app\components\integration1c;

use app\components\base\Component;
use app\components\integration1c\models\DirtyData;
use app\components\log\LoggerInterface;
use yii\di\Instance;
use yii\redis\Connection;

/**
 * Class SqsOrder
 * @package app\components\integration1c
 *
 * @property string $queueName
 * @property string $sqsName
 * @property int $countPart
 * @property Connection $redis
 */
class SqsOrder extends Component implements SenderInterface
{
    /**
     * @var string
     */
    public $queueName = 'integration1c';

    /**
     * @var string
     */
    public $sqsName = '1cErp.fifo';

    /**
     * @var Connection
     */
    public $redis = 'redis';

    /**
     * @var int
     */
    public $countPart = 100;

    /**
     * @var string|array|LoggerInterface
     */
    public $logger;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, \yii\redis\Connection::class);

        if ($this->logger) {
            $this->logger = Instance::ensure($this->logger, LoggerInterface::class);
            $this->logger->setTags([
                'route' => __FILE__,
                'process_id' => getmypid(),
            ]);
        }

        if ($this->countPart < 1) {
            $this->countPart = 100;
        }
    }

    /**
     * @throws \Exception
     */
    public function sendPart(): void
    {
        if ($data = $this->redis->lrange($this->queueName, -$this->countPart, -1)) {
            $resultData = $groupData = [];
            $groupID = null;
            $groupCount = 0;
            for ($i = count($data) - 1; $i >= 0; $i--) {
                try {
                    $preparedData = new DirtyData(json_decode($data[$i], true));
                    $preparedData->prepareData();
                    if ($preparedData->getData()) {
                        if ($groupID != $preparedData->getGroupID()) {
                            $groupID = $preparedData->getGroupID();
                            $groupCount = 0;
                            $resultData = array_merge($resultData, $groupData);
                            $groupData = [];
                        }
                        $groupData[] = $preparedData->getAttributes();
                    }
                } catch (\Throwable $e) {
                    if ($this->logger) {
                        $this->logger->log($e->getMessage(), ['messageBody' => $data[$i]]);
                    }
                } finally {
                    $groupCount++;
                }
            }

            if ($groupData) {
                if (empty($groupData[0]['group_id']) || ($groupData[0]['group_id'] && count($data) < $this->countPart)) {
                    $resultData = array_merge($resultData, $groupData);
                    $groupCount = 0;
                }

                $this->sqsClient->sendMessage([
                    'QueueUrl' => $this->sqsClient->getSqsQueueUrl($this->sqsName),
                    'MessageGroupId' => 0,
                    'MessageBody' => json_encode($resultData)]);
            }
            $this->redis->ltrim($this->queueName, 0, -(count($data) - $groupCount + 1));
        }
    }
}