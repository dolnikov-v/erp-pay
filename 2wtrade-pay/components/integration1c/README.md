### Предисловие
Для распаралеливания в FIFO в роли MessageGroupId будет в дальнейшем использоваться ID страны.
Сейчас реализация для 1 потока.

Из-за большого кол-ва сообщений прошлось засовывать их в очередь пачками раз в минуту. (самое старое в конце массива, т.е. записывать с конца)

Наименование очереди: **1cErp.fifo** (для тестов с префиксом "**dev_**")

Тип очереди: **FIFO**

Структура сообщений
----
```json5
[
    {
        id: "country", //string сущность, к которой относятся сообщения (country/delivery/order/... etc.)
        action: "insert", //string delete|update|insert
        data: {} //obj объекс со свойсвами, характерными для каждой сущности
    },
    {
        id: "order_product",
        action: "delete",
        data: {}
    }
]
```

#Объекты

currency
----
```json5
{
    id: 1, //int
    char_code: "USD", //string(3)
    num_code: 840, //int(3)
    name: "Доллар США"
}
```
country
----
```json5
{
    id: 67, //only int
    name: "Гватемала", //string
    char_code: 'GT', //string(2)
    currency_id: 50, //int
    created_at: 1497355279, //int unix timestamp
}
```
storage
----
```json5
{
    id: 193, //int
    country_id: 67, //int
    name: "Logiczen SA warehouse", //string
    created_at: 1542013712, //int unix timestamp
}
```
product
----
```json5
{
    id: 1, //int
    name: "Goji cream", //string
    created_at: 1459173640 //int unix timestamp
}
```
source
----
```json5
{
    id: 1, //int
    name: "adcombo" //string
}
```
**. . .** etc models

order
-----
```json5
{
    id: 10417724, //int Возможно стоит поменять на id
    country_id: 67, //int
    source_id: 1, //null|int
    status_id: 18, //int
    price: 250, //double
    price_total: 375, //double
    price_currency: 50, //int link currency->id
    created_at: 1546634020 //int unix timestamp
}
```
order_product
----
```json5
{
    id: 1,
    order_id: 10417724, //int
    product_id: 39, //int
    price: 250, //double
    quantity: 1, //int
    storage_id: 193 //int
}
```
