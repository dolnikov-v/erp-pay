<?php

namespace app\components\integration1c;

/**
 * Interface SenderInterface
 * @package app\components\integration1c
 */
interface SenderInterface
{
    /**
     * @throws \Exception
     */
    public function sendPart(): void;
}