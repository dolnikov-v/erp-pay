<?php

namespace app\components\mailer;


use app\components\ComponentTrait;
use app\helpers\Utils;
use yii\helpers\Url;

/**
 * Class Message
 * @package app\components\mailer
 */
class Message extends \yii\swiftmailer\Message
{
    use ComponentTrait;

    /** @var int Лимит общего размера файлов в байтах, которые отправятся как обычные файлы, а не как ссылки */
    public $maxCommonFileSizeLimit = 10485760;

    /** @var string Папка, в которую сохраняется контент */
    public $contentPath = '/var/tmp';

    /** @var array Массив путей, из которых доступно прямое чтение файлов, иначе они копируются в контент директорию */
    public $allowForAccessDirs = ['/var/tmp'];

    /** @var string Лайаут для вывода файлов */
    public $attachmentLinkLayout = 'layouts/html-attachment-links';

    /** @var string Роут, по которому будут скачиваться файлы */
    public $urlPathToDownloadFile = '/';

    /** @var string Алгоритм генерации уникального имени файла */
    public $generateFileNamesAlgorithm = 'sha256';

    /**
     * @var array
     */
    protected $attachments = [];

    /**
     * @var string
     */
    protected $defaultBody;

    /**
     * @var bool
     */
    protected $isRenderAttachmentLinks = false;

    /**
     * Прикрепление файлов в виде ссылок на скачивание
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    protected function changeLargeAttachmentsToLinks()
    {
        if (empty($this->attachments) || $this->isRenderAttachmentLinks) {
            return;
        }
        $this->sortAttachments();
        $commonSize = 0;
        $links = [];
        foreach ($this->attachments as $key => $attachment) {
            $commonSize += $attachment['size'];
            /** @var \Swift_Mime_Attachment $entity */
            $entity = $attachment['entity'];
            if ($commonSize <= $this->maxCommonFileSizeLimit) {
                if (!in_array($entity, $this->getSwiftMessage()->getChildren())) {
                    $this->getSwiftMessage()->attach($entity);
                }
            } else {
                if (in_array($entity, $this->getSwiftMessage()->getChildren())) {
                    $this->getSwiftMessage()->detach($entity);
                }
                if (!empty($attachment['content']) && empty($attachment['path'])) {
                    $attachment['path'] = $this->saveAttachmentContent($attachment['content']);
                    $this->attachments[$key] = $attachment;
                }
                if (!empty($attachment['path'])) {
                    $filename = $entity->getFilename();
                    if (!$filename) {
                        $filename = "attachment_{$key}";
                    }
                    if (!$this->attachmentInAccessibleDir($attachment['path'])) {
                        $attachment['path'] = $this->copyFileToContentPath($attachment['path']);
                        $this->attachments[$key] = $attachment;
                    }
                    $links[$filename] = $this->generateLinkForDownloadFile($attachment['path'], $filename);
                }
            }
        }

        if (!empty($links)) {
            $body = $this->getMailer()->render($this->attachmentLinkLayout, [
                'content' => $this->defaultBody,
                'links' => $links,
                'message' => $this
            ]);
            $this->isRenderAttachmentLinks = true;
            parent::setHtmlBody($body);
            $this->isRenderAttachmentLinks = false;
        }
    }

    /**
     * Генерация сслыки для скачивания файла
     *
     * @param string $path
     * @param string $fileName
     * @return string
     */
    protected function generateLinkForDownloadFile(string $path, string $fileName)
    {
        return Url::to([
            $this->urlPathToDownloadFile,
            'hash' => base64_encode(gzencode(json_encode([
                'path' => $path,
                'fileName' => $fileName
            ], JSON_UNESCAPED_UNICODE)))
        ], true);
    }

    /**
     * Сортируем привязанный контент по возрастанию размера
     */
    protected function sortAttachments()
    {
        usort($this->attachments, function ($a, $b) {
            if ($a['size'] == $b['size']) {
                return 0;
            }
            return ($a['size'] < $b['size']) ? -1 : 1;
        });
    }

    /**
     * Сохранение контента в файл
     *
     * @param $content
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function saveAttachmentContent($content): string
    {
        Utils::prepareDir($this->contentPath);
        $filename = hash($this->generateFileNamesAlgorithm, $content) . '.tmp';
        $filePath = $this->getContentFilePath($filename);
        $f = fopen($filePath, 'wb');
        fwrite($f, $content);
        fclose($f);
        return $filePath;
    }

    /**
     * Проверка на то, что файл лежит в разрешенной директории
     *
     * @param $path
     * @return bool
     */
    protected function attachmentInAccessibleDir(string $path): bool
    {
        $dirs = $this->allowForAccessDirs;
        if (!is_array($dirs)) {
            $dirs = [$dirs];
        }
        $dirs[] = $this->contentPath;
        foreach ($dirs as $dir) {
            if (Utils::checkSubDirOfDir($path, $dir)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $path
     * @return string
     * @throws \Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function copyFileToContentPath(string $path)
    {
        $hash = hash_file($this->generateFileNamesAlgorithm, $path);
        $filename = $hash . '.tmp';
        if (!file_exists($distPath = $this->getContentFilePath($filename))) {
            if (!copy($path, $distPath)) {
                throw new \Exception('Cannot copy attachment to content path.');
            }
        }
        return $distPath;
    }

    /**
     * @param $filename
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function getContentFilePath($filename)
    {
        Utils::prepareDir($this->contentPath);
        return rtrim($this->contentPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * @param $html
     * @return Message|\yii\swiftmailer\Message
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function setHtmlBody($html)
    {
        $value = parent::setHtmlBody($html);
        if (!$this->isRenderAttachmentLinks) {
            $this->defaultBody = $html;
            $this->changeLargeAttachmentsToLinks();
        }
        return $value;
    }

    /**
     * @param $text
     * @return Message|\yii\swiftmailer\Message
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function setTextBody($text)
    {
        $value = parent::setTextBody($text);
        if (!$this->isRenderAttachmentLinks) {
            $this->defaultBody = $text;
            $this->changeLargeAttachmentsToLinks();
        }
        return $value;
    }

    /**
     * @param $fileName
     * @param array $options
     * @return Message|\yii\swiftmailer\Message
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function attach($fileName, array $options = [])
    {
        $value = parent::attach($fileName, $options);
        $children = $this->getSwiftMessage()->getChildren();
        $this->attachments[] = [
            'entity' => end($children),
            'path' => $fileName,
            'size' => filesize($fileName)
        ];
        $this->changeLargeAttachmentsToLinks();
        return $value;
    }

    /**
     * @param $content
     * @param array $options
     * @return Message|\yii\swiftmailer\Message
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function attachContent($content, array $options = [])
    {
        $value = parent::attachContent($content, $options);
        $children = $this->getSwiftMessage()->getChildren();
        $this->attachments[] = [
            'entity' => end($children),
            'size' => sizeof($content),
            'content' => $content
        ];
        $this->changeLargeAttachmentsToLinks();
        return $value;
    }
}