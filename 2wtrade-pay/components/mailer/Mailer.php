<?php
namespace app\components\mailer;

use app\models\User;

/**
 * Class Mailer
 * @package app\components\mailer
 */
class Mailer extends \yii\swiftmailer\Mailer
{
    /**
     * @param \yii\mail\MessageInterface $message
     * @return bool
     */
    public function beforeSend($message)
    {
        // Перед отправкой проверяем адреса на валидность и возможность отправки
        $toArray = $message->getTo();
        foreach ($toArray as $key => $value) {
            if (!$this->canSendEmailToRecipient($key)) {
                unset($toArray[$key]);
            }
        }
        $message->setTo($toArray);

        $ccArray = $message->getCc() ?? [];
        foreach ($ccArray as $key => $value) {
            if (!$this->canSendEmailToRecipient($key)) {
                unset($ccArray[$key]);
            }
        }
        $message->setCc($ccArray);

        $bccArray = $message->getBcc() ?? [];
        foreach ($bccArray as $key => $value) {
            if (!$this->canSendEmailToRecipient($key)) {
                unset($bccArray[$key]);
            }
        }
        $message->setBcc($bccArray);

        if (empty($message->getTo())) {
            return false;
        }

        return parent::beforeSend($message);
    }

    /**
     * Проверка на возможность отправки сообщения на указанный адрес
     * @param $recipient
     * @return bool
     */
    protected function canSendEmailToRecipient($recipient)
    {
        return !User::find()->where(['email' => $recipient, 'status' => [User::STATUS_BLOCKED, User::STATUS_DELETED, User::STATUS_BLOCKED | User::STATUS_DELETED]])->exists();
    }
}