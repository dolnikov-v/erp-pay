<?php

namespace app\components\web;

use app\models\Country;
use Yii;

/**
 * Class UrlManager
 * @package app\components\web
 */
class UrlManager extends \yii\web\UrlManager
{
    /**
     * @var array
     */
    private $patternsIgnoredUrl = [
        '#^home/(index)#',
        '#^systemstatus/#',
        '#^logger/#',
        '#^order/(status|workflow)#',
        '#^api/#',
        '#^auth/(login|logout)#',
        '#^catalog/(country|timezone|currency|product|landing|notification|language|certificate|sms-poll-question|operators-penalty|un-buyout-reason|source|partner)#',
        '#^i18n/#',
        '#^administration/#',
        '#^profile/#',
        '#^access/#',
        '#^delivery/(api-class)#',
        '#^checklist/#',
        '#^widget/#',
        '#^salary/#',
        '#^report/(time-sheet|salary|invoice|debts)#',
        '#^finance/(report-profitability-analysis)#',
        '#^webhook/#',
        '#^marketplace/#',
    ];

    /**
     * @param array|string $params
     * @return string
     */
    public function createUrl($params)
    {
        $force_country_slug = isset($params['force_country_slug']) ? $params['force_country_slug'] : false;
        unset($params['force_country_slug']);

        $url = parent::createUrl($params);

        $route = trim($params[0], '/');

        if (!$this->isIgnoredUrl($route)) {
            $country = Yii::$app->user->getCountry();

            if ($country) {
                return '/' . (is_string($force_country_slug) ? $force_country_slug : $country->slug) . $url;
            }
        }

        return $url;
    }

    /**
     * @param string $url
     * @return boolean
     */
    public function isIgnoredUrl($url)
    {
        foreach ($this->patternsIgnoredUrl as $pattern) {
            if (preg_match($pattern, $url)) {
                return true;
            }
        }

        return false;
    }
}
