<?php

namespace app\components\web;

use app\components\base\SubLayoutContextInterface;
use app\components\ComponentTrait;
use Yii;
use yii\base\View;
use yii\web\HttpException;

/**
 * Class Controller
 * @package app\components\controllers
 */
class Controller extends \yii\web\Controller implements SubLayoutContextInterface
{
    use ComponentTrait;

    /**
     * Подшаблон, накладываемый на общий шаблон и применяемый к каждому вызову render() и renderContent()
     * @var string
     */
    public $subLayout;

    /**
     * @throws HttpException
     */
    public function init()
    {
        parent::init();

        // Выставляем тему согласно выбранной пользователем
        if (!Yii::$app->user->isGuest) {
            $theme = Yii::$app->user->identity->getUserTheme();
            Yii::$app->view->theme->basePath = '@app/web/themes/' . $theme;
            Yii::$app->view->theme->baseUrl = '@web/themes/' . $theme;
            Yii::$app->view->theme->pathMap = ['@app/views/' => '@app/web/themes/' . $theme];
        }

        $language = Yii::$app->user->getLanguage();
        $timezone = Yii::$app->user->getTimezone();

        Yii::$app->language = $language->locale;
        Yii::$app->formatter->setLocale($language->locale);
        Yii::$app->formatter->setTimezone($timezone->timezone_id);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->userActionLog->start();

        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            $user->lastact_at = time();
            $user->save(true, ['lastact_at']);
        }

        return parent::beforeAction($action);
    }

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $content = $this->getView()->render($view, $params, $this);
        return $this->renderContent($content, $params);
    }

    /**
     * @param string $content
     * @param array $params
     * @return string
     */
    public function renderContent($content, $params = [])
    {
        foreach ($this->findSubLayoutFiles($this->getView()) as $subLayout) {
            $content = $this->getView()->renderFile($subLayout, array_merge($params, ['content' => $content]), $this);
        }
        return parent::renderContent($content);
    }

    /**
     * Поиск всех возможных применяемых подшаблонов
     * @param View $view
     * @return array
     */
    public function findSubLayoutFiles($view): array
    {
        $subLayouts = [];
        $module = $this->module;
        if (is_string($this->subLayout)) {
            $subLayouts[] = $this->findSubLayoutFilePath($view, $module, $this->subLayout);
        }

        while ($module !== null) {
            if (($module instanceof SubLayoutContextInterface) && is_string($module->subLayout)) {
                $subLayouts[] = $this->findSubLayoutFilePath($view, $module, $module->subLayout);
            }
            $module = $module->module;
        }
        return $subLayouts;
    }

    /**
     * @param View $view
     * @param yii\base\Module $module
     * @param string $subLayout
     * @return bool|string
     */
    protected function findSubLayoutFilePath($view, $module, $subLayout)
    {
        if (strncmp($subLayout, '@', 1) === 0) {
            $file = Yii::getAlias($subLayout);
        } elseif (strncmp($subLayout, '/', 1) === 0) {
            $file = Yii::$app->getLayoutPath() . DIRECTORY_SEPARATOR . substr($subLayout, 1);
        } else {
            $file = $module->getLayoutPath() . DIRECTORY_SEPARATOR . $subLayout;
        }

        if (pathinfo($file, PATHINFO_EXTENSION) !== '') {
            return $file;
        } else {
            $path = $file . '.' . $view->defaultExtension;
            if ($view->defaultExtension !== 'php' && !is_file($path)) {
                $path = $file . '.php';
            }
        }
        return $path;
    }

    /**
     * @return null|string
     */
    public function getSubLayout()
    {
        return $this->subLayout;
    }
}
