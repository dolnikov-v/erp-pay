<?php

namespace app\components\web;

use app\models\Country;
use app\models\Notification;
use app\models\Timezone;
use app\models\User as UserModel;
use app\models\UserCountry;
use app\models\UserDelivery;
use app\models\UserNotification;
use app\models\UserSource;
use app\models\NotificationUser;
use app\models\NotificationRole;
use app\modules\i18n\models\Language;
use Yii;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;

/**
 * Class User
 * @package app\components\web
 * @property UserModel|null $identity
 * @property boolean $isSuperadmin
 * @property boolean $isImplant
 * @property boolean $isDeliveryManager
 * @property boolean $isBusinessAnalyst
 * @property Language $language
 * @property Country $country
 * @property Timezone $timezone
 * @property Notification[] $currentNotifications
 */
class User extends \yii\web\User
{
    /** @var array */
    public $loginUrl = ['auth/login'];

    /**
     * @var null|string
     */
    public $apiAccessType;

    /** @var array */
    private $roles;
    /** @var array */
    private $permissions;
    /** @var boolean */
    private $isSuperadmin;
    /** @var boolean */
    private $isImplant;
    /** @var boolean */
    private $isDeliveryManager;
    /** @var boolean */
    private $isBusinessAnalyst;
    /** @var Language */
    private $language;
    /** @var Country */
    private $country;
    /** @var Timezone */
    private $timezone;
    /** @var Notification[] */
    private $currentNotifications;

    /** @var array */
    public $rejectedSources = [];   // список запрещенных источников (strings)

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->identity) {
            $this->rejectedSources = UserSource::find()
                ->where([UserSource::tableName() . '.user_id' => $this->id])
                ->andWhere([UserSource::tableName() . '.active' => 0])
                ->select('source_id')->column();
        }

        if ($this->identity && $this->identity->force_logout) {
            $this->identity->force_logout = 0;
            $this->identity->save(false, ['force_logout']);
            $this->logout();

            Yii::$app->response->redirect($this->loginUrl);
            Yii::$app->end();
        }

        if ($this->identity && $this->identity->hasStatus(UserModel::STATUS_BLOCKED)) {
            $this->logout();
            Yii::$app->response->redirect($this->loginUrl);
            Yii::$app->end();
        }
    }

    /**
     * @param string $permissionName
     * @param array $params
     * @param boolean $allowCaching
     * @return boolean
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        if (is_null($this->roles)) {
            $this->initRoles();
        }

        if ($this->getIsSuperadmin()) {
            return true;
        }

        if (is_null($this->permissions)) {
            $this->initPermissions();
        }

        return array_key_exists($permissionName, $this->permissions);
    }

    /**
     * @return bool
     */
    public function getIsSuperadmin()
    {
        if ($this->isGuest) {
            return false;
        }

        if (is_null($this->isSuperadmin)) {
            $this->isSuperadmin = Yii::$app->authManager->getAssignment(UserModel::ROLE_SUPERADMIN, $this->id);

            if (is_null($this->isSuperadmin)) {
                $this->isSuperadmin = false;
            }
        }

        return $this->isSuperadmin;
    }

    /**
     * @return bool
     */
    public function getIsSecurityManager()
    {
        if ($this->isGuest) {
            return false;
        }

        $result = Yii::$app->authManager->getAssignment(UserModel::ROLE_SECURITY_MANAGER, $this->id);

        if (is_null($result)) {
            return false;
        }

        return true;
    }

    /**
     * @param null $slug
     * @throws HttpException
     */
    public function setCountry($slug = null)
    {
        if (!$this->isGuest) {
            $country = null;

            if ($slug) {
                $query = Country::find()->active();

                if (!$this->getIsSuperadmin()) {
                    $query->joinWith(['userCountry'])
                        ->where([UserCountry::tableName() . '.user_id' => $this->id]);
                }

                $country = $query->bySlug($slug)->one();
            }

            if (!$country && Yii::$app->session->get('user.country.slug')) {
                $this->country = Country::find()
                    ->active()
                    ->bySlug(Yii::$app->session->get('user.country.slug'))->one();
            } else {
                $this->country = $country;
            }

            if (is_null($this->country)) {
                $this->country = Country::getDefault();
            }

            if ($this->country) {
                Yii::$app->session->set('user.country.slug', $this->country->slug);
            } else {
                throw new HttpException(400, Yii::t('common', 'У вас не назначена страна.'));
            }
        }
    }

    /**
     * @return Language|null
     * @throws HttpException
     */
    public function getLanguage()
    {
        if (is_null($this->language)) {
            /** @var Language $language */

            $language = null;
            $user = UserModel::findOne($this->id);
            if ($user && $user->language) {
                $language = Language::find()->byLocale($user->language)->one();
            }

            if (!$language) {
                $language = Language::findOne(Yii::$app->session->get('user.language.id'));
            }

            if ($language && $language->active) {
                $this->language = $language;
            } else {
                $this->language = Language::getDefault();
            }
        }

        if (is_null($this->language)) {
            throw new HttpException(400, Yii::t('common', 'Ошибка инициализации языка интерфейса.'));
        }

        return $this->language;
    }

    /**
     * @return Country
     * @throws HttpException
     */
    public function getCountry()
    {
        if (is_null($this->country)) {
            $this->country = Country::find()->where(['slug' => Yii::$app->session->get('user.country.slug')])->one();

            if (is_null($this->country)) {
                $this->country = Country::getDefault();
            }
        }

        if (is_null($this->country)) {
            throw new HttpException(400, Yii::t('common', 'У вас не назначена страна.'));
        }

        return $this->country;
    }

    /**
     * @return Timezone
     * @throws HttpException
     */
    public function getTimezone()
    {
        if (is_null($this->timezone)) {
            if ($this->isGuest) {
                $this->timezone = Timezone::getDefault();
            } else {
                if (is_null($this->identity->timezone_id)) {
                    if ($this->getCountry()->timezone_id) {
                        $this->timezone = Timezone::findOne($this->getCountry()->timezone_id);
                    } else {
                        $this->timezone = Timezone::getDefault();
                    }
                } else {
                    $this->timezone = Timezone::findOne($this->identity->timezone_id);
                }
            }
        }

        if (is_null($this->timezone)) {
            throw new HttpException(400, Yii::t('common', 'У вас не назначена временная зона.'));
        }

        return $this->timezone;
    }

    /**
     * @return Notification[]
     */
    public function getCurrentNotifications()
    {
        if (is_null($this->currentNotifications)) {
            $this->currentNotifications = $this->identity->currentNotifications;
        }

        return $this->currentNotifications;
    }

    /**
     * @return integer
     */
    public function getCountUnreadNotifications()
    {
        return $this->identity->getCountUnreadNotifications();
    }

    /**
     * @return string
     */
    public function getRunningLineNotifications()
    {
        $messages = '';

        // КОСТЫЛЬ!!! пока не реализовали модуль новостей
        $messages .= '<span style="color: red;">' . Yii::t('common', 'Внимание! Появился единый номер экстренной поддержки +7 9513667788') . '</span>';

        return $messages;
    }

    /**
     * Инициализация ролей
     */
    private function initRoles()
    {
        $this->roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);
    }

    /**
     * Инициализация прав
     */
    private function initPermissions()
    {
        $this->permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->identity->id);
    }

    /**
     * Пользователь службы доставки, роль implant
     * @return bool
     */
    public function getIsImplant()
    {
        if (is_null($this->isImplant)) {
            $this->isImplant = Yii::$app->authManager->getAssignment(UserModel::ROLE_IMPLANT, $this->id);

            if (is_null($this->isImplant)) {
                $this->isImplant = false;
            }
        }
        return $this->isImplant;
    }

    /**
     * Пользователь менеджер доставки
     * @return bool
     */
    public function getIsDeliveryManager()
    {
        if (is_null($this->isDeliveryManager)) {
            $this->isDeliveryManager = Yii::$app->authManager->getAssignment(UserModel::ROLE_DELIVERY_MANAGER, $this->id);

            if (is_null($this->isDeliveryManager)) {
                $this->isDeliveryManager = false;
            }
        }
        return $this->isDeliveryManager;
    }

    /**
     * Пользователь бизнес аналитик
     * @return bool
     */
    public function getIsBusinessAnalyst()
    {
        if (is_null($this->isBusinessAnalyst)) {
            $this->isBusinessAnalyst = Yii::$app->authManager->getAssignment(UserModel::ROLE_BUSINESS_ANALYST, $this->id);

            if (is_null($this->isBusinessAnalyst)) {
                $this->isBusinessAnalyst = false;
            }
        }
        return $this->isBusinessAnalyst;
    }

    /**
     * привязанные пользователю службы доставки
     * @return array
     */
    public function getDeliveriesIds()
    {
        $deliveries = UserDelivery::find()->byUserId($this->id)->all();
        $delivery_ids = ArrayHelper::getColumn($deliveries, 'delivery_id');
        //если ничего не привязано то нечего показывать
        if (!$delivery_ids) $delivery_ids = [-1];
        return $delivery_ids;
    }
}
