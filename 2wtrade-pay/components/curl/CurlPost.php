<?php
namespace app\components\curl;

/**
 * Class CurlPost
 * @package app\components\curl
 */
class CurlPost extends Curl
{
    /**
     * @param string $url
     * @param array $headers
     */
    public function __construct($url, $headers = [])
    {
        parent::__construct($url, $headers);

        curl_setopt($this->curl, CURLOPT_POST, true);
    }
}
