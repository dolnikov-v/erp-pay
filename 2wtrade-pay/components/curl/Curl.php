<?php
namespace app\components\curl;

/**
 * Class Curl
 * @package app\components\curl
 */
class Curl
{
    protected $url;
    protected $curl;
    protected $httpCode;
    protected $headers = [];
    protected $fields;
    protected $answer;

    /**
     * @param string $url
     * @param array $headers
     */
    public function __construct($url, $headers = [])
    {
        $this->url = $url;
        $this->curl = curl_init($url);

        if (is_array($headers)) {
            if (empty($headers)) {
                $headers = [
                    'Accept' => 'application/json',
                ];
            }

            $this->setHeaders($headers);
        }

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HEADER, false);
        $this->setUrl($url);
    }

    /**
     * @param null|array $fields
     * @return mixed
     */
    public function query($fields = null)
    {
        $this->fields = null;

        if ($fields) {
            $this->fields = $fields;

            curl_setopt($this->curl, CURLOPT_POST, true);
            $httpQuery = preg_replace('/\%5B\d+\%5D/', '%5B%5D', http_build_query($fields));
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $httpQuery);
        }

        $this->answer = curl_exec($this->curl);
        $this->httpCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        curl_close($this->curl);

        return $this->answer ? $this->answer : null;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
    }

    /**
     * @param integer $option
     * @param mixed $value
     */
    public function setOption($option, $value)
    {
        curl_setopt($this->curl, $option, $value);
    }

    /**
     * @param $headers
     */
    protected function setHeaders($headers)
    {
        $curlHeaders = [];

        foreach ($headers as $headerKey => $headerValue) {
            $curlHeaders[] = $headerKey . ': ' . $headerValue;
        }

        $this->headers = $curlHeaders;
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $curlHeaders);
    }
}
