<?php
namespace app\components\curl;

use Yii;
use yii\base\InvalidParamException;

/**
 * Class CurlFactory
 * @package app\components\curl
 */
class CurlFactory
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';

    /**
     * @param string $method
     * @param string $url
     * @param array $headers
     * @return \app\components\curl\Curl
     * @throws InvalidParamException
     */
    public static function build($method, $url, $headers = [])
    {
        $curl = "\\app\\components\\curl\\Curl" . ucfirst(strtolower($method));

        if (class_exists($curl)) {
            return new $curl($url, $headers);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип cURL.'));
        }
    }
}
