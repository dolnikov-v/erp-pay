<?php
namespace app\components\curl;

/**
 * Class CurlGet
 * @package app\components\curl
 */
class CurlGet extends Curl
{
    /**
     * @param null|array $fields
     * @return mixed
     */
    public function query($fields = null)
    {
        if ($fields) {
            $urlParams = [];
            $parts = parse_url($this->url);

            if (!empty($parts['query'])) {
                parse_str($parts['query'], $urlParams);
            }

            $fields = array_merge($urlParams, $fields);
            $httpQuery = preg_replace('/\%5B\d+\%5D/', '%5B%5D', http_build_query($fields));
            $url = $parts['scheme'] . '://' . $parts['host'] . $parts['path'] . '?' . $httpQuery;

            $this->setUrl($url);
        }

        return parent::query();
    }
}
