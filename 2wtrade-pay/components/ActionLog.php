<?php
namespace app\components;

use app\models\UserActionLog;
use Yii;
use yii\base\Component;

/**
 * Class ActionLog
 * @package app\components
 */
class ActionLog extends Component
{
    private static $ignoredUrls = [
        '/user/ping',
    ];
    /** @var boolean */
    private $started = false;

    /** @var UserActionLog */
    private $log;

    /**
     * Начало
     */
    public function start()
    {
        $url = Yii::$app->request->url;

        if (!in_array($url, self::$ignoredUrls)) {
            $this->log = new UserActionLog();
            $this->log->user_id = Yii::$app->user->id;
            $this->log->url = $url;
            $this->log->http_status = 200;
            $this->log->get_data = json_encode(Yii::$app->request->get());
            $this->log->post_data = json_encode(Yii::$app->request->post());

            if ($this->log->save()) {
                $this->started = true;
            }
        }
    }

    /**
     * @return UserActionLog
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        return $this->started;
    }
}
