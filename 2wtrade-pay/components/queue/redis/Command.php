<?php

namespace app\components\queue\redis;

/**
 * Class Command
 * @package app\components\queue\redis
 */
class Command extends \yii\queue\redis\Command
{
    /**
     * @param null|string $id
     * @param string $message
     * @param int $ttr
     * @param int $attempt
     * @return bool
     * @throws \Exception
     */
    protected function handleMessage($id, $message, $ttr, $attempt)
    {
        // Если у нас есть расширение pcntl, то работаем через fork
        if (extension_loaded('pcntl')) {
            // Закрываем соединение
            $this->queue->redis->close();
            $pid = pcntl_fork();
            if ($pid == -1) {
                throw new \Exception("Unable to create fork process.");
            } else if ($pid) {
                // Родительский процесс
                pcntl_waitpid($pid, $status);
                return $status == 0;
            } else {
                exit($this->queue->execute($id, $message, $ttr, $attempt, $this->queue->getWorkerPid()) ? 0 : 1);
            }
        } else {
            return parent::handleMessage($id, $message, $ttr, $attempt);
        }
    }
}