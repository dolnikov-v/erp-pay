<?php

namespace app\components\queue;

use yii\queue\ExecEvent;
use yii\queue\redis\Queue;
use Yii;

/**
 * Class BaseQueue
 * @package app\components\queue
 */
class BaseQueue extends Queue
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_EXEC, function (ExecEvent $event) {
            Yii::$app->db->close();
        });
    }
}