<?php

namespace app\components\queue;

use app\jobs\BaseJob;
use app\modules\telegram\components\Telegram;
use Yii;
use yii\queue\ExecEvent;

/**
 * Class QueueTelegram
 * @package app\components\queue
 */
class QueueTelegram extends BaseQueue
{
    /**
     * @var bool
     */
    public $handled = true;

    /**
     * Инициализация и назначение экшенов
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_EXEC, function (ExecEvent $event) {
            /**
             * @var BaseJob $job
             */
            $job = $event->job;
            $errors = $job->errors;
            if (count($job->errors) > 2) {
                $errors = array_slice($job->errors, 0, 2);
                $errors[3] = '... Прочие ошибки смотрите в логах. Общее кол-во ошибок = ' . count($job->errors);
            }
            $telegram = new Telegram(Yii::$app->params['telegramBot']['token']);
            if ($job->errors) {
                $telegram->sendMessage($job->jobTelegramID, 'Задача <<' . $job->jobName . '>> выполнена с ошибками.'.PHP_EOL.implode(PHP_EOL.PHP_EOL, $errors));
            } else {
                $telegram->sendMessage($job->jobTelegramID, 'Задача <<' . $job->jobName . '>> выполнена успешно.');
            }
        });
        $this->on(self::EVENT_AFTER_ERROR, function (ExecEvent $event) {
            if (!empty($event->error)) {
                $logger = \Yii::$app->get("processingLogger");
                $log = $logger->getLogger([
                    'type' => 'job',
                    'component' => self::className(),
                    'process_id' => getmypid(),
                    'message_type' => 'exception',
                ]);
                $log($event->error);
            }
            /**
             * @var BaseJob $job
             */
            $job = $event->job;
            $errors = $job->errors;
            if (count($job->errors) > 2) {
                $errors = array_slice($job->errors, 0, 2);
                $errors[3] = '... Прочие ошибки смотрите в логах. Общее кол-во ошибок = ' . count($job->errors);
            }
            $telegram = new Telegram(Yii::$app->params['telegramBot']['token']);
            $telegram->sendMessage($job->jobTelegramID, 'Задача <<'.$job->jobName.'>> завершена с критической ошибкой.'.PHP_EOL.implode(PHP_EOL.PHP_EOL, $errors));
        });
    }
}