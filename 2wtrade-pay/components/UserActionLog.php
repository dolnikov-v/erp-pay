<?php
namespace app\components;

use app\models\UserActionLog as UserActionLogModel;
use Yii;
use yii\base\Component;

/**
 * Class UserActionLog
 * @package app\components
 */
class UserActionLog extends Component
{
    /** @var boolean */
    private $started = false;
    /** @var \app\models\UserActionLog */
    private $log;

    private static $ignoredUrls = [
        '/access/user/ping',
        '/reportoperational/export-to-mail/sent',
    ];

    /**
     * Начало
     */
    public function start()
    {
        $url = Yii::$app->request->url;

        if (!in_array($url, self::$ignoredUrls)) {

            $this->log = new UserActionLogModel();
            $this->log->user_id = Yii::$app->user->id;
            $this->log->url = $url;
            $this->log->http_status = 200;
            $this->log->get_data = @json_encode(Yii::$app->request->get());
            $this->log->post_data = @json_encode(Yii::$app->request->post());

            if ($this->log->save()) {
                $this->started = true;
            }
        }
    }

    /**
     * @return \app\models\UserActionLog
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        return $this->started;
    }
}
