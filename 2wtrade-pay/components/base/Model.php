<?php

namespace app\components\base;

use app\components\ModelTrait;

/**
 * Class Model
 * @package app\components\base
 */
class Model extends \yii\base\Model
{
    use ModelTrait;
}