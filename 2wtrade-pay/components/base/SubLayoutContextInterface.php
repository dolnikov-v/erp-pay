<?php

namespace app\components\base;

/**
 * Интерфейс, подклюяаемый классами для возможности использования подшаблонов при выводе view файлов
 *
 * Interface SubLayoutContextInterface
 * @package app\components\base
 *
 * @property string|null $subLayout
 */
interface SubLayoutContextInterface
{
    /**
     * @return null|string
     */
    public function getSubLayout();
}