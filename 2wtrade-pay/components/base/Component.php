<?php

namespace app\components\base;


use app\components\ComponentTrait;

/**
 * Class Component
 * @package app\components\base
 */
class Component extends \yii\base\Component
{
    use ComponentTrait;
}