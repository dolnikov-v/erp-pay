<?php

namespace app\components\base;

use Yii;
use yii\filters\AccessControl;
use yii\web\Application;

/**
 * Class Module
 * @package app\components\base
 */
class Module extends \yii\base\Module
{
    /** @var array */
    private $ignoredPermissions = [
        'auth.login',
        'auth.logout',
        'i18n.language.change',
        'access.user.ping',
        'messenger',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [];
        if (Yii::$app instanceof Application) {
            $permissionName = $this->getPermissionName();
            $allow = in_array($permissionName, $this->ignoredPermissions) || Yii::$app->user->can($permissionName);
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => $allow,
                        'roles' => ['@'],
                    ],
                ],
            ];
        }
        return $behaviors;
    }

    /**
     * @return string
     */
    public function getPermissionName()
    {
        $module = str_replace('-', '', strtolower(Yii::$app->controller->module->id));
        $controller = str_replace('-', '', strtolower(Yii::$app->controller->id));
        $action = str_replace('-', '', strtolower(Yii::$app->controller->action->id));

        $operation = str_replace('/', '.', $controller) . '.' . $action;

        if ($module && !in_array($module, ['basic'])) {
            $operation = $module . '.' . $operation;
        }

        return $operation;
    }
}
