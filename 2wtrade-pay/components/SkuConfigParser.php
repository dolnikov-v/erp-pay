<?php
namespace app\components;

use Yii;
use app\modules\delivery\models\DeliverySku;
use app\modules\delivery\models\DeliveryApiClass;

class SkuConfigParser
{
    public $errors = [];
    public $className;
    public $deliveryPath;

    public function __construct($className)
    {
        $this->className = $className;
        $this->deliveryPath =  Yii::getAlias('@deliveryApiClasses');
    }

    public function parse()
    {
        $class = DeliveryApiClass::findOne(['name' => $this->className]);
        $deliveries = $class->deliveries;

        foreach ($deliveries as $delivery) {
            $fullPath = $this->getFullPath($class->class_path, $delivery->country->char_code);
            $products = null;

            if (!$products = $this->parseConfig($fullPath)) {
                continue;
            };
            foreach ($products as $productId => $item) {
                $sku = new DeliverySku();
                $sku->delivery_id = $delivery->id;
                $sku->product_id = $productId;
                $sku->sku = $item;
                if (!$sku->save()) {
                    $this->errors[] = $sku->getFirstErrorAsString() . PHP_EOL;
                };
            }
        }
    }

    /**
     * @param $fullPath
     * @return bool | array
     */
    public function parseConfig($fullPath)
    {
        if (!file_exists($fullPath)) {
            $this->errors[] = print_r('File not found ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $method = $this->className . 'ConfigParser';
        if (!method_exists($this, $method)) {
            $this->errors[] = print_r('There is no method ' . $method . PHP_EOL, true);
            return false;
        }
        return $this->{$method}($fullPath);
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function aCommerceApiConfigParser($fullPath)
    {
        require ($fullPath);

        /**
         * @var array $config
         */
        if (!isset($config)) {
            $this->errors[] = print_r('There is no config in ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $items = $config['products'];
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item;
        }
        return $productsData;
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function BizApiConfigParser($fullPath)
    {
        require ($fullPath);

        /**
         * @var array $products
         */
        if (!isset($products)) {
            $this->errors[] = print_r('There is no config in ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $items = $products;
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item;
        }
        return $productsData;
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function BluedartFulfillmentConfigParser($fullPath)
    {
        $config = require ($fullPath);

        /**
         * @var array $products
         */
        if (!isset($config)) {
            $this->errors[] = print_r('There is no config in ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $items = $config['products'];
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item;
        }
        return $productsData;
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function TrafLeadApiConfigParser($fullPath)
    {
        require ($fullPath);

        /**
         * @var array $products
         */
        if (!isset($this->articles)) {
            $this->errors[] = print_r('There is no config in ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $items = $this->articles;
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item;
        }
        return $productsData;
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function UPSConfigParser($fullPath)
    {
        $config = require ($fullPath);

        /**
         * @var array $products
         */
        if (!isset($config['products'])) {
            $this->errors[] = print_r('There is no config in ' . $fullPath . PHP_EOL, true);
            return false;
        }
        $items = $config['products'];
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item['sku'];
        }
        return $productsData;
    }

    /**
     * Returns list of products with sku from config file
     * @param $fullPath
     * @return array|bool
     */
    public function pfcApiConfigParser($fullPath)
    {
        $product = null;
        require ($fullPath);

        /**
         * @var array $product
         */
        if (!isset($product)) {
            return false;
        }
        $items = $product;
        $productsData = [];
        foreach ($items as $id => $item) {
            $productsData[$id] = $item['sku'];
        }
        return $productsData;
    }

    /**
     * @param $classPath
     * @param $countryCharCode
     * @return string
     */
    public function getFullPath($classPath, $countryCharCode)
    {
        return $this->deliveryPath . substr($classPath, 0, strrpos($classPath, '/')+1) . $countryCharCode . '/config.php';
    }
}