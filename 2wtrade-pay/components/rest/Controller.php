<?php
namespace app\components\rest;

use app\components\ComponentTrait;
use Yii;
use yii\rest\Controller as RestController;
use yii\web\Response;

/**
 * Class Controller
 * @package app\components\rest
 */
class Controller extends RestController
{
    use ComponentTrait;

    const RESPONSE_STATUS_SUCCESS = 'success';
    const RESPONSE_STATUS_FAIL = 'fail';
    const RESPONSE_STATUS_DUPLICATE = 'duplicate';

    /**
     * @var string Формат ответа
     */
    protected $responseFormat = Response::FORMAT_JSON;

    /**
     * @var array Данные для ответа
     */
    protected $responseData = [
        'status' => self::RESPONSE_STATUS_FAIL,
        'data' => [],
    ];

    /**
     * @inheritdoc
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (\Exception $e) {
            $this->addMessage($e->getMessage());
            $statusCode = isset($e->statusCode) ? $e->statusCode : 400;

            return $this->fail($statusCode);
        }
    }

    /**
     * @return array
     */
    protected function respond()
    {
        Yii::$app->response->format = $this->responseFormat;

        return $this->responseData;
    }

    /**
     * Успешный ответ
     */
    protected function success()
    {
        Yii::$app->response->statusCode = 200;
        $this->responseData['status'] = self::RESPONSE_STATUS_SUCCESS;

        return $this->respond();
    }

    /**
     * Ответ о том, что есть дубль
     */
    protected function duplicate()
    {
        Yii::$app->response->statusCode = 200;
        $this->responseData['status'] = self::RESPONSE_STATUS_DUPLICATE;

        return $this->respond();
    }

    /**
     * @param null $statusCode
     * @return array
     */
    protected function fail($statusCode = null, $exeption = null)
    {
        if (is_null($statusCode)) {
            $statusCode = 400;
        }

        Yii::$app->response->statusCode = $statusCode;
        $this->responseData['status'] = self::RESPONSE_STATUS_FAIL;

        if(!is_null($exeption)){
            $this->responseData['message'] = $exeption;
        }

        return $this->respond();
    }

    /**
     * @param $message
     */
    protected function addMessage($message)
    {
        if (!isset($this->responseData['data']['messages'])) {
            $this->responseData['data']['messages'] = [];
        }

        $this->responseData['data']['messages'][] = $message;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function addDataValue($key, $value)
    {
        if (!isset($this->responseData['data'][$key])) {
            $this->responseData['data'][$key] = [];
        }

        $this->responseData['data'][$key][] = $value;
    }

    /**
     * @param $value
     */
    protected function addData($value)
    {
        $this->responseData['data'][] = $value;
    }

    /**
     * @param \yii\base\Model $model
     * @param boolean $first
     */
    protected function addMessagesByModel($model, $first = false)
    {
        $errorsAttributes = $model->getErrors();

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $this->addMessage($error);

                if ($first) {
                    return;
                }
            }
        }
    }
}
