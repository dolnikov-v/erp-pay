<?php
namespace app\components\rest;

use app\models\api\ApiUser;
use app\models\api\ApiUserDelivery;
use app\models\api\ApiUserToken;
use app\models\Source;
use app\modules\delivery\models\Delivery;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class User
 * @package app\components\rest
 * @property ApiUserToken $token
 * @property ApiUser $identity
 * @property integer $id
 *
 * @property Source $source
 */
class User extends Component
{
    /** Время жизни в секундах */
    public static $lifetime = 3600;

    /** @var ApiUserToken */
    protected $token;

    /** @var ApiUser */
    protected $identity;

    /**
     * @return ApiUserToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param ApiUserToken $model
     */
    public function setToken($model)
    {
        $this->token = $model;
        $this->identity = $model->user;
        if ($this->token->lifetime > 0) {
            $this->prolongLifetimeToken();
        }
    }

    /**
     * @return ApiUser
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->identity->id;
    }


    /**
     * Продлить время жизни токена
     */
    public function prolongLifetimeToken()
    {
        $this->token->lifetime = time() + self::$lifetime;
        $this->token->save();
    }

    /**
     * @return \app\modules\delivery\models\Delivery[]
     * @throws NotFoundHttpException
     */
    public function getDeliveries()
    {
        $apiDeliveries = ApiUserDelivery::find()
            ->where(['user_id' => $this->getId()])
            ->all();

        if ($apiDeliveries) {
            $deliveries = Delivery::find()
                ->where(['IN', 'id', ArrayHelper::getColumn($apiDeliveries, 'delivery_id')])
                ->all();
        } else {
            throw new NotFoundHttpException('No deliveries of available services.');
        }

        return $deliveries;
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function getDeliveriesId()
    {
        $apiDeliveries = ApiUserDelivery::find()
            ->where(['user_id' => $this->getId()])
            ->all();

        if (!$apiDeliveries) {
            throw new NotFoundHttpException('No deliveries of available services.');
        }

        return ArrayHelper::getColumn($apiDeliveries, 'delivery_id');
    }

    /**
     * @param $country
     * @return Delivery
     * @throws NotFoundHttpException
     */
    public function getDelivery($country)
    {
        $delivery = ApiUserDelivery::find()
            ->joinWith('delivery')
            ->where(['user_id' => $this->getId()])
            ->andWhere([Delivery::tableName() . '.country_id' => $country->id])
            ->one();

        if (!$delivery) {
            throw new NotFoundHttpException('No deliveries of available services.');
        }

        return $delivery->delivery;
    }

    /**
     * @return \app\models\Source
     */
    public function getSource()
    {
        return $this->identity->source;
    }
}
