<?php

namespace app\components;

use app\components\db\mysql\Schema;
use app\modules\administration\models\CrontabTask;
use Yii;
use yii\db\Migration;
use app\components\db\mysql\ColumnSchemaBuilder;

/**
 * Class CustomMigration
 * @package app\components
 */
class CustomMigration extends Migration
{
    const CASCADE = 'CASCADE';
    const RESTRICT = 'RESTRICT';
    const SET_NULL = 'SET NULL';
    const NO_ACTION = 'NO ACTION';

    /**
     * @var null|string
     */
    protected $tableOptions = null;

    /**
     * @var string
     */
    protected $pkPrefix = 'PK';

    /**
     * @var string
     */
    protected $idxPrefix = 'IDX';

    /**
     * @var string
     */
    protected $uniPrefix = 'UNI';

    /**
     * @var string
     */
    protected $fkPrefix = 'FK';

    /**
     * @var \yii\rbac\DbManager
     */
    protected $authManager = null;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->db->enableSchemaCache = false;

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->authManager = Yii::$app->getAuthManager();
    }


    /**
     * Builds and executes a SQL statement for creating a new DB table.
     *
     * The columns in the new  table should be specified as name-definition pairs (e.g. 'name' => 'string'),
     * where name stands for a column name which will be properly quoted by the method, and definition
     * stands for the column type which can contain an abstract DB type.
     *
     * The [[QueryBuilder::getColumnType()]] method will be invoked to convert any abstract type into a physical one.
     *
     * If a column is specified with definition only (e.g. 'PRIMARY KEY (name, type)'), it will be directly
     * put into the generated SQL.
     *
     * Если не передан $options, то будут использованы умолчательные из $this->tableOptions. Чтобы профорсить пустые
     * опции надо передать пустую строку.
     *
     * @param string $table the name of the table to be created. The name will be properly quoted by the method.
     * @param array $columns the columns (name => definition) in the new table.
     * @param string $options additional SQL fragment that will be appended to the generated SQL.
     */
    public function createTable($table, $columns, $options = null)
    {
        if (is_null($options)) {
            $options = $this->tableOptions;
        }
        return parent::createTable($table, $columns, $options);
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string|array $columns
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        if (is_null($name)) {
            $name = $this->getPkName($table, $columns);
        }
        parent::addPrimaryKey($name, $table, $columns);
    }

    /**
     * @param $table
     * @param $columns
     * @return string
     */
    public function getPkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);
        return strtolower(sprintf(
            '%s_%s',
            $this->pkPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string|array $columns
     * @return string
     */
    public function includePrimaryKey($columns)
    {
        $columns = is_array($columns) ? implode(',', $columns) : $columns;
        return ('PRIMARY KEY (' . $columns . ')');
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string|array $columns
     * @param bool|false $unique
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        if (is_null($name)) {
            if ($unique) {
                $name = $this->getUniName($table, $columns);
            } else {
                $name = $this->getIdxName($table, $columns);
            }
        }

        parent::createIndex($name, $table, $columns, $unique);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getUniName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);
        return strtolower(sprintf(
            '%s_%s',
            $this->uniPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getIdxName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);
        return strtolower(sprintf(
            '%s_%s',
            $this->idxPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string|array $columns
     * @param array|string $refTable
     * @param string $refColumns
     * @param null $delete
     * @param null $update
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if (is_null($name)) {
            $name = $this->getFkName($table, $columns);
        }
        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @param string $name
     * @param string $table
     * @param array|string $columns
     */
    public function dropForeignKey($name, $table, $columns = null)
    {
        if (is_null($name)) {
            $name = $this->getFkName($table, $columns);
        }
        parent::dropForeignKey($name, $table);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getFkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);
        return strtolower(substr(sprintf(
            '%s_%s_%s',
            $this->fkPrefix,
            $table,
            $columns
        ), 0, 64));
    }

    /**
     * @param array $values
     * @return ColumnSchemaBuilder
     */
    public function enum($values)
    {
        $builder = new ColumnSchemaBuilder(Schema::TYPE_ENUM, null, Yii::$app->db, []);
        $builder->enum($values);

        return $builder;
    }

    /**
     * Добавление задания для крона
     * @param string $name
     * @param string $description
     * @param int $active
     */
    protected function insertCrontabTask(string $name, string $description, int $active = 1)
    {
        $crontabTask = CrontabTask::findOne(['name' => $name]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = $name;
            $crontabTask->active = $active;
            $crontabTask->description = $description;
            $crontabTask->save();
        }
    }

    /**
     * Удаление задания для крона
     * @param string $name
     */
    protected function removeCrontabTask(string $name)
    {
        $crontabTask = CrontabTask::findOne(['name' => $name]);
        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
