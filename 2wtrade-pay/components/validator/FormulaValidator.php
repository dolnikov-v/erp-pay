<?php

namespace app\components\validator;

use app\helpers\math\Formula;
use yii\validators\Validator;
use Yii;

/**
 * Class FormulaValidator
 * @package app\components\validator
 */
class FormulaValidator extends Validator
{
    public $existVariables;

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        if (!Formula::check($model->$attribute)) {
            $this->addError($model, $attribute, Yii::t('common', 'Поле «{attribute}» заполнено некорректно'));
        } elseif ($this->existVariables)  {
            if ($variables = array_diff(Formula::getVariables($model->$attribute), $this->existVariables)) {
                $message = Yii::t('common', 'Поле «{attribute}» содержет неопределенные переменные: {variables}', ['variables' => '«' . join('», «', $variables) . '»']);
                $this->addError($model, $attribute, $message);
            }
        }
    }
}

