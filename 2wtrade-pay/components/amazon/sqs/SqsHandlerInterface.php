<?php

namespace app\components\amazon\sqs;

/**
 * Interface SqsHandlerInterface
 * @package app\components\amazon\sqs
 */
interface SqsHandlerInterface
{
    /**
     * @param array $messages
     * @param array $receiptHandlers
     */
    public function handleMessages(array $messages, array &$receiptHandlers);
}