<?php
namespace app\components\grid;

use app\widgets\custom\Checkbox;
use yii\grid\DataColumn;

/**
 * Class CustomCheckboxColumn
 * @package app\components\grid
 */
class CustomCheckboxColumn extends DataColumn
{
    /**
     * @var string
     */
    public $label;

    /**
     * @var boolean
     */
    public $encodeLabel = false;

    /**
     * @var array
     */
    public $headerOptions = ['class' => 'th-custom-checkbox'];

    /**
     * @var array
     */
    public $contentOptions = ['class' => 'td-custom-checkbox'];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (is_null($this->label)) {
            $this->label = Checkbox::widget([
                'style' => 'checkbox-inline',
                'label' => true,
            ]);
        }
    }
}
