<?php
namespace app\components\grid;


use app\widgets\custom\DropdownActions;
use yii\grid\Column;
use yii\helpers\Url;

/**
 * Class ActionColumn
 * @package app\components\grid
 */
class ActionColumn extends Column
{
    /** @var array */
    public $headerOptions = ['class' => 'th-actions'];

    /** @var array */
    public $contentOptions = ['class' => 'td-actions'];

    /** @var bool Разделитель */
    public $divider = false;

    /** @var array */
    public $items = [];

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     * @throws \Exception
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $items = [];

        foreach ($this->items as $key => $item) {
            if (empty($item['can']) || (is_callable($item['can']) && call_user_func($item['can'], $model)) || (!is_callable($item['can']) && $item['can'])) {
                if (!isset($item['label'])) {
                    $items[$key] = '';
                } else {
                    $items[$key] = [
                        'label' => $item['label'],
                        'url' => $item['url']($model),
                        'attributes' => (isset($item['attributes'])) ? $item['attributes']($model) : [],
                        'style' => (isset($item['style'])) ? $item['style'] : '',
                    ];
                }
            }
        }

        return DropdownActions::widget([
            'links' => $items
        ]);
    }
}
