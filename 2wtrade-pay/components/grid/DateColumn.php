<?php
namespace app\components\grid;

use yii\grid\DataColumn;
use yii\helpers\Html;
use Yii;

/**
 * Class DateColumn
 * @package app\components\grid
 */
class DateColumn extends DataColumn
{
    /** @var bool */
    public $withRelativeTime = false;

    /** @var string */
    public $formatType = 'Datetime';

    /** @var array */
    public $headerOptions = ['class' => 'th-date text-center'];

    /** @var array */
    public $contentOptions = ['class' => 'text-center'];

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    public function renderDataCellContent($model, $key, $index)
    {
        $time = $this->getDataCellValue($model, $key, $index);

        if (is_null($time)) {
            return $time;
        } else {
            if(!in_array($this->formatType, ['Date', 'Datetime', 'FullTime', 'FullDatetime', 'RelativeTime', 'Time', 'Timestamp'])){
                $this->formatType = 'Datetime';
            }
            return Html::tag('div', Yii::$app->formatter->{"as{$this->formatType}"}($time)) .
                ($this->withRelativeTime ? Html::tag('div', Yii::$app->formatter->asDatetime($time), ['class' => 'font-size-12 text-muted']) : '');
        }
    }
}
