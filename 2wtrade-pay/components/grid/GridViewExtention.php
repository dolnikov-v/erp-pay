<?php
namespace app\components\grid;

use kartik\grid\GridView as KartikGridView;

class GridViewExtention extends KartikGridView
{
    public $positionPageSummary = 'bottom';

    /**
     * @inheritdoc
     */
    public function renderTableBody()
    {
        $content = parent::renderTableBody();
        if ($this->showPageSummary) {
            if ($this->positionPageSummary === 'top') {
                return $this->renderPageSummary() . $content;
            } else {
                return $content . $this->renderPageSummary();
            }
        }
        return $content;
    }
}