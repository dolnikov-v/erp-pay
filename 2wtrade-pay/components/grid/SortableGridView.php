<?php
namespace app\components\grid;

use app\components\assets\SortableGridAsset;
use himiklab\sortablegrid;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

/**
 * Class GridView
 * @package app\components\grid
 */
class SortableGridView extends sortablegrid\SortableGridView
{
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'table table-striped table-hover table-sortable'];

    /**
     * @var array
     */
    public $rowOptions = ['class' => 'tr-vertical-align-middle'];

    /** @var string|array Sort action */
    public $sortableAction = ['sort'];

    public function init()
    {
        parent::init();
        $this->sortableAction = Url::to($this->sortableAction);
    }

    public function run()
    {
        $this->registerWidget();
        parent::run();
    }

    protected function registerWidget()
    {
        $view = $this->getView();
        $view->registerJs("jQuery('#{$this->options['id']}').SortableGridView('{$this->sortableAction}');");
        SortableGridAsset::register($view);
    }

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $config['tableOptions'] = isset($config['tableOptions']) ? ArrayHelper::merge($this->tableOptions, $config['tableOptions']) : $this->tableOptions;
        $this->emptyText = Yii::t('common', 'Данные отсутствуют');

        parent::__construct($config);
    }
}