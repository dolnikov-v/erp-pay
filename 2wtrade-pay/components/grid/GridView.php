<?php
namespace app\components\grid;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class GridView
 * @package app\components\grid
 */
class GridView extends \yii\grid\GridView
{
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'table table-striped table-hover table-sortable'];

    /**
     * @var array
     */
    public $rowOptions = ['class' => 'tr-vertical-align-middle'];

    /**
     * По умолчанию оборачиваем в див
     * @var array
     */
    public $options = ['tag' => false];

    /**
     * @var string
     */
    public $layout = '{items}';

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $config['tableOptions'] = isset($config['tableOptions']) ? ArrayHelper::merge($this->tableOptions, $config['tableOptions']) : $this->tableOptions;
        $this->emptyText = Yii::t('common', 'Данные отсутствуют');

        parent::__construct($config);
    }
}
