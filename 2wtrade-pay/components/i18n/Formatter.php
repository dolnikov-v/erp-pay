<?php
namespace app\components\i18n;

use app\helpers\i18n\Formatter as FormatterHelper;
use app\models\Timezone;
use app\modules\i18n\models\Language;
use Yii;

/**
 * Class Formatter
 * @package app\components\i18n
 */
class Formatter extends \yii\i18n\Formatter
{
    public $locale = Language::DEFAULT_LOCALE;
    public $timeZone = Timezone::DEFAULT_TIMEZONE;

    public $dateFormat;
    public $dateFormatStrict;
    public $timeFormat;
    public $datetimeFormat;
    public $fullTimeFormat;
    public $monthFormat;
    public $monthFormatStrict;

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->dateFormat = FormatterHelper::getDateFormat($this->locale);
        $this->dateFormatStrict = FormatterHelper::getDateFormatStrict($this->locale);
        $this->timeFormat = FormatterHelper::getTimeFormat($this->locale);
        $this->datetimeFormat = FormatterHelper::getDatetimeFormat($this->locale);
        $this->fullTimeFormat = FormatterHelper::getFullTimeFormat($this->locale);
        $this->monthFormat = FormatterHelper::getMonthFormat($this->locale);
        $this->monthFormatStrict = FormatterHelper::getMonthFormatStrict($this->locale);
        $this->decimalSeparator = FormatterHelper::getDecimalSeparator($this->locale);
        $this->thousandSeparator = FormatterHelper::getThousandSeparator($this->locale);
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timeZone = $timezone;
    }

    /**
     * @param \DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asDate($value, $format = null)
    {
        return parent::asDate($value, ($format ? $format : $this->dateFormat));
    }

    /**
     * @param \DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asDateUTC($value, $format = null)
    {
        $zone = $this->timeZone;
        $this->setTimezone(Timezone::DEFAULT_TIMEZONE);
        $return = parent::asDate($value, ($format ? $format : $this->dateFormat));
        $this->setTimezone($zone);
        return $return;
    }

    /**
     * @param \DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asTime($value, $format = null)
    {
        return parent::asTime($value, ($format ? $format : $this->timeFormat));
    }

    /**
     * @param \DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asDatetime($value, $format = null)
    {
        return parent::asDatetime($value, ($format ? $format : $this->datetimeFormat));
    }

    /**
     * @param $value
     * @param null $format
     * @return string
     */
    public function asFullTime($value, $format = null)
    {
        return parent::asTime($value, ($format ? $format : $this->fullTimeFormat));
    }

    /**
     * @param $value
     * @return string
     */
    public function asFullDatetime($value)
    {
        return parent::asDate($value, $this->dateFormat) . ' ' . parent::asTime($value, $this->fullTimeFormat);
    }

    /**
     * @param \DateInterval|\DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asRelativeTime($value, $format = null)
    {
        return parent::asRelativeTime($value);
    }

    /**
     * @param \DateTime|int|string $value
     * @return bool|int|string
     */
    public function asTimestampLocal($value)
    {
        return self::asTimestamp($value, $this->timeZone);
    }

    /**
     * @param \DateTime|int|string $value
     * @param string $timeZone принимаем нужную тайм зону
     * @return bool|int|string
     */
    public function asTimestamp($value, $timeZone = null)
    {
        if (!$timeZone) {
            // если зона не принята то ставим дефалтную
            $timeZone = $this->defaultTimeZone;
        }
        $dateFormats = [
            $this->dateFormatStrict => 'd.m.Y',
            $this->timeFormat => 'H:i',
            $this->datetimeFormat => 'd.m.Y H:i',
            $this->fullTimeFormat => 'd.m.Y H:i:s',
            $this->monthFormatStrict => 'm.Y',
        ];

        if (is_string($value)) {
            foreach ($dateFormats as $format => $convertedFormat) {
                if (strncmp($format, 'php:', 4) === 0) {
                    $format = substr($format, 4);
                }
                if (($dateTime = \DateTime::createFromFormat($format, $value, new \DateTimeZone($timeZone))) !== false) {
                    $value = $dateTime;
                    break;
                }
            }
        }
        return parent::asTimestamp($value);
    }

    /**
     * @param \DateTime|int|string $value
     * @param null|string $format
     * @return string
     */
    public function asMonth($value, $format = null)
    {
        return parent::asDate($value, ($format ? $format : $this->monthFormat));
    }
}
