<?php
namespace app\components\rbac;

use yii\rbac\ManagerInterface;

/**
 * Class DbManager
 * @package app\components\rbac
 */
class DbManager extends \yii\rbac\DbManager implements ManagerInterface
{

}
