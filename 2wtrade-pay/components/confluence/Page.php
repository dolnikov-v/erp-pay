<?php
namespace app\components\confluence;

/**
 * Class Page
 * @package app\commands\components\confluence
 * @property array $ancestors
 * @property $space
 * @property $body
 * @property $id
 * @property $version
 * @property $restrictions
 */
class Page
{
    const URI = 'content';

    public
        $type = 'page',
        //$status = 'current',
        $title;

}