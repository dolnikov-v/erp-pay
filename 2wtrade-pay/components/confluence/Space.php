<?php
namespace app\components\confluence;

/**
 * Class Space
 * @package app\commands\components\confluence
 * @property $id
 * @property $key
 * @property $name
 * @property $description
 * @property $metadata
 */
class Space
{
    const URI = 'space';

}