<?php
namespace app\components\confluence;

use Yii;
use app\models\Country;
use yii\httpclient\Client;
use app\modules\salary\models\Office;
use app\modules\delivery\models\Delivery;


class Confluence
{
    const NOT_ACTIVE_COUNTRIES = [
        10, 15, 16, 17, 18, 19, 20, 21, 22, 35, 37, 38, 40, 42, 43, 45, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 69, 70, 71
    ];

    private $userName;
    private $password;
    private $spaceKey = 'DLT';
    private $space;
    public $host;

    public $url;

    /**
     * Confluence constructor.
     */
    public function __construct()
    {
        $this->userName = Yii::$app->params['confluence']['credentials']['username'];
        $this->password = Yii::$app->params['confluence']['credentials']['password'];
        $this->host = Yii::$app->params['confluence']['rest'];
        $this->space = (object)['key' => $this->spaceKey];
}

    public function createPage(Page $page)
    {
        $page->space = $this->space;

        $result = $this->postRequest(Page::URI, $page);
        if (!isset($result->id)) {
            throw new \Exception(print_r($result, true));
        }
        return $result;
}

    public function createPageWithRestrictions(Page $page, $ancestorPageId)
    {
        $page->space = $this->space;
        $page->restrictions = (object)[
            'read' => (object)[
            'operations' => 'read',
            'restrictions' => (object)[
                'group' => (object)[
                    'type' => 'group',
                    'name' => 'india_view',
                ]
            ]
        ]
        ];
        $ancestorPage = new Page();
        $ancestorPage->id = $ancestorPageId;
        $page->ancestors = [$ancestorPage];
        $result = $this->postRequest(Page::URI, $page);
        if (!isset($result->id)) {
            throw new \Exception(print_r($result, true));
        }
        return $result;
    }

    /**
     * @param $ancestorId
     * @return array
     */
    public function createCountriesPage($ancestorId)
    {
        $countries = Country::find()->where(['NOT IN','id', self::NOT_ACTIVE_COUNTRIES ])
            ->active()
            ->own()
            ->customCollection('id', 'name_en');
        $countryPageIds = [];
        foreach ($countries as $id => $country) {
            print_r($country);
            echo PHP_EOL;
            /**
             * @var Page $page
             */
            $page = new Page();
            $page->space = $this->space;
            $page->title = $country;
            $page->ancestors = [$ancestorId];
            $page->body = (object)[
                'storage' => (object)[
                    'value' => "<p>All information relating to {$country}.</p>",
                    'representation' => 'storage'
                ]
            ];
            $countryPageIds[$id] = $this->createPage($page);
        }
        return $countryPageIds;
}

    /**
     * @param $countryPageIds
     * @return array
     * @internal param $ancestorId
     */
    public function createDeliveriesPage($countryPageIds)
    {

        $deliveryPageIds = [];
        foreach ($countryPageIds as $countryId => $country) {

            $page = new Page();
            $page->space = $this->space;
            $page->title = 'Courier Services' . ' ' . $country->title;
            $page->ancestors = [$country];
            $page->body = (object)[
                'storage' => (object)[
                    'value' => "<p>All information deliveries relating to $country->title .</p>",
                    'representation' => 'storage'
                ]
            ];
            $mainDeliveryPage = $this->createPage($page);

            $deliveries = Delivery::find()->byCountryId($countryId)->all();
            foreach ($deliveries as $delivery) {
                $deliveryName = str_ireplace('&', '', $delivery->name);
                print_r($deliveryName);
                echo PHP_EOL;
                $page = new Page();
                $page->space = $this->space;
                $page->title = $deliveryName . ' ' . $country->title;
                $page->ancestors = [$mainDeliveryPage];
                $page->body = (object)[
                    'storage' => (object)[
                        'value' => "<p>All information relating to $deliveryName $country->title.</p>",
                        'representation' => 'storage'
                    ]
                ];
                $deliveryPageIds[$delivery->id] = $this->createPage($page);
                //print_r($deliveryPageIds[$delivery->id]->title??$deliveryPageIds[$delivery->id]);
                //echo PHP_EOL;
            }

        }
        return $deliveryPageIds;
    }

    /**
     * @param $countryPageIds
     * @return array
     * @internal param $ancestorId
     */
    public function createCallCentersPage($countryPageIds)
    {
        $ccOfficePageIds = [];
        foreach ($countryPageIds as $countryId => $country) {

            $page = new Page();
            $page->space = $this->space;
            $page->title = 'Call center offices relating to country' . ' ' . $country->title;
            $page->ancestors = [$country];
            $page->body = (object)[
                'storage' => (object)[
                    'value' => "<p>All information deliveries relating to $country->title .</p>",
                    'representation' => 'storage'
                ]
            ];
            $mainCcPage = $this->createPage($page);

            $callCenterOffices = Office::find()->byCountryId($countryId)->all();
            foreach ($callCenterOffices as $office) {
                print_r($office->name);
                echo PHP_EOL;
                $page = new Page();
                $page->space = $this->space;
                $page->title = str_ireplace('&', '',$office->name);
                $page->ancestors = [$mainCcPage];
                $page->body = (object)[
                    'storage' => (object)[
                        'value' => "<p>All information relating to {$office->name}.</p>",
                        'representation' => 'storage'
                    ]
                ];
                $ccOfficePageIds[$office->id] = $this->createPage($page);
            }

        }
        return $ccOfficePageIds;
    }


   /* public function deleteAncestorPage($id)
    {
        //$space = $this->createThrashSpace();
        //echo $space->key . PHP_EOL;
        //$move = $this->movePageToSpace($id, "DLT");
        //echo 'moved' . PHP_EOL;

        return $this->deleteSpace("DLT");
    }*/

    /**
     * @return Space
     */
    public function createThrashSpace()
    {
        $space = new Space();
        $space->key = 'DLT';
        $space->name = 'Temp space to delete';

        return $space = $this->postRequest(Space::URI, $space);
    }

    /*public function movePageToSpace($pageId, $spaceKey)
    {
        $page = new Page();
        $page->space = (object)['key' => $spaceKey];
        $page->version = (object)['number' => 2];
        //$page->title = 'test3';
//        return $this->updateRequest(Page::URI . '/' . $pageId, $page);
        return $this->updateRequest("space/SPC/page/59637771", $page);
    }*/

    /*private function deleteSpace($spaceKey)
    {
        return $this->deleteRequest(Space::URI . '/' . $spaceKey);
    }*/

    private function postRequest($uri, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->host . $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "authorization: Basic " . base64_encode("$this->userName:$this->password"),
                "content-type: application/json",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $result = json_decode($response);
        if ($err /*|| ($code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) > 201*/) {
            throw new \Exception($err??$code??null);
        }
        curl_close($curl);
        return $result;
    }

    public function updateRequest($uri, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->host . $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "authorization: Basic " . base64_encode("$this->userName:$this->password"),
                "content-type: application/json",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $result = json_decode($response);
        curl_close($curl);
        if ($err /*|| $code = curl_getinfo($curl, CURLINFO_HTTP_CODE) > 201*/) {
            throw new \Exception($err??$code??null);
        }
        return $result;
    }

    public function deleteRequest($uri)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->host . $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            //CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "authorization: Basic " . base64_encode("$this->userName:$this->password"),
                "content-type: application/json",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $result = json_decode($response);
        curl_close($curl);
        if ($err /*|| $code = curl_getinfo($curl, CURLINFO_HTTP_CODE) > 201*/) {
            throw new \Exception($err??$code??null);
        }
        return $result;
    }

    /**
     * @param $groupName
     * @return bool
     * @throws \Exception
     */
    public function addGroup($groupName)
    {
        $post = [
            "jsonrpc" => "2.0",
            "method" => "addGroup",
            "params" => [$groupName],
            "id" => 12345,
        ];
        $userName = Yii::$app->params['jira']['credentials']['username'];
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(Yii::$app->params['confluence']['jsonRpcUrl'])
            ->setFormat('json')
            ->addHeaders(['Authorization' => 'Basic '. base64_encode("$userName:$this->password")])
            ->setData($post)
            ->send();

        if(!$response->isOk && ($result = json_decode($response->content)) && isset($result->error) && !$result->result){
            throw new \Exception($response->content);
        }
        print_r($response->content);
        echo PHP_EOL;
        return $response->content;
    }

    /**
     *
     */
    public function addGroupsForCountries()
    {
        $countries = Country::find()->where(['NOT IN','id', self::NOT_ACTIVE_COUNTRIES ])
            ->active()
            ->own()
            ->customCollection('id', 'name_en');
        foreach ($countries as $id => $country) {
            $this->addGroup("$country" . "_read");
            $this->addGroup("$country" . "_edit");
        }
    }
}