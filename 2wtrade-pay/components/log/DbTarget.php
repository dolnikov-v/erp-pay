<?php
namespace app\components\log;

use Yii;
use yii\log\Target;
use app\models\UserActionLogQuery;

/**
 * Class DbTarget
 * @package app\components\log
 */
class DbTarget extends Target
{
    private static $ignoredQueries = [
        'INSERT INTO user_action_log',
        'UPDATE user_action_log',
        'UPDATE user SET lastact_at',
    ];

    /**
     * Экспорт данных
     */
    public function export()
    {
        if (Yii::$app->userActionLog->isStarted()) {
            $log = Yii::$app->userActionLog->getLog();
            $log->http_status = Yii::$app->response->statusCode;
            $log->save();

            foreach ($this->messages as $message) {
                $query = $message[0];

                if (is_string($query)) {
                    $save = true;

                    foreach (self::$ignoredQueries as $sql) {
                        if (mb_strpos($query, $sql) === 0) {
                            $save = false;
                            break;
                        }
                    }

                    if ($save && (mb_strpos($query, 'UPDATE ') === 0 || mb_strpos($query, 'INSERT ') === 0 || mb_strpos($query, 'DELETE ') === 0)) {
                        $actionLogQuery = new UserActionLogQuery();
                        $actionLogQuery->action_log_id = $log->id;
                        $actionLogQuery->query = $query;

                        $actionLogQuery->save();
                    }
                }
            }
        }
    }
}
