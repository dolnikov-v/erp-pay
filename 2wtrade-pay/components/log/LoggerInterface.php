<?php

namespace app\components\log;

/**
 * Interface LoggerInterface
 * @package app\components\log
 */
interface LoggerInterface
{
    const TYPE_INFO = 'info';
    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';

    /**
     * @param string $message
     * @param array $tags
     * @param string $type
     */
    public function log(string $message, array $tags = [], string $type = self::TYPE_INFO);

    /**
     * Установление тэгов по умолчанию для всех логов
     * @param array $tags
     */
    public function setTags(array $tags);
}