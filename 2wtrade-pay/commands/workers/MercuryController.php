<?php

namespace app\commands\workers;

use app\components\console\AmazonWorkerController;
use app\models\Currency;
use app\models\Product;
use app\modules\delivery\models\Delivery;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListBarcode;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct;

/**
 * Class MercuryController
 * @package app\commands\workers
 */
class MercuryController extends AmazonWorkerController
{
    /**
     * Получение складских остатков из очереди меркурия
     */
    public function actionGetGoodsAccounting()
    {
        $this->getMessagesFromQueue('goodsAccounting', [$this, 'saveGoodsAccountingData'], 'goodsAccountingErrors', 60);
    }

    /**
     * Получение платежек от КС из очереди меркурия
     */
    public function actionGetDeliveryPayments()
    {
        $this->getMessagesFromQueue('deliveryPayment', [$this, 'saveDeliveryPaymentData'], 'deliveryPaymentErrors', 60);
    }

    /**
     * Получение штрих кодов из очереди меркурия
     */
    public function actionGetBarcodeResponse()
    {
        $this->getMessagesFromQueue('barcodeResponse', [$this, 'saveBarcodeResponse'], 'barcodeResponseErrors', 60);
    }

    /**
     * warehouse_id - наш идешник в пее
     * type - приход или списание (receipt|shipment)
     * good_id - наш идешник товара в пее
     * amount - количество товаров
     * doc_id - текстовое поле с номером акта оприходования или поступления
     *
     * @param $message
     * @return mixed
     * @throws \yii\db\Exception
     */
    protected function saveGoodsAccountingData($message)
    {
        $response['status'] = true;

        if (!isset($message['warehouse_id'])) {
            $response['message'] = 'no parameter warehouse_id';
            $response['status'] = false;
            return $response;
        }

        if ($message['type'] != 'receipt' && $message['type'] != 'shipment') {
            $response['message'] = 'incorrect type = ' . $message['type'];
            $response['status'] = false;
            return $response;
        }

        if (!isset($message['amount'])) {
            $response['message'] = 'no parameter amount';
            $response['status'] = false;
            return $response;
        }

        if (!isset($message['type'])) {
            $response['message'] = 'no parameter type';
            $response['status'] = false;
            return $response;
        }

        if (!isset($message['good_id'])) {
            $response['message'] = 'no parameter good_id';
            $response['status'] = false;
            return $response;
        }

        $storage = Storage::findOne($message['warehouse_id']);
        if (!$storage) {
            $response['message'] = 'warehouse not found = ' . $message['warehouse_id'];
            $response['status'] = false;
            return $response;
        }

        $product = Product::findOne($message['good_id']);
        if (!$product) {
            $response['message'] = 'product not found ' . $message['good_id'];
            $response['status'] = false;
            return $response;
        }

        $storageDocument = new StorageDocument();
        if ($message['type'] == 'receipt') {
            $storageDocument->type = StorageDocument::TYPE_CORRECTION_POSITIVE;
        }
        if ($message['type'] == 'shipment') {
            $storageDocument->type = StorageDocument::TYPE_CORRECTION_NEGATIVE;
        }
        $storageDocument->quantity = $message['amount'];
        $storageDocument->product_id = $product->id;
        $storageDocument->storage_id_to = $storage->id;
        $storageDocument->comment = $message['doc_id'] ?? '';

        $transaction = StorageProduct::getDb()->beginTransaction();
        if ($storageDocument->save()) {
            try {
                StorageProduct::addProductOnStorage($storageDocument->product_id, $storageDocument->storage_id_to, $storageDocument->quantity);
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response['status'] = false;
            }
        } else {
            $transaction->rollBack();
            $response['message'] = $storageDocument->getFirstErrorAsString();
            $response['status'] = false;
        }

        return $response;
    }


    /**
     * @param array $message
     *
     *
     * {
     * "document_type":"string",
     * "data":{
     *   "delivery_id":"integer",
     *   "paid_at":"integer\/timestamp",
     *   "receiving_bank":"string",
     *   "payer":"string",
     *   "bank_recipient":"string",
     *   "remittance_information":"string",
     *   "income":"double",
     *   "currency":"string",
     *   "currency_rate":"double"}
     * }
     *
     * @return array
     */
    protected function saveDeliveryPaymentData($message)
    {

        $response['status'] = true;

        if (!isset($message['data'])) {
            $response['message'] = 'no parameter data';
            $response['status'] = false;
            return $response;
        }
        $data = $message['data'];

        $properties = [
            'delivery_id',
            'paid_at',
            'receiving_bank',
            'payer',
            'bank_recipient',
            'remittance_information',
            'income',
            'currency',
            'currency_rate'
        ];

        foreach ($properties as $property) {
            if (!isset($data[$property])) {
                $response['message'] = 'no parameter in data - '. $property;
                $response['status'] = false;
                return $response;
            }
        }

        $delivery = Delivery::findOne($data['delivery_id']);
        if (!$delivery) {
            $response['message'] = 'delivery not found = ' . $data['delivery_id'];
            $response['status'] = false;
            return $response;
        }

        $deliveryPayment = PaymentOrder::find()
            ->where([
                'delivery_id' => $delivery->id,
                'paid_at' => $data['paid_at']
            ])
            ->one();
        if (!$deliveryPayment) {
            $deliveryPayment = new PaymentOrder();
        }

        $currency = Currency::find()
            ->orWhere([
                'id' => $data['currency'],
                'name' => $data['currency'],
                'num_code' => $data['currency'],
                'char_code' => $data['currency']
            ])
            ->one();
        if (!$currency) {
            $response['message'] = 'currency not found = ' . $data['currency'];
            $response['status'] = false;
            return $response;
        }

        $deliveryPayment->delivery_id = $delivery->id;
        $deliveryPayment->paid_at = $data['paid_at'];
        $deliveryPayment->receiving_bank = $data['receiving_bank'];
        $deliveryPayment->payer = $data['payer'];
        $deliveryPayment->bank_recipient = $data['bank_recipient'];
        $deliveryPayment->name = $data['remittance_information'];
        $deliveryPayment->sum = $data['income'];
        $deliveryPayment->currency_id = $currency->id;
        $deliveryPayment->source = PaymentOrder::SOURCE_MERCURY;

        if (!$deliveryPayment->save()) {
            $response['message'] = $deliveryPayment->getFirstErrorAsString();
            $response['status'] = false;
        }

        return $response;
    }


    /**
     * @param array $message
     *
     * {"list_id": 2, "order_id": 22231, "barcode": '3123123123', "image": base64}
     *
     * @return array
     */
    protected function saveBarcodeResponse($message)
    {
        $response['status'] = true;

        $properties = [
            'list_id',
            'order_id',
            'barcode',
            'image',
        ];

        foreach ($properties as $property) {
            if (!isset($message[$property])) {
                $response['message'] = 'no parameter in data - '. $property;
                $response['status'] = false;
                return $response;
            }
        }

        $orderLogisticList = OrderLogisticList::findOne($message['list_id']);
        if (!$orderLogisticList) {
            $response['message'] = 'orderLogisticList not found list_id = ' . $message['list_id'];
            $response['status'] = false;
            return $response;
        }

        $orderLogisticListBarcode = OrderLogisticListBarcode::findOne([
            'list_id' => $message['list_id'],
            'order_id' => $message['order_id']
        ]);

        if (!$orderLogisticListBarcode) {
            $response['message'] = 'orderLogisticListBarcode not found list_id = ' . $message['list_id'] . ' order_id = ' . $message['order_id'];
            $response['status'] = false;
            return $response;
        }

        $orderLogisticListBarcode->barcode = $message['barcode'];

        if ($fileName = $orderLogisticListBarcode->saveFile($message['image'])) {
            $orderLogisticListBarcode->barcode_filename = $fileName;
            if (!$orderLogisticListBarcode->save()) {
                $response['message'] = $orderLogisticListBarcode->getFirstErrorAsString();
                $response['status'] = false;
            }
        }
        else {
            $response['message'] = 'File save error ' . $orderLogisticListBarcode->order_id;
            $response['status'] = false;
        }

        $orderLogisticList->checkAllBarcodesReady();

        return $response;
    }
}