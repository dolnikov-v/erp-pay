<?php

namespace app\commands\workers;

use app\components\console\WorkerController;
use app\modules\callcenter\components\api\Lead;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\logger\components\log\Logger;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class OldCallCenter
 * @package app\commands\workers
 */
class OldCallCenterController extends WorkerController
{
    /**
     * @param null $cc_id - Ид КЦ
     * @param null $queue - УРЛ очереди амазона
     * @param null $offset_from - смещение относительно текущего времени в днях
     * @param null $offset_to - смещение относительно текущего времени в днях
     * @param int $last_update_offset - последнее обновление более n минут назад
     * @param int $batch_size - количество заказов на один запрос
     * @param int $lifetime - время жизни воркера в  секундах
     * @param int $timeout - время таймаута при отсутствии данных в секундах
     */
    public function actionSendRequestsToUpdateQueue($cc_id = null, $queue = null, $offset_from = null, $offset_to = null, $last_update_offset = 5, $batch_size = 100, $limit = 1000, $lifetime = 86400, $timeout = 30)
    {
        Yii::$app->db->enableSlaves = false;

        set_time_limit(0);
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $launchTime = time();
            $client = $this->getSqsClient();
            $queueUrl = (is_string($queue) && !empty($queue)) ? $queue : $client->getSqsQueueUrl('amazonOldCallCenterUpdateQueueUrl');

            $offset_from = intval($offset_from);
            $offset_to = intval($offset_to);
            $last_update_offset = intval($last_update_offset);
            $batch_size = intval($batch_size);
            $lifetime = intval($lifetime);
            $timeout = intval($timeout);

            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {
                if ($callCenters = Lead::getCallCentersToGetStatus(null, $offset_from, $offset_to, $last_update_offset)) {
                    $iterationStartTime = time();
                    $urls = [];
                    foreach ($callCenters as $callCenter) {
                        if (is_numeric($cc_id) && !empty($cc_id) && $callCenter->id != $cc_id) {
                            continue;
                        }

                        // Блокируем обновление статусов для одного КЦ
                        if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                            mkdir(Yii::getAlias('@runtime') . '/blocks/');
                        }

                        $file_lock = "/blocks/call_center_get_statuses_{$callCenter->id}_{$callCenter->country->char_code}_{$offset_from}_{$offset_to}.lock";

                        $file = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');

                        if (!flock($file, LOCK_EX | LOCK_NB)) {
                            fclose($file);
                            continue;
                        }

                        $queueTypes = Lead::getCallCenterTypesToGetStatus($callCenter, $iterationStartTime, null, $offset_from, $offset_to, $last_update_offset);

                        $minCronLaunchedAt = null;
                        foreach ($queueTypes[$callCenter->id] as $queueType) {
                            if ($requests = Lead::getPartRequestsToGetStatus($callCenter, $queueType, $iterationStartTime, 0, $limit, null, $offset_from, $offset_to, $last_update_offset, $minCronLaunchedAt)) {
                                $counter = 0;
                                $minCronLaunchedAt = max(ArrayHelper::getColumn($requests, 'cron_launched_at'));
                                if (!$minCronLaunchedAt) {
                                    $minCronLaunchedAt = 1;
                                }
                                while ($counter < count($requests)) {
                                    $batch = array_slice($requests, $counter, $batch_size);
                                    $counter += $batch_size;
                                    $urls[] = ['url' => Lead::prepareUrlToGetStatus($callCenter, $batch, $queueType), 'call_center_id' => $callCenter->id];
                                    CallCenterRequest::updateAll(['cron_launched_at' => time()], ['id' => ArrayHelper::getColumn($batch, 'id')]);
                                }
                            }
                        }
                        fclose($file);
                    }

                    $counter = 0;
                    $batchSize = 10;
                    shuffle($urls);
                    while ($counter < count($urls)) {
                        $batch = array_slice($urls, $counter, $batchSize);
                        $counter += $batchSize;

                        $messages = [];
                        foreach ($batch as $key => $value) {
                            $messages[] = [
                                'Id' => $key,
                                'MessageBody' => json_encode($value, JSON_UNESCAPED_UNICODE),
                            ];
                        }

                        $data = [
                            'QueueUrl' => $queueUrl,
                            'Entries' => $messages,
                        ];

                        try {
                            $result = $client->sendMessageBatch($data);
                            // Если по какой-то причине не удалось отправить в очередь
                            $failedRequests = $result->get('Failed');
                            if (is_array($failedRequests)) {
                                foreach ($failedRequests as $request) {
                                    $cronLog("Error while sending a message to update queue: {$request['Message']}", ['url' => json_encode($batch[$request['Id']], JSON_UNESCAPED_UNICODE), 'error' => $request['Message'], 'type' => 'queue']);
                                }
                            }
                        } catch (\Throwable $e) {
                            $cronLog("Exception while sending url to update queue: {$e->getMessage()}", ['exception' => $e->getMessage(), 'type' => 'sender']);
                        }
                    }

                    sleep(rand(1, 3));
                } else {
                    sleep($timeout);
                }
            }
        } catch (\Throwable $e) {
            $cronLog("Exception while worker worked: {$e->getMessage()}", ['exception' => $e->getMessage(), 'type' => 'worker']);
        }
    }

    /**
     * @param null $queue - УРЛ очереди
     * @param int $lifetime - Время жизни воркера в секундах
     * @param int $timeout - Время таймаута в случае пустого ответа
     */
    public function actionGetRequestsFromUpdateQueue($queue = null, $lifetime = 86400, $timeout = 60)
    {
        Yii::$app->db->enableSlaves = false;
        set_time_limit(0);
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);
        try {
            $launchTime = time();
            $client = $this->getSqsClient();
            $queueUrl = (is_string($queue) && !empty($queue)) ? $queue : $client->getSqsQueueUrl('amazonOldCallCenterUpdateQueueUrl');
            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {
                $response = $client->receiveMessage([
                    'MaxNumberOfMessages' => 10,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $queueUrl,
                ]);
                $messages = $response->get('Messages');
                if (count($messages) == 0) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    sleep($timeout);
                    continue;
                }

                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

                $urls = [];
                foreach ($messageBodies as $messageId => $messageBody) {
                    $urls[$messageId] = json_decode($messageBody, true);
                }

                $errors = Lead::getStatusesByUrls($urls);
                foreach ($errors as $error) {
                    $message = isset($error['exception']) ? "Exception while get statuses from call center: {$error['exception']}" : "Errors while get statuses from call center: {$error['errors']}";
                    $cronLog($message, array_merge($error, ['type' => 'updater']));
                }

                if (!empty($handlers)) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $queueUrl,
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    if (is_array($failedRequests)) {
                        foreach ($failedRequests as $request) {
                            $cronLog("Error while deleting a message from queue: {$request['Message']}", ['messageBody' => $messageBodies[$request['Id']], 'error' => $request['Message']]);
                        }
                    }
                }
            }
        } catch (\Throwable $e) {
            $cronLog("Exception while worker worked: {$e->getMessage()}", ['exception' => $e->getMessage(), 'type' => 'worker']);
        }
    }
}