<?php

namespace app\commands\workers;


use app\components\console\WorkerController;
use app\modules\logger\components\log\Logger;
use app\modules\report\models\Invoice;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class InvoiceController
 * @package app\commands\workers
 */
class InvoiceController extends WorkerController
{
    /**
     * @param null|string $queue_url
     * @param int $timeout
     */
    public function actionClose($queue_url = null, $timeout = 10)
    {
        set_time_limit(0);

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $client = $this->getSqsClient();
            $queueUrl = $queueUrl = (is_string($queue_url) && !empty($queue_url)) ? $queue_url : $client->getSqsQueueUrl('close_invoice');

            $response = $client->receiveMessage([
                'MaxNumberOfMessages' => 10,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $queueUrl,
            ]);
            $messages = $response->get('Messages');
            if (count($messages) == 0) {
                // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                sleep($timeout);
                return;
            }

            $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
            $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

            $requests = [];

            foreach ($messageBodies as $messageId => $messageBody) {
                $requests[$messageId] = json_decode($messageBody, true);
            }

            /**
             * @var Invoice[] $invoices
             */
            $invoices = Invoice::find()
                ->where(['id' => ArrayHelper::getColumn($requests, 'invoice_id')])
                ->indexBy('id')
                ->all();

            foreach ($requests as $messageId => $request) {
                if (!isset($invoices[$request['invoice_id']]) || $invoices[$request['invoice_id']]->status != Invoice::STATUS_CLOSING) {
                    continue;
                }
                try {
                    $invoices[$request['invoice_id']]->close($request['data']);
                } catch (\Throwable $e) {
                    $cronLog($e->getMessage(), [
                        'invoice_id' => $request['invoice_id'],
                        'request' => json_encode($request, JSON_UNESCAPED_UNICODE)
                    ]);
                    $this->log($e->getMessage());
                    unset($handlers[$messageId]);
                }
            }

            if (!empty($handlers)) {
                $deletingEntries = [];
                foreach ($handlers as $messageId => $handler) {
                    $deletingEntries[] = [
                        'Id' => $messageId,
                        'ReceiptHandle' => $handler,
                    ];
                }

                $result = $client->deleteMessageBatch([
                    'QueueUrl' => $queueUrl,
                    'Entries' => $deletingEntries,
                ]);

                $failedRequests = $result->get('Failed');
                if($failedRequests) {
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }
            }
        } catch (\Throwable $e) {
            $cronLog("Error: " . $e->getMessage());
            $this->log($e->getMessage());
        }
    }
}