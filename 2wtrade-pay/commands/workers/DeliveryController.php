<?php

namespace app\commands\workers;

use app\components\console\WorkerController;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\models\DeliveryApiResponse;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\logger\components\log\Logger;
use app\modules\order\models\OrderStatus;
use app\modules\salary\models\Penalty as CallCenterPenalty;
use app\modules\salary\models\PenaltyType as CallCenterPenaltyType;
use app\modules\salary\models\search\PersonSearch as CallCenterPersonSearch;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Class DeliveryController
 * @package app\commands\workers
 */
class DeliveryController extends WorkerController
{


    /**
     * Получение статусов заказов из очереди амазона
     * @param int $lifetime
     * @param null $queueUrl
     * @param integer $timeout
     */
    public function actionGetOrdersFromUpdateQueue($queueUrl = null, $lifetime = 86400, $timeout = 120)
    {
        $client = $this->getSqsClient();

        /**
         * @var Transmitter[] $transmitters
         */
        $transmitters = [];

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $queueUrl = (is_string($queueUrl) && !empty($queueUrl)) ? $queueUrl : $client->getSqsQueueUrl('amazonCheckTrackingQueueUrl');

            $launchTime = time();
            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {
                $response = $client->receiveMessage([
                    'MaxNumberOfMessages' => 10,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $queueUrl,
                ]);
                $messages = $response->get('Messages');
                if (count($messages) == 0) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    sleep($timeout);
                }
                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

                /**
                 * Атрибуты, по которым проверяется изменение информации с момента последнего запроса статуса
                 * Учитываем, что атрибуты АПИ класса с пустыми значениями к проверке не допускаются
                 * (атрибут из очереди) => (атрибут из апи класса)
                 */
                $attributesToCheckingForChange = [
                    'status_id' => 'status',
                    'returned_at' => 'date_returned',
                    'approved_at' => 'date_approved',
                    'accepted_at' => 'date_accepted',
                    'partner_name' => 'delivery_partner_name',
                    'finance_tracking' => 'finance_tracking',
                    'unshipping_reason' => 'unshipping_reason'
                ];

                $responses = [];
                foreach ($messageBodies as $messageId => $messageBody) {
                    $message = json_decode($messageBody, true);
                    try {
                        if (!isset($message['api_class']) || empty($message['api_class'])) {
                            throw new \Exception('Path to ApiClass is not set.');
                        }
                        if (!isset($transmitters[$message['api_class']])) {
                            $transmitters[$message['api_class']] = new Transmitter(null, $message['api_class']);
                        }
                        $transmitter = $transmitters[$message['api_class']];

                        $orders = [];
                        $orderResponses = [];
                        if (isset($message['orders'])) {
                            // Должен вернуться массив ответов, в которых обязательно должно быть поле order_id
                            $response = $transmitter->batchGetOrderInfoForArray($message['orders']);
                            if (isset($response['status']) && $response['status'] == DeliveryApiResponse::STATUS_SUCCESS) {
                                $orders = ArrayHelper::index($message['orders'], 'order_id');
                                $orderResponses = $response['data'];
                            } else {
                                throw new \Exception(isset($response['message']) ? $response['message'] : 'Query to delivery service was ended with error!');
                            }
                        } else {
                            $response = $transmitter->getOrderInfoForArray($message);
                            if ($response['status'] == DeliveryApiResponse::STATUS_SUCCESS) {
                                $orders[$message['order_id']] = $message;
                                $response['data']['order_id'] = $message['order_id'];
                                $orderResponses[] = $response['data'];
                            } else {
                                throw new \Exception(isset($response['message']) ? $response['message'] : 'Query to delivery service was ended with error!');
                            }
                        }

                        foreach ($orderResponses as $orderResponse) {
                            $wasChanged = false;

                            foreach ($attributesToCheckingForChange as $attribute => $mapAttribute) {
                                if ($orders[$orderResponse['order_id']] && array_key_exists($attribute, $orders[$orderResponse['order_id']]) && isset($orderResponse[$mapAttribute]) && !empty($orderResponse[$mapAttribute]) && $orderResponse[$mapAttribute] != $orders[$orderResponse['order_id']][$attribute]) {
                                    $wasChanged = true;
                                    break;
                                }
                            }
                            if ($wasChanged) {
                                $responses[] = $orderResponse;
                            }
                        }

                    } catch (\Throwable $e) {
                        $cronLog('Error when getting data from courier: ' . $e->getMessage() . PHP_EOL . print_r($e->getTrace(), true), [
                            'api_class' => isset($message['api_class']) ? $message['api_class'] : '',
                            'message' => $messageBody,
                            'type' => 1,
                            'order_id' => $message['order_id'] ?? '',
                            'tracking' => $message['tracking'] ?? '',
                        ]);
                    }

                }

                $counter = 0;
                $batchSize = 500;
                while ($counter < count($responses)) {
                    $batch = array_slice($responses, $counter, $batchSize);
                    $counter += $batchSize;
                    $orderIds = ArrayHelper::getColumn($batch, 'order_id');

                    $requests = $requests = DeliveryRequest::find()
                        ->with(['order'])
                        ->where([
                            'status' => [DeliveryRequest::STATUS_IN_LIST, DeliveryRequest::STATUS_IN_PROGRESS],
                            'order_id' => $orderIds
                        ])
                        ->all();
                    /**
                     * @var DeliveryRequest[] $requests
                     */
                    $requests = ArrayHelper::index($requests, 'order_id');

                    foreach ($batch as $response) {
                        if (!isset($requests[$response['order_id']])) {
                            continue;
                        }
                        $request = $requests[$response['order_id']];
                        $order = $request->order;
                        $order->route = Yii::$app->controller->route;
                        $status = $response['status'];

                        $lastUpdateResponse = null;
                        if (isset($response['last_update_info'])) {
                            $lastUpdateResponse = $request->addUpdateResponse($response['last_update_info']);
                        }

                        $mapDates = [
                            'returned_at' => 'date_returned',
                            'approved_at' => 'date_approved',
                            'accepted_at' => 'date_accepted',
                            'paid_at' => 'date_payment',
                            'finance_tracking' => 'finance_tracking',
                            'unshipping_reason' => 'unshipping_reason',
                        ];

                        foreach ($mapDates as $attribute => $mapAttribute) {
                            if (empty($request->$attribute) && isset($response[$mapAttribute])) {
                                $request->$attribute = $response[$mapAttribute];
                            }
                        }

                        if (isset($response['api_error'])) {
                            $request->api_error = $response['api_error'];
                        }

                        $transaction = Yii::$app->db->beginTransaction();
                        try {

                            if (isset($response['call_center_penalty_type']) && !empty($response['call_center_penalty_type'])) {
                                if (($trigger = CallCenterPenaltyType::getTriggerByString($response['call_center_penalty_type']))
                                    && ($penaltyType = CallCenterPenaltyType::find()->byTrigger($trigger)->one())
                                    && $penaltyType->active
                                    && ($person = CallCenterPersonSearch::findPersonByOrderId($request->order_id))
                                    && !CallCenterPenalty::find()->where([
                                        'person_id' => $person->id,
                                        'order_id' => $request->order_id,
                                        'penalty_type_id' => $penaltyType->id
                                    ])->exists()
                                ) {
                                    $penalty = new CallCenterPenalty([
                                        'order_id' => $request->order_id,
                                        'person_id' => $person->id,
                                        'penalty_type_id' => $penaltyType->id,
                                        'penalty_sum_usd' => $penaltyType->sum,
                                        'penalty_percent' => $penaltyType->percent,
                                    ]);
                                    if (!$penalty->save()) {
                                        throw new \Exception($penalty->getFirstErrorAsString());
                                    }
                                }
                            }

                            if ($order->status_id != $status) {
                                $order->status_id = $status;
                                if (!$order->save(false, ['status_id'])) {
                                    throw new \Exception($order->getFirstErrorAsString());
                                }

                                if (empty($request->approved_at) && in_array($order->status_id, [
                                        OrderStatus::STATUS_DELIVERY_BUYOUT,
                                        OrderStatus::STATUS_DELIVERY_REFUND,
                                        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                                        OrderStatus::STATUS_DELIVERY_DENIAL,
                                        OrderStatus::STATUS_DELIVERY_RETURNED
                                    ])
                                ) {
                                    $request->approved_at = time();
                                }

                                if (OrderStatus::isFinalDeliveryStatus($status)) {
                                    $request->status = DeliveryRequest::STATUS_DONE;
                                    $request->done_at = time();
                                } elseif ($order->status_id == OrderStatus::STATUS_DELIVERY_REJECTED) {
                                    $request->status = DeliveryRequest::STATUS_ERROR;
                                }
                            }
                            $request->route = Yii::$app->controller->route;
                            if (!$request->save()) {
                                throw new \Exception($request->getFirstErrorAsString());
                            }

                            if ($lastUpdateResponse && !$lastUpdateResponse->save()) {
                                throw new \Exception($lastUpdateResponse->getFirstErrorAsString());
                            }

                            $request->saveUnBuyoutReason($response['unshipping_reason'] ?? '');

                            $transaction->commit();
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            $request->api_error = 'Error when saving response data: ' . $e->getMessage();
                            $request->save(false, ['api_error']);
                            $cronLog('Error when saving response data: ' . $e->getMessage() . PHP_EOL . print_r($e->getTrace(), true), [
                                'order_id' => $request->order_id,
                                'response' => json_encode($response, JSON_UNESCAPED_UNICODE),
                                'type' => 2
                            ]);
                        }
                    }
                }

                if (!empty($handlers)) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $queueUrl,
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }
            }
        } catch (\Throwable $e) {
            $cronLog('Error: ' . $e->getMessage() . PHP_EOL . print_r($e->getTrace(), true), ['type' => 3]);
        }
    }
}