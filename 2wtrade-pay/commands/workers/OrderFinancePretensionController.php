<?php

namespace app\commands\workers;

use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;
use yii\console\Controller;
use yii\helpers\Console;

class OrderFinancePretensionController extends Controller
{
    const BATCH_SIZE = 200;

    public function getAddmissileStatuses()
    {
        return [
            DeliveryReport::STATUS_CLOSED,
            DeliveryReport::STATUS_COMPLETE,
        ];
    }

    /**
     * Проверка указанного отчета на претензии
     * @param integer $delivery_report_id
     */
    public function actionCheckReport($delivery_report_id)
    {
        if ($deliveryReport = $this->getDeliveryReportById($delivery_report_id)) {
            OrderFinancePretension::generatePretensionsByReport($deliveryReport, $console = true);
        }
    }

    /**
     * Проверка претензий по всем отчетам
     * Все финансовые отчеты у которых заполнен маппинг статусов
     */
    public function actionCheckReports()
    {
        $query = DeliveryReport::find()
            ->where(['type' => DeliveryReport::TYPE_FINANCIAL])
            ->andWhere(['is not', 'status_pair', null])
            ->andWhere(['status' => $this->getAddmissileStatuses()]);

        foreach ($query->batch(self::BATCH_SIZE) as $deliveryReports) {
            foreach ($deliveryReports as $report) {
                /** @var DeliveryReport $report */
                if ($this->checkDeliveryReport($report, $report->id)) {
                    OrderFinancePretension::generatePretensionsByReport($report, $console = true);
                }
            }
        }
    }

    /**
     * Поиск отчета
     * @param $delivery_report_id
     * @return DeliveryReport|bool
     */
    public function getDeliveryReportById($delivery_report_id)
    {
        $deliveryReport = DeliveryReport::findOne(['id' => $delivery_report_id]);

        return $this->checkDeliveryReport($deliveryReport, $delivery_report_id, false);
    }

    /**
     * Проверка отчета на наличие претензий в записях
     * @param DeliveryReport $deliveryReport
     * @param integer $delivery_report_id
     * @param bool $pass_errors
     * @return DeliveryReport|bool
     */
    public function checkDeliveryReport($deliveryReport, $delivery_report_id, $pass_errors = true)
    {
        $checkStatuses = [DeliveryReport::STATUS_IN_WORK, DeliveryReport::STATUS_COMPLETE, DeliveryReport::STATUS_CLOSED];

        Console::output("DeliveryReport id #" . $delivery_report_id);

        if (!$deliveryReport) {
            if (!$pass_errors) {
                Console::error("Error: report #" . $delivery_report_id . " not found.");
            }
        } else {
            if ($deliveryReport->type != DeliveryReport::TYPE_FINANCIAL && !$pass_errors) {
                Console::error("Error: report #" . $deliveryReport->id . " type is not " . DeliveryReport::TYPE_FINANCIAL . ".");
            } elseif (!in_array($deliveryReport->status, $this->getAddmissileStatuses()) && !$pass_errors) {
                Console::error("Error: report #" . $delivery_report_id . " status not in " . implode(", ", $checkStatuses) . ".");
            } elseif (empty($deliveryReport->status_pair) || is_null($deliveryReport->status_pair)) {
                Console::error("Error: report #" . $delivery_report_id . " not having STATUS_PAIR");
            } else {
                return $deliveryReport;
            }
        }

        return false;
    }
}