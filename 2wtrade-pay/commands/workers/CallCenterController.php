<?php

namespace app\commands\workers;

use app\components\console\WorkerController;
use app\models\Country;
use app\modules\api\models\ApiLog;
use app\modules\callcenter\components\NewCallCenterHandler;
use app\modules\callcenter\exceptions\CallCenterRequestAlreadyDone;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\logger\components\log\Logger;
use app\modules\order\models\Order;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class CallCenterController
 * @package app\commands\workers
 */
class CallCenterController extends WorkerController
{
    /**
     * NPAY-1413 Воркер, который принимает результаты прозвона кц из очереди
     *
     * @param string $queue_url // урла амазоновской очереди, по дефолту берется из конфигов
     * @param integer $lifetime // время жизни воркера в секунды
     * @param integer $timeout // время таймаута если очередь пустая
     */
    public function actionGetCallCenterStatuses($queue_url = null, $lifetime = 86400, $timeout = 60)
    {
        $this->log('Getting CallCenter statuses');

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $launchTime = time();
            $client = $this->getSqsClient();
            $queueUrl = $queueUrl = (is_string($queue_url) && !empty($queue_url)) ? $queue_url : $client->getSqsQueueUrl('amazonGetCallCenterStatusesQueueUrl');

            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {

                $response = $client->receiveMessage([
                    'MaxNumberOfMessages' => 10,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $queueUrl,
                ]);
                $messages = $response->get('Messages');

                if (count($messages) == 0) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    sleep($timeout);
                    continue;
                }

                $this->log('Count messages: ' . count($messages));

                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

                $responses = [];
                $checkQueueResponses = [];

                foreach ($messageBodies as $messageId => $messageBody) {
                    $order = json_decode($messageBody, true);

                    if (isset($order['queue_type']) && in_array($order['queue_type'], array_keys(CallCenterCheckRequest::getTypes()))) {
                        $checkQueueResponses[$messageId] = $order;
                    } else {
                        $responses[$messageId] = $order;
                    }
                }

                /**
                 * @var Order[] $orders
                 */
                $orders = Order::find()
                    ->with(['callCenterRequest', 'deliveryRequest', 'country'])
                    ->where([Order::tableName() . '.id' => ArrayHelper::getColumn($responses, 'foreign_id')])
                    ->all();
                $orders = ArrayHelper::index($orders, 'id');

                $errors = [];

                // Если не удалось найти заказ по нашему ИД пробуем искать по ИД КЦ
                foreach ($responses as $messageId => $response) {
                    if (!isset($response['foreign_id']) || empty($response['foreign_id']) || !isset($orders[$response['foreign_id']])) {
                        $order = Order::find()->joinWith(['callCenterRequest.callCenter', 'country'])
                            ->with(['deliveryRequest', 'country'])
                            ->where([
                                Country::tableName() . '.slug' => $response['country_char_code'],
                                CallCenter::tableName() . '.is_amazon_query' => 1,
                                CallCenterRequest::tableName() . '.foreign_id' => $response['id']
                            ])
                            ->one();
                        if ($order) {
                            $responses[$messageId]['foreign_id'] = $order->id;
                            $orders[$order->id] = $order;
                        }
                    }
                }

                // Чистим статичные переменные, так как настройки курьерок, стран и т.д. могли измениться пока мы получали новые ссобщения
                NewCallCenterHandler::clearStaticVars();

                foreach ($responses as $messageId => $response) {
                    $orderId = null;
                    try {
                        if (!isset($response['foreign_id'])) {
                            throw new \Exception("Order ID not found");
                        }
                        $orderId = $response['foreign_id'];
                        if (!isset($orders[$orderId])) {
                            throw new \Exception("Order not found.");
                        }
                        $orders[$orderId] = NewCallCenterHandler::saveResponse($response, $orders[$orderId]);

                    }
                    catch (CallCenterRequestAlreadyDone $e) {
                        $this->log($e->getMessage());
                        $errors[$messageId] = [
                            'order_id' => $orderId,
                            'foreign_id' => $response['id'],
                            'order' => $response,
                            'error' => $e->getMessage()
                        ];
                    }
                    catch (yii\db\Exception $e) {
                        unset($handlers[$messageId]);
                        $this->log($e->getMessage());
                        $errors[$messageId] = [
                            'order_id' => $orderId,
                            'foreign_id' => $response['id'],
                            'order' => $response,
                            'error' => $e->getMessage()
                        ];
                    } catch (\Throwable $e) {
                        $errors[$messageId] = [
                            'order_id' => $orderId,
                            'foreign_id' => $response['id'],
                            'order' => $response,
                            'error' => $e->getMessage()
                        ];
                        $cronLog("Error: " . $e->getMessage(), [
                            'order_id' => $orderId,
                            'response' => $messageBodies[$messageId]
                        ]);
                    }
                }

                foreach ($checkQueueResponses as $messageId => $response) {
                    try {
                        if (!isset($response['foreign_id'])) {
                            throw new \Exception("Order ID not found");
                        }
                        /**
                         * @var CallCenterCheckRequest $request
                         */
                        $request = CallCenterCheckRequest::find()
                            ->where([
                                CallCenterCheckRequest::tableName() . '.order_id' => $response['foreign_id'],
                                CallCenterCheckRequest::tableName() . '.foreign_id' => $response['id'],
                                CallCenterCheckRequest::tableName() . '.type' => $response['queue_type']
                            ])->one();
                        if (!$request) {
                            $request = CallCenterCheckRequest::find()
                                ->where([
                                    CallCenterCheckRequest::tableName() . '.order_id' => $response['foreign_id'],
                                    CallCenterCheckRequest::tableName() . '.type' => $response['queue_type']
                                ])->andWhere(['OR', [CallCenterCheckRequest::tableName(). '.foreign_id' => null], [CallCenterCheckRequest::tableName(). '.foreign_id' => 0]])->one();
                        }

                        if ($request) {
                            NewCallCenterHandler::saveCheckResponse($response, $request);
                        } else {
                            throw new \Exception("Request not found.");
                        }
                    } catch (yii\db\Exception $e) {
                        unset($handlers[$messageId]);
                        $this->log($e->getMessage());
                        $errors[$messageId] = [
                            'order_id' => $response['foreign_id'],
                            'foreign_id' => $response['id'],
                            'queue_type' => $response['queue_type'],
                            'order' => $response,
                            'error' => $e->getMessage()
                        ];
                    } catch (\Throwable $e) {
                        $errors[$messageId] = [
                            'order_id' => $response['foreign_id'],
                            'foreign_id' => $response['id'],
                            'queue_type' => $response['queue_type'],
                            'order' => $response,
                            'error' => $e->getMessage()
                        ];
                        $cronLog("Error: " . $e->getMessage(), [
                            'order_id' => $response['foreign_id'],
                            'response' => $messageBodies[$messageId],
                            'queue_type' => $response['queue_type']
                        ]);
                    }
                }

                // Если по каким-то причинам нам не удалось сохранить у себя изменения, то кладем данные в очередь ошибок

                if (!empty($errors)) {
                    $errorMessages = [];

                    foreach ($errors as $key => $values) {
                        $errorMessages[] = [
                            'Id' => $key,
                            'MessageBody' => json_encode($values, JSON_UNESCAPED_UNICODE),
                        ];
                    }
                    $result = $client->sendMessageBatch([
                        'QueueUrl' =>$client->getSqsQueueUrl('amazonGetCallCenterStatusesError'),
                        'Entries' => $errorMessages
                    ]);

                    // Если по какой-то причине не удалось отправить ошибки в очередь, то не удаляем сообщения
                    $failedRequests = $result->get('Failed');

                    if (is_array($failedRequests)) {
                        foreach ($failedRequests as $request) {
                            $cronLog("Error while sending a message to error queue: {$request['Message']}", [
                                'body' => json_encode($errors[$request['Id']], JSON_UNESCAPED_UNICODE),
                                'error' => $request['Message']
                            ]);
                            unset($handlers[$request['Id']]);
                        }
                    }
                }

                if (!empty($handlers)) {
                    $deletingEntries = [];
                    if (is_array($handlers)) {
                        foreach ($handlers as $messageId => $handler) {
                            $deletingEntries[] = [
                                'Id' => $messageId,
                                'ReceiptHandle' => $handler,
                            ];
                        }
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $queueUrl,
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');

                    if (is_array($failedRequests)) {
                        foreach ($failedRequests as $request) {
                            $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                                'messageBody' => $messageBodies[$request['Id']],
                                'error' => $request['Message']
                            ]);
                        }
                    }
                }

            }
        } catch (\Throwable $e) {
            $cronLog("Error: " . $e->getMessage());
            $this->log($e->getMessage());
        }

        $this->log('done');
    }

    /**
     * Воркер, который создает новый ордер на основании запроса из кц амазон-очереди
     *
     * @param string|null $queue_url // урла амазоновской очереди, по дефолту берется из конфигов
     * @param integer $lifetime // время жизни воркера в секундах
     * @param integer $timeout // время таймаута если очередь пустая
     */
    public function actionCreateNewOrderByCallCenter($queue_url = null, $lifetime = 86400, $timeout = 120)
    {
        $this->log('Getting new orders from CallCenter');

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $launchTime = time();
            $client = $this->getSqsClient();
            $queueUrl = (is_string($queue_url) && !empty($queue_url)) ? $queue_url : $client->getSqsQueueUrl('amazonLeadNewLeadFromCC');

            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {
                $response = $client->receiveMessage([
                    'MaxNumberOfMessages' => 10,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $queueUrl,
                ]);
                $messages = $response->get('Messages');
                if (count($messages) == 0) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    sleep($timeout);
                    continue;
                }

                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');
                $responses = [];

                foreach ($messageBodies as $messageId => $messageBody) {
                    $order = json_decode($messageBody, true);
                    $responses[$messageId] = $order;
                }

                $errors = [];
                $orders = [];
                $output = [];

                // Чистим статичные переменные, так как настройки курьерок, стран и т.д. могли измениться пока мы получали новые ссобщения
                NewCallCenterHandler::clearStaticVars();

                foreach ($responses as $messageId => $response) {
                    $apiLog = new ApiLog();
                    $apiLog->type = ApiLog::TYPE_LEAD;
                    $apiLog->input_data = json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                    $apiLog->status = ApiLog::STATUS_FAIL;

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $order = NewCallCenterHandler::createNewOrderFromResponse($response);
                        $orders[$messageId] = $order;
                        $apiLog->status = ApiLog::STATUS_SUCCESS;
                        $outputData = [
                            'id' => $response['id'],
                            'foreign_id' => $order->id,
                        ];
                        $apiLog->output_data = json_encode($outputData);
                        $transaction->commit();

                        try {
                            $result = NewCallCenterHandler::sendOurOrderIdThroughApi($outputData);
                            if (!$result['status']) {
                                throw new \Exception($result['response']);
                            }
                        } catch (\Throwable $e) {
                            $cronLog('Error when sending our order id to call center: ' . $e->getMessage(), [
                                'order_id' => $order->id,
                                'foreign_id' => $response['id']
                            ]);
                            $output[$messageId] = $outputData;
                        }
                    } catch (yii\db\Exception $e) {
                        $transaction->rollBack();
                        $errors[$messageId] = [
                            'foreign_id' => $response['id'],
                            'order' => $response,
                            'error' => $e->getMessage(),
                        ];
                        unset($handlers[$messageId]);
                        $this->log($e->getMessage());
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        $errors[$messageId] = [
                            'foreign_id' => $response['id'],
                            'order' => $response,
                            'error' => $e->getMessage(),
                        ];
                        $cronLog("Error: " . $e->getMessage(), [
                            'foreign_id' => $response['id'],
                            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
                        ]);
                    }

                    $apiLog->save();
                }

                if (!empty($output)) {
                    $outputMessages = [];
                    foreach ($output as $messageId => $out) {
                        $outputMessages[] = [
                            'Id' => $messageId,
                            'MessageBody' => json_encode($out, JSON_UNESCAPED_UNICODE),
                        ];
                    }

                    $result = $client->sendMessageBatch([
                        'QueueUrl' => $client->getSqsQueueUrl('amazonLeadNewLeadFromCCResponse'),
                        'Entries' => $outputMessages,
                    ]);

                    $failedMessages = $result->get('Failed');
                    foreach ($failedMessages as $message) {
                        $cronLog('Error! Failed sent out message through amazon queue: ' . $message['Message'], $output[$message['Id']]);
                    }
                }

                // Если по каким-то причинам нам не удалось сохранить у себя изменения, то кладем данные в очередь ошибок
                if (!empty($errors)) {
                    $errorMessages = [];
                    foreach ($errors as $key => $values) {
                        $errorMessages[] = [
                            'Id' => $key,
                            'MessageBody' => json_encode($values, JSON_UNESCAPED_UNICODE),
                        ];
                    }
                    $result = $client->sendMessageBatch([
                        'QueueUrl' => $client->getSqsQueueUrl('amazonLeadNewLeadFromCCErrors'),
                        'Entries' => $errorMessages
                    ]);

                    // Если по какой-то причине не удалось отправить ошибки в очередь, то не удаляем сообщения
                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while sending a message to error queue: {$request['Message']}", [
                            'body' => json_encode($errors[$request['Id']], JSON_UNESCAPED_UNICODE),
                            'error' => $request['Message']
                        ]);
                        unset($handlers[$request['Id']]);
                    }
                }

                if (!empty($handlers)) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $queueUrl,
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }

            }
        } catch (\Throwable $e) {
            $cronLog('Error: ' . $e->getMessage());
            $this->log($e->getMessage());
        }

        $this->log('done');
    }


    /**
     * @param null $queue_url // урла амазоновской очереди, по дефолту берется из конфигов
     * @param int $lifetime секунды (время жизни воркера)
     * @param int $timeout секунды (время таймаута при пустоте очереди)
     */
    public function actionGetSendErrors($queue_url = null, $lifetime = 86400, $timeout = 600)
    {
        set_time_limit(0);
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $client = $this->getSqsClient();
            $queueUrl = (is_string($queue_url) && !empty($queue_url)) ? $queue_url : $client->getSqsQueueUrl('amazonLeadSendErrorQueue');
            $launchTime = time();

            while (((time() - $launchTime) < $lifetime) || empty($lifetime)) {
                $response = $client->receiveMessage([
                    'MaxNumberOfMessages' => 10,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $queueUrl,
                ]);
                $messages = $response->get('Messages');
                if (count($messages) == 0) {
                    // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                    sleep($timeout);
                    continue;
                }

                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');
                $responses = [];
                $checkQueueResponses = [];

                foreach ($messageBodies as $messageId => $messageBody) {
                    $response = json_decode($messageBody, true);
                    if (isset($response['queue_type']) && in_array($response['queue_type'], array_keys(CallCenterCheckRequest::getTypes()))) {
                        $checkQueueResponses[$messageId] = $response;
                    } else {
                        $responses[$messageId] = $response;
                    }
                }

                /**
                 * @var Order[] $orders
                 */
                $orders = Order::find()
                    ->with(['callCenterRequest'])
                    ->where([Order::tableName() . '.id' => ArrayHelper::getColumn($responses, 'order_id')])
                    ->indexBy('id')
                    ->all();

                foreach ($responses as $messageId => $response) {
                    $orderId = null;
                    try {
                        if (!isset($response['order_id'])) {
                            throw new \Exception("Order ID not found");
                        }
                        $orderId = $response['order_id'];
                        if (!isset($orders[$orderId])) {
                            throw new \Exception("Order not found.");
                        }
                        NewCallCenterHandler::saveSendError($response, $orders[$orderId]);

                    } catch (yii\db\Exception $e) {
                        $this->log($e->getMessage());
                        unset($handlers[$messageId]);
                    } catch (\Throwable $e) {
                        $cronLog("Error: " . $e->getMessage(), [
                            'order_id' => $orderId,
                            'response' => $messageBodies[$messageId],
                            'queue_type' => isset($response['queue_type']) ? $response['queue_type'] : ''
                        ]);
                    }
                }

                foreach ($checkQueueResponses as $messageId => $response) {
                    try {
                        if (!isset($response['order_id'])) {
                            throw new \Exception("Order ID not found");
                        }
                        /**
                         * @var CallCenterCheckRequest $request
                         */
                        $request = CallCenterCheckRequest::find()
                            ->where([
                                CallCenterCheckRequest::tableName() . '.order_id' => $response['foreign_id'],
                                CallCenterCheckRequest::tableName() . '.foreign_id' => $response['id'],
                                CallCenterCheckRequest::tableName() . '.type' => $response['queue_type']
                            ])->one();
                        if (!$request) {
                            $request = CallCenterCheckRequest::find()
                                ->where([
                                    CallCenterCheckRequest::tableName() . '.order_id' => $response['foreign_id'],
                                    CallCenterCheckRequest::tableName() . '.type' => $response['queue_type']
                                ])->andWhere(['OR', [CallCenterCheckRequest::tableName(). '.foreign_id' => null], [CallCenterCheckRequest::tableName(). '.foreign_id' => 0]])->one();
                        }
                        if ($request) {
                            NewCallCenterHandler::saveSendToCheckError($response, $request);
                        } else {
                            throw new \Exception("Order not found.");
                        }
                    } catch (yii\db\Exception $e) {
                        $this->log($e->getMessage());
                        unset($handlers[$messageId]);
                    } catch (\Throwable $e) {
                        $cronLog("Error: " . $e->getMessage(), [
                            'order_id' => $response['order_id'],
                            'response' => $messageBodies[$messageId],
                            'queue_type' => $response['queue_type']
                        ]);
                    }
                }

                if (!empty($handlers)) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }

                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $queueUrl,
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }
            }
        } catch (\Throwable $e) {
            $this->log($e->getMessage());
            $cronLog("Exception while worker worked: {$e->getMessage()}", [
                'exception' => $e->getMessage(),
                'type' => 'worker'
            ]);
        }
    }
}