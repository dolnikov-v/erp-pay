<?php

namespace app\commands\workers;

use app\components\console\WorkerController;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\catalog\models\ExternalSource;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonLink;
use Aws\Exception\AwsException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class EmployManagementController
 * @package app\commands\workers
 */
class EmployManagementController extends WorkerController
{
    //в секундах
    const TIMEOUT = 60;
    const TYPE_LOGGER_ACTION = 'manage employ';
    const DELETE_MESSAGES = true;

    const ACTION_EMPLOY = 'employ';
    const ACTION_DISMISS = 'dismissal';

    const TOKEN_FOR_2WCALL = 'b5e9962145ff10481ed14913eaa0877587b28fea';
    const LIST_DOCS = [
        'employement_agreement_with_operators_of call_center_indonesia_update.docx',
        'information_resources_rules_for_users.docx'
    ];


    /**
     * @return array
     */
    public function getCollectionTypes()
    {
        return [
            self::ACTION_EMPLOY,
            self::ACTION_DISMISS
        ];
    }


    /**
     * @param string $data
     * @return array
     */
    public function parseData($data)
    {
        return $this->mapping($data);
    }

    /**
     * @param $data
     * @return array
     */
    public function compareData($data)
    {
        $comparedData = [];

        if (isset($data['type']) && isset($data['docs'])) {

            foreach ($data['docs'] as $num=>$personData){
                $comparedData[$num] = $personData;
                $comparedData[$num]['type'] = $data['type'];

                if (isset($comparedData[$num]['surname']) && isset($comparedData[$num]['name'])) {
                    $comparedData[$num]['name'] = $comparedData[$num]['surname'] . ' ' . $comparedData[$num]['name'];
                }
            }
        }

        return $comparedData;
    }

    /**
     * @param $data
     * @return array
     */
    public function mapping($data)
    {
        $map = [
            //добавить mercury_id в таблицу salary_person
            'id' => 'mercury_id',
            'company_id' => 'office_id',
            'type' => 'type',
            'name' => 'name',
            'sex' => 'gender',
            'phone' => 'phone',
            'e_mail' => 'corporate_email',
            'residing' => 'address',
            'work_shift_id' => 'working_shift_id',
            'job_position_id' => 'designation_id',
            'monthly_salary_local' => 'salary_local',
            'monthly_salary_usd' => 'salary_usd',
            'birth_date' => 'birth_date',
            'experience' => 'experience',
            'bank_account' => 'account_number',
        ];

        $mapped = [];

        foreach ($data as $k => $v) {
            if (isset($map[$k])) {
                $mapped[$map[$k]] = $v;
            }
        }

        $mapped['active'] = Person::ACTIVE;

        return $mapped;
    }

    /**
     * @return string
     */
    public function getTemplateEmail()
    {
        return <<<EMAIL
       
        <h1>Hello, {name}</h1>
        <p>We are glad to see you in the ranks of our Company.</p>
        </p>We remind you that when you have a job, you voluntarily signed the following documents (they are all in the attachment to the letter):</p>
        <ol>
            <li>Employment contract</li>
            <li>Regulations on fines and bonuses</li>
            <li>Consent to the processing of personal data</li>
            <li>Agreement for internship 2 days free of charge (if employed, then these days are paid)</li>
            <li>Consent to the calculation of SP only for the time worked</li>
            <li>Consent to work on company standards</li>
            <li>Information resources rules for users</li>
        </ol>
        <p>We ask you to carefully read them again and strictly follow the conditions prescribed in these documents.</p>
        
        <p>Your Access to system <a href="{source_url}">{source_url}</a>
        
        <p>
        <strong>username:</strong> {username}<br/>
        <strong>password:</strong> {password}
        </p>
EMAIL;
    }

    /**
     * Получить сообщения из очереди
     * @return array
     */
    public function getMessages($queue)
    {
        $responses = [];

        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'action' => self::TYPE_LOGGER_ACTION,
            'process_id' => getmypid(),
        ]);

        try {
            $client = $this->getSqsClient();
            $response = $client->receiveMessage([
                'MaxNumberOfMessages' => 10,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $client->getSqsQueueUrl($queue),
            ]);

            $messages = $response->get('Messages');


            if (count($messages) == 0) {
                // Если очередь пустая, то ждем $timeout секунд и чекаем опять
                $this->log('Empty queue / Timeout ' . self::TIMEOUT . ' seconds');
                sleep(self::TIMEOUT);
            } else {
                $handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
                $messageBodies = ArrayHelper::map($messages, 'MessageId', 'Body');

                //$messageBodies = ['{"type":"dismissal", "docs":[{"id":5271, "timestamp":152352118570242,"surname":"ECHEVERRIA","name":"MARIELENA HERNANDEZ_5271","sex":"M","residing":"Russia, Moscow, Zagorodnoe avenue, 2","phone":"durdom","e_mail":"Entrenamiento2wtrade@gmail.com","experience":null,"url_crm":"2wcall.com","speak_languages":null,"head_id":null,"monthly_salary_local":0.0000,"mon_sal_loc_after_taxed":0.0000,"monthly_salary_usd":0.0000,"login_type":"Persistent","local_id":"3273","bank_account":"3273","birth_date":"2018-04-11","has_criminal":"N","bank_name":"VA bank","bank_address":"xxx","swift":"xxx","contractors_folder_id":8,"company_id":8,"job_position_id":5,"work_shift_id":160,"pay_id":3273,"pay_action":"dismissal","call_countries":[],"contractor_id":null,"date_est_finish":null,"currency_id":null,"doc_control_point_id":null,"implement_country_id":null,"doc_date":"2018-04-11","notes":null,"id":5271,"impl_status":"dismissal","creator_id":null,"rec_user_id":null}]}'];


                if (is_array($messageBodies)) {
                    foreach ($messageBodies as $messageId => $messageBody) {

                        $message = json_decode($messageBody,1);

                        $data = $this->compareData($message);

                        $responses = array_merge($responses, $data);
                    }
                }

                //delete messages from queue
                if (!empty($handlers) && self::DELETE_MESSAGES) {
                    $deletingEntries = [];
                    foreach ($handlers as $messageId => $handler) {
                        $deletingEntries[] = [
                            'Id' => $messageId,
                            'ReceiptHandle' => $handler,
                        ];
                    }


                    $result = $client->deleteMessageBatch([
                        'QueueUrl' => $client->getSqsQueueUrl($queue),
                        'Entries' => $deletingEntries,
                    ]);

                    $failedRequests = $result->get('Failed');
                    foreach ($failedRequests as $request) {
                        $cronLog("Error while deleting a message from queue: {$request['Message']}", [
                            'messageBody' => $messageBodies[$request['Id']],
                            'error' => $request['Message']
                        ]);
                    }
                }
            }
        } catch (\Throwable $e) {
            $cronLog("Error: " . $e->getMessage());
            $this->log($e->getMessage());
        }

        $this->log('Receiving done');
        $this->log('Count messages: ' . count($responses));

        return $responses;
    }

    /**
     * Принимаем данные для сторонних источников из очереди Амазона из Меркурия
     * Работа с операциями
     * Перебрать все сообщения, проверить на корректность - отдать дальше в работу
     */
    public function actionManage()
    {
        $actions = $this->getMessages('mercuryQueue');

        $results = [];

        foreach ($actions as $action) {
            if ($this->validateParams($action)) {
                $result = $this->getActionFromType($action);

                if (!empty($result)) {
                    $results[] = $result;
                }
            }
        }

        //вернуть меркурию данные?
        return $results;
    }

    /**
     * @param $action
     * @return array
     */
    public function getActionFromType($action)
    {
        switch ($action['type']) {
            case self::ACTION_DISMISS :
                $result = $this->DismissalAction($action);
                break;
            case self::ACTION_EMPLOY :
                $result = $this->EmployAction($action);
                break;
        }

        return isset($result) ? $result : [];
    }

    /**
     * Слушаем очереди Амазона - с данными от сторонних источников
     */
    public function actionGetDataFromExternalSource()
    {
        //случшаем ответки от всех сторонних источников
        $external_sources = ExternalSource::find()
            ->where(['is not', 'incoming_amazon_queue_name', null])
            ->all();

        foreach ($external_sources as $source) {
            $logger = Yii::$app->get("processingLogger");
            $cronLog = $logger->getLogger([
                'route' => $this->getRoute(),
                'action' => __METHOD__,
                'process_id' => getmypid(),
            ]);


            $messages = $this->getMessages($source->incoming_amazon_queue_name);

            foreach ($messages as $message) {
                $transaction = yii::$app->db->beginTransaction();

                //создать линк на юзера стороннего источника
                $result = $this->createPersonLinkCrm($message);

                if (!$result['success']) {
                    $error = $result['errors'];
                } else {
                    //создать запись в CallCenterUser
                    $addCallCenterUser = $this->createCallCenterUser($message);
                    //определить данные для отправки емайл
                    $person = $this->getPersonByMercuryLink($result['data']->person_id);

                    if ($addCallCenterUser['success']) {
                        if ($person) {
                            //Результат отправки мыла
                            $sendEmail = $this->sendEmail($person, $message, $source);
                            echo 'SEND MAIL: ' . ($sendEmail ? 'true' : false) . PHP_EOL;
                        }
                    }
                }

                if (isset($error)) {
                    echo $error;
                    $cronLog($error);
                    $transaction->rollBack();
                } else {
                    $transaction->commit();
                }

                echo '<pre>' . print_r($message, 1) . '</pre>';
            }
        }
    }

    /**
     * @param Person $personModel
     * @param array $accountData
     * @return bool
     */
    public function sendEmail($personModel, $accountData, $source)
    {
        $docsPath = yii::getAlias('@docs');
        $tpl = $this->getTemplateEmail();
        $emailBody = strtr($tpl, [
            '{name}' => $personModel->name,
            '{username}' => $accountData['username'],
            '{password}' => $accountData['password'],
            '{source_url}' => $source->url
        ]);
        //отправить пользователю email
        $sendEmail = Yii::$app->mailer->compose()
            ->setFrom(yii::$app->params['noReplyEmail'])
            ->setTo($personModel->corporate_email)
            ->setSubject('Registration account')
            ->setHtmlBody($emailBody);
        //прикрепим доки
        foreach (self::LIST_DOCS as $doc) {
            $sendEmail->attach($docsPath . DIRECTORY_SEPARATOR . $doc);
        }

        return $sendEmail->send();
    }

    /**
     * @param $data
     * @return array
     */
    public function createCallCenterUser($data)
    {
        echo 'CREATED CALL_CENTER_USER' . PHP_EOL;
        echo '<pre>' . print_r($data, 1) . '</pre>' . PHP_EOL;
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'action' => __METHOD__,
            'process_id' => getmypid(),
        ]);

        //Пока мы умеем распозновать только 2wcall для CallCenterUser
        if ($data['source'] == PersonLink::TYPE_CRM) {

            //если у КЦ включен
            $callCenter = CallCenter::find()->where(['is_amazon_query' => 1])->one();

            if ($callCenter) {
                //определить физ КЦ по очереди амазон
                $callCenterUser = new CallCenterUser();
                $callCenterUser->callcenter_id = $callCenter->id;
                $callCenterUser->user_id = (string)$data['id'];
                $callCenterUser->person_id = $data['foreign_id'];
                $callCenterUser->user_login = $data['username'];
                $callCenterUser->user_password = $data['password'];
                $callCenterUser->active = CallCenterUser::ACTIVE;

                if (!$callCenterUser->save()) {
                    $error = json_encode($callCenterUser->getErrors(), JSON_UNESCAPED_UNICODE);
                    $cronLog($error);
                    echo 'CALL CENTER USER NOT CREATED: ' . $error . PHP_EOL;
                    $result = [
                        'success' => false,
                        'error' => $error
                    ];
                } else {
                    echo 'CALL CENTER USER #' . $callCenterUser->id . ' CREATED' . PHP_EOL;
                    $result = [
                        'success' => true
                    ];
                }
            }

        }

        if (!isset($result)) {
            $error = 'Error identificate callCenter';
            echo $error . PHP_EOL;
            $cronLog($error);

            $result = [
                'success' => false,
                'error' => $error
            ];
        }

        return $result;

    }

    /**
     * Вернёт массив со сторонними источниками и ролями этих источников
     * @param $designation_id
     * @return array
     */
    public function getDataDesignation($designation_id)
    {
        $designationQuery = Designation::find()
            ->where([
                'id' => $designation_id,
                'active' => 1
            ]);

        $designationData = [];

        if ($designationQuery->exists()) {
            $designation = $designationQuery->one();

            $designationExtensionRoles = $designation->getDesignationExtensionRole()->all();

            //переберём все связи должности с со сторонними источниками (для получения списка ролей)
            foreach ($designationExtensionRoles as $designationExtensionRole) {
                $externalSources = $designationExtensionRole->getExternalSource()->all();

                //переберём все сторонние источники для получения списка доступных ролей
                foreach ($externalSources as $source) {
                    $externalSourceRoles = $source->getExternalSourceRole()->all();

                    //переберём роли сторонних источников для сбора данных о них
                    foreach ($externalSourceRoles as $externalSourceRole) {

                        //нужны только те роли, которые указаны в связях с должностью
                        if ($externalSourceRole->id == $designationExtensionRole->external_source_role_id) {

                            if (!isset($designationData[$source->id])) {

                                $designationData[$source->id] = [
                                    'source' => $source,
                                    'roles' => []
                                ];
                            }

                            $designationData[$source->id]['roles'][] = $externalSourceRole->name;
                        }
                    }
                }
            }
        } else {
            $designationData = [];
        }

        return $designationData;
    }

    /**
     *  //не понятно пока - возможно валидаторы отдельные нужны для каждого типа операций
     * @param $action
     * @return bool
     */
    public function validateParams($action)
    {
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'action' => self::TYPE_LOGGER_ACTION,
            'process_id' => getmypid(),
            'mercury_id' => isset($action['id']) ? $action['id'] : null,
        ]);

        $actionString = json_encode($action, JSON_UNESCAPED_UNICODE);


        if (!isset($action['type'])) {
            $error = "Error: incorrect format json (no type): ";
        }

        if (isset($error)) {
            $cronLog($error . $actionString);
            return false;
        }

        return true;
    }

    /**
     * @param string $message
     * @param boolean $eol
     */
    protected function log($message, $eol = true)
    {
        if (YII_ENV_DEV) {
            $message .= $eol ? PHP_EOL : '';

            Console::stdout($message);
        }
    }

    /**
     * Метод для приёма нового сотрудника
     * @param $data
     * @return array
     */
    public function EmployAction($data)
    {
        echo '<pre>' . print_r($data, 1) . '</pre>';

        $actionString = json_encode($data, JSON_UNESCAPED_UNICODE);
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'action' => self::TYPE_LOGGER_ACTION,
            'process_id' => getmypid(),
            'request' => $actionString
        ]);

        $mappedData = $this->parseData($data);

        echo 'MAPPED DATA: ' . PHP_EOL;
        echo '<pre>' . print_r($mappedData, 1) . '</pre>' . PHP_EOL;

        $transaction = yii::$app->db->beginTransaction();

        $model = new Person();

        //создаем физ лицо
        if ($model->load($mappedData, '') && $model->validate() && $model->save()) {

            //ссылочку сделаем в salary_person_link на меркурий
            $salaryPersonLinkMercury = $this->createPersonLinkMercury($data['id'], $model);

            if (!$salaryPersonLinkMercury['success']) {
                $errors = $salaryPersonLinkMercury['errors'];
            } else {
                //получим данные о сторонних источниках и ролях этих источников по должности
                $dataDesignation = $this->getDataDesignation($model->designation_id);

                if (!empty($dataDesignation)) {
                    //отправим в очереди сторонних источников данные о новых пользователях
                    $personParams = [
                        'email' => $model->corporate_email,
                        'name' => $model->name,
                        'foreign_id' => $model->id,
                        'phone' => $model->phone,
                        'partner_token' => self::TOKEN_FOR_2WCALL,
                        'country' => $data['call_countries']
                    ];

                    //попытаемся отправить и на этом этапе все.
                    $this->prepareMessageQueue($dataDesignation, $personParams);
                }
            }
        } else {
            $errors = $model->getFirstErrors();
        }

        //не смогли создать физ лицо с ссылками
        if (isset($errors)) {
            $transaction->rollBack();
            $cronLog("Error: " . json_encode($errors, JSON_UNESCAPED_UNICODE));
            $result = [
                'success' => false,
                'mercury_id' => $data['id'],
                'errors' => $errors
            ];
        } else {
            //физ лицо с ссылками успешно создано
            $transaction->commit();
            $result = [
                'success' => true,
                'mercury_id' => $data['id'],
                'model' => $model
            ];

        }

        return $result;
    }

    /**
     * Метод для подготовки и отправки данных в Amazon о новых пользователях сторонних источников
     * @param $data
     * @param $personParams
     */
    public function prepareMessageQueue($data, $personParams)
    {
        foreach ($data as $k => $params) {
            //Оправим в очередь по урлу стороннего источника, указанного в справочнике сторонних источников
            if (isset($params['source']) && $params['source']->outbound_amazon_queue_name && isset($params['roles']) && !empty($params['roles'])) {
                $message = [
                    'MessageBody' => json_encode([
                        'action' => self::ACTION_EMPLOY,
                        'user' => $personParams,
                        'roles' => $params['roles']
                    ], JSON_UNESCAPED_UNICODE),
                    'QueueUrl' => $this->sqsClient->getSqsQueueUrl($params['source']->outbound_amazon_queue_name)
                ];
                //отправить в Амазон
                $this->sentToAmazon($message);
            }
        }
    }

    /**
     * @param $message
     * @return \Aws\Result|string
     */
    public function sentToAmazon($message)
    {
        $client = $this->getSqsClient();

        try {
            //что делать если все успешно??
            $response = $client->sendMessage($message);
            echo '<pre>' . print_r($response, 1) . '</pre>';
        } catch (AwsException $e) {
            $logger = Yii::$app->get("processingLogger");
            $cronLog = $logger->getLogger([
                'route' => $this->getRoute(),
                'action' => self::TYPE_LOGGER_ACTION,
                'process_id' => getmypid(),
                'QueueUrl' => $message['QueueUrl']
            ]);
            $response = $e->getMessage();
            $cronLog('sent to sqs: ' . $e->getMessage());
        }

        //можем отсылать все обратно в меркурий
        return $response;
    }

    /**
     * Поиск ссылки на меркурий
     * @param $foreign_id
     * @return PersonLink
     */
    public function getPersonLinkMercury($foreign_id)
    {
        return PersonLink::find()
            ->where(['type' => PersonLink::TYPE_MERCURY])
            ->andWhere(['foreign_id' => $foreign_id])
            ->one();
    }

    /**
     * @param $person_id
     * @return Person
     */
    public function getPersonByMercuryLink($person_id)
    {
        return Person::find()
            ->where(['id' => $person_id])
            ->andWhere(['active' => Person::ACTIVE])
            ->one();
    }

    /**
     * Все ссылки кроме меркурия
     * @return PersonLink[]
     */
    public function getOtherPersonLinks()
    {
        return PersonLink::find()
            ->where(['<>', 'type', PersonLink::TYPE_MERCURY])
            ->all();
    }

    /**
     * @param $external_source_user_id
     * @return CallCenterUser
     */
    public function getCallCenterUserByExternalSourceUserId($external_source_user_id)
    {
        return CallCenterUser::find()->where(['user_id' => $external_source_user_id])->one();
    }

    /**
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getExternalSourceByName($name)
    {
        return ExternalSource::find()
            ->where(['name' => $name])
            ->andWhere(['is not', 'outbound_amazon_queue_name', null])
            ->one();
    }

    /**
     * Увольнение сотрудника
     * @param $action
     * @return array
     */
    public function DismissalAction($action)
    {
        echo 'DISMISSAL' . PHP_EOL;
        echo '<pre>' . print_r($action, 1) . '</pre>' . PHP_EOL;
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'action' => self::TYPE_LOGGER_ACTION,
            'process_id' => getmypid(),
            'response' => $action
        ]);

        if (!isset($action['id'])) {
            $error = 'Mercury id is not isset';
        } else {

            $personLinkMercury = $this->getPersonLinkMercury($action['id']);

            if ($personLinkMercury) {
                echo 'PERSON LINK MERCURY #' . $personLinkMercury->id . ' FOUND' . PHP_EOL;
                //Найти физ лицо и залочить его
                $person = $this->getPersonByMercuryLink($personLinkMercury->person_id);

                if ($person) {
                    echo 'PERSON #' . $person->id . ' FOUNDED' . PHP_EOL;
                    $person->active = Person::NOT_ACTIVE;

                    //не смогли выключить даже физ лицо
                    if (!$person->save(false)) {
                        $error = json_encode($person->getErrors(), JSON_UNESCAPED_UNICODE);
                        echo 'PEROSN NOT BLOCKED ON PAY : ' . $error . PHP_EOL;
                    } else {
                        echo 'PEROSN BLOCKED ON PAY' . PHP_EOL;
                        //Необходимо узнать все ссылки на сторонние источники и отправить данные для блокировки пользователя
                        $personLinks = $this->getOtherPersonLinks();

                        foreach ($personLinks as $personLink) {
                            $type = $personLink->type;
                            $external_source_user_id = $personLink->foreign_id;

                            //если был создан call_center_user нужно его залочить
                            $callCenterUser = $this->getCallCenterUserByExternalSourceUserId($external_source_user_id);

                            if ($callCenterUser) {
                                echo 'CALL_CENTER_USER #' . $callCenterUser->id . ' FOUNDED' . PHP_EOL;
                                $callCenterUser->active = CallCenterUser::NOT_ACTIVE;

                                if (!$callCenterUser->save()) {
                                    $error = json_encode($callCenterUser->getErrors(), JSON_UNESCAPED_UNICODE);
                                    echo 'CALL_CENTER_USER NOT BLOCKED' . PHP_EOL;
                                } else {
                                    echo 'CALL_CENTER_USER BLOCKED' . PHP_EOL;
                                    //осталось определить очереди для сторонних источников - и разослать туда простые сообщения
                                    $externalSource = $this->getExternalSourceByName($type);

                                    if ($externalSource) {
                                        //формируем тело сообщения и отсылаем в указанную очередь
                                        $queue = $externalSource->outbound_amazon_queue_name;

                                        echo 'FOUNDED SOURCE "' . $externalSource->name . ' => ' . $queue . PHP_EOL;

                                        $message = [
                                            'MessageBody' => json_encode([
                                                'action' => self::ACTION_DISMISS,
                                                'user_id' => $external_source_user_id,
                                            ], JSON_UNESCAPED_UNICODE),
                                            'QueueUrl' => $this->sqsClient->getSqsQueueUrl($queue)
                                        ];
                                        //отправим данные в очередь источника
                                        $result = $this->sentToAmazon($message);

                                        echo '<pre>' . print_r($message, 1) . '</pre>' . PHP_EOL;
                                        echo '<pre>' . print_r($result, 1) . '</pre>' . PHP_EOL;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    echo 'PERSON NOT FOUND' . PHP_EOL;
                }

            } else {
                $error = 'salary person link not found';
            }
        }

        if (isset($error)) {
            echo 'ERROR : ' . $error . PHP_EOL;
            $cronLog($error);
            $result = [
                'success' => false,
                'error' => $error
            ];
        }

        return isset($result) ? $result : [];
    }

    /**
     * Создадим лин в SalaryPersonLink на Меркурий
     * @param $foreign_id
     * @param $personModel
     * @return array
     */
    public function createPersonLinkMercury($foreign_id, $personModel)
    {
        $salaryPersonLinkModel = new PersonLink();
        $salaryPersonLinkModel->person_id = $personModel->id;
        $salaryPersonLinkModel->type = PersonLink::TYPE_MERCURY;
        $salaryPersonLinkModel->foreign_id = (string)$foreign_id;

        if (!$salaryPersonLinkModel->save()) {
            $result = [
                'success' => false,
                'mercury_id' => $foreign_id,
                'errors' => json_encode($salaryPersonLinkModel->getErrors(), JSON_UNESCAPED_UNICODE)
            ];
        } else {
            $result = [
                'success' => true,
                'mercury_id' => $foreign_id,
                'data' => $salaryPersonLinkModel
            ];
        }

        return $result;
    }

    /**
     * Создадим лин в SalaryPersonLink на CRM
     * @param $data
     * @return array
     */
    public function createPersonLinkCrm($data)
    {
        echo 'CREATE PERSON LINK CRM: ' . PHP_EOL;
        echo '<pre>' . print_r($data, 1) . '</pre>' . PHP_EOL;

        $salaryPersonLinkModel = new PersonLink();
        $salaryPersonLinkModel->person_id = $data['foreign_id'];
        $salaryPersonLinkModel->type = $data['source'];
        $salaryPersonLinkModel->foreign_id = (string)$data['id'];

        if (!$salaryPersonLinkModel->save()) {
            $result = [
                'success' => false,
                'errors' => json_encode($salaryPersonLinkModel->getErrors(), JSON_UNESCAPED_UNICODE)
            ];
            echo 'PERSON LINK NOT CREATED' . PHP_EOL;
            echo '<pre>' . print_r($result, 1) . '</pre>' . PHP_EOL;
        } else {
            $result = [
                'success' => true,
                'data' => $salaryPersonLinkModel
            ];
            echo 'PERSON LINK CREATED' . PHP_EOL;
            echo '<pre>' . print_r($salaryPersonLinkModel->id, 1) . '</pre>' . PHP_EOL;
        }

        return $result;
    }
}