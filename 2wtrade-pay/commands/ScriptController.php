<?php

namespace app\commands;

use app\components\confluence\Confluence;
use app\components\confluence\Page;
use app\components\db\abstracts\ConnectionInterface;
use app\components\db\models\DirtyData;
use app\components\SkuConfigParser;
use app\jobs\CreateBarcodes;
use app\jobs\DeliveryReportDelete;
use app\models\Country;
use app\models\Currency;
use app\models\Product;
use app\models\Source;
use app\modules\callcenter\components\api\Lead;
use app\modules\callcenter\components\NewCallCenterHandler;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestCallData;
use app\modules\callcenter\models\CallCenterRequestHistory;
use app\modules\callcenter\models\CallCenterRequestReserve;
use app\modules\callcenter\models\CallCenterRequestSendResponse;
use app\modules\callcenter\models\CallCenterRequestUpdateResponse;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\crocotime\components\CrocotimeApi;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryChargesCalculator;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryContractFile;
use app\modules\delivery\models\DeliveryPartner;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestReserved;
use app\modules\delivery\models\DeliveryRequestSendResponse;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use app\modules\delivery\models\DeliveryRequestUpdateResponse;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\messenger\models\Message;
use app\modules\order\jobs\OrderCalculateFinanceBalanceJob;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListBarcode;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\InvoiceOrder;
use app\modules\salary\models\Office;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\Staffing;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\console\Controller;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\httpclient\Client;
use app\modules\webhook\controllers\SkypeController;

/**
 * Class ScriptController
 * @package app\commands
 */
class ScriptController extends Controller
{

    const
        CROCOTIME_TOKEN = 'c0ad65a0250f628c226276cae47d2010223940968846ec7dd950b78adb007bcd',
        CROCOTIME_URL = 'http://87.103.246.84:8085',
        CROCOTIME_APP_VERSION = '5.6.2';

    /**
     * @var Client $client
     */
    public $client;

    /**
     * Set Source Form
     */
    public function actionSetSourceForm()
    {
        Console::stdout('Set value source form...' . PHP_EOL);

        $offset = 0;
        $limit = 100;

        $query = Order::find()
            ->with('orderLogStatuses')
            ->where(['source_form' => null])
            ->andWhere(['!=', Order::tableName() . '.id', '19337'])
            ->limit($limit);

        while ($orders = $query->offset($offset)->all()) {
            foreach ($orders as $order) {
                if ($order->status_id == OrderStatus::STATUS_SOURCE_LONG_FORM) {
                    $order->source_form = Order::TYPE_FORM_LONG;
                    $order->save(false, ['source_form']);
                } elseif ($order->status_id == OrderStatus::STATUS_SOURCE_SHORT_FORM) {
                    $order->source_form = Order::TYPE_FORM_SHORT;
                    $order->save(false, ['source_form']);
                } else {
                    if ($order->orderLogStatuses) {
                        $logStatuses = ArrayHelper::getColumn($order->orderLogStatuses, 'old');

                        if (in_array(OrderStatus::STATUS_SOURCE_LONG_FORM, $logStatuses)) {
                            $order->source_form = Order::TYPE_FORM_LONG;
                            $order->save(false, ['source_form']);
                        } elseif (in_array(OrderStatus::STATUS_SOURCE_SHORT_FORM, $logStatuses)) {
                            $order->source_form = Order::TYPE_FORM_SHORT;
                            $order->save(false, ['source_form']);
                        }
                    }
                }
            }

            $offset = $offset + $limit;

            Console::stdout($offset . PHP_EOL);
        }

        Console::stdout('done.' . PHP_EOL);
    }


    /**
     * Получить ID оператора по данным Колцентра для старых заказов
     */
    public function actionGetCallCenterOper($cc_id = null)
    {
        Console::stdout('Getting Oper ID from call-center... ' . PHP_EOL);

        set_time_limit(0);
        $result = Lead::getOperFromCallCenter($cc_id);

        Console::stdout(PHP_EOL . 'Taken records = ' . $result[0] . PHP_EOL);
        Console::stdout('Updated records = ' . $result[1] . PHP_EOL);
        Console::stdout('Not found records = ' . $result[2] . PHP_EOL);
        Console::stdout('done.' . PHP_EOL);
    }


    /**
     * Перекидывание истории обзвона всех заказов в отдельную таблицу
     *
     * @param int $request_limit
     * @param int $batch_size
     * @throws \yii\mongodb\Exception
     */
    public function actionParseCallHistoryIntoTable($request_limit = 1000, $batch_size = 100)
    {
        $query = CallCenterRequest::find()->select([
            CallCenterRequest::tableName() . '.id',
            CallCenterRequest::tableName() . '.call_center_id'
        ])->asArray();
        $query->leftJoin('call_center_request_history', '`call_center_request_history`.`call_center_request_id` = ' . CallCenterRequest::tableName() . '.id');
        $query->andWhere(['is not', CallCenterRequest::tableName() . '.cc_update_response_at', null]);
        $query->andWhere(['is', 'call_center_request_history.id', null]);

        $query->orderBy([CallCenterRequest::tableName() . '.id' => SORT_DESC]);

        $counter = 0;
        foreach ($query->batch($batch_size) as $requests) {
            $rows = [];
            $lastUpdateResponses = ArrayHelper::map(CallCenterRequestUpdateResponse::find()
                ->where(['call_center_request_id' => array_map('intval', ArrayHelper::getColumn($requests, 'id'))])
                ->lastResponses(), 'call_center_request_id', 'response');
            foreach ($requests as $request) {
                if (!isset($lastUpdateResponses[$request['id']])) {
                    continue;
                }
                $response = json_decode($lastUpdateResponses[$request['id']], true);
                $history = $response['history'];
                unset($response);
                foreach ($history as $historyItem) {
                    $rows[] = [
                        'call_center_request_id' => $request['id'],
                        'call_center_id' => $request['call_center_id'],
                        'operator_id' => $historyItem['oper'],
                        'foreign_status' => $historyItem['status_id'],
                        'called_at' => strtotime($historyItem['oe_changed']),
                    ];
                }
                if (count($rows) > 1000) {
                    \Yii::$app->db->createCommand()
                        ->batchInsert('call_center_request_history', [
                            'call_center_request_id',
                            'call_center_id',
                            'operator_id',
                            'foreign_status',
                            'called_at'
                        ], $rows)
                        ->execute();
                    $rows = [];
                }
            }
            if (count($rows) > 0) {
                \Yii::$app->db->createCommand()->batchInsert('call_center_request_history', [
                    'call_center_request_id',
                    'call_center_id',
                    'operator_id',
                    'foreign_status',
                    'called_at'
                ], $rows)->execute();
            }
            $counter += count($requests);
            if ($counter >= $request_limit) {
                unset($requests);
                break;
            }
        }
    }

    /**
     * Взять заявки КЦ за последний месяц
     * вытащить их историю какой оператор апрувнул заказ и обновить ID оператора в поле last_foreign_operator
     * NPAY-1515
     *
     * @param int $days число дней в прошлое
     */
    public function actionUpdateLastForeignOperator($days = 31)
    {
        $offset = 0;
        $limit = 1000;
        $repeat = true;

        $ccApproveStatuses = [];
        foreach (Lead::getMapStatuses() as $key => $statusId) {
            if ($statusId == OrderStatus::STATUS_CC_APPROVED) {
                $ccApproveStatuses[] = $key;
            }
        }

        $total = 0;
        $updated = 0;
        $errors = 0;
        while ($repeat) {

            $subQuery = new Query();
            $subQuery->select([
                'max_called_at' => 'max(called_at)',
                'call_center_request_id'
            ])
                ->from(CallCenterRequestHistory::tableName())
                ->where([
                    CallCenterRequestHistory::tableName() . '.foreign_status' => $ccApproveStatuses
                ])
                ->groupBy(
                    CallCenterRequestHistory::tableName() . '.call_center_request_id'
                );

            $requests = CallCenterRequestHistory::find()
                ->innerJoin(['sub' => $subQuery],
                    CallCenterRequestHistory::tableName() . '.call_center_request_id=sub.call_center_request_id and ' . CallCenterRequestHistory::tableName() . '.called_at=sub.max_called_at')
                ->leftJoin(CallCenterRequest::tableName(),
                    CallCenterRequestHistory::tableName() . '.call_center_request_id=' . CallCenterRequest::tableName() . '.id')
                ->where([
                    CallCenterRequestHistory::tableName() . '.foreign_status' => $ccApproveStatuses
                ])
                ->andWhere(['>=', CallCenterRequest::tableName() . '.created_at', strtotime('-' . $days . ' days')])
                ->limit($limit)
                ->offset($offset)
                ->all();

            foreach ($requests as $request) {
                $total++;
                if ($request->callCenterRequest->last_foreign_operator != $request->operator_id) {
                    $request->callCenterRequest->last_foreign_operator = $request->operator_id;
                    if ($request->callCenterRequest->save(true, ['last_foreign_operator'])) {
                        $updated++;
                    } else {
                        $errors++;
                    }
                }
            }

            if (empty($requests)) {
                $repeat = false;
            }

            $offset += $limit;
        }
        echo 'total = ' . $total . PHP_EOL;
        echo 'updated = ' . $updated . PHP_EOL;
        echo 'errors = ' . $errors . PHP_EOL;
    }

    public function actionUpdateFillInformationAboutAutocheckAddress($cc_id = null, $time_from = null, $time_to = null, $limit = 10000)
    {
        $query = CallCenterRequest::find()
            ->where([CallCenterRequest::tableName() . '.status' => 'done'])
            ->andWhere(['is', CallCenterRequest::tableName() . '.autocheck_address', null]);
        $query->andWhere(['is not', CallCenterRequest::tableName() . '.cc_update_response_at', null]);
        if (!empty($cc_id)) {
            $query->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $cc_id]);
        }

        if (!empty($time_from)) {
            $query->andWhere(['>=', CallCenterRequest::tableName() . '.created_at', strtotime($time_from)]);
        }

        if (!empty($time_to)) {
            $query->andWhere(['<=', CallCenterRequest::tableName() . '.created_at', strtotime($time_to)]);
        }

        foreach ($query->batch(100) as $requests) {
            /** @var CallCenterRequest $request */
            foreach ($requests as $request) {
                if ($request->lastUpdateResponse) {
                    $response = json_decode($request->lastUpdateResponse->response, true);
                    if (isset($response['autocheck_address'])) {
                        $request->autocheck_address = $response['autocheck_address'];
                        $request->save(false, ['autocheck_address']);
                    }
                }
            }
        }
    }

    /**
     * Переносит отдел с сотрудиками из CallCenterOffice в Crocotime
     * @param $officeName
     * @return bool
     */
    public function actionCreateCrocotimeOffice($officeName)
    {
        if (!is_string($officeName)) {
            $this->stdout("Wrong id! \n", Console::FG_RED);
            return false;
        }

        $this->client = new Client();
        try {
            if (!$office = Office::find()->byName($officeName)->one()) {
                $this->stdout('CallCenterOffice ' . $officeName . ' не найден!', Console::BOLD);
                return false;
            };
            $departmentId = $this->addGroup($officeName, $office);
            $this->addEmployees($office, $departmentId);
        } catch (\Exception $e) {
            $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
            //$cronLog($e->getMessage());
        }
        return true;
    }

    /**
     * Добавляет отдел в Crocotime
     * @param  string $name
     * @param Office $office
     * @return null
     * @throws \Exception
     */
    private function addGroup($name, $office)
    {
        $post = [
            "server_token" => self::CROCOTIME_TOKEN,
            "app_version" => self::CROCOTIME_APP_VERSION,
            "controller" => "WorkspaceActionController",
            "action" => "insert",
            "query" => [
                "domain" => "departments",
                "record" => [
                    "display_name" => $name,
                    "parent_group_id" => -1,
                    "schedule_id" => 3,
                ]
            ]
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl(self::CROCOTIME_URL)
            ->setFormat('json')
            ->setData($post)
            ->send();

        print_r($response->content);
        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !isset($result->result->affected_ids)
        ) {
            throw new \Exception($response->content);
        }
        $this->stdout('CallCenterOffice создан ' . $result->result->affected_ids[0] . PHP_EOL, Console::FG_PURPLE);
        $office->crocotime_parent_group_id = $result->result->affected_ids[0];
        if (!$office->save()) {
            throw new \Exception($office->getFirstErrorAsString());
        }
        return $result->result->affected_ids[0];
    }

    /**
     * Добавляет сотрудников в Crocotime
     * @param Office $office
     * @param $departmentId
     * @throws \Exception
     */
    private function addEmployees($office, $departmentId)
    {
        $personal = $office->people;
        $timeZone = $office->country->timezone->timezone_id;
        foreach ($personal as $person) {

            $saveResult = $person->savePersonLinkCrocoTime($this->addEmployee($person->name, $departmentId, $timeZone));
            if ($saveResult !== true) {
                throw new \Exception($saveResult);
            };
        }
    }

    /**
     * Добавляет сотрудника в Crocotime
     * @param $fullName
     * @param $timeZone
     * @throws \Exception
     */
    private function addEmployee($fullName, $departmentId, $timeZone)
    {
        $fullName = $this->splitFullName($fullName);
        $firstName = $fullName['first_name'];
        $secondName = $fullName['second_name'];

        $post = [
            "server_token" => self::CROCOTIME_TOKEN,
            "app_version" => self::CROCOTIME_APP_VERSION,
            "controller" => "WorkspaceActionController",
            "action" => "insert",
            "query" => [
                "domain" => "employees",
                "record" => [
                    "parent_group_id" => $departmentId,
                    "privilege" => 0,
                    "schedule_id" => 3,
                    "first_name" => $firstName,
                    "second_name" => $secondName,
                    "position_id" => 1,
                    "time_zone" => $timeZone,
                    "screenshot_period" => 300
                ]
            ]
        ];

        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl(self::CROCOTIME_URL)
            ->setFormat('json')
            ->setData($post)
            ->send();

        if (!$response->isOk
            || !($result = json_decode($response->content))
            || !isset($result->result->affected_ids)
        ) {
            throw new \Exception($response->content);
        }
        $this->stdout('Person создан ' . $result->result->affected_ids[0] . PHP_EOL, Console::BOLD);
        return $result->result->affected_ids[0];
    }

    /**
     * @param $fullName
     * @return array
     */
    private function splitFullName($fullName)
    {
        $nameArray = explode(' ', $fullName);
        return [
            'first_name' => $nameArray[0],
            'second_name' => implode(' ', array_slice($nameArray, 1)),
        ];
    }

    /**
     * Парсит ску из конфигов курьерок в базу
     * @TODO Удалить после парсинга сам метод и всё сопутстсвующее
     */
    public function actionConfigParser()
    {
        $deliveryClassNames = [
            'PfcApi',
            'aCommerceApi',
            'BizApi',
            'BluedartFulfillment',
            'TrafLeadApi',
            'UPS'
        ];
        try {
            foreach ($deliveryClassNames as $className) {
                $skuConfigParser = new SkuConfigParser($className);
                $this->stdout("$className parsing starting..." . PHP_EOL, Console::FG_GREEN);
                $skuConfigParser->parse();
                if ($skuConfigParser->errors) {
                    $this->stdout(print_r($skuConfigParser->errors, true) . PHP_EOL, Console::FG_PURPLE);
                }
            }
        } catch (\Exception $exception) {
            $this->stdout(print_r($skuConfigParser->errors, true) . PHP_EOL, Console::FG_PURPLE);
            $this->stdout($exception->getMessage() . PHP_EOL, Console::FG_RED);
        }
    }

    /**
     * Правка продуктов у заказов, при несовпадении с тем, что пришло из КЦ
     * @param $time_from - timestamp с которого необходимо начать проверку
     * @param null $cc_id
     */
    public function actionUpdateOrderProducts($time_from, $cc_id = null)
    {
        $query = CallCenterRequest::find()
            ->with(['order.orderProducts'])
            ->where([CallCenterRequest::tableName() . '.status' => 'done'])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.cron_launched_at', $time_from]);
        $query->andWhere(['is not', CallCenterRequest::tableName() . '.cc_update_response_at', null]);
        if (!empty($cc_id)) {
            $query->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $cc_id]);
        }

        foreach ($query->batch(500) as $requests) {
            /** @var CallCenterRequest $request */
            foreach ($requests as $request) {
                if ($request->lastUpdateResponse) {
                    $response = json_decode($request->lastUpdateResponse->response, true);
                    if (count($request->order->orderProducts) != count($response['products'])) {
                        // Пересохранение товаров
                        foreach ($request->order->orderProducts as $orderProduct) {
                            $orderProduct->delete();
                        }

                        foreach ($response['products'] as $product) {
                            $orderProduct = new OrderProduct();
                            $orderProduct->order_id = $request->order->id;
                            $orderProduct->product_id = $product['product_id'];
                            $orderProduct->price = $product['price'];
                            $orderProduct->quantity = $product['quantity'];
                            $orderProduct->save();
                        }
                    }
                }
            }
        }
    }

    public function actionConfluenceStructure()
    {
        try {
            $page = new Page();
            $page->title = "Countries";
            $page->body = (object)[
                'storage' => (object)[
                    'value' => '<p>Information about delivery services, call centers etc.</p>',
                    'representation' => 'storage'
                ]
            ];
            //$page->body->storage->value = 'Information about delivery services, call centers etc.';
            $confluence = new Confluence();
            $mainPageId = $confluence->createPage($page);
            echo "Countries start" . PHP_EOL;
            $countryPageIds = $confluence->createCountriesPage($mainPageId);
            echo "Delivery start" . PHP_EOL;
            $confluence->createDeliveriesPage($countryPageIds);
            echo "CC start" . PHP_EOL;
            $confluence->createCallCentersPage($countryPageIds);
            echo PHP_EOL;
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public function addGroups()
    {
        try {
            $confluence = new Confluence();
            $confluence->addGroupsForCountries();
        } catch (\Exception $exception) {

        }
    }

    /**
     * @param $created_from
     */
    public function actionCopyDeliveryRequestsToReserve($created_from)
    {
        $query = DeliveryRequest::find();
        if (!empty($created_from)) {
            $query->andWhere(['>', 'created_at', $created_from]);
        }

        $total = $query->count();
        echo "Total {$total} requests" . PHP_EOL;
        $count = 0;
        foreach ($query->batch(500) as $requests) {
            $reserveRequests = DeliveryRequestReserved::find()
                ->where(['order_id' => ArrayHelper::getColumn($requests, 'order_id')])
                ->indexBy('order_id')
                ->all();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($requests as $request) {
                    if (!isset($reserveRequests[$request->order_id])) {
                        $reserveRequests[$request->order_id] = new DeliveryRequestReserved();
                    }
                    $reserveRequest = $reserveRequests[$request->order_id];
                    if ($reserveRequest->copyAttributes($request)) {
                        $reserveRequest->enableLog = false;
                        $reserveRequest->save(false);
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                echo $e->getMessage() . PHP_EOL;
                exit();
            }
            $count += count($requests);
            echo "{$count} of {$total}" . PHP_EOL;
        }
    }

    /**
     * @param $created_from
     */
    public function actionCopyCallCenterRequestsToReserve($created_from)
    {
        $query = CallCenterRequest::find();
        if (!empty($created_from)) {
            $query->andWhere(['>', 'created_at', $created_from]);
        }

        $total = $query->count();
        echo "Total {$total} requests" . PHP_EOL;
        $count = 0;
        foreach ($query->batch(500) as $requests) {
            $reserveRequests = CallCenterRequestReserve::find()
                ->where(['id' => ArrayHelper::getColumn($requests, 'id')])
                ->indexBy('id')
                ->all();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($requests as $request) {
                    if (!isset($reserveRequests[$request->id])) {
                        $reserveRequests[$request->id] = new CallCenterRequestReserve();
                    }
                    $reserveRequest = $reserveRequests[$request->id];
                    if ($reserveRequest->copyAttributes($request)) {
                        $reserveRequest->enableLog = false;
                        $reserveRequest->save(false);
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                echo $e->getMessage() . PHP_EOL;
                exit();
            }
            $count += count($requests);
            echo "{$count} of {$total}" . PHP_EOL;
        }
    }

    public function actionCopyDeliveryRequestsFromReserve($created_from)
    {
        $query = DeliveryRequestReserved::find();
        if (!empty($created_from)) {
            $query->andWhere(['>', 'created_at', $created_from]);
        }
        $total = $query->count();
        echo "Total {$total} requests" . PHP_EOL;
        $count = 0;
        foreach ($query->batch(500) as $requests) {
            $reserveRequests = DeliveryRequest::find()
                ->where(['order_id' => ArrayHelper::getColumn($requests, 'order_id')])
                ->indexBy('order_id')
                ->all();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($requests as $request) {
                    if (!isset($reserveRequests[$request->order_id])) {
                        $reserveRequests[$request->order_id] = new DeliveryRequest();
                    }
                    $reserveRequest = $reserveRequests[$request->order_id];
                    if ($reserveRequest->copyAttributes($request)) {
                        $reserveRequest->enableLog = false;
                        $reserveRequest->save(false);
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                echo $e->getMessage() . PHP_EOL;
                exit();
            }
            $count += count($requests);
            echo "{$count} of {$total}" . PHP_EOL;
        }
    }

    /**
     * @param $created_from
     */
    public function actionCopyCallCenterRequestsFromReserve($created_from)
    {
        $query = CallCenterRequestReserve::find();
        if (!empty($created_from)) {
            $query->andWhere(['>', 'created_at', $created_from]);
        }

        $total = $query->count();
        echo "Total {$total} requests" . PHP_EOL;
        $count = 0;
        foreach ($query->batch(500) as $requests) {
            $reserveRequests = CallCenterRequest::find()
                ->where(['id' => ArrayHelper::getColumn($requests, 'id')])
                ->indexBy('id')
                ->all();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($requests as $request) {
                    if (!isset($reserveRequests[$request->id])) {
                        $reserveRequests[$request->id] = new CallCenterRequest();
                    }
                    $reserveRequest = $reserveRequests[$request->id];
                    if ($reserveRequest->copyAttributes($request)) {
                        $reserveRequest->enableLog = false;
                        $reserveRequest->save(false);
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                echo $e->getMessage() . PHP_EOL;
                exit();
            }
            $count += count($requests);
            echo "{$count} of {$total}" . PHP_EOL;
        }
    }

    /**
     * @param $created_from
     */
    public function actionCopyDataStorage()
    {
        Yii::$app->db->createCommand()->truncateTable(StorageProduct::tableName());

        $storageParts = StoragePart::find()
            ->select([
                'storage_id' => StoragePart::tableName() . '.storage_id',
                'product_id' => StoragePart::tableName() . '.product_id',
                'balance' => 'SUM(' . StoragePart::tableName() . '.balance)',
            ])
            ->groupBy([StoragePart::tableName() . '.storage_id', StoragePart::tableName() . '.product_id'])
            ->asArray()
            ->all();

        $arrayForInsert = null;
        foreach ($storageParts as $storagePart) {
            $arrayForInsert[] = [
                $storagePart['storage_id'],
                $storagePart['product_id'],
                $storagePart['balance'],
                time(),
                time()
            ];
        }

        if (!empty($arrayForInsert)) {
            Yii::$app->db->createCommand()->batchInsert(
                StorageProduct::tableName(),
                [
                    'storage_id',
                    'product_id',
                    'balance',
                    'created_at',
                    'updated_at'
                ],
                $arrayForInsert
            )->execute();
        }
        unset($arrayForInsert);
    }


    /**
     * Загрузить данные по операторам Испании
     *
     * @param $callCenterId integer
     * @param $month integer
     * @param $year integer
     */
    public function actionSpainOperators($callCenterId = 6, $month = 9, $year = 2017)
    {
        $callCenter = CallCenter::findOne($callCenterId);
        if (!$callCenter) {
            Console::stdout('no callCenter found.' . PHP_EOL);
            die();
        }

        $dataWorkTime = [
            [4, 'CamilaForero', 19213],
            [5, 'CamilaForero', 27134],
            [6, 'CamilaForero', 17682],
            [7, 'CamilaForero', 26872],
            [8, 'CamilaForero', 26979],
            [10, 'CamilaForero', 23584],
            [11, 'CamilaForero', 27445],
            [13, 'CamilaForero', 26721],
            [14, 'CamilaForero', 27537],
            [15, 'CamilaForero', 26901],
            [16, 'CamilaForero', 26672],
            [18, 'CamilaForero', 27204],
            [19, 'CamilaForero', 22979],
            [20, 'CamilaForero', 27192],
            [21, 'CamilaForero', 24195],
            [22, 'CamilaForero', 30941],
            [24, 'CamilaForero', 19615],
            [25, 'CamilaForero', 6390],
            [6, 'F.Arnold', 17805],
            [7, 'F.Arnold', 26789],
            [8, 'F.Arnold', 27103],
            [10, 'F.Arnold', 22915],
            [11, 'F.Arnold', 27064],
            [12, 'F.Arnold', 27069],
            [13, 'F.Arnold', 27057],
            [15, 'F.Arnold', 25598],
            [16, 'F.Arnold', 26341],
            [18, 'F.Arnold', 26808],
            [19, 'F.Arnold', 21745],
            [20, 'F.Arnold', 26832],
            [21, 'F.Arnold', 24074],
            [22, 'F.Arnold', 26621],
            [24, 'F.Arnold', 26494],
            [25, 'F.Arnold', 6397],
            [6, 'G.Julian', 16402],
            [7, 'G.Julian', 26236],
            [8, 'G.Julian', 26270],
            [9, 'G.Julian', 22851],
            [11, 'G.Julian', 26900],
            [12, 'G.Julian', 26684],
            [13, 'G.Julian', 26423],
            [14, 'G.Julian', 26511],
            [15, 'G.Julian', 26409],
            [17, 'G.Julian', 22487],
            [18, 'G.Julian', 26704],
            [19, 'G.Julian', 22589],
            [20, 'G.Julian', 27125],
            [21, 'G.Julian', 24734],
            [22, 'G.Julian', 27174],
            [23, 'G.Julian', 28120],
            [25, 'G.Julian', 6471],
            [6, 'G.Sara', 17589],
            [7, 'G.Sara', 10565],
            [8, 'G.Sara', 26791],
            [9, 'G.Sara', 20330],
            [12, 'G.Sara', 24942],
            [13, 'G.Sara', 23030],
            [14, 'G.Sara', 24890],
            [18, 'G.Sara', 26328],
            [19, 'G.Sara', 17635],
            [21, 'G.Sara', 23620],
            [22, 'G.Sara', 25626],
            [23, 'G.Sara', 25405],
            [24, 'G.Sara', 22754],
            [25, 'G.Sara', 6549],
            [7, 'J.Mendoza', 9672],
            [8, 'J.Mendoza', 26569],
            [10, 'J.Mendoza', 23309],
            [11, 'J.Mendoza', 25969],
            [12, 'J.Mendoza', 26377],
            [13, 'J.Mendoza', 26858],
            [14, 'J.Mendoza', 28699],
            [15, 'J.Mendoza', 26619],
            [16, 'J.Mendoza', 25366],
            [18, 'J.Mendoza', 26236],
            [19, 'J.Mendoza', 23584],
            [20, 'J.Mendoza', 26906],
            [21, 'J.Mendoza', 23961],
            [22, 'J.Mendoza', 27399],
            [24, 'J.Mendoza', 22976],
            [25, 'J.Mendoza', 6486],
            [5, 'J.Ricardo', 5805],
            [6, 'J.Ricardo', 25089],
            [7, 'J.Ricardo', 26445],
            [8, 'J.Ricardo', 25443],
            [9, 'J.Ricardo', 23240],
            [11, 'J.Ricardo', 25642],
            [12, 'J.Ricardo', 27047],
            [13, 'J.Ricardo', 26646],
            [14, 'J.Ricardo', 26113],
            [15, 'J.Ricardo', 23704],
            [16, 'J.Ricardo', 26590],
            [18, 'J.Ricardo', 26772],
            [19, 'J.Ricardo', 17382],
            [20, 'J.Ricardo', 26019],
            [21, 'J.Ricardo', 25834],
            [22, 'J.Ricardo', 27059],
            [24, 'J.Ricardo', 27816],
            [20, 'LauraRodriguez', 23862],
            [21, 'LauraRodriguez', 22922],
            [22, 'LauraRodriguez', 26218],
            [23, 'LauraRodriguez', 25834],
            [25, 'LauraRodriguez', 5746],
            [5, 'LolaMaretes', 24027],
            [6, 'LolaMaretes', 8624],
            [7, 'LolaMaretes', 25527],
            [8, 'LolaMaretes', 27012],
            [9, 'LolaMaretes', 20313],
            [12, 'LolaMaretes', 20160],
            [13, 'LolaMaretes', 24637],
            [14, 'LolaMaretes', 20488],
            [15, 'LolaMaretes', 19805],
            [17, 'LolaMaretes', 20331],
            [18, 'LolaMaretes', 19678],
            [19, 'LolaMaretes', 21803],
            [20, 'LolaMaretes', 27536],
            [21, 'LolaMaretes', 24023],
            [22, 'LolaMaretes', 30309],
            [23, 'LolaMaretes', 26319],
            [25, 'LolaMaretes', 6345],
            [7, 'M.Scarpetta', 9052],
            [8, 'M.Scarpetta', 25629],
            [9, 'M.Scarpetta', 22207],
            [11, 'M.Scarpetta', 26606],
            [12, 'M.Scarpetta', 27522],
            [13, 'M.Scarpetta', 25816],
            [14, 'M.Scarpetta', 26132],
            [15, 'M.Scarpetta', 26453],
            [16, 'M.Scarpetta', 25702],
            [18, 'M.Scarpetta', 26161],
            [19, 'M.Scarpetta', 22356],
            [21, 'M.Scarpetta', 24329],
            [22, 'M.Scarpetta', 26817],
            [23, 'M.Scarpetta', 23307],
            [24, 'M.Scarpetta', 23251],
            [25, 'M.Scarpetta', 6762],
            [4, 'OlgaMelo', 23149],
            [5, 'OlgaMelo', 28023],
            [6, 'OlgaMelo', 26095],
            [7, 'OlgaMelo', 25794],
            [8, 'OlgaMelo', 26974],
            [10, 'OlgaMelo', 23249],
            [11, 'OlgaMelo', 27052],
            [12, 'OlgaMelo', 14181],
            [13, 'OlgaMelo', 25816],
            [14, 'OlgaMelo', 27705],
            [15, 'OlgaMelo', 26447],
            [16, 'OlgaMelo', 26050],
            [18, 'OlgaMelo', 27256],
            [19, 'OlgaMelo', 23220],
            [20, 'OlgaMelo', 27414],
            [21, 'OlgaMelo', 24474],
            [22, 'OlgaMelo', 27685],
            [24, 'OlgaMelo', 22550],
            [25, 'OlgaMelo', 6359],
            [6, 'P.Gloria', 17672],
            [7, 'P.Gloria', 25137],
            [8, 'P.Gloria', 26417],
            [9, 'P.Gloria', 23802],
            [11, 'P.Gloria', 26609],
            [12, 'P.Gloria', 27044],
            [13, 'P.Gloria', 26528],
            [18, 'P.Gloria', 26798],
            [6, 'R.Lidys', 15988],
            [7, 'R.Lidys', 20218],
            [8, 'R.Lidys', 24482],
            [9, 'R.Lidys', 23403],
            [11, 'R.Lidys', 26339],
            [12, 'R.Lidys', 26056],
            [13, 'R.Lidys', 26989],
            [14, 'R.Lidys', 26872],
            [16, 'R.Lidys', 26583],
            [18, 'R.Lidys', 22486],
            [19, 'R.Lidys', 23691],
            [20, 'R.Lidys', 27431],
            [21, 'R.Lidys', 23817],
            [22, 'R.Lidys', 26911],
            [23, 'R.Lidys', 26300],
            [25, 'R.Lidys', 2185],
            [6, 'S.Paula', 17358],
            [7, 'S.Paula', 25975],
            [8, 'S.Paula', 27192],
            [10, 'S.Paula', 22987],
            [11, 'S.Paula', 26975],
            [12, 'S.Paula', 25554],
            [13, 'S.Paula', 26109],
            [15, 'S.Paula', 25886],
            [18, 'S.Paula', 26755],
            [19, 'S.Paula', 23549],
            [20, 'S.Paula', 27504],
            [21, 'S.Paula', 23899],
            [22, 'S.Paula', 26815],
            [24, 'S.Paula', 23618],
            [25, 'S.Paula', 2503],
            [7, 'Y.Colorado', 10592],
            [8, 'Y.Colorado', 26933],
            [10, 'Y.Colorado', 23252],
            [11, 'Y.Colorado', 27572],
            [12, 'Y.Colorado', 27557],
            [13, 'Y.Colorado', 25435],
            [14, 'Y.Colorado', 26695],
            [15, 'Y.Colorado', 25668],
            [16, 'Y.Colorado', 22628],
            [18, 'Y.Colorado', 23057],
            [19, 'Y.Colorado', 23457],
            [20, 'Y.Colorado', 27137],
            [21, 'Y.Colorado', 23592],
            [22, 'Y.Colorado', 28029],
            [23, 'Y.Colorado', 26655],
            [25, 'Y.Colorado', 5969],
            [4, 'YohanaValencia', 96],
            [5, 'YohanaValencia', 22],
            [6, 'YohanaValencia', 31],
            [9, 'YohanaValencia', 71],
            [12, 'YohanaValencia', 36],
            [13, 'YohanaValencia', 21],
            [14, 'YohanaValencia', 2336],
            [15, 'YohanaValencia', 7958],
            [18, 'YohanaValencia', 15],
            [22, 'YohanaValencia', 8554],
        ];

        $createdUsers = 0;
        $savedTimes = 0;
        $errors = [];
        foreach ($dataWorkTime as $item) {
            $login = $item[1];
            $date = date('Y-m-d', strtotime($year . '-' . $month . '-' . $item[0]));
            $time = $item[2];
            $callCenterUser = CallCenterUser::find()->where(
                [
                    'user_login' => $login,
                    'callcenter_id' => $callCenter->id
                ])->one();
            if (!$callCenterUser) {
                $callCenterUser = new CallCenterUser();
                $callCenterUser->callcenter_id = $callCenter->id;
                $callCenterUser->active = 1;
                $callCenterUser->user_login = $login;
                if ($callCenterUser->save()) {
                    $createdUsers++;
                } else {
                    $errors[] = $callCenter->getFirstErrorAsString();
                }
            }
            if ($callCenterUser) {
                $callCenterWorkTime = CallCenterWorkTime::find()->where(
                    [
                        'call_center_user_id' => $callCenterUser->id,
                        'date' => $date
                    ])->one();
                if (!$callCenterWorkTime) {
                    $callCenterWorkTime = new CallCenterWorkTime();
                    $callCenterWorkTime->call_center_user_id = $callCenterUser->id;
                    $callCenterWorkTime->date = $date;
                }
                $callCenterWorkTime->time = $time;
                if ($callCenterWorkTime->save()) {
                    $savedTimes++;
                } else {
                    $errors[] = $callCenterWorkTime->getFirstErrorAsString();
                }
            }
        }

        Console::stdout('createdUsers ' . $createdUsers . PHP_EOL);
        Console::stdout('savedTimes ' . $savedTimes . PHP_EOL);
        if ($errors) {
            Console::stdout('errors ' . print_r($errors, true) . PHP_EOL);
        }
    }


    /**
     * Заполнить таблицу delivery_request_unbuyout_reason
     *
     * @param $fromDate string
     * @param $batchSize integer
     */
    public function actionFillUnBuyoutReason($fromDate = null, $batchSize = 500)
    {
        set_time_limit(0);

        if (!$fromDate) {
            $fromDate = '2017-05-05';   // когда добавили unshipping_reason
        }
        $from = strtotime($fromDate);

        $reasons = ArrayHelper::map(
            UnBuyoutReasonMapping::find()
                ->asArray()
                ->all(), 'foreign_reason', 'id'
        );

        $deliveryRequestsQuery =
            DeliveryRequest::find()
                ->where([
                    'and',
                    ['>=', DeliveryRequest::tableName() . '.created_at', $from],
                    ['is not', DeliveryRequest::tableName() . '.unshipping_reason', null],
                    ['<>', DeliveryRequest::tableName() . '.unshipping_reason', '']
                ]);

        $errors = [];
        $dataDelete = [];
        $dataInsert = [];
        foreach ($deliveryRequestsQuery->batch($batchSize) as $requests) {
            $dataDelete = [];
            $dataInsert = [];
            $batchErrors = [];
            foreach ($requests as $request) {
                $reasonID = 0;
                if (isset($reasons[$request->unshipping_reason])) {
                    $reasonID = $reasons[$request->unshipping_reason];
                } else {
                    $new = new UnBuyoutReasonMapping();
                    $new->foreign_reason = $request->unshipping_reason;
                    if ($new->save()) {
                        $reasonID = $new->id;
                        $reasons[$request->unshipping_reason] = $reasonID;
                    } else {
                        $errors[] = $new->getFirstErrorAsString();
                        $batchErrors[] = $new->getFirstErrorAsString();
                    }
                }
                if ($reasonID) {
                    $dataDelete[] = $request->id;
                    $dataInsert[] = [
                        'delivery_request_id' => $request->id,
                        'reason_mapping_id' => (int)$reasonID,
                    ];
                }
            }
            if ($batchErrors) {
                Console::stdout('errors ' . print_r($batchErrors, true) . PHP_EOL);
            }
            if ($dataInsert) {
                DeliveryRequestUnBuyoutReason::deleteAll([
                    'delivery_request_id' => $dataDelete
                ]);

                Yii::$app->db->createCommand()->batchInsert(
                    DeliveryRequestUnBuyoutReason::tableName(),
                    [
                        'delivery_request_id',
                        'reason_mapping_id',
                    ],
                    $dataInsert
                )->execute();
            }
        }

        Console::stdout('size of dataDelete ' . sizeof($dataDelete) . PHP_EOL);
        Console::stdout('size of dataInsert ' . sizeof($dataInsert) . PHP_EOL);

        if ($errors) {
            Console::stdout('errors ' . print_r($errors, true) . PHP_EOL);
        }
    }

    /**
     * @param null $delivery_id
     * @param null $time_from
     * @param null $time_to
     */
    public function actionCalculateDeliveryChargesForFinishedOrders($delivery_id = null, $time_from = null, $time_to = null)
    {
        date_default_timezone_set('UTC');

        $query = DeliveryContract::find()
            ->innerJoinWith('chargesCalculatorModel', false)
            ->where([DeliveryChargesCalculator::tableName() . '.active' => 1]);
        $query->with(['delivery', 'chargesCalculatorModel']);
        if ($delivery_id) {
            $query->andWhere([DeliveryContract::tableName() . '.delivery_id' => $delivery_id]);
        }

        $contracts = $query->all();

        $closedReports = DeliveryReport::find()
            ->joinWith('records', false)
            ->select('order_id')
            ->where(['record_status' => 'status_updated'])
            ->andWhere([DeliveryReport::tableName() . '.type' => 'financial'])
            ->andWhere([DeliveryReport::tableName() . '.status' => 'closed'])
            ->andWhere([DeliveryReportRecord::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')]);

        try {
            if (is_array($contracts)) {
                foreach ($contracts as $contract) {
                    echo "Start calculating for {$contract->delivery->country->name} - {$contract->delivery->name} - #{$contract->id}" . PHP_EOL;
                    $chargesCalculator = $contract->getChargesCalculator();

                    $query = Order::find()
                        ->innerJoinWith(['deliveryRequest'])
                        ->where([DeliveryRequest::tableName() . '.status' => DeliveryRequest::STATUS_DONE]);
                    $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $contract->delivery_id]);
                    $query->andWhere(['not exists', $closedReports]);
                    $query->with(['deliveryRequest', 'callCenterRequest', 'orderProducts', 'financePrediction']);
                    $query->joinWith(['financePrediction.invoiceOrder'], false);
                    $query->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null]);
                    if ($time_from) {
                        $query->andWhere(['>=', DeliveryRequest::tableName() . '.created_at', $time_from]);
                    }

                    if ($time_to) {
                        $query->andWhere(['<=', DeliveryRequest::tableName() . '.created_at', $time_to]);
                    }
                    if ($contract->date_from) {
                        $query->andWhere([
                            '>=',
                            'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                            strtotime('midnight', strtotime($contract->date_from))
                        ]);
                    }
                    if ($contract->date_to) {
                        $query->andWhere([
                            '<=',
                            'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                            strtotime('23:59:59', strtotime($contract->date_to))
                        ]);
                    }
                    $query->with(['deliveryRequest', 'callCenterRequest', 'orderProducts', 'financePrediction']);

                    $chargesCalculator->calculateForBatch($query);

                    echo "{$contract->delivery->country->name} - {$contract->delivery->name} - #{$contract->id} has finished" . PHP_EOL;
                }
            }
        } catch (\Throwable $e) {
            echo "Exception " . $e->getMessage() . PHP_EOL . PHP_EOL;
            echo $e->getTraceAsString() . PHP_EOL . PHP_EOL;
        }
    }

    /**
     * @param integer $cc_id
     * @param null $timeFrom // timestamp по дате апрува
     * @param null $timeTo // timestamp по дате апрува
     * @throws \Exception
     */
    public function actionUpdateNewCCOrdersAddress($cc_id, $timeFrom = null, $timeTo = null)
    {
        $query = CallCenterRequest::find()->joinWith('order')
            ->where([CallCenterRequest::tableName() . '.call_center_id' => $cc_id])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getApproveList()]);
        if ($timeFrom) {
            $query->andWhere(['>=', CallCenterRequest::tableName() . '.approved_at', $timeFrom]);
        }

        if ($timeTo) {
            $query->andWhere(['<=', CallCenterRequest::tableName() . '.approved_at', $timeTo]);
        }

        foreach ($query->batch(500) as $requests) {
            /**
             * @var $requests CallCenterRequest[]
             */
            foreach ($requests as $request) {
                if ($request->lastUpdateResponse) {
                    $response = json_decode($request->lastUpdateResponse->response, true);
                    $order = $request->order;
                    NewCallCenterHandler::prepareAndLoadValuesToOrder($response, $order);
                    if (!$order->save()) {
                        echo "Can't save order #" . $order->id . PHP_EOL;
                        echo "Reason: " . $order->getFirstErrorAsString();
                    }
                }
            }
        }
    }

    public function actionMigrateChargesValues()
    {
        DeliveryContractFile::deleteAll(); // очистим все записи
        DeliveryContract::deleteAll(); // очистим все записи
        $deliveriesData = Delivery::find()->where(['is not', Delivery::tableName() . '.charges_values', null])->all();
        if (is_array($deliveriesData)) {
            foreach ($deliveriesData as $deliveriesItem) {
                $deliveryContract = new DeliveryContract();
                $deliveryContract->charges_values = $deliveriesItem->charges_values;
                $deliveryContract->delivery_id = $deliveriesItem->id;
                $deliveryContract->save(false);
            }
        }
    }

    /**
     * Первоначальное заполнение штатного расписания
     */
    public function actionStaffingInit()
    {

        Staffing::deleteAll();

        $offices = Office::find()->active()->andWhere(['type' => Office::TYPE_CALL_CENTER])->all();

        foreach ($offices as $office) {
            $data = [
                [
                    'office_id' => $office->id,
                    'designation_id' => '5',
                    'max_persons' => '45',
                    'min_jobs' => NULL,
                    'salary_usd' => NULL,
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '2',
                    'max_persons' => '1',
                    'min_jobs' => '45',
                    'salary_usd' => NULL,
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '3',
                    'max_persons' => '3',
                    'min_jobs' => NULL,
                    'salary_usd' => NULL,
                    'salary_local' => NULL,
                    'comment' => 'лидер группы из 3х старших операторов',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '8',
                    'max_persons' => '1',
                    'min_jobs' => NULL,
                    'salary_usd' => '4',
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '6',
                    'max_persons' => '1',
                    'min_jobs' => '30',
                    'salary_usd' => '5',
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '71',
                    'max_persons' => '9',
                    'min_jobs' => NULL,
                    'salary_usd' => '8',
                    'salary_local' => NULL,
                    'comment' => 'лидер группы из 5 операторов',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '7',
                    'max_persons' => '1',
                    'min_jobs' => '30',
                    'salary_usd' => '6',
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ],
                [
                    'office_id' => $office->id,
                    'designation_id' => '9',
                    'max_persons' => '1',
                    'min_jobs' => NULL,
                    'salary_usd' => '3',
                    'salary_local' => NULL,
                    'comment' => '',
                    'created_at' => time(),
                    'updated_at' => time()
                ]
            ];

            Yii::$app->db->createCommand()->batchInsert(
                Staffing::tableName(),
                [
                    'office_id',
                    'designation_id',
                    'max_persons',
                    'min_jobs',
                    'salary_usd',
                    'salary_local',
                    'comment',
                    'created_at',
                    'updated_at'
                ],
                $data
            )->execute();
        }
    }

    /**
     * Одноразовый скрипт для синхронизации смен в крокотайм и пее
     * Выполнять до синхронизации сотрудников по сменам
     */
    public function actionSynchronizeShifts()
    {
        $shifts = WorkingShift::find()
            ->joinWith('offices')
            ->where(['not', [Office::tableName() . '.crocotime_parent_group_id' => null]])
            ->andWhere(['crocotime_id' => null])
            ->all();

        $crocotimeApi = new CrocotimeApi();
        foreach ($shifts as $shift) {
            if (!$shift->crocotime_id = $crocotimeApi->insertSchedule($shift->name)) {
                print_r($crocotimeApi->errors);
                break;
            };
            echo "Shift $shift->crocotime_id inserted" . PHP_EOL;
            if (!$shift->save()) {
                print_r($shift->errors);
                break;
            };
            if (!$crocotimeApi->updateSchedule($shift)) {
                print_r($crocotimeApi->errors);
                break;
            };
            echo "Shift $shift->crocotime_id updated" . PHP_EOL;
        }
    }

    /**
     * Одноразовый скрипт для синхронизации сотрудников в крокотайм по сменам
     * Выполнять после синхронизации смен actionSynchronizeShifts
     */
    public function actionSynchronizeShiftsForEmployees()
    {
        $crocoTimeApi = new CrocotimeApi();
        $employeesByWorkShift = $crocoTimeApi->crocotimeEmployeeIdByShifts();
        foreach ($employeesByWorkShift as $shiftId => $employeeIds) {
            $crocoTimeApi->batchUpdateEmployeeSchedule($shiftId, $employeeIds);
        }
        if ($crocoTimeApi->errors) {
            print_r($crocoTimeApi->errors);
        }
    }

    /**
     * тест Skype API
     * @param int $to
     * @param string $message
     */
    public function actionTestSkype(int $to, string $message)
    {
        $skypeMessage = new Message([
            'serviceType' => Message::SERVICE_TYPE_SKYPE,
            'to' => ['userID' => $to],
            'text' => $message,
        ]);
        $result = \Yii::$app->getModule('messenger')->send($skypeMessage);
        var_dump($result);
    }

    /**
     * Ищем фин отчеты за последние 7 дней и ищем претензии в них по дублям
     *
     * @param int $days
     */
    public function actionFalseStatusOrderPretension($days = 7)
    {
        $timeBorder = strtotime('-' . $days . ' days');
        $reports = DeliveryReport::find()
            ->where([
                DeliveryReport::tableName() . '.status' => [
                    DeliveryReport::STATUS_CLOSED,
                    DeliveryReport::STATUS_COMPLETE
                ]
            ])
            ->andWhere([
                DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL
            ])
            ->andWhere([
                '>',
                DeliveryReport::tableName() . '.updated_at',
                $timeBorder
            ])
            ->all();

        foreach ($reports as $report) {
            /** @var $report DeliveryReport */
            $report->setFalseStatusOrderPretension();
        }
    }

    /**
     * Восстановление отсутствующих курсов валют в order_finance_prediction
     *
     * fill-corrupted-currencies-in-prediction-finances
     * @param null $delivery_id
     * @throws \Exception
     */
    public function actionFillCorruptedCurrenciesInPredictionFinances($delivery_id = null)
    {
        date_default_timezone_set('UTC');

        $query = DeliveryContract::find()
            ->innerJoinWith('chargesCalculatorModel', false)
            ->where([DeliveryChargesCalculator::tableName() . '.active' => 1]);
        $query->with(['delivery', 'chargesCalculatorModel']);
        if ($delivery_id) {
            $query->andWhere([DeliveryContract::tableName() . '.delivery_id' => $delivery_id]);
        }

        $lastCurrencyId = Currency::find()->select('MAX(id)')->scalar();
        $contracts = $query->all();

        if (is_array($contracts)) {
            foreach ($contracts as $contract) {
                echo "Start calculating for {$contract->delivery->country->name} - {$contract->delivery->name} - #{$contract->id}" . PHP_EOL;
                $chargesCalculator = $contract->getChargesCalculator();

                $query = Order::find()->innerJoinWith(['deliveryRequest', 'financePrediction']);
                $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $contract->delivery_id]);

                if ($contract->date_from) {
                    $query->andWhere([
                        '>=',
                        'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                        strtotime('midnight', strtotime($contract->date_from))
                    ]);
                }
                if ($contract->date_to) {
                    $query->andWhere([
                        '<=',
                        'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                        strtotime('23:59:59', strtotime($contract->date_to))
                    ]);
                }
                $orCondition = ['or'];
                foreach (OrderFinancePrediction::getCurrencyFields() as $field => $currencyField) {
                    $orCondition[] = ['is', $currencyField, null];
                    $orCondition[] = [$currencyField => 0];
                    $orCondition[] = [$currencyField => new Expression("`$field`")];
                    $orCondition[] = ['>', $currencyField, $lastCurrencyId];
                }
                $query->andWhere($orCondition);
                $query->with(['deliveryRequest', 'callCenterRequest', 'orderProducts', 'financePrediction']);

                $countOrders = $query->count();
                $counter = 0;
                $updated = 0;
                Console::startProgress($counter, $countOrders);
                foreach ($query->batch(1000) as $orders) {
                    $batches = [];
                    /**
                     * @var Order[] $orders
                     */
                    foreach ($orders as $order) {
                        $finance = $chargesCalculator->calculate($order);
                        if ($finance) {
                            $encoded = json_encode($finance->getAttributes(array_values(OrderFinancePrediction::getCurrencyFields())));
                            if (!isset($batches[$encoded])) {
                                $batches[$encoded] = [];
                            }
                            $batches[$encoded][] = $order->id;
                        }
                        Console::updateProgress(++$counter, $countOrders);
                    }
                    foreach ($batches as $encoded => $orderIds) {
                        $decoded = json_decode($encoded, true);
                        $updated += OrderFinancePrediction::updateAll($decoded, ['order_id' => $orderIds]);
                    }
                }
                Console::endProgress("{$contract->delivery->country->name} - {$contract->delivery->name} - #{$contract->id} has finished. Updated {$updated} records" . PHP_EOL);
            }
        }
    }

    /**
     * fill-delivery-partners-table
     *
     * Перенос названий партнеров КС из delivery_request в delivery_partner
     */
    public function actionFillDeliveryPartnersTable()
    {
        Console::stdout('Start creating delivery partners in table `delivery_partner');
        $existedPartners = DeliveryPartner::find()->select('name')->column();
        $query = DeliveryRequest::find()->select(['delivery_id', 'partner_name'])
            ->where(['is not', 'partner_name', null])
            ->andWhere(['<>', 'partner_name', ''])
            ->groupBy('partner_name');
        $totalCount = $query->count();
        $counter = 0;
        Console::startProgress($counter, $totalCount);
        foreach ($query->batch(1000) as $rows) {
            $insert = [];
            foreach ($rows as $row) {
                if (!in_array($row['partner_name'], $existedPartners)) {
                    $insert[] = [
                        'delivery_id' => $row['delivery_id'],
                        'name' => $row['partner_name'],
                        'created_at' => time()
                    ];
                }
            }
            if ($insert) {
                DeliveryPartner::getDb()->createCommand()->batchInsert(DeliveryPartner::tableName(), [
                    'delivery_id',
                    'name',
                    'created_at'
                ], $insert);
            }
            $counter += count($rows);
            Console::updateProgress($counter, $totalCount);
        }
        Console::endProgress('All delivery partner has been created in table `delivery_partner`.');

        Console::stdout(PHP_EOL);

        Console::stdout('Start update delivery_request set delivery_partner_id');
        $existedPartners = DeliveryPartner::find()->all();
        $counter = 0;
        Console::startProgress($counter, sizeof($existedPartners));
        foreach ($existedPartners as $partner) {
            /** @var $partner DeliveryPartner */
            Yii::$app->db->createCommand()->update('delivery_request', [
                'delivery_partner_id' => $partner->id
            ], [
                'delivery_id' => $partner->delivery_id,
                'partner_name' => $partner->name
            ]);
            $counter++;
            Console::updateProgress($counter, sizeof($existedPartners));
        }
        Console::endProgress('All delivery_request updated');
    }

    /**
     * Обновление факт. фин. информации из отчетов от КС
     * reload-order-finance-fact-data
     *
     * @param $delivery_id
     * @throws Exception
     */
    public function actionReloadOrderFinanceFactData($delivery_id)
    {
        $query = OrderFinanceFact::find()
            ->joinWith(['deliveryReport'], false)
            ->where([DeliveryReport::tableName() . '.delivery_id' => $delivery_id])
            ->with('deliveryReportRecord');

        $totalCount = $query->count();
        $counter = 0;
        Console::startProgress($counter, $totalCount);

        foreach ($query->batch(500) as $facts) {
            /** @var OrderFinanceFact $fact */
            foreach ($facts as $fact) {
                foreach (array_keys(OrderFinanceFact::getCurrencyFields()) as $field) {
                    if ($fact->deliveryReportRecord->hasAttribute($field)) {
                        $fact->{$field} = $fact->deliveryReportRecord->{$field};
                    }
                }
                if (!$fact->save()) {
                    throw new Exception($fact->getFirstErrorAsString());
                }
                Console::updateProgress(++$counter, $totalCount);
            }
        }
        Console::endProgress("Updated {$counter} order finance facts.");
    }

    /*
     * Выбирает листы в статусе "генерация штрих-кодов" для которых нет записей в order_logistic_list_barcode и запускает job на генерацию
     */
    public function actionCreateBarcodeJob()
    {
        $lists = OrderLogisticList::find()
            ->joinWith('orderLogisticListBarcodes')
            ->where(['status' => OrderLogisticList::STATUS_BARCODING])
            ->andWhere(['is', OrderLogisticListBarcode::tableName() . '.list_id', null])
            ->all();

        foreach ($lists as $list) {
            Yii::$app->queue->push(new CreateBarcodes(['orderLogisticListId' => $list->id]));
        }
    }

    /**
     * Генерация фактов для записей в отчетах
     * @param null $deliveryReportId
     * @throws \Exception
     */
    public function actionGenerateOrderFinanceFact($deliveryReportId = null)
    {
        $query = DeliveryReportRecord::find()
            ->joinWith(['financeFact', 'report'], false)
            ->with(['report.currencies', 'report.payments'])
            ->where([
                DeliveryReportRecord::tableName() . '.record_status' => 'status_updated',
                DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL
            ])
            ->andWhere(['is', OrderFinanceFact::tableName() . '.id', null]);
        if ($deliveryReportId) {
            $query->andWhere([DeliveryReport::tableName() . '.id' => $deliveryReportId]);
        }

        $totalCount = $query->count();
        $counter = 0;
        Console::startProgress($counter, $totalCount);
        foreach ($query->batch(500) as $records) {
            /** @var DeliveryReportRecord $record */
            foreach ($records as $record) {
                if ($record->relatedOrder->finance) {
                    $payment = $record->report->getPayments()
                        ->andWhere([PaymentOrder::tableName() . '.name' => $record->relatedOrder->finance->payment])
                        ->one();
                }
                $record->createFinanceFact($payment ?? null);
                Console::updateProgress(++$counter, $totalCount);
            }
        }
        Console::endProgress("All facts are created.");
    }

    /**
     * @param null|int $countryId
     * @param null|int $deliveryId
     */
    public function actionCalculateOrderFinanceBalance($countryId = null, $deliveryId = null)
    {
        $query = OrderFinancePrediction::find()
            ->select([DeliveryRequest::tableName() . '.id'])
            ->joinWith('order.deliveryRequest')
            ->where([Order::tableName().'.status_id' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList())])
            ->orderBy([DeliveryRequest::tableName() . '.id' => SORT_ASC]);
        if ($countryId) {
            $query->andWhere([Order::tableName() . '.country_id' => $countryId]);
        }
        if ($deliveryId) {
            $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $deliveryId]);
        }

        $totalCount = $query->count();
        $counter = 0;
        $lastId = 0;

        Console::startProgress($counter, $totalCount);

        while (($ids = (clone $query)->andWhere(['>', DeliveryRequest::tableName().'.id', $lastId])->limit(1000)->column()) && !is_null($lastId)) {
            foreach ($ids as $id) {
                Yii::$app->queueOrders->push(new OrderCalculateFinanceBalanceJob(['deliveryRequestId' => $id]));
                Console::updateProgress(++$counter, $totalCount);
            }
            $lastId = $id ?? null;
        }
        Console::endProgress();
    }

    /**
     * Установление сроков отчета старым (первоначально для ДЗ), в которых их нет
     */
    public function actionSetReportPeriod(): void
    {
        $records = DeliveryReport::find()
            ->select([
                'id' => DeliveryReport::tableName() . '.id',
                'period_from' => 'min(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
                'period_to' => 'max(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
            ])
            ->joinWith('records.order.deliveryRequest')
            ->where(['OR',
                ['is', DeliveryReport::tableName() . '.period_from', null],
                ['is', DeliveryReport::tableName() . '.period_to', null],
            ])
            ->groupBy(DeliveryReport::tableName() . '.id')
            ->asArray()->all();
        foreach ($records as $record) {
            DeliveryReport::updateAll(['period_from' => $record['period_from'], 'period_to' => $record['period_to']], ['id' => $record['id']]);
        }
    }

    /**
     * @param $userID
     * @param $text
     */
    public function actionTestSkypeBot($userID, $text)
    {
        SkypeController::sendUserById($userID, $text);
    }

    /**
     * Временный скрипт для отправки заказов на проверку адреса если от КС BoxMe выпала ошибка
     *
     * @param int $deliveryId 282 КС BoxMe
     * @param null $limit
     */
    public function actionCheckAddressBoxme($deliveryId = 282, $limit = null)
    {

        $subQuery = new Query();
        $subQuery->select([
            'order_id' => 'distinct(order_id)',
            'id' => 'id'
        ])
            ->from(CallCenterCheckRequest::tableName())
            ->andWhere([
                CallCenterCheckRequest::tableName() . '.type' => CallCenterCheckRequest::TYPE_CHECK_ADDRESS
            ]);

        $query = Order::find()
            ->innerJoinWith('deliveryRequest', false)
            ->innerJoinWith('callCenterRequest', false)
            ->leftJoin(['sub' => $subQuery],
                Order::tableName() . '.id=sub.order_id')
            ->where([
                Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_REJECTED,
                DeliveryRequest::tableName() . '.delivery_id' => $deliveryId
            ])
            ->andWhere([
                'or',
                ['like', DeliveryRequest::tableName() . '.api_error', 'This address is out of delivery area'],
                ['like', DeliveryRequest::tableName() . '.api_error', 'Boxme does not support for this information'],
            ])
            ->andWhere(['is', 'sub.id', null]);

        if ($limit) {
            $query->limit($limit);
        }

        foreach ($query->all() as $model) {
            echo 'OrderId= ' . $model->id;

            $orderCheckHistory = new CallCenterCheckRequest;
            $orderCheckHistory->status_id = $model->status_id;
            $orderCheckHistory->api_error = $model->deliveryRequest->api_error;
            $orderCheckHistory->order_id = $model->id;

            if ($model->canChangeStatusTo(OrderStatus::STATUS_CC_CHECKING)) {
                $model->status_id = OrderStatus::STATUS_CC_CHECKING;
                if ($model->save(true, ['status_id'])) {
                    $orderCheckHistory->type = CallCenterCheckRequest::TYPE_CHECK_ADDRESS;
                    $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
                    $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;
                    if ($orderCheckHistory->save()) {
                        echo ' add to check_address' . PHP_EOL;
                    } else {
                        echo $orderCheckHistory->getFirstErrorAsString() . PHP_EOL;
                    }
                }
            }
        }
    }

    /**
     * Удаление отмеченных на удаление отчетов на случай если не сработала job
     */
    public function actionDeliveryReportsDelete()
    {
        $deliveryReports = DeliveryReport::findAll(['status' => DeliveryReport::STATUS_DELETE]);
        foreach ($deliveryReports as $deliveryReport) {
            $this->stdout('Deleting report ' . $deliveryReport->id . ' ');
            $job = new DeliveryReportDelete(['deliveryReportId' => $deliveryReport->id]);
            try {
                $result = $job->run();
                if ($result) {
                    $this->stdout('true' . PHP_EOL);
                } else {
                    $this->stdout('false' . PHP_EOL);
                }
            } catch (\Exception $e) {
                $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
            }
        }
    }

//    TODO: переписать на новые издержки
    /**
     * Расчет баланса платежек
     * @deprecated больше не списывает с доп издержек!
     */
    public function actionCalculateBalanceForPayments()
    {
        $reports = DeliveryReport::find()
            ->joinWith('payments')
            ->where(new Expression('(SELECT SUM(' . PaymentOrder::tableName() . '.balance)) = (SELECT SUM(' . PaymentOrder::tableName() . '.sum))'))
            ->andWhere([
                DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_FINANCIAL,
            ])
            ->groupBy(DeliveryReport::tableName() . '.id')
            ->orderBy([DeliveryReport::tableName() . '.id' => SORT_ASC])
            ->all();
        $errors = $reportIDs = [];
        Console::startProgress(0, count($reports));
        foreach ($reports as $key => $report) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $records = DeliveryReportRecord::find()
                    ->where([
                        'delivery_report_id' => $report->id,
                        'record_status' => DeliveryReportRecord::STATUS_STATUS_UPDATED
                    ])
                    ->all();
                if (count($records) > 0) {
                    $payments = $report->payments;
                    if (count($payments) > 0) {
//                        TODO: + доп. издержки
                        $report->sum_total = $report->total_costs;
                        if (!$report->save(true, ['sum_total'])) {
                            throw new \Exception($report->getFirstErrorAsString());
                        } else {
                            Yii::$app->getDb()->createCommand()->insert('temp_recalculated_balance_payments', ['report_id' => $report->id])->execute();
                        }

                        $contract = $report->delivery->getActiveContractByDelivery($report->delivery->id, $report->period_from, $report->period_to);
                        $currentPayment = array_pop($payments);

                        foreach ($records as $record) {
                            if (!$record->relatedOrder) {
                                throw new \Exception('отсутствует relatedOrder');
                            }
                            if (!$currentPayment) {
                                throw new \Exception('отсутствует currentPayment');
                            }

                            PaymentOrder::getDb()->createCommand('SELECT 1 FROM ' . PaymentOrder::tableName() . ' WHERE id = :id LOCK IN SHARE MODE', [':id' => (int)$currentPayment->id])->execute();

                            $order = $record->relatedOrder;
                            $statusFromReport = intval($report->statuses[$record->status]);

                            $deliveryRequest = $order->deliveryRequest;
                            if (empty($deliveryRequest)) {
                                throw new \Exception('отсутствует deliveryRequest в заказе №' . $order->id);
                            }

                            $costs = 0;
                            $financeColumns = DeliveryReportRecord::getPriceColumn();
                            if (!$record->report->currencies) {
                                $report->setDefaultByReport();
                                $record->report->refresh();
                                if (!$record->report->currencies) {
                                    throw new \Exception('Не заданы валюты отчета');
                                }
                            }

                            $rates = [];
                            foreach ($financeColumns as $column => $attribute) {
                                if ($currID = $record->report->currencies->getAttribute($column . '_currency_id')) {
                                    $rates[$currID] = $report->getRate($currID, $report->country->currency_id, null, $currentPayment ?? null);
                                    if (empty($rates[$currID])) {
                                        throw new \Exception(Yii::t('common', 'Отсутствует рейт для перевода из {from} в {to}',
                                            ['from' => Currency::findOne(['id' => $currID])->char_code, 'to' => Currency::findOne(['id' => $report->currency_id])->char_code]));
                                    }
                                } else {
                                    throw new \Exception(Yii::t('common', 'В финансовых колонках отчета отсутствует поле {field}', ['field' => $column . '_currency_id']));
                                }

                                $val = doubleval($record->$column) / (!empty($rates[$currID]) ? $rates[$currID] : 1);
                                if ($column != DeliveryReportRecord::COLUMN_PRICE_COD) {
                                    $costs += $val;
                                }
                            }

                            if ($statusFromReport == OrderStatus::STATUS_FINANCE_MONEY_RECEIVED) {
                                if ($rate = $report->getRate($report->currencies->getAttribute('price_cod_currency_id', $report->country->currency_id), $report->country->currency_id, null, $currentPayment ?? null)) {
                                    $priceCod = (float)$record->price_cod / $rate;
                                } else {
                                    throw new \Exception(Yii::t('common', 'Отсутствует рейт для перевода из {from} в {to}',
                                        ['from' => Currency::findOne(['id' => $report->currencies->getAttribute('price_cod_currency_id', $report->currency_id)])->char_code, 'to' => Currency::findOne(['id' => $report->currency_id])->char_code]));
                                }
                                if (!$contract || $contract->mutual_settlement == DeliveryContract::MUTUAL_SETTLEMENT_ON) {
                                    $priceCod -= $costs;
                                }

                                if (!$currentPayment) {
                                    throw new \Exception(Yii::t('Необходимо прикрепить хотя бы один платеж.'));
                                }
                                if ($rate = $report->getRate($currentPayment->currency_id, $report->country->currency_id, null, $currentPayment)) {
                                    if ($report->sum_total) {
                                        if ($report->sum_total <= $priceCod) {
                                            $priceCod -= $report->sum_total;
                                            $report->sum_total = 0;
                                        } else {
//                                            TODO: + списание с доп. издержек
                                            $report->sum_total -= $priceCod;
                                            $priceCod = 0;
                                        }
                                        if (!$report->save(true, ['sum_total'])) {
                                            throw new \Exception($report->getFirstErrorAsString());
                                        }
                                    }
                                    while (($currentPayment->balance / $rate) < $priceCod && count($payments) > 0) {
                                        $priceCod -= $currentPayment->balance / $rate;
                                        $currentPayment->balance = 0;
                                        if (!$currentPayment->save(true, ['balance'])) {
                                            throw new \Exception($currentPayment->getFirstErrorAsString());
                                        }

                                        $currentPayment = array_pop($payments);
                                        PaymentOrder::getDb()->createCommand('SELECT 1 FROM ' . PaymentOrder::tableName() . ' WHERE id = :id LOCK IN SHARE MODE', [':id' => (int)$currentPayment->id])->execute();
                                        $currentPayment->refresh();
                                        $rate = $report->getRate($currentPayment->currency_id, $report->country->currency_id, null, $currentPayment);
                                    }
                                    $balance = round($currentPayment->balance / $rate, 4);
                                    $currentPayment->balance = ($balance - $priceCod) * $rate;
                                    if (!$currentPayment->save(false, ['balance'])) {
                                        throw new \Exception($currentPayment->getFirstErrorAsString());
                                    }
                                } else {
                                    throw new \Exception(Yii::t('common',
                                        'Не удалось определить курс валюты для указанной даты платежа.'));
                                }
                            } elseif (!$contract || $contract->mutual_settlement == DeliveryContract::MUTUAL_SETTLEMENT_ON) {
                                $tempPayments = $payments;
                                $paymentConnect = $currentPayment;
                                while ($deliveryRequest->created_at > $paymentConnect->paid_at && count($tempPayments) > 0) {
                                    $paymentConnect = array_pop($tempPayments);
                                }
                                $rate = null;
                                if ($rate = $report->getRate($paymentConnect->currency_id, $report->country->currency_id, null, $paymentConnect)) {
                                    $paymentConnect->balance = $paymentConnect->balance + $costs * $rate;
                                    if (!$paymentConnect->save(false, ['balance'])) {
                                        throw new \Exception($paymentConnect->getFirstErrorAsString());
                                    }
                                } else {
                                    throw new \Exception(Yii::t('common',
                                        'Не удалось определить курс валюты для указанной даты платежа.'));
                                }
                            }
                        }
                        $reportIDs[] = $report->id;
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $errors[$report->id] = $e->getMessage();
            }
            Console::updateProgress($key + 1, count($reports));
        }
        Console::endProgress();

        if ($reportIDs) {
            echo "\n" . count($reportIDs) . ' reports: ' . implode(', ', $reportIDs) . "\n";
        }
        if ($errors) {
            print_r($errors);
        }
    }

    /**
     * Сохранение call_data для старых заказов в новую таблицу
     * @param null $dateFrom
     * @param null $dateTo
     * @param null $callCenterId
     * @param int $batchSize
     */
    public function actionSaveCallData($dateFrom = null, $dateTo = null, $callCenterId = null, $batchSize = 500)
    {
        if (!$dateFrom) {
            $dateFrom = date('Y-m-d');
        }

        $query = CallCenterRequest::find()
            ->where(['>=', 'created_at', strtotime($dateFrom)]);

        if ($callCenterId) {
            $query->andWhere(['call_center_id' => $callCenterId]);
        }

        if ($dateTo) {
            $query->andWhere(['<=', 'created_at', strtotime($dateTo)]);
        }

        foreach ($query->batch($batchSize) as $requests) {
            foreach ($requests as $request) {
                /** @var CallCenterRequest $request */

                if ($request->getLastUpdateResponse()) {
                    $updateResponse = json_decode($request->getLastUpdateResponse()->response, true);

                    if (isset($updateResponse['call_data'])) {
                        CallCenterRequestCallData::saveCallData($updateResponse['call_data'], $request->id);
                    }
                }
            }
        }
    }

    /**
     * отправка базового справочника для интеграции с 1С
     */
    public function actionSendHandbook()
    {
        $db = Yii::$app->getDb();
        if ($db instanceof ConnectionInterface) {
            foreach (Currency::find()->all() as $currency) {
                $db->addBufferChange(new DirtyData([
                    'id' => Currency::clearTableName(),
                    'action' => DirtyData::ACTION_INSERT,
                    'data' => $currency->getAttributes()
                ]));
            }

            foreach (Product::find()->where(['active' => true])->all() as $item) {
                $db->addBufferChange(new DirtyData([
                    'id' => Product::clearTableName(),
                    'action' => DirtyData::ACTION_INSERT,
                    'data' => $item->getAttributes()
                ]));
            }

            foreach (Source::find()->where(['active' => true])->all() as $item) {
                $db->addBufferChange(new DirtyData([
                    'id' => Source::clearTableName(),
                    'action' => DirtyData::ACTION_INSERT,
                    'data' => $item->getAttributes()
                ]));
            }
        }
    }

    /**
     * отправка базового справочника для интеграции с 1С по стране
     * @param int $countryId
     */
    public function actionSendHandbookForCountry(int $countryId)
    {
        $db = Yii::$app->getDb();
        if ($db instanceof ConnectionInterface) {
            $country = Country::find()->where(['id' => $countryId])->one();
            $db->addBufferChange(new DirtyData([
                'id' => Country::clearTableName(),
                'action' => DirtyData::ACTION_INSERT,
                'data' => $country->getAttributes()
            ]));

            foreach (Storage::find()->where(['country_id' => $country->id])->all() as $item) {
                $db->addBufferChange(new DirtyData([
                    'id' => Storage::clearTableName(),
                    'action' => DirtyData::ACTION_INSERT,
                    'data' => $item->getAttributes()
                ]));
            }
        }
    }

    /**
     * тест CMC ZorraService
     * @param string $to
     * @param string $message
     */
    public function actionTestSms(string $to, string $message)
    {
        $smsService = yii::$app->get('smsService');

        $result = $smsService->sendSmsNotification($to, null, $message, '2Wstore');
        var_dump($result);
    }
}
