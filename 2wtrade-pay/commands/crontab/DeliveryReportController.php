<?php

namespace app\commands\crontab;

use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\deliveryreport\components\scanner\ReportScanner;
use app\components\console\CrontabController;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\administration\models\CrontabTask;
use Yii;

/**
 * Class DeliveryReportController
 * @package app\commands
 */
class DeliveryReportController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'parsereports' => CrontabTask::TASK_DELIVERY_REPORT_PARSE_REPORTS,
    ];

    /**
     * Сканирование и проверка отчетов от служб доставки
     * @param  integer|null $country_id
     * @param integer|null $delivery_id
     * @param integer $limit
     */
    public function actionParseReports($country_id = null, $delivery_id = null, $limit = 100)
    {
        Yii::$app->db->enableSlaves = false;
        $this->log('Begin parsing reports...', false);
        set_time_limit(0);
        $query = DeliveryReport::find()->where([
            'status' => [
                DeliveryReport::STATUS_QUEUE,
                DeliveryReport::STATUS_CHECKING_QUEUE,
            ]
        ])->orderBy('priority DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($country_id)) {
            $query->andWhere([DeliveryReport::tableName() . '.country_id' => $country_id]);
        }

        if (!empty($delivery_id)) {
            $query->andWhere([DeliveryReport::tableName() . '.delivery_id' => $delivery_id]);
        }

        $count = 0;
        foreach ($query->each() as $model) {
            /**
             * @var DeliveryReport $model
             */
            /** Обновляем данные, т.к. пока обрабатывался предыдущий отчет, данные могли измениться */
            $model->refresh();
            if (!in_array($model->status, [DeliveryReport::STATUS_QUEUE, DeliveryReport::STATUS_CHECKING_QUEUE])) {
                continue;
            }
            $reportFile = new ReportScanner($model);
            $oldStatus = $model->status;
            $fp = null;
            try {
                $file_lock = "/blocks/delivery_report_parse_reports{$model->country_id}_{$model->delivery_id}_{$model->status}.lock";
                $fp = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');
                if (!flock($fp, LOCK_EX | LOCK_NB)) {
                    fclose($fp);
                    continue;
                }
                if ($model->status == DeliveryReport::STATUS_QUEUE) {
                    $model->deleteRecords();
                    $model->status = DeliveryReport::STATUS_PARSING;
                    $model->error_message = '';
                    if (!$model->save(false, ['status', 'error_message'])) {
                        throw new \Exception($model->getFirstErrorAsString());
                    }

                    $this->log("Старт сканирования отчета: {$model->name}");
                    $reportFile->Parse(DeliveryReport::getFileFullRoute($model->file));
                    $this->log("Отчет: {$model->name} был успешно просканирован.");
                    $oldStatus = $model->status;
                }
                $model->status = DeliveryReport::STATUS_CHECKING;
                if (!$model->save(false, ['status'])) {
                    throw new \Exception($model->getFirstErrorAsString());
                }

                $this->log("Старт проверки отчета: {$model->name}");
                $reportFile->Check();
                $this->log("Отчет: {$model->name} был успешно проверен.");

                $model->status = DeliveryReport::STATUS_IN_WORK;
                if (!$model->save(false, ['status'])) {
                    throw new \Exception($model->getFirstErrorAsString());
                }
                fclose($fp);
                $count++;
            } catch (\Exception $e) {
                $exception = [
                    'message' => $e->getMessage(),
                    'oldStatus' => $oldStatus
                ];
                $model->status = DeliveryReport::STATUS_ERROR;
                $model->error_message = json_encode($exception);
                $model->save(true, ['status', 'error_message']);
                $cronAnswer = new CrontabTaskLogAnswer();
                $cronAnswer->log_id = $this->log->id;
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $cronAnswer->data = json_encode([
                    'report_id' => $model->id,
                    'country_id' => $model->country_id,
                    'error' => $e->getMessage()
                ], JSON_UNESCAPED_UNICODE);
                $cronAnswer->save();
                $this->log("Возникла ошибка: '" . $e->getMessage() . "' при работе с отчетом: {$model->name}");
                if ($fp) {
                    fclose($fp);
                }
            }
        }
        $this->log("Обработано {$count} отчетов.");
        $this->log('done.');
    }
}
