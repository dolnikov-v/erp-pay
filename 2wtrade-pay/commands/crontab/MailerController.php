<?php
namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\notification\EmailQueue;
use app\models\UserNotificationSetting;
use app\modules\administration\models\CrontabTask;
use app\modules\delivery\models\Delivery;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class MailerController
 * @package app\commands\crontab
 */
class MailerController extends CrontabController
{
    protected $mapTasks = [
        'sendnotifications' => CrontabTask::TASK_MAILER_SEND_NOTIFICATIONS,
        'senddeliverypaymentreminders' => CrontabTask::TASK_MAILER_SEND_DELIVERY_PAYMENT_REMINDERS,
    ];

    /**
     * Отсылка напоминаний курьеркам о предстоящих платежах
     *
     * @param null|string $time текущее время в формате, понятном для strtotime. полезно только для отладки
     */
    public function actionSendDeliveryPaymentReminders($time = null)
    {
        $now = time();

        if (!is_null($time)) {
            $now = strtotime($time);
        }

        $deliveries = Delivery::find()->where(['active' => 1])->all();

        foreach ($deliveries as $delivery) {

            if (!$delivery->isTimeToSendReminder($now)) {
                continue;
            }

            if (!trim ($delivery->payment_reminder_email_to) || !trim ($delivery->payment_reminder_email_reply_to)) {
                $this->log ("emty to: or reply to: address!");
                continue;
            }

            $success = Yii::$app->mailer->compose(['text' => '@app/mail/delivery-payment-reminder/mail'], [])
                ->setFrom(Yii::$app->params['noReplyEmail'])
                ->setTo(explode(",", $delivery->payment_reminder_email_to))
                ->setReplyTo(explode(",", $delivery->payment_reminder_email_reply_to))
                ->setSubject("Payment notification from 2WTrade")
                ->send();

            if ($success) {
                $delivery->payment_reminder_sent_at = $now;
                $delivery->save(false, ['payment_reminder_sent_at']);
            } else {
                $this->log("couldn't send payment notification");
            }
        }
    }

    /**
     * @param $interval
     */
    public function actionSendNotifications($interval = UserNotificationSetting::INTERVAL_ALWAYS)
    {
        if (!array_key_exists($interval, UserNotificationSetting::getIntervalCollection())) {
            throw new InvalidParamException('Incorrect interval.');
        }

        $this->log('Sending email...', false);

        $queue = new EmailQueue();
        $queue->send($interval);

        $this->log('done.');
    }
}
