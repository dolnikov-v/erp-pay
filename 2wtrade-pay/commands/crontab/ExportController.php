<?php

namespace app\commands\crontab;

use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\components\console\CrontabController;
use app\modules\order\models\OrderClarification;
use app\modules\order\models\OrderExport;
use Yii;

/**
 * Class ExportController
 * @package app\commands\crontab
 */
class ExportController extends CrontabController
{
    protected $mapTasks = [
        'generatefiles' => CrontabTask::TASK_GENERATE_EXPORT_FILES,
        'sendorderclarification' => CrontabTask::TASK_SEND_ORDER_CLARIFICATION,
    ];

    /**
     * @param int $seconds default 3600
     */
    public function actionGenerateFiles(int $seconds = 3600)
    {
        try {
            $data = OrderExport::find()->where(['generated_at' => null])->andWhere('created_at < UNIX_TIMESTAMP(NOW() - INTERVAL :sec SECOND)', [':sec' => abs(intval($seconds))])->all();
            if ($data) {
                $added = [];
                foreach ($data as $item) {
                    if (!Yii::$app->queueExport->push(new \app\jobs\ExportOrder(['id' => $item->id]))) {
                        $this->addError($item->id);
                    } else {
                        $added[] = $item->id;
                    }
                }
                if ($added) {
                    $this->addSuccessMessage($added);
                }
            }
        } catch (\Exception $e) {
            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $this->log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->answer = Yii::t('common', 'Критическая ошибка при постановке в очередь.');
            $logAnswer->data = $e->getMessage();
            $logAnswer->save();
        }
    }

    /**
     * Отправка на уточнение в КС писем с выгрузкой заказов в файл
     * @param int $seconds default 3600
     */
    public function actionSendOrderClarification(int $seconds = 3600)
    {
        try {
            $data = OrderClarification::find()->where(['generated_at' => null])->andWhere('created_at < UNIX_TIMESTAMP(NOW() - INTERVAL :sec SECOND)', [':sec' => abs(intval($seconds))])->all();
            if ($data) {
                $added = [];
                foreach ($data as $item) {
                    if (!Yii::$app->queueExport->push(new \app\jobs\SendOrderClarification(['id' => $item->id]))) {
                        $this->addError($item->id);
                    } else {
                        $added[] = $item->id;
                    }
                }
                if ($added) {
                    $this->addSuccessMessage($added);
                }
            }
        } catch (\Exception $e) {
            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $this->log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->answer = Yii::t('common', 'Критическая ошибка при постановке в очередь.');
            $logAnswer->data = $e->getMessage();
            $logAnswer->save();
        }
    }

    /**
     * @param int $id
     */
    private function addError(int $id)
    {
        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $this->log->id;
        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $logAnswer->answer = Yii::t('common', 'Задачу на экспорт для записи №{id} не удалось поставить в очередь.', ['id' => $id]);
        $logAnswer->save();
    }

    /**
     * @param array $ids
     */
    private function addSuccessMessage(array $ids)
    {
        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $this->log->id;
        $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $logAnswer->answer = Yii::t('common', 'Завершена постановка в очередь для следующих записей: {ids}.', ['ids' => implode('; ', $ids)]);
        $logAnswer->save();
    }
}