<?php
namespace app\commands\crontab;

use app\modules\delivery\components\api\Transmitter;
use app\modules\order\models\Order;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\components\api\TransmitterAdaptee;
use app\modules\packager\models\Packager;
use Yii;
use Exception;
use app\components\console\CrontabController;
use app\models\Country;
use app\modules\administration\models\CrontabTask;
use app\modules\packager\models\OrderPackaging;
use yii\helpers\ArrayHelper;

/**
 * Class PackagerController
 * @package app\commands\crontab
 */
class PackagerController extends CrontabController
{
    protected $mapTasks = [
        'sendorderstopackaging' => CrontabTask::TASK_PACKAGER_SEND_ORDERS_TO_PACKAGING,
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSendOrdersToPackaging()
    {
        $this->log('Start sending orders to packaging...');
        $countries = Country::find()->active()->all();

        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        foreach ($countries as $country) {

            $packagers = Packager::find()
                ->where([Packager::tableName() . '.auto_sending' => 1])
                ->byCountryId($country->id)
                ->active()
                ->all();

            foreach ($packagers as $packager) {

                $emails = $packager->getEmailList();
                if (!$emails) {
                    continue;
                }

                $deliveries = Delivery::find()
                    ->active()
                    ->byCountryId($country->id)
                    ->where([Delivery::tableName() . '.packager_id' => $packager->id])
                    ->all();

                try {

                    foreach ($deliveries as $delivery) {
                        $up = false;
                        if ($delivery->packager_send_label || $delivery->packager_send_invoice) {
                            $sent = true;
                            $to = array_shift($emails);
                            $query = OrderPackaging::find()
                                ->where([
                                    OrderPackaging::tableName() . '.packager_id' => $packager->id,
                                    OrderPackaging::tableName() . '.delivery_id' => $delivery->id,
                                    OrderPackaging::tableName() . '.status' => OrderPackaging::STATUS_PENDING,
                                    OrderPackaging::tableName() . '.method' => OrderPackaging::METHOD_EMAIL,
                                ]);
                        }

                        if ($delivery->packager_send_label) {
                            if (!$delivery->hasApiGenerateLabel()) {
                                throw new Exception(Yii::t('common', 'API-класс #{delivery_api_class_id} не умеет гененрировать этикетки.', [
                                    'delivery_api_class_id' => $delivery->api_class_id,
                                ]));
                            }
                            $transmitterAdaptee = new TransmitterAdaptee($delivery);
                            /** @var OrderPackaging[] $items */
                            foreach ($query->batch(100) as $items) {
                                $list = ArrayHelper::getColumn($items, 'order_id');
                                $orders = Order::find()
                                    ->where(['id' => $list])
                                    ->all();

                                if ($orders) {
                                    $filePath = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'order_packaging_list';
                                    $result = $transmitterAdaptee->generateLabel($orders, $filePath, false);

                                    if (ArrayHelper::getValue($result, 'status') == Transmitter::STATUS_SUCCESS) {
                                        $fileName = $result['data'];

                                        $mailer = Yii::$app->mailer
                                            ->compose()
                                            ->setFrom(Yii::$app->params['noReplyEmail'])
                                            ->setTo($to)
                                            ->setSubject('Orders for packaging from delivery - ' . $delivery->name)
                                            ->setTextBody('Label to orders in attach')
                                            ->attach($filePath . DIRECTORY_SEPARATOR . $fileName);

                                        if ($emails) {
                                            $mailer->setCc($emails);
                                        }
                                        $sent = $mailer->send();
                                        $up = true;
                                    } else {
                                        throw new Exception(Yii::t('common', 'API-класс #{delivery_api_class_id} при генерации этикеток ответил с ошибкой.', [
                                            'delivery_api_class_id' => $delivery->api_class_id,
                                        ]));
                                    }
                                }
                            }
                        }
                        if ($delivery->packager_send_invoice && $sent) {
                            $invoices = [];
                            /**
                             * @var OrderPackaging $orderPackaging
                             */
                            foreach ($query->all() as $orderPackaging) {
                                try {
                                    if ($fileName = $orderPackaging->generateInvoices()) {
                                        $invoices[] = [$fileName, $orderPackaging->invoice ?? null];
                                    }
                                } catch (\Exception $e) {
                                    throw new Exception(Yii::t('common', 'OrderPackaging при генерации накладых ответил с ошибкой.'));
                                }
                            }
                            if ($invoices) {
                                foreach (array_chunk($invoices, 100) as $key => $val) {
                                    $part = (count($invoices) > 100 ? 'part_' . ($key + 1) : '');
                                    $archiveName = $packager->country->char_code . $packager->id . '_' . $delivery->id . '_' . Yii::$app->formatter->asDate(time(), 'php:ym') . ($part ? '-' . $part : '');
                                    $archive = $packager->package($val, $archiveName);
                                    $mailer = Yii::$app->mailer
                                        ->compose()
                                        ->setFrom(Yii::$app->params['noReplyEmail'])
                                        ->setTo($to)
                                        ->setSubject('Invoices of orders for packaging from delivery - ' . $delivery->name . ' ' . $part)
                                        ->setTextBody('Invoice to orders in attach')
                                        ->attach($archive);
                                    if ($emails) {
                                        $mailer->setCc($emails);
                                    }
                                    $sent = $mailer->send();
                                    unlink($archive);
                                    $up = true;
                                }
                            }
                        }
                        if ($up) {
                            foreach ($items as $packaging) {
                                $packaging->status = ($sent) ? OrderPackaging::STATUS_DONE : OrderPackaging::STATUS_ERROR;
                                $packaging->save();
                            }
                        }
                    }

                } catch (Exception $e) {

                    $cronLog($e->getMessage(), [
                        'delivery_id' => $delivery->id,
                        'packager_id' => $packager->id,
                    ]);
                }
            }
        }

        $this->log('Sending completed.');
    }
}