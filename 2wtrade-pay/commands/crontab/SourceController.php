<?php

namespace app\commands\crontab;


use app\components\console\CrontabController;
use app\models\Source;
use app\modules\administration\models\CrontabTask;
use app\modules\order\models\Lead;
use Yii;

/**
 * Class AdcomboController
 * @package app\commands\crontab
 */
class SourceController extends CrontabController
{
    protected $mapTasks = [
        'pseudoapprove' => CrontabTask::TASK_SOURCE_PSEUDO_APPROVE,
        'trashholds' => CrontabTask::TASK_SOURCE_TRASH_HOLDS,
    ];

    /**
     * Ложный апрув лидов для источников, если процент аппрува упал до критической отметки
     * @param integer $source_id
     */
    public function actionPseudoApprove($source_id = null)
    {
        $query = Source::find()->where(['use_pseudo_approve' => 1]);

        if ($source_id) {
            $query->andWhere(['id' => $source_id]);
        }
        $sources = $query->all();
        foreach ($sources as $source) {
            foreach ($source->countries as $country) {
                try {
                    $source->setPseudoApproveForPreviousDay($country, 5);
                } catch (\Throwable $e) {
                    echo $e->getMessage() . PHP_EOL;
                    echo $e->getTraceAsString() . PHP_EOL;
                }
            }
        }
    }

    /**
     * трешить "протухшие" холды для адкомбо
     * все лиды за предыдущий месяц (таблицы lead) в статусе холд ставить в треш.
     * @param null $from
     * @param null $to
     */
    public static function actionTrashHolds($from = null, $to = null)
    {
        $dateFrom = $from ?? date("Y-m-01", strtotime("-1 month")) . " 00:00:00";
        $dateTo = $to ?? date("Y-m-t", strtotime("-1 month")) . " 23:59:59";

        try {
            Yii::$app->db->createCommand()
                ->update(Lead::tableName(), [
                    'source_status' => Lead::STATUS_TRASH
                ], [
                    'and',
                    [
                        'in',
                        'source_status',
                        [
                            Lead::STATUS_HOLD,
                            Lead::STATUS_UNCALLED,
                            Lead::STATUS_RECALL
                        ]
                    ],
                    ['between', 'ts_spawn', strtotime($dateFrom), strtotime($dateTo)]
                ])->execute();
        } catch (\Throwable $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getTraceAsString() . PHP_EOL;
        }
    }
}