<?php
namespace app\commands\crontab;

use app\models\Country;
use app\models\Timezone;
use app\models\User;
use app\models\UserCountry;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\components\console\CrontabController;
use app\modules\widget\components\Widget;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetUser;
use yii\helpers\ArrayHelper;
use Yii;


/**
 * Class WidgetController
 * @package app\commands\crontab
 */
class WidgetController extends CrontabController
{

    protected $mapTasks = [
        'makecache' => CrontabTask::TASK_CACHE_WIDGETS,
    ];


    public function actionMakeCache()
    {
        $this->log("Start make widgets cache...\n");

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $cronAnswer->save();

        $records = 0;
        $answer = [];
        try {

            $widgetUsers = WidgetUser::find()
                ->joinWith(['type'])
                ->joinWith(['user'])
                ->where([
                    WidgetType::tableName() . '.status' => WidgetType::STATUS_ACTIVE,
                ])
                ->all();

            $query = Country::find()->active()->all();
            $allCountries = ArrayHelper::map($query, 'id', 'timezone_id');

            foreach ($widgetUsers as $widgetUser) {

                //не кешировать некоторые Виджеты
                if ($widgetUser->type->no_cache == WidgetType::NO_CACHE) {
                    continue;
                }

                //не кешировать для неактивных пользователей
                if ($widgetUser->user->status != User::STATUS_DEFAULT) {
                    continue;
                }

                if ($widgetUser->user->isSuperadmin) {
                    $countries = $allCountries;
                }
                else {
                    $query = UserCountry::find()
                        ->joinWith('country')
                        ->where(['user_id' => $widgetUser->user->id])
                        ->andWhere(['`country`.active' => 1])
                        ->all();
                    $countries = ArrayHelper::map($query, 'country_id', 'country.timezone_id');
                }

                $timeOffset = null;
                if (!is_null($widgetUser->user->timezone_id)) {
                    $timezone = Timezone::findOne($widgetUser->user->timezone_id);
                    if ($timezone) {
                        $timeOffset = $timezone->time_offset;
                    }
                }

                //виджет по всем странам за раз
                if ($widgetUser->type->by_country == WidgetType::BY_ALL_COUNTRIES) {

                    $widgetCache = WidgetCache::find()
                        ->byWidgetUser($widgetUser->id)
                        ->one();
                    if (!$widgetCache) {
                        $widgetCache = new WidgetCache();
                        $widgetCache->widgetuser_id = $widgetUser->id;
                    }
                    /** @var \app\modules\widget\models\WidgetUser $widgetUser */

                    // сохрение данных для кеширования
                    $data = Widget::getData($widgetUser, null, array_keys($countries), $timeOffset);
                    $widgetCache->data = $widgetCache->saveData($data);
                    $widgetCache->cached_at = time();

                    if ($widgetCache->save()) {
                        $records++;
                    } else {
                        $answer['save_error'] = 'Ошибка при генерации виджета WidgetUser.id = ' . $widgetCache->id . ' ' . print_r($widgetCache->errors, true);
                    }
                }

                //виджет зависит от страны
                if ($widgetUser->type->by_country == WidgetType::BY_COUNTRY) {

                    foreach ($countries as $countryId => $timezoneID) {

                        if (is_null($timeOffset)) {
                            if ($timezoneID) {
                                $timezone = Timezone::findOne($timezoneID);
                                if ($timezone) {
                                    $timeOffset = $timezone->time_offset;
                                }
                            } else {
                                $timezone = Timezone::getDefault();
                                if ($timezone) {
                                    $timeOffset = $timezone->time_offset;
                                }
                            }
                        }


                        $widgetCache = WidgetCache::find()
                            ->byCountry($countryId)
                            ->byWidgetUser($widgetUser->id)
                            ->one();
                        if (!$widgetCache) {
                            $widgetCache = new WidgetCache();
                            $widgetCache->country_id = $countryId;
                            $widgetCache->widgetuser_id = $widgetUser->id;
                        }
                        /** @var \app\modules\widget\models\WidgetUser $widgetUser */

                        // сохранение данных для кеширования
                        $data = Widget::getData($widgetUser, $countryId, null, $timeOffset);
                        $widgetCache->data = $widgetCache->saveData($data);
                        $widgetCache->cached_at = time();

                        if ($widgetCache->save()) {
                            $records++;
                        } else {
                            $answer['save_error'] = 'Ошибка при генерации виджета WidgetUser.id = ' . $widgetCache->id . ' ' . print_r($widgetCache->errors, true);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->log($e->getTraceAsString() . ' ' . $e->getMessage());
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        }

        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $cronAnswer->answer = json_encode($answer);

        $cronAnswer->save();
        $this->log("Records: ".$records);
        $this->log("Done.\n");
    }
}