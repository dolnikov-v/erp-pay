<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\CurrencyFromCurrencyLayer;
use app\modules\administration\models\CrontabTask;

/**
 * Class CurrencyController
 * @package app\crontab
 */
class CurrencyController extends CrontabController
{
    protected $mapTasks = [
        'updaterates' => CrontabTask::TASK_UPDATE_CURRENCIES_RATES,
    ];

    /**
     * Обновление курсов валют
     */
    public function actionUpdateRates()
    {
        $this->log('Update currencies rates... ', false);

        CurrencyFromCurrencyLayer::updateRates($this->log);

        $this->log('done.');
    }
}
