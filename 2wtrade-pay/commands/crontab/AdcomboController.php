<?php

namespace app\commands\crontab;

use app\components\adcombo\AdComboParser;
use app\components\console\CrontabController;
use app\models\Source;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use Yii;
use yii\console\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class AdcomboController
 * @package app\commands\crontab
 */
class AdcomboController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'parse' => CrontabTask::TASK_ADCOMBO_PARSER,
        'parseapprovepenalties' => CrontabTask::TASK_ADCOMBO_PARSE_APPROVE_PENALTIES,
    ];

    /**
     * @param null $start
     * @param null $end
     * @throws \Exception
     */
    public function actionParseApprovePenalties($start = null, $end = null)
    {
        if ((!strtotime($start) || !strtotime($end)) && !in_array(null, [$start, $end])
            && (preg_match("#^\d{4}\-\d{2}\-\d{2}$#", $start) || preg_match("#^\d{4}\-\d{2}\-\d{2}$#", $end))) {

            $this->log("Not a valid period format \"{$start}\", \"{$end}\"");
            exit;
        }
        /** @var AdComboParser $parser */
        $parser = yii::$app->get('adcombo');
        $parser->parseApprovePenalties(function ($data, $start, $end) {
            $orderIds = Order::find()->where([
                'source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar(),
                'foreign_id' => ArrayHelper::getColumn($data, 'order_id')
            ])->select('id')->column();
            $cnt = Lead::updateAll(['has_approve_penalty' => true], ['order_id' => $orderIds]);
            $this->log("Chunk period to {$start} from {$end}: set approve penalty for {$cnt} leads.");
            sleep(1);
        }, $start, $end);

    }

    /**
     * without params
     * php yii crontab/adcombo/parse
     *
     * with params
     * php yii crontab/adcombo/parse 2018-01-28 2018-01-29
     *
     * @param null $start
     * @param null $end
     */
    public function actionParse($start = null, $end = null)
    {
        Console::stdout("Start parsing ADCOMBO statuses " . PHP_EOL);
        Console::stdout("Time: " . date('Y-m-d H:i:s') . PHP_EOL);
        /** @var AdComboParser $parser */
        $parser = yii::$app->get('adcombo');

        try {
            $parser->Parse($start, $end);

            Console::stdout("Data for updating lead: APPROVE {$parser->countAllApprove}, TRASH {$parser->countAllTrash}, CANCELLED {$parser->countAllCancelled}" . PHP_EOL);

            Console::stdout("Executing time: {$parser->getTimeExecution()} sec." . PHP_EOL);

            $this->updateLeads($parser->adcombo_data);

        } catch (Exception $e) {

            Yii::$app->notification->send(
                Notification::TRIGGER_ADCOMBO_PARSER_ERROR,
                [
                    'error' => $e->getMessage(),
                ]
            );

            Console::stdout('Exception catched: ' . $e->getMessage() . PHP_EOL);
            Console::stdout('Error: ' . $parser->error);
        }

        Console::stdout("End parsing ADCOMBO statuses " . PHP_EOL);
    }

    /**
     * @param array $leads leads by statuses group
     */
    public function updateLeads($leads)
    {
        if (is_array($leads)) {
            try {
                foreach ($leads as $status => $order_ids) {
                    $updateCount = 0;
                    if (!empty($order_ids)) {
                        $chunked_orders = array_chunk($order_ids, 1000);

                        foreach ($chunked_orders as $chunked_order) {
                            $update = yii::$app->db->createCommand()
                                ->update(Lead::tableName(), ['source_status' => $status], ['order_id' => ArrayHelper::getColumn($chunked_order, 'comment.data.order_id')])
                                ->execute();
                            foreach ($chunked_orders as $order) {
                                Yii::$app->db->createCommand()
                                    ->update(Lead::tableName(), ['ts_spawn' => $order['ts_spawn']], ['order_id' => $order['comment']['data']['order_id']])
                                    ->execute();
                            }
                            $updateCount += $update;
                        }

                        Console::stdout("Updated leads count {$updateCount} by {$status}" . PHP_EOL);
                    }
                }
            } catch (\yii\db\Exception $e) {
                Console::stdout("Exception when updating order statuses: " . $e->getMessage() . PHP_EOL);
            }
        } else {
            var_dump($leads);
        }
    }
}