<?php
namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\Currency;
use app\models\CurrencyRateHistory;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\crocotime\models\CrocotimeWorkTime;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Office;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\PenaltyType;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonGreetingCard;
use app\modules\salary\models\PersonLink;
use app\modules\salary\models\StaffingBonus;
use Yii;
use yii\swiftmailer\Message as SwiftMessage;
use app\models\Notification;

/**
 * Class PersonController
 * @package app\commands\crontab
 */
class PersonController extends CrontabController
{
    const PENALTY_TYPE_SOCIAL = 'social networks';
    const PENALTY_TYPE_LATE = 'late';

    protected $mapTasks = [
        'makegreetingcards' => CrontabTask::TASK_MAKE_GREETING_CARDS,
        'sendgreetingcards' => CrontabTask::TASK_SEND_GREETING_CARDS,
        'makeautopenalty' => CrontabTask::TASK_MAKE_AUTO_PENALTY,
        'sendsalarysheet' => CrontabTask::TASK_SEND_SALARY_SHEET,
        'makemonthlybonus' => CrontabTask::TASK_MAKE_MONTHLY_BONUS,
    ];

    /**
     * Генерация поздравлений с праздником для сотрудников
     * раз в сутки будем проверять
     */
    public function actionMakeGreetingCards()
    {
        // список сотрудников, у кого ДР

        $errors = [];

        $query = Person::find()
            ->active()
            ->andWhere(['is', 'dismissal_date', null])
            ->andWhere(['is not', 'corporate_email', null])
            ->andWhere(['<>', 'corporate_email', '']);

        $or = [];
        $or[] = 'or';
        $or[] = ['DATE_FORMAT(birth_date, "%m-%d")' => date('m-d')];
        if (date('m-d') == '02-29') {
            $or[] = ['DATE_FORMAT(birth_date, "%m-%d")' => '03-01'];
        }
        $query->andWhere($or);
        $persons = $query->all();

        $count = 0;
        foreach ($persons as $person) {

            if (trim($person->office->birthday_greeting_card_template_subject) != '' &&
                trim($person->office->birthday_greeting_card_template_message) != ''
            ) {
                $personGreetingCard = new PersonGreetingCard();
                $personGreetingCard->person_id = $person->id;
                $personGreetingCard->email_to = $person->corporate_email;
                $personGreetingCard->subject = $person->makeBirthdayGreetingCardSubject();
                $personGreetingCard->message = $person->makeBirthdayGreetingCardMessage();

                if ($personGreetingCard->validate()) {
                    if (!$personGreetingCard->save()) {
                        $errors[] = $personGreetingCard->getFirstErrorAsString();
                    } else {
                        $count++;
                    }
                }
            }
        }


        $holidays = Holiday::find()->byDate(date('Y-m-d'))->all();
        foreach ($holidays as $holiday) {

            if (trim($holiday->greeting_card_template_subject) != '' &&
                trim($holiday->greeting_card_template_message) != ''
            ) {
                $persons = Person::find()
                    ->active()
                    ->andWhere(['is', 'dismissal_date', null])
                    ->andWhere(['is not', 'corporate_email', null])
                    ->andWhere(['<>', 'corporate_email', ''])
                    ->all();

                foreach ($persons as $person) {
                    $personGreetingCard = new PersonGreetingCard();
                    $personGreetingCard->person_id = $person->id;
                    $personGreetingCard->email_to = $person->corporate_email;
                    $personGreetingCard->subject = $person->makeHolidayGreetingCardSubject($holiday);
                    $personGreetingCard->message = $person->makeHolidayGreetingCardMessage($holiday);

                    if ($personGreetingCard->validate()) {
                        if (!$personGreetingCard->save()) {
                            $errors[] = $personGreetingCard->getFirstErrorAsString();
                        } else {
                            $count++;
                        }
                    }
                }
            }
        }

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        if ($errors) {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        } else {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        }
        $cronAnswer->answer = json_encode(['errors' => $errors, 'count' => $count]);
        $cronAnswer->save();
    }


    /**
     * Отправка подготовленных поздравлений с праздниками
     * @param $limit integer ограничение для отправки писем партиями
     */
    public function actionSendGreetingCards($limit = 50)
    {
        $personGreetingCards = PersonGreetingCard::find()->pending()->limit($limit)->all();

        $errors = [];
        $count = 0;
        foreach ($personGreetingCards as $personGreetingCard) {

            $sent = true;
            try {
                $message = new SwiftMessage();
                $message->setFrom(Yii::$app->params['infoMailAddress']);
                $message->setTo($personGreetingCard->email_to);
                $message->setSubject($personGreetingCard->subject);
                $message->setTextBody($personGreetingCard->message);
                $message->send();
            } catch (\Exception $e) {
                $sent = false;
                $errors[] = 'send message error ' . $e->getMessage();
            }

            if ($sent) {
                $personGreetingCard->status = PersonGreetingCard::STATUS_SENT;
                $personGreetingCard->send_at = time();
            } else {
                $personGreetingCard->status = PersonGreetingCard::STATUS_ERROR;
            }

            if (!$personGreetingCard->save()) {
                $errors[] = $personGreetingCard->getFirstErrorAsString();
            } else {
                $count++;
            }
        }

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        if ($errors) {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        } else {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        }
        $cronAnswer->answer = json_encode(['errors' => $errors, 'count' => $count]);
        $cronAnswer->save();
    }

    /**
     * Создание автоматических штрафов сотрудникам
     * @param $days integer по умолчанию смотрим отработку только за сегодня
     */
    public function actionMakeAutoPenalty($days = 0)
    {
        $forbiddenTimeLimit = PenaltyType::PENALTY_AUTO_SOCIAL_MEDIA_LIMIT * 60;
        $lateTimeLimit = PenaltyType::PENALTY_AUTO_10_MIN_LATE_LIMIT * 60;

        $query = CrocotimeWorkTime::find()
            ->leftJoin(PersonLink::tableName(),
                PersonLink::tableName() . '.foreign_id=' . CrocotimeWorkTime::tableName() . '.crocotime_employee_id and ' .
                PersonLink::tableName() . '.type = "' . PersonLink::TYPE_CROCOTIME . '"')
            ->leftJoin(Person::tableName(), Person::tableName() . '.id=' . PersonLink::tableName() . '.person_id')
            ->leftJoin(Office::tableName(), Office::tableName() . '.id=' . Person::tableName() . '.office_id')
            ->where([
                'or',
                ['>=', CrocotimeWorkTime::tableName() . '.forbidden_time', $forbiddenTimeLimit],
                ['>=', CrocotimeWorkTime::tableName() . '.early_end_time', $lateTimeLimit],
                ['>=', CrocotimeWorkTime::tableName() . '.late_time', $lateTimeLimit],
            ])
            ->andWhere([
                '>=',
                CrocotimeWorkTime::tableName() . '.date',
                date('Y-m-d', strtotime('-' . $days . ' days'))
            ])
            ->andWhere([
                Office::tableName() . '.auto_penalty' => 1
            ]);

        $crocotimeWorkTimes = $query->all();

        $errors = [];
        $count = 0;

        if ($crocotimeWorkTimes) {
            $penaltyTypeLate = PenaltyType::findOne(PenaltyType::PENALTY_AUTO_10_MIN_LATE_ID);
            $penaltyTypeSocial = PenaltyType::findOne(PenaltyType::PENALTY_AUTO_SOCIAL_MEDIA_ID);

            foreach ($crocotimeWorkTimes as $crocotimeWorkTime) {

                $person = Person::find()->byCrocotimeId($crocotimeWorkTime->crocotime_employee_id)->one();
                if ($person) {

                    $date = $crocotimeWorkTime->date;
                    if ($crocotimeWorkTime->forbidden_time >= $forbiddenTimeLimit && $penaltyTypeSocial) {
                        $result = Penalty::createPenalty($person->id, $date, $penaltyTypeSocial, 'Forbidden Time: ' . round($crocotimeWorkTime->forbidden_time / 60) . ' min');
                        if ($result === true) {
                            $count++;
                            Yii::$app->notification->send(
                                Notification::TRIGGER_MAKE_AUTO_PENALTY,
                                [
                                    'person' => $person->name,
                                    'office' => $person->office->name,
                                    'penalty' => self::PENALTY_TYPE_SOCIAL,
                                    'date' => Yii::$app->formatter->asDate($date, 'php:d.m.Y'),
                                ],
                                (int)$person->office->country_id
                            );
                        } else if (is_string($result)) {
                            $errors[] = $result;
                        }
                    }

                    if ($crocotimeWorkTime->late_time >= $lateTimeLimit && $penaltyTypeLate) {
                        $result = Penalty::createPenalty($person->id, $date, $penaltyTypeLate, 'Late Time: ' . round($crocotimeWorkTime->late_time / 60) . ' min');
                        if ($result === true) {
                            $count++;
                            Yii::$app->notification->send(
                                Notification::TRIGGER_MAKE_AUTO_PENALTY,
                                [
                                    'person' => $person->name,
                                    'office' => $person->office->name,
                                    'penalty' => self::PENALTY_TYPE_LATE,
                                    'date' => Yii::$app->formatter->asDate($date, 'php:d.m.Y'),
                                ],
                                (int)$person->office->country_id
                            );
                        } else if (is_string($result)) {
                            $errors[] = $result;
                        }
                    }

                    if ($crocotimeWorkTime->early_end_time >= $lateTimeLimit && $penaltyTypeLate) {
                        $result = Penalty::createPenalty($person->id, $date, $penaltyTypeLate, 'Early End Time: ' . round($crocotimeWorkTime->early_end_time / 60) . ' min');
                        if ($result === true) {
                            $count++;
                            Yii::$app->notification->send(
                                Notification::TRIGGER_MAKE_AUTO_PENALTY,
                                [
                                    'person' => $person->name,
                                    'office' => $person->office->name,
                                    'penalty' => self::PENALTY_TYPE_LATE,
                                    'date' => Yii::$app->formatter->asDate($date, 'php:d.m.Y'),
                                ],
                                (int)$person->office->country_id
                            );
                        } else if (is_string($result)) {
                            $errors[] = $result;
                        }
                    }
                }
            }
        }

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        if ($errors) {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        } else {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        }
        $cronAnswer->answer = json_encode([
            'errors' => $errors,
            'crocotimes' => sizeof($crocotimeWorkTimes),
            'penalty' => $count
        ]);
        $cronAnswer->save();
    }

    /**
     * Формирование + отправка расчетки сотрудникам зп проекта
     */
    public function actionSendSalarySheet()
    {
        $monthlyStatements = MonthlyStatement::find()->closed()->notSent()->all();
        if (!$monthlyStatements) {
            return false;
        }
        $errors = [];
        $count = 0;
        foreach ($monthlyStatements as $monthlyStatement) {
            $monthlyStatementData = MonthlyStatementData::find()->byStatement($monthlyStatement->id)->notSent()->all();
            if ($monthlyStatementData) {
                foreach ($monthlyStatementData as $data) {
                    /** @var $data MonthlyStatementData */
                    if ($data->salary_sheet_file) {
                        $person = Person::findOne($data->person_id);
                        if ($person) {
                            if ($person->corporate_email) {
                                try {
                                    $message = new SwiftMessage();
                                    $message->setFrom(Yii::$app->params['infoMailAddress']);
                                    $message->setTo($person->corporate_email);

                                    $subject = "Salary calculation " . Yii::$app->formatter->asDate($monthlyStatement->date_from, 'php:F');

                                    $text = "Dear Employee!" . PHP_EOL .
                                        "You salary calculation for worked month is sent you in this email." . PHP_EOL .
                                        "Please find it in the attachment." . PHP_EOL .
                                        "Best regards, PhilomelaCall" . PHP_EOL;

                                    $message->setSubject($subject);
                                    $message->setTextBody($text);
                                    $message->attach(MonthlyStatement::getSalaryFilePath($data->person_id) . DIRECTORY_SEPARATOR . $data->salary_sheet_file,
                                        ['fileName' => $data->salary_sheet_file]
                                    );
                                    $message->send();
                                    $count++;
                                    $data->sent_at = time();
                                    if (!$data->save()) {
                                        $errors[] = $data->getFirstErrorAsString();
                                    } else {
                                        $count++;
                                    }
                                } catch (\Exception $e) {
                                    $errors[] = 'Send message error: ' . $e->getMessage();
                                }
                            } else {
                                $errors[] = 'No person email. ID = ' . $data->id . '. person_id = ' . $data->person_id;
                            }
                        } else {
                            $errors[] = 'Person not found. ID = ' . $data->id . '. person_id = ' . $data->person_id;
                        }
                    } else {
                        $errors[] = 'File not ready. ID = ' . $data->id;
                    }
                }
            }
            $monthlyStatement->sent_at = time();
            if (!$monthlyStatement->save(true, ['sent_at'])) {
                $errors[] = $monthlyStatement->getFirstErrorAsString();
            }
        }

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        if ($errors) {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        } else {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        }
        $cronAnswer->answer = json_encode(['errors' => $errors, 'count' => $count]);
        $cronAnswer->save();
        return true;
    }

    /**
     * Создание ежемесячных бонусов сотрудникам
     * Запускать 1 раз в день
     */
    public function actionMakeMonthlyBonus()
    {

        $errors = [];
        $count = 0;
        $day[date('j')] = date('j');

        $staffingBonusesQuery = StaffingBonus::find()
            ->where(['<=', 'day', date('j')]);
        if (date('j') == date('t')) {
            $staffingBonusesQuery->orWhere(['day' => StaffingBonus::DAY_LAST]);
            $staffingBonusesQuery->orWhere(['>=', 'day', date('t')]);
        }
        $staffingBonuses = $staffingBonusesQuery->all();

        $date = date('Y-m-d');
        $rate = 0;
        foreach ($staffingBonuses as $staffingBonus) {
            /** @var $staffingBonus StaffingBonus */

            if (!$rate) {
                $rate = $staffingBonus->staffing->office->country->getMinRateByDate($date);
            }

            $maximumBonus = $staffingBonus->staffing->office->country->getMaximumBonus($rate, $staffingBonus->bonusType->type);

            $persons = Person::find()
                ->byOfficeId($staffingBonus->staffing->office_id)
                ->byDesignationId($staffingBonus->staffing->designation_id)
                ->active()
                ->andWhere(['is', 'dismissal_date', null])
                ->andWhere(['=', 'approved', 1])
                ->all();

            foreach ($persons as $person) {
                $result = $person->makeMonthlyBonus($staffingBonus->bonusType, $date, $rate, $maximumBonus);
                $count += $result['count'];
                if ($result['errors']) {
                    $errors = array_merge($errors, $result['errors']);
                }
            }
        }

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        if ($errors) {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        } else {
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        }
        $cronAnswer->answer = json_encode(['errors' => $errors, 'count' => $count]);
        $cronAnswer->save();
        return true;
    }
}