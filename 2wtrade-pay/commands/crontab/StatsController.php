<?php
namespace app\commands\crontab;

use app\modules\order\models\OrderStatus;
use app\modules\administration\models\CrontabTask;
use app\modules\systemstatus\models\OrderStats;

use yii\console\Controller;
use yii\db\Query;

use Yii;

/**
 * Class StatsController
 * @package app\commands\crontab
 */
class StatsController extends Controller
{

    protected $mapTasks = [
        'sendnewrelic' => CrontabTask::TASK_SEND_STATS_NEWRELIC,
        'registerlocal' => CrontabTask::TASK_REGISTER_STATS_LOCAL,
    ];


    /**
     * Record local stats
     */
    public function actionRegisterlocal()
    {

        $stats = (new Query())
            ->select('count(o.id) as cnt, c.char_code country, d.name delivery')
            ->from('`order` o')
            ->innerJoin('country c', 'c.id = o.country_id')
            ->leftJoin('delivery d', 'd.id = o.pending_delivery_id')
            ->where('status_id=:id', ['id' => OrderStatus::STATUS_CC_APPROVED])
            ->groupBy(['o.country_id', 'o.pending_delivery_id'])
            ->all();

        foreach ($stats as $s) {
            $orderStats = new OrderStats();
            $orderStats->metric = "CC_Approved/{$s['country']}_{$s['delivery']}";
            $orderStats->value = (int)$s['cnt'];
            $orderStats->save();
        }
    }

    /**
     * Send stats to newrelic
     */
    public function actionSendnewrelic()
    {
        if (!extension_loaded('newrelic')) {
            return;
        }


        $stats = (new Query())
            ->select('count(o.id) as cnt, c.char_code country, d.name delivery')
            ->from('`order` o')
            ->innerJoin('country c', 'c.id = o.country_id')
            ->leftJoin('delivery d', 'd.id = o.pending_delivery_id')
            ->where('status_id=:id', ['id' => OrderStatus::STATUS_CC_APPROVED])
            ->groupBy(['o.country_id', 'o.pending_delivery_id'])
            ->all();


        foreach ($stats as $s) {
            newrelic_custom_metric("Custom/ApprovedNotInDelivery/{$s['country']}_{$s['delivery']}", (int)$s['cnt']);
        }

        $stats = (new Query ())
            ->select("count(o.id) cnt, c.char_code country, d.name delivery")
            ->from("`order` o")
            ->innerJoin("delivery_request dr", "dr.order_id=o.id")
            ->innerJoin("country c", "c.id=o.country_id")
            ->innerJoin("delivery d", "d.id=dr.delivery_id")
            ->where("status_id=:id", ['id' => OrderStatus::STATUS_DELIVERY_REJECTED])
            ->groupBy(["o.country_id", "dr.delivery_id"])
            ->all();


        foreach ($stats as $s) {
            newrelic_custom_metric("Custom/RejectetByDelivery/{$s['country']}_{$s['delivery']}", (int)$s['cnt']);
        }


        $stats = (new Query ())
            ->select("count(dr.id) as cnt, dr.status status, d.name delivery")
            ->from("`delivery_request` dr")
            ->innerJoin("delivery d", "d.id=dr.delivery_id")
            ->innerJoin("country c", "c.id=d.country_id")
            ->where("dr.status<>:status", ['status' => 'done'])
            ->groupBy(["status", "delivery"])
            ->all();

        foreach ($stats as $s) {
            newrelic_custom_metric("Custom/DeliveryQueueSize/{$s['delivery']}/{$s['status']}", (int)$s['cnt']);
        }


        $stats = (new Query ())
            ->select("count(ccr.id) as cnt, ccr.status status, c.char_code country")
            ->from("`call_center_request` ccr")
            ->leftJoin("call_center cc", "cc.id = ccr.call_center_id")
            ->leftJoin("country c", "cc.country_id = c.id")
            ->where("ccr.status<>:status", ['status' => 'done'])
            ->groupBy(["status", "country"])
            ->all();

        foreach ($stats as $s) {
            newrelic_custom_metric("Custom/CallcenterQueueSize/{$s['country']}/{$s['status']}", (int)$s['cnt']);
        }
    }
}
