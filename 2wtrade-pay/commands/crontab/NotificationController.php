<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\notification\SkypeQueue;
use app\helpers\i18n\Formatter;
use app\models\Currency;
use app\models\Notification;
use app\models\Source;
use app\modules\administration\models\CrontabTask;
use app\modules\api\models\ApiLog;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\finance\components\EstimateFinanceProvider;
use app\modules\finance\components\ReportFormCountryCurrentBalance;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\ReportDeliveryDebts;
use app\modules\webhook\controllers\SkypeController;
use app\modules\webhook\models\SkypeUser;
use yii;
use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\models\DeliveryContract;
use app\modules\deliveryreport\models\DeliveryReport;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\modules\order\models\Lead;

class NotificationController extends CrontabController
{
    const PRECISION = 2;

    /** @var bool */
    public $sendNotifier = false;

    protected $mapTasks = [
        'checkapilogsuccess' => CrontabTask::TASK_LEAD_LOG_SUCCESS_ABSENT_10MINUTS,
        'checkdeliveryreportactuality' => CrontabTask::TASK_DELIVERY_REPORT_NOTIFICATION_DELAY,
        'checksendinglist' => CrontabTask::TRIGGER_ORDER_LOGISTIC_CHECK_SENDING_LIST,
        'checkcallcenterrequestactuality' => CrontabTask::TASK_CALL_CENTER_REQUEST_ACTUALITY,
        'requestserrorsummary' => CrontabTask::TASK_REQUESTS_ERROR_SUMMARY,
        'profitstattoskype' => CrontabTask::TASK_PROFIT_STAT_TO_SKYPE,
        'sendskypenotifications' => CrontabTask::TASK_SEND_SKYPE_NOTIFICATIONS,
        'debtsthreemonth' => CrontabTask::TASK_DEBTS_NOTIFICATION_FOR_THREE_MONTH,
    ];

    public function actionCheckApiLogSuccess()
    {

        $this->log('Start finding last success log on the api_log...');

        $before10minuts = time() + 10 * 60;

        $apiLogs = ApiLog::find()
            ->where([ApiLog::tableName() . '.status' => ApiLog::STATUS_SUCCESS])
            ->orderBy([ApiLog::tableName() . '.id' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->all();

        if (empty($apiLogs)) {
            $this->sendNotifier = true;
        } else {
            if ($apiLogs[0]['created_at'] < $before10minuts) {
                $this->sendNotifier = true;
            }
        }

        if ($this->sendNotifier) {
            $this->log('send notification');
            $Notification = yii::$app->get('notification');
            $lastSuccessTime = isset($apiLogs[0]['created_at']) ? date('d/m/Y H:i:s', $apiLogs[0]['created_at']) : yii::t('common', 'не определено');

            $Notification->send(Notification::TRIGGER_LEAD_ABSENT_10MINUTS, ['text' => $lastSuccessTime], null);
        }

        $this->log('end finding last success log on the api_log...');

    }

    public function actionCheckDeliveryReportActuality()
    {
        $success = true;

        try {
            $reports = DeliveryReport::find()
                ->select(new Expression('max(' . DeliveryReport::tableName() . '.created_at) created_at, period_to, delivery.name, ' . DeliveryReport::tableName() . '.id, contact.email, user.email tm_email, user.username tm_name'))
                ->innerJoin(Delivery::tableName() . ' delivery', 'delivery.id = ' . DeliveryReport::tableName() . '.delivery_id and delivery.active = 1')
                ->innerJoin([
                    'contract' => DeliveryContract::find()
                        ->select(new Expression('max(date_to) date_to, delivery_id'))
                        ->groupBy('delivery_id')
                ], 'contract.delivery_id = ' . DeliveryReport::tableName() . '.delivery_id')
                ->leftJoin(DeliveryContacts::tableName() . ' contact', 'contact.delivery_id = ' . DeliveryReport::tableName() . '.delivery_id')
                ->leftJoin(Country::tableName() . ' country', 'country.id = delivery.country_id')
                ->leftJoin(User::tableName() . ' user', 'user.id = country.curator_user_id')
                ->where([DeliveryReport::tableName() . '.type' => DeliveryReport::TYPE_TRANSITIONAL])
                ->andWhere(new Expression('contract.date_to is null or contract.date_to > unix_timestamp(now())'))
                ->groupBy(DeliveryReport::tableName() . '.delivery_id')
                ->having(new Expression('created_at < unix_timestamp(DATE_SUB(date(now()), INTERVAL 4 day)) and (period_to is null or period_to > unix_timestamp(now()))'))
                ->asArray()->all();

            if ($reports) {
                $cronAnswer = new CrontabTaskLogAnswer();
                $cronAnswer->log_id = $this->log->id;
                $cronAnswer->data = Yii::t('common', 'Отсутствуют свежие данные по {num} КС.', ['num' => count($reports)]);
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $cronAnswer->save();

                foreach ($reports as $report) {
                    if (!empty($report['email']) || !empty($report['tm_email'])) {
                        $message = Yii::$app->mailer->compose('@app/mail/delivery-report/mail', [
                            'delivery' => $report['name'],
                            'tm_name' => $report['tm_name'],
                            'tm_email' => $report['tm_email'],
                        ])
                            ->setFrom(Yii::$app->params['noReplyEmail'])
                            ->setSubject('reminder: to send status report to 2Wtrade');
                        if (empty($report['tm_email']) || empty($report['email'])) {
                            $message->setTo($report['email'] ?? $report['tm_email']);
                        } else {
                            $message->setTo($report['email'])
                                ->setCc($report['tm_email']);
                        }
                        if (!$message->send()) {
                            $cronAnswer = new CrontabTaskLogAnswer();
                            $cronAnswer->log_id = $this->log->id;
                            $cronAnswer->data = Yii::t('common', 'Ошибка при отправке сообщения.');
                            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $cronAnswer->save();
                        }
                    } else {
                        $cronAnswer = new CrontabTaskLogAnswer();
                        $cronAnswer->log_id = $this->log->id;
                        $cronAnswer->data = Yii::t('common', 'Отсутствуют контакты для отправки оповещения для КС № {id}', ['id' => $report['id'] ?? 'undefined']);
                        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                        $cronAnswer->save();
                    }
                }
            }
        } catch (\Exception $e) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->data = json_encode([
                'message' => Yii::t('common', 'Выброшено исключение.'),
                'error' => $e->getMessage()
            ], JSON_UNESCAPED_UNICODE);
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            $success = false;
        }

        return $success;
    }

    /**
     * @param int $minutes
     * @return bool
     */
    public function actionCheckCallCenterRequestActuality(int $minutes = 15)
    {
        $minutes = abs($minutes);
        if ($minutes < 1) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->data = Yii::t('common', 'Минимальбный интервал проверки состовляет 1 минуту!');
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            return false;
        }

        $success = true;

        try {
            $request = CallCenterRequest::find()
                ->select(new Expression('count(' . CallCenterRequest::tableName() . '.id) as num, ' . Country::tableName() . '.*'))
                ->joinWith(['callCenter.country'], false)
                ->where(['status' => CallCenterRequest::STATUS_IN_PROGRESS])
                ->andWhere(new Expression(Country::tableName() . '.active = 1 and ' . Country::tableName() . '.is_stop_list != 1'))
                ->andWhere(new Expression(CallCenterRequest::tableName() . '.foreign_id IS NULL OR ' . CallCenterRequest::tableName() . '.foreign_id = 0'))
                ->andWhere(new Expression(CallCenterRequest::tableName() . '.updated_at < NOW() - INTERVAL ' . $minutes . ' MINUTE'))
                ->groupBy(Country::tableName() . '.id')
                ->asArray()->all();
            if ($request) {
                foreach ($request as $data) {
                    Yii::$app->notification->send(
                        Notification::TRIGGER_CALL_CENTER_REPORT_ACTUALITY,
                        [
                            'count' => (int)$data['num'],
                            'country' => $data['name'],
                            'minute' => $minutes
                        ],
                        (int)$data['id']
                    );
                }
            }
        } catch (\Exception $e) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->data = json_encode([
                'message' => Yii::t('common', 'Выброшено исключение.'),
                'error' => $e->getMessage()
            ], JSON_UNESCAPED_UNICODE);
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            $success = false;
        }

        return $success;
    }

    public function actionCheckSendingList()
    {
        try {
            $orderLogisticLists = OrderLogisticList::find()
                ->select([
                    'updated_at' => 'max(' . OrderLogisticList::tableName() . '.updated_at)',
                    'count' => 'count(' . OrderLogisticList::tableName() . '.id)',
                    'country' => 'c.name',
                    'delivery' => 'd.name',
                    'country_id' => 'c.id'
                ])
                ->where([
                    'status' => [
                        OrderLogisticList::STATUS_PENDING,
                        OrderLogisticList::STATUS_DOWNLOADED,
                        OrderLogisticList::STATUS_BARCODING
                    ]
                ])
                ->leftJoin(Country::tableName() . ' c', OrderLogisticList::tableName() . '.country_id = c.id')
                ->leftJoin(Delivery::tableName() . ' d', OrderLogisticList::tableName() . '.delivery_id = d.id')
                ->andWhere(new Expression(OrderLogisticList::tableName() . '.updated_at < unix_timestamp(DATE_SUB(date(now()), INTERVAL 1 hour))'))
                ->groupBy(['c.id', 'delivery_id'])
                ->asArray()->all();

            if ($orderLogisticLists) {
                foreach ($orderLogisticLists as $item) {
                    $cronAnswer = new CrontabTaskLogAnswer();
                    $cronAnswer->log_id = $this->log->id;
                    $cronAnswer->data = json_encode([
                        'size_of_lists' => sizeof($orderLogisticLists),
                        'country' => $item['country'],
                        'delivery' => $item['delivery'],
                        'num' => $item['count'],
                        'last' => Yii::$app->formatter->asDatetime($item['updated_at'], Formatter::getDatetimeFormat()),
                    ]);
                    $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                    $cronAnswer->save();

                    Yii::$app->notification->send(
                        Notification::TRIGGER_ORDER_LOGISTIC_CHECK_SENDING_LIST,
                        [
                            'country' => $item['country'],
                            'delivery' => $item['delivery'],
                            'num' => $item['count'],
                            'last' => Yii::$app->formatter->asDatetime($item['updated_at'], Formatter::getDatetimeFormat()),
                        ],
                        $item['country_id']
                    );
                }
            }
        } catch (\Exception $e) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->data = json_encode([
                'message' => Yii::t('common', 'Выброшено исключение.'),
                'error' => $e->getMessage()
            ], JSON_UNESCAPED_UNICODE);
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            return false;
        }
        return true;
    }

    /**
     * @param int $errorStringLimit
     */
    public function actionRequestsErrorSummary($errorStringLimit = 50)
    {

        $finalStatuses = ArrayHelper::getColumn(OrderStatus::getAllFinalStatuses(), 'id');

        $orderQuery = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->joinWith('country', false)
            ->where([
                Country::tableName() . '.active' => 1,
                Country::tableName() . '.is_stop_list' => 0,
                Country::tableName() . '.is_partner' => 0,
                Country::tableName() . '.is_distributor' => 0,
            ])
            ->andWhere([
                'not in',
                Order::tableName() . '.status_id',
                $finalStatuses
            ]);


        $callQuery = clone $orderQuery;
        $callQuery
            ->addSelect([
                'error' => 'SUBSTRING(' . CallCenterRequest::tableName() . '.api_error, 1, ' . $errorStringLimit . ')'
            ])
            ->joinWith('callCenterRequest', false)
            ->andWhere([CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_ERROR])
            ->groupBy(['country_id', 'error']);

        foreach ($callQuery->asArray()->all() as $item) {
            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_REQUESTS_ERROR_SUMMARY,
                [
                    'count' => $item['count'],
                    'message' => $item['error'],
                ],
                (int)$item['country_id']
            );
        }

        $deliveryQuery = clone $orderQuery;
        $deliveryQuery
            ->addSelect([
                'error' => 'SUBSTRING(' . DeliveryRequest::tableName() . '.api_error, 1, ' . $errorStringLimit . ')'
            ])
            ->joinWith('deliveryRequest', false)
            ->andWhere([DeliveryRequest::tableName() . '.status' => DeliveryRequest::STATUS_ERROR])
            ->groupBy(['country_id', 'error']);

        foreach ($deliveryQuery->asArray()->all() as $item) {
            Yii::$app->notification->send(
                Notification::TRIGGER_DELIVERY_REQUESTS_ERROR_SUMMARY,
                [
                    'count' => $item['count'],
                    'message' => $item['error'],
                ],
                (int)$item['country_id']
            );
        }
    }

    /**
     * @param string $ids
     * @return bool
     */
    public function actionProfitStatToSkype(string $ids)
    {
        $countMonth = 5;
        $ids = array_map('trim', explode(',', $ids));
        foreach ($ids as $key => $id) {
            if ($id != intval($id)) {
                $cronAnswer = new CrontabTaskLogAnswer();
                $cronAnswer->log_id = $this->log->id;
                $cronAnswer->answer = Yii::t('common', 'Не верный формат id пользователя ({id})', ['id' => $id]);
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $cronAnswer->save();
                unset($ids[$key]);
            }
        }
        if (!count($ids)) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->answer = Yii::t('common', 'Нет корректных id пользователей.');
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            return false;
        }

        $users = SkypeUser::find()->joinWith(['skypeUserLinks'])->where(['skype_user_id' => $ids])->all();
        if (!count($users)) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->answer = Yii::t('common', 'Указанные пользователи не найдены.');
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            return false;
        }
        $countriesActive = Country::find()->active()->notStopped()->notPartner()->notDistributor()->asArray()->all();
        $countries = [];
        $countriesByUser = [];
        foreach ($users as $skypeUser) {
            foreach ($skypeUser->skypeUserLinks as $link) {
                if ($link->user->isSuperadmin) {
                    $countriesByUser[$link->skype_user_id] = array_map('intval', array_column($countriesActive, 'id'));
                    $countries = $countriesByUser[$link->skype_user_id];
                } else {
                    foreach ($link->user->countries as $country) {
                        if ($country->active && !$country->is_stop_list && !$country->is_partner && !$country->is_distributor) {
                            $countriesByUser[$link->skype_user_id][] = $country->id;
                            $countries[] = $country->id;
                        }
                    }
                }
            }
        }
        if (!count($countries)) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->answer = Yii::t('common', 'Для указанных пользователей нет доступных стран.');
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            return false;
        }
        $reportCountries = ReportExpensesCountry::find()
            ->where(['country_id' => array_unique($countries)])
            ->active()
            ->orderBy(['name' => SORT_ASC])
            ->all();
        $date = date('m.Y');
        $dates = [];
        for ($i = 0; $i < $countMonth; $i++) {
            $dates[$i]['from'] = date('Y-m-d 00:00:00', strtotime('01.' . $date . ' - ' . $i . ' MONTH'));
            $dates[$i]['to'] = date('Y-m-t 23:59:59', strtotime('01.' . $date . ' - ' . $i . ' MONTH'));
        }
        foreach ($reportCountries as $country) {
            for ($i = 0; $i < $countMonth; $i++) {
                $reportForm = new ReportFormCountryCurrentBalance();
                $data[$i][$country->country_id] = $this->getData($country->country_id, $dates[$i]['from'], $dates[$i]['to']);
                if ($profits = $reportForm->getProfit([
                    'countries' => [$country],
                    'month' => (int)date('m', strtotime($dates[$i]['from'])),
                    'year' => (int)date('Y', strtotime($dates[$i]['from'])),
                ])) {
                    $data[$i][$country->country_id]['profit'] = $profits[0]['profit'] ?? 0;
                }
                $data[$i][$country->country_id]['countryName'] = $country->country->name;
            }
        }
        $months = [
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
        ];
        foreach ($countriesByUser as $userID => $countries) {
            $countries = array_unique($countries);
            if ($countCountries = count($reportCountries) - count(array_diff(array_column($reportCountries, 'country_id'), $countries))) {
                $text = "Операционный баланс\nПрибыль/ср. чек/выкуп";
                foreach ($data as $month => $countryData) {
                    $text .= "\n" . ArrayHelper::getValue($months, intval(date('m', strtotime($dates[$month]['from']))) - 1) . "\n";
                    $total = ['profit' => 0, 'avgbuyoutcheck' => 0, 'avgbuyoutcount' => 0];
                    foreach ($countryData as $countryID => $item) {
                        if (in_array($countryID, $countries)) {
                            $item['profit'] = round($item['profit'] ?? 0, self::PRECISION);
                            $item['avgbuyoutcheck'] = round(($item['buyoutincome'] ?? 0) / (!empty($item['buyoutcount']) ? $item['buyoutcount'] : 1), self::PRECISION);
                            $item['avgbuyoutcount'] = round(($item['buyoutcount'] ?? 0) * 100 / (!empty($item['approvecount']) ? $item['approvecount'] : 1), self::PRECISION);
                            $text .= "{$item['countryName']}: " . \Yii::$app->formatter->asCurrency($item['profit'], 'usd') . ' / ' . \Yii::$app->formatter->asCurrency($item['avgbuyoutcheck'], 'usd') . " / {$item['avgbuyoutcount']}%\n";
                            $total['profit'] += $item['profit'];
                            $total['avgbuyoutcheck'] += $item['avgbuyoutcheck'];
                            $total['avgbuyoutcount'] += $item['avgbuyoutcount'];
                        }
                    }
                    $total['avgbuyoutcheck'] = round($total['avgbuyoutcheck'] / $countCountries, self::PRECISION) ?? 0;
                    $total['avgbuyoutcount'] = round($total['avgbuyoutcount'] / $countCountries, self::PRECISION) ?? 0;
                    $text .= 'Всего: '. \Yii::$app->formatter->asCurrency($total['profit'], 'usd') . ' / ' . \Yii::$app->formatter->asCurrency($total['avgbuyoutcheck'], 'usd') ." / {$total['avgbuyoutcount']}%\n";
                }
                SkypeController::sendUserById($userID, $text);
            }
        }
        return true;
    }

    /**
     * only for actionProfitStatToSkype
     * @param int $country_id
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function getData(int $country_id, string $dateFrom, string $dateTo)
    {
        $timeFrom = Yii::$app->formatter->asTimestamp($dateFrom);
        $timeTo = Yii::$app->formatter->asTimestamp($dateTo);

        $mapStatuses = [
            'approve_count' => OrderStatus::getApproveCCList(),
            'delivery_count' => OrderStatus::getInDeliveryAndFinanceList(),
            'in_process_count' => OrderStatus::getProcessDeliveryList(),
            'buyout_count' => OrderStatus::getBuyoutList(),
            'money_received_count' => OrderStatus::getOnlyMoneyReceivedList(),
            'refuse_count' => OrderStatus::getNotBuyoutList(),
            'lead_count' => OrderStatus::getAllList()
        ];
        $tmp = [];
        foreach ($mapStatuses as $statuses) {
            $tmp = array_merge($tmp, $statuses);
        }
        $mapStatuses['all'] = array_unique($tmp);

        $query = new \app\modules\report\extensions\Query();
        $query->from([Order::tableName()]);
        $fromQuery = (new  \app\modules\report\extensions\Query())->from(Order::tableName())
            ->leftJoin(['original_order_table' => Order::tableName()], Order::tableName() . ".id = `original_order_table`.duplicate_order_id")
            ->where([Order::tableName() . '.duplicate_order_id' => null, Order::tableName() . '.country_id' => $country_id])
            ->select([
                'status_id' => '(CASE WHEN `original_order_table`.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') THEN `original_order_table`.status_id ELSE ' . Order::tableName() . '.status_id END)',
                'id' => 'COALESCE(`original_order_table`.id, `order`.id)'
            ])->andWhere(['=', new Expression('COALESCE(`original_order_table`.source_id, ' . Order::tableName() . '.source_id)'), Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
        $query->innerJoin(['unique_order_table' => $fromQuery], '`unique_order_table`.id = ' . Order::tableName() . '.id');

        $usdCurrency = Currency::getUSD();
        $convertPriceTotalToCurrencyId = $usdCurrency->id;

        // Количество заказов в статусе
        foreach ($mapStatuses as $name => $statuses) {
            $query->addInConditionCount('unique_order_table.status_id', $statuses, $name);
        }
        // Доход по выкупам
        $case = $query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency, ' . $convertPriceTotalToCurrencyId . ')',
            'unique_order_table.status_id',
            $mapStatuses['buyout_count']
        );
        $query->addSumCondition($case, 'buyout_income');

        $query->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');

        $query->andWhere(['>=', Lead::tableName() . '.ts_spawn', $timeFrom]);
        $query->andWhere(['<=', Lead::tableName() . '.ts_spawn', $timeTo]);
        $query->andWhere(['in', 'unique_order_table.status_id', $mapStatuses['all']]);

        $subQuery = new \app\modules\report\extensions\Query();
        $subQuery->from([OrderProduct::tableName()]);
        $subQuery->select([
            'order_id' => OrderProduct::tableName() . '.order_id',
            'product_id' => OrderProduct::tableName() . '.product_id',
            'quantity' => 'SUM(' . OrderProduct::tableName() . '.quantity)'
        ]);
        $subQuery->leftJoin(Order::tableName(), Order::tableName().'.id = ' . OrderProduct::tableName().'.order_id');
        $subQuery->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');
        $subQuery->innerJoin(LeadProduct::tableName(), LeadProduct::tableName() . '.order_id = ' . OrderProduct::tableName() . '.order_id AND ' . LeadProduct::tableName() . '.product_id = ' . OrderProduct::tableName() . '.product_id');
        $subQuery->where([Order::tableName().'.country_id' => $country_id, Order::tableName().'.source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
        $subQuery->andWhere(['>=', Lead::tableName() . '.ts_spawn', $timeFrom]);
        $subQuery->andWhere(['<=', Lead::tableName() . '.ts_spawn', $timeTo]);
        $subQuery->groupBy(['order_id', 'product_id']);
        $aliasSq0 = 'sq0';
        $query->innerJoin([$aliasSq0 => $subQuery], $aliasSq0 . '.order_id=' . Order::tableName() . '.id');

        $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['approve_count']);
        $query->addSumCondition($condition, 'productquantity');
        $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['buyout_count']);
        $query->addSumCondition($condition, 'productbuyoutquantity');
        $condition = $subQuery->buildCaseCondition($aliasSq0 . '.quantity', 'unique_order_table.status_id', $mapStatuses['refuse_count']);
        $query->addSumCondition($condition, 'productrefusequantity');

        $query->addSelect([
            EstimateFinanceProvider::GROUP_BY_PRODUCT => $aliasSq0 . '.product_id'
        ]);

        $subQuery = new \app\modules\report\extensions\Query();
        $subQuery->from([CallCenterCheckRequest::tableName()]);
        $subQuery->select([
            'count' => 'count(' . CallCenterCheckRequest::tableName() . '.id)',
            'product_id' => LeadProduct::tableName() . '.product_id',
        ]);
        $subQuery->leftJoin(Order::tableName(), Order::tableName() . '.id=' . CallCenterCheckRequest::tableName() . '.order_id');
        $subQuery->leftJoin(LeadProduct::tableName(), LeadProduct::tableName() . '.order_id=' . Order::tableName() . '.id');
        $subQuery->andWhere([Order::tableName() . '.country_id' => $country_id]);
        $subQuery->andWhere(['>=', CallCenterCheckRequest::tableName() . '.done_at', $timeFrom]);
        $subQuery->andWhere(['<=', CallCenterCheckRequest::tableName() . '.done_at', $timeTo]);
        $subQuery->groupBy(['product_id']);
        $aliasSq2 = 'sq2';

        $query->leftJoin([$aliasSq2 => $subQuery], $aliasSq2 . '.product_id=' . $aliasSq0 . '.product_id');
        $query->addSelect(['recallcount' => $aliasSq2 . '.count']);

        $query->indexBy(EstimateFinanceProvider::GROUP_BY_PRODUCT);
        $query->groupBy(EstimateFinanceProvider::GROUP_BY_PRODUCT);

        $data = $query->all();
        $result = [];
        foreach ($data as $item) {
            foreach ($item as $key => $val) {
                $result[$key] += $val;
            }
        }

        return $result;
    }

    /**
     * @throws yii\base\InvalidConfigException
     */
    public function actionDebtsThreeMonth(): void
    {
        $dateEnd = date('Y-m-01');
        $dateStart = date('Y-m-01', strtotime(' -2 MONTH'));
        $debts = ReportDeliveryDebts::find()
            ->joinWith('delivery.country', false)
            ->where([
                'is_partner' => 0,
                'is_distributor' => 0,
            ])
            ->andWhere([
                'is not',
                ReportDeliveryDebts::tableName() . '.created_at',
                null
            ])
            ->andWhere(['AND',
                ['<=', ReportDeliveryDebts::tableName() . '.month', $dateEnd],
                ['>=', ReportDeliveryDebts::tableName() . '.month', $dateStart]
            ])
            ->all();

        $data = [];
        $months = [
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
        ];
        foreach ($debts as $item) {
            /** @var ReportDeliveryDebts $item */
            if (($diff = $item->decodedData['buyout_sum_total_dollars'] - $item->decodedData['charges_not_accepted_dollars']) || ($item->delivery->active && $item->delivery->country->active)) {
                $data[$item->month][$item->delivery->country->name][$item->delivery->name] = round($diff, self::PRECISION);
            }
        }
        $text = '';
        foreach ($data as $month => $countries) {
            $subText = '';
            $total = 0;
            foreach ($countries as $country => $deliveries) {
                foreach ($deliveries as $delivery => $val) {
                    $subText .= "{$country} / {$delivery} / " . Yii::$app->formatter->asCurrency($val, 'USD') . " \n";
                    $total += $val;
                }
            }
            if (!empty($subText)) {
                $text .= "\n" . ArrayHelper::getValue($months, intval(date('m', strtotime($month))) - 1)
                    . date(' Y', strtotime($month)) . ' (итого: ' . Yii::$app->formatter->asCurrency($total, 'USD') . ")\n"
                    . $subText;
            }
        }
        if (!empty($text)) {
            Yii::$app->notification->send(
                Notification::TRIGGER_DEBTS_NOTIFICATION_FOR_THREE_MONTH,
                [
                    'text' => $text,
                ]
            );
        }
    }

    public function actionSendSkypeNotifications(): void
    {
        $this->log('Sending Skype...', false);

        $queue = new SkypeQueue();
        $queue->send('always');

        $this->log('done.');
    }
}