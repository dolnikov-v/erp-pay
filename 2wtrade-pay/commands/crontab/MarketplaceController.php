<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\marketplace\abstracts\extensions\MarketPlaceWithGetOrdersByTimeIntervalInterface;
use app\modules\marketplace\components\MarketPlaceWithExtendedServices;

/**
 * Class MarketplaceController
 * @package app\commands\crontab
 */
class MarketplaceController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'createorders' => CrontabTask::TASK_MARKETPLACE_CREATE_ORDERS
    ];

    /**
     * @param MarketPlaceWithExtendedServices $marketplace
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     */
    protected function saveOrders(MarketPlaceWithExtendedServices $marketplace, \DateTime $dateFrom, \DateTime $dateTo)
    {
        if ($marketplace instanceof MarketPlaceWithGetOrdersByTimeIntervalInterface) {
            $result = $marketplace->createOrders($dateFrom, $dateTo);
            $log = [
                'log_id' => $this->log->id,
                'url' => $result['url'] ?? null
            ];

            if (!$result['error']) {
                $log['status'] = CrontabTaskLogAnswer::STATUS_FAIL;
                $this->log($result['message']);
            } else {
                $log['status'] = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $this->log('Saving orders: ' . implode(', ', $result['data']));
            }

            $crontabTaskLogAnswer = new CrontabTaskLogAnswer($log);
            $crontabTaskLogAnswer->data = $result['data_log'];

            $answer = [
                'ordersCount' => count($result['data']),
                'orderIds' => !empty($result['data']) ? implode(', ', $result['data']) : '-'
            ];

            if (!empty($result['message'])) {
                $answer['message'] = $result['message'];
            }

            $crontabTaskLogAnswer->answer = json_encode($answer);
            $crontabTaskLogAnswer->save();
        }
    }

    /**
     * @param string|null $marketplaceId
     * @param string|null $from
     * @param string|null $to
     * @throws \Exception
     */
    public function actionCreateOrders(string $marketplaceId = null, string $from = null, string $to = null)
    {
        if ($from and $to) {
            $dateFrom = new \DateTime($from);
            $dateTo = new \DateTime($to);
        } else {
            $dateTo = new \DateTime();
            $dateFrom = clone $dateTo;
            $dateFrom->sub(new \DateInterval('P1D'));
        }

        if ($marketplaceId) {
            $marketplace = \Yii::$app->getModule('marketplace')->getMarketPlaceById($marketplaceId);
            $this->saveOrders($marketplace, $dateFrom, $dateTo);
        } else {
            $marketplaces = \Yii::$app->getModule('marketplace')->getMarketplaces();
            foreach ($marketplaces as $marketplace) {
                $this->saveOrders($marketplace, $dateFrom, $dateTo);
            }
        }
    }
}
