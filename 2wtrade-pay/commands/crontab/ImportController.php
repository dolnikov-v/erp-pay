<?php
namespace app\commands\crontab;

use app\models\AccountingCost;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\components\console\CrontabController;
use app\modules\report\models\ReportDeliveryDebts;
use yii\httpclient\Client;

/**
 * Class ImportController
 * @package app\commands\crontab
 */
class ImportController extends CrontabController
{

    protected $mapTasks = [
        'accountingcosts' => CrontabTask::TASK_IMPORT_1C_COSTS,
        'olddeliverydebts' => CrontabTask::TASK_REPORT_IMPORT_OLD_DELIVERY_DEBTS,
    ];

    /**
     * @param string $dateFrom
     * @param string $dateTo
     */
    public function actionAccountingCosts($dateFrom = '', $dateTo = '')
    {
        $this->log("Start get costs from 1C...\n");

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $cronAnswer->save();

        $records = 0;
        $answer = [];
        try {

            if ($dateFrom == '')
                $dateFrom = date("Ymd", strtotime("-7days"));

            if ($dateTo == '')
                $dateTo = date("YmdHis");

            $url = "http://37.194.203.24/1CTrade/hs/Costs/" . $dateFrom . "/" . $dateTo . "/";

            $cronAnswer->url = $url;
            $cronAnswer->data = json_encode([
                'url' => $url
            ]);

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_HTTPHEADER, [
                "Authorization: Basic QWRtaW4=",
                "Cache-Control: no-cache",
                "Postman-Token: 83d91de0-bb8e-9683-32d5-599fd2650002",
            ]);
            curl_setopt($c, CURLOPT_PORT, 81);
            //curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
            //curl_setopt($c, CURLOPT_SSL_VERIFYHOST,  2);
            $result = curl_exec($c);
            $resultHttpCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
            $resultErrorNo = curl_errno($c);

            $answer['result'] = $result;
            $answer['resultHttpCode'] = $resultHttpCode;

            if ($resultErrorNo) {
                $answer['resultCurlError'] = curl_error($c);
            }

            curl_close($c);

            $encoding = mb_detect_encoding($result);
            $this->log("encoding = " . $encoding);

            if ($encoding != 'UTF-8')
                $result = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result));

            $resultXml = simplexml_load_string($result);

            if (isset($resultXml->Статья)) {

                foreach ($resultXml->Статья as $article) {

                    $date = date("Y-m-d", strtotime(trim($article->Дата)));

                    $accountingCost = AccountingCost::find()->byDate($date)->one();
                    if (!$accountingCost)
                        $accountingCost = new AccountingCost();

                    $accountingCost->date_article = $date;
                    $accountingCost->code = trim($article->КодСтатьи);
                    $accountingCost->name = trim($article->Наименование);
                    $accountingCost->country = trim($article->Страна);
                    $accountingCost->subdivision = trim($article->Подразделение);
                    $accountingCost->turnover = floatval(trim($article->СуммаОборот));
                    $accountingCost->incoming = floatval(trim($article->СуммаПриход));
                    $accountingCost->outcoming = floatval(trim($article->СуммаРасход));
                    $accountingCost->save();
                    if ($accountingCost->save()) {
                        $records++;
                    } else {
                        $answer['save_error'] = 'Ошибка при сохранении ответа';
                    }
                }
            } else {
                $answer['read_error'] = "Error: Unexpected format";
                $answer['result'] = $result;
                $this->log("Error: Unexpected format");
                $cronAnswer->answer = json_encode($answer);
                $cronAnswer->save();
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            }

        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        }

        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $cronAnswer->answer = json_encode($answer);

        $cronAnswer->save();
        $this->log("Records: " . $records);
        $this->log("Done.\n");
    }


    /**
     * @param string $monthTo
     */
    public function actionOldDeliveryDebts($monthTo = '2016-09')
    {
        $urlExport = 'http://tracking.m2corp.ru/backend/report-delivery-debts/export';
        $token = 'NGU1OGM0NDIxYWQ4YmUzZDA3OTM0OGZmYjZlNTg4YzU=';

        $this->log("Start import old delivery debts..." . PHP_EOL);

        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $cronAnswer->save();

        $records = 0;
        $answer = [];
        $errors = [];
        try {

            $cronAnswer->url = $urlExport;
            $cronAnswer->data = json_encode([
                'url' => $urlExport
            ]);

            $client = new Client();
            $client->setTransport('yii\httpclient\CurlTransport');
            $response = $client->createRequest()
                ->addHeaders(['Authorization' => $token])
                ->setMethod('get')
                ->setUrl($urlExport)
                ->send();

            if ($response->isOk && $resultApi = json_decode($response->content)) {
                $this->log("ok" . PHP_EOL);

                foreach ($resultApi as $item) {
                    if ($item->month <= $monthTo) {

                        $country = Country::findOne(['name_en' => $item->country]);
                        if (!$country) {
                            $e = 'No country ' . $item->country;
                            $errors[$e] = $e;
                            continue;
                        }

                        $delivery = Delivery::findOne(['name' => $item->delivery, 'country_id' => $country->id]);
                        if (!$delivery) {
                            $e = 'No delivery ' . $item->delivery . ' in country ' . $item->country;
                            $errors[$e] = $e;
                            continue;
                        }

                        $debts = ReportDeliveryDebts::findOne([
                            'delivery_id' => $delivery->id,
                            'month' => $item->month . '-01',
                        ]);

                        if (!$debts) {
                            $debts = new ReportDeliveryDebts();
                        }
                        $debts->delivery_id = $delivery->id;
                        $debts->month = $item->month . '-01';


                        $debts->data = json_encode(
                            [
                                "month" => $debts->month,
                                "delivery_id" => $delivery->id,
                                "contract_id" => null,
                                "country_id" => $country->id,
                                "delivery_name" => $delivery->name,
                                "country_name" => $country->name,
                                "country_slug" => $country->slug,
                                "currency" => $country->currency,
                                "orders_in_process" => 0,
                                "orders_unbuyout" => 0,
                                "orders_total" => 0,
                                "orders_buyout" => 0,
                                "orders_money_received" => 0,
                                "money_received_sum_total_dollars" => $item->money_received_sum_total_bucks,
                                "charges_accepted_dollars" => 0,
                                "buyout_sum_total_dollars" => $item->sum_total_bucks - $item->money_received_sum_total_bucks,
                                "charges_not_accepted_dollars" => 0,
                                "only_buyout" => 0,
                                "sum_total_dollars" => 0,
                                "charges_total_dollars" => 0,
                                "end_sum_dollars" => $item->sum_total_bucks
                            ], JSON_UNESCAPED_UNICODE);

                        $debts->is_old_data = 1;
                        $debts->created_at = time();
                        if ($debts->save()) {
                            $records++;
                        } else {
                            $e = $debts->getFirstErrorAsString();
                            $errors[$e] = $e;
                        }
                    }
                }


            } else {
                $this->log("fail" . PHP_EOL);
                $answer['read_error'] = "Error: fail";
                $cronAnswer->answer = json_encode($answer);
                $cronAnswer->save();
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            }

        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        }

        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $cronAnswer->answer = json_encode($answer);

        $cronAnswer->save();
        if ($errors) {
            $this->log("Errors: " . print_r(array_values($errors), true));
        }
        $this->log("Records: " . $records);
        $this->log("Done.\n");
    }
}