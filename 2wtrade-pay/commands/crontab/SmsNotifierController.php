<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\notification\SmsQueue;
use app\modules\administration\models\CrontabTask;

/**
 * Class SmsNotificationController
 * @package app\commands\crontab
 */
class SmsNotifierController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'sendnotifications' => CrontabTask::TASK_SMS_NOTIFIER_SEND_NOTIFICATIONS,
    ];

    /**
     * Отправка SMS уведомлений о непрочитанных нотификациях пользователям системы
     */
    public function actionSendNotifications()
    {
        $this->log('Sending sms...', false);

        $queue = new SmsQueue();
        $queue->send('always');

        $this->log('done.');
    }
}
