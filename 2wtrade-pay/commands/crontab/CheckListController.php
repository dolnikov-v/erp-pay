<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\checklist\components\AutoCheck;
use app\modules\checklist\components\AutoCheckCall;
use app\modules\checklist\models\CheckListItem;

/**
 * Class CheckListController
 * @package app\commands\crontab
 */
class CheckListController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'run' => CrontabTask::TASK_AUTO_CHECK_LIST,
    ];

    /**
     * Автоматическая проверка чек листа
     */
    public function actionRun()
    {
        foreach (CheckListItem::getTriggersCollection() as $key => $item) {
            if ($key != CheckListItem::NO_TRIGGER) {
                AutoCheck::check($key);
                AutoCheckCall::check($key);
            }
        }
    }
}
