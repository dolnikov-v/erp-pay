<?php
namespace app\commands\crontab;

use app\components\cleaners\ApiLog;
use app\components\cleaners\CrontabTaskLog;
use app\components\cleaners\Media;
use app\components\cleaners\Notification;
use app\components\cleaners\UserActionLog;
use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;

/**
 * Class CleanerController
 * @package app\commands\crontab
 */
class CleanerController extends CrontabController
{
    protected $mapTasks = [
        'cleanuseractionlog' => CrontabTask::TASK_CLEAN_USER_ACTION_LOG,
        'cleanapilog' => CrontabTask::TASK_CLEAN_API_LOG,
        'cleancrontabtasklog' => CrontabTask::TASK_CLEAN_CRONTAB_TASK_LOG,
        'checkcrontablog' => CrontabTask::TASK_CHECK_CRONTAB_LOG,
        'cleanmedia' => CrontabTask::TASK_CLEAN_API_LOG,
        'cleannotifications' => CrontabTask::TASK_CLEAN_NOTIFICATIONS,
    ];

    /**
     * Очистка истории посещений пользователей
     */
    public function actionCleanUserActionLog()
    {
        $this->log('Clearing action logs of users... ', false);

        $totalCount = UserActionLog::clean();

        $crontabTaskLogAnswer = new CrontabTaskLogAnswer([
            'log_id' => $this->log->id,
            'answer' => "Удалено {$totalCount} логов.",
            'status' => CrontabTaskLogAnswer::STATUS_SUCCESS
        ]);

        $crontabTaskLogAnswer->save();

        $this->log('done.');
    }

    /**
     * Очистка логов по запросам API
     * @param int $days За какое количество дней хранить логи
     * @param int $batchSize
     */
    public function actionCleanApiLog($days = 30, $batchSize = 500)
    {
        $this->log('Clearing api logs...', false);

        $totalCount = ApiLog::clean($days, $batchSize);

        $crontabTaskLogAnswer = new CrontabTaskLogAnswer([
            'log_id' => $this->log->id,
            'answer' => "Удалено {$totalCount} логов.",
            'status' => CrontabTaskLogAnswer::STATUS_SUCCESS
        ]);

        $crontabTaskLogAnswer->save();

        $this->log('done.');
    }

    /**
     * Очистка логов Crontab
     */
    public function actionCleanCrontabTaskLog()
    {
        $this->log('Clearing crontab task logs...', false);

        CrontabTaskLog::clean();

        $this->log('done.');
    }

    /**
     * @param $delete
     */
    public function actionCheckCrontabLog($delete = false)
    {
        $this->log('Check unlinked files crontab task logs...');

        CrontabTaskLog::check($delete);

        $this->log('done.');
    }

    /**
     * @param bool $delete
     * Очистка непривязанных файлов Media
     */
    public function actionCleanMedia($delete = false)
    {
        $this->log('Clean unlinked files media...');

        Media::clean($delete);

        $this->log('done.');
    }

    /**
     * @param bool $delete
     * Очистка старых прочитанных оповещений
     */
    public function actionCleanNotifications($delete = false)
    {
        $this->log('Clean unreaded notifications...');

        Notification::clean($delete);

        $this->log('done.');
    }
}
