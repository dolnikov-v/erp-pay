<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\jobs\DeliveryCalculateCharges;
use app\jobs\InvoiceGenerate;
use app\models\Country;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequestExtra;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryInvoiceOptions;
use app\modules\order\components\ExcelBuilder;
use app\modules\order\components\ExcelBuilderFactory;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderLogisticListRequest;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\modules\report\models\InvoiceOrder;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\components\DeliveryHandler;
use app\modules\delivery\components\lists\ListsWorker;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryApiResponse;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\logger\components\log\Logger;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use app\modules\order\models\OrderSendError;
use app\modules\order\models\OrderStatus;
use app\modules\salary\models\Penalty as CallCenterPenalty;
use app\modules\salary\models\PenaltyType as CallCenterPenaltyType;
use app\modules\salary\models\search\PersonSearch as CallCenterPersonSearch;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

use app\modules\report\models\Invoice;

/**
 * Class DeliveryController
 * @package app\commands\crontab
 */
class DeliveryController extends CrontabController
{
    const POSTDELIVERY_MONITORING_CUTOFF = 5184000; // примерно два месяца в секундах
    const POSTDELIVERY_MONITORING_DELAY = 604800; // примерно неделя в сукундах. промежуток между перепроверками одного и того же заказа

    /** @var Delivery[] */
    private static $deliveries = [];

    const GET_STATUSES_TYPE_NOT_DEFERRED = 'not_deferred';
    const GET_STATUSES_TYPE_DEFERRED = 'deferred';

    protected $mapTasks = [
        'sendorders' => CrontabTask::TASK_DELIVERY_SEND_ORDERS,
        'getordersinfo' => CrontabTask::TASK_DELIVERY_GET_ORDERS_INFO,
        'monitordelivered' => CrontabTask::TASK_DELIVERY_MONITOR_DELIVERED,
        'reportanomalies' => CrontabTask::TASK_DELIVERY_REPORT_ANOMALIES,
        'notdeliveredorders' => CrontabTask::TASK_DELIVERY_NOT_DELIVERED_ORDERS,
        'sendlogisticlists' => CrontabTask::TASK_DELIVERY_SEND_LOGISTIC_LISTS,
        'generatelists' => CrontabTask::TASK_DELIVERY_GENERATE_LISTS,
        'generatecorrectionlists' => CrontabTask::TASK_DELIVERY_GENERATE_CORRECTION_LISTS,
        'getordersinfoforlists' => CrontabTask::TASK_DELIVERY_GET_ORDERS_INFO_FOR_LISTS,
        'checkingdeliveryperiod' => CrontabTask::TASK_DELIVERY_CHECKING_DELIVERY_PERIOD,
        'sendorderstoupdatequeue' => CrontabTask::TASK_DELIVERY_SEND_ORDERS_TO_UPDATE_QUEUE,
        'calculatecharges' => CrontabTask::TASK_DELIVERY_CALCULATE_CHARGES,
        'generateexpresslists' => CrontabTask::TASK_DELIVERY_GENERATE_EXPRESS_LISTS,
        'generatepretensionlists' => CrontabTask::TASK_DELIVERY_GENERATE_PRETENSION_LISTS,
        'trytosavetransferredorders' => CrontabTask::TASK_DELIVERY_SAVE_TRANSFERRED_ORDERS,
        'autoinvoicesender' => CrontabTask::TASK_DELIVERY_AUTO_INVOICE_SENDER,
    ];

    /**
     * Рассылка сводных нотификаций про аномальную смену статуса терминальных заказов
     */
    public function actionReportAnomalies()
    {
        $countries = self::getCountriesWithAnomalies();

        foreach ($countries as $country) {
            $requests = DeliveryRequest::find()
                ->joinWith([
                    'order',
                    'delivery',
                ])
                ->with([
                        'order.country',
                    ]
                )
                ->byStatus(DeliveryRequest::STATUS_ERROR_AFTER_DONE)
                ->andWhere([Order::tableName() . '.country_id' => $country->id])
                ->orderBy([DeliveryRequest::tableName() . '.cron_launched_at' => SORT_DESC])
                ->all();

            $total = count($requests);
            $transitions = [];

            foreach ($requests as $request) {
                $transition = $request->delivery->name;

                $info = json_decode($request->foreign_info, true);

                if (!is_null($info)) {
                    if (is_numeric($info['current_order_status_id']) && is_numeric($info['data']['status'])) {
                        $transition .= " {$info['current_order_status_id']} -> {$info['data']['status']}";
                    }
                }

                if (!array_key_exists($transition, $transitions)) {
                    $transitions[$transition] = [];
                }

                $transitions[$transition][] = $request->order_id;
            }

            $counters = [];
            foreach ($transitions as $transition => $orders) {
                $counters[$transition] = count($orders);
            }

            Yii::$app->notification->send(
                Notification::TRIGGER_DELIVERY_ANOMALY_SUMMARY,
                [
                    'country' => $country->name,
                    'transitions' => json_encode($counters),
                    'count' => $total,
                ],
                $country->id
            );
        }
    }

    /**
     * @return Country[]
     */
    protected static function getCountriesWithAnomalies()
    {
        $countries = [];

        $requests = DeliveryRequest::find()
            ->joinWith([
                'order',
                'delivery',
                'delivery.apiClass',
                'delivery.country',
            ])
            ->with([
                'order.country',
            ])
            ->byStatus(DeliveryRequest::STATUS_ERROR_AFTER_DONE)
            ->andWhere(['is not', Delivery::tableName() . '.api_class_id', null])
            ->andWhere([Delivery::tableName() . '.active' => 1])
            ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->all();

        foreach ($requests as $request) {
            $countries[$request->order->country->id] = $request->order->country;
        }

        return $countries;
    }

    /**
     * Слежение за завершенными заказами на случай если курьерка почему-то сменила статус
     */
    public function actionMonitorDelivered($deliveryId = null)
    {
        $deliveries = self::getDeliveriesByRequestStatus(DeliveryRequest::STATUS_DONE);

        foreach ($deliveries as $delivery) {

            if (is_numeric($deliveryId) && $delivery->id !== (int)$deliveryId) {
                continue;
            }

            // блокировка от двойного запуска
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }

            $lockFilename = "/blocks/delivery_monitor_delivered_{$delivery->id}.lock";

            $lockFp = fopen(Yii::getAlias('@runtime') . $lockFilename, 'w');

            if (!flock($lockFp, LOCK_EX | LOCK_NB)) {
                fclose($lockFp);
                continue;
            }

            $requests = DeliveryRequest::find()
                ->joinWith([
                        'order',
                        'delivery',
                        'delivery.apiClass',
                        'delivery.country',
                    ]
                )
                ->with([
                        'order.country',
                        'order.country.timezone',
                    ]
                )
                ->byStatus(DeliveryRequest::STATUS_DONE)
                ->byDeliveryId($delivery->id)
                ->andWhere([Delivery::tableName() . '.active' => 1])
                ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
                ->andWhere([Country::tableName() . '.active' => 1])
                ->andWhere([
                    ">",
                    DeliveryRequest::tableName() . ".done_at",
                    time() - self::POSTDELIVERY_MONITORING_CUTOFF
                ])// больше завершения_время мониторинг_катофа
                ->andWhere([
                    "<",
                    DeliveryRequest::tableName() . ".cron_launched_at",
                    time() - self::POSTDELIVERY_MONITORING_DELAY
                ])// будет ждать неделю даже если при прошлой проверке апи был (например) недоступен. todo: нормальные политики ретраев
                ->orderBy([DeliveryRequest::tableName() . '.cron_launched_at' => SORT_ASC])
                ->limit(1000)
                ->all();

            $totalRequests = count($requests);
            $cnt = 0;

            foreach ($requests as $request) {
                $cnt++;
                $this->log("\rRequest $cnt of $totalRequests; Current country: {$request->order->country->name} ({$request->order->country->char_code}); Current order: {$request->order->id};",
                    false);

                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $this->log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;


                try {
                    $response = $request->getOrderInfoViaApiTransmitter();

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $logAnswerAnswer = $response;

                        if (isset($logAnswerAnswer['data']['last_update_info'])) {
                            $logAnswerAnswer['data']['last_update_info'] = '!! not logged due to result being successfull (by monitorDelivered) !!';
                        }

                        $logAnswerAnswer['country_id'] = $request->order->country_id;
                        $logAnswerAnswer['order_id'] = $request->order->id;
                        $logAnswerAnswer['request_id'] = $request->id;

                        if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {

                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                            $order = $request->order;
                            $status = intval($response['data']['status']);

                            $data = $response['data'];


                            if ($order->status_id != $status) {
                                // статус 18 (деньги получены) ставится на основе финансовых отчетов - самого авторитетного источника информации, поэтому мы их даже и не перепроверяем.
                                if (!($order->status_id == OrderStatus::STATUS_FINANCE_MONEY_RECEIVED && $status == OrderStatus::STATUS_DELIVERY_BUYOUT)) {


                                    $logAnswerAnswer['message'] = 'Смена статуса ранее завершенной заявки';
                                    $logAnswerAnswer['current_order_status_id'] = $order->status_id;

                                    $request->status = DeliveryRequest::STATUS_ERROR_AFTER_DONE;
                                    $request->foreign_info = json_encode($data, JSON_UNESCAPED_UNICODE);

                                    $request->save(false, ['status', 'foreign_info']);
                                }
                            }

                            // отмечаем запуск только если апи работоспособен, чтобы не ждать потом ретрая вечность
                            $request->cron_launched_at = time();
                            $request->save(false, ['cron_launched_at']);

                        } else {
                            // пока просто игнорируем, но todo: сделать счетчик ретраев и вообще вменяемую политику ретраев
                        }

                        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                        $transaction->commit();
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                } catch (\Throwable $e) {
                    $logAnswerAnswer = [
                        'error' => $e->getMessage(),
                        'country_id' => $request->order->country_id,
                        'order_id' => $request->order->id,
                        'request_id' => $request->id,
                        'message' => "Ошибка при сверке статуса ранее завершенной заявки",
                    ];

                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                }

                $logAnswer->save();
                usleep(rand(10000, 1000000)); // 0.01 ... 1.0 seconds
            }

            fclose($lockFp);
        }
    }

    /**
     * @param integer $status
     * @param string $type
     * @param integer $offsetFrom
     * @param integer $offsetTo
     * @param integer $lastUpdateOffset
     * @return Delivery[]
     */
    protected static function getDeliveriesByRequestStatus(
        $status,
        $type = null,
        $offsetFrom = 0,
        $offsetTo = 0,
        $lastUpdateOffset = 0
    )
    {
        $subQuery = Country::find()
            ->select(['delivery_id' => Delivery::tableName() . '.id'])
            ->join('JOIN', Delivery::tableName(), Delivery::tableName() . '.country_id = ' . Country::tableName() . '.id and ' . Delivery::tableName() . '.active = 1 and ' . Delivery::tableName() . '.can_tracking = 1')
            ->join('JOIN', DeliveryApiClass::tableName(), DeliveryApiClass::tableName() . '.id = ' . Delivery::tableName() . '.api_class_id and ' . DeliveryApiClass::tableName() . '.active = 1 and ' . DeliveryApiClass::tableName() . '.can_tracking = 1')
            ->where([Country::tableName() . '.active' => 1]);

        $query = DeliveryRequest::find()
            ->select([DeliveryRequest::tableName() . '.delivery_id'])
            ->from(['preparedFilter' => $subQuery])
            ->join('JOIN', DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.delivery_id = preparedFilter.delivery_id and ' . DeliveryRequest::tableName() . '.status = \'' . $status . '\'')
            ->join('JOIN', Order::tableName(), Order::tableName() . '.id = ' . DeliveryRequest::tableName() . '.order_id' . (!empty($type) ? ' and ' . Order::tableName() . '.status_id ' . ($type == self::GET_STATUSES_TYPE_DEFERRED ? 'IN' : 'NOT IN') . ' (' . implode(',', DeliveryHandler::getDeferredStatuses()) . ')' : ''));

        if (!empty($offsetFrom)) {
            $time = strtotime("-{$offsetFrom} days");
            $query->andWhere([
                '<=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)',
                $time
            ]);
        }
        if (!empty($offsetTo)) {
            $time = strtotime("-{$offsetTo} days");
            $query->andWhere([
                '>=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)',
                $time
            ]);
        }

        if (!empty($lastUpdateOffset)) {
            $time = strtotime("-{$lastUpdateOffset} minutes");
            $query->andWhere([
                'or',
                ['is', DeliveryRequest::tableName() . '.cron_launched_at', null],
                ['<=', DeliveryRequest::tableName() . '.cron_launched_at', $time]
            ]);
        }

        $query->groupBy([DeliveryRequest::tableName() . '.delivery_id']);
        $query->orderBy([DeliveryRequest::tableName() . '.cron_launched_at' => SORT_ASC]);
        $deliveryIds = $query->column();

        $deliveries = Delivery::find()
            ->with(['apiClass', 'country'])
            ->where(['id' => $deliveryIds])
            ->indexBy('id')
            ->all();

        return $deliveries;
    }

    /**
     * Отправка заказов
     */
    public function actionSendOrders($delivery_id = null)
    {
        Yii::$app->db->enableSlaves = false;
        self::$deliveries = self::getDeliveriesToSendOrders();


        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");


        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);


        foreach (self::$deliveries as $delivery) {
            if (is_numeric($delivery_id)) {
                if ($delivery->id != $delivery_id) {
                    continue;
                }
                $this->log("delivery id explicitly specified and found!");
            }

            $log = $cronLog("processing delivery {$delivery->name}", ['delivery_id' => $delivery->id]);

            $currentOffset = strtotime(date('Y-m-d H:i:s')) - strtotime(date('Y-m-d'));

            if (!$delivery->allowedTiming($currentOffset)) {
                $log ("not allowed to send to this delivery at this time of day");
                continue;
            }


            // блокируем обновление статусов для одной курьерки
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }

            $file_lock = "/blocks/delivery_send_orders_{$delivery->id}_{$delivery->char_code}_{$delivery->country->id}_{$delivery->country->char_code}.lock";

            $fp[$delivery->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');

            if (!flock($fp[$delivery->id], LOCK_EX | LOCK_NB)) {
                $this->log("unable to lock {$file_lock}");
                fclose($fp[$delivery->id]);
                continue;
            }

            $this->log('Sending orders...', false);

            set_time_limit(0);

            $requests = DeliveryRequest::find()
                ->joinWith([
                    'order.deliverySendError',
                    'delivery',
                    'delivery.apiClass',
                    'delivery.country',
                ])
                ->with([
                    'order.country.currency',
                    'order.country.timezone',
                    'delivery.timings',
                    'order.orderProducts.product',
                    'order.deliveryRequest.delivery',
                    'order.callCenterRequest',
                ])
                ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                ->byStatus(DeliveryRequest::STATUS_PENDING)
                ->andWhere(['is not', Delivery::tableName() . '.api_class_id', null])
                ->andWhere([Delivery::tableName() . '.active' => 1])
                ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
                ->andWhere([Country::tableName() . '.active' => 1])
                ->andWhere(['is', OrderSendError::tableName() . '.type', null])
                ->limit(1000)
                ->all();


            $log ("sending " . count($requests) . " requests to {$delivery->name}...");

            $this->log("got " . count($requests) . " requests to send...");

            foreach ($requests as $request) {

                // используем order_id как наиболее релевантный для последующего поиска идентификатор
                $requestLog = $log ("processing single request...", ["order_id" => $request->order->id]);

                $response = null;

                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $this->log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                $logAnswerAnswer = [];
                try {
                    $request->cron_launched_at = time();
                    $request->save(false, ['cron_launched_at']);

                    $response = $request->sendOrderViaApiTransmitter();

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $logAnswerAnswer = $response;
                        $logAnswerAnswer['country_id'] = $request->order->country_id;
                        $logAnswerAnswer['order_id'] = $request->order->id;
                        $logAnswerAnswer['request_id'] = $request->id;

                        $result = DeliveryHandler::saveResponseAfterSending($request, $response);

                        if ($result['status'] == 'success') {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                        } else {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswerAnswer['errors'] = [$result['error']];
                        }
                        foreach ($result['messages'] as $message) {
                            $requestLog($message);
                        }

                        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                        $transaction->commit();
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        $sendErrorModel = new OrderSendError([
                            'type' => OrderSendError::TYPE_DELIVERY_SEND,
                            'order_id' => $request->order->id,
                            'error' => $e->getMessage(),
                            'response' => json_encode($response, JSON_UNESCAPED_UNICODE),
                        ]);
                        $sendErrorModel->save();
                        throw $e;
                    }
                } catch (\Throwable $e) {
                    $requestLog ("failed to send order to delivery due to exception: " . $e->getMessage() . "\n\n" . $e->getTraceAsString(),
                        ["record_type" => "error"]);
                    $logAnswerAnswer = [
                        'error' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                        'country_id' => $request->order->country_id,
                        'order_id' => $request->order->id,
                        'request_id' => $request->id,
                        'delivery_answer' => json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE),
                    ];

                    Yii::$app->notification->send(
                        Notification::TRIGGER_EXCEPTION_SENT_TO_COURIER,
                        [
                            'order_id' => $request->order_id,
                            'country' => $request->order->country->name,
                            'delivery' => $request->delivery->name,
                            'error' => $e->getMessage(),
                        ],
                        $request->order->country_id
                    );

                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                }

                if (!$logAnswer->save()) {
                    $this->log("unable to save logAnswer!");
                }

                sleep(rand(1, 3));
            }

            fclose($fp[$delivery->id]);
        }
        unset($fp);

        $cronLog ("done");

        $this->log('done.');
    }

    /**
     * @return Delivery[]
     */
    protected static function getDeliveriesToSendOrders()
    {
        $deliveries = [];

        $requests = DeliveryRequest::find()
            ->joinWith([
                'order',
                'delivery',
                'delivery.apiClass',
                'delivery.country',
            ])
            ->byStatus(DeliveryRequest::STATUS_PENDING)
            ->andWhere(['is not', Delivery::tableName() . '.api_class_id', null])
            ->andWhere([Delivery::tableName() . '.active' => 1])
            ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Delivery::tableName() . '.our_api' => 0])
            ->all();

        foreach ($requests as $request) {
            $deliveries[$request->delivery->id] = $request->delivery;
        }

        return $deliveries;
    }

    /**
     * Получение информации о заказах
     *
     * @param integer $delivery_id - id КС
     * @param string $type - тип "deferred" или "not_deferred"
     * @param integer $offset_from - смещение даты отправки заявки с (в днях)
     * @param integer $offset_to - смещение даты отправки заявки по (в днях)
     * @param integer $last_update_offset - последнее обновление более n минут назад
     * @param integer $limit - общий лимит на каждую очередь
     * @throws \yii\db\Exception
     */
    public function actionGetOrdersInfo(
        $delivery_id = null,
        $type = null,
        $offset_from = 0,
        $offset_to = 0,
        $last_update_offset = 60,
        $limit = 1000
    )
    {
        Yii::$app->db->enableSlaves = false;
        if (extension_loaded('newrelic')) {
            newrelic_end_transaction(true);// end transaction and ignore the stats (actual stats are inside those loops below)
        }

        set_time_limit(0);
        $offset_from = intval($offset_from);
        $offset_to = intval($offset_to);
        $last_update_offset = intval($last_update_offset);
        $limit = intval($limit);

        self::$deliveries = self::getDeliveriesByRequestStatus(DeliveryRequest::STATUS_IN_PROGRESS, $type, $offset_from,
            $offset_to, $last_update_offset);

        foreach (self::$deliveries as $delivery) {
            if (is_numeric($delivery_id) && !empty($delivery_id)) {
                if ($delivery->id != $delivery_id) {
                    continue;
                }
            }

            if (($delivery->hasApiGetOrdersInfoForArray() || $delivery->hasApiBatchGetOrdersInfoForArray()) && $delivery->apiClass->use_amazon_queue) {
                continue;
            }

            $types = self::getTypesForGetOrdersInfo();
            foreach ($types as $currentType) {
                if (!empty($type) && $type != $currentType) {
                    continue;
                }

                // блокируем обновление статусов для одного КЦ
                if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                    mkdir(Yii::getAlias('@runtime') . '/blocks/');
                }

                $file_lock = "/blocks/delivery_get_orders_info_{$delivery->id}_{$delivery->char_code}_{$delivery->country->id}_{$delivery->country->char_code}_{$currentType}_{$offset_from}_{$offset_to}.lock";

                $fp[$delivery->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');
                if (!flock($fp[$delivery->id], LOCK_EX | LOCK_NB)) {
                    fclose($fp[$delivery->id]);
                    continue;
                }

                $this->log("Get orders info for delivery {$delivery->id} and type {$currentType}...");


                $query = DeliveryRequest::find()
                    ->joinWith([
                            'order',
                            'delivery',
                            'delivery.apiClass',
                            'delivery.country',
                        ]
                    )
                    ->with([
                            'order.country',
                            'order.country.timezone',
                            'order.deliveryRequest',
                        ]
                    )
                    ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                    ->byStatus(DeliveryRequest::STATUS_IN_PROGRESS)
                    ->andWhere([Delivery::tableName() . '.active' => 1])
                    ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
                    ->andWhere([Country::tableName() . '.active' => 1])
                    ->orderBy([
                        DeliveryRequest::tableName() . '.cron_launched_at' => SORT_ASC,
                        DeliveryRequest::tableName() . '.updated_at' => SORT_ASC
                    ]);
                $query->andWhere([
                    $currentType == self::GET_STATUSES_TYPE_DEFERRED ? 'IN' : 'NOT IN',
                    Order::tableName() . '.status_id',
                    DeliveryHandler::getDeferredStatuses()
                ]);
                if (!empty($limit)) {
                    $query->limit($limit);
                }

                if (!empty($offset_from)) {
                    $time = strtotime("-{$offset_from} days");
                    $query->andWhere([
                        '<=',
                        'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)',
                        $time
                    ]);
                }
                if (!empty($offset_to)) {
                    $time = strtotime("-{$offset_to} days");
                    $query->andWhere([
                        '>=',
                        'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)',
                        $time
                    ]);
                }
                if (!empty($last_update_offset)) {
                    $time = strtotime("-{$last_update_offset} minutes");
                    $query->andWhere([
                        'or',
                        ['is', DeliveryRequest::tableName() . '.cron_launched_at', null],
                        ['<=', DeliveryRequest::tableName() . '.cron_launched_at', $time]
                    ]);
                }

                $requests = $query->all();
                if ($delivery->hasApiBatchGetOrdersInfo()) {
                    // апишка умеет пачками
                    while ($remaining = count($requests)) {
                        $this->log("$remaining requests remaining...");

                        try {
                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                foreach ($requests as $request) {
                                    $request->cron_launched_at = time();
                                    $request->save(false, ['cron_launched_at']);
                                }
                                $transaction->commit();
                            } catch (\Throwable $e) {
                                $transaction->rollBack();
                                throw $e;
                            }
                            $response = $delivery->getBatchOrdersInfoViaApiTransmitter($requests);
                        } catch (\Throwable $e) {
                            $logAnswer = new CrontabTaskLogAnswer();
                            $logAnswer->log_id = $this->log->id;
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswerAnswer = [];
                            $logAnswerAnswer['country_id'] = $delivery->country_id;
                            $logAnswerAnswer['order_ids'] = implode(ArrayHelper::getColumn($requests, 'order_id'));
                            $logAnswerAnswer['request_ids'] = implode(ArrayHelper::getColumn($requests, 'id'));
                            $logAnswerAnswer['message'] = $e->getMessage();
                            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                            $logAnswer->save();
                            continue;
                        }

                        if (!$response or !is_array($response)) {
                            $this->log("failed attempt to check batch. response was: " . print_r($response, true));
                            continue;
                        }


                        if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {
                            $apiStatuses = $response['data'];

                            $responseCount = count($apiStatuses);

                            $this->log("Got $responseCount orders in the response.");
                            //$this->log(print_r($apiStatuses, true));

                            foreach ($apiStatuses as $response) {
                                $tracking = $response['order_id'];

                                $request = DeliveryRequest::find()
                                    ->with([
                                            'order',
                                            'delivery',
                                        ]
                                    )
                                    ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                                    ->andWhere([DeliveryRequest::tableName() . ".tracking" => $tracking])
                                    ->one();

                                if (!$request) {
                                    continue;
                                }

                                $transaction = Yii::$app->db->beginTransaction();

                                $logAnswer = new CrontabTaskLogAnswer();
                                $logAnswer->log_id = $this->log->id;
                                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                                $logAnswerAnswer = $response;

                                $logAnswerAnswer['country_id'] = $request->order->country_id;
                                $logAnswerAnswer['order_id'] = $request->order->id;
                                $logAnswerAnswer['request_id'] = $request->id;

                                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                                try {

                                    $order = $request->order;
                                    $status = $response['status'];

                                    $lastUpdateResponse = null;
                                    if (isset($response['last_update_info'])) {
                                        $lastUpdateResponse = $request->addUpdateResponse($response['last_update_info']);
                                    }

                                    $mapDates = [
                                        'returned_at' => 'date_returned',
                                        'approved_at' => 'date_approved',
                                        'accepted_at' => 'date_accepted',
                                        'paid_at' => 'date_payment',
                                        'finance_tracking' => 'finance_tracking',
                                        'unshipping_reason' => 'unshipping_reason',
                                    ];

                                    foreach ($mapDates as $attribute => $mapAttribute) {
                                        if (empty($request->$attribute) && isset($response[$mapAttribute])) {
                                            $request->$attribute = $response[$mapAttribute];
                                        }
                                    }

                                    $request->setDeliveryPartnerId($response['delivery_partner_name'] ?? '');

                                    $request->saveUnBuyoutReason($response['unshipping_reason'] ?? '');

                                    if (isset($response['call_center_penalty_type']) && !empty($response['call_center_penalty_type'])) {
                                        if (($trigger = CallCenterPenaltyType::getTriggerByString($response['call_center_penalty_type']))
                                            && ($penaltyType = CallCenterPenaltyType::find()
                                                ->byTrigger($trigger)
                                                ->one())
                                            && $penaltyType->active
                                            && ($person = CallCenterPersonSearch::findPersonByOrderId($request->order_id))
                                            && !CallCenterPenalty::find()->where([
                                                'person_id' => $person->id,
                                                'order_id' => $request->order_id,
                                                'penalty_type_id' => $penaltyType->id
                                            ])->exists()
                                        ) {
                                            $penalty = new CallCenterPenalty([
                                                'order_id' => $request->order_id,
                                                'person_id' => $person->id,
                                                'penalty_type_id' => $penaltyType->id,
                                                'penalty_sum_usd' => $penaltyType->sum,
                                                'penalty_percent' => $penaltyType->percent,
                                            ]);
                                            if (!$penalty->save()) {
                                                throw new \Exception($penalty->getFirstErrorAsString());
                                            }
                                        }
                                    }

                                    if (isset($response['api_error'])) {
                                        $request->api_error = $response['api_error'];
                                    }

                                    if ($order->status_id != $status) {
                                        if ($order->canChangeStatusTo($status, null, true)) {
                                            $order->status_id = $status;
                                            $order->save(false, ['status_id']);

                                            if (empty($request->approved_at) && in_array($order->status_id, [
                                                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                                                    OrderStatus::STATUS_DELIVERY_REFUND,
                                                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                                                    OrderStatus::STATUS_DELIVERY_DENIAL,
                                                    OrderStatus::STATUS_DELIVERY_RETURNED
                                                ])
                                            ) {
                                                $request->approved_at = time();
                                            }

                                            if (OrderStatus::isFinalDeliveryStatus($status)) {
                                                $request->status = DeliveryRequest::STATUS_DONE;
                                                $request->done_at = time();
                                            }
                                        } else {
                                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                            $logAnswerAnswer['message'] = 'Неразрешенное значение статуса.';
                                        }
                                    }

                                    /** @var DeliveryRequest $requestCheck */
                                    $requestCheck = DeliveryRequest::findOne($request->id);

                                    if ($requestCheck && $requestCheck->status == DeliveryRequest::STATUS_IN_PROGRESS) {
                                        $request->route = Yii::$app->controller->route;
                                        $request->save();
                                        if ($lastUpdateResponse) {
                                            $lastUpdateResponse->save();
                                        }
                                    } else {
                                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                        $logAnswerAnswer['errors'] = 'Во время выполнения запроса, сменился статус заявки.';
                                    }

                                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);

                                } catch (\Throwable $e) {
                                    $logAnswerAnswer = [
                                        'error' => $e->getMessage(),
                                        'country_id' => $request->order->country_id,
                                        'order_id' => $request->order->id,
                                        'request_id' => $request->id,
                                    ];

                                    $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                                }

                                if (!empty($logAnswerAnswer['error']) || !empty($logAnswerAnswer['message']) || !empty($logAnswerAnswer['errors'])) {
                                    $error_msg = [];
                                    if (!empty($logAnswerAnswer['error'])) {
                                        $error_msg[] = "error: " . $logAnswerAnswer['error'];
                                    }
                                    if (!empty($logAnswerAnswer['message'])) {
                                        $error_msg[] = "message: " . $logAnswerAnswer['message'];
                                    }
                                    if (!empty($logAnswerAnswer['errors'])) {
                                        $error_msg[] = "errors: " . $logAnswerAnswer['errors'];
                                    }
                                    if (!empty($data)) {
                                        $error_msg[] = 'response:' . print_r($data, true);
                                    }
                                    Yii::$app->notification->send(Notification::TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER,
                                        [
                                            'order_id' => $request->order->id,
                                            'country' => $request->order->country->name,
                                            'delivery' => $request->delivery->char_code,
                                            'error' => implode(' | ', $error_msg),
                                        ]
                                    );
                                }
                                $logAnswer->save();
                                $transaction->commit();
                            }
                        } else {

                        }
                    }
                } else {
                    // для апишек, которые не умеют новый подход
                    $totalRequests = count($requests);
                    $cnt = 0;

                    foreach ($requests as $request) {

                        $cnt++;
                        $this->log("\rRequest $cnt of $totalRequests; Current country: {$request->order->country->name} ({$request->order->country->char_code}); Current order: {$request->order->id};",
                            false);

                        $request->cron_launched_at = time();
                        $request->save(false, ['cron_launched_at']);

                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $this->log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                        try {
                            $response = $request->getOrderInfoViaApiTransmitter();

                            $logAnswerAnswer = $response;
                            $logAnswerAnswer['country_id'] = $request->order->country_id;
                            $logAnswerAnswer['order_id'] = $request->order->id;
                            $logAnswerAnswer['request_id'] = $request->id;

                            $transaction = Yii::$app->db->beginTransaction();
                            $lastUpdateResponse = null;
                            try {
                                if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {
                                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                                    $order = $request->order;
                                    $status = $response['data']['status'];

                                    if (isset($response['data']['last_update_info'])) {
                                        $lastUpdateResponse = $request->addUpdateResponse($response['data']['last_update_info']);
                                        $logAnswerAnswer['data']['last_update_info'] = '!! removed by getOrdersInfo due to request being successfull !!';
                                    }

                                    $data = $response['data'];

                                    $mapDates = [
                                        'returned_at' => 'date_returned',
                                        'approved_at' => 'date_approved',
                                        'accepted_at' => 'date_accepted',
                                        'paid_at' => 'date_payment',
                                        'finance_tracking' => 'finance_tracking',
                                        'unshipping_reason' => 'unshipping_reason',
                                    ];

                                    foreach ($mapDates as $attribute => $mapAttribute) {
                                        if (empty($request->$attribute) && isset($data[$mapAttribute])) {
                                            $request->$attribute = $data[$mapAttribute];
                                        }
                                    }

                                    $request->setDeliveryPartnerId($response['delivery_partner_name'] ?? '');

                                    $request->saveUnBuyoutReason($data['unshipping_reason'] ?? '');

                                    if (isset($data['api_error'])) {
                                        $request->api_error = $data['api_error'];
                                    }

                                    if (isset($data['call_center_penalty_type']) && !empty($data['call_center_penalty_type'])) {
                                        if (($trigger = CallCenterPenaltyType::getTriggerByString($data['call_center_penalty_type']))
                                            && ($penaltyType = CallCenterPenaltyType::find()
                                                ->byTrigger($trigger)
                                                ->one())
                                            && $penaltyType->active
                                            && ($person = CallCenterPersonSearch::findPersonByOrderId($request->order_id))
                                            && !CallCenterPenalty::find()->where([
                                                'person_id' => $person->id,
                                                'order_id' => $request->order_id,
                                                'penalty_type_id' => $penaltyType->id
                                            ])->exists()
                                        ) {
                                            $penalty = new CallCenterPenalty([
                                                'order_id' => $request->order_id,
                                                'person_id' => $person->id,
                                                'penalty_type_id' => $penaltyType->id,
                                                'penalty_sum_usd' => $penaltyType->sum,
                                                'penalty_percent' => $penaltyType->percent,
                                            ]);
                                            if (!$penalty->save()) {
                                                throw new \Exception($penalty->getFirstErrorAsString());
                                            }
                                        }
                                    }

                                    if ($order->status_id != $status) {
                                        if ($order->canChangeStatusTo($status, null, true)) {
                                            $order->status_id = $status;
                                            $order->save(false, ['status_id']);

                                            if (empty($request->approved_at) && in_array($order->status_id, [
                                                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                                                    OrderStatus::STATUS_DELIVERY_REFUND,
                                                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                                                    OrderStatus::STATUS_DELIVERY_DENIAL,
                                                    OrderStatus::STATUS_DELIVERY_RETURNED
                                                ])
                                            ) {
                                                $request->approved_at = time();
                                            }

                                            if (OrderStatus::isFinalDeliveryStatus($status)) {
                                                $request->status = DeliveryRequest::STATUS_DONE;
                                                $request->done_at = time();
                                            }
                                        } else {
                                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                            $logAnswerAnswer['message'] = 'Неразрешенное значение статуса.';
                                        }
                                    }
                                } else {
                                    if (isset($response['data']['cs_response']) && !empty($response['data']['cs_response'])) {
                                        $lastUpdateResponse = $request->addUpdateResponse($response['data']['cs_response']);
                                    }
                                }

                                /** @var DeliveryRequest $requestCheck */
                                $requestCheck = DeliveryRequest::findOne($request->id);

                                if ($requestCheck && $requestCheck->status == DeliveryRequest::STATUS_IN_PROGRESS) {
                                    $request->route = Yii::$app->controller->route;
                                    $request->save();
                                    if ($lastUpdateResponse) {
                                        $lastUpdateResponse->save();
                                    }
                                } else {
                                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                    $logAnswerAnswer['errors'] = 'Во время выполнения запроса, сменился статус заявки.';
                                }
                                $transaction->commit();
                            } catch (\Throwable $e) {
                                $transaction->rollBack();
                                $logAnswerAnswer['error'] = $e->getMessage();
                            }

                            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                        } catch (\Throwable $e) {

                            $logAnswerAnswer = [
                                'error' => $e->getMessage(),
                                'country_id' => $request->order->country_id,
                                'order_id' => $request->order->id,
                                'request_id' => $request->id,
                            ];

                            $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                        }
                        if (!empty($logAnswerAnswer['error']) || !empty($logAnswerAnswer['message']) || !empty($logAnswerAnswer['errors'])) {
                            $error_msg = [];
                            if (!empty($logAnswerAnswer['error'])) {
                                $error_msg[] = "error: " . $logAnswerAnswer['error'];
                            }
                            if (!empty($logAnswerAnswer['message'])) {
                                $error_msg[] = "message: " . $logAnswerAnswer['message'];
                            }
                            if (!empty($logAnswerAnswer['errors'])) {
                                $error_msg[] = "errors: " . $logAnswerAnswer['errors'];
                            }
                            if (!empty($data)) {
                                $error_msg[] = 'response:' . print_r($data, true);
                            }
                            if (!isset($response) || !($response['status'] == DeliveryApiResponse::STATUS_FAIL && $response['typeError'] == DeliveryRequest::API_WARNING_SYSTEM)) {
                                Yii::$app->notification->send(Notification::TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER, [
                                        'order_id' => $request->order->id,
                                        'country' => $request->order->country->name,
                                        'delivery' => $request->delivery->char_code,
                                        'error' => implode(' | ', $error_msg),
                                    ]
                                );
                            }
                        }
                        $logAnswer->save();

                        usleep(rand(1000, 1000000)); // 0.001 ... 1.0 seconds
                    }
                }
                fclose($fp[$delivery->id]);
            }
        }
        unset($fp);
        $this->log("done.");
    }

    /**
     * @param null|integer $delivery_id
     * @throws \yii\db\Exception
     */
    public function actionGetOrdersInfoForLists($delivery_id = null)
    {
        if (extension_loaded('newrelic')) {
            newrelic_end_transaction(true);// end transaction and ignore the stats (actual stats are inside those loops below)
        }

        $this->log("Get orders info...", false);
        set_time_limit(0);

        self::$deliveries = self::getDeliveriesByRequestStatus(DeliveryRequest::STATUS_IN_LIST);


        foreach (self::$deliveries as $delivery) {
            if (is_numeric($delivery_id)) {
                if ($delivery->id != $delivery_id) {
                    continue;
                }
            }

            if (!$delivery->hasApiCanGetOrderInfoForList()) {
                continue;
            }

            // блокируем обновление статусов для одного КЦ
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }

            $file_lock = "/blocks/delivery_get_orders_info_for_lists_{$delivery->id}_{$delivery->char_code}_{$delivery->country->id}_{$delivery->country->char_code}.lock";

            $fp[$delivery->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');
            if (!flock($fp[$delivery->id], LOCK_EX | LOCK_NB)) {
                fclose($fp[$delivery->id]);
                continue;
            }


            $requests = DeliveryRequest::find()
                ->joinWith([
                        'order',
                        'delivery',
                        'delivery.apiClass',
                        'delivery.country',
                    ]
                )
                ->with([
                        'order.country',
                        'order.country.timezone',
                        'order.deliveryRequest',
                    ]
                )
                ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                ->byStatus(DeliveryRequest::STATUS_IN_LIST)
                ->andWhere([Delivery::tableName() . '.active' => 1])
                ->andWhere([DeliveryApiClass::tableName() . '.active' => 1])
                ->andWhere([Country::tableName() . '.active' => 1])
                ->orderBy([
                    DeliveryRequest::tableName() . '.cron_launched_at' => SORT_ASC,
                    DeliveryRequest::tableName() . '.updated_at' => SORT_ASC
                ])
                ->limit(1000)
                ->all();


            if ($delivery->hasApiBatchGetOrdersInfo()) {
                // апишка умеет пачками
                while ($remaining = count($requests)) {
                    $this->log("$remaining requests remaining...");
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            foreach ($requests as $request) {
                                $request->cron_launched_at = time();
                                $request->save(false, ['cron_launched_at']);
                            }
                            $transaction->commit();
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            throw $e;
                        }
                        $response = $delivery->getBatchOrdersInfoViaApiTransmitter($requests);
                    } catch (\Throwable $e) {
                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $this->log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                        $logAnswerAnswer = [];
                        $logAnswerAnswer['country_id'] = $delivery->country_id;
                        $logAnswerAnswer['order_ids'] = implode(ArrayHelper::getColumn($requests, 'order_id'));
                        $logAnswerAnswer['request_ids'] = implode(ArrayHelper::getColumn($requests, 'id'));
                        $logAnswerAnswer['message'] = $e->getMessage();
                        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                        $logAnswer->save();
                        continue;
                    }

                    if (!$response or !is_array($response)) {
                        $this->log("failed attempt to check batch. response was: " . print_r($response, true));
                        continue;
                    }


                    if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {
                        $apiStatuses = $response['data'];

                        $responseCount = count($apiStatuses);

                        $this->log("Got $responseCount orders in the response.");
                        $this->log(print_r($apiStatuses, true));

                        foreach ($apiStatuses as $response) {
                            $order_id = $response['order_id'];

                            $request = DeliveryRequest::find()
                                ->with(['order', 'delivery'])
                                ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery->id])
                                ->andWhere([DeliveryRequest::tableName() . ".order_id" => $order_id])
                                ->one();

                            if (!$request) {
                                continue;
                            }

                            $transaction = Yii::$app->db->beginTransaction();

                            $logAnswer = new CrontabTaskLogAnswer();
                            $logAnswer->log_id = $this->log->id;
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                            $logAnswerAnswer = $response;
                            $logAnswerAnswer['country_id'] = $request->order->country_id;
                            $logAnswerAnswer['order_id'] = $request->order->id;
                            $logAnswerAnswer['request_id'] = $request->id;

                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                            try {

                                $order = $request->order;
                                $status = $response['status'];

                                $lastUpdateResponse = null;
                                if (isset($response['last_update_info'])) {
                                    $lastUpdateResponse = $request->addUpdateResponse($response['last_update_info']);
                                }

                                $mapDates = [
                                    'returned_at' => 'date_returned',
                                    'approved_at' => 'date_approved',
                                    'accepted_at' => 'date_accepted',
                                    'paid_at' => 'date_payment',
                                    'finance_tracking' => 'finance_tracking',
                                    'tracking' => 'tracking',
                                ];

                                $request->setDeliveryPartnerId($response['delivery_partner_name'] ?? '');

                                foreach ($mapDates as $attribute => $mapAttribute) {
                                    if (empty($request->$attribute) && isset($response[$mapAttribute])) {
                                        $request->$attribute = $response[$mapAttribute];
                                    }
                                }

                                if (isset($response['call_center_penalty_type']) && !empty($response['call_center_penalty_type'])) {
                                    if (($trigger = CallCenterPenaltyType::getTriggerByString($response['call_center_penalty_type']))
                                        && ($penaltyType = CallCenterPenaltyType::find()->byTrigger($trigger)->one())
                                        && $penaltyType->active
                                        && ($person = CallCenterPersonSearch::findPersonByOrderId($request->order_id))
                                        && !CallCenterPenalty::find()->where([
                                            'person_id' => $person->id,
                                            'order_id' => $request->order_id,
                                            'penalty_type_id' => $penaltyType->id
                                        ])->exists()
                                    ) {
                                        $penalty = new CallCenterPenalty([
                                            'order_id' => $request->order_id,
                                            'person_id' => $person->id,
                                            'penalty_type_id' => $penaltyType->id,
                                            'penalty_sum_usd' => $penaltyType->sum,
                                            'penalty_percent' => $penaltyType->percent,
                                        ]);
                                        if (!$penalty->save()) {
                                            throw new \Exception($penalty->getFirstErrorAsString());
                                        }
                                    }
                                }

                                if ($order->status_id != $status) {
                                    if ($order->canChangeStatusTo($status)) {
                                        $order->status_id = $status;
                                        $order->save(false, ['status_id']);

                                        if (empty($request->approved_at) && in_array($order->status_id, [
                                                OrderStatus::STATUS_DELIVERY_BUYOUT,
                                                OrderStatus::STATUS_DELIVERY_REFUND,
                                                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                                                OrderStatus::STATUS_DELIVERY_DENIAL,
                                                OrderStatus::STATUS_DELIVERY_RETURNED
                                            ])
                                        ) {
                                            $request->approved_at = time();
                                        }

                                        if (!empty($request->tracking)) {
                                            $request->status = DeliveryRequest::STATUS_IN_PROGRESS;
                                        }

                                        if (OrderStatus::isFinalDeliveryStatus($status)) {
                                            $request->status = DeliveryRequest::STATUS_DONE;
                                            $request->done_at = time();
                                        }
                                    } else {
                                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                        $logAnswerAnswer['message'] = 'Неразрешенное значение статуса.';
                                    }
                                }

                                /** @var DeliveryRequest $requestCheck */
                                $requestCheck = DeliveryRequest::findOne($request->id);

                                if ($requestCheck && $requestCheck->status == DeliveryRequest::STATUS_IN_LIST) {
                                    $request->route = Yii::$app->controller->route;
                                    $request->save();
                                    if ($lastUpdateResponse) {
                                        $lastUpdateResponse->save();
                                    }
                                } else {
                                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                    $logAnswerAnswer['errors'] = 'Во время выполнения запроса, сменился статус заявки.';
                                }

                                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);

                            } catch (\Throwable $e) {
                                $logAnswerAnswer = [
                                    'error' => $e->getMessage(),
                                    'country_id' => $request->order->country_id,
                                    'order_id' => $request->order->id,
                                    'request_id' => $request->id,
                                ];

                                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                            }

                            if (!empty($logAnswerAnswer['error']) || !empty($logAnswerAnswer['message']) || !empty($logAnswerAnswer['errors'])) {
                                $error_msg = [];
                                if (!empty($logAnswerAnswer['error'])) {
                                    $error_msg[] = "error: " . $logAnswerAnswer['error'];
                                }
                                if (!empty($logAnswerAnswer['message'])) {
                                    $error_msg[] = "message: " . $logAnswerAnswer['message'];
                                }
                                if (!empty($logAnswerAnswer['errors'])) {
                                    $error_msg[] = "errors: " . $logAnswerAnswer['errors'];
                                }
                                if (!empty($data)) {
                                    $error_msg[] = 'response:' . print_r($data, true);
                                }
                                Yii::$app->notification->send(Notification::TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER, [
                                        'order_id' => $request->order->id,
                                        'country' => $request->order->country->name,
                                        'delivery' => $request->delivery->char_code,
                                        'error' => implode(' | ', $error_msg),
                                    ]
                                );
                            }
                            $logAnswer->save();
                            $transaction->commit();
                        }
                    }
                }
            } else {
                // для апишек, которые не умеют новый подход
                $totalRequests = count($requests);
                $cnt = 0;

                foreach ($requests as $request) {

                    $cnt++;
                    $this->log("\rRequest $cnt of $totalRequests; Current country: {$request->order->country->name} ({$request->order->country->char_code}); Current order: {$request->order->id};",
                        false);
                    $transaction = Yii::$app->db->beginTransaction();

                    $request->cron_launched_at = time();
                    $request->save(false, ['cron_launched_at']);

                    $logAnswer = new CrontabTaskLogAnswer();
                    $logAnswer->log_id = $this->log->id;
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                    try {
                        $response = $request->getOrderInfoViaApiTransmitter();

                        $logAnswerAnswer = $response;
                        $logAnswerAnswer['country_id'] = $request->order->country_id;
                        $logAnswerAnswer['order_id'] = $request->order->id;
                        $logAnswerAnswer['request_id'] = $request->id;

                        $lastUpdateResponse = null;
                        if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;

                            $order = $request->order;
                            $status = $response['data']['status'];

                            if (isset($response['data']['last_update_info'])) {
                                $lastUpdateResponse = $request->addUpdateResponse($response['data']['last_update_info']);
                            }

                            $data = $response['data'];

                            $mapDates = [
                                'returned_at' => 'date_returned',
                                'approved_at' => 'date_approved',
                                'accepted_at' => 'date_accepted',
                                'paid_at' => 'date_payment',
                                'finance_tracking' => 'finance_tracking',
                                'tracking' => 'tracking',
                            ];

                            $request->setDeliveryPartnerId($response['delivery_partner_name'] ?? '');

                            foreach ($mapDates as $attribute => $mapAttribute) {
                                if (empty($request->$attribute) && isset($data[$mapAttribute])) {
                                    $request->$attribute = $data[$mapAttribute];
                                }
                            }

                            if (isset($data['call_center_penalty_type']) && !empty($data['call_center_penalty_type'])) {
                                if (($trigger = CallCenterPenaltyType::getTriggerByString($data['call_center_penalty_type']))
                                    && ($penaltyType = CallCenterPenaltyType::find()->byTrigger($trigger)->one())
                                    && $penaltyType->active
                                    && ($person = CallCenterPersonSearch::findPersonByOrderId($request->order_id))
                                    && !CallCenterPenalty::find()->where([
                                        'person_id' => $person->id,
                                        'order_id' => $request->order_id,
                                        'penalty_type_id' => $penaltyType->id
                                    ])->exists()
                                ) {
                                    $penalty = new CallCenterPenalty([
                                        'order_id' => $request->order_id,
                                        'person_id' => $person->id,
                                        'penalty_type_id' => $penaltyType->id,
                                        'penalty_sum_usd' => $penaltyType->sum,
                                        'penalty_percent' => $penaltyType->percent,
                                    ]);
                                    if (!$penalty->save()) {
                                        throw new \Exception($penalty->getFirstErrorAsString());
                                    }
                                }
                            }

                            if ($order->status_id != $status) {
                                if ($order->canChangeStatusTo($status)) {
                                    $order->status_id = $status;
                                    $order->save(false, ['status_id']);

                                    if (empty($request->approved_at) && in_array($order->status_id, [
                                            OrderStatus::STATUS_DELIVERY_BUYOUT,
                                            OrderStatus::STATUS_DELIVERY_REFUND,
                                            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                                            OrderStatus::STATUS_DELIVERY_DENIAL,
                                            OrderStatus::STATUS_DELIVERY_RETURNED
                                        ])
                                    ) {
                                        $request->approved_at = time();
                                    }

                                    if (!empty($request->tracking)) {
                                        $request->status = DeliveryRequest::STATUS_IN_PROGRESS;
                                    }

                                    if (OrderStatus::isFinalDeliveryStatus($status)) {
                                        $request->status = DeliveryRequest::STATUS_DONE;
                                        $request->done_at = time();
                                    }
                                } else {
                                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                                    $logAnswerAnswer['message'] = 'Неразрешенное значение статуса.';
                                }
                            }
                        } else {

                        }

                        /** @var DeliveryRequest $requestCheck */
                        $requestCheck = DeliveryRequest::findOne($request->id);

                        if ($requestCheck && $requestCheck->status == DeliveryRequest::STATUS_IN_LIST) {
                            $request->route = Yii::$app->controller->route;
                            $request->save();
                            if ($lastUpdateResponse) {
                                $lastUpdateResponse->save();
                            }
                        } else {
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswerAnswer['errors'] = 'Во время выполнения запроса, сменился статус заявки.';
                        }

                        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                    } catch (\Throwable $e) {

                        $logAnswerAnswer = [
                            'error' => $e->getMessage(),
                            'country_id' => $request->order->country_id,
                            'order_id' => $request->order->id,
                            'request_id' => $request->id,
                        ];

                        $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                    }
                    if (!empty($logAnswerAnswer['error']) || !empty($logAnswerAnswer['message']) || !empty($logAnswerAnswer['errors'])) {
                        $error_msg = [];
                        if (!empty($logAnswerAnswer['error'])) {
                            $error_msg[] = "error: " . $logAnswerAnswer['error'];
                        }
                        if (!empty($logAnswerAnswer['message'])) {
                            $error_msg[] = "message: " . $logAnswerAnswer['message'];
                        }
                        if (!empty($logAnswerAnswer['errors'])) {
                            $error_msg[] = "errors: " . $logAnswerAnswer['errors'];
                        }
                        if (!empty($data)) {
                            $error_msg[] = 'response:' . print_r($data, true);
                        }
                        if (!isset($response) || !($response['status'] == DeliveryApiResponse::STATUS_FAIL && $response['typeError'] == DeliveryRequest::API_WARNING_SYSTEM)) {
                            Yii::$app->notification->send(Notification::TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER, [
                                    'order_id' => $request->order->id,
                                    'country' => $request->order->country->name,
                                    'delivery' => $request->delivery->char_code,
                                    'error' => implode(' | ', $error_msg),
                                ]
                            );
                        }
                    }
                    $logAnswer->save();
                    $transaction->commit();

                    usleep(rand(1000, 1000000)); // 0.001 ... 1.0 seconds
                }
            }
            fclose($fp[$delivery->id]);
        }
        unset($fp);
        $this->log("done.");
    }

    /**
     * Уведомление о заказах, у которых нарушен срок доставки
     */
    public function actionNotDeliveredOrders()
    {
        $this->log("Not Delivered Orders...");

        $orders = DeliveryRequest::find()
            ->select([
                'order_id' => Order::tableName() . '.id'
            ])
            ->joinWith([
                'order'
            ])
            ->where([
                DeliveryRequest::tableName() . '.status' => [
                    DeliveryRequest::STATUS_IN_PROGRESS,
                    DeliveryRequest::STATUS_IN_LIST
                ]
            ])
            ->andWhere([
                'is_bad_delivery_request_sent_at(' . DeliveryRequest::tableName() . '.id, 
                ' . Order::tableName() . '.customer_zip, 
                ' . Order::tableName() . '.customer_city, 
                ' . Order::tableName() . '.customer_province)' => 1
            ])
            ->andWhere([
                'not in',
                Order::tableName() . '.status_id',
                [
                    OrderStatus::STATUS_CC_REJECTED,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_CC_TRASH,
                    OrderStatus::STATUS_CC_DOUBLE,
                    OrderStatus::STATUS_DELIVERY_LOST,
                ]
            ])
            ->asArray()
            ->all();

        foreach ($orders as $row) {
            Yii::$app->notification->send(Notification::TRIGGER_ORDER_NOT_DELIVERED, [
                'order_id' => $row['order_id'],
            ]);
        }

        $this->log("done.");
    }

    /**
     * Отправка сгенерированных листов
     */
    public function actionSendLogisticLists()
    {
        $this->log('Sending logistic lists...', false);
        date_default_timezone_set('UTC');
        set_time_limit(0);

        $lists = OrderLogisticList::find()
            ->with([
                'country.timezone',
                'country',
                'delivery',
            ])
            ->joinWith([
                'actualListExcel',
            ])
            ->where([
                'or',
                [OrderLogisticListExcel::tableName() . '.id' => null],
                [OrderLogisticListExcel::tableName() . '.sent_at' => null],
            ])
            ->andWhere(['is not', OrderLogisticList::tableName() . '.orders_hash', null])
            ->orderBy([OrderLogisticList::tableName() . '.id' => SORT_ASC])
            ->all();

        foreach ($lists as $list) {
            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $this->log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

            $logAnswer->data = [
                'list_id' => $list->id,
                'ids' => $list->ids,
            ];

            $logAnswer->answer = [];

            $timezone = $list->country->timezone;

            if ($timezone) {
                $currentTimeInCountry = time() + $timezone->time_offset;
                $createdAt = $list->created_at + $timezone->time_offset;

                if (date('Y-m-d H:i:s', $currentTimeInCountry) > date('Y-m-d 23:00:00', $createdAt)) {
                    try {
                        $listExcel = $list->delivery->apiTransmitter->sendList($list);
                        $logAnswer->answer['system_file_name'] = $listExcel->system_file_name;
                        $logAnswer->answer['delivery_file_name'] = $listExcel->delivery_file_name;

                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                    } catch (\Throwable $e) {
                        $logAnswer->answer['message'] = $e->getMessage();
                    }
                }
            } else {
                $logAnswer->answer['message'] = Yii::t('common', 'Отсутствует временная зона в стране.');
            }

            $logAnswer->data = json_encode($logAnswer->data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            $logAnswer->answer = json_encode($logAnswer->answer, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            $logAnswer->save();
        }

        $this->log('done.');
    }

    /**
     *  Автоматическая генерация и отправка листов с Экспресс заказами
     */
    public function actionGenerateExpressLists()
    {
        $this->log("Start generation and sending express logistic lists...\n", false);

        $deliveries = Delivery::find()->where([
            '!=',
            'list_send_to_emails',
            ''
        ])->autoListGeneration()->active()->expressDelivery()->with(['country'])->orderBy('country_id')->all();

        foreach ($deliveries as $delivery) {

            $listsWorker = new ListsWorker([
                'express' => true,
                'delivery' => $delivery
            ]);

            $listsWorker->generate($this->log);
            $listsWorker->send($this->log);
            $this->log('done.');
        }
    }

    /**
     *  Автоматическая генерация и отправка листов с претензиями
     * @throws \yii\base\Exception
     */
    public function actionGeneratePretensionLists()
    {
        $this->log("Start generation and sending pretension lists...\n", false);

        $deliveries = Delivery::find()
            ->with(['country'])
            ->autoPretensionListGeneration()
            ->active()
            ->orderBy('country_id')
            ->all();

        foreach ($deliveries as $delivery) {

            if ($delivery->isTimeToMakePretension()) {

                $orderQuery = Order::find()
                    ->joinWith([
                        'callCenterRequest',
                        'deliveryRequest',
                    ])
                    ->andWhere([DeliveryRequest::tableName() . ".delivery_id" => $delivery->id]);

                $orderQuery->joinWith(['orderFinancePretensions']);

                foreach (OrderLogisticList::getPretensionMapping() as $listType => $pretensionType) {

                    $queryPretension = clone $orderQuery;
                    $queryPretension->andWhere(['in', OrderFinancePretension::tableName() . '.type', $pretensionType]);
                    $queryPretension->andWhere([OrderFinancePretension::tableName() . '.status' => OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS]);
                    $queryPretension->andWhere(['>=', OrderFinancePretension::tableName() . '.created_at', strtotime("-1 days")]);

                    $orders = $queryPretension->all();

                    if ($orders) {

                        $successIds = ArrayHelper::getColumn($orders, 'id');

                        $transaction = OrderLogisticList::getDb()->beginTransaction();
                        try {
                            $list = new OrderLogisticList();
                            $list->country_id = $delivery->country_id;
                            $list->delivery_id = $delivery->id;
                            $list->type = $listType;
                            $list->number = $list->getTodayCountByDelivery() + 1;
                            $list->ids = implode(',', $successIds);
                            $list->user_id = 1;

                            if ($list->save()) {

                                if ($delivery->hasApiGenerateList()) {
                                    $excelList = $delivery->apiTransmitter->downloadList($list);
                                    $excelList->waiting_for_send = 1;
                                    if (!$excelList->save(true)) {
                                        $transaction->rollBack();
                                        throw new \Exception($excelList->getFirstErrorAsString());
                                    }
                                } else {
                                    $builder = ExcelBuilderFactory::build($list, OrderLogisticListExcel::FORMAT_XLSX,
                                        !empty($delivery->autoListGenerationColumns) ? $delivery->autoListGenerationColumns : ExcelBuilder::$autoGenerateColumns, 1);
                                    $result = $builder->generate();
                                    if ($result['status'] != 'success') {
                                        $transaction->rollBack();
                                        throw new \Exception('Возникла ошибка при генерации excel файла. Лист не может быть создан.');
                                    }
                                }

                                /** @var OrderLogisticListExcel $excelList */
                                $excelList = $list->getActualListExcel()->one();

                                $answer = [];
                                $errors = [];

                                foreach ($list->getEmailList($delivery->auto_pretension_send ? true : false) as $emailTo) {

                                    $orderLogisticListRequest = OrderLogisticListRequest::find()
                                        ->byList($list->id)
                                        ->byEmail($emailTo)
                                        ->one();
                                    if (!$orderLogisticListRequest) {
                                        $orderLogisticListRequest = new OrderLogisticListRequest();
                                        $orderLogisticListRequest->list_id = $list->id;
                                        $orderLogisticListRequest->email_to = $emailTo;
                                    }

                                    $message = $orderLogisticListRequest->buildEmailMessageForLogisticList($excelList);

                                    if ($message->send()) {
                                        $answer[] = 'Отправлено. ' . $orderLogisticListRequest->email_to;
                                        $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_SENT;
                                    } else {
                                        $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_ERROR;
                                        $errors[] = 'Не удалось отправить сообщение. ' . $orderLogisticListRequest->email_to;
                                    }
                                    if (!$orderLogisticListRequest->save(true)) {
                                        $transaction->rollBack();
                                        throw new \Exception($orderLogisticListRequest->getFirstErrorAsString());
                                    }

                                    sleep(1);
                                }

                                $excelList->sent_at = time();
                                $excelList->waiting_for_send = 0;
                                $excelList->save(false, ['sent_at', 'waiting_for_send']);

                                $list->status = OrderLogisticList::STATUS_SENT;
                                $list->save(false, ['status']);

                                OrderFinancePretension::updateAll([
                                    'status' => OrderFinancePretension::PRETENSION_STATUS_SEND,
                                    'updated_at' => time()
                                ], [
                                    'and',
                                    [
                                        '=',
                                        'status',
                                        OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS
                                    ],
                                    ['in', 'order_id', explode(',', $list->ids)]
                                ]);


                                $logAnswer = new CrontabTaskLogAnswer();
                                $logAnswer->log_id = $this->log->id;
                                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                                $logAnswer->data = json_encode([
                                    'delivery_id' => $list->delivery_id,
                                    'list_id' => $list->id,
                                    'ids' => $list->ids,
                                    'errors' => $errors,
                                    'answer' => $answer,
                                ]);

                                $logAnswer->save();

                                $transaction->commit();
                            }
                        }
                        catch (\Exception $e) {
                            $transaction->rollBack();
                            $logAnswer = new CrontabTaskLogAnswer();
                            $logAnswer->log_id = $this->log->id;
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswer->answer = json_encode([
                                'error' => $e->getMessage(),
                            ]);
                            $logAnswer->save();
                        }
                    }
                }
            }
        }

        $this->log('done.');
    }


    /**
     *  Автоматическая генерация и отправка листов
     * @param null|int $deliveryId
     */
    public function actionGenerateLists($deliveryId = null)
    {
        $this->log("Start generation and sending logistic lists...\n", false);

        $query = Delivery::find()->where([
            '!=',
            'list_send_to_emails',
            ''
        ])->autoListGeneration()->active()->with(['country'])->orderBy('country_id');
        if ($deliveryId) {
            $query->andWhere(['id' => $deliveryId]);
        }
        $deliveries = $query->all();

        foreach ($deliveries as $delivery) {

            $listsWorker = new ListsWorker([
                'delivery' => $delivery
            ]);

            $listsWorker->generate($this->log);
            $listsWorker->send($this->log);
            $this->log('done.');
        }

        $this->log('done.');
    }

    /**
     *  Автоматическая генерация и отправка коррекционных листов
     */
    public function actionGenerateCorrectionLists()
    {
        $query = CallCenterCheckRequest::find()->joinWith('order.deliveryRequest', false)
            ->where([
                CallCenterCheckRequest::tableName() . '.need_correction' => CallCenterCheckRequest::NEED_CORRECTION
            ])->andWhere([
                'in',
                Order::tableName() . '.status_id',
                OrderStatus::getProcessDeliveryAndLogisticList()
            ]);

        $query->select('DISTINCT ' . DeliveryRequest::tableName() . '.delivery_id');

        $deliveryIds = $query->column();
        $query->select = null;

        $this->log("Start generation and sending correction logistic lists...\n", false);

        foreach ($deliveryIds as $deliveryId) {
            $delivery = Delivery::findOne($deliveryId);
            if ($delivery && $delivery->active) {
                $clonedQuery = clone $query;
                $clonedQuery->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $deliveryId]);
                $requests = $clonedQuery->select([
                    CallCenterCheckRequest::tableName() . '.id',
                    CallCenterCheckRequest::tableName() . '.order_id'
                ])->asArray()->all();
                $listsWorker = new ListsWorker([
                    'correction' => true,
                    'delivery' => $delivery,
                    'ordersIds' => ArrayHelper::getColumn($requests, 'order_id'),
                ]);
                $listsWorker->generate($this->log, true);
                $listsWorker->send($this->log, true);

                CallCenterCheckRequest::updateAll([
                    'need_correction' => null,
                    'updated_at' => time()
                ], ['id' => ArrayHelper::getColumn($requests, 'id')]);
            }
        }

        $this->log('done.');
    }

    /**
     *  Перевод заказов из статуса "Курьерка (ожидает отправки)"
     */
    public function actionCheckingDeliveryPeriod($customDeliveryId = null)
    {
        $query = Order::find()
            ->select([Order::tableName() . '.pending_delivery_id'])
            ->where([Order::tableName() . '.status_id' => OrderStatus::STATUS_LOG_DEFERRED])
            ->groupBy([Order::tableName() . '.pending_delivery_id']);
        $deliveryIds = $query->column();

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");


        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);

        foreach ($deliveryIds as $deliveryId) {
            if (!empty($customDeliveryId) && $deliveryId != $customDeliveryId) {
                continue;
            }
            $delivery = Delivery::findOne($deliveryId);
            $query = Order::find()->where([
                Order::tableName() . '.pending_delivery_id' => $deliveryId,
                Order::tableName() . '.status_id' => OrderStatus::STATUS_LOG_DEFERRED,
            ]);

            if (!empty($delivery)) {
                $contract = $delivery->getActiveContract();
                if ($contract && !empty($contract->max_delivery_period)) {
                    $time = strtotime("+{$contract->max_delivery_period}days");
                    $query->andWhere([
                        '<=',
                        new Expression('IFNULL(' . Order::tableName() . '.delivery_time_from, 0)'),
                        $time
                    ]);
                }
            }

            $query->joinWith(['callCenterRequest.extra'], false);
            $query->andWhere([
                '<=',
                new Expression('IFNULL(' . CallCenterRequestExtra::tableName() . '.hold_to, 0)'),
                time(),
            ]);
            $query->with([
                'deliveryRequest',
            ]);

            foreach ($query->batch(500) as $orders) {
                /**
                 * @var Order[] $orders
                 */
                if (empty($delivery)) {
                    $cronLog("Delivery #{$deliveryId} does not exist", ['delivery_id' => $deliveryId]);
                }

                foreach ($orders as $order) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $order->route = Yii::$app->controller->route;
                        $order->status_id = OrderStatus::STATUS_CC_APPROVED;
                        if (!$order->save(false, ['status_id', 'updated_at'])) {
                            throw new \Exception($order->getFirstErrorAsString());
                        }
                        if (empty($delivery)) {
                            if ($order->deliveryRequest && $order->deliveryRequest->status == DeliveryRequest::STATUS_IN_LIST) {
                                $order->status_id = OrderStatus::STATUS_LOG_GENERATED;
                                if (!$order->save(false, ['status_id'])) {
                                    throw new \Exception($order->getFirstErrorAsString());
                                }
                            }
                        } else {
                            $delivery->prepareOrderToSend($order);
                        }
                        $transaction->commit();
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        $cronLog("Order #{$order->id} has error: " . $e->getMessage(), [
                            'order_id' => $order->id,
                            'delivery_id' => $deliveryId,
                            'error' => $e->getMessage()
                        ]);
                    }
                }

            }
        }
    }

    /**
     * @return array
     */
    protected static function getTypesForGetOrdersInfo()
    {
        return [
            self::GET_STATUSES_TYPE_DEFERRED,
            self::GET_STATUSES_TYPE_NOT_DEFERRED,
        ];
    }

    /**
     * Отправка заказов в очердь амазона на обновление статусов
     *
     * @param null $delivery_id - Ид курьерки
     * @param int $update_frequency - частота отправки каждого заказа в минутах
     * @param int $batch_size - размер одной пачки, которая отправляется за раз в очередь от одной курьерки
     * @param int $batch_count - количество пачек на одну курьерку
     */
    public function actionSendOrdersToUpdateQueue($delivery_id = null, $update_frequency = 60, $batch_size = 30, $batch_count = 40)
    {
        Yii::$app->db->enableSlaves = false;
        $client = $this->getSqsClient();

        self::$deliveries = self::getDeliveriesByRequestStatus(DeliveryRequest::STATUS_IN_PROGRESS, null, 0, 0, $update_frequency);

        $batchCounter = 0;
        while ($batchCounter < $batch_count) {
            foreach (self::$deliveries as $delivery) {
                if (is_numeric($delivery_id) && !empty($delivery_id)) {
                    if ($delivery->id != $delivery_id) {
                        continue;
                    }
                }

                if ((!$delivery->hasApiBatchGetOrdersInfoForArray() && !$delivery->hasApiGetOrdersInfoForArray()) || !$delivery->apiClass->use_amazon_queue) {
                    continue;
                }
                //Если для курьерки указана специальная очередь то выбираем ее, если нет то берем очередь по умолчанию

                if ($delivery->amazon_queue_name) {
                    $queueName = $delivery->amazon_queue_name;
                } elseif ($delivery->apiClass->amazon_queue_name) {
                    $queueName = $delivery->apiClass->amazon_queue_name;
                } else {
                    $queueName = 'amazonCheckTrackingQueueUrl';
                }

                $queueUrl = $client->getSqsQueueUrl($queueName);

                // блокируем обновление статусов для одного КЦ
                if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                    mkdir(Yii::getAlias('@runtime') . '/blocks/');
                }

                $file_lock = "/blocks/delivery_send_orders_to_update_queue_{$delivery->id}_{$delivery->char_code}_{$delivery->country->id}_{$delivery->country->char_code}.lock";

                $fp[$delivery->id] = fopen(Yii::getAlias('@runtime') . $file_lock, 'w');
                if (!flock($fp[$delivery->id], LOCK_EX | LOCK_NB)) {
                    fclose($fp[$delivery->id]);
                    continue;
                }

                $this->log("Get orders info for delivery {$delivery->id}...");

                $subQuery = DeliveryRequest::find()->select('id')->where(['delivery_id' => $delivery->id]);
                $query = DeliveryRequest::find()
                    ->innerJoin(['preparedFilter' => $subQuery], DeliveryRequest::tableName() . '.id = preparedFilter.id')
                    ->with([
                        'order.country',
                    ])
                    ->byStatus(DeliveryRequest::STATUS_IN_PROGRESS)
                    ->orderBy([
                        DeliveryRequest::tableName() . '.cron_launched_at' => SORT_ASC,
                        DeliveryRequest::tableName() . '.updated_at' => SORT_ASC
                    ]);
                if (!empty($batch_size)) {
                    $query->limit($batch_size);
                }

                if (!empty($update_frequency)) {
                    $time = strtotime("-{$update_frequency} minutes");
                    $query->andWhere([
                        'or',
                        ['is', DeliveryRequest::tableName() . '.cron_launched_at', null],
                        ['<=', DeliveryRequest::tableName() . '.cron_launched_at', $time]
                    ]);
                }

                $requests = $query->all();

                if ($delivery->hasApiBatchGetOrdersInfoForArray()) {
                    $counter = 0;
                    $batchSize = $batch_size ?: 25;
                    while ($counter < count($requests)) {
                        /**
                         * @var $batch DeliveryRequest[]
                         */
                        $batch = array_slice($requests, $counter, $batchSize);
                        $counter += $batchSize;
                        $data = ['orders' => [], 'api_class' => $delivery->apiClass->class_path];

                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $this->log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;

                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            foreach ($batch as $request) {
                                $data['orders'][] = DeliveryHandler::prepareDataForSendToUpdateQueue($request);
                            }
                            DeliveryRequest::updateAll(['cron_launched_at' => time()], ['id' => ArrayHelper::getColumn($requests, 'id')]);
                            $transaction->commit();
                        } catch (\Throwable $e) {
                            $transaction->rollBack();
                            $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                            $logAnswer->save();
                            continue;
                        }

                        $data = [
                            'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
                            'QueueUrl' => $queueUrl,
                        ];
                        $logAnswer->data = json_encode($data, JSON_UNESCAPED_UNICODE);

                        try {
                            $result = $client->sendMessage($data);
                            $logAnswer->answer = json_encode($result, JSON_UNESCAPED_UNICODE);
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                        } catch (\Throwable $e) {
                            $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                        }

                        $logAnswer->save();
                    }
                } else {
                    $messages = [];
                    foreach ($requests as $request) {
                        $data = DeliveryHandler::prepareDataForSendToUpdateQueue($request);
                        $data['api_class'] = $delivery->apiClass->class_path;

                        $messages[] = [
                            'Id' => $request->id,
                            'MessageBody' => json_encode($data, JSON_UNESCAPED_UNICODE),
                        ];
                    }

                    DeliveryRequest::updateAll(['cron_launched_at' => time()], ['id' => ArrayHelper::getColumn($requests, 'id')]);
                    $counter = 0;
                    $batchSize = 10;
                    while ($counter < count($messages)) {
                        $batch = array_slice($messages, $counter, $batchSize);
                        $counter += $batchSize;

                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $this->log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                        $data = [
                            'QueueUrl' => $queueUrl,
                            'Entries' => $batch,
                        ];
                        $logAnswer->data = json_encode($data, JSON_UNESCAPED_UNICODE);

                        try {
                            $result = $client->sendMessageBatch($data);
                            $logAnswer->answer = json_encode($result, JSON_UNESCAPED_UNICODE);
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                        } catch (\Throwable $e) {
                            $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                        }

                        $logAnswer->save();
                    }
                }

                fclose($fp[$delivery->id]);
            }
            $batchCounter++;
        }
    }

    /**
     * Расчет расходов на доставку для заказов
     */
    public function actionCalculateCharges($delivery_id = null)
    {
        $job = new DeliveryCalculateCharges();
        $job->delivery_id = $delivery_id;
        $job->log_id = $this->log->id;
        if (!$job->actionRun()) {
            $this->setInError();
            return false;
        }
        return true;
    }

    /**
     * @param int $limit
     */
    public function actionTryToSaveTransferredOrders($limit = 1000)
    {
        set_time_limit(0);

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);

        $query = OrderSendError::find()->where(['type' => OrderSendError::TYPE_DELIVERY_SEND])
            ->limit($limit)
            ->orderBy(['cron_launched_at' => SORT_ASC])
            ->with(['order.deliveryRequest.order']);

        foreach ($query->batch(500) as $records) {
            /**
             * @var OrderSendError[] $records
             */
            foreach ($records as $record) {
                $request = $record->order->deliveryRequest;
                $response = json_decode($record->response, true);
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $result = DeliveryHandler::saveResponseAfterSending($request, $response);
                    foreach ($result['messages'] as $message) {
                        $cronLog($message, ['order_id' => $request->order->id]);
                    }
                    $record->delete();
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    $record->error = $e->getMessage();
                    $record->cron_launched_at = time();
                    $cronLog("Error: " . $e->getMessage(), ['order_id' => $request->order->id]);
                    $record->save();
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function actionAutoInvoiceSender()
    {
        $options = DeliveryInvoiceOptions::find()
            ->where(['day' => gmdate('d'), 'time' => gmdate('H:i')])
            ->all();
        if (!$options) {
            return false;
        }

        foreach ($options as $option) {
//            определяем за какой период будут плановые инвойсы
            if ($option->from > $option->day) {
                $actualFrom = gmmktime(0, 0, 0, gmdate('m') - 1, $option->from, gmdate('Y'));
                $actualTo = gmmktime(0, 0, 0, gmdate('m') - 1, $option->to, gmdate('Y'));
            } else {
                $actualFrom = gmmktime(0, 0, 0, gmdate('m'), $option->from, gmdate('Y'));
                $actualTo = gmmktime(0, 0, 0, gmdate('m'), $option->to, gmdate('Y'));
            }

            $contracts = DeliveryContract::find()
                ->where(['delivery_id' => $option->delivery_id])
                ->andWhere(new Expression('date_from is null or date_from < :from', [':from' => $actualTo]))
                ->all();
            if ($contracts) {
                foreach ($contracts as $contract) {
                    $reportForm = new ReportFormInvoice(['includeDoneOrders' => false]);
                    if ($contract->delivery->invoice_currency_id && is_int($contract->delivery->invoice_currency_id)) {
                        $reportForm->currencyId = $contract->delivery->invoice_currency_id;
                    }
                    if ($contract->delivery->invoice_round_precision && is_int($contract->delivery->invoice_round_precision)) {
                        $reportForm->roundPrecision = $contract->delivery->invoice_round_precision;
                    }
//                    Сборка пропущенных данных из-за несвоевременных закрытий заказов
                    if (empty($contract->date_from) || $contract->date_from < $actualFrom) {
                        try {
                            if ($invoice = $reportForm->createInvoice($contract->id, ($contract->date_from ? strtotime($contract->date_from) : null), $actualFrom, null, $contract->requisite_delivery_id, ['status' => Invoice::STATUS_GENERATING])) {
                                Yii::$app->queueInvoiceGenerate->push(new InvoiceGenerate([
                                    'id' => $invoice->id,
                                    'emails' => ($option->delivery->auto_send_invoice ? $option->delivery->invoiceEmailList : [])
                                ]));
                            }
                        } catch (\Throwable | \Exception $e) {
                            $logAnswer = new CrontabTaskLogAnswer();
                            $logAnswer->log_id = $this->log->id;
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswer->data = json_encode($reportForm->getAttributes(), JSON_UNESCAPED_UNICODE);
                            $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                            $logAnswer->save();
                        }
                    }
//                    Плановые данные
                    if (empty($contract->date_to) || $contract->date_to > $actualFrom) {
                        try {
                            if ($invoice = $reportForm->createInvoice($contract->id, $actualFrom, $actualTo, null, $contract->requisite_delivery_id, ['status' => Invoice::STATUS_GENERATING])) {
                                Yii::$app->queueInvoiceGenerate->push(new InvoiceGenerate([
                                    'id' => $invoice->id,
                                    'emails' => ($option->delivery->auto_send_invoice ? $option->delivery->invoiceEmailList : [])
                                ]));
                            }
                        } catch (\Throwable | \Exception $e) {
                            $logAnswer = new CrontabTaskLogAnswer();
                            $logAnswer->log_id = $this->log->id;
                            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            $logAnswer->data = json_encode($reportForm->getAttributes(), JSON_UNESCAPED_UNICODE);
                            $logAnswer->answer = json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
                            $logAnswer->save();
                        }
                    }
                }
            }
        }
        return true;
    }
}
