<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\models\Country;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\order\jobs\OrderEmailNotificationJob;
use app\modules\order\jobs\OrderSmsNotificationJob;
use app\modules\order\models\Order;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\smsnotification\abstracts\SmsNotificationInformationInterface;
use app\modules\smsnotification\abstracts\SmsServiceInterface;
use Yii;

/**
 * Class SmsNotificationController
 * @package app\commands\crontab
 */
class OrderNotifierController extends CrontabController
{
    const API_GET_ANSWERS = 'http://2wstore.com/notification/get_answers.php';
    const API_LOGIN = '2wtrade_users';
    const API_PASSWORD = '2wtrade_store123321';
    //Адрес сайта, на котором принимаются ответы
    const RESPONSE_LINK = 'http://2wstore.com/notification/id';

    /**
     * @var array
     */
    protected $mapTasks = [
        'sendnotifications' => CrontabTask::TASK_ORDER_NOTIFIER_SEND_NOTIFICATIONS,
        'checknotifications' => CrontabTask::TASK_ORDER_NOTIFIER_CHECK_NOTIFICATIONS,
        'getanswersfornotification' => CrontabTask::TASK_GET_ANSWERS_FOR_NOTIFICATION,
    ];

    /**
     * Отправка смс уведомлений
     */
    public function actionSendNotifications()
    {
        $this->log('Start sending notifications to customers...');
        $countries = Country::find()->active()->all();

        foreach ($countries as $country) {
            $query = OrderNotificationRequest::find()
                ->joinWith(['order'])
                ->with([
                    'orderNotification',
                    'order.deliveryRequest.delivery',
                    'order.country.timezone',
                    'order.pendingDelivery',
                    'order.orderProducts.product'
                ])
                ->andWhere(['or',
                    [OrderNotificationRequest::tableName() . '.sms_status' => OrderNotificationRequest::SMS_STATUS_PENDING],
                    [OrderNotificationRequest::tableName() . '.email_status' => OrderNotificationRequest::EMAIL_STATUS_PENDING],
                ])
                ->andWhere([OrderNotificationRequest::tableName() . '.status' => OrderNotificationRequest::SMS_STATUS_READY])
                ->andWhere([Order::tableName() . '.country_id' => $country->id]);

            foreach ($query->batch(500) as $requests) {

                /**
                 * @var OrderNotificationRequest[] $requests
                 */
                foreach ($requests as $request) {
                    //Обычные уведомления сразу в статусе ready_to_sent, с ответом - только после того как их заберёт апи 2wstore
                    if ($request->orderNotification->sms_active
                        && $request->sms_status == OrderNotificationRequest::SMS_STATUS_PENDING
                        && !empty($request->phone_to)
                    ) {
                        $this->queueTransport->push(new OrderSmsNotificationJob(['requestId' => $request->id]));
                    }

                    if ($request->orderNotification->email_active
                        && $request->email_status == OrderNotificationRequest::EMAIL_STATUS_PENDING
                        && !empty($request->email_to)
                    ) {
                        $this->queueTransport->push(new OrderEmailNotificationJob(['requestId' => $request->id]));
                    }
                }
            }
        }
        $this->log('Sending completed.');
    }

    /**
     *  Обновление статусов заявок
     */
    public function actionCheckNotifications()
    {
        $this->log('Start getting the status of notifications ...');
        $countries = Country::find()->active()->all();

        foreach ($countries as $country) {
            $query = OrderNotificationRequest::find()
                ->joinWith(['order'])
                ->where([
                    Order::tableName() . '.country_id' => $country->id,
                    OrderNotificationRequest::tableName() . '.sms_status' => OrderNotificationRequest::SMS_STATUS_IN_PROGRESS,
                ]);

            foreach ($query->batch(500) as $requests) {
                /**
                 * @var OrderNotificationRequest[] $requests
                 */
                foreach ($requests as $request) {
                    $cronAnswer = new CrontabTaskLogAnswer();
                    $cronAnswer->log_id = $this->log->id;
                    $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                    $cronAnswer->data = json_encode([
                        'order_id' => $request->order->id,
                        'country_id' => $country->id,
                        'request_id' => $request->id,
                    ]);

                    if (empty($request->sms_foreign_id)) {
                        $cronAnswer->answer = json_encode([
                            'error' => 'Не указан внешний id'
                        ]);
                        $cronAnswer->save();
                        continue;
                    }

                    $answer = [];
                    try {
                        $result = $this->smsService->getNotificationInfo($request->sms_foreign_id);
                        $answer['response'] = $result->getResponse();
                        $request->sms_api_error = $result->getMessage();
                        $request->sms_api_code = $result->getCode();
                        if ($result->getStatus() == SmsServiceInterface::STATUS_FAIL) {
                            $request->sms_status = OrderNotificationRequest::SMS_STATUS_ERROR;
                            $answer['error'] = $result->getMessage();
                            $answer['code'] = $result->getCode();
                        } else {
                            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                            switch ($result->getSmsNotificationInformation()->getStatus()) {
                                case SmsNotificationInformationInterface::STATUS_DONE:
                                    $request->sms_status = OrderNotificationRequest::SMS_STATUS_DONE;
                                    break;
                                case SmsNotificationInformationInterface::STATUS_ERROR:
                                    $request->sms_status = OrderNotificationRequest::SMS_STATUS_ERROR;
                                    break;
                                default:
                                    $request->sms_status = OrderNotificationRequest::SMS_STATUS_IN_PROGRESS;
                                    break;
                            }
                            $request->sms_date_created = $result->getSmsNotificationInformation()->getCreatedAt() ?? time();
                            $request->sms_foreign_id = $result->getSmsNotificationInformation()->getForeignId();
                            $request->sms_foreign_status = $result->getSmsNotificationInformation()->getForeignStatus();
                            $request->sms_price = $result->getSmsNotificationInformation()->getPrice();
                            $request->sms_date_updated = $result->getSmsNotificationInformation()->getUpdatedAt();
                            $request->sms_date_sent = $result->getSmsNotificationInformation()->getSentAt() ?? $request->sms_date_sent;
                            $request->sms_uri = $result->getSmsNotificationInformation()->getUri();
                        }

                        $transaction = OrderNotificationRequest::getDb()->beginTransaction();
                        try {
                            if (!$request->save()) {
                                $answer['save_error'] = 'Ошибка при сохранении ответа от сервиса.';
                                $answer['save_errors'] = implode("\n", $request->errors);
                                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                            }
                            $cronAnswer->answer = json_encode($answer);
                            $cronAnswer->save();
                            $transaction->commit();
                        } catch (\Exception $e) {
                            $transaction->rollBack();
                            throw $e;
                        }

                    } catch (\Exception $e) {
                        $cronAnswer->answer = json_encode([
                            'error' => $e->getMessage(),
                            'code' => $e->getCode(),
                        ]);
                        $cronAnswer->save();
                        $this->log($e->getMessage() . "\n");
                    }
                }
            }
        }

        $this->log('Getting completed.');
    }

    /**
     * @return array
     */
    public function sendToApi($url)
    {
        $options = [
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
            CURLOPT_USERPWD => self::API_LOGIN . ':' . self::API_PASSWORD,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => ['from' => 1],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ];

        $c = curl_init($url);
        curl_setopt_array($c, $options);
        $error = curl_errno($c);
        $text_error = curl_error($c);
        $result = curl_exec($c);
        curl_close($c);

        return [
            'error' => $error,
            'text_error' => $text_error,
            'result' => $result
        ];
    }

    public function actionGetAnswersForNotification()
    {
        $answerApi = $this->sendToApi(self::API_GET_ANSWERS);

        $this->log('Sms & email notification get answers.. ', true);

        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        if ($answerApi['error'] != 0) {
            $cronLog($answerApi['error'] . ':' . $answerApi['text_error'], ['get-answers' => false]);
        } else {
            $response = json_decode($answerApi['result']);

            if (!$response->success) {
                $cronLog($response->error, ['get-answers' => false]);
            } else {

                $cronLog(count($response->answers), ['get-answers' => true]);

                foreach ($response->answers as $k => $v) {
                    $model = OrderNotificationRequest::find()->where(['id' => $v->foreign_id])->one();

                    if (is_null($model)) {
                        $cronLog('not found : ' . $v->foreign_id, ['get-answers' => false]);
                    } else {
                        $model->answer_text = $v->answer_text;
                        $model->answered_at = $v->answered_at;
                        $model->updated_at = $v->updated_at;

                        if (!$model->save()) {
                            $cronLog('not saved : ' . json_encode([
                                    'not saved' => 'notification',
                                    'foreign_id' => $v->foreign_id,
                                    'answered_text' => $v->answer_text,
                                    'answered_at' => $v->answered_at,
                                    'error' => var_export($model->getErrors())
                                ]), ['get-answers' => false]);
                        }
                    }
                }

            }
        }

        $this->log('done.');
    }
}
