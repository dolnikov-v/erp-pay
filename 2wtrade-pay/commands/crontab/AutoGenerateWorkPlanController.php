<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\salary\models\WorkTimePlanGenerate;
use Yii;

/**
 * Class AutoGenerateWorkPlanController
 * @package app\commands\crontab
 */
class AutoGenerateWorkPlanController extends CrontabController
{
    /** @var array */
    public $exitsWorkPlan = [];

    /** @var array */
    protected $mapTasks = [
        'run' => CrontabTask::TASK_AUTO_GENERATE_WORK_PLANE,
    ];

    /**
     * @param $from
     * @param $to
     *
     * пример запуска на определенный период php yii crontab/auto-generate-work-plan/run 2018-01-01 2018-01-31
     */
    public function actionRun($from = null, $to = null)
    {
        $WorkTimePlanGenerator = new WorkTimePlanGenerate();

        //25 числа мы генерим план на след. месяц
        $WorkTimePlanGenerator->dateFrom = $from ?? date("Y-m-01", strtotime("+1 month"));
        //на весь след. месяц
        $WorkTimePlanGenerator->dateTo = $to ?? date("Y-m-t", strtotime("+1 month"));

        $WorkTimePlanGenerator->global = true;

        $WorkTimePlanGenerator->generate(true);
    }
}