<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\components\notification\TelegramQueue;
use app\models\User;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\telegram\components\TelegramService;

/**
 * Class TelegramController
 * @package app\commands\crontab
 */
class TelegramController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'getchats' => CrontabTask::TASK_GET_TELEGRAM_CHATS,
        'sendnotifications' => CrontabTask::TASK_SEND_TELEGRAM_NOTIFICATIONS,
    ];

    /**
     * Чтение данных чатов из Telegram бота, поиск соответсвий по номерам телефона
     */
    public function actionGetChats()
    {
        $cronAnswer = new CrontabTaskLogAnswer();
        $cronAnswer->log_id = $this->log->id;
        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        $cronAnswer->save();

        $records = 0;
        $answer = [];
        try {

            $result = TelegramService::getChats();

            $cronAnswer->url = $result['url'];

            $answer['resultHttpCode'] = $result['resultHttpCode'];
            if (isset($result['resultCurlError'])) {
                $answer['resultCurlError'] = $result['resultCurlError'];
            }

            if (isset($result['data']->result)) {

                foreach ($result['data']->result as $data) {

                    $chat_id = $data->message->chat->id ?? null;
                    $phone = $data->message->contact->phone_number ?? null;

                    if (!$chat_id) {
                        //$answer['save_error'][] = 'No chat_id';
                        continue;
                    }

                    if (!$phone) {
                        //телефон приходит далеко не всегда
                        //$answer['save_error'][] = 'No phone for chat_id: ' . $chat_id;
                        continue;
                    }

                    $user = User::find()
                        ->where(['phone' => $phone])
                        ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
                        ->one();

                    if ($user instanceof User) {
                        $user->telegram_chat_id = (string)$chat_id;
                        if ($user->save(true, ['telegram_chat_id'])) {
                            $records++;
                        } else {
                            $answer['save_error'][] = 'Error:' . $user->getFirstErrorAsString();
                        }
                    } else {
                        $answer['save_error'][] = 'User not found phone: ' . $phone . ' chat_id: ' . $chat_id;
                    }
                }
            } else {
                $answer['read_error'] = "Error: Unexpected format";
                $answer['result'] = $result['data'];
                $cronAnswer->answer = json_encode($answer);
                $cronAnswer->save();
                $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            }

        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
        }

        $cronAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
        $cronAnswer->answer = json_encode($answer);

        $cronAnswer->save();
    }

    /**
     * Отправка в Telegram уведомлений о непрочитанных нотификациях пользователям системы
     */
    public function actionSendNotifications()
    {
        $this->log('Sending Telegram...', false);

        $queue = new TelegramQueue();
        $queue->send('always');

        $this->log('done.');
    }
}
