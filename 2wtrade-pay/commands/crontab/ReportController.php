<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\helpers\i18n\Formatter;
use app\models\Country;
use app\models\Currency;
use app\models\Notification;
use app\models\Product;
use app\models\UserCountry;
use app\models\UserNotification;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestHistory;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\finance\components\ReportFormProfitabilityAnalysisForCompanyStandards;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\logger\components\log\Logger;
use app\modules\notification\components\AdcomboStatus;
use app\modules\notification\components\Buyouts;
use app\modules\notification\components\BuyoutsByCountry;
use app\modules\notification\components\CallCenterDailyReport;
use app\modules\notification\components\CallCenterEfficiencyOperators;
use app\modules\notification\components\CheckDayApprove;
use app\modules\notification\components\HoldLead;
use app\modules\notification\components\Skype;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\debts\DebtsDaily;
use app\modules\report\components\debts\models\DebtsDailyRequest;
use app\modules\report\components\invoice\generators\InvoiceFinanceBuilder;
use app\modules\report\components\invoice\generators\InvoiceTableBuilder;
use app\modules\report\components\ReportAdcomboComparator;
use app\modules\report\components\ReportCallCenterComparator;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\models\CompanyStandards;
use app\modules\report\models\CompanyStandardsDelivery;
use app\modules\report\models\Invoice;
use app\modules\report\models\ReportAdcombo;
use app\modules\report\models\ReportCallCenter;
use app\modules\report\models\ReportDebts;
use app\modules\reportoperational\models\Team;
use app\modules\reportoperational\models\TeamCountry;
use app\modules\reportoperational\models\TeamCountryUser;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\Office;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\telegram\components\TelegramService;
use app\models\NotificationUser;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class ReportController
 * @package app\commands\crontab
 *
 * @property array $ordersInHoldStatusNotificationData
 * @property array $ordersInReadyToSendStatusNotificationData
 * @property array $holdsNotificationData
 */
class ReportController extends CrontabController
{
    const REPORTING_CUTOFF = 2678400; // 31 день в секундах
    const CC_APPROVED_EXPIRATION = 86400; // сутки в секундах
    const DELIVERY_PENDING_EXPIRATION = 172800; // двое суток
    const ORDER_NEW_STATUS_STALE_HOUR = 3600;   // час в секундах
    const ORDER_NEW_STATUS_STALE_DAY = 86400;   // сутки в секундах
    const MONTH_IN_SECONDS = 2678400;           // 31 день в секундах
    const FREEZE_NORMS_FOR_STATUSES = [
        '- 5 min' => [1, 2],
        '- 24 hours' => [6],
        '- 6 hours' => [9],
        '- 12 hours' => [12, 13, 14],
        '- 15 days' => [15],
    ];
    const LIMIT_STORAGE_PRODUCT = 2000;

    protected $mapTasks = [
        'insertdebts' => CrontabTask::TASK_REPORT_INSERT_DEBTS,
        'orderdaily' => CrontabTask::TASK_REPORT_ORDER_DAILY,
        'twodayserror' => CrontabTask::TASK_REPORT_ORDER_TWO_DAYS_ERROR,
        'productonstorage' => CrontabTask::TASK_REPORT_PRODUCT_ON_STORAGE,
        'comparewithadcombo' => CrontabTask::TASK_REPORT_COMPARE_WITH_ADCOMBO,
        'noproductsfororder' => CrontabTask::TASK_REPORT_NO_PRODUCTS_FOR_ORDER,
        'deliveryrejected' => CrontabTask::TASK_REPORT_DELIVERY_REJECTED,
        'staleccapproved' => CrontabTask::TASK_REPORT_STALE_CC_APPOVED,
        'staledeliverypending' => CrontabTask::TASK_REPORT_STALE_DELIVERY_PENDING,
        'comparewithcallcenter' => CrontabTask::TASK_REPORT_COMPARE_WITH_CALL_CENTER,
        'staleordernewstatus' => CrontabTask::TASK_ORDER_NEW_STATUS_STALE,
        'notrackinsentorders' => CrontabTask::TASK_REPORT_NO_TRACK_SENT_ORDERS,
        'orderdailyfluctuation' => CrontabTask::TASK_REPORT_ORDER_DAILY_FLUCTUATION,
        'calculatedeliverydebts' => CrontabTask::TASK_REPORT_CALCULATE_DELIVERY_DEBTS,
        'ordersunsent' => CrontabTask::TASK_REPORT_ORDERS_UNSENT,
        'ordersinstatus12' => CrontabTask::TASK_REPORT_STATUS_IN_1_2,
        'ordersinstatus31' => CrontabTask::TASK_REPORT_STATUS_IN_31,
        'ordersfrozen' => CrontabTask::TASK_REPORT_ORDERS_FROZEN,
        'ordersstillfrozen' => CrontabTask::TASK_REPORT_ORDERS_STILL_FROZEN,
        'noanswerfromcc' => CrontabTask::TASK_REPORT_NO_ANSWER_FROM_CC,
        'rapidincreaseordersinautotrash' => CrontabTask::TASK_REPORT_RAPID_INCREASE_ORDERS_IN_AUTOTRASH,
        'smsbuyoutsnotification' => CrontabTask::TASK_SMS_BUYOUTS_NOTIFICATION,
        'buyoutsbycountry' => CrontabTask::TASK_SMS_BUYOUTS_NOTIFICATION,
        'skypebuyoutscoordination' => CrontabTask::TASK_SKYPE_BUYOUTS_COORDINATION,
        'checkapprovedorders' => CrontabTask::TASK_CALL_CENTER_NOT_APPROVE_NOTIFICATION,
        'holdleadspercent' => CrontabTask::TASK_CALL_CENTER_HOLD_LEADS,
        'checkapprovelasthour' => CrontabTask::TASK_CALL_CENTER_NO_APPROVE_LAST_HOUR,
        'checkdayapprove' => CrontabTask::TASK_CALL_CENTER_CHECK_DAY_APPROVE,
        'callcenterdailyreport' => CrontabTask::TASK_CALL_CENTER_DAILY_REPORT,
        'callcenterefficiencyoperators' => CrontabTask::TASK_CALL_CENTER_EFFICIENCY_OPERATORS,
        'checkadcombogetstatuses' => CrontabTask::TASK_ADCOMBO_NOT_GET_STATUSES,
        'adcombostatus' => CrontabTask::TASK_REPORT_ADCOMBO_STATUS,
        'holdordersnotification' => CrontabTask::TASK_SEND_HOLDS_NOTIFICATIONS,
        'ordersinholdstatus' => CrontabTask::TASK_ORDERS_IN_HOLD_STATUS,
        'ordersinreadytosendstatus' => CrontabTask::TASK_ORDERS_IN_READY_TO_SEND_STATUS,
        'generateinvoices' => CrontabTask::TASK_REPORT_GENERATE_INVOICES,
        'ordersinstatuses12_9_37_38' => CrontabTask::TASK_REPORT_STATUS_IN_12_9_37_38,
        'calculatedeliverydebtsold' => CrontabTask::TASK_REPORT_CALCULATE_DELIVERY_DEBTS_OLD,
        'calculatecompanystandards' => CrontabTask::TASK_REPORT_CALCULATE_COMPANY_STANDARDS,
        'savedeliverydebtsdaily' => CrontabTask::TASK_REPORT_SAVE_DELIVERY_DEBTS_DAILY,
    ];

    /**
     * рассылка (ежедневных) нотификаций о количестве заказов застрявших в статусе CC_APPROVED больше чем на день
     *
     */
    public function actionStaleCcApproved()
    {
        $data = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->joinWith('callCenterRequest')
            ->where([">", Order::tableName() . ".created_at", time() - self::REPORTING_CUTOFF])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_CC_APPROVED])
            ->andWhere(["<", CallCenterRequest::tableName() . ".approved_at", time() - self::CC_APPROVED_EXPIRATION])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $stopList = Country::getStopListCountryNotification();

        $this->log("sending notifications to " . count($data) . " countries");
        foreach ($data as $datum) {
            if (!in_array((int)$datum['country_id'], $stopList)) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_STALE_CC_APPROVED,
                    [
                        'count' => $datum['count'],
                    ],
                    (int)$datum['country_id']
                );
            }

        }
    }


    /**
     * Заказы в статусах отправки в КС с разбивкой по странам и командам
     */
    public function actionOrdersInReadyToSendStatus()
    {

        $Texts = [];
        $teams = Team::getAllTeams();

        $data = $this->getOrdersInReadyToSendStatusNotificationData();

        // формируем данные для смс и/или телеграма для тимлида каждой команды
        foreach ($teams as $team) {

            if (isset($team['leader_id']) && intval($team['leader_id']) > 0) {      // Исключаем команды без тимлидов
                $teamCountries = TeamCountry::getTeamCountriesList($team['id']);

                $teamText = $this->prepareOrdersInReadyToSendStatusNotificationText($team, $teamCountries, $data);

                $Texts[] = $teamText;

                // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
                $chatId = 0;
                if ($team['leader_id'] == 12) {         // чат мельникова
                    $chatId = -260910681;
                }
                if ($team['leader_id'] == 248) {        // чат кураева/антипченко
                    $chatId = -296114036;
                }
                if ($chatId < 0) {
                    $this->log("sending notifications to telegram for leader of " . $team['name'] . " team...");
                    $res = TelegramService::sendNotification($chatId, $teamText);
                    if ($res['data']->ok) {
                        $this->log("ok");
                    } else {
                        $this->log("error");
                    }
                    $this->log(PHP_EOL);
                }

            }
        }

        // формируем сводные данные для директора
        $text = '';
        foreach ($Texts as $t) {
            $text .= $t . PHP_EOL;
        }

        // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
        $this->log("sending notifications to telegram for director...");
        $res = TelegramService::sendNotification(-293397591, $text);            // чат для Юли и Джеффа
        if ($res) {
            $this->log("ok");
        } else {
            $this->log("error");
        }
        $this->log(PHP_EOL);

    }

    /**
     * Заказы в статусах холдов с разбивкой по странам и командам
     */
    public function actionOrdersInHoldStatus()
    {
        $Texts = [];
        $teams = Team::getAllTeams();

        $data = $this->getOrdersInHoldStatusNotificationData();

        // формируем данные для смс и/или телеграма для тимлида каждой команды
        foreach ($teams as $team) {

            if (isset($team['leader_id']) && intval($team['leader_id']) > 0) {      // Исключаем команды без тимлидов
                $teamCountries = TeamCountry::getTeamCountriesList($team['id']);
                $teamText = $this->prepareOrdersInHoldStatusNotificationText($team, $teamCountries, $data);

                $Texts[] = $teamText;

                // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
                $chatId = 0;
                if ($team['leader_id'] == 12) {         // чат мельникова
                    $chatId = -260910681;
                }
                if ($team['leader_id'] == 248) {        // чат кураева/антипченко
                    $chatId = -296114036;
                }
                if ($chatId < 0) {
                    $this->log("sending notifications to telegram for leader of " . $team['name'] . " team...");
                    $res = TelegramService::sendNotification($chatId, $teamText);
                    if ($res['data']->ok) {
                        $this->log("ok");
                    } else {
                        $this->log("error");
                    }
                    $this->log(PHP_EOL);
                }

            }
        }

        // формируем сводные данные для директора
        $text = '';
        foreach ($Texts as $t) {
            $text .= $t . PHP_EOL;
        }

        // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
        $this->log("sending notifications to telegram for director...");
        $res = TelegramService::sendNotification(-293397591, $text);            // чат для Юли и Джеффа
        if ($res) {
            $this->log("ok");
        } else {
            $this->log("error");
        }
        $this->log(PHP_EOL);

    }

    /**
     * рассылка (ежедневных) нотификаций о количестве заказов в холдах за сегодня (утром и вечером)
     *
     */
    public function actionHoldOrdersNotification()
    {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $Texts = [];
        $teams = Team::getAllTeams();

        $data = $this->getHoldsNotificationData();

        // формируем данные для смс и/или телеграма для тимлида каждой команды
        foreach ($teams as $team) {

            if (isset($team['leader_id']) && intval($team['leader_id']) > 0) {      // Исключаем команды без тимлидов
                $teamCountries = TeamCountry::getTeamCountriesList($team['id']);
                $teamText = $this->prepareHoldsNotificationText($team, $teamCountries, $data);

                $Texts[] = $teamText;

                $this->log("sending notifications to transport query for leader of " . $team['name'] . PHP_EOL);

                // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
                $chatId = 0;
                if ($team['leader_id'] == 12) {         // чат мельникова
                    $chatId = -260910681;
                }
                if ($team['leader_id'] == 248) {        // чат кураева/антипченко
                    $chatId = -296114036;
                }
                if ($chatId < 0) {
                    $this->log("sending notifications to telegram for leader of " . $team['name'] . " team...");
                    $res = TelegramService::sendNotification($chatId, $teamText);
                    if ($res['data']->ok) {
                        $this->log("ok");
                    } else {
                        $this->log("error");
                    }
                    $this->log(PHP_EOL);
                }

            }
        }

        // формируем сводные данные для директора
        $text = '';
        foreach ($Texts as $t) {
            $text .= $t . PHP_EOL;
        }

        // Временно отправляем в телеграм сообщения напрямую в нужный чат (костыль)
        $this->log("sending notifications to telegram for director...");
        $res = TelegramService::sendNotification(-293397591, $text);            // чат для Юли и Джеффа
        if ($res) {
            $this->log("ok");
        } else {
            $this->log("error");
        }
        $this->log(PHP_EOL);

    }


    /**
     * рассылка (ежедневных) sms нотификаций о количестве выкупов за цикл 7 дней
     *
     * @param null|string $testMode
     *
     * @throws Exception
     */
    public function actionSmsBuyoutsNotification($testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $days = [7, 15, 30, 45];
        $checkDates = Buyouts::getCheckDates($days);
        $allTeamsText = '';
        $teams = Team::getAllTeams();

        $data = Buyouts::getData($checkDates);
        // формируем данные для смс и/или телеграма для тимлида каждой команды
        foreach ($teams as $team) {
            $userIds = [];
            if (!empty($team['leader_id'])) {
                $userIds[$team['leader_id']] = $team['leader_id'];
            }
            $teamCountryUsers = TeamCountryUser::find()->select(['user_id'])->distinct()->byTeam($team['id'])->column();
            if ($teamCountryUsers) {
                $userIds = $userIds + array_combine($teamCountryUsers, $teamCountryUsers);
            }
            $teamCountries = TeamCountry::getTeamCountriesNamesList($team['id']);
            $teamText = Buyouts::prepareText($team, $teamCountries, $data, $checkDates);
            // формируем сводные данные для директора
            $allTeamsText .= $teamText;

            if ($testMode) {
                continue;
            }
            if ($userIds) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_TEAM_BUYOUTS_NOTIFICATION,
                    [
                        'text' => $teamText,
                    ],
                    null,
                    $userIds
                );
            }
        }

        if ($testMode) {
            $testNotifier = 6;
        }

        Yii::$app->notification->send(
            Notification::TRIGGER_ALL_TEAMS_BUYOUTS_NOTIFICATION,
            [
                'text' => $allTeamsText,
            ],
            null,
            $testNotifier ?? null
        );
    }

    public function actionSkypeBuyoutsCoordination($testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $days = [7, 15, 30, 45];
        $checkDates = Buyouts::getCheckDates($days);
        $allTeamsText = "Общая информация по выкупам всех команд" . "<br />";
        $teams = Team::getAllTeams();

        $data = Buyouts::getData($checkDates);
        $anyText = false;
        foreach ($teams as $team) {
            if (!isset($team['leader_id'])) {
                continue;
            }
            $teamCountries = TeamCountry::getTeamCountriesNamesList($team['id']);
            $teamText = Buyouts::prepareText($team, $teamCountries, $data, $checkDates, "<br />", true);
            if ($teamText != "") {
                $anyText = true;
                $allTeamsText .= $teamText;
            }
        }
        if ($anyText) {
            Skype::sendSkypeMessage($allTeamsText, $testMode ? Skype::TEST_CHAT_ID : Skype::COORDINATION_CHAT_ID);
        }
    }


    /**
     * @param null $onlyCountryId
     * @param null $testMode
     */
    public function actionBuyoutsByCountry($onlyCountryId = null, $testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $days = [7, 15, 30, 45];
        $checkDates = BuyoutsByCountry::getCheckDates($days);
        $data = BuyoutsByCountry::getData($checkDates);

        $countries = ArrayHelper::index(
            Country::find()
                ->where(['id' => array_keys($data)])
                ->asArray()
                ->all(),
            'id');

        if ($onlyCountryId) {
            $tmp = $countries[$onlyCountryId] ?? [];
            unset($countries);
            $countries[$onlyCountryId] = $tmp;
        }

        foreach ($data as $countryId => $countryData) {
            if (isset($countries[$countryId])) {
                BuyoutsByCountry::sendMessageByCountry($countryData, $countries[$countryId], $checkDates, $testMode);
            }
        }
    }


    /**
     * рассылка (ежедневных) нотификаций о существенных суточных колебаниях заказов (+/-30%) относительно 31 дня
     *
     */
    public function actionOrderDailyFluctuation()
    {
        $dataLastMonth = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->where([">=", Order::tableName() . ".created_at", time() - self::MONTH_IN_SECONDS - 86400])
            ->andWhere(["<", Order::tableName() . ".created_at", time() - 86400])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $dataLastDay = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->where([">=", Order::tableName() . ".created_at", time() - 86400])
            ->andWhere(["<", Order::tableName() . ".created_at", time()])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $stopList = Country::getStopListCountryNotification();

        foreach ($dataLastDay as $day) {
            foreach ($dataLastMonth as $month) {
                if ($day['country_id'] == $month['country_id']) {
                    $delta = $day['count'] - ($month['count'] / 31);
                    $sign = '';
                    if ($delta > 0) {
                        $sign = 'больше';
                    }
                    if ($delta < 0) {
                        $sign = 'меньше';
                    }
                    $delta = abs($delta) / ($month['count'] / 31) * 100;    //переводим в %
                    if ($delta >= 30) {
                        if (!in_array((int)$day['country_id'], $stopList)) {
                            Yii::$app->notification->send(
                                Notification::TRIGGER_REPORT_ORDER_DAILY_FLUCTUATION,
                                [
                                    'delta' => $delta,
                                    'sign' => $sign,
                                ],
                                (int)$day['country_id']
                            );
                            break;
                        }
                    }
                }
            }
        }
    }


    /**
     * рассылка (ежедневных) нотификаций о количестве заказов отправленных в курьерку без трекера за последние 31 день
     *
     */
    public function actionNoTrackInSentOrders()
    {
        $data = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->joinWith('deliveryRequest')
            ->where(["LIKE", DeliveryRequest::tableName() . ".tracking", ""])
            ->orWhere(["IS", DeliveryRequest::tableName() . ".tracking", null])
            ->andwhere([">=", DeliveryRequest::tableName() . ".sent_at", time() - self::MONTH_IN_SECONDS])
            ->andWhere(["<", DeliveryRequest::tableName() . ".sent_at", time()])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $stopList = Country::getStopListCountryNotification();

        $this->log("sending notifications to " . count($data) . " countries");

        foreach ($data as $datum) {
            if (!in_array((int)$datum['country_id'], $stopList)) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_NO_TRACK_SENT_ORDERS,
                    [
                        'count' => $datum['count'],
                    ],
                    (int)$datum['country_id']
                );
            }
        }
    }

    /**
     * рассылка (ежечасно) нотификаций о количестве новых заказов застрявших в статусе 1 или 2 больше чем на час и на сутки
     *
     */
    public function actionStaleOrderNewStatus()
    {
        $dataOverDay = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->where([Order::tableName() . '.status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM])
            ->orWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_SOURCE_LONG_FORM])
            ->andWhere(["<", Order::tableName() . ".updated_at", time() - self::ORDER_NEW_STATUS_STALE_DAY])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $dataOverHour = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->where([Order::tableName() . '.status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM])
            ->orWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_SOURCE_LONG_FORM])
            ->andWhere(["<", Order::tableName() . ".updated_at", time() - self::ORDER_NEW_STATUS_STALE_HOUR])
            ->andWhere([">", Order::tableName() . ".updated_at", time() - self::ORDER_NEW_STATUS_STALE_DAY])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $activeCityArray = [];
        // формируем массив активных городов
        foreach ($dataOverDay as $item) {
            $activeCityArray[] = $item['country_id'];
        }
        // дозаполняем массив активных городов
        foreach ($dataOverHour as $item) {
            $activeCityArray[] = $item['country_id'];
        }
        $activeCityArray = array_unique($activeCityArray);
        sort($activeCityArray);

        $stopList = Country::getStopListCountryNotification();

        $this->log("sending notifications to " . count($activeCityArray) . " countries");

        for ($i = 0; $i < count($activeCityArray); $i++) {
            $dayCount = 0;
            $hourCount = 0;

            foreach ($dataOverDay as $item) {
                if (intval($item['country_id']) == $i) {
                    $dayCount = intval($item['count']);
                    break;
                }
            }
            foreach ($dataOverHour as $item) {
                if (intval($item['country_id']) == $i) {
                    $hourCount = intval($item['count']);
                    break;
                }
            }

            $resArray = [
                'country_id' => intval($activeCityArray[$i]),
                'overhour' => $hourCount,
                'overday' => $dayCount,
            ];

            if (isset($resArray['overhour']) && isset($resArray['overday'])) {
                if ($resArray['overday'] != 0 || $resArray['overhour'] != 0) {
                    if (!in_array((int)$resArray['country_id'], $stopList)) {
                        Yii::$app->notification->send(
                            Notification::TRIGGER_REPORT_STALE_NEW_ORDER,
                            [
                                'overday' => $resArray['overday'],
                                'overhour' => $resArray['overhour'],
                            ],
                            (int)$resArray['country_id']
                        );
                    }
                }
            }
        }
    }

    /**
     * рассылка (ежедневных) нотификаций о количестве заказов застрявших в статусе DELIVERY_PENDING больше чем на два дня
     *
     */
    public function actionStaleDeliveryPending()
    {
        $data = Order::find()
            ->select([
                'count' => 'count(*)',
                'country_id' => 'country_id',
            ])
            ->joinWith('deliveryRequest')
            ->where([">", Order::tableName() . ".created_at", time() - self::REPORTING_CUTOFF])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_PENDING])
            ->andWhere(["<", DeliveryRequest::tableName() . ".sent_at", time() - self::DELIVERY_PENDING_EXPIRATION])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();
        $this->log("sending notifications to " . count($data) . " countries");

        $stopList = Country::getStopListCountryNotification();

        foreach ($data as $datum) {
            if (!in_array((int)$datum['country_id'], $stopList)) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_STALE_DELIVERY_PENDING,
                    [
                        'count' => $datum['count'],
                    ],
                    (int)$datum['country_id']
                );
            }
        }

    }

    /**
     * Рассылка (ежедневных) нотификаций о количестве заказов отклоненных в курьерке за последний примерно месяц
     */
    public function actionDeliveryRejected()
    {
        $data = Order::find()
            ->select([
                'count' => 'count(id)',
                'country_id' => 'country_id',
            ])
            ->where([">", "updated_at", time() - self::REPORTING_CUTOFF])
            ->andWhere(['status_id' => OrderStatus::STATUS_DELIVERY_REJECTED])
            ->groupBy(["country_id"])
            ->asArray()
            ->all();

        $stopList = Country::getStopListCountryNotification();

        foreach ($data as $datum) {
            if (!in_array((int)$datum['country_id'], $stopList)) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_DELIVERY_REJECTED,
                    [
                        'count' => $datum['count'],
                    ],
                    (int)$datum['country_id']
                );
            }
        }
    }

    /**
     * Заполнение отчетной таблицы report_debts данными
     */
    public function actionInsertDebts()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            /*
             * Основной sql-запрос отчета:
             * SELECT o.country_id as country_id,
                       d.delivery_id as delivery_id,
                       FROM_UNIXTIME(d.sent_at, '%Y-%m') as month_year,
                       o.status_id as status_id,
                       SUM(o.price) as summa
                FROM `order` o,
                    delivery_request d
                WHERE o.id = d.order_id
                      and d.sent_at!=null
                GROUP BY o.country_id, d.delivery_id, FROM_UNIXTIME(d.sent_at, '%Y-%m'), o.status_id
                ORDER BY o.country_id, d.delivery_id, FROM_UNIXTIME(d.sent_at, '%Y-%m'), o.status_id
             */

            $this->log('Begin Reports/insertDebts...');
            $nowDay = time();

            $data = Order::find()
                ->select([
                    'id' => '`order`.id',
                    'country_id' => '`order`.`country_id`',
                    'delivery_id' => '`delivery_request`.`delivery_id`',
                    'month_year' => 'CONCAT(FROM_UNIXTIME(`delivery_request`.`sent_at`, "%Y-%m"),"-01")',
                    'status_id' => '`order`.`status_id`',
                    'total' => 'SUM(`order`.`price`)',
                ])
                ->joinWith('deliveryRequest')
                ->where(['!=', '`delivery_request`.`sent_at`', 'null'])
                ->groupBy([
                    'order.country_id',
                    'delivery_request.delivery_id',
                    'CONCAT(FROM_UNIXTIME(`delivery_request`.`sent_at`, "%Y-%m"),"-01")',
                    'order.status_id',
                ])
                ->asArray()
                ->all();

            $monthYear = null;
            $debts = 0;
            $potentialRevenue = 0;
            $revenue = 0;
            $refund = 0;
            $deliveryId = null;
            $countryId = null;
            $i = 0;

            foreach ($data as $row) {
                if (
                    ($monthYear != null && $monthYear != $row['month_year']) ||
                    ($deliveryId != null && $deliveryId != $row['delivery_id']) ||
                    ($countryId != null && $countryId != $row['country_id'])
                ) {
                    $reportItem = new ReportDebts();
                    $reportItem->day = $nowDay;
                    $reportItem->month_year = $monthYear;
                    $reportItem->debts = $debts;
                    $reportItem->potential_revenue = $potentialRevenue;
                    $reportItem->revenue = $revenue;
                    $reportItem->refund = $refund;
                    $reportItem->delivery_id = $deliveryId;
                    $reportItem->country_id = $countryId;
                    $dateCreate = time();
                    $reportItem->created_at = $dateCreate;
                    $reportItem->updated_at = $dateCreate;
                    $reportItem->save(false);

                    $debts = 0;
                    $potentialRevenue = 0;
                    $revenue = 0;
                    $refund = 0;
                    $monthYear = null;
                    $deliveryId = null;
                    $countryId = null;
                    $i++;
                }

                switch ($row['status_id']) {
                    case 15:
                        $debts += $row['total'];
                        $potentialRevenue += $row['total'];
                        break;
                    case 16:
                        $refund += $row['total'];
                        break;
                    case 18:
                        $revenue += $row['total'];
                        break;
                    case 12:
                    case 14:
                    case 21:
                    case 22:
                    case 24:
                    case 27:
                    case 29:
                        $potentialRevenue += $row['total'];
                        break;
                    default:
                        break;
                }

                $deliveryId = $row['delivery_id'];
                $monthYear = $row['month_year'];
                $countryId = $row['country_id'];
            }

            if ($monthYear != null && $deliveryId != null && $countryId != null) {
                $reportItem = new ReportDebts();
                $reportItem->day = $nowDay;
                $reportItem->month_year = $monthYear;
                $reportItem->debts = $debts;
                $reportItem->potential_revenue = $potentialRevenue;
                $reportItem->revenue = $revenue;
                $reportItem->refund = $refund;
                $reportItem->delivery_id = $deliveryId;
                $reportItem->country_id = $countryId;
                $dateCreate = time();
                $reportItem->created_at = $dateCreate;
                $reportItem->updated_at = $dateCreate;
                $reportItem->save(false);

                $i++;
            }

            $transaction->commit();

            $this->log("Добавлено $i записей за " . date("d.m.Y"));
            $this->log('done.');

        } catch (\Exception $e) {
            $this->setInError();
            $this->log('Ошибка: ' . $e->getMessage());
            $transaction->rollback();
        }
    }

    /**
     * Отправка оповещения от отчета DailyReport (Ежедневная сверка заказов)
     */
    public function actionOrderDaily()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->log('Begin OrderDaily...');

            $timeTo = strtotime(date('Y-m-d')) - 1;
            $timeFrom = $timeTo - 86399;

            $dataOrders = Order::find()
                ->joinWith([
                    'country',
                ])
                ->select([
                    'created' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', Order::tableName() . '.created_at', $timeFrom])
                ->andWhere(['<=', Order::tableName() . '.created_at', $timeTo])
                ->groupBy([
                    'country_id',
                ])
                ->asArray()
                ->all();

            $dataCallCenterApproved = Order::find()
                ->joinWith([
                    'country',
                    'callCenterRequest',
                ])
                ->select([
                    'call_approved' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', CallCenterRequest::tableName() . '.approved_at', $timeFrom])
                ->andWhere(['<=', CallCenterRequest::tableName() . '.approved_at', $timeTo])
                ->groupBy([
                    'country_id',
                ])
                ->asArray()
                ->all();

            $dataCallCenterSent = Order::find()
                ->joinWith([
                    'country',
                    'callCenterRequest',
                ])
                ->select([
                    'call_sent' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', CallCenterRequest::tableName() . '.sent_at', $timeFrom])
                ->andWhere(['<=', CallCenterRequest::tableName() . '.sent_at', $timeTo])
                ->groupBy([
                    'country_id',
                ])
                ->asArray()
                ->all();

            $dataDeliverySent = Order::find()
                ->joinWith([
                    'country',
                    'deliveryRequest',
                ])
                ->select([
                    'delivery_sent' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $timeFrom])
                ->andWhere(['<=', DeliveryRequest::tableName() . '.sent_at', $timeTo])
                ->groupBy([
                    'country_id',
                ])
                ->asArray()
                ->all();

            $dataDeliveryError = Order::find()
                ->joinWith([
                    'country',
                    'deliveryRequest',
                ])
                ->select([
                    'delivery_error' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $timeFrom])
                ->andWhere(['<=', DeliveryRequest::tableName() . '.sent_at', $timeTo])
                ->andWhere([DeliveryRequest::tableName() . '.status' => 'error'])
                ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_REJECTED])
                ->groupBy([
                    'country_id',
                ])
                ->asArray()
                ->all();

            $resultArray = [];
            foreach ($dataOrders as $dataItem) {
                $resultArray[$dataItem['country_id']] = [
                    'created' => $dataItem['created'],
                    'call_approved' => 0,
                    'call_sent' => 0,
                    'delivery_sent' => 0,
                    'delivery_error' => 0,
                    'country_id' => $dataItem['country_id'],
                ];
            }

            foreach ($dataCallCenterApproved as $dataItem) {
                if (isset($resultArray[$dataItem['country_id']])) {
                    $resultArray[$dataItem['country_id']]['call_approved'] = $dataItem['call_approved'];
                } else {
                    $resultArray[$dataItem['country_id']] = [
                        'created' => 0,
                        'call_approved' => $dataItem['call_approved'],
                        'call_sent' => 0,
                        'delivery_sent' => 0,
                        'delivery_error' => 0,
                        'country_id' => $dataItem['country_id'],
                    ];
                }
            }

            foreach ($dataCallCenterSent as $dataItem) {
                if (isset($resultArray[$dataItem['country_id']])) {
                    $resultArray[$dataItem['country_id']]['call_sent'] = $dataItem['call_sent'];
                } else {
                    $resultArray[$dataItem['country_id']] = [
                        'created' => 0,
                        'call_approved' => 0,
                        'call_sent' => $dataItem['call_sent'],
                        'delivery_sent' => 0,
                        'delivery_error' => 0,
                        'country_id' => $dataItem['country_id'],
                    ];
                }
            }

            foreach ($dataDeliverySent as $dataItem) {
                if (isset($resultArray[$dataItem['country_id']])) {
                    $resultArray[$dataItem['country_id']]['delivery_sent'] = $dataItem['delivery_sent'];
                } else {
                    $resultArray[$dataItem['country_id']] = [
                        'created' => 0,
                        'call_approved' => 0,
                        'call_sent' => 0,
                        'delivery_sent' => $dataItem['delivery_sent'],
                        'delivery_error' => 0,
                        'country_id' => $dataItem['country_id'],
                    ];
                }
            }

            foreach ($dataDeliveryError as $dataItem) {
                if (isset($resultArray[$dataItem['country_id']])) {
                    $resultArray[$dataItem['country_id']]['delivery_error'] = $dataItem['delivery_error'];
                } else {
                    $resultArray[$dataItem['country_id']] = [
                        'created' => 0,
                        'call_approved' => 0,
                        'call_sent' => 0,
                        'delivery_sent' => 0,
                        'delivery_error' => $dataItem['delivery_error'],
                        'country_id' => $dataItem['country_id'],
                    ];
                }
            }

            $stopList = Country::getStopListCountryNotification();

            foreach ($resultArray as $resultKey => $resultValue) {
                try {
                    if (!in_array((int)$resultValue['country_id'], $stopList)) {
                        Yii::$app->notification->send(Notification::TRIGGER_REPORT_ORDER_DAILY, [
                            'dateFrom' => Yii::$app->formatter->asDatetime($timeFrom, Formatter::getDatetimeFormat()),
                            'dateTo' => Yii::$app->formatter->asDatetime($timeTo, Formatter::getDatetimeFormat()),
                            'created' => $resultValue['created'],
                            'call_approved' => $resultValue['call_approved'],
                            'call_sent' => $resultValue['call_sent'],
                            'delivery_sent' => $resultValue['delivery_sent'],
                            'delivery_error' => $resultValue['delivery_error'],
                        ],
                            $resultValue['country_id']);
                    }
                } catch (\Exception $e) {
                    $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                }
            }

            $transaction->commit();

            $this->log('done.');
        } catch (\Exception $e) {
            $this->setInError();
            $this->log('Error: ' . $e->getMessage());
            $transaction->rollback();
        }
    }

    /**
     * Отправка оповещения о заказах с ошибкой более одного дня
     */
    public function actionTwoDaysError()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->log('Begin TwoDaysError...');

            $timeTo = strtotime(date('Y-m-d')) - 1;
            $timeFrom = $timeTo - 86399 - 86400;

            $dataDelivery = Order::find()
                ->joinWith([
                    'country',
                    'deliveryRequest',
                ])
                ->select([
                    'delivery_error' => 'COUNT(1)',
                    'country_id' => Country::tableName() . '.id',
                    'id' => Order::tableName() . '.id',
                ])
                ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $timeFrom])
                ->andWhere(['<=', DeliveryRequest::tableName() . '.sent_at', $timeTo])
                ->andWhere([DeliveryRequest::tableName() . '.status' => 'error'])
                ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_REJECTED])
                ->groupBy(['country_id',])
                ->asArray()
                ->all();

            $stopList = Country::getStopListCountryNotification();

            foreach ($dataDelivery as $dataItem) {
                try {
                    if (!in_array((int)$dataItem['country_id'], $stopList)) {
                        Yii::$app->notification->send(Notification::TRIGGER_REPORT_ORDER_TWO_DAYS_ERROR, [
                            'dateFrom' => date(Formatter::getDatetimeFormat(), $timeFrom),
                            'dateTo' => date(Formatter::getDatetimeFormat(), $timeTo),
                            'delivery_error' => $dataItem['delivery_error'],
                        ],
                            $dataItem['country_id']);
                    }
                } catch (\Exception $e) {
                    $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                }
            }

            $transaction->commit();

            $this->log('done.');
        } catch (\Exception $e) {
            $this->setInError();
            $this->log('Error: ' . $e->getMessage());
            $transaction->rollback();
        }
    }

    /**
     * Отправка оповещения о балансе товаров на складах
     */
    public function actionProductOnStorage()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->log('Begin ProductOnStorage...');

            $stopList = Country::getStopListCountryNotification();

            /* берем только те страны, которые есть у пользователей данного триггера */
            $userCountries = NotificationUser::find()
                ->joinWith(['userCountry'])
                ->select([
                    'country_id' => UserCountry::tableName() . '.country_id',
                    'user_id' => UserCountry::tableName() . '.user_id'
                ])
                ->where(['trigger' => Notification::TRIGGER_PRODUCT_ON_STORAGE])
                ->groupBy(['country_id'])
                ->asArray()
                ->all();
            $userCountriesList = ArrayHelper::getColumn($userCountries, 'country_id');

            $products = StorageProduct::find()
                ->joinWith([
                    'storage',
                    'product',
                ])
                ->select([
                    'sum_product' => 'SUM(' . StorageProduct::tableName() . '.balance)',
                    'country_id' => Storage::tableName() . '.country_id',
                    'product_id' => Product::tableName() . '.id',
                    'product_name' => Product::tableName() . '.name',
                    'storage_id' => Storage::tableName() . '.id',
                ])
                ->where([Storage::tableName() . '.country_id' => $userCountriesList])
                ->andWhere(['not in', Storage::tableName() . '.country_id', $stopList])
                ->andWhere([StorageProduct::tableName() . '.unlimit' => 0])
                ->groupBy([
                    'country_id',
                    'product_id',
                ])
                ->having(['<=', 'sum_product', self::LIMIT_STORAGE_PRODUCT])
                ->asArray()
                ->all();

            $countryId = null;
            $str = 'Products on storages: ';
            foreach ($products as $item) {
                if ($countryId != null && $countryId != $item['country_id']) {
                    try {
                        $str = rtrim($str, ', ');
                        Yii::$app->notification->send(Notification::TRIGGER_PRODUCT_ON_STORAGE, [
                            'text' => $str,
                        ], $countryId);
                    } catch (\Exception $e) {
                        $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                    }
                    $str = 'Products on storages: ';
                }
                $str .= $item['product_name'] . ' - ' . $item['sum_product'] . ', ';
                $countryId = $item['country_id'];
            }

            if (count($products) > 0) {
                try {
                    $str = mb_substr($str, 0, mb_strlen($str) - 9);
                    Yii::$app->notification->send(Notification::TRIGGER_PRODUCT_ON_STORAGE, [
                        'text' => $str,
                    ],
                        $countryId);
                } catch (\Exception $e) {
                    $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                }
            }

            $transaction->commit();

            $this->log('done.');
        } catch (\Exception $e) {
            $this->setInError();
            $this->log('Error: ' . $e->getMessage());
            $transaction->rollback();
        }
    }

    /**
     * Отправка оповещения о количестве заказов, для которых нет сейчас товаров
     */
    public function actionNoProductsForOrder()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->log('Begin NoProductsForOrder...');

            $subQuery = (new Query())->select(StorageProduct::tableName() . '.id')
                ->from([StorageProduct::tableName(), Storage::tableName()])
                ->where([StorageProduct::tableName() . '.storage_id' => Storage::tableName() . '.id'])
                ->andWhere([Storage::tableName() . '.country_id' => Order::tableName() . '.country_id'])
                ->andWhere([StorageProduct::tableName() . '.product_id' => OrderProduct::tableName() . '.product_id'])
                ->andWhere(['>=', StorageProduct::tableName() . '.balance', OrderProduct::tableName() . '.quantity'])
                ->andWhere(['!=', StorageProduct::tableName() . '.unlimit', 1])
                ->limit(1);
            $products = Order::find()
                ->innerJoinWith([
                    'orderProducts',
                    'orderProducts.product',
                ])
                ->select([
                    'country_id' => Order::tableName() . '.country_id',
                    'product_id' => OrderProduct::tableName() . '.product_id',
                    'product_name' => Product::tableName() . '.name',
                    'product_quantity' => OrderProduct::tableName() . '.quantity',
                    'order_id' => OrderProduct::tableName() . '.order_id',
                    'id' => OrderProduct::tableName() . '.id',
                ])
                ->where([
                    'in',
                    Order::tableName() . '.status_id',
                    [OrderStatus::STATUS_SOURCE_LONG_FORM, OrderStatus::STATUS_SOURCE_SHORT_FORM],
                ])
                ->andWhere(['not exists', $subQuery])
                ->orderBy('country_id, product_id')
                ->asArray()
                ->all();

            $stopList = Country::getStopListCountryNotification();

            $orderIds = null;
            $countryId = null;
            $productId = null;
            $productCount = 0;
            $productName = '';
            $notificationForCountry = '';
            foreach ($products as $item) {
                if (($productId != null && $productId != $item['product_id']) || ($countryId != null && $countryId != $item['country_id'])) {
                    $notificationForCountry .= "$productName: $productCount, ";
                    $productCount = 0;
                }
                if ($countryId != null && $countryId != $item['country_id']) {
                    $notificationForCountry = rtrim($notificationForCountry, ", ");
                    $orderCount = Order::find()
                        ->select(['count_order' => 'COUNT(1)'])
                        ->where(['IN', 'id', $orderIds])
                        ->asArray()
                        ->one();

                    try {
                        if (!in_array($countryId, $stopList)) {
                            Yii::$app->notification->send(Notification::TRIGGER_NO_PRODUCTS_FOR_ORDER, [
                                'text' => Yii::t('common', 'Нет товаров на складах для {count} заказов(а)',
                                        ['count' => $orderCount['count_order']]) . ': ' . $notificationForCountry,
                            ],
                                $countryId);
                        }
                    } catch (\Exception $e) {
                        $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                        $logAnswer = new CrontabTaskLogAnswer();
                        $logAnswer->log_id = $this->log->id;
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                        $logAnswer->answer = Yii::t('common', 'Ошибка при добавлении оповещения: ') . $e->getMessage();
                        $logAnswer->save();
                    }
                    $notificationForCountry = '';
                    $orderIds = null;
                    $productCount = 0;
                }
                $productCount += $item['product_quantity'];
                $productName = $item['product_name'];
                $countryId = $item['country_id'];
                $productId = $item['product_id'];
                $orderIds [] = $item['order_id'];
            }

            $notificationForCountry = rtrim($notificationForCountry, ", ");
            $orderCount = Order::find()
                ->select(['count_order' => 'COUNT(1)'])
                ->where(['IN', 'id', $orderIds])
                ->asArray()
                ->one();
            try {
                if (!in_array($countryId, $stopList)) {
                    Yii::$app->notification->send(Notification::TRIGGER_NO_PRODUCTS_FOR_ORDER, [
                        'text' => Yii::t('common', 'Нет товаров на складах для {count} заказов(а)',
                                ['count' => $orderCount['count_order']]) . ': ' . $notificationForCountry,
                    ],
                        $countryId);
                }
            } catch (\Exception $e) {
                $this->log('Ошибка при добавлении оповещения: ' . $e->getMessage());
                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $this->log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $logAnswer->answer = Yii::t('common', 'Ошибка при добавлении оповещения: ') . $e->getMessage();
                $logAnswer->save();
            }

            $transaction->commit();

            $this->log('done.');
        } catch (\Exception $e) {
            $this->setInError();
            $this->log('Error: ' . $e->getMessage());
            $transaction->rollback();
        }
    }

    /**
     * Сверка статусов и количества заказов в AdCombo и у нас
     *
     * @param $type
     * @param $checkPrevious
     *
     * @throws \Exception
     */
    public function actionCompareWithAdcombo($type = ReportAdcombo::TYPE_DAILY, $checkPrevious = true)
    {
        $this->log('Начинаем сверку с АдКомбо...', false);
        if ($checkPrevious == 'false') {
            $checkPrevious = false;
        }
        $toTime = strtotime(date("d.m.Y 00:00"));

        switch ($type) {
            case ReportAdcombo::TYPE_WEEKLY:
                $strPeriod = "-7 days";
                break;
            case ReportAdcombo::TYPE_MONTHLY:
                $strPeriod = "-1 month";
                break;
            case ReportAdcombo::TYPE_HOURLY:
                $strPeriod = "-1 hour";
                $toTime = strtotime(date("d.m.Y H:00"));
                break;
            default:
                $type = ReportAdcombo::TYPE_DAILY;
                $strPeriod = "-1 day";
        }

        $countries = Country::find()->active()->all();

        $stopList = Country::getStopListCountryNotification();

        foreach ($countries as $country) {
            $lastRecord = ReportAdcombo::find()->where([
                'type' => $type,
                'country_id' => $country->id,
            ])->orderBy('to desc')->one();
            if ($lastRecord) {
                $fromTime = $lastRecord->to;
            } else {
                $fromTime = strtotime($strPeriod, $toTime);
            }

            if ($checkPrevious) {
                $query = ReportAdcombo::find()->where(['country_id' => $country->id, 'type' => $type]);
                $query->andWhere([
                    'or',
                    '`hold_count` != `our_hold_count`',
                    '`confirmed_count` != `our_confirmed_count`',
                    '`trash_count` != `our_trash_count`',
                    '`cancelled_count` != `our_cancelled_count`',
                    '`difference_count` > 0',
                    '`hold_count` > 0',
                ]);
                $records = $query->all();
                if (!is_array($records)) {
                    $records = [];
                }
            } else {
                $records = [];
            }

            if ($toTime - $fromTime > 0) {
                $model = new ReportAdcombo();

                $model->country_id = $country->id;
                $model->type = $type;
                $model->from = $fromTime;
                $model->to = $toTime;
                $records[] = $model;
            }

            foreach ($records as $record) {
                try {
                    ReportAdcomboComparator::compareWithAdcombo($record);
                } catch (\Exception $e) {
                    $this->log("Возникла ошибка: '" . $e->getMessage() . "' при сравнении заказов с АдКомбо для страны: {$country->name}");
                    $this->setInError();
                    throw $e;
                }

                $hasErrors = false;

                foreach ($record->differenceArray as $diff) {
                    if ($diff > 0) {
                        $hasErrors = true;
                        break;
                    }
                }

                if ($type != ReportAdcombo::TYPE_HOURLY) {
                    if ($record->isNewRecord) {
                        if (!in_array($country->id, $stopList)) {
                            Yii::$app->notification->send(Notification::TRIGGER_REPORT_ADCOMBO_DAILY_INFO, [
                                'dateFrom' => Yii::$app->formatter->asDatetime($record->from,
                                    Formatter::getDatetimeFormat()),
                                'dateTo' => Yii::$app->formatter->asDatetime($record->to, Formatter::getDatetimeFormat()),
                                'holdCount' => $record->hold_count,
                                'confirmedCount' => $record->confirmed_count,
                                'trashCount' => $record->trash_count,
                                'cancelledCount' => $record->cancelled_count,
                                'totalCount' => $record->adcombo_total_count,
                                'ourTotalCount' => $record->our_total_count,
                                'ourHoldCount' => $record->our_hold_count,
                                'ourConfirmedCount' => $record->our_confirmed_count,
                                'ourTrashCount' => $record->our_trash_count,
                                'ourCancelledCount' => $record->our_cancelled_count,
                                'ccCount' => $record->cc_count,
                            ], $country->id);
                        }
                    }

                    if ($hasErrors) {
                        if (!in_array($country->id, $stopList)) {
                            Yii::$app->notification->send(Notification::TRIGGER_REPORT_ADCOMBO_DAILY_DANGER, [
                                'dateFrom' => Yii::$app->formatter->asDatetime($record->from,
                                    Formatter::getDatetimeFormat()),
                                'dateTo' => Yii::$app->formatter->asDatetime($record->to, Formatter::getDatetimeFormat()),
                                'statusCount' => $record->difference_count,
                                'adcomboCount' => $record->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO],
                                'ourCount' => $record->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR],
                                'ccCount' => $record->our_total_count - $record->cc_count,
                            ], $country->id);
                        }
                    }
                } else {
                    $totalAdComboCount = $record->adcombo_total_count;
                    $totalOrderCount = $record->our_total_count;
                    $totalCallCount = $record->cc_count;
                    if ($record->isNewRecord) {
                        if (!in_array($country->id, $stopList)) {
                            Yii::$app->notification->send(Notification::TRIGGER_REPORT_ADCOMBO_HOURLY_INFO, [
                                'dateFrom' => Yii::$app->formatter->asDatetime($fromTime, Formatter::getDatetimeFormat()),
                                'dateTo' => Yii::$app->formatter->asDatetime($toTime, Formatter::getDatetimeFormat()),
                                'totalAdComboCount' => $totalAdComboCount,
                                'totalOrderCount' => $totalOrderCount,
                                'totalCallCount' => $totalCallCount,
                            ], $country->id);
                        }

                        if ($hasErrors) {
                            if (!in_array($country->id, $stopList)) {
                                Yii::$app->notification->send(Notification::TRIGGER_REPORT_ADCOMBO_HOURLY_DANGER, [
                                    'dateFrom' => Yii::$app->formatter->asDatetime($fromTime,
                                        Formatter::getDatetimeFormat()),
                                    'dateTo' => Yii::$app->formatter->asDatetime($toTime, Formatter::getDatetimeFormat()),
                                    'totalAdComboCount' => $totalAdComboCount,
                                    'totalOrderCount' => $totalOrderCount,
                                    'totalCallCount' => $totalCallCount,
                                    'differenceOrder' => $record->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR] + $record->difference_count,
                                    'differenceCallCenter' => $totalOrderCount - $totalCallCount,
                                ], $country->id);
                            }
                        }
                    }
                }

                if (!$record->save()) {
                    $this->log("Возникла ошибка при сохранении лога в базу данных. Ошибки: " . implode(', ',
                            $record->errors));
                    $this->setInError();
                    throw new Exception('Возникла ошибка при сохранении лога в базу данных.');
                }
            }
        }

        $this->log('Финиш.');
    }


    /**
     *  Сверка с КЦ
     *
     * @param string $type
     * @param bool $checkPrevious
     *
     * @throws Exception
     * @throws \Exception
     */
    public function actionCompareWithCallCenter($type = ReportCallCenter::TYPE_DAILY, $checkPrevious = true)
    {
        $this->log('Начинаем сверку с КЦ');

        if ($checkPrevious == 'false') {
            $checkPrevious = false;
        }

        $toTime = strtotime(date("d.m.Y 00:00"));

        switch ($type) {
            case ReportAdcombo::TYPE_WEEKLY:
                $strPeriod = "-7 days";
                break;
            case ReportAdcombo::TYPE_MONTHLY:
                $strPeriod = "-1 month";
                break;
            case ReportAdcombo::TYPE_HOURLY:
                $strPeriod = "-1 hour";
                $toTime = strtotime(date("d.m.Y H:00"));
                break;
            default:
                $type = ReportAdcombo::TYPE_DAILY;
                $strPeriod = "-1 day";
        }

        $countries = Country::getCountriesWithActiveCallCenter();
        $stopList = Country::getStopListCountryNotification();

        foreach ($countries as $country) {
            $this->log("Начинаем сверять {$country->name}");
            $lastRecord = ReportCallCenter::find()->where([
                'type' => $type,
                'country_id' => $country->id,
            ])->orderBy(['to' => SORT_DESC])->one();
            if ($lastRecord) {
                $fromTime = $lastRecord->to;
            } else {
                $fromTime = strtotime($strPeriod, $toTime);
            }

            if ($checkPrevious) {

                $query = ReportCallCenter::find()->where([
                    'country_id' => $country->id,
                    'type' => $type,
                ]);
                $query->andWhere([
                    'or',
                    '`cc_post_call_count` != `our_post_call_count`',
                    '`cc_approved_count` != `our_approved_count`',
                    '`cc_fail_call_count` != `our_fail_call_count`',
                    '`cc_recall_count` != `our_recall_count`',
                    '`cc_rejected_count` != `our_rejected_count`',
                    '`cc_double_count` != `our_double_count`',
                    '`cc_trash_count` != `our_trash_count`',
                    '`difference_count` > 0',
                    '`cc_post_call_count` > 0',
                    '`cc_fail_call_count` > 0',
                    '`cc_recall_count` > 0',
                ]);
                $query->orderBy(['to' => SORT_DESC]);
                $records = $query->all();
                if (!is_array($records)) {
                    $records = [];
                }
            } else {
                $records = [];
            }

            if ($toTime - $fromTime > 0) {
                $model = new ReportCallCenter();

                $model->country_id = $country->id;
                $model->type = $type;
                $model->from = $fromTime;
                $model->to = $toTime;
                $records[] = $model;
            }

            foreach ($records as $record) {
                try {
                    ReportCallCenterComparator::compareWithCallCenters($record);
                    if (!in_array($country->id, $stopList)) {
                        Yii::$app->notification->send(Notification::TRIGGER_REPORT_CALL_CENTER_COMPARE_INFO, [
                            'from' => Yii::$app->formatter->asDatetime($record->from,
                                Formatter::getDatetimeFormat()),
                            'to' => Yii::$app->formatter->asDatetime($record->to, Formatter::getDatetimeFormat()),
                            'ccCount' => $record->ccTotalCount,
                            'ourCount' => $record->ourTotalCount,
                            'ccP' => $record->cc_post_call_count,
                            'ourP' => $record->our_post_call_count,
                            'ccApprove' => $record->cc_approved_count,
                            'ourApprove' => $record->our_approved_count,
                            'ccFail' => $record->cc_fail_call_count,
                            'ourFail' => $record->our_fail_call_count,
                            'ccRecall' => $record->cc_recall_count,
                            'ourRecall' => $record->our_recall_count,
                            'ccCancel' => $record->cc_rejected_count,
                            'ourCancel' => $record->our_rejected_count,
                            'ccDouble' => $record->cc_double_count,
                            'ourDouble' => $record->our_double_count,
                            'ccT' => $record->cc_trash_count,
                            'ourT' => $record->our_trash_count,
                        ], $country->id);
                    }

                    $flag = false;

                    foreach ($record->differenceArray as $diff) {
                        if ($diff > 0) {
                            $flag = true;
                            break;
                        }
                    }

                    if ($flag) {
                        if (!in_array($country->id, $stopList)) {
                            Yii::$app->notification->send(Notification::TRIGGER_REPORT_CALL_CENTER_COMPARE_DANGER, [
                                'from' => Yii::$app->formatter->asDatetime($record->from,
                                    Formatter::getDatetimeFormat()),
                                'to' => Yii::$app->formatter->asDatetime($record->to, Formatter::getDatetimeFormat()),
                                'ccCount' => $record->ccTotalCount,
                                'ourCount' => $record->ourTotalCount,
                                'differenceCount' => $record->difference_count,
                            ], $country->id);
                        }
                    }
                } catch (BadRequestHttpException $e) {
                    $this->log("Возникла ошибка при запросе заказов из КЦ: {$e->getMessage()}");
                } catch (\Exception $e) {
                    $this->log("Возникла ошибка при запросе заказов из КЦ: {$e->getMessage()}");
                    $this->setInError();
                    throw $e;
                }

                if (!$record->save()) {
                    $this->log("Возникла ошибка при сохранении лога в базу данных. Ошибки: " . implode(', ',
                            $record->errors));
                    $this->setInError();
                    throw new Exception('Возникла ошибка при сохранении лога в базу данных.');
                }
            }
            $this->log("Сверили {$country->name}");
        }
    }

    /**
     *  Подсчет дебиторской задолжности по месяцам
     *
     * @param integer $country_id
     * @param integer $delivery_id
     */
    public function actionCalculateDeliveryDebts($country_id = null, $delivery_id = null)
    {
        echo "RUN".PHP_EOL;
        try {
            ReportFormDeliveryDebts::prepareData($delivery_id, $country_id);
        } catch (\Throwable $e) {
            echo "CATCH ERROR".PHP_EOL;
            print_r($e->getMessage());
            $this->log($e->getMessage());
            $logAnswer = new CrontabTaskLogAnswer([
                'log_id' => $this->log->id,
                'data' => json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE)
            ]);
            $logAnswer->save();
        }
    }

    /**
     *  Подсчет дебиторской задолжности по месяцам (Старая)
     *
     * @param integer $country_id
     * @param integer $delivery_id
     */
    public function actionCalculateDeliveryDebtsOld($country_id = null, $delivery_id = null)
    {
        try {
            ReportFormDeliveryDebtsOld::prepareData($delivery_id, $country_id);
        } catch (\Throwable $e) {
            $this->log($e->getMessage());
            $logAnswer = new CrontabTaskLogAnswer([
                'log_id' => $this->log->id,
                'data' => json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE)
            ]);
            $logAnswer->save();
        }
    }


    /**
     * Рассылка (ежедневных) нотификаций о неотправленных заказах (которые на момент отправки находятся в колонках 1, 2, 6, 19, 31).
     */
    public function actionOrdersUnsent()
    {
        $data = Order::find()
            ->select([
                'count' => 'count(' . Order::tableName() . '.id)',
                'country_id' => Order::tableName() . '.country_id',
            ])
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->where([
                Order::tableName() . '.status_id' => [
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_DELIVERY_REJECTED,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ],
            ])
            ->andWhere([Country::tableName() . '.active' => 1, Country::tableName() . '.is_partner' => 1])
            ->groupBy(Order::tableName() . '.country_id')
            ->asArray()
            ->all();

        $stopList = Country::getStopListCountryNotification();

        foreach ($data as $datum) {
            if (!in_array((int)$datum['country_id'], $stopList)) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_ORDERS_UNSENT,
                    [
                        'count' => $datum['count'],
                    ],
                    (int)$datum['country_id']
                );
            }
        }
    }

    /**
     * Оповещения по скоплению заказов в статусах 12,9,37,38 с группировкой по стране и КС
     */
    public function actionOrdersInStatuses12_9_37_38()
    {
        $interval = 12 * 3600;

        $query = new Query();
        $query->from(Order::tableName())
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Delivery::tableName(), Delivery::tableName() . '.id=' . DeliveryRequest::tableName() . '.delivery_id')
            ->where([
                Order::tableName() . '.status_id' => [
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SENT,
                    OrderStatus::STATUS_LOG_RECEIVED
                ]
            ])
            ->andWhere(['is not', DeliveryRequest::tableName() . '.accepted_at', null])
            ->andWhere(['<=', DeliveryRequest::tableName() . '.accepted_at', time() - $interval])
            ->andWhere(['not in', Order::tableName() . '.country_id', Country::getStopListCountryNotification()])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Delivery::tableName() . '.active' => 1])
            ->orderBy(['total' => SORT_DESC]);

        $query->addSelect([
            'total' => new Expression('count(' . Order::tableName() . '.id)'),
            'in12hour' => new Expression("count(UNIX_TIMESTAMP() - accepted_at <= {$interval} OR NULL)"),
            'country' => Country::tableName() . '.name',
            'delivery' => Delivery::tableName() . '.name',
        ]);

        $query->addGroupBy([
            Order::tableName() . '.country_id',
            DeliveryRequest::tableName() . '.delivery_id'
        ]);

        $data = $query->all();

        if (count($data) > 0) {
            $info = PHP_EOL . '';
            $total_all = 0;
            $count_in12hours = 0;

            foreach ($data as $line) {
                $total_all += $line['total'];
                $count_in12hours += $line['in12hour'];

                $out12hour = $line['total'] - $line['in12hour'];

                $info .= $line['delivery'] . ' (' . yii::t('common', $line['country'], 'En-us') . ') : ' . $line['in12hour'] . ' / ' . $out12hour . '<br/>' . PHP_EOL;
            }

            Yii::$app->notification->send(
                Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38,
                [
                    'date' => yii::$app->formatter->asDate(time(), 'php:d.m.Y'),
                    'total' => $total_all,
                    'in12hour' => $count_in12hours,
                    'info' => '<br/>' . PHP_EOL . $info
                ]
            );
        }
    }

    /**
     * Скопление в колонках 1 и 2 в сумме по всем странам более 500 заказов
     *
     * @param $border
     * @param integer $timeoffset в секундах
     * @param integer $border2
     */
    public function actionOrdersInStatus12($border = 1000, $timeoffset = 300, $border2 = 10)
    {
        $count = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->where([
                Order::tableName() . '.status_id' => [
                    OrderStatus::STATUS_SOURCE_LONG_FORM,
                    OrderStatus::STATUS_SOURCE_SHORT_FORM,
                ],
            ])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(["!=", Country::tableName() . '.is_stop_list', 1])
            ->count();

        if ($count > $border) {
            Yii::$app->notification->send(
                Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2,
                [
                    'count' => $count,
                ]
            );
        }

        $stopList = Country::getStopListCountryNotification();

        // Скопление заказов в 1 и 2 колонках отдельно по странам
        $query = new Query();
        $query->from(Order::tableName());
        $query->select([
            'country_id' => Order::tableName() . '.country_id',
            'cnt' => "COUNT(" . Order::tableName() . '.id)',
        ]);
        $query->groupBy([Order::tableName() . '.country_id']);

        $query->leftJoin(Country::tableName(), Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
        $query->addSelect([
            'country_name' => Country::tableName() . '.name',
        ]);
        $query->where(['<', Order::tableName() . '.created_at', time() - $timeoffset]);
        $query->andWhere([
            Order::tableName() . '.status_id' => [
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM,
            ],
        ]);
        $totalCounts = $query->all();

        foreach ($totalCounts as $totalCount) {
            if ($totalCount['cnt'] > $border2) {
                if (!in_array((int)$totalCount['country_id'], $stopList)) {
                    Yii::$app->notification->send(
                        Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY,
                        [
                            'count' => $totalCount['cnt'],
                            'country' => $totalCount['country_name'],
                        ],
                        $totalCount['country_id']
                    );
                }
            }
        }
    }

    /**
     * Процент необзвоненных новых лидов за прошедшие сутки по оффисам
     * @return bool
     */
    public function actionHoldLeadsPercent()
    {
        if (!$offices = HoldLead::getOfficesByEndOfWorkDay()) {
            return false;
        }

        foreach ($offices as $office) {
            $buyOutData = HoldLead::getBuyoutData($office);
            if ($buyOutData['holdPerCent'] > 50) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_CALL_CENTER_HOLD_LEADS,
                    [
                        'officeName' => $office->name,
                        'hold' => implode(', ', $buyOutData['hold']),
                        'all' => $buyOutData['all'],
                    ]
                );
            }
        }

        return false;
    }


    /**
     * Проверить наличие апрувов за последний час в работающих КЦ
     *
     * @param integer $minApprove , если 0 то нет апрувов вообще
     *
     * @return bool
     * @throws Exception
     */
    public function actionCheckApproveLastHour($minApprove = 0)
    {
        $offices = Office::getListWorkingOffices();
        foreach ($offices as $office) {

            $officeCallCenters = ArrayHelper::getColumn($office->callCenters, 'id');
            if (!$officeCallCenters) {
                continue;
            }

            $dateTime = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
            $moscowDateStart = $dateTime->getTimestamp();

            $query = Order::find()
                ->joinWith(['callCenterRequest'])
                ->where(['>', Order::tableName() . '.created_at', $moscowDateStart])
                ->andWhere(['>', CallCenterRequest::tableName() . '.cc_update_response_at', strtotime('-1hour')])
                ->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $officeCallCenters])
                ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getApproveList()]);

            $approves = $query->count();

            if ($approves <= $minApprove) {

                $query = Order::find()
                    ->joinWith(['callCenterRequest'])
                    ->where(['>', Order::tableName() . '.created_at', $moscowDateStart])
                    ->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $officeCallCenters])
                    ->andWhere(['is not', Order::tableName() . '.source_id', null]);

                $leads = $query->count();

                if ($approves != 0 || $leads != 0) {
                    $workTimeFrom = $office->workTime['from'] ?? 0;
                    $workTimeTo = $office->workTime['to'] ?? 0;

                    Yii::$app->notification->send(
                        Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR,
                        [
                            'officeName' => $office->name,
                            'workTime' => Yii::$app->formatter->asTime($workTimeFrom, 'php:H:i') . '-' . Yii::$app->formatter->asTime($workTimeTo, 'php:H:i') . ' GMT+0',
                            'countApprove' => $approves,
                            'countLead' => $leads,
                        ]
                    );
                }
            }
        }

        return false;
    }

    /**
     * @param null|string $testMode
     *
     * @throws Exception
     */
    public function actionCheckDayApprove($testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $offices = Office::getListWorkingOffices();

        foreach ($offices as $office) {

            if (!$office->callCenters) {
                continue;
            }

            $workTimeFrom = $office->workTime['from'] ?? 0;
            $workTimeTo = $office->workTime['to'] ?? 0;
            $textNotification = CheckDayApprove::buildMessageForOffice($office, $testMode);

            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_CHECK_DAY_APPROVE,
                [
                    'officeName' => $office->name,
                    'workTime' => Yii::$app->formatter->asTime($workTimeFrom, 'php:H:i') . '-' . Yii::$app->formatter->asTime($workTimeTo, 'php:H:i') . ' GMT+0',
                    'text' => $textNotification,
                ]
            );
        }
    }

    /**
     * @param null|string $testMode
     *
     * @throws Exception
     */
    public function actionCallCenterDailyReport($testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $offices = Office::getListWorkingOffices();

        $officeData = CallCenterDailyReport::getData();

        foreach ($offices as $office) {
            $operatorsOnline = CallCenterDailyReport::getOperatorsOnline($office->id);

            $workTimeFrom = $office->workTime['from'] ?? 0;
            $workTimeTo = $office->workTime['to'] ?? 0;
            $textNotification = CallCenterDailyReport::prepareText($officeData, $office, $operatorsOnline, $testMode);

            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_DAILY_REPORT,
                [
                    'officeName' => $office->name,
                    'workTime' => Yii::$app->formatter->asTime($workTimeFrom, 'php:H:i') . '-' . Yii::$app->formatter->asTime($workTimeTo, 'php:H:i') . ' GMT+0',
                    'text' => $textNotification,
                ]
            );
        }
    }

    /**
     * @param null|string $testMode
     *
     * @throws Exception
     */
    public function actionCallCenterEfficiencyOperators($testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $offices = Office::getListWorkingOffices(30, 30);

        $currentTime = date('H:i', time() - strtotime('midnight'));
        foreach ($offices as $office) {
            $workTime = $office->getWorkTime();
            if ((isset($workTime['from']) && date('H:i', $workTime['from'] + 30 * 60) == $currentTime) || (isset($workTime['to']) && date('H:i', $workTime['to'] + 30 * 60) == $currentTime) || $testMode) {
                $textNotification = CallCenterEfficiencyOperators::prepareText($office, $testMode);

                Yii::$app->notification->send(
                    Notification::TRIGGER_CALL_CENTER_EFFICIENCY_OPERATORS,
                    [
                        'officeName' => $office->name,
                        'workTime' => $office->getWorkingHours(),
                        'text' => $textNotification,
                    ]
                );
            }
        }
    }


    /**
     * @param int $timeoffset / в минутах
     * @param int $percentage / в процентах
     */
    public function actionCheckApprovedOrders($timeoffset = 60, $percentage = 60)
    {
        $offices = Office::find()
            ->with(['callCenters.country', 'country.timezone'])
            ->active()
            ->andWhere(['type' => Office::TYPE_CALL_CENTER])
            ->all();
        $currentTime = time() - strtotime("midnight");
        $commonCount = 0;
        $problemCountries = [];
        foreach ($offices as $office) {
            $dateTime = new \DateTime('now', new \DateTimeZone($office->country->timezone->timezone_id));
            $currentDay = $dateTime->format('w');
            $workTime = 86400;
            if (!empty($office->workTime)) {
                if (isset($office->workTime['from']) && isset($office->workTime['to'])) {
                    if (!($currentTime >= ($office->workTime['from'] + $timeoffset * 60) && $currentTime <= $office->workTime['to']) && !($office->workTime['to'] < $office->workTime['from'] && $currentTime < $office->workTime['to'])) {
                        continue;
                    }
                    if ($office->workTime['to'] < $office->workTime['from']) {
                        $workTime = (86400 - $office->workTime['from']) + $office->workTime['to'];
                    } else {
                        $workTime = $office->workTime['to'] - $office->workTime['from'];
                    }
                }

                if (isset($office->workTime['days'])) {
                    if (!in_array($currentDay, $office->workTime['days'])) {
                        continue;
                    }
                }
            }
            $workTime /= 60;
            if ($workTime < $timeoffset) {
                $timeoffset = $workTime;
            }
            if (Holiday::find()
                ->where(['date' => $dateTime->format('Y-m-d'), 'country_id' => $office->country_id])
                ->exists()
            ) {
                continue;
            }

            $query = CallCenterRequest::find()->joinWith(['order.country', 'callCenter'])
                ->where([CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_DONE])
                ->andWhere([Country::tableName() . '.active' => 1, CallCenter::tableName() . '.active' => 1])
                ->andWhere(["!=", Country::tableName() . '.is_stop_list', 1])
                ->andWhere(['>', CallCenterRequest::tableName() . '.approved_at', 0])
                ->andWhere([CallCenterRequest::tableName() . '.call_center_id' => ArrayHelper::getColumn($office->callCenters, 'id')])
                ->andWhere([
                    'not in',
                    Order::tableName() . '.status_id',
                    [
                        OrderStatus::STATUS_CC_REJECTED,
                        OrderStatus::STATUS_SOURCE_LONG_FORM,
                        OrderStatus::STATUS_SOURCE_SHORT_FORM,
                        OrderStatus::STATUS_CC_POST_CALL,
                        OrderStatus::STATUS_CC_RECALL,
                        OrderStatus::STATUS_CC_FAIL_CALL,
                        OrderStatus::STATUS_CC_TRASH,
                        OrderStatus::STATUS_AUTOTRASH,
                        OrderStatus::STATUS_AUTOTRASH_DOUBLE,
                        OrderStatus::STATUS_CC_DOUBLE,
                    ],
                ]);

            $averageCount = $query->andWhere([
                    '>',
                    CallCenterRequest::tableName() . '.cc_update_response_at',
                    strtotime('-30days'),
                ])->count() / (($workTime / $timeoffset) * 30);
            $hourCount = $query->andWhere([
                '>',
                CallCenterRequest::tableName() . '.cc_update_response_at',
                strtotime('-' . (empty($timeoffset) ? 60 : $timeoffset . 'minutes')),
            ])->count();

            if ($hourCount < round($averageCount * ($percentage / 100))) {
                $commonCount += $hourCount;
                foreach ($office->callCenters as $callCenter) {
                    $problemCountries[$callCenter->country_id] = $callCenter->country->name;
                }
            }
        }

        if (!empty($problemCountries)) {
            Yii::$app->notification->send(
                Notification::TRIGGER_CALL_CENTER_SMALL_APPROVE,
                [
                    'count' => $commonCount,
                    'countries' => implode(', ', $problemCountries),
                    'minutes' => $timeoffset,
                ]
            );
        }
    }

    /**
     * @param int $timeout / в минутах
     */
    public function actionCheckAdcomboGetStatuses($timeout = 30)
    {
        $query = CallCenterRequest::find()->joinWith(['order.country', 'callCenter'])
            ->select([
                'country' => Country::tableName() . '.name',
                'count' => 'SUM(' . Order::tableName() . '.id)',
            ])
            ->where([CallCenterRequest::tableName() . '.status' => CallCenterRequest::STATUS_DONE])
            ->andWhere(['>', CallCenterRequest::tableName() . '.approved_at', 0])
            ->andWhere([Order::tableName() . '.source_confirmed' => 0])
            ->andWhere([Country::tableName() . '.active' => 1, CallCenter::tableName() . '.active' => 1])
            ->andWhere(["!=", Country::tableName() . '.is_stop_list', 1])
            ->andWhere([
                '<',
                CallCenterRequest::tableName() . '.cc_update_response_at',
                strtotime('-' . empty($timeout) ? 30 : $timeout . 'minutes'),
            ])
            ->andWhere([
                'not in',
                Order::tableName() . '.status_id',
                [
                    OrderStatus::STATUS_CC_REJECTED,
                    OrderStatus::STATUS_SOURCE_LONG_FORM,
                    OrderStatus::STATUS_SOURCE_SHORT_FORM,
                    OrderStatus::STATUS_CC_POST_CALL,
                    OrderStatus::STATUS_CC_RECALL,
                    OrderStatus::STATUS_CC_FAIL_CALL,
                    OrderStatus::STATUS_CC_TRASH,
                    OrderStatus::STATUS_AUTOTRASH,
                    OrderStatus::STATUS_AUTOTRASH_DOUBLE,
                    OrderStatus::STATUS_CC_DOUBLE,
                ],
            ])
            ->groupBy([Order::tableName() . '.country_id']);
        $records = $query->createCommand()->queryAll();
        $count = array_sum(ArrayHelper::getColumn($records, 'count'));
        if ($count > 0) {
            Yii::$app->notification->send(
                Notification::TRIGGER_ADCOMBO_DONT_GET_STATUSES,
                [
                    'count' => $count,
                    'minutes' => $timeout,
                    'countries' => implode(', ', ArrayHelper::getColumn($records, 'country')),
                ]
            );
        }
    }

    /**
     * Скопление более 500 заказов в 31й колонке по всем странам
     *
     * @param int $border
     */
    public function actionOrdersInStatus31($border = 1000)
    {
        $count = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->where([
                Order::tableName() . '.status_id' => [
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ],
            ])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(["!=", Country::tableName() . '.is_stop_list', 1])
            ->count();

        if ($count > $border) {
            Yii::$app->notification->send(
                Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_31,
                [
                    'count' => $count,
                ]
            );
        }
    }

    /**
     * Скопление в статусах 1,2,6,9,12,14,15,16,19,27,31,32
     * дольше нормы
     */
    public function actionOrdersFrozen()
    {
        /**
         * Отправляем уведомления один раз в 2 часа
         */
        $notifications = UserNotification::find()
            ->select(['params'])
            ->where(['trigger' => Notification::TRIGGER_REPORT_ORDERS_FROZEN])
            ->andWhere([">", 'created_at', strtotime('- 2 hours')])
            //->andWhere(["<", 'created_at', strtotime('- 2 hours')])
            ->asArray()
            ->all();

        if ($notifications) {
            return;
        }

        $stopList = Country::getStopListCountryNotification();

        foreach (self::FREEZE_NORMS_FOR_STATUSES as $timeNorm => $statuses) {
            $frozenOrders = $this->getFrozenOrders($statuses, $timeNorm);
            if ($frozenOrders) {
                foreach ($frozenOrders as $frozenOrdersByCountry) {
                    if (!in_array((int)$frozenOrdersByCountry['country_id'], $stopList)) {
                        Yii::$app->notification->send(
                            Notification::TRIGGER_REPORT_ORDERS_FROZEN,
                            [
                                'amount' => $frozenOrdersByCountry['count'],
                                'country_name' => $frozenOrdersByCountry['country_name'],
                                'status_id' => $frozenOrdersByCountry['status_id'],
                                'time_norm' => $timeNorm,
                            ],
                            $frozenOrdersByCountry['country_id']
                        );
                    }
                }
            }
        }
    }

    /**
     * Если заказы скопившиеся в статусах 1,2,6,9,12,14,15,16,19,27,31,32
     * в течении 2 часов не уменьшились
     */
    public function actionOrdersStillFrozen()
    {
        $notifications = UserNotification::find()
            ->select(['params', 'country_id'])
            ->where(['trigger' => Notification::TRIGGER_REPORT_ORDERS_FROZEN])
            ->andWhere([">", 'created_at', strtotime('- 3 hours')])
            ->andWhere(["<", 'created_at', strtotime('- 2 hours')])
            ->asArray()
            ->all();

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);

        $stopList = Country::getStopListCountryNotification();

        try {
            if ($notifications) {
                $this->log('Notifications received');
                foreach (self::FREEZE_NORMS_FOR_STATUSES as $timeNorm => $statuses) {
                    $frozenOrders = $this->getFrozenOrders($statuses, $timeNorm);
                    if ($frozenOrders) {
                        foreach ($notifications as $notification) {
                            $params = json_decode($notification['params'], true);
                            $countryId = $notification['country_id'];
                            foreach ($frozenOrders as $frozenOrdersByCountry) {
                                if ($frozenOrdersByCountry['country_id'] == $countryId
                                    && $frozenOrdersByCountry['status_id'] == $params['status_id']
                                ) {
                                    $this->log('Найдено соответствие статус: ' . $frozenOrdersByCountry['status_id'] . ' страна ' . $params['country_name']);
                                    $this->log('Колличество сейчас:' . $frozenOrdersByCountry['count']);
                                    $this->log('Колличество 2 часа назад:' . $params['count']);
                                    if ($params['amount'] <= $frozenOrdersByCountry['count']) {
                                        if (!in_array((int)$frozenOrdersByCountry['country_id'], $stopList)) {
                                            $this->log('Notification must be sent');
                                            Yii::$app->notification->send(
                                                Notification::TRIGGER_REPORT_ORDERS_STILL_FROZEN,
                                                [
                                                    'amount' => $frozenOrdersByCountry['count'],
                                                    'country_name' => $frozenOrdersByCountry['country_name'],
                                                    'status_id' => $frozenOrdersByCountry['status_id'],
                                                    'time_norm' => $timeNorm,
                                                ],
                                                $frozenOrdersByCountry['country_id']
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else $this->log('No notifications yet');

        } catch (\Exception $e) {
            $cronLog('Error: ' . $e->getMessage());
        }
    }

    /**
     * @param $statuses
     * @param $norm
     *
     * @return Order[]
     */
    private function getFrozenOrders($statuses, $norm)
    {
        return $frozenOrders = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'count' => 'count(' . Order::tableName() . '.id)',
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'status_id' => Order::tableName() . '.status_id',
            ])
            ->where([Order::tableName() . '.status_id' => $statuses])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(['<', Order::tableName() . '.updated_at', strtotime($norm)])
            ->groupBy(['country_id', 'status_id'])
            ->asArray()
            ->all();
    }

    /**
     * не получали больше часа статусов из КЦ по всем странам скопом
     */
    public function actionNoAnswerFromCc()
    {
        $max = CallCenterRequest::find()
            ->max(CallCenterRequest::tableName() . ".cc_update_response_at");

        if ($max) {
            $difference = time() - $max;
            if ($difference > self::ORDER_NEW_STATUS_STALE_HOUR) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_REPORT_NO_ANSWER_FROM_CC,
                    [
                        'time' => gmdate("H:i:s", $difference),
                    ]
                );
            }
        }
    }

    /**
     *  Если за последний час увеличилось количество заказов в автотреше больше, чем в 1.5 раза от среднего количества автотрешей в час за неделю
     */
    public function actionRapidIncreaseOrdersInAutotrash()
    {
        $lastHourStart = strtotime("-1 hour");
        $lastWeekStart = strtotime("-7 days", $lastHourStart);

        $middleValue = Order::find()
                ->where([
                    'status_id' => [
                        OrderStatus::STATUS_AUTOTRASH,
                        OrderStatus::STATUS_AUTOTRASH_DOUBLE,
                    ],
                ])
                ->andWhere(['>=', 'created_at', $lastWeekStart])
                ->andWhere(['<', 'created_at', $lastHourStart])
                ->count() / 24 * 7;
        $currentValue = Order::find()->where([
            'status_id' => [
                OrderStatus::STATUS_AUTOTRASH,
                OrderStatus::STATUS_AUTOTRASH_DOUBLE,
            ],
        ])->andWhere(['>=', 'created_at', $lastHourStart])->count();

        if ($currentValue >= $middleValue * 1.5) {
            Yii::$app->notification->send(
                Notification::TRIGGER_RAPID_INCREASE_ORDERS_IN_AUTOTRASH,
                [
                    'count' => $currentValue,
                    'normal' => $middleValue,
                ]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function actionGenerateInvoices()
    {
        $query = Invoice::find()->where(['status' => Invoice::STATUS_QUEUE]);

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);
        foreach ($query->each(1) as $invoice) {
            /**
             * @var Invoice $invoice
             */
            $invoice->status = Invoice::STATUS_GENERATING;
            $invoice->save(true, ['status']);
            try {
                $pdfGenerator = new InvoiceFinanceBuilder($invoice);
                $excelGenerator = new InvoiceTableBuilder($invoice);
                $invoice->invoice_filename = $pdfGenerator->generate();
                $invoice->orders_filename = $excelGenerator->generate();
                $invoice->status = Invoice::STATUS_GENERATED;
                if (!$invoice->save(true, ['invoice_filename', 'orders_filename', 'status'])) {
                    throw new \Exception($invoice->getFirstErrorAsString());
                };
            } catch (\Throwable $e) {
                $invoice->status = Invoice::STATUS_QUEUE;
                $invoice->save(true, ['status']);
                $this->log($e->getMessage());
                $cronLog($e->getMessage());
            }
        }
    }

    /**
     * @param null $countryId
     * @param null $testMode
     */
    public function actionAdcomboStatus($countryId = null, $testMode = null)
    {
        $testMode = $testMode ? 'test' : null;
        $dataArray = AdcomboStatus::getData($countryId);
        foreach ($dataArray['countries'] as $countryId => $country) {
            try {
                if (array_key_exists($dataArray['countries'][$countryId]['name'], Skype::ADCOMBO_CHAT_COUNTRIES)) {
                    AdcomboStatus::sendMessageByCountry($dataArray['data'][$countryId] ?? [], $country, $dataArray['asr'], $testMode);
                }
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }
        }
    }

    /**
     * Пересчет таблицы
     *
     * @param int|null $forTime timestamp
     */
    public function actionCalculateCompanyStandards(int $forTime = null): void
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);
        $round = 2; // округление
        $firstRecord = 1467331200; // время начала статистики
        try {
            if ($forTime && (!is_int($forTime) || $forTime < $firstRecord || $forTime > time())) {
                throw new \Exception(Yii::t('common', 'Не верная дата пересчета.'));
            }

            if (empty($forTime)) {
                $firstRun = (new Query())->from('report_company_standards')->exists();
                $startTime = (new Query())->createCommand()->setSql("select unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01')) start")->queryOne()['start'] ?? 0;

                $last_update_sms = $firstRun ? $firstRecord :
                    (Lead::find()->alias('lead')
                            ->innerJoin(OrderNotificationRequest::tableName() . ' onr', "onr.order_id = lead.order_id and COALESCE(onr.sms_date_sent, onr.sms_date_created, onr.created_at) >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->where(['onr.sms_status' => [OrderNotificationRequest::SMS_STATUS_DONE, OrderNotificationRequest::SMS_STATUS_IN_PROGRESS, OrderNotificationRequest::SMS_STATUS_PENDING]])
                            ->orderBy(['lead.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_delivery = $firstRun ? $firstRecord :
                    (CallCenterRequest::find()->alias('ccr')
                            ->innerJoin(DeliveryRequest::tableName() . ' dr', "ccr.order_id = dr.order_id and COALESCE(dr.sent_at, dr.created_at) >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->where(['is not', 'ccr.approved_at', null])
                            ->orderBy(['ccr.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_call = $firstRun ? $firstRecord :
                    (Lead::find()->alias('lead')
                            ->innerJoin(CallCenterRequest::tableName() . ' ccr', "ccr.order_id = lead.order_id and ccr.sent_at >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->where(['is not', 'ccr.sent_at', null])
                            ->orderBy(['lead.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_call_try = $firstRun ? $firstRecord :
                    (CallCenterRequest::find()->alias('ccr')
                            ->innerJoin(CallCenterRequestHistory::tableName() . ' ccrh', "ccr.id = ccrh.call_center_request_id and ccrh.called_at >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->where(['is not', 'ccr.sent_at', null])
                            ->orderBy(['ccr.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_approve = $firstRun ? $firstRecord :
                    (Lead::find()->alias('lead')
                            ->innerJoin(CallCenterRequest::tableName() . ' ccr', "ccr.order_id = lead.order_id and ccr.approved_at >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->orderBy(['lead.created_at' => SORT_ASC])
                            ->limit(1)
                            ->one()->created_at ?? $startTime);
                $last_update_avg_check = $firstRun ? $firstRecord :
                    (Lead::find()->alias('lead')
                            ->innerJoin(CallCenterRequest::tableName() . ' ccr', "ccr.order_id = lead.order_id and ccr.approved_at >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->orderBy(['lead.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_error_address = $firstRun ? $firstRecord :
                    (Lead::find()->alias('lead')
                            ->innerJoin(CallCenterCheckRequest::tableName() . ' cccr', "cccr.order_id = lead.order_id
                                and cccr.status = '" . CallCenterCheckRequest::TYPE_CHECK_ADDRESS . "'
                                and cccr.updated_at >= unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")
                            ->orderBy(['lead.created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_accepted = $firstRun ? $firstRecord :
                    (DeliveryRequest::find()
                            ->where(['and',
                                ['is not', 'sent_at', null],
                                ['is not', 'accepted_at', null],
                                ['>=', 'accepted_at', "unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))"]
                            ])
                            ->orderBy(['created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_payment = $firstRun ? $firstRecord :
                    (DeliveryRequest::find()
                            ->where(['and',
                                ['is not', 'paid_at', null],
                                ['is not', 'approved_at', null],
                                ['>=', 'paid_at', "unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))"]
                            ])
                            ->orderBy(['created_at' => SORT_ASC])
                            ->one()->created_at ?? $startTime);
                $last_update_costs = $firstRecord;
            } else {
                $forTime = strtotime(date('Y-m-01', $forTime));
                $last_update_sms = $last_update_delivery = $last_update_call = $last_update_call_try =
                $last_update_approve = $last_update_avg_check = $last_update_error_address = $last_update_accepted =
                $last_update_payment = $last_update_costs = $forTime;
            }

//                поступление лида
            $dataAdmissionLead = Lead::find()
                ->select([
                    'country_id' => Order::tableName() . '.country_id',
                    'month' => 'date_format(from_unixtime(' . Lead::tableName() . ".created_at), '%Y.%m')",
                    'admission_lead' => 'ROUND(sum(' . Lead::tableName() . '.created_at - ' . Lead::tableName() . ".ts_spawn)/count(*), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->innerJoinWith(['order'], false)
                ->where(['is not', Lead::tableName() . '.ts_spawn', null])
                ->andWhere(['>=', Lead::tableName() . '.created_at', $forTime ? $forTime : (!$firstRun ? (new Expression("unix_timestamp(DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y-%m-01'))")) : 0)])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                первая смс
            $dataFirstSms = Lead::find()->alias('lead')
                ->innerJoin(OrderNotificationRequest::tableName() . ' onr', 'onr.order_id = lead.order_id')
                ->innerJoin(Order::tableName() . ' order', 'order.id = lead.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(lead.created_at), '%Y.%m')",
                    'first_sms' => "ROUND(sum(COALESCE(onr.sms_date_sent, onr.sms_date_created, onr.created_at) - lead.created_at)/count(*), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['onr.sms_status' => [OrderNotificationRequest::SMS_STATUS_DONE, OrderNotificationRequest::SMS_STATUS_IN_PROGRESS, OrderNotificationRequest::SMS_STATUS_PENDING]])
                ->andWhere(['>=', 'lead.created_at', $last_update_sms])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                передача в КС
            $dataSendToDelivery = DeliveryRequest::find()->alias('dr')
                ->innerJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.order_id = dr.order_id')
                ->innerJoin(Order::tableName() . ' order', 'order.id = dr.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(ccr.created_at), '%Y.%m')",
                    'send_to_delivery' => "ROUND(SUM(COALESCE(dr.sent_at, dr.created_at) - ccr.approved_at)/count(*), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['is not', 'ccr.approved_at', null])
                ->andWhere(['>=', 'ccr.created_at', $last_update_delivery])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                первый звонок
            $dataFirstCall = Lead::find()->alias('lead')
                ->innerJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.order_id = lead.order_id')
                ->innerJoin(Order::tableName() . ' order', 'order.id = lead.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(lead.created_at), '%Y.%m')",
                    'first_call' => "ROUND(sum(ccr.sent_at - lead.created_at)/count(*), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['is not', 'ccr.sent_at', null])
                ->andWhere(['>=', 'lead.created_at', $last_update_call])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                попыток дозвона
            $dataTryCallCount = CallCenterRequestHistory::find()->alias('ccrh')
                ->innerJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.id = ccrh.call_center_request_id')
                ->innerJoin(Order::tableName() . ' order', 'order.id = ccr.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(ccr.created_at), '%Y.%m')",
                    'try_call_count' => "ROUND(count(*) / count(distinct order.id), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['>=', 'ccr.created_at', $last_update_call_try])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                дней дозвона
            $dataTryCallDays = CallCenterRequestHistory::find()->alias('ccrh')
                ->innerJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.id = ccrh.call_center_request_id')
                ->innerJoin(Order::tableName() . ' order', 'order.id = ccr.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(ccr.created_at), '%Y.%m')",
                    'try_call_days' => "ROUND(count(distinct concat(date_format(from_unixtime(ccrh.called_at), '%Y.%m')), order.id) / count(distinct order.id), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['>=', 'ccr.created_at', $last_update_call_try])
                ->andWhere(['is not', 'ccrh.called_at', null])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                % апрува КЦ
            $dataApprovePercent = Lead::find()->alias('lead')
                ->innerJoin(Order::tableName() . ' order', 'order.id = lead.order_id')
                ->leftJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.order_id = lead.order_id and ccr.approved_at is not null')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(lead.created_at), '%Y.%m')",
                    'approve_percent' => "ROUND(100 * count(distinct ccr.id) / count(lead.order_id), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['>=', 'lead.created_at', $last_update_approve])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;

//                средний чек апрува КЦ
            $dataAvgApproveCheck = Lead::find()->alias('lead')
                ->innerJoin(Order::tableName() . ' order', 'order.id = lead.order_id')
                ->innerJoin(CallCenterRequest::tableName() . ' ccr', 'ccr.order_id = lead.order_id and ccr.approved_at is not null')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(lead.created_at), '%Y.%m')",
                    'avg_approve_check' => "ROUND(avg(convertToCurrencyByCountry(order.price_total, order.price_currency, {$convertPriceTotalToCurrencyId})), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['>=', 'lead.created_at', $last_update_avg_check])
                ->andWhere(['is not', 'order.price_currency', null])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                кол-во ошибок адреса
            $dataErrorInAddress = Lead::find()->alias('lead')
                ->innerJoin(CallCenterCheckRequest::tableName() . ' cccr', "cccr.order_id = lead.order_id and cccr.type = '" . CallCenterCheckRequest::TYPE_CHECK_ADDRESS . "'")
                ->innerJoin(Order::tableName() . ' order', 'order.id = lead.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(lead.created_at), '%Y.%m')",
                    'error_in_address' => "count(cccr.id)",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['>=', 'lead.created_at', $last_update_error_address])
                ->groupBy(['country_id', 'month'])
                ->asArray()->all();

//                начало доставки
            $dataAcceptedTime = DeliveryRequest::find()->alias('dr')
                ->innerJoin(Order::tableName() . ' order', 'order.id = dr.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(dr.created_at), '%Y.%m')",
                    'delivery_id' => 'dr.delivery_id',
                    'accepted_time' => "ROUND(avg(dr.accepted_at - dr.sent_at), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['and',
                    ['is not', 'dr.sent_at', null],
                    ['is not', 'dr.accepted_at', null],
                    ['>', 'dr.accepted_at', 'dr.sent_at'],
                    ['>=', 'dr.created_at', $last_update_accepted]
                ])
                ->andWhere('dr.accepted_at > dr.sent_at')
                ->groupBy(['country_id', 'month', 'delivery_id'])
                ->asArray()->all();

//                поступление ДС
            $dataPaymentTime = DeliveryRequest::find()->alias('dr')
                ->innerJoin(Order::tableName() . ' order', 'order.id = dr.order_id')
                ->select([
                    'country_id' => 'order.country_id',
                    'month' => "date_format(from_unixtime(dr.created_at), '%Y.%m')",
                    'delivery_id' => 'dr.delivery_id',
                    'payment_time' => "ROUND(avg(dr.paid_at - dr.approved_at), {$round})",
                    'updated_at' => 'unix_timestamp()',
                ])
                ->where(['and',
                    ['is not', 'dr.paid_at', null],
                    ['is not', 'dr.approved_at', null],
                    ['>', 'dr.paid_at', 'dr.approved_at'],
                    ['>=', 'dr.created_at', $last_update_payment]
                ])
                ->andWhere('dr.paid_at > dr.sent_at')
                ->groupBy(['country_id', 'month', 'delivery_id'])
                ->asArray()->all();

//            расходы
            $reportCountries = ReportExpensesCountry::find()
                ->active()
                ->orderBy(['name' => SORT_ASC])
                ->all();
            foreach ($reportCountries as $country) {
                $i = 1;
                while (strtotime(--$i . ' MONTH') >= $last_update_costs) {
                    $date = strtotime($i . ' MONTH');
                    $reportForm = new ReportFormProfitabilityAnalysisForCompanyStandards();
                    $data = $reportForm->apply([
                        'country_id' => $country->id,
                        'month' => date('m', $date),
                        'year' => date('Y', $date),
                        'isForecastPeriod' => false,
                        'forecastParams' => null,
                    ]);
                    $result = [];
                    foreach ($data as $model) {
                        switch ($model['code']) {
                            case 'costs':   //расходы
                                $result['costs'] = $model['total'];
                                break;
                            case 'income':   //прибыль
                                $result['income'] = $model['total'];
                                break;
                            case 'lead_costs':   //реклама
                                $result['leadCost'] = $model['total'];
                                break;
                            case 'catalog.category.call_center.costs':   //по КЦ расходы
                                $result['callCenterCosts'] = $model['total'];
                                break;
                            case boolval(preg_match("/^catalog\.category\.product\.subcategory(\d{1,4}).costs$/i", $model['code'])):   //по продуктам
                                $result['productCosts'] += $model['total'];
                                break;
                        }
                    }
                    if (!empty($result)) {
                        $dataCosts[] = [
                            'country_id' => $country->country_id,
                            'month' => date('Y.m', $date),
                            'costs' => $result['costs'],
                            'income' => $result['income'],
                            'lead_cost' => $result['leadCost'],
                            'call_center_costs' => $result['callCenterCosts'],
                            'product_costs' => $result['productCosts'],
                        ];
                    }
                }
            }

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!empty($dataAdmissionLead)) {
                    $this->batchUpdate($dataAdmissionLead);
                }
                if (!empty($dataFirstSms)) {
                    $this->batchUpdate($dataFirstSms);
                }
                if (!empty($dataSendToDelivery)) {
                    $this->batchUpdate($dataSendToDelivery);
                }
                if (!empty($dataFirstCall)) {
                    $this->batchUpdate($dataFirstCall);
                }
                if (!empty($dataTryCallCount)) {
                    $this->batchUpdate($dataTryCallCount);
                }
                if (!empty($dataTryCallDays)) {
                    $this->batchUpdate($dataTryCallDays);
                }
                if (!empty($dataApprovePercent)) {
                    $this->batchUpdate($dataApprovePercent);
                }
                if (!empty($dataAvgApproveCheck)) {
                    $this->batchUpdate($dataAvgApproveCheck);
                }
                if (!empty($dataErrorInAddress)) {
                    $this->batchUpdate($dataErrorInAddress);
                }
                if (!empty($dataAcceptedTime)) {
                    $this->batchUpdate($dataAcceptedTime, CompanyStandardsDelivery::clearTableName(), ['country_id', 'month', 'delivery_id']);
                }
                if (!empty($dataPaymentTime)) {
                    $this->batchUpdate($dataPaymentTime, CompanyStandardsDelivery::clearTableName(), ['country_id', 'month', 'delivery_id']);
                }
                if (!empty($dataCosts)) {
                    $this->batchUpdate($dataCosts);
                }

                $query = (new Query())->createCommand();
                $query->update(CompanyStandards::clearTableName(), ['created_at' => new Expression('unix_timestamp()')], 'created_at is null')->execute();
                $query->update(CompanyStandardsDelivery::clearTableName(), ['created_at' => new Expression('unix_timestamp()')], 'created_at is null')->execute();

                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $this->log($e->getMessage());
                $cronLog($e->getMessage());
            }
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $cronLog($e->getMessage());
        }
    }

    /*====================== HELPER'S FUNCTION =====================*/

    /**
     * @param string $table
     * @param array $data
     * @param array $pk
     * @throws Exception
     */
    private function batchUpdate(array $data, string $table = 'report_company_standards', array $pk = ['country_id', 'month']): void
    {
        $pk = array_merge($pk, ['created_at']);
        $onDuplicate = [];
        $keys = array_keys($data[0]);
        foreach ($keys as $key) {
            if (empty($pk) || !in_array($key, $pk)) {
                $onDuplicate[] = "`{$key}`=VALUES(`{$key}`)";
            }
        }
        $onDuplicate = implode(',', $onDuplicate);
        foreach (array_chunk($data, 1000) as $record) {
            $query = new Query();
            $command = $query->createCommand()->batchInsert($table, $keys, $record);
            $command->setRawSql($command->getSql() . ' on duplicate key update ' . $onDuplicate);
            $command->execute();
        }
    }

    /**
     * @return array
     */
    private function getHoldsNotificationData()
    {

        $startDay = strtotime('now 00:00:00'); // - 325*86400;      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $endDay = $startDay + 86399;

        $stopList = Country::getStopListCountryNotification();
        $countryMap = Country::find()
            ->where([Country::tableName() . '.active' => 1])
            ->andWhere(["!=", Country::tableName() . '.is_stop_list', 1])
            ->all();
        $countryMap = ArrayHelper::map($countryMap, 'id', 'name');
        $data = [];
        foreach ($countryMap as $key => $cname) {
            $data[] = [
                'country_id' => $key,
                'country_name' => $cname,
                'count_lead' => number_format(round(0, 2), 2),
                'count_hold' => number_format(round(0, 2), 2),
                'count_sent' => number_format(round(0, 2), 2),
                'count_accept' => number_format(round(0, 2), 2),
            ];
        }

        // Получаем лиды и холды за весь период с разбивкой по странам
        $dataLead = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'count_lead' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getLeadStatusList()) . ') OR NULL)',
                'count_hold' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getHoldStatusList()) . ') OR NULL)',
            ])
            ->where([Order::tableName() . '.duplicate_order_id' => null])
            ->andWhere(["NOT IN", Country::tableName() . '.id', $stopList])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        // Получаем количество отправленных в курьерку и принятых ими заказов
        $dataSent = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'count_sent' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getTransferredStatusList()) . ') OR NULL)',
                'count_accept' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getInDeliveryAndFinanceList()) . ') OR NULL)',
            ])
            ->where([Order::tableName() . '.duplicate_order_id' => null])
            ->andWhere(["NOT IN", Country::tableName() . '.id', $stopList])
            ->andWhere([">=", Order::tableName() . ".created_at", $startDay])
            ->andWhere(["<=", Order::tableName() . ".created_at", $endDay])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        foreach ($data as &$item) {
            $item['count_lead'] = 0;
            $item['count_hold'] = 0;
            foreach ($dataLead as $datum) {
                if ($datum['country_id'] == $item['country_id']) {
                    $item['count_lead'] = $datum['count_lead'];
                    $item['count_hold'] = $datum['count_hold'];
                }
            }
            $item['count_sent'] = 0;
            $item['count_accept'] = 0;
            foreach ($dataSent as $datum) {
                if ($datum['country_id'] == $item['country_id']) {
                    $item['count_sent'] = $datum['count_sent'];
                    $item['count_accept'] = $datum['count_accept'];
                }
            }
        }

        return $data;
    }


    /**
     * формируем текст по холдам для команд
     *
     * @param $team
     * @param $teamCountries
     * @param $data
     *
     * @return string
     */
    private function prepareHoldsNotificationText($team, $teamCountries, $data)
    {

        $total = [
            'leads' => 0,
            'holds' => 0,
            'sent' => 0,
            'accept' => 0,
        ];
        $temp = '';
        foreach ($teamCountries as $country) {
            foreach ($data as $item) {
                if ($item['country_id'] == $country) {
                    $temp .= $item['country_name'] . ':' . PHP_EOL;
//                    $temp .= "\t" .Yii::t('common', 'Лиды:') ."\t\t\t\t" .$item['count_lead'] .PHP_EOL;
                    $temp .= "    " . Yii::t('common', 'Холды:') . "    " . $item['count_hold'] . PHP_EOL;
//                    $temp .= "\t" .Yii::t('common', 'Отправлено в КС:') ."\t" .$item['count_sent'] .PHP_EOL;
                    $temp .= "    " . Yii::t('common', 'Принято КС:') . "    " . $item['count_accept'] . PHP_EOL;

                    $total['leads'] += intval($item['count_lead']);
                    $total['holds'] += intval($item['count_hold']);
                    $total['sent'] += intval($item['count_sent']);
                    $total['accept'] += intval($item['count_accept']);
                }
            }
        }

        $text = 'По состоянию на ' . date('d.m.Y', time())
            . ' по странам региона ' . $team['name']
            . ' распределение заказов составляет:' . PHP_EOL;

        $text .= $temp;
        $text .= Yii::t('common', 'Итого по региону: ')
            //            .$total['leads'] .' лидов, '
            . $total['holds'] . ' холдов, '
            //            .$total['sent'] .' отправленных в КС, '
            . $total['accept'] . ' принято КС'
            . PHP_EOL;

        return $text;
    }


    /**
     * @return array
     */
    private function getOrdersInHoldStatusNotificationData()
    {

        $stopList = Country::getStopListCountryNotification();

        $prevData = json_decode(Yii::$app->cache->get('orders_in_lead_status_notification'), true);

        // Получаем общее количество (оставшихся) лидов за сегодня
        $startDay = strtotime('now 00:00:00'); // - 325 * 86400;                         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $endDay = $startDay + 86399;
        $todayLeadsCount = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'count_lead' => 'COUNT(1)',
            ])
            ->where([Order::tableName() . '.duplicate_order_id' => null])
            ->andWhere(["NOT IN", Country::tableName() . '.id', $stopList])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getHoldStatusList()])
            ->andWhere([">=", Order::tableName() . ".created_at", $startDay])
            ->andWhere(["<=", Order::tableName() . ".created_at", $endDay])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        if (!empty($todayLeadsCount)) {
            Yii::$app->cache->set('orders_in_lead_status_notification', json_encode($todayLeadsCount), 86400);
        }

        $data = [];
        foreach ($todayLeadsCount as $tItem) {

            $sign = "\xE2\x9D\x97";
            $tendency = "\xF0\x9F\x94\xBB";
            $value = 0;
            if (empty($prevData)) {
                if (isset($tItem['count_lead'])) {
                    $value = $tItem['count_lead'];
                }
                $data[] = [
                    'country_id' => $tItem['country_id'],
                    'country_name' => $tItem['country_name'],
                    'sign' => $sign,
                    'tendency' => "\xF0\x9F\x94\xBA",
                    'value' => $value,
                ];
            } else {
                foreach ($prevData as $pData) {
                    if ($tItem['country_id'] == $pData['country_id']) {
                        if ($pData['count_lead'] < $tItem['count_lead']) {
                            $tendency = "\xF0\x9F\x94\xBA";
                        }
                        if (isset($tItem['count_lead'])) {
                            $value = $tItem['count_lead'];
                        }
                        $data[] = [
                            'country_id' => $tItem['country_id'],
                            'country_name' => $tItem['country_name'],
                            'sign' => $sign,
                            'tendency' => $tendency,
                            'value' => $value,
                        ];
                    }
                }
            }
        }

        return $data;
    }

    /**
     * формируем текст по заказам в статусах лидов для команд
     *
     * @param $team
     * @param $teamCountries
     * @param $data
     *
     * @return string
     */
    private function prepareOrdersInHoldStatusNotificationText($team, $teamCountries, $data)
    {

        $total = 0;
        $temp = '';
        foreach ($teamCountries as $country) {
            foreach ($data as $item) {
                if ($item['country_id'] == $country) {
                    $temp .= $item['sign'] . ' ' . $item['country_name'] . ":    " . $item['value'] . ' ' . $item['tendency'] . PHP_EOL;
                    $total += $item['value'];
                }
            }
        }

        $text = 'По состоянию на ' . date('d.m.Y H:i', time())
            . ' по странам региона ' . $team['name']
            . ' количество заказов ожидающих обзвона составляет:' . PHP_EOL;

        $text .= $temp;
        $text .= Yii::t('common', 'Итого по региону: ')
            . $total . ' заказов ожидающих обзвона'
            . PHP_EOL;

        return $text;
    }

    /**
     * @return array
     */
    private function getOrdersInReadyToSendStatusNotificationData()
    {

        $stopList = Country::getStopListCountryNotification();

        $prevData = json_decode(Yii::$app->cache->get('orders_in_ready_to_send_status_notification'), true);

        // Получаем общее количество ожидающих отправки заказов за сегодня
        $startDay = strtotime('now 00:00:00'); // - 325 * 86400;                         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $endDay = $startDay + 86399;
        $todayReadyCount = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'count_lead' => 'COUNT(1)',
            ])
            ->where([Order::tableName() . '.duplicate_order_id' => null])
            ->andWhere(["NOT IN", Country::tableName() . '.id', $stopList])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getReadyToSendStatusList()])
            ->andWhere([">=", Order::tableName() . ".created_at", $startDay])
            ->andWhere(["<=", Order::tableName() . ".created_at", $endDay])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        if (!empty($todayReadyCount)) {
            Yii::$app->cache->set('orders_in_ready_to_send_status_notification', json_encode($todayReadyCount), 86400);
        }

        $data = [];
        foreach ($todayReadyCount as $tItem) {

            $sign = "\xE2\x9D\x97";
            $tendency = "\xF0\x9F\x94\xBB";
            $value = 0;
            if (empty($prevData)) {
                if (isset($tItem['count_lead'])) {
                    $value = $tItem['count_lead'];
                }
                $data[] = [
                    'country_id' => $tItem['country_id'],
                    'country_name' => $tItem['country_name'],
                    'sign' => $sign,
                    'tendency' => "\xF0\x9F\x94\xBA",
                    'value' => $value,
                ];
            } else {
                foreach ($prevData as $pData) {
                    if ($tItem['country_id'] == $pData['country_id']) {
                        if ($pData['count_lead'] < $tItem['count_lead']) {
                            $tendency = "\xF0\x9F\x94\xBA";
                        }
                        if (isset($tItem['count_lead'])) {
                            $value = $tItem['count_lead'];
                        }
                        $data[] = [
                            'country_id' => $tItem['country_id'],
                            'country_name' => $tItem['country_name'],
                            'sign' => $sign,
                            'tendency' => $tendency,
                            'value' => $value,
                        ];
                    }
                }
            }
        }

        return $data;
    }

    /**
     * формируем текст по заказам в статусах ожидают отправки в КС для команд
     *
     * @param $team
     * @param $teamCountries
     * @param $data
     *
     * @return string
     */
    private function prepareOrdersInReadyToSendStatusNotificationText($team, $teamCountries, $data)
    {

        $total = 0;
        $temp = '';
        foreach ($teamCountries as $country) {
            foreach ($data as $item) {
                if ($item['country_id'] == $country) {
                    $temp .= $item['sign'] . ' ' . $item['country_name'] . ":\t\t" . $item['value'] . ' ' . $item['tendency'] . PHP_EOL;
                    $total += $item['value'];
                }
            }
        }

        $text = 'По состоянию на ' . date('d.m.Y H:i', time())
            . ' по странам региона ' . $team['name']
            . ' заказов ожидающих отправки в КС:' . PHP_EOL;

        $text .= $temp;
        $text .= Yii::t('common', 'Итого по региону: ')
            . $total . ' ожидающих отправки в КС заказов'
            . PHP_EOL;

        return $text;
    }


    /**
     * Пересчет всех данных для динамики дебиторской задолженности
     * @throws \Exception
     */
    function actionSaveDeliveryDebtsDaily()
    {
        $debtsDaily = new DebtsDaily();
        $baseData = $debtsDaily->loadBaseData(new DebtsDailyRequest());
        if ($baseData) {
            $debtsDaily->saveBaseData($baseData);
        }
    }
}
