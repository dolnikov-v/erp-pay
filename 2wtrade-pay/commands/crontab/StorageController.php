<?php
namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryStorage;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\storage\models\StorageProductPurchase;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\Order;
use app\models\Product;
use app\models\Country;
use app\models\AuthAssignment;
use app\models\User;
use Yii;
use yii\swiftmailer\Message as SwiftMessage;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\storage\models\StorageDocument;
use app\modules\bar\models\Bar;
use app\modules\storage\models\StorageHistoryOrder;
use yii\db\Expression;

/**
 * Class StorageController
 * @package app\commands\crontab
 */
class StorageController extends CrontabController
{
    protected $mapTasks = [
        'storageproductpurchase' => CrontabTask::TASK_STORAGE_PRODUCT_PURCHASE,
        'freestoragehistoryorder' => CrontabTask::TASK_FREE_STORAGE_HISTORY_ORDER,
    ];

    /**
     * Отправка email о min складских остатках
     */
    public function actionStorageProductPurchase()
    {
        $this->log('Send emails for storage product purchase... ');

        $logAnswer = new CrontabTaskLogAnswer();
        $logAnswer->log_id = $this->log->id;

        $leaderEmails = null;
        $userEmails = null;

        try {
            $query = StorageProductPurchase::find()
                ->select([
                    'id' => StorageProductPurchase::tableName() . '.id',
                    'country_id' => StorageProductPurchase::tableName() . '.country_id',
                    'country_name' => Country::tableName() . '.name',
                    'product_id' => StorageProductPurchase::tableName() . '.product_id',
                    'product_name' => Product::tableName() . '.name',
                    'plan_lead_day' => StorageProductPurchase::tableName() . '.plan_lead_day',
                    'limit_days' => StorageProductPurchase::tableName() . '.limit_days',
                    'limit_count' => StorageProductPurchase::tableName() . '.limit_count',
                ])
                ->joinWith(['country', 'product'])
                ->orderBy('country_name, product_name');

            $query->addSelect([
                'balance' => StorageProduct::find()
                    ->select(['sum(' . StorageProduct::tableName() . '.balance)'])
                    ->joinWith(['storage'])
                    ->where([Storage::tableName() . '.country_id' => StorageProductPurchase::tableName() . '.country_id'])
                    ->andWhere([StorageProduct::tableName() . '.product_id' => StorageProductPurchase::tableName() . '.product_id'])
            ]);

            $query->addSelect([
                'orders' => Order::find()
                    ->select(['count(1) / 3'])
                    ->joinWith(['orderProducts'])
                    ->where([Order::tableName() . '.country_id' => StorageProductPurchase::tableName() . '.country_id'])
                    ->andWhere([OrderProduct::tableName() . '.product_id' => StorageProductPurchase::tableName() . '.product_id'])
                    ->andWhere(['>=', OrderProduct::tableName() . '.created_at', time() - 3 * 86400])
                    ->andWhere(['is', Order::tableName() . '.duplicate_order_id', null])
            ]);

            $data = $query->asArray()->all();
            $arrayForLeadersSend = null;
            $arrayForSend = null;
            foreach ($data as $item) {
                if ($item['balance'] <= $item['limit_count']) {
                    $arrayForLeadersSend[] = $item;
                    continue;
                }

                $fact = 0;
                if (isset($item['orders']) && $item['orders'] > 0) {
                    $fact = $item['balance'] / $item['orders'];
                }

                if ($fact <= $item['limit_days']) {
                    $arrayForLeadersSend[] = $item;
                    continue;
                }

                $plan = 0;
                if (isset($item['plan_lead_day']) && $item['plan_lead_day'] > 0) {
                    $plan = $item['balance'] / $item['plan_lead_day'];
                }
                if ($plan <= $item['limit_days']) {
                    $arrayForSend[] = $item;
                    continue;
                }
            }

            $emails = AuthAssignment::find()
                ->select(['email' => User::tableName() . '.email'])
                ->leftJoin(User::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id')
                ->where(['item_name' => 'leader'])
                ->asArray()->all();

            foreach ($emails as $email) {
                $leaderEmails[] = $email['email'];
            }

            $emails = AuthAssignment::find()
                ->select(['email' => User::tableName() . '.email'])
                ->leftJoin(User::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id')
                ->where(['item_name' => 'supply.manager'])
                ->asArray()->all();

            foreach ($emails as $email) {
                $userEmails[] = $email['email'];
            }

            $textBody = "";
            $country_name = null;
            foreach ($arrayForLeadersSend as $oneItem) {
                if ($country_name == null || $country_name != $oneItem['country_name']) {
                    $textBody .= $country_name . PHP_EOL;
                }
                $textBody .= $oneItem['product_name'] . ' ' . $oneItem['balance'] . PHP_EOL;
                $country_name = $oneItem['country_name'];
            }

            $message = new SwiftMessage();
            $message->setFrom(Yii::$app->params['infoMailAddress']);
            $message->setSubject("2WTrade storage products with min balance - " . date('Y.m.d', time()));
            $message->setTextBody($textBody);
            $message->setTo($leaderEmails);
            if ($message->send()) {
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $logAnswer->answer = json_encode([
                    'emails' => ($leaderEmails == null) ? '' : implode(",", $leaderEmails),
                ]);
            } else {
                throw new \Exception('Не удалось отправить сообщение лидерам.');
            }

            $textBody = "";
            foreach ($arrayForSend as $oneItem) {
                if ($country_name == null || $country_name != $oneItem['country_name']) {
                    $textBody .= $country_name . PHP_EOL;
                }
                $textBody .= $oneItem['product_name'] . ' ' . $oneItem['balance'] . PHP_EOL;
                $country_name = $oneItem['country_name'];
            }
            $message->setFrom(Yii::$app->params['infoMailAddress']);
            $message->setSubject("2WTrade storage products with min balance - " . date('Y.m.d', time()));
            $message->setTextBody($textBody);
            $message->setTo($userEmails);
            if ($message->send()) {
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                $logAnswer->answer = json_encode([
                    'emails' => ($userEmails == null) ? '' : implode(",", $userEmails),
                ]);
            } else {
                throw new \Exception('Не удалось отправить сообщение остальным.');
            }

            $logAnswer->data = json_encode([
                'emails' => (($leaderEmails == null) ? '' : implode(",", $leaderEmails)) . ',' . (($userEmails == null) ? '' : implode(",", $userEmails)),
            ]);

            $logAnswer->save();

        } catch (\Exception $e) {
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->answer = json_encode([
                'error' => $e->getMessage(),
                'emails' => (($leaderEmails == null) ? '' : implode(",", $leaderEmails)) . ',' . (($userEmails == null) ? '' : implode(",", $userEmails)),
            ]);
            $this->log($e->getMessage());
        }

        $this->log('done.');
    }

    /**
     * Распределение товаров по складам (без штрих-кодов)
     */
    public function actionFreeStorageHistoryOrder()
    {
        /* списываемые товары */
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $data = StorageHistoryOrder::find()
                ->select([
                    'id' => StorageHistoryOrder::tableName() . '.id',
                    'product_id' => StorageHistoryOrder::tableName() . '.product_id',
                    'order_id' => StorageHistoryOrder::tableName() . '.order_id',
                    'country_id' => StorageHistoryOrder::tableName() . '.country_id',
                    'quantity' => StorageHistoryOrder::tableName() . '.quantity',
                    'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                ])
                ->innerJoinWith('deliveryRequest')
                ->where(['type' => 'minus'])
                ->orderBy(StorageHistoryOrder::tableName() . '.cron_launched_at')
                ->limit(1000)->asArray()->all();

            $arrayForDelete = null;
            $arrayForUpdate = null;

            foreach ($data as $item) {
                $bar = Bar::find()
                    ->where([Bar::tableName() . '.order_id' => $item['order_id']])->limit(1)->one();

                if ($bar !== null) { /* такой заказ уже используется в штрих-кодах */
                    $arrayForDelete[] = $item['id'];
                    continue;
                }

                $storageDoc = StorageDocument::find()
                    ->where([StorageDocument::tableName() . '.order_id' => $item['order_id']])
                    ->andWhere([StorageDocument::tableName() . '.product_id' => $item['product_id']])
                    ->andWhere([StorageDocument::tableName() . '.quantity' => $item['quantity']])
                    ->andWhere([StorageDocument::tableName() . '.type' => StorageDocument::TYPE_WRITEOFF_ORDER])
                    ->orderBy(StorageDocument::tableName() . '.created_at DESC')->one();

                if ($storageDoc !== null) { /* это дубликат, такая запись уже есть */
                    $arrayForDelete[] = $item['id'];
                    continue;
                }

                /* ищем склад с такой КС в этой стране */
                $storageItem = StorageProduct::find()
                    ->innerJoinWith('storage.deliveryStorage', false)
                    ->where([Storage::tableName() . '.country_id' => $item['country_id']])
                    ->andWhere([Storage::tableName() . '.use_bar_code' => 0])
                    ->andWhere([DeliveryStorage::tableName() . '.delivery_id' => $item['delivery_id']])
                    ->andWhere([Storage::tableName() . '.id' => new Expression(StorageProduct::tableName() . '.storage_id')])
                    ->andWhere([StorageProduct::tableName() . '.product_id' => $item['product_id']])
                    ->andWhere(['>=', StorageProduct::tableName() . '.balance', $item['quantity']])
                    ->limit(1)->one();

                /* ищем любой склад, привязанный к этой КС в этой стране */
                if ($storageItem === null) {
                    $storageItem = StorageProduct::find()
                        ->innerJoinWith('storage.deliveryStorage', false)
                        ->where([Storage::tableName() . '.country_id' => $item['country_id']])
                        ->andWhere([Storage::tableName() . '.use_bar_code' => 0])
                        ->andWhere([DeliveryStorage::tableName() . '.delivery_id' => $item['delivery_id']])
                        ->andWhere([Storage::tableName() . '.id' => new Expression(StorageProduct::tableName() . '.storage_id')])
                        ->andWhere([StorageProduct::tableName() . '.product_id' => $item['product_id']])
                        ->limit(1)->one();
                }

                if ($storageItem !== null) {
                    if ($storageItem->unlimit != 1) { // делаем только не для безлимитного товара
                        try {
                            StorageDocument::createNewDocument(
                                StorageDocument::TYPE_WRITEOFF_ORDER,
                                $storageItem->product_id,
                                $item['quantity'],
                                $storageItem->storage_id,
                                null,
                                null,
                                null,
                                $item['order_id'],
                                null,
                                null,
                                $storageItem->shelf_life
                            );
                        } catch (\Exception $e) {
                            $arrayForUpdate[] = $item['id'];
                            continue;
                        }
                        try {
                            StorageProduct::addProductOnStorage($storageItem->product_id, $storageItem->storage_id, -$item['quantity'], $storageItem->shelf_life);
                            OrderProduct::updateAll(['storage_id' => $storageItem->storage_id], ['order_id' => $item['order_id'], 'product_id' => $item['product_id'], 'quantity' => $item['quantity']]);
                        } catch (\Exception $e) {
                            $arrayForUpdate[] = $item['id'];
                            continue;
                        }
                    }
                }
                $arrayForDelete[] = $item['id'];
            }
            if (count($arrayForDelete) > 0) {
                StorageHistoryOrder::deleteAll(['in', 'id', $arrayForDelete]);
            }
            if (count($arrayForUpdate) > 0) {
                StorageHistoryOrder::updateAll(['cron_launched_at' => time()], ['in', 'id', $arrayForUpdate]);
            }
            unset($arrayForDelete);
            unset($arrayForUpdate);
            $transaction->commit();
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $transaction->rollBack();
        }


        /* возвращаемые товары */
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $data = StorageHistoryOrder::find()
                ->select([
                    'id' => StorageHistoryOrder::tableName() . '.id',
                    'product_id' => StorageHistoryOrder::tableName() . '.product_id',
                    'order_id' => StorageHistoryOrder::tableName() . '.order_id',
                    'country_id' => StorageHistoryOrder::tableName() . '.country_id',
                    'quantity' => StorageHistoryOrder::tableName() . '.quantity',
                ])
                ->where(['type' => 'plus'])
                ->orderBy(StorageHistoryOrder::tableName() . '.cron_launched_at')
                ->limit(1000)->asArray()->all();

            $arrayForDelete = null;
            $arrayForUpdate = null;

            foreach ($data as $item) {
                $storageDoc = StorageDocument::find()
                    ->where([StorageDocument::tableName() . '.order_id' => $item['order_id']])
                    ->andWhere([StorageDocument::tableName() . '.product_id' => $item['product_id']])
                    ->andWhere([StorageDocument::tableName() . '.quantity' => $item['quantity']])
                    ->andWhere([StorageDocument::tableName() . '.type' => StorageDocument::TYPE_RETURN_ORDER])
                    ->orderBy(StorageDocument::tableName() . '.created_at DESC')->one();

                if ($storageDoc !== null) { /* это дубликат, такая запись уже есть */
                    $arrayForDelete[] = $item['id'];
                    continue;
                }

                /* ищем продукт заказа */
                $productItem = OrderProduct::find()
                    ->where([OrderProduct::tableName() . '.order_id' => $item['order_id']])
                    ->andWhere([OrderProduct::tableName() . '.product_id' => $item['product_id']])
                    ->andWhere([OrderProduct::tableName() . '.quantity' => $item['quantity']])
                    ->limit(1)->one();

                if ($productItem !== null) {
                    if ($productItem->storage_id != null) { // это заказ из безлимитной партии
                        try {
                            StorageDocument::createNewDocument(
                                StorageDocument::TYPE_RETURN_ORDER,
                                $productItem->product_id,
                                $item['quantity'],
                                $productItem->storage_id,
                                null,
                                null,
                                null,
                                $item['order_id'],
                                null,
                                null
                            );
                        } catch (\Exception $e) {
                            $arrayForUpdate[] = $item['id'];
                            continue;
                        }
                        try {
                            StorageProduct::addProductOnStorage($productItem->product_id, $productItem->storage_id, $item['quantity']);
                            OrderProduct::updateAll(['storage_id' => null], ['id' => $productItem->id]);
                        } catch (\Exception $e) {
                            $arrayForUpdate[] = $item['id'];
                            continue;
                        }
                    }
                    $arrayForDelete[] = $item['id'];
                } else {
                    $arrayForUpdate[] = $item['id'];
                }

            }
            if (count($arrayForDelete) > 0) {
                StorageHistoryOrder::deleteAll(['in', 'id', $arrayForDelete]);
            }
            if (count($arrayForUpdate) > 0) {
                StorageHistoryOrder::updateAll(['cron_launched_at' => time()], ['in', 'id', $arrayForUpdate]);
            }
            unset($arrayForDelete);
            unset($arrayForUpdate);
            $transaction->commit();
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $transaction->rollBack();
        }
    }
}