<?php

namespace app\commands\crontab;

use app\models\Country;
use app\modules\administration\models\CrontabTask;
use app\modules\order\models\Order;
use app\modules\report\models\SmsPollHistory;
use app\components\console\CrontabController;
use app\modules\smsnotification\components\GoogleApi;
use app\modules\report\controllers\SmsPollHistoryController as SmsPoll;
use Yii;


/**
 * Class WidgetController
 * @package app\commands\crontab
 */
class SmsPollHistoryController extends CrontabController
{
    const API_GET_ANSWERS = 'http://2wstore.com/poll/get_answers.php';
    const API_LOGIN = '2wtrade_users';
    const API_PASSWORD = '2wtrade_store123321';

    //Адрес сайта, на котором принимаются ответы
    const RESPONSE_LINK = 'http://2wstore.com/poll/id';

    protected $mapTasks = [
        'takesmspollhistory' => CrontabTask::TASK_TAKE_SMS_POLL_HISTORY,
        'getanswersforsmspollhistory' => CrontabTask::TASK_GET_ANSWERS_FOR_SMS_POLL_HISTORY
    ];

    public function actionGetAnswersForSmsPollHistory()
    {
        $answerApi = $this->sendToApi(self::API_GET_ANSWERS);

        $this->log('Sms poll history get answers.. ', true);

        /** @var app\modules\logger\components\log\Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        if ($answerApi['error'] != 0) {
            $log = $cronLog($answerApi['error'] . ':' . $answerApi['text_error'], ['get-answers' => false]);
        } else {
            $response = json_decode($answerApi['result']);

            if (!$response->success) {
                $log = $cronLog($response->error, ['get-answers' => false]);
            } else {

                $log = $cronLog(count($response->answers), ['get-answers' => true]);

                foreach ($response->answers as $k => $v) {
                    $model = SmsPollHistory::find()->where(['id' => $v->foreign_id])->one();

                    if (is_null($model)) {
                        $log = $cronLog('not found : ' . $v->foreign_id, ['get-answers' => false]);
                    } else {
                        $model->answer_text = $v->answer_text;
                        $model->answered_at = $v->answered_at;
                        $model->updated_at = $v->updated_at;

                        if (!$model->save()) {
                            $log = $cronLog('not saved : ' . json_encode([
                                    'not saved' => 'sms_poll_history',
                                    'foreign_id' => $v->foreign_id,
                                    'answered_text' => $v->answer_text,
                                    'answered_at' => $v->answered_at,
                                    'error' => var_export($model->getErrors())
                                ]), ['get-answers' => false]);
                        }
                    }
                }

            }
        }

        $this->log('done.');
    }

    /**
     * @return array
     */
    public function sendToApi($url)
    {
        $options = [
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
            CURLOPT_USERPWD => self::API_LOGIN . ':' . self::API_PASSWORD,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => ['from' => 1],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ];

        $c = curl_init($url);
        curl_setopt_array($c, $options);
        $error = curl_errno($c);
        $text_error = curl_error($c);
        $result = curl_exec($c);
        curl_close($c);

        return [
            'error' => $error,
            'text_error' => $text_error,
            'result' => $result
        ];
    }

    /**
     * @return array
     */
    public function actionTakeSmsPollHistory()
    {
        $questions = SmsPollHistory::find()
            ->joinWith(['order'])
            ->where(['=', SmsPollHistory::tableName() . '.status', SmsPollHistory::HISTORY_STATUS_READY])
            ->all();

        foreach ($questions as $question) {
            $country = Country::find()
                ->where([Country::tableName() . '.id' => $question->order->country_id])
                ->one();

            $responseUrl = GoogleApi::getShortUrl(['longUrl' => self::RESPONSE_LINK . '/' . $question->id]);

            //Гугл апи не вернул шортлинк
            if (!isset($responseUrl['answer']->id)) {
                return [
                    'status' => SmsPollHistory::HISTORY_STATUS_ERROR,
                    'message' => $responseUrl['answer']->error->message
                ];
            }

            $full_text = $question->question_text . ' ' . $responseUrl['answer']->id;

            $smsService = yii::$app->get('smsService');

            //попытка отправки смс
            $response = $smsService->sendSmsNotification(
                $question->customer_mobile,
                null,
                $full_text,
                $country->sms_notifier_phone_from,
                [
                    'char_code' => $country->char_code
                ]
            );

            if ($response['status'] == SmsPollHistory::HISTORY_STATUS_SUCCESS) {
                $response['message'] = null;
            } else {
                $response['status'] = SmsPollHistory::HISTORY_STATUS_ERROR;
                $response['code'] = 1;
                $response['message'] = json_encode($response['message'], JSON_UNESCAPED_UNICODE);
            }
            //обновляем данные по отосланному смс, пишем полный текст вопроса с ссылкой
            SmsPoll::saveHistory($question->order, $question, $response, $question->id);
        }

    }
}