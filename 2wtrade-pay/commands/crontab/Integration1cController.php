<?php
namespace app\commands\crontab;

use app\components\integration1c\SqsOrder;
use app\modules\administration\models\CrontabTask;
use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTaskLogAnswer;
use yii\di\Instance;


class Integration1cController extends CrontabController
{
    /**
     * @var SqsOrder
     */
    private $sqsOrder = 'sqsOrder';

    /**
     * @var array
     */
    protected $mapTasks = [
        'sendto1cchanges' => CrontabTask::TASK_SEND_TO_1C_CHANGES,
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->sqsOrder = Instance::ensure($this->sqsOrder, SqsOrder::class);
    }

    /**
     * @param int $count
     */
    public function actionSendTo1cChanges(int $count = 100)
    {
        try {
            $this->sqsOrder->sendPart();
        } catch (\Throwable $e) {
            $cronAnswer = new CrontabTaskLogAnswer();
            $cronAnswer->log_id = $this->log->id;
            $cronAnswer->data = $e->getMessage();
            $cronAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $cronAnswer->save();
            echo $e->getMessage();
        }
    }
}