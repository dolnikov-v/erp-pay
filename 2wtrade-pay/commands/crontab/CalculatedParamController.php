<?php
namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\models\CalculatedParam;
use app\modules\administration\models\CrontabTask;

/**
 * Class CalculatedParamController
 * @package app\commands\crontab
 */
class CalculatedParamController extends CrontabController
{
    /**
     * @var array
     */
    protected $mapTasks = [
        'run' => CrontabTask::TASK_CALCULATED_PARAM,
    ];

    /**
     * Вычисление параметров
     */
    public function actionRun()
    {
        set_time_limit(0);
        CalculatedParam::calculate();
    }
}