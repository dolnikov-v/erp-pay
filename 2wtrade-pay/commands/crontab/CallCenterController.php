<?php

namespace app\commands\crontab;

use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use app\modules\callcenter\components\api\Lead;
use app\modules\callcenter\components\NewCallCenterHandler;
use app\modules\callcenter\components\queue\CallCenterQueue;
use app\modules\callcenter\components\Sender;
use app\modules\callcenter\jobs\SendCallCenterRequestJob;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\logger\components\log\Logger;
use Yii;
use yii\di\Instance;

/**
 * Class CallCenterController
 * @package app\commands\crontab
 */
class CallCenterController extends CrontabController
{
    protected $mapTasks = [
        'sendrequests' => CrontabTask::TASK_CALL_CENTER_SEND_REQUESTS,
        'sendcheckorder' => CrontabTask::TASK_CALL_CENTER_SEND_CHECK_ORDER,
        'getstatuses' => CrontabTask::TASK_CALL_CENTER_GET_STATUSES,
        'getneworders' => CrontabTask::TASK_CALL_CENTER_GET_NEW_ORDERS,
        'resendrequests' => CrontabTask::TASK_CALL_CENTER_RESEND_REQUESTS,
        'getusers' => CrontabTask::TASK_CALL_CENTER_GET_USERS,
        'getcheckingorderinfo' => CrontabTask::TASK_CC_GET_CHECKING_ORDER_INFO,
        'getopersactivitydata' => CrontabTask::TASK_CC_GET_OPERS_ACTIVITY_DATA,
        'getopersnumberofcalls' => CrontabTask::TASK_CC_GET_OPERS_NUMBER_OF_CALLS,
        'actualizeopersdirections' => CrontabTask::TASK_ACTUALIZE_OPERS_DIRECTIONS,
        'sendcheckdelivery' => CrontabTask::TASK_CC_SEND_CHECK_DELIVERY,
        'trytosavesentleads' => CrontabTask::TASK_CALL_CENTER_TRY_TO_SAVE_SENT_LEADS,
        'getuserorder' => CrontabTask::TASK_CALL_CENTER_GET_USER_ORDER,
        'getproductcall' => CrontabTask::TASK_CALL_CENTER_GET_PRODUCT_CALL,
        'checkingstatus' => CrontabTask::TASK_CALL_CENTER_CHECKING_STATUS,
    ];

    /**
     * @var Sender|string|array
     */
    public $requestSender = [
        'class' => Sender::class
    ];

    /**
     * @var CallCenterQueue|string|array
     */
    public $requestSenderQueue = 'queueCallCenterSend';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->requestSender = Instance::ensure($this->requestSender, Sender::class, $this->module);
        $this->requestSenderQueue = Instance::ensure($this->requestSenderQueue, CallCenterQueue::class, $this->module);
    }

    /**
     * Отправка заявок на обзвон
     * @param integer $ccId
     * @param int $timeBetweenRepeats
     * @param int $batchSize
     */
    public function actionSendRequests($ccId = null, $timeBetweenRepeats = 60, $batchSize = 1000)
    {
        $this->log("Start pushing requests to queue for sending to call center...");
        $counter = 0;
        foreach ($this->requestSender->getOrdersWaitingForSendToCallCenter($ccId, $timeBetweenRepeats, $batchSize) as $order) {
            $this->requestSenderQueue->push(new SendCallCenterRequestJob([
                'orderId' => $order->id
            ]));
            $counter++;
        }
        $this->log("$counter requests was pushed to queue for sending to call center.");
    }

    /**
     * Повторная отправка заявок на обзвон
     */
    public function actionResendRequests()
    {
        $this->log('Resending requests in call-center... ', false);

        Yii::$app->db->enableSlaves = false;
        set_time_limit(0);
        Lead::resendCallCenterRequests($this->log);

        $this->log('done.');
    }

    /**
     * Принятие результатов прозвона
     * @param integer $cc_id
     * @param string $type
     * @param integer $offset_from смещение даты создания с (в днях)
     * @param integer $offset_to смещение даты создания по (в днях)
     * @param integer $last_update_offset последнее обновление более n минут назад
     * @param integer $limit общий лимит на каждую очередь
     * @param integer $batchSize лимит заказов на один запрос
     */
    public function actionGetStatuses(
        $cc_id = null,
        $type = null,
        $offset_from = 0,
        $offset_to = 0,
        $last_update_offset = 5,
        $limit = 3000,
        $batchSize = 100
    )
    {
        $this->log("Getting statuses from Call Center ID# " . $cc_id . "...", false);

        set_time_limit(0);
        $start = time();

        $offset_from = intval($offset_from);
        $offset_to = intval($offset_to);
        $last_update_offset = intval($last_update_offset);
        $limit = intval($limit);
        $batchSize = intval($batchSize);

        Lead::getStatusesFromCallCenter($this->log, $cc_id, null, $type, $offset_from, $offset_to, $last_update_offset,
            $limit, $batchSize);

        $this->log(' done. Call Center ID# ' . $cc_id . ' in ' . (time() - $start) . " seconds");
    }

    /**
     * Получение новых заказов из колл-центра
     */
    public function actionGetNewOrders()
    {
        $this->log('Getting new orders from call-center... ', false);

        set_time_limit(0);
        Lead::getNewOrdersFromCallCenter($this->log);

        $this->log('done.');
    }

    /**
     * Отправка в 200 очередь для проверки
     * @param integer $cc_id
     * @param integer $limit
     * @param string $queue
     */
    public function actionSendCheckOrder($cc_id = null, $limit = 100, $queue = null)
    {
        $this->log('Send orders for checking by call-center... ', false);

        set_time_limit(0);
        $limit = intval($limit);
        $cc_id = intval($cc_id);

        if (empty($limit)) {
            $limit = 100;
        }
        Yii::$app->db->enableSlaves = false;
        Lead::sendCheckOrder($this->log, $cc_id, $limit, $queue);
        NewCallCenterHandler::sendCheckRequests($this->log, $cc_id, $queue);

        $this->log('done.');
    }

    /**
     * Получение информации о заказах, которые отправлены на проверку
     * @param null $cc_id
     * @param null $queue
     * @param int $limit
     * @param int $batch_size
     */
    public function actionGetCheckingOrderInfo($cc_id = null, $queue = null, $limit = 3000, $batch_size = 100)
    {
        $this->log('Getting information about checking orders from call-center... ', false);

        set_time_limit(0);
        $limit = intval($limit);
        $cc_id = intval($cc_id);

        if (empty($limit)) {
            $limit = 100;
        }
        Yii::$app->db->enableSlaves = false;
        Lead::getInformationForCheckingOrders($this->log, $cc_id, $queue, $limit, $batch_size);

        $this->log('done.');
    }

    /**
     * Получение списка пользователей Колл Центра
     */
    public function actionGetUsers($cc_id = null)
    {

        $this->log('Get Users from call-center... ', false);

        set_time_limit(0);
        Lead::getUsersFromCallCenter($this->log, $cc_id);

        $this->log('done.');
    }

    /**
     * Получение данных об отпработаннов времени пользователей Колл Центра
     * @param string $dateFrom
     * @param string $dateTo
     * @param null $cc_id
     * @param string $login
     * @throws \yii\db\Exception
     */
    public function actionGetOpersActivityData($dateFrom = '', $dateTo = '', $cc_id = null, $login = '')
    {
//        if ($dateFrom == '')
//            $dateFrom = date("Y-m-d", strtotime("-2day"));

        if ($dateTo == '')
            $dateTo = date('Y-m-d', strtotime('+1day'));

        $this->log('GetOpersActivity from call-center... ', false);

        Lead::getOpersActivityDataFromCallCenter($this->log, $cc_id, $dateFrom, $dateTo, $login);

        $this->log('done.');
    }

    /**
     * Получение данных о колчестве сделанных звонков пользователями Колл Центра
     * @param null $cc_id
     * @param string $dateFrom
     * @param string $dateTo
     */
    public function actionGetOpersNumberOfCalls($dateFrom = '', $dateTo = '', $cc_id = null)
    {
//        if ($dateFrom == '')
//            $dateFrom = date("Y-m-d", strtotime("-2day"));

        if ($dateTo == '')
            $dateTo = date('Y-m-d', strtotime('+1day'));

        $this->log('GetOpersNumberOfCalls from call-center... ', false);

        Lead::getOpersNumberOfCallsFromCallCenter($this->log, $cc_id, $dateFrom, $dateTo);

        $this->log('done.');
    }


    /**
     *
     */
    public function actionActualizeOpersDirections()
    {
        $this->log('Actualize Opers Directions... ', false);

        // TODO скрипт отключил до переработки, т.к. затирает старую нужную информацию
        // NewCallCenterHandler::actualizeOpersDirections($this->log);

        $this->log('done.');
    }

    /**
     * Отправка 10 заказов раз в сутки в статусе 15 в очередь check_delivery (NPAY-1453)
     */
    public function actionSendCheckDelivery()
    {
        CallCenterCheckRequest::sendToCheckDelivery();
    }

    /**
     * @param int $limit
     */
    public function actionTryToSaveSentLeads($limit = 1000)
    {
        set_time_limit(0);

        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");

        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
            'crontabLog' => $this->log->id,
        ]);

        Lead::tryToSaveSentLeads($cronLog, $limit);
    }

    /**
     * Получение кол-ва звонков по заказам и по операторам из Колл Центра
     * @param integer $daysAgo - кол-во дней назад, за которые нужно запросить данные
     */
    public function actionGetUserOrder($daysAgo = null)
    {
        $this->log('Get Order calls from call-center... ', false);

        set_time_limit(0);
        date_default_timezone_set('UTC');
        Lead::getUserOrderFromCallCenter($this->log, $daysAgo);

        $this->log('done.');
    }

    /**
     * Получение кол-ва звонков по заказам из Колл Центра
     * @param integer $daysAgo - кол-во дней назад, за которые нужно запросить данные
     * @param integer $cc_id
     */
    public function actionGetProductCall($daysAgo = null, $cc_id = null)
    {
        $this->log('Get Product calls from call-center... ', false);

        set_time_limit(0);
        date_default_timezone_set('UTC');
        Lead::getProductCallFromCallCenter($this->log, $daysAgo, $cc_id);

        $this->log('done.');
    }

    /**
     * Запрос статусов обзвона в КЦ*
     * @param integer $common Количество секунд, прошедших с последнего обновления статуса обычной заявки, по умолчанию - 3600 секунд
     * @param integer $check Количество секунд, прошедших с последнего обновления статуса проверочной заявки, по умолчанию - 43200 секунд
     * @param integer $cc_id Идентификатор КЦ
     * @param string $dateFrom С какой даты проверять по созданию, формат Y-m-d
     * @param string $dateTo По какую дату создания заявки проверять, формат Y-m-d
     */
    public function actionCheckingStatus($common = 3600, $check = 43200, $cc_id = null, $dateFrom = null, $dateTo = null)
    {
        $this->log('Checking Status in call-center... ', false);

        NewCallCenterHandler::checkingStatus($this->log, $common, $check, $cc_id, $dateFrom, $dateTo);

        $this->log('done.');
    }
}
