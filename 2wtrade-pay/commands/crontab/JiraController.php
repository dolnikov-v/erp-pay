<?php
namespace app\commands\crontab;

use app\models\User;
use app\models\Country;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;
use app\components\jira\Jira;
use JiraRestApi\JiraException;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use \yii\db\Expression;
use app\modules\smsnotification\components\GoogleApi;

/**
 *
 * @property array $countries
 */
class JiraController extends CrontabController
{
    const FREEZE_NORMS_FOR_STATUSES = [
        '- 5 min' => [1, 2, 31],
        '- 2 hours' => 6,
        '- 1 sec' => [19, 32, 29],
        '- 24 hours' => 3
    ];

    protected $mapTasks = [
        'monitororderstatuses' => CrontabTask::TASK_MONITOR_ORDER_STATUSES,
        'repeatmonitororderstatuses' => CrontabTask::TASK_MONITOR_ORDER_STATUSES,
        'monitoringdelivering' => CrontabTask::TASK_JIRA_MONITORING_DELIVERING,
    ];

    protected static $deliveries;
    protected static $countryes;

    /**
     * Мониторинг заказов по состоянию статусов
     * при отклонении от нормы создается задача в Jira
     */
    public function actionMonitorOrderStatuses()
    {
        try {
            $countries = $this->getCountries();
            $this->stdout("Countries list received" . PHP_EOL, Console::FG_GREEN);
            $jira = new Jira();
            $frozenOrdersByCountry = [];

            foreach (self::FREEZE_NORMS_FOR_STATUSES as $timeNorm => $statuses) {
                $frozenOrdersByNorm = $this->getFrozenOrdersByNorm($countries['all'], $statuses, $timeNorm);
                $frozenOrdersByCountry = array_merge($frozenOrdersByCountry, $frozenOrdersByNorm);
            }
            $this->stdout("Order statuses indicators received" . PHP_EOL, Console::FG_GREEN);
            $frozenOrdersGroupByCountry = ArrayHelper::index($frozenOrdersByCountry, 'status_id', 'country_name');
            $frozenOrdersGroupByStatuses = ArrayHelper::index($frozenOrdersByCountry, 'country_name', 'status_id');

            $issue = $jira->createMonitoringIssue($frozenOrdersGroupByStatuses);
            $this->stdout("Main issue created $issue->key" . PHP_EOL, Console::FG_GREEN);
            $issueFields = [];
            foreach ($frozenOrdersGroupByCountry as $country => $frozenOrdersByStatuses) {
                $jira->issueType = 'Sub-task';
                $countryUser = $countries['byUser'][$country]['user'];
                $issueFields[] = $jira->buildIssueField($country, $frozenOrdersByStatuses, $countryUser);
            }
            $jira->createIssues($issueFields);
            $this->stdout("Done" . PHP_EOL, Console::FG_GREEN);
        } catch (JiraException $e) {
            echo PHP_EOL;
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED, Console::UNDERLINE);
        }
    }

    /**
     * Мониторинг заказов по состоянию статусов
     * при отклонении от нормы создается задача в Jira
     */
    public function actionRepeatMonitorOrderStatuses()
    {
        try {
            $countries = $this->getCountries();
            $this->stdout("Countries list received" . PHP_EOL, Console::FG_GREEN);
            $jira = new Jira();
            $frozenOrdersByCountry = [];

            foreach (self::FREEZE_NORMS_FOR_STATUSES as $timeNorm => $statuses) {
                $frozenOrdersByNorm = $this->getFrozenOrdersByNorm($countries['all'], $statuses, $timeNorm);
                $frozenOrdersByCountry = array_merge($frozenOrdersByCountry, $frozenOrdersByNorm);
            }

            $this->stdout("Order statuses indicators received" . PHP_EOL, Console::FG_GREEN);
            $frozenOrdersGroupByCountry = ArrayHelper::index($frozenOrdersByCountry, 'status_id', 'country_name');
            $frozenOrdersGroupByStatuses = ArrayHelper::index($frozenOrdersByCountry, 'country_name', 'status_id');
            $subTusks = $jira->getSubTasks();
            $jira->addComment($jira->parentIssueKey, $jira->buildSummaryDescription($frozenOrdersGroupByStatuses));

            $this->stdout("Main Task Comment created" . PHP_EOL, Console::FG_GREEN);

            foreach ($frozenOrdersGroupByCountry as $country => $frozenOrdersByStatuses) {
                $jira->addComment($subTusks[$country], $jira->descriptionText($frozenOrdersByStatuses));
            }
            $this->stdout("Done" . PHP_EOL, Console::FG_GREEN);
        } catch (JiraException $e) {
            print("Error occurred! " . PHP_EOL . $e->getMessage());
        }
    }

    /**
     * Мониторинг просроченных сроков доставки
     * при отклонении от нормы создаются задачи и подзадачи в Jira
     */
    public function actionMonitoringDelivering()
    {
        $this->initStatic();
        date_default_timezone_set('UTC');
        $statuses = OrderStatus::getProcessDeliveryAndLogisticList();

        $data = Order::find()
            ->joinWith(['deliveryRequest', 'country'], false)
            ->select([
                'count_orders' => new Expression('COUNT(1)'),
                'country_id' => Order::tableName() . '.country_id',
                'curator_user_id' => Country::tableName() . '.curator_user_id',
                'delivery_id' => DeliveryRequest::tableName() . '.delivery_id'
            ])
            ->where([Country::tableName() . '.is_stop_list' => 0])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.is_partner' => 0])
            ->andWhere([Country::tableName() . '.is_distributor' => 0])
            ->andWhere([Order::tableName() . '.status_id' => $statuses])
            ->andWhere(['is not', DeliveryRequest::tableName() . '.delivery_id', null])
            ->andWhere(['is_bad_delivery_request_sent_at(' . DeliveryRequest::tableName() . '.id, 
                ' . Order::tableName() . '.customer_zip, 
                ' . Order::tableName() . '.customer_city, 
                ' . Order::tableName() . '.customer_province)' => 1])
            ->groupBy(['country_id', 'delivery_id'])
            ->orderBy(['country_id' => SORT_ASC, 'delivery_id' => SORT_ASC])
            ->asArray()
            ->all();

        try {
            $jira = new Jira();
            if ($data) {
                $result = [];
                $totalOrders = 0;
                foreach ($data as $item) {

                    $countryId = $item['country_id'];
                    $deliveryId = $item['delivery_id'];

                    $delivery = self::$deliveries[$deliveryId] ?? '';
                    $country = self::$countryes[$countryId]['name'] ?? '';

                    $result[$countryId]['country'] = $country;

                    $result[$countryId]['count'] = $result[$countryId]['count'] ?? 0;
                    $result[$countryId]['count'] += $item['count_orders'];
                    $result[$countryId]['curator_user_id'] = $item['curator_user_id'] ?? 0;

                    $link = 'http://2wtrade-pay.com/' . (self::$countryes[$countryId]['slug'] ?? '') . "/order/index/index?DeliveryDelayFilter[list][]=1&DeliveryFilter[delivery][]=" . $item['delivery_id'] . "&StatusFilter[status][]=" . join('&StatusFilter[status][]=', $statuses);
                    $linkGoogle = GoogleApi::getShortUrl(['longUrl' => $link]);
                    if (isset($linkGoogle['answer']->id) && !empty($linkGoogle['answer']->id)) {
                        $link = $linkGoogle['answer']->id;
                    }
                    $result[$countryId]['delivery'][$deliveryId] = '[' . $delivery . ' - ' . $item['count_orders'] . ' orders were not delivered on time under the contract |' . $link . ']';

                    $totalOrders += $item['count_orders'];
                }

                $mainTaskTitle = $totalOrders . ' orders were not delivered on time under the contract';
                $mainTaskDescription = '';
                foreach ($result as $countryId => $countryResult) {

                    $result[$countryId]['title'] = $countryResult['country'] . ' - ' . $countryResult['count'] . ' orders were not delivered on time under the contract';
                    $result[$countryId]['description'] = implode(PHP_EOL, $countryResult['delivery']);

                    $result[$countryId]['receiver'] = '';
                    if ($countryResult['curator_user_id']) {
                        $userCurator = User::findOne($countryResult['curator_user_id']);
                        if ($userCurator && $userCurator->email) {
                            $userName = $jira->getJiraUserNameByEmail($userCurator->email);
                            $result[$countryId]['receiver'] = $userName;
                        }
                    }

                    $mainTaskDescription .= $result[$countryId]['title'] . PHP_EOL;
                    $mainTaskDescription .= $result[$countryId]['description'] . PHP_EOL . PHP_EOL;
                }


                $mainTaskKey = $jira->createTask($mainTaskTitle, $mainTaskDescription, [\Yii::$app->params['jira']['host']]);
                if (is_array($mainTaskKey)) {
                    foreach ($mainTaskKey as $user => $item) {
                        $this->stdout("Created issue key " . $item->key . PHP_EOL, Console::FG_GREEN);
                        foreach ($result as $countryResult) {
                            if ($countryResult['receiver']) {
                                $jira->createSubTask($item->key, $countryResult['title'], $countryResult['description'], $countryResult['receiver']);
                            }
                        }
                    }
                }
            }
        } catch (JiraException $e) {
            echo PHP_EOL;
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED, Console::UNDERLINE);
        }
    }


    /**
     * @param array $countries
     * @param array $statuses
     * @param string $norm
     * @return Order[]
     */
    public function getFrozenOrdersByNorm($countries, $statuses, $norm)
    {
        return $frozenOrders = Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->select([
                'count' => 'count(' . Order::tableName() . '.id)',
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'status_id' => Order::tableName() . '.status_id',
                'norm' => '"(' . $norm . ')"',
                'charCode' => Country::tableName() . '.char_code',
            ])
            ->where([Order::tableName() . '.status_id' => $statuses])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.id' => $countries])
            ->andWhere(['<', Order::tableName() . '.updated_at', strtotime($norm)])
            ->groupBy(['country_id', 'status_id'])
            ->asArray()
            ->all();
    }

    /**
     * Возвращает массив [ид стран для мониторинга, страны по имени с пользователем прикрепленным к этой стране]
     * @return array
     */
    protected function getCountries()
    {
        $users = User::find()->byRole('it_controller')->all();
        $countriesByUser = $countries = [];
        $jira = new Jira();

        foreach ($users as $user) {
            if (!$user->email) {
                $this->stderr("No email for user $user->username" . PHP_EOL, Console::FG_RED);
                continue;
            }

            $userName = $jira->getJiraUserNameByEmail($user->email);
            if (!$userName) {
                $this->stderr("User with $user->email не найден в Jira" . PHP_EOL, Console::FG_RED);
                continue;
            }
            $userCountries = $user->getCountries()
                ->select(['id', 'name'])
                ->andWhere(['=', Country::tableName() . '.is_stop_list', 0])
                ->asArray()
                ->indexBy('id')
                ->all();
            foreach ($userCountries as $countryId => $data) {
                $data += ['user' => $userName];
                $userCountries[$countryId] = $data;
            }
            $byCountryName = ArrayHelper::index($userCountries, 'name');
            $countries += $userCountries;
            $countriesByUser += $byCountryName;
        }
        $allUserCountryIds = array_keys($countries);
        return [
            'all' => $allUserCountryIds,
            'byUser' => $countriesByUser
        ];
    }

    protected function initStatic()
    {
        if (!is_array(self::$countryes)) {
            $countries = Country::find()->all();
            if (is_array($countries)) {
                foreach ($countries as $country) {
                    self::$countryes[$country->id] = [
                        'name' => $country->name,
                        'slug' => $country->slug,
                    ];
                }
            }
        }
        if (!is_array(self::$deliveries)) {
            self::$deliveries = Delivery::find()->collection();
        }
    }
}