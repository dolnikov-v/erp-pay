<?php
namespace app\commands\crontab;

use Yii;
use yii\helpers\Console;
use yii\console\Controller;
use app\modules\logger\components\log\Logger;
use app\modules\crocotime\models\CrocotimeWorkTime;
use app\modules\crocotime\components\CrocotimeApi;
use app\modules\administration\models\CrontabTask;
use app\modules\salary\models\Person;

/**
 * Class CrocotimeController
 * @package app\commands\crontab
 *
 * @property mixed $profileSettings
 * @property null|array $offices
 * @property bool|array $employees
 * @property mixed $profileSettingsByOffice
 */
class CrocotimeController extends Controller
{

    /**
     * @var array
     */
    protected $mapTasks = [
        'syncemployees' => CrontabTask::TASK_SYNC_EMPLOYEES,
        'syncoffices' => CrontabTask::TASK_SYNC_OFFICES,
        'worktime' => CrontabTask::TASK_WORK_TIME,
    ];

    /**
     *
     */
    public function actionSyncEmployees()
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);
        try {
            $currentEmployees = Person::find()->crocotimeIdList();
            $crocoTimeApi = new CrocotimeApi();

            $apiEmployees = $crocoTimeApi->getEmployees();
            $newEmployees = $crocoTimeApi->findNewEmployees($apiEmployees, $currentEmployees);
            $crocoTimeApi->setProfileSettings($newEmployees);
            $crocoTimeApi->bindEmployees($newEmployees);
            if ($crocoTimeApi->notifications) {
                $this->stdout('Notifications' . PHP_EOL . print_r($crocoTimeApi->notifications, true) . PHP_EOL, Console::BOLD);
	            $crocoTimeApi->sendEmailNotifications();
            }
            if ($crocoTimeApi->errors) {
                throw new \Exception(print_r($crocoTimeApi->errors, true));
            }
        } catch (\Exception $e) {
            $this->stdout($e->getMessage());
            $cronLog($e->getMessage());
        }
    }

    /**
     * @internal param $apiOffices [department_id => , display_name => ]
     */
    public function actionSyncOffices()
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);
        $crocoTimeApi = new CrocotimeApi();
        if (!$apiOffices = $crocoTimeApi->getOffices()) {
            $this->stdout('Error ' . print_r($crocoTimeApi->errors, true), Console::BOLD);
            $cronLog('Error ' . print_r($crocoTimeApi->errors, true));
        };
        $crocoTimeApi->bindOffices($apiOffices);
    }

    public function actionWorkTime()
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);

        try {
            $crocoTimeApi = new CrocotimeApi();
            $crocotimeEmployeesIds = array_keys(Person::find()->active()->crocotimeIdList());
            $workTime = $crocoTimeApi->workTime($crocotimeEmployeesIds);
            $activity = $crocoTimeApi->activity($crocotimeEmployeesIds);

            $errors = [];
            foreach ($crocotimeEmployeesIds as $employeeId) {

                if (!isset($workTime[$employeeId]->day)) {
                    continue;
                }

                $date = date('Y-m-d', $workTime[$employeeId]->day);

                $crocotimeWorkTime = CrocotimeWorkTime::find()
                    ->where([
                        'crocotime_employee_id' => $employeeId,
                        'date' => $date
                    ])->one();

                if (!$crocotimeWorkTime) {
                    $crocotimeWorkTime = new CrocotimeWorkTime();
                    $crocotimeWorkTime->crocotime_employee_id = $employeeId;
                    $crocotimeWorkTime->date = $date;
                }

                $crocotimeWorkTime->effective_time = $activity[$employeeId]->permitted_time ?? null;
                $crocotimeWorkTime->start_at = $workTime[$employeeId]->begin;
                $crocotimeWorkTime->end_at = $workTime[$employeeId]->end;
                $crocotimeWorkTime->forbidden_time = $activity[$employeeId]->forbidden_time ?? null;
                $crocotimeWorkTime->unknown_time = $activity[$employeeId]->unknown_time ?? null;
                $crocotimeWorkTime->late_time = $activity[$employeeId]->late_time ?? null;
                $crocotimeWorkTime->early_end_time = $activity[$employeeId]->early_end_time ?? null;
                $crocotimeWorkTime->summary_time = $activity[$employeeId]->summary_time ?? null;
                $crocotimeWorkTime->norm = $activity[$employeeId]->norm ?? null;

                if (!$crocotimeWorkTime->validate()) {
                    $errors[] = $crocotimeWorkTime->getFirstErrorAsString();
                    continue;
                }
                $crocotimeWorkTime->save();
            }

            if ($crocoTimeApi->errors) {
                throw new \Exception(print_r($crocoTimeApi->errors, true));
            }
        } catch (\Exception $exception) {
            $this->stdout($exception->getMessage());
            $cronLog($exception->getMessage());
        }

        if (isset($errors)) {
            $cronLog(print_r($errors, true));
        }
    }

    public function actionCheckAccount()
    {
        /** @var Logger $logger */
        $logger = Yii::$app->get("processingLogger");
        $cronLog = $logger->getLogger([
            'route' => $this->getRoute(),
            'process_id' => getmypid(),
        ]);
    }
}