<?php

namespace app\commands\crontab;

use app\jobs\UpdateVoipExpense;
use app\components\console\CrontabController;
use app\modules\administration\models\CrontabTask;
use yii\helpers\Console;
use yii\httpclient\Client;

/**
 * Class VoipExpenseController
 * @property array $foreignCountriesList
 * @property Client $client
 * @package app\commands\crontab
 */
class VoipExpenseController extends CrontabController
{
    protected $mapTasks = [
        'voipexpense' => CrontabTask::TASK_VOIP_EXPENSE
    ];

    public
        $startDate = null,
        $endDate = null,
        $specificCountryId = null;

    public function options($actionID)
    {
        return ['startDate', 'endDate', 'specificCountryId'];
    }

    public function optionAliases()
    {
        return [
            'from' => 'startDate',
            'to' => 'endDate',
            'country_id' => 'specificCountryId',
        ];
    }

    /**
     * Parse information from https://my.zorra.com/voice/stats/byCountry
     *
     * парсим после 02:00 по мск т.к. у них длительность звонка макс 2 часа и учитывается звонок на дату начала,
     * чтобы все звонки учитывались за сутки смотрим стату после 02:00 по мск на след день.
     *
     * Available params
     * -from
     * -to
     * -country_id
     * @return bool
     */
    public function actionVoipExpense()
    {
        if (!$this->validateParams()) {
            return false;
        }

        $job = new UpdateVoipExpense([
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'specificCountryId' => $this->specificCountryId,
            'log_id' => $this->log->id,
        ]);

        if (!$job->actionRun()) {
            $this->setInError();
            return false;
        }
        return true;
    }

    /**
     * Validate console params
     * @return bool
     */
    private function validateParams()
    {
        $success = true;
        if ($this->startDate && !strtotime($this->startDate)) {
            $this->stdout("Wrong date format - from! \n", Console::FG_RED);
            $success = false;
        }
        if ($this->endDate && !strtotime($this->endDate)) {
            $this->stdout("Wrong date format - to!\n", Console::FG_RED);
            $success = false;
        }
        return $success;
    }
}