<?php

namespace app\abstracts\rest;

/**
 * Interface AnswerInterface
 * @package app\abstracts
 */
interface AnswerInterface
{
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';
    const STATUS_WARNING = 'warning';

    /**
     * @return null|string
     */
    public function getStatus(): ?string;

    /**
     * @return null|string
     */
    public function getMessage(): ?string;

    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @return bool
     */
    public function isSuccess(): bool;
}