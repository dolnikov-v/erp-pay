<?php
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

class StickHeaderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/site';

    public $css = [
        'stick.css',
    ];

    public $js = [
        'stick.js',
    ];

    public function init() {
        if (!Yii::$app->user->isGuest) {
            $this->sourcePath = '@app/web/themes/' .Yii::$app->user->identity->theme .'/site';
        }
    }
}