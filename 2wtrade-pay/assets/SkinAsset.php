<?php
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Class SkinAsset
 * @package app\assets
 */
class SkinAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/skins';

    public $css = [];

    public $depends = [
        'app\assets\SiteAsset',
        'app\widgets\navbar\assets\CountrySwitcherAsset',
    ];

    public function init() {
        if (Yii::$app->user->identity->theme) {
            $this->sourcePath = '@app/web/themes/' . Yii::$app->user->identity->theme . '/skins';
        }
        if (Yii::$app->user->identity->skin) {
            $this->css = [Yii::$app->user->identity->skin . '/' . Yii::$app->user->identity->skin . '.min.css'];
        }
    }

}
