<?php
namespace app\assets\skins;

/**
 * Class TealAsset
 * @package app\assets\skins
 */
class TealAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/teal';

    public $css = [
        'teal.min.css'
    ];
}