<?php
namespace app\assets\skins;

/**
 * Class BrownAsset
 * @package app\assets\skins
 */
class BrownAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/brown';

    public $css = [
        'brown.min.css'
    ];
}