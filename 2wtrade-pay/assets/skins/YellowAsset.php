<?php
namespace app\assets\skins;

/**
 * Class YellowAsset
 * @package app\assets\skins
 */
class YellowAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/yellow';

    public $css = [
        'yellow.min.css'
    ];
}