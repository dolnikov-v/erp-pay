<?php
namespace app\assets\skins;

/**
 * Class GreenAsset
 * @package app\assets\skins
 */
class GreenAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/green';

    public $css = [
        'green.min.css'
    ];
}