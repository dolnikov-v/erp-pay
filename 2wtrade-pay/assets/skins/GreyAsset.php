<?php
namespace app\assets\skins;

/**
 * Class GreyAsset
 * @package app\assets\skins
 */
class GreyAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/grey';

    public $css = [
        'grey.min.css'
    ];
}