<?php
namespace app\assets\skins;

/**
 * Class BrownAsset
 * @package app\assets\skins
 */
class PurpleAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/purple';

    public $css = [
        'purple.min.css'
    ];
}