<?php
namespace app\assets\skins;

/**
 * Class IndigoAsset
 * @package app\assets\skins
 */
class IndigoAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/indigo';

    public $css = [
        'indigo.min.css'
    ];
}