<?php
namespace app\assets\skins;

/**
 * Class BrownAsset
 * @package app\assets\skins
 */
class RedAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/red';

    public $css = [
        'red.min.css'
    ];
}