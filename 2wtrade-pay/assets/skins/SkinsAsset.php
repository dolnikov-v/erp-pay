<?php
namespace app\assets\skins;

use yii\web\AssetBundle;

/**
 * Class SkinsAsset
 * @package app\assets\skins
 */
abstract class SkinsAsset extends AssetBundle
{
    public $depends = [
        'app\assets\SiteAsset'
    ];
}
