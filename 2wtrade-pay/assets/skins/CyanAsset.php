<?php
namespace app\assets\skins;

/**
 * Class CyanAsset
 * @package app\assets\skins
 */
class CyanAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/cyan';

    public $css = [
        'cyan.min.css'
    ];
}