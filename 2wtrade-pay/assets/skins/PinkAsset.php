<?php
namespace app\assets\skins;

/**
 * Class PinkAsset
 * @package app\assets\skins
 */
class PinkAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/pink';

    public $css = [
        'pink.min.css'
    ];
}