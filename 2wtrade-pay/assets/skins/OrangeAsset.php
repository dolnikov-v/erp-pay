<?php
namespace app\assets\skins;

/**
 * Class OrangeAsset
 * @package app\assets\skins
 */
class OrangeAsset extends SkinsAsset
{
    public $sourcePath = '@app/web/theme/skins/orange';

    public $css = [
        'orange.min.css'
    ];
}