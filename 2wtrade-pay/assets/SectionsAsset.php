<?php
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class SectionsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/sections';

    public $js = [
        'menu.min.js',
        'menubar.min.js',
        'gridmenu.min.js',
        'sidebar.min.js'
    ];

    public function init() {
        if (!Yii::$app->user->isGuest) {
            $this->sourcePath = '@app/web/themes/' .Yii::$app->user->identity->theme .'/sections';
        }
    }
}
