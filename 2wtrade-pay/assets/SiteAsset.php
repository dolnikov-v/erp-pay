<?php
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class SiteAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/site';

    public $css = [
        'site.min.css',
        'main.css',
    ];

    public $js = [
        'core.min.js',
        'site.min.js',
        'main.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\assets\vendor\BootstrapAsset',
        'app\assets\vendor\LsCacheAsset',
        'app\assets\FontAsset',
    ];

    public function init() {
        if (!Yii::$app->user->isGuest) {
            $this->sourcePath = '@app/web/themes/' .Yii::$app->user->identity->theme .'/site';
        }
    }
}

// @todo: Перенести стили bootstrap в папку vendor/bootstrap
// @todo: Добавить apple-touch-icon и shortcut icon
