<?php
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Class ErrorAsset
 * @package app\assets
 */
class ErrorsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/basic/pages/errors';

    public $css = [
        'errors.min.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];

    public function init() {
        if (!Yii::$app->user->isGuest) {
            $this->sourcePath = '@app/web/themes/' .Yii::$app->user->identity->theme .'/pages/errors';
        }
    }
}
