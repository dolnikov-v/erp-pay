<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class ScriptAsset extends AssetBundle
{
    public $depends = [
        'app\assets\vendor\ModernizrAsset',
        'app\assets\vendor\BreakpointsAsset',
    ];
}
