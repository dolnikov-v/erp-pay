<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AnimsitionAsset
 * @package app\assets\vendor
 */
class AnimsitionAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/animsition';

    public $css = [
        'animsition.min.css'
    ];

    public $js = [
        'animsition.min.js',
        'components/animsition.min.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
