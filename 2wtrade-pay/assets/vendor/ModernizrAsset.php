<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class ModernizrAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/modernizr';

    public $js = [
        'modernizr.js'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
