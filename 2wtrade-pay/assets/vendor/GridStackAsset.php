<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class GridStackAsset
 * @package doindo\assets
 */
class GridStackAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/gridstack';

    public $css = [
        'gridstack.min.css',
        'gridstack-extra.min.css'
    ];

    public $js = [
        'gridstack.min.js',
        'component/gridstack.min.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
        'app\assets\vendor\JqueryUiAsset',
        'app\assets\vendor\LoDashAsset',
    ];
}
