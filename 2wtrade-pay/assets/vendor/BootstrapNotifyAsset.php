<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapNotifyAsset
 * @package app\assets\vendor
 */
class BootstrapNotifyAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/bootstrap-notify';

    public $js = [
        'notify.min.js',
    ];
}
