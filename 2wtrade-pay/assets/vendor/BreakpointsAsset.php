<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class BreakpointsAsset
 * @package app\assets\vendor
 */
class BreakpointsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/breakpoints';

    public $js = [
        'breakpoints.js'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
