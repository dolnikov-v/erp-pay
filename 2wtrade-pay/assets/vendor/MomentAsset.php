<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class MomentAsset
 * @package app\assets\vendor
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/moment';

    public $js = [
        'moment-with-locales.js',
    ];
}
