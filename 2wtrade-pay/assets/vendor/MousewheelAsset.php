<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class MousewheelAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/mousewheel';

    public $js = [
        'jquery.mousewheel.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
