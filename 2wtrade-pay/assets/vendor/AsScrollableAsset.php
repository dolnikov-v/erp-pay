<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AsScrollableAsset
 * @package app\assets\vendor
 */
class AsScrollableAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/asscrollable';

    public $css = [
        'asScrollable.min.css'
    ];

    public $js = [
        'jquery.asScrollable.all.js',
        'components/asscrollable.min.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\assets\vendor\BootstrapAsset',
        'app\assets\SiteAsset',
    ];
}
