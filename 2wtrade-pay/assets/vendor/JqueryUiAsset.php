<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class GridStackAsset
 * @package doindo\assets
 */
class JqueryUiAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/jquery-ui';

    public $js = [
        'jquery-ui.min.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
