<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class Html5ShivAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/html5shiv';

    public $js = [
        'html5shiv.min.js'
    ];

    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => View::POS_HEAD,
    ];
}
