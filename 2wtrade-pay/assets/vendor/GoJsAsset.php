<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class GoJsAsset
 * @package app\assets\vendor
 */
class GoJsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/go-js';

    public $js = [
        'go.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset'
    ];
}
