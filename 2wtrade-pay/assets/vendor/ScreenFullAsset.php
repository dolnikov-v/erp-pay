<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class ScreenFullAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/screenfull';

    public $js = [
        'screenfull.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
