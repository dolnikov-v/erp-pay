<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class LsCacheAsset
 * @package app\assets\vendor
 */
class LsCacheAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/lscache';

    public $js = [
        'lscache.min.js'
    ];
}
