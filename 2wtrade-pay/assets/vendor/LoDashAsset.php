<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class LoDashAsset
 * @package app\assets\vendor
 */
class LoDashAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/lodash';

    public $js = [
        'lodash.min.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
