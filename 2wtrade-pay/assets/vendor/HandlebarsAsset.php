<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class HandlebarsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/handlebars';

    public $js = [
        'handlebars.min.js'
    ];
}
