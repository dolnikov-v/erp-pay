<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AsScrollAsset
 * @package doindo\assets
 */
class AsScrollAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/asscroll';

    public $js = [
        'jquery-asScroll.min.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
