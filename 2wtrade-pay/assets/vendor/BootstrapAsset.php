<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapAsset
 * @package doindo\assets
 */
class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/bootstrap';

    public $css = [
        'bootstrap.min.css',
        'bootstrap-extend.min.css'
    ];

    public $js = [
        'bootstrap.min.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset'
    ];
}
