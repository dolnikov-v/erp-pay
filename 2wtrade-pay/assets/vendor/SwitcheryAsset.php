<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class SwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/switchery';

    public $css = [
        'switchery.css'
    ];

    public $js = [
        'switchery.min.js',
        'components/switchery.min.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
