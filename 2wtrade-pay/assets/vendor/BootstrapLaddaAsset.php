<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapLaddaAsset
 * @package app\assets\vendor
 */
class BootstrapLaddaAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/ladda-bootstrap';

    public $css = [
        'ladda.min.css',
    ];

    public $js = [
        'spin.min.js',
        'ladda.min.js',
        'components/ladda-bootstrap.min.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
