<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class JJsonViewerAsset
 * @package app\assets\vendor
 */
class JJsonViewerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/jjsonviewer';

    public $css = [
        'jjsonviewer.css'
    ];

    public $js = [
        'jjsonviewer.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
