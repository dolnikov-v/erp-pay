<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class SlidePanelAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/slidepanel';

    public $css = [
        'slidePanel.css'
    ];

    public $js = [
        'jquery-slidePanel.min.js',
        'components/slidepanel.min.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
