<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class RespondAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/respond';

    public $js = [
        'respond.min.js'
    ];

    public $jsOptions = [
        'condition' => 'lt IE 10',
        'position' => View::POS_HEAD,
    ];
}
