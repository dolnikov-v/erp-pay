<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class IntroJsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/intro-js';

    public $css = [
        'introjs.css'
    ];

    public $js = [
        'intro.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
