<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class JqueryAsset
 * @package app\assets\vendor
 */
class JqueryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/jquery';

    public $js = [
        'jquery.min.js',
    ];
}
