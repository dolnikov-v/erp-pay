<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class CropperAsset
 * @package app\assets\vendor
 */
class CropperAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/vendor/cropper';

    /**
     * @var array
     */
    public $js = [
        'cropper.min.js'
    ];

    /**
     * @var array
     */
    public $css = [
        'cropper.min.css'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
