<?php
namespace app\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AsHoverScrollAsset
 * @package doindo\assets
 */
class AsHoverScrollAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/ashoverscroll';

    public $js = [
        'jquery-asHoverScroll.min.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
