<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class GlyphIconAsset
 * @package app\assets\fonts
 */
class GlyphIconAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/fonts/glyphicons';

    public $css = [
        'glyphicons.min.css'
    ];
}
