<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class RobotoAsset extends AssetBundle
{
    public $css = [
        'http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300italic'
    ];
}
