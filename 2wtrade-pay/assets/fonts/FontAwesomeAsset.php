<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package app\assets\vendor
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/fonts/font-awesome';

    public $css = [
        'font-awesome.min.css'
    ];
}
