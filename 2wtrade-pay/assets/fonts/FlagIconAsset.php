<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class FlagIconAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/vendor/flag-icon-css';

    public $css = [
        'flag-icon.css'
    ];
}
