<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class WebIconsAsset
 * @package app\assets\fonts
 */
class WebIconsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/fonts/web-icons';

    public $css = [
        'web-icons.min.css'
    ];
}
