<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FontIonIconsAsset
 * @package app\assets\vendor
 */
class FontIonIconsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/fonts/ionicons';

    public $css = [
        'ionicons.min.css'
    ];
}
