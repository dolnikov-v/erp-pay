<?php
namespace app\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class BrandIconsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/vendor/fonts/brand-icons';

    public $css = [
        'brand-icons.min.css'
    ];
}
