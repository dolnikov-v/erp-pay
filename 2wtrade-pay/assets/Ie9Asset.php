<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package doindo\assets
 */
class Ie9Asset extends AssetBundle
{
    public $depends = [
        'app\assets\vendor\Html5ShivAsset',
    ];
}
