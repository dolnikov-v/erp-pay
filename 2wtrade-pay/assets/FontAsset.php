<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class FontAsset
 * @package app\assets
 */
class FontAsset extends AssetBundle
{
    public $depends = [
        'app\assets\fonts\FlagIconAsset',
        'app\assets\fonts\GlyphIconAsset',
        'app\assets\fonts\WebIconsAsset',
        'app\assets\fonts\FontAwesomeAsset',
        'app\assets\fonts\FontIonIconsAsset',
        'app\assets\fonts\BrandIconsAsset',
        'app\assets\fonts\RobotoAsset',
    ];
}
