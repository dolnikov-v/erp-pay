<?php
namespace app\assets\uikit;

use yii\web\AssetBundle;

/**
 * Class ButtonsAsset
 * @package app\assets\uikit
 */
class ButtonsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/components/buttons';

    /**
     * @var array
     */
    public $js = [
        'buttons.min.js',
    ];

    /**
     * @var array
     */
    public $css = [
        'buttons.min.css',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
