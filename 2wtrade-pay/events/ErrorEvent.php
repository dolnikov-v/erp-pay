<?php

namespace app\events;


use yii\base\Event;

/**
 * Class ErrorEvent
 * @package app\events
 */
class ErrorEvent extends Event
{
    /**
     * @var string
     */
    public $message;
}